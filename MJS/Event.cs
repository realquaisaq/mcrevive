﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace MJS
{
    public static class Event
    {
        public static bool Trigger(string Event, string Parameters)
        {
            Events.Log("Event " + Event + " was triggered with parameters: " + Parameters);
            if (!Exists(Event)) return false;
            Script.Execute("mjs/events/" + Event + ".mjs", Parameters);
            return true;
        }
        public static bool Exists(string Event)
        {
            return File.Exists("mjs/events/" + Event + ".mjs");
        }

        public static void LoadShit()
        {

        }
        public static void SaveShit()
        {
            int errLoc = 0;
            try
            {
                if (!File.Exists("memory/MJSvars.txt")) File.Create("memory/MJSvars.txt").Close();
                errLoc++;
                string[] lines = new string[Player.variableNames.Count + Map.variableNames.Count + 2];
                int i = 0;
                lines[i] = "Player variables: name[=]type[=]default";
                i++;
                errLoc++;
                int j = 0;
                foreach (string varName in Player.variableNames)
                {
                    string thetype = Player.variableTypes[j];
                    object thedefault = Player.variableDefaultValues[j];
                    lines[i] = varName + "[=]" + thetype + "[=]" + thetype == "string" || thetype == "double" ? (string)thedefault : thetype == "Level" ? ((Map)thedefault).name : thetype == "Player" ? ((Player)thedefault).name : "";
                    i++;
                    j++;
                }
                errLoc++;
                lines[i] = "Map variables: name[=]type[=]default";
                i++;
                j = 0;
                foreach (string varName in Map.variableNames)
                {
                    string thetype = Map.variableTypes[j];
                    object thedefault = Map.variableDefaultValues[j];
                    lines[i] = varName + "[=]" + thetype + "[=]" + thetype == "string" || thetype == "double" ? (string)thedefault : thetype == "Level" ? ((Map)thedefault).name : thetype == "Player" ? ((Player)thedefault).name : "";
                    i++;
                    j++;
                }
                errLoc++;
                File.WriteAllLines("memory/MJSvars.txt", lines);
            }
            catch (Exception e)
            {
                Events.Log("Error while saving shit on location " + errLoc + ": " + e.Message);
            }
        }
    }
}
