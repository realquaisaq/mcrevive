﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MJS
{
    public class Player
    {
        public static List<Player> players = new List<Player>();
        public static List<string> variableNames = new List<string>();
        public static List<string> variableTypes = new List<string>();
        public static List<object> variableDefaultValues = new List<object>();
        
        public Rank rank;
        public Map map;
        public string name;
        public string title;
        public string titleColor;
        public ushort[] pos = new ushort[3];
        public byte[] rot = new byte[2];
        public List<object> variableValues;

        public Player(string Name, string Title, string TitleColor, Map Map, Rank Rank)
        {
            name = Name;
            title = Title;
            titleColor = TitleColor;
            map = Map;
            rank = Rank;
            variableValues = new List<object>();
        }
        public void SendMessage(string msg)
        {
            Events.SendMessage(name, msg);
        }
        public void SendMap(Map Map)
        {
            Events.SendPlayerMap(name, Map.name);
        }
        public void SendPos(ushort x, ushort y, ushort z, byte rotx, byte roty)
        {
            Events.SendPlayerPos(name, x, y, z, rotx, roty);
        }
        public void SendBlockchange(ushort x, ushort y, ushort z, ushort type)
        {
            Events.SendPlayerBlock(name, x, y, z, type);
        }
        public void Kick(string msg)
        {
            Events.KickPlayer(name, msg);
        }

        public void UpdatePos(ushort x, ushort y, ushort z, byte rotx, byte roty)
        {
            pos[0] = x; pos[1] = y; pos[2] = z; rot[0] = rotx; rot[1] = roty;
        }

        public static Player Find(string name)
        {
            foreach (Player who in players)
                if (who.name == name)
                    return who;
            return null;
        }

        public static bool varExists(string name)
        {
            return variableNames.Contains(name);
        }
        public object varValue(string name)
        {
            int i = 0;
            foreach (string varName in variableNames)
            {
                if (varName == name) return variableValues[i];
                i++;
            }
            return null;
        }
        public static bool addVar(string name, object defaultValue)
        {
            if (variableNames.Contains(name)) return false;
            variableNames.Add(name);
            variableTypes.Add(defaultValue.GetType().Name);
            variableDefaultValues.Add(defaultValue);
            foreach (Player p in Player.players) p.variableValues.Add(defaultValue);
            return true;
        }
        public static bool delVar(string name)
        {
            if (!variableNames.Contains(name)) return false;
            int i = 0;
            foreach (string varName in variableNames)
            {
                if (varName == name) goto foundIndex;
                i++;
            }
        foundIndex:
            variableNames.RemoveRange(i, 1);
            variableTypes.RemoveRange(i, 1);
            foreach (Player p in Player.players)
            {
                p.variableValues.RemoveRange(i, 1);
            }
            return true;
        }
    }
}
