﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MJS
{
    public class Block
    {
        public static List<Block> blocks = new List<Block>();

        public string name;
        public ushort id;
        public Block(string Name, ushort ID)
        {
            name = Name;
            id = ID;
        }
        public static void Load(List<ushort> ids, List<string> names)
        {
            if (ids.Count != names.Count) throw new Exception("There is more " + (ids.Count < names.Count ? "'Block Names'" : "'Block IDs'") +
                " than " + (ids.Count < names.Count ? "'Block IDs'" : "'Block Names'") + ".");
            for (int i = 0; i < ids.Count; ++i)
                blocks.Insert(i, new Block(names[i], ids[i]));
        }
        public static ushort Number(string name)
        {
            foreach (Block b in blocks)
                if (b.name == name)
                    return b.id;
            return 255; //Null
        }
    }
}
