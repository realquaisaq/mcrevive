﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MJS
{
    public class Command
    {
        public static List<Command> commands = new List<Command>();

        public string name;
        public int permission;

        public Command(string Name, int Permission)
        {
            name = Name;
            permission = Permission;
        }
        public void Use(Player user, string message)
        {
            Events.UseCommand(name, user.name, message);
        }

        public static void Load(List<string> names, List<int> permissions)
        {
            if (names.Count != permissions.Count) throw new Exception("There is more " + (permissions.Count < names.Count ? "'Command Names'" : "'Command Permissions'") +
                " than " + (permissions.Count < names.Count ? "'Command Permissions'" : "'Command Names'") + ".");
            for (int i = 0; i < names.Count; ++i)
                commands.Insert(i, new Command(names[i], permissions[i]));
        }
        public static Command Find(string name)
        {
            foreach (Command c in commands)
                if (c.name == name)
                    return c;
            return null;
        }
    }
}
