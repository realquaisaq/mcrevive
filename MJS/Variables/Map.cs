﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MJS
{
    public class Map
    {
        public static List<Map> maps = new List<Map>();
        public static List<string> variableNames = new List<string>();
        public static List<string> variableTypes = new List<string>();
        public static List<object> variableDefaultValues = new List<object>();
        
        public string name;
        public string owner;
        public ushort width;
        public ushort height;
        public ushort depth;
        public ushort[] blocks;
        public List<Player> players = new List<Player>();
        public List<object> variableValues = new List<object>();

        public Map(string Name, ushort Width, ushort Height, ushort Depth, ushort[] Blocks, string Owner)
        {
            name = Name;
            width = Width;
            height = Height;
            depth = Depth;
            blocks = Blocks;
            owner = Owner;
        }
        public void Save()
        {
            Events.SaveMap(name);
        }
        public void Unload()
        {
            Events.UnloadMap(name);
        }
        public ushort GetBlock(ushort x, ushort y, ushort z)
        {
            if (x < 0) { return 255; }
            if (x >= width) { return 255; }
            if (y < 0) { return 255; }
            if (y >= depth) { return 255; }
            if (z < 0) { return 255; }
            if (z >= height) { return 255; }
            return blocks[x + z * width + y * width * height];
        }
        public void SetBlock(ushort x, ushort y, ushort z, ushort type)
        {
            blocks[x + width * z + width * height * y] = type;
            Events.SetBlock(name, x, y, z, type);
        }
        public static void SetBlock(string map, ushort x, ushort y, ushort z, ushort type)
        {
            Map.Find(map).SetBlock(x, y, z, type);
        }
        public static void UpdateBlocks(string map, ushort[] Blocks)
        {
            Map.Find(map).blocks = Blocks;
        }
 
        public static Map Find(string name)
        {
            foreach (Map m in maps)
                if (m.name == name)
                    return m;
            return null;
        }

        public static bool varExists(string name)
        {
            return variableNames.Contains(name);
        }
        public object varValue(string name)
        {
            int i = 0;
            foreach (string varName in variableNames)
            {
                if (varName == name) return variableValues[i];
                i++;
            }
            return null;
        }
        public static bool addVar(string name, object defaultValue)
        {
            if (variableNames.Contains(name)) return false;
            variableNames.Add(name);
            variableTypes.Add(defaultValue.GetType().Name);
            variableDefaultValues.Add(defaultValue);
            foreach (Map m in maps) m.variableValues.Add(defaultValue);
            return true;
        }
        public static bool delVar(string name)
        {
            if (!variableNames.Contains(name)) return false;
            int i = 0;
            foreach (string varName in variableNames)
            {
                if (varName == name) goto foundIndex;
                i++;
            }
        foundIndex:
            variableNames.RemoveRange(i, 1);
            variableTypes.RemoveRange(i, 1);
            foreach (Map m in maps) m.variableValues.RemoveRange(i, 1);
            return true;
        }
    }
}
