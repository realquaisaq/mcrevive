﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MJS
{
    public class Rank
    {
        public static List<Rank> ranks = new List<Rank>();
        
        public string name;
        public int permission;
        public string color;

        public Rank(string Name, int Permission, string Color)
        {
            name = Name;
            permission = Permission;
            color = Color;
        }
    }
}
