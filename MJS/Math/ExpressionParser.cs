using System;
using System.Collections;
using System.Text;

namespace MJS
{
  public class ExpressionParser
  {
    private Hashtable ops;
    private Hashtable trees;
    private Hashtable htbl;
    private Hashtable spconst;
    private int maxoplength;
    private int sb_init;

    public ExpressionParser()
    {
      this.ops = new Hashtable(52);
      this.spconst = new Hashtable(12);
      this.trees = new Hashtable(101);
      this.ops.Add((object) "^", (object) new Operator("^", 2, 3));
      this.ops.Add((object) "+", (object) new Operator("+", 2, 6));
      this.ops.Add((object) "-", (object) new Operator("-", 2, 6));
      this.ops.Add((object) "/", (object) new Operator("/", 2, 4));
      this.ops.Add((object) "*", (object) new Operator("*", 2, 4));
      this.ops.Add((object) "cos", (object) new Operator("cos", 1, 2));
      this.ops.Add((object) "sin", (object) new Operator("sin", 1, 2));
      this.ops.Add((object) "exp", (object) new Operator("exp", 1, 2));
      this.ops.Add((object) "ln", (object) new Operator("ln", 1, 2));
      this.ops.Add((object) "tan", (object) new Operator("tan", 1, 2));
      this.ops.Add((object) "acos", (object) new Operator("acos", 1, 2));
      this.ops.Add((object) "asin", (object) new Operator("asin", 1, 2));
      this.ops.Add((object) "atan", (object) new Operator("atan", 1, 2));
      this.ops.Add((object) "cosh", (object) new Operator("cosh", 1, 2));
      this.ops.Add((object) "sinh", (object) new Operator("sinh", 1, 2));
      this.ops.Add((object) "tanh", (object) new Operator("tanh", 1, 2));
      this.ops.Add((object) "sqrt", (object) new Operator("sqrt", 1, 2));
      this.ops.Add((object) "cotan", (object) new Operator("cotan", 1, 2));
      this.ops.Add((object) "fpart", (object) new Operator("fpart", 1, 2));
      this.ops.Add((object) "acotan", (object) new Operator("acotan", 1, 2));
      this.ops.Add((object) "round", (object) new Operator("round", 1, 2));
      this.ops.Add((object) "ceil", (object) new Operator("ceil", 1, 2));
      this.ops.Add((object) "floor", (object) new Operator("floor", 1, 2));
      this.ops.Add((object) "fac", (object) new Operator("fac", 1, 2));
      this.ops.Add((object) "sfac", (object) new Operator("sfac", 1, 2));
      this.ops.Add((object) "abs", (object) new Operator("abs", 1, 2));
      this.ops.Add((object) "log", (object) new Operator("log", 2, 5));
      this.ops.Add((object) "%", (object) new Operator("%", 2, 4));
      this.ops.Add((object) ">", (object) new Operator(">", 2, 7));
      this.ops.Add((object) "<", (object) new Operator("<", 2, 7));
      this.ops.Add((object) "&&", (object) new Operator("&&", 2, 8));
      this.ops.Add((object) "==", (object) new Operator("==", 2, 7));
      this.ops.Add((object) "!=", (object) new Operator("!=", 2, 7));
      this.ops.Add((object) "||", (object) new Operator("||", 2, 9));
      this.ops.Add((object) "!", (object) new Operator("!", 1, 1));
      this.ops.Add((object) ">=", (object) new Operator(">=", 2, 7));
      this.ops.Add((object) "<=", (object) new Operator("<=", 2, 7));
      this.spconst.Add((object) "euler", (object) 2.71828182845905);
      this.spconst.Add((object) "pi", (object) 3.14159265358979);
      this.spconst.Add((object) "nan", (object) double.NaN);
      this.spconst.Add((object) "infinity", (object) double.PositiveInfinity);
      this.spconst.Add((object) "true", (object) 1.0);
      this.spconst.Add((object) "false", (object) 0.0);
      this.maxoplength = 6;
      this.sb_init = 50;
    }

    private bool matchParant(string exp)
    {
      int num = 0;
      int length = exp.Length;
      for (int index = 0; index < length; ++index)
      {
        if ((int) exp[index] == 40)
          ++num;
        else if ((int) exp[index] == 41)
          --num;
      }
      return num == 0;
    }

    private bool isAlpha(char ch)
    {
      if ((int) ch >= 97 && (int) ch <= 122)
        return true;
      if ((int) ch >= 65)
        return (int) ch <= 90;
      else
        return false;
    }

    private bool isVariable(string str)
    {
      int length = str.Length;
      if (this.isAllNumbers(str))
        return false;
      for (int index = 0; index < length; ++index)
      {
        if (this.getOp(str, index) != null || this.isAllowedSym(str[index]))
          return false;
      }
      return true;
    }

    private bool isConstant(char ch)
    {
      return char.IsDigit(ch);
    }

    private bool isConstant(string exp)
    {
      try
      {
        if (double.IsNaN(double.Parse(exp)))
          return false;
      }
      catch
      {
        return false;
      }
      return true;
    }

    private bool isAllNumbers(string str)
    {
      int index = 0;
      bool flag = false;
      switch (str[0])
      {
        case '-':
        case '+':
          index = 1;
          break;
      }
      for (int length = str.Length; index < length; ++index)
      {
        char c = str[index];
        if (!char.IsDigit(c) && ((int) c != 46 && (int) c != 44 || flag))
          return false;
        flag = (int) c == 46 || (int) c == 44;
      }
      return true;
    }

    private bool isOperator(string str)
    {
      return this.ops.ContainsKey((object) str);
    }

    private bool isTwoArgOp(string str)
    {
      if (str == null)
        return false;
      object obj = this.ops[(object) str];
      if (obj == null)
        return false;
      else
        return ((Operator) obj).arguments() == 2;
    }

    private bool isInteger(double a)
    {
      return a - (double) (int) a == 0.0;
    }

    private bool isEven(int a)
    {
      return this.isInteger((double) (a / 2));
    }

    private bool isAllowedSym(char s)
    {
      if ((int) s != 44 && (int) s != 46 && ((int) s != 41 && (int) s != 40) && ((int) s != 62 && (int) s != 60 && ((int) s != 38 && (int) s != 61)))
        return (int) s == 124;
      else
        return true;
    }

    private void Syntax(string exp)
    {
      int index = 0;
      if (!this.matchParant(exp))
        throw new Exception("Non matching paranthesis");
      int length1 = exp.Length;
      while (index < length1)
      {
        try
        {
          string op1;
          if ((op1 = this.getOp(exp, index)) != null)
          {
            int length2 = op1.Length;
            index += length2;
            string op2 = this.getOp(exp, index);
            if (op2 != null)
            {
              if (this.isTwoArgOp(op2))
              {
                if (!op2.Equals("+"))
                {
                  if (!op2.Equals("-"))
                    throw new Exception("Syntax error near -> " + exp.Substring(index - length2));
                }
              }
            }
          }
          else
          {
            if (!this.isAlpha(exp[index]) && !this.isConstant(exp[index]) && !this.isAllowedSym(exp[index]))
              throw new Exception("Syntax error near -> " + exp.Substring(index));
            ++index;
          }
        }
        catch (IndexOutOfRangeException ex)
        {
            ex = new IndexOutOfRangeException();
          ++index;
        }
      }
    }

    private string putMult(string exp)
    {
      int index = 0;
      int num = 0;
      string str = (string) null;
      StringBuilder stringBuilder = new StringBuilder(exp);
      int length = exp.Length;
      while (index < length)
      {
        try
        {
          if ((str = this.getOp(exp, index)) != null && !this.isTwoArgOp(str) && this.isAlpha(exp[index - 1]))
          {
            stringBuilder.Insert(index + num, "*");
            ++num;
          }
          else if (this.isAlpha(exp[index]) && this.isConstant(exp[index - 1]) && (str == null || !str.Equals("log")))
          {
            stringBuilder.Insert(index + num, "*");
            ++num;
          }
          else if ((int) exp[index] == 40 && this.isConstant(exp[index - 1]))
          {
            stringBuilder.Insert(index + num, "*");
            ++num;
          }
          else if (this.isAlpha(exp[index]) && (int) exp[index - 1] == 41 && (str == null || !str.Equals("log")))
          {
            stringBuilder.Insert(index + num, "*");
            ++num;
          }
          else if ((int) exp[index] == 40 && (int) exp[index - 1] == 41)
          {
            stringBuilder.Insert(index + num, "*");
            ++num;
          }
          else if ((int) exp[index] == 40)
          {
            if (this.isAlpha(exp[index - 1]))
            {
              if (this.backTrack(exp.Substring(0, index)) == null)
              {
                stringBuilder.Insert(index + num, "*");
                ++num;
              }
            }
          }
        }
        catch
        {
        }
        if (str != null)
          index += str.Length;
        else
          ++index;
        str = (string) null;
      }
      return ((object) stringBuilder).ToString();
    }

    private string parseE(string exp)
    {
      StringBuilder stringBuilder = new StringBuilder(exp);
      int num;
      int index = num = 0;
      for (int length = exp.Length; index < length; ++index)
      {
        try
        {
          if ((int) exp[index] == 101)
          {
            if (char.IsDigit(exp[index - 1]))
            {
              if (!char.IsDigit(exp[index + 1]))
              {
                if ((int) exp[index + 1] != 45)
                {
                  if ((int) exp[index + 1] != 43)
                    continue;
                }
                if (!char.IsDigit(exp[index + 2]))
                  continue;
              }
              stringBuilder[index + num] = '*';
              stringBuilder.Insert(index + num + 1, "10^");
              num += 3;
            }
          }
        }
        catch
        {
        }
      }
      return ((object) stringBuilder).ToString();
    }

    private string skipSpaces(string str)
    {
      int index = 0;
      int length = str.Length;
      StringBuilder stringBuilder = new StringBuilder(length);
      for (; index < length; ++index)
      {
        if ((int) str[index] != 32)
          stringBuilder.Append(str[index]);
      }
      return ((object) stringBuilder).ToString();
    }

    private int match(string exp, int index)
    {
      int length = exp.Length;
      int index1 = index;
      int num = 0;
      for (; index1 < length; ++index1)
      {
        if ((int) exp[index1] == 40)
          ++num;
        else if ((int) exp[index1] == 41)
          --num;
        if (num == 0)
          return index1;
      }
      return index;
    }

    private string getOp(string exp, int index)
    {
      int length = exp.Length;
      for (int index1 = 0; index1 < this.maxoplength; ++index1)
      {
        if (index >= 0 && index + this.maxoplength - index1 <= length)
        {
          string str = exp.Substring(index, this.maxoplength - index1);
          if (this.isOperator(str))
            return str;
        }
      }
      return (string) null;
    }

    private Node parse(string exp)
    {
      Node node = (Node) null;
      int index;
      int num1 = index = 0;
      int length = exp.Length;
      if (length == 0)
        throw new Exception("Wrong number of arguments to operator");
      int num2;
      if ((int) exp[0] == 40 && (num2 = this.match(exp, 0)) == length - 1)
        return this.parse(exp.Substring(1, num2 - 1));
      if (this.isVariable(exp))
        return new Node(exp);
      if (this.isAllNumbers(exp))
      {
        try
        {
          return new Node(double.Parse(exp));
        }
        catch (FormatException ex)
        {
            ex = new FormatException();
            throw new Exception("Syntax error-> " + exp + " (not using regional decimal separator?)");
        }
      }
      else
      {
        while (index < length)
        {
          string op1;
          if ((op1 = this.getOp(exp, index)) == null)
          {
            string exp1 = this.arg((string) null, exp, index);
            string op2 = this.getOp(exp, index + exp1.Length);
            if (op2 == null)
              throw new Exception("Missing operator");
            if (this.isTwoArgOp(op2))
            {
              string exp2 = this.arg(op2, exp, index + exp1.Length + op2.Length);
              if (exp2.Equals(""))
                throw new Exception("Wrong number of arguments to operator " + op2);
              node = new Node(op2, this.parse(exp1), this.parse(exp2));
              index += exp1.Length + op2.Length + exp2.Length;
            }
            else
            {
              if (exp1.Equals(""))
                throw new Exception("Wrong number of arguments to operator " + op2);
              node = new Node(op2, this.parse(exp1));
              index += exp1.Length + op2.Length;
            }
          }
          else if (this.isTwoArgOp(op1))
          {
            string exp1 = this.arg(op1, exp, index + op1.Length);
            if (exp1.Equals(""))
              throw new Exception("Wrong number of arguments to operator " + op1);
            if (node == null)
            {
              if (!op1.Equals("+") && !op1.Equals("-"))
                throw new Exception("Wrong number of arguments to operator " + op1);
              node = new Node(0.0);
            }
            node = new Node(op1, node, this.parse(exp1));
            index += exp1.Length + op1.Length;
          }
          else
          {
            string exp1 = this.arg(op1, exp, index + op1.Length);
            if (exp1.Equals(""))
              throw new Exception("Wrong number of arguments to operator " + op1);
            node = new Node(op1, this.parse(exp1));
            index += exp1.Length + op1.Length;
          }
        }
        return node;
      }
    }

    private string arg(string _operator, string exp, int index)
    {
      int length = exp.Length;
      StringBuilder stringBuilder = new StringBuilder(this.sb_init);
      int index1 = index;
      int num1 = _operator != null ? ((Operator) this.ops[(object) _operator]).precedence() : -1;
      while (index1 < length)
      {
        if ((int) exp[index1] == 40)
        {
          int num2 = this.match(exp, index1);
          stringBuilder.Append(exp.Substring(index1, num2 + 1 - index1));
          index1 = num2 + 1;
        }
        else
        {
          string op;
          if ((op = this.getOp(exp, index1)) != null)
          {
            if (stringBuilder.Length != 0 && !this.isTwoArgOp(this.backTrack(((object) stringBuilder).ToString())) && ((Operator) this.ops[(object) op]).precedence() >= num1)
              return ((object) stringBuilder).ToString();
            stringBuilder.Append(op);
            index1 += op.Length;
          }
          else
          {
            stringBuilder.Append(exp[index1]);
            ++index1;
          }
        }
      }
      return ((object) stringBuilder).ToString();
    }

    private string backTrack(string str)
    {
      int length = str.Length;
      try
      {
        for (int index = 0; index <= this.maxoplength; ++index)
        {
          string op;
          if ((op = this.getOp(str, length - 1 - this.maxoplength + index)) != null && length - this.maxoplength - 1 + index + op.Length == length)
            return op;
        }
      }
      catch
      {
      }
      return (string) null;
    }

    private double fac(double val)
    {
      if (!this.isInteger(val) || val < 0.0)
        return double.NaN;
      if (val <= 1.0)
        return 1.0;
      else
        return val * this.fac(val - 1.0);
    }

    private double sfac(double val)
    {
      if (!this.isInteger(val) || val < 0.0)
        return double.NaN;
      if (val <= 1.0)
        return 1.0;
      else
        return val * this.sfac(val - 2.0);
    }

    private double fpart(double val)
    {
      if (val >= 0.0)
        return val - Math.Floor(val);
      else
        return val - Math.Ceiling(val);
    }

    private double toValue(Node tree)
    {
      if (tree.getType() == Node.TYPE_CONSTANT)
        return tree.getValue();
      if (tree.getType() == Node.TYPE_VARIABLE)
      {
        string variable = tree.getVariable();
        if (this.spconst.ContainsKey((object) variable))
          return (double) this.spconst[(object) variable];
        string str = this.get(variable);
        if (this.isConstant(str))
          return double.Parse(str);
        this.Syntax(str);
        return this.toValue(this.parse(this.putMult(this.parseE(str))));
      }
      else
      {
        string @operator = tree.getOperator();
        Node tree1 = tree.arg1();
        if (tree.arguments() == 2)
        {
          Node tree2 = tree.arg2();
          if (@operator.Equals("+"))
            return this.toValue(tree1) + this.toValue(tree2);
          if (@operator.Equals("-"))
            return this.toValue(tree1) - this.toValue(tree2);
          if (@operator.Equals("*"))
            return this.toValue(tree1) * this.toValue(tree2);
          if (@operator.Equals("/"))
            return this.toValue(tree1) / this.toValue(tree2);
          if (@operator.Equals("^"))
            return Math.Pow(this.toValue(tree1), this.toValue(tree2));
          if (@operator.Equals("log"))
            return Math.Log(this.toValue(tree2)) / Math.Log(this.toValue(tree1));
          if (@operator.Equals("%"))
            return this.toValue(tree1) % this.toValue(tree2);
          if (@operator.Equals("=="))
          {
            if (this.toValue(tree1) != this.toValue(tree2))
              return 0.0;
            else
              return 1.0;
          }
          else if (@operator.Equals("!="))
          {
            if (this.toValue(tree1) == this.toValue(tree2))
              return 0.0;
            else
              return 1.0;
          }
          else if (@operator.Equals("<"))
          {
            if (this.toValue(tree1) >= this.toValue(tree2))
              return 0.0;
            else
              return 1.0;
          }
          else if (@operator.Equals(">"))
          {
            if (this.toValue(tree1) <= this.toValue(tree2))
              return 0.0;
            else
              return 1.0;
          }
          else if (@operator.Equals("&&"))
          {
            if (this.toValue(tree1) != 1.0 || this.toValue(tree2) != 1.0)
              return 0.0;
            else
              return 1.0;
          }
          else if (@operator.Equals("||"))
          {
            if (this.toValue(tree1) != 1.0 && this.toValue(tree2) != 1.0)
              return 0.0;
            else
              return 1.0;
          }
          else if (@operator.Equals(">="))
          {
            if (this.toValue(tree1) < this.toValue(tree2))
              return 0.0;
            else
              return 1.0;
          }
          else if (@operator.Equals("<="))
          {
            if (this.toValue(tree1) > this.toValue(tree2))
              return 0.0;
            else
              return 1.0;
          }
        }
        else
        {
          if (@operator.Equals("sqrt"))
            return Math.Sqrt(this.toValue(tree1));
          if (@operator.Equals("sin"))
            return Math.Sin(this.toValue(tree1));
          if (@operator.Equals("cos"))
            return Math.Cos(this.toValue(tree1));
          if (@operator.Equals("tan"))
            return Math.Tan(this.toValue(tree1));
          if (@operator.Equals("asin"))
            return Math.Asin(this.toValue(tree1));
          if (@operator.Equals("acos"))
            return Math.Acos(this.toValue(tree1));
          if (@operator.Equals("atan"))
            return Math.Atan(this.toValue(tree1));
          if (@operator.Equals("ln"))
            return Math.Log(this.toValue(tree1));
          if (@operator.Equals("exp"))
            return Math.Exp(this.toValue(tree1));
          if (@operator.Equals("cotan"))
            return 1.0 / Math.Tan(this.toValue(tree1));
          if (@operator.Equals("acotan"))
            return 1.5707963267949 - Math.Atan(this.toValue(tree1));
          if (@operator.Equals("ceil"))
            return Math.Ceiling(this.toValue(tree1));
          if (@operator.Equals("round"))
            return Math.Round(this.toValue(tree1));
          if (@operator.Equals("floor"))
            return Math.Floor(this.toValue(tree1));
          if (@operator.Equals("fac"))
            return this.fac(this.toValue(tree1));
          if (@operator.Equals("abs"))
            return Math.Abs(this.toValue(tree1));
          if (@operator.Equals("fpart"))
            return this.fpart(this.toValue(tree1));
          if (@operator.Equals("sfac"))
            return this.sfac(this.toValue(tree1));
          if (@operator.Equals("sinh"))
          {
            double d = this.toValue(tree1);
            return (Math.Exp(d) - 1.0 / Math.Exp(d)) / 2.0;
          }
          else if (@operator.Equals("cosh"))
          {
            double d = this.toValue(tree1);
            return (Math.Exp(d) + 1.0 / Math.Exp(d)) / 2.0;
          }
          else if (@operator.Equals("tanh"))
          {
            double d = this.toValue(tree1);
            return (Math.Exp(d) - 1.0 / Math.Exp(d)) / 2.0 / ((Math.Exp(d) + 1.0 / Math.Exp(d)) / 2.0);
          }
          else if (@operator.Equals("!"))
          {
            if (this.toValue(tree1) == 1.0)
              return 0.0;
            else
              return 1.0;
          }
        }
        throw new Exception("Unknown operator");
      }
    }

    private string get(string key)
    {
      object obj = this.htbl[(object) key];
      if (obj == null)
        throw new Exception("No value associated with " + key);
      try
      {
        return (string) obj;
      }
      catch
      {
        throw new Exception("Wrong type value for " + key + " expected String");
      }
    }

    public double Parse(string exp, Hashtable tbl)
    {
      if (exp == null || exp.Equals(""))
        throw new Exception("First argument to method eval is null or empty string");
      if (tbl == null)
        return this.Parse(exp, new Hashtable());
      this.htbl = tbl;
      string exp1 = this.skipSpaces(exp.ToLower());
      this.sb_init = exp1.Length;
      try
      {
        double num;
        if (this.trees.ContainsKey((object) exp1))
        {
          num = this.toValue((Node) this.trees[(object) exp1]);
        }
        else
        {
          this.Syntax(exp1);
          Node tree = this.parse(this.putMult(this.parseE(exp1)));
          num = this.toValue(tree);
          this.trees.Add((object) exp1, (object) tree);
        }
        return num;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }
  }
}
