﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MJS
{
    public static class Events
    {
        public delegate void CMD(string cmd, string who, string msg);
        public delegate void Empty();
        public delegate void MSG(string msg);
        public delegate void SendBlock(string who, ushort x, ushort y, ushort z, ushort type);
        public delegate void SendMap(string who, string map);
        public delegate void SendPos(string who, ushort x, ushort y, ushort z, byte rotx, byte roty);
        public delegate void PMSG(string who, string msg);
            
        public static CMD OnUseCommand;
        public static Empty OnGetMaps;
        public static MSG OnLog;
        public static MSG OnGlobalMessage;
        public static MSG OnLoadMap;
        public static MSG OnUnloadMap;
        public static MSG OnSaveMap;
        public static SendBlock OnSetBlock;
        public static SendBlock OnSendPlayerBlock;
        public static SendMap OnSendPlayerMap;
        public static SendPos OnSendPlayerPos;
        public static PMSG OnKickPlayer;
        public static PMSG OnSendMessage;

        public static void UseCommand(string cmd, string who, string msg) { if (OnUseCommand != null) OnUseCommand(cmd, who, msg); }
        public static void GetMaps() { if (OnGetMaps != null) OnGetMaps(); }
        public static void Log(string msg) { if (OnLog != null) OnLog(msg); }
        public static void GlobalMessage(string msg) { if (OnGlobalMessage != null) OnGlobalMessage(msg); }
        public static void LoadMap(string map) { if (OnLoadMap != null) OnLoadMap(map); }
        public static void UnloadMap(string map) { if (OnUnloadMap != null) OnUnloadMap(map); }
        public static void SaveMap(string map) { if (OnSaveMap != null) OnSaveMap(map); }
        public static void SetBlock(string who, ushort x, ushort y, ushort z, ushort type) { if (OnSetBlock != null) OnSetBlock(who, x, y, z, type); }
        public static void SendPlayerBlock(string who, ushort x, ushort y, ushort z, ushort type) { if (OnSendPlayerBlock != null) OnSendPlayerBlock(who, x, y, z, type); }
        public static void SendPlayerMap(string who, string map) { if (OnSendPlayerMap != null) OnSendPlayerMap(who, map); }
        public static void SendPlayerPos(string who, ushort x, ushort y, ushort z, byte rotx, byte roty) { if (OnSendPlayerPos != null) OnSendPlayerPos(who, x, y, z, rotx, roty); }
        public static void KickPlayer(string who, string msg) { if (OnKickPlayer != null) OnKickPlayer(who, msg); }
        public static void SendMessage(string who, string msg) { if (OnSendMessage != null) OnSendMessage(who, msg); }
    }
}
