This is a simple tutorial on how to set up you MCQuai server and tips to make it better!

1. Run the MCQuai.exe(CLI console) which is located in the downloaded folder.
2.  Shut down the CLI.
3.  In the folder "properties", open the different files with Notepad, and edit the settings. Then Save
4. Open the Ranks folder and in the owner.txt place you username(Example: soccer101nic). Then save it.
5. Now open the MCQuai.exe(GUI console) and your server is live and ready to go.

**Tips and Tricks**
1. Placing a 0.0.0 in front of your server name to get it higher up on the WoM Client Server List
2. Add custom ranks and be nice to your players
3. Dont ban Devs or you will NOT be happy :p
4. Have Fun!!!!!!!!
