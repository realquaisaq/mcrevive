This is a simple tutorial on how to set up you MCRevive server and tips to make it better!

1. Run the MCRevive.exe(CLI console) which is located in the downloaded folder.
2.  Shut down the CLI.
3.  In the folder "properties", open the different files with Notepad and edit the settings. Then Save
4. Open the Ranks folder and in the owner.txt place you username(Example: quaisaq). Then save it.
5. Now open the MCRevive.exe(CLI console) and your server is live and ready to go.

**Tips and Tricks**
1. Placing a 0.0.0 in front of your server name to get it higher up on the WoM Client Server List
2. Add custom ranks and be nice to your players
3. Sign up on the MCRevive Forums, located at http://www.mcrevive.com/forums/
3. Have Fun!!!!!!!!
