 /*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace MCRevive.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            Server.usingCLI = true;
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(GlobalExHandler);
            Server s = new Server();
            s.OnLog += Log;
            s.OnCommand += Log;
            s.OnMJS += Log;
            s.OnSettingsUpdate += SettingsUpdate;
            s.Start();
            Server.ParseInput();
            if (Server.shuttingdown) { Thread.Sleep(5000); Server.Exit(); }
            if (!Server.Updated)
                if (Server.updateCheck)
                    Log("Update found!");
        }

        static void SettingsUpdate()
        {
            Console.Title = Server.name + " - R" + Server.Version;
        }

        static void Log(string message)
        {
            Console.WriteLine(message);
        }
        public static void GlobalExHandler(object sender, UnhandledExceptionEventArgs e)
        {

            Exception ex = (Exception)e.ExceptionObject;
            Server.ErrorLog(ex);
            Thread.Sleep(500);

            if (Server.restartOnError)
                Program.restartMe();
            else
                Program.restartMe(false);
        }
        static public void restartMe(bool fullRestart = true)
        {
            Thread restartThread = new Thread(new ThreadStart(delegate
            {
                if (!Server.shuttingdown)
                {
                    saveAll();

                    Server.shuttingdown = true;

                    try { Server.listen.Close(); }
                    catch { }
                    Server.forceShutdown = true;
                    if (!fullRestart)
                    {
                        Server.s.Start();
                        Server.forceShutdown = false;
                    }
                    else
                        Server.Restart();
                }
            }));
            restartThread.Start();
        }
        static public void saveAll()
        {
            try
            {
                List<Player> kickList = new List<Player>();
                kickList.AddRange(Player.players);
                kickList.ForEach((p) => { p.Kick("Server restarted! Rejoin soon!"); });
            }
            catch (Exception exc) { Server.ErrorLog(exc); }
        }
    }
}
