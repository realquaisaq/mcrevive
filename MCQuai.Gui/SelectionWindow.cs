﻿
using System;
using System.Windows.Forms;

namespace MCRevive.Gui
{
    public partial class SelectionWindow : Form
    {
        public enum WindowKind { Rank, Achievement, MJS };
        public WindowKind WK = WindowKind.Rank;

        public SelectionWindow()
        {
            InitializeComponent();
        }

        private void SelectionWindow_Load(object sender, EventArgs e)
        {
            int i = 1;
            int newline = 6;
            int btnnum = 1;
            int oldposX = 0;
            int oldposY = 0;

            if (WK == WindowKind.Rank)
            {
                this.Text = "Select rank to give";
                Group.GroupList.ForEach((grp) =>
                {
                    if (grp.name.ToLower() != "nobody" && grp.name.ToLower() != "banned")
                    {
                        if (btnnum + i > newline) { this.Height += 50; newline += 3; }
                        CButton btn = new CButton();

                        btn.Location = new System.Drawing.Point(12 + oldposX, 12 + oldposY);
                        btn.Name = "btn" + grp.name;
                        btn.Size = new System.Drawing.Size(100, 50);
                        btn.TabIndex = i + btnnum;
                        btn.Text = grp.name;
                        btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                        btn.UseVisualStyleBackColor = true;
                        btn.Click += new System.EventHandler(this.btn_Click);
                        this.Controls.Add(btn);

                        if (i != 3) { oldposX += 100; i++; }
                        else { oldposX = 0; oldposY += 50; i = 1; btnnum += 3; }
                    }
                });
            }
            else if (WK == WindowKind.Achievement)
            {
                this.Width += 100;
                this.Text = "Click the achievements you would like";
                newline = 8;
                Server.allAchievements.ForEach((ach) =>
                {
                    if (btnnum > newline) { this.Height += 65; newline += 4; }
                    CheckBox chk = new CheckBox();

                    chk.Appearance = Appearance.Button;
                    chk.Location = new System.Drawing.Point(12 + oldposX, 12 + oldposY);
                    chk.Name = "chk" + ach.realname + ((Server.allAchievements.Find(a => a.filename == ach.filename).allowed) ? "t" : "f");
                    chk.Size = new System.Drawing.Size(100, 50);
                    chk.TabIndex = i + btnnum;
                    chk.Text = ach.realname;
                    chk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    chk.Checked = Server.allAchievements.Find(a => a.filename == ach.filename).allowed;
                    chk.CheckedChanged += delegate { chk_CheckedChanged(chk.Checked, chk.Text); };
                    chk.UseVisualStyleBackColor = true;
                    this.Controls.Add(chk);

                    if (i != 4) { oldposX += 100; i++; }
                    else { oldposX = 0; oldposY += 50; i = 1; btnnum += 4; }
                });

                CButton btn = new CButton();
                btn.Location = new System.Drawing.Point(12, oldposY + 62);
                btn.Name = "btnSave";
                btn.Size = new System.Drawing.Size(180, 30);
                btn.TabIndex = i + btnnum + 1;
                btn.Text = "Save";
                btn.UseVisualStyleBackColor = true;
                btn.Click += new EventHandler(btnSave_Click);
                this.Controls.Add(btn);

                btn = new CButton();
                btn.Location = new System.Drawing.Point(Width - 208, oldposY + 62);
                btn.Name = "btnDiscard";
                btn.Size = new System.Drawing.Size(180, 30);
                btn.TabIndex = i + btnnum + 2;
                btn.Text = "Discard";
                btn.UseVisualStyleBackColor = true;
                btn.Click += new EventHandler(btnDiscard_Click);
                this.Controls.Add(btn);
                //this.Height += 20;
            }
            else if (WK == WindowKind.MJS)
            {
                Server.s.OnMJS += updateLog;
                this.Width += 100;
                this.Height += 200;
                this.Text = "MJS Log";
                TextBox txt = new TextBox();
                txt.Location = new System.Drawing.Point(12, 12);
                txt.Name = "txtMJSLog";
                txt.Multiline = true;
                txt.ReadOnly = true;
                txt.Size = new System.Drawing.Size(400,300);
                txt.TabIndex = 0;
                txt.Text = Program.MJSLog;
                txt.SelectedText = "";
                this.Controls.Add(txt);
            }
        }
        private void updateLog(string p)
        {
            foreach (Control c in this.Controls)
                if (c.Name == "txtMJSLog")
                    c.Text = Program.MJSLog;
        }

        private void btn_Click(object sender, EventArgs e)
        {
            Command.all.Find("setrank").Use(null, Program.selected + " " + sender.ToString().Split(':')[1].Trim().ToLower());
            GuiWindow.RankWindowUse = false;
            this.Dispose();
        }

        private void chk_CheckedChanged(bool Checked, string ach)
        {
            foreach (Control tP in Controls)
            {
                if (tP is CheckBox)
                {
                    if (tP.Text == ach) tP.Name = "chk" + tP.Name.Substring(3, tP.Name.Length - 4) + ((Checked) ? "t" : "f");
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Server.allAchievements = new System.Collections.Generic.List<Server.achievements>();
            foreach (Control tP in Controls)
            {
                if (tP is CheckBox)
                {
                    Server.achievements ds = new Server.achievements();
                    ds.realname = tP.Name.Substring(3, tP.Name.Length - 4);
                    ds.filename = tP.Name.Replace(" ", "-").Replace("'", "").ToLower().Substring(3, tP.Name.Length - 4);
                    ds.allowed = (tP.Name.Remove(0, tP.Name.Length - 1) == "t");
                    Server.allAchievements.Add(ds);
                }
            }
            MCRevive.Properties.Save("achievements");
            this.Dispose();
        }

        private void btnDiscard_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
