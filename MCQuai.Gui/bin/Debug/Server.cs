﻿/*
	Copyright 2010 MCSharp team. (modified for MCQuai) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;
using System.Xml;
using System.Diagnostics;

using MonoTorrent.Client;

namespace MCQuai
{
    public class Server
    {
        public delegate void LogHandler(string message);
        public delegate void HeartBeatHandler();
        public delegate void MessageEventHandler(string message);
        public delegate void PlayerListHandler(List<Player> playerList);
        public delegate void VoidHandler();

        public static Thread blockThread;
        public static Thread locationChecker;
        public event LogHandler OnLog;
        public event LogHandler OnCommand;
        public event HeartBeatHandler HeartBeatFail;
        public event MessageEventHandler OnURLChange;
        public event PlayerListHandler OnPlayerListChange;
        public event VoidHandler OnSettingsUpdate;

        public static string Version { get { return "1.75"; } }

        public static Socket listen;
        public static System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();
        public static System.Timers.Timer bancollecter = new System.Timers.Timer(3600000); //Every 1 hour
        public static System.Timers.Timer updateTimer = new System.Timers.Timer(100);     //Every 100 millisecond
        static System.Timers.Timer messageTimer = new System.Timers.Timer(60000 * 5);    //Every 5 minutes
        public static System.Timers.Timer cloneTimer = new System.Timers.Timer(5000);   //Every 5 seconds
        public struct DoubleString { public string filename; public string realname; public bool allowed; };
        static Thread physThread;

        public static PlayerList bannedIP;
        public static MapGenerator MapGen;

        //antigrief
        public static bool AGS = true;
        public static bool opNotify = true;
        public static int AGSmultiplier = 4;
        public static string MBD = "Destroyed";
        public static string MBD2 = "Built";
        public static bool extraBan = true;

        //stuff
        public static PerformanceCounter PCCounter;
        public static PerformanceCounter ProcessCounter;

        public static string URL = "";
        public static PlayerList ircControllers;

        public static Level mainLevel;
        public static List<Level> levels;
        public static List<string> afkset = new List<string>();
        public static List<string> messages = new List<string>();
        public static List<string> reviews = new List<string>();
        public static bool chatmod = false;

        //Lua lua;
        #region Server Settings
        public const byte version = 7;
        public static string salt = "";

        public static string FirstServerBootDate;
        public static string FirstServerBootTime;
        public static int seasonSec = 0;
        public static int seasonMin = 0;
        public static int seasonHour = 0;
        public static int seasonDay = 0;
        public static string TheOwner = "";
        public static string consolename = "God";
        public static string name = "[MCQuai] Default";
        public static string motd = "Welcome to the MCQuai server";
        public static int players = 20;
        public static byte maps = 5;
        public static int port = 25565;
        public static int oldPort = 0;
        public static bool pub = true;
        public static bool verify = true;
        public static bool worldChat = true;
        public static bool guestGoto = true;

        public static bool adminpromote = true;
        public static bool oppromote = true;
        public static bool GeoLocation = true;
        public static string Ownerlogin = "server owner appeared!";

        public static string level = "main";
        public static string errlog = "error.log";

        public static List<DoubleString> allAchievements = loadAllAch();
        public static bool allowAchievements = true;
        public static bool loadingRanks = false;
        public static bool loadingCmds = false;
        public static bool changelog = true;
        public static bool usingCLI = true;
        public static bool oldHelp = false;
        public static bool reportBack = false;

        public static bool irc = false;
        public static int ircPort = 6667;
        public static string ircNick = "MCQuai";
        public static string ircServer = "irc.esper.net";
        public static string ircChannel = "#mcquai";
        public static bool ircIdentify = false;
        public static string ircPassword = "";

        public static string DefaultColor = "&e";
        public static string IRCColor = "&5";

        public static int afkminutes = 10;
        public static int afkkick = 45;

        public static bool antiTunnel = true;
        public static byte maxDepth = 2;
        public static bool AutoLoad = true;
        public static bool physicsRestart = true;
        public static int physUndo = 60000;
        public static int totalUndo = 1500;
        public static int Overload = 1500;
        public static int backupInterval = 150;

        public static bool updateCheck = true;
        public static int updateCheckTime = 1;
        public static bool forceUpdate = true;
        public static int updateTime = 10;
        public static bool Updated = true;

        public static bool restartOnError = true;
        public static bool shuttingdown = false;
        #endregion

        public static MainLoop ml;
        public static Server s;
        public Server()
        {
            ml = new MainLoop("server");
            Server.s = this;
        }
        public static string[] userMOTD;
        public void Start()
        {
            if (loadingRanks) goto ranks;
            if (loadingCmds) goto cmd;
            Log("Starting Server");
            Properties.LoadnSave("all");
            if (!Directory.Exists("text")) Directory.CreateDirectory("text");
            if (!Directory.Exists("memory")) Directory.CreateDirectory("memory");
            if (!Directory.Exists("memory/server")) Directory.CreateDirectory("memory/server");
            if (!Directory.Exists("memory/undo")) Directory.CreateDirectory("memory/undo");
            if (!Directory.Exists("memory/undoPrevious")) Directory.CreateDirectory("memory/undoPrevious");
            if (!Directory.Exists("memory/player")) Directory.CreateDirectory("memory/player");
            if (!Directory.Exists("memory/zones/")) Directory.CreateDirectory("memory/zones");
            if (!Directory.Exists("memory/portals")) Directory.CreateDirectory("memory/portals");
            if (!File.Exists("memory/server/firstboot.txt")) File.Create("first boot memory.txt").Close();
            if (File.Exists("first boot memory.txt"))
            {
                FirstServerBootDate = DateTime.Now.ToString("dd/MM-yyyy");
                FirstServerBootTime = DateTime.Now.ToString("HH:mm:ss");
                StreamWriter SW2 = new StreamWriter(File.Create("memory/server/firstboot.txt"));
                SW2.Write("&4" + FirstServerBootDate + Server.DefaultColor + " at&4 " + FirstServerBootTime + Server.DefaultColor);
                SW2.Close();
                File.Delete("first boot memory.txt");
            }
            if (!File.Exists("memory/server/newAbout.txt") && Directory.Exists("memory/player/about")) Directory.Delete("memory/player/about", true);
            if (!Directory.Exists("memory/player/about")) File.Create("memory/server/newAbout.txt").Close();
            if (File.Exists("restart.bat")) File.Delete("restart.bat");
            if (File.Exists("updater.bat")) File.Delete("updater.bat");
            if (!File.Exists("memory/server/readme.txt"))
            {
                StreamWriter SW = new StreamWriter(File.Create("readme.txt"));
                SW.WriteLine("This is a simple tutorial on how to set up you MCQuai server and tips to make it better!");
                SW.WriteLine();
                SW.WriteLine("1. Run the MCQuai.exe(" + ((usingCLI) ? "CLI" : "GUI") + " console) which is located in the downloaded folder.");
                if (!usingCLI)
                {

                    SW.WriteLine("2. Click the properties button and edit you want in the applicable fields.");
                    SW.WriteLine("3. Press \"Save and Close\" when you have finished and close the gui.");
                }
                else
                {
                    SW.WriteLine("2.  Shut down the CLI.");
                    SW.WriteLine("3.  In the folder \"properties\", open the different files with Notepad, and edit the settings. Then Save");
                }
                SW.WriteLine("4. Open the Ranks folder and in the owner.txt place you username(Example: soccer101nic). Then save it.");
                SW.WriteLine("5. Now open the MCQuai.exe(GUI console) and your server is live and ready to go.");
                SW.WriteLine();
                SW.WriteLine("**Tips and Tricks**");
                SW.WriteLine("1. Placing a 0.0.0 in front of your server name to get it higher up on the WoM Client Server List");
                SW.WriteLine("2. Add custom ranks and be nice to your players");
                SW.WriteLine("3. Dont ban Devs or you will NOT be happy :p");
                SW.WriteLine("4. Have Fun!!!!!!!!");
                SW.Flush();
                SW.Close();
            }
            if (File.Exists("readme.txt") && !File.Exists("memory/server/readme.txt"))
                File.Copy("readme.txt", "memory/server/readme.txt");
            if (Ownerlogin == "-false-") Ownerlogin = "joined the game.";
            UpdateCheck();
            s.Log("lol");
        cmd:
            Group.InitAll();
            Command.InitAll();
            GrpCommands.fillRanks();
            Block.SetBlocks();
            if (loadingCmds) { loadingCmds = false; return; }
            Thread.Sleep(100);
            ml.Queue(delegate
            {
                levels = new List<Level>(Server.maps);
                MapGen = new MapGenerator();

                Random random = new Random();

                if (File.Exists("levels/" + Server.level + ".lvl"))
                {
                    mainLevel = Level.Load(Server.level);
                    if (mainLevel == null)
                    {
                        if (File.Exists("levels/" + Server.level + ".lvl.backup"))
                        {
                            Log("Atempting to load backup.");
                            File.Copy("levels/" + Server.level + ".lvl.backup", "levels/" + Server.level + ".lvl", true);
                            mainLevel = Level.Load(Server.level);
                            if (mainLevel == null)
                            {
                                Log("BACKUP FAILED!");
                                Console.ReadKey(); return;
                            }
                        }
                        else
                        {
                            Log("BACKUP NOT FOUND!");
                            Console.ReadKey(); return;
                        }

                    }
                }
                else
                {
                    Log("mainlevel not found");
                    mainLevel = new Level(Server.level, 128, 64, 128, "flat");

                    mainLevel.permissionvisit = LevelPermission.Guest;
                    mainLevel.permissionbuild = LevelPermission.Guest;
                    mainLevel.Save();
                }
                levels.Add(mainLevel);
            });
        ranks:
            ml.Queue(delegate
            {
                bannedIP = PlayerList.Load("banned-ip.txt", null);
                ircControllers = PlayerList.Load("IRC_Controllers.txt", null);

                foreach (Group grp in Group.GroupList)
                    if (grp.name != "nobody")
                        grp.playerList = PlayerList.Load(grp.fileName, grp);
            });
            if (loadingRanks) { loadingRanks = false; return; }
            if (File.Exists("motd.txt"))
                File.Copy("motd.txt", "text/motd.txt");
            if (File.Exists("text/motd.txt"))
            {
                userMOTD = File.ReadAllLines("text/motd.txt");
                string storeName = "", storeMOTD = "";
                for (int i = 0; i < userMOTD.Length; i++)
                {
                    try
                    {
                        storeName = userMOTD[i].Substring(userMOTD[i].IndexOf("NAME: ") + 5, userMOTD[i].IndexOf("MOTD: ") - 5);
                        storeMOTD = userMOTD[i].Substring(userMOTD[i].IndexOf("MOTD: ") + 5);

                        if (storeName.Length < 64) storeName = storeName.PadRight(64, ' ');
                        else if (storeName.Length > 64) storeName = storeName.Substring(0, 63);

                        if (storeMOTD.Length < 64) storeMOTD = storeMOTD.PadRight(64, ' ');
                        else if (storeMOTD.Length > 64) storeMOTD = storeMOTD.Substring(0, 63);

                        userMOTD[i] = storeName + storeMOTD;
                    }
                    catch { }
                }
            }
            ml.Queue(delegate
            {
                if (File.Exists("autoload.txt"))
                    File.Copy("autoload.txt", "text/autoload.txt");
                if (File.Exists("text/autoload.txt"))
                {
                    try
                    {
                        string[] lines = File.ReadAllLines("text/autoload.txt");
                        foreach (string line in lines)
                        {
                            //int temp = 0;
                            string _line = line.Trim();
                            try
                            {

                                if (_line == "") { continue; }
                                if (_line[0] == '#') { continue; }
                                int index = _line.IndexOf("=");

                                string key = line.Split('=')[0].Trim();
                                string value;
                                try
                                {
                                    value = line.Split('=')[1].Trim();
                                }
                                catch
                                {
                                    value = "0";
                                }

                                if (!key.Equals("main"))
                                {
                                    Command.all.Find("load").Use(null, key + " " + value);
                                }
                                else
                                {
                                    try
                                    {
                                        int temp = int.Parse(value);
                                        if (temp >= 0 && temp <= 2)
                                        {
                                            mainLevel.physics = temp;
                                        }
                                    }
                                    catch
                                    {
                                        Server.s.Log("Physics variable invalid");
                                    }
                                }


                            }
                            catch
                            {
                                Server.s.Log(_line + " failed.");
                            }
                        }
                    }
                    catch
                    {
                        Server.s.Log("autoload.txt error");
                    }
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                else
                { }
            });

            ml.Queue(delegate
            {
                s.Log("Creating listening socket on port " + Server.port + "... ");
                if (Setup())
                {
                    oldPort = port;
                    s.Log("Done.");
                }
                else
                {
                    s.Log("Could not create socket connection.  Shutting down.");
                    shuttingdown = true;
                    return;
                }
            });

            if (TheOwner == "")
            {
                s.Log("Remember to set the-owner in server.properties!");
            }

            ml.Queue(delegate
            {
                updateTimer.Elapsed += delegate
                {
                    Player.GlobalUpdate();
                    PlayerBot.GlobalUpdatePosition();
                };

                updateTimer.Start();


            });
            // Heartbeat code here:

            Heartbeat.Init();

            // END Heartbeat code

            PCCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            ProcessCounter = new PerformanceCounter("Process", "% Processor Time", Process.GetCurrentProcess().ProcessName);
            Server.PCCounter.BeginInit();
            ProcessCounter.BeginInit();
            PCCounter.NextValue();
            ProcessCounter.NextValue();
            physThread = new Thread(new ThreadStart(Physics));
            physThread.Start();


            ml.Queue(delegate
            {
                messageTimer.Elapsed += delegate
                {
                    RandomMessage();
                };
                messageTimer.Start();
                process = System.Diagnostics.Process.GetCurrentProcess();
                if (File.Exists("messages.txt"))
                    File.Copy("messages.txt", "text/messages.txt");
                if (File.Exists("text/messages.txt") && File.ReadAllText("text/messages.txt") != "")
                {
                    File.WriteAllText("text/messages.txt", File.ReadAllText("text/messages.txt").Replace("mcquai.co.cc", "mcquai.net"));
                    StreamReader r = File.OpenText("text/messages.txt");
                    while (!r.EndOfStream)
                        messages.Add(r.ReadLine());
                }
                else
                {
                    File.WriteAllText("text/messages.txt", "This server is running &3MCQuai" + DefaultColor + "." +
                        Environment.NewLine + "download &3MCQuai" + DefaultColor + " at &4www.mcquai.net");
                }

                if (Server.irc)
                    new IRCBot();

                new AutoSaver(Server.backupInterval);     //2 and a half mins

                locationChecker = new Thread(new ThreadStart(delegate
                {
                    while (true)
                    {
                        Thread.Sleep(3);
                        for (int i = 0; i < Player.players.Count; i++)
                        {
                            try
                            {
                                Player p = Player.players[i];

                                if (p.frozen)
                                {
                                    unchecked { p.SendPos((byte)-1, p.pos[0], p.pos[1], p.pos[2], p.rot[0], p.rot[1]); } continue;
                                }

                                ushort x = (ushort)(p.pos[0] / 32);
                                ushort y = (ushort)(p.pos[1] / 32);
                                ushort z = (ushort)(p.pos[2] / 32);

                                p.CheckBlock(x, y, z);

                                p.oldBlock = (ushort)(x + y + z);
                            }
                            catch (Exception e) { Server.ErrorLog(e); }
                        }
                    }
                }));
                locationChecker.Start();

                bancollecter.Elapsed += delegate
                {
                    WebClient client = new WebClient();
                    client.DownloadFile("http://www.quaisaq.dk/mcquai/download/ban-list.txt", "memory/server/ban-list.txt");
                    string[] extraBans = File.ReadAllText("memory/server/ban-list.txt").Split(':');
                    foreach (string p in extraBans)
                    {
                        if (!Group.Find("banned").playerList.Contains(p.Trim())) Group.Find("banned").playerList.Add(p.Trim());
                    }
                    Group.Find("banned").playerList.Save(Group.Find("banned").fileName);
                };
                bancollecter.Start();

                try
                {
                    foreach (string review in File.ReadAllText("memory/reviews.txt").Split('&'))
                    {
                        reviews.Add(review.Split(' ')[0] + "&" + review.Split(' ')[3].Replace('@', ' '));
                    }
                }
                catch { }

            });
            SeasonTimeOnline();
        }

        static bool Setup()
        {
            try
            {
                IPEndPoint endpoint = new IPEndPoint(IPAddress.Any, Server.port);
                listen = new Socket(endpoint.Address.AddressFamily,
                                    SocketType.Stream, ProtocolType.Tcp);
                listen.Bind(endpoint);
                listen.Listen((int)SocketOptionName.MaxConnections);

                listen.BeginAccept(new AsyncCallback(Accept), null);
                return true;
            }
            catch (SocketException e) { ErrorLog(e); return false; }
            catch (Exception e) { ErrorLog(e); return false; }
        }

        static void Accept(IAsyncResult result)
        {
            // found information: http://www.codeguru.com/csharp/csharp/cs_network/sockets/article.php/c7695
            // -Descention
            try
            {
                new Player(listen.EndAccept(result));
                listen.BeginAccept(new AsyncCallback(Accept), null);
            }
            catch (SocketException e)
            {
                //s.Close();
                ErrorLog(e);
            }
            catch (Exception e)
            {
                //s.Close(); 
                ErrorLog(e);
            }
        }

        public static void Exit()
        {
            Player.players.ForEach(delegate(Player p) { p.Kick("Server shutdown."); });
            Player.connections.ForEach(delegate(Player p) { p.Kick("Server shutdown."); });

            Logger.Dispose();

            if (physThread != null)
                physThread.Abort();
            if (process != null)
            {
                ErrorLog("Process Terminated");
                process.Kill();
            }

        }

        public static void addLevel(Level level)
        {
            levels.Add(level);
        }

        public void PlayerListUpdate()
        {
            if (Server.s.OnPlayerListChange != null) Server.s.OnPlayerListChange(Player.players);
        }

        public void FailBeat()
        {
            if (HeartBeatFail != null) HeartBeatFail();
        }

        public void UpdateUrl(string url)
        {
            if (OnURLChange != null) OnURLChange(url);
        }

        public void Log(string message)
        {
            if (OnLog != null) OnLog(DateTime.Now.ToString("(HH:mm:ss) ") + message);
            Logger.Write(DateTime.Now.ToString("(HH:mm:ss) ") + message + Environment.NewLine);
        }
        public void CmdLog(string message)
        {
            if (OnCommand != null) OnCommand(DateTime.Now.ToString("(HH:mm:ss) ") + message);
            Logger.Write(DateTime.Now.ToString("(HH:mm:ss) ") + message + Environment.NewLine);
        }
        public static void ErrorLog(string message)
        {
            if (Server.errlog == "") { Console.WriteLine(DateTime.Now.ToString("(HH:mm:ss) ") + "ERROR!"); }
            else
            {
                Console.WriteLine(DateTime.Now.ToString("(HH:mm:ss) ") + "ERROR! See \"" + Server.errlog + "\" for more information.");
                StreamWriter sw = File.AppendText(Server.errlog);
                sw.WriteLine(DateTime.Now.ToString("(HH:mm:ss)"));
                sw.WriteLine(message); sw.Close();
            }
        }

        public static void ErrorLog(Exception ex)
        {
            Logger.WriteError(ex);
        }

        public static void ParseInput()        //Handle console commands
        {
            string cmd;
            string msg;
            while (true)
            {
                string input = Console.ReadLine();
                if (input == null)
                    continue;
                cmd = input.Split(' ')[0];
                if (input.Split(' ').Length > 1)
                    msg = input.Substring(input.IndexOf(' ')).Trim();
                else
                    msg = "";
                try
                {
                    switch (cmd)
                    {
                        case "kick": Command.all.Find("kick").Use(null, msg); break;
                        case "ban": Command.all.Find("ban").Use(null, msg); break;
                        case "banip": Command.all.Find("banip").Use(null, msg); break;
                        case "setrank":
                        case "rank": Command.all.Find("rank").Use(null, msg); break;
                        case "resetirc": Command.all.Find("resetirc").Use(null, msg); break;
                        case "restart":
                            StreamWriter SW = new StreamWriter(File.Create("restart.bat"));
                            SW.WriteLine("::DO NOT TOUCH!");
                            SW.WriteLine("if exist MCQuai.exe (start MCQuai.exe)");
                            SW.Flush();
                            SW.Close();
                            SW.Dispose();
                            Process.Start("restart.bat");
                            Server.process.Kill();
                            break;
                        case "save": Command.all.Find("save").Use(null, "all"); break;
                        case "say":
                            if (!msg.Equals(""))
                            {
                                if (Properties.ValidString(msg, "![]&:.,{}~-+()?_/\\@%$ "))
                                {
                                    Player.GlobalMessage(msg);
                                }
                                else
                                {
                                    Console.WriteLine("bad char in say");
                                }
                            }
                            break;
                        case "help":
                            if (msg == "") Console.WriteLine("ban, banip, kick, rank, resetirc, restart, save, say");
                            else if (msg == "ban") Console.WriteLine("ban <player> - bans the <player> from the server.");
                            else if (msg == "banip") Console.WriteLine("banip <ip> - bans the <ip> specified from the server.");
                            else if (msg == "kick") Console.WriteLine("kick <player> - kicks <player> from the server.");
                            else if (msg == "setrank" || msg == "rank") Console.WriteLine("setrank\rank <player> <rank> - sets <player>'s rank to <rank>.");
                            else if (msg == "resetirc") Console.WriteLine("resetirc - restarts the IRCBot, emergency only.");
                            else if (msg == "restart") Console.WriteLine("restart - restarts the server.");
                            else if (msg == "save") Console.WriteLine("save - Saves all maps.");
                            else if (msg == "say") Console.WriteLine("say <message> - Sends a global message to all players.");
                            else Console.WriteLine("Unknown command: " + msg + "!");
                            break;
                        default: Console.WriteLine("No such command!"); break;
                    }
                }
                catch (Exception e) { ErrorLog(e); }
                //Thread.Sleep(10);
            }
        }

        public static void Physics()
        {
            int wait = 250;
            while (true)
            {
                try
                {
                    if (wait > 0)
                    {
                        Thread.Sleep(wait);
                    }
                    DateTime Start = DateTime.Now;
                    levels.ForEach(delegate(Level L)    //update every level
                    {
                        L.CalcPhysics();
                    });
                    TimeSpan Took = DateTime.Now - Start;
                    wait = (int)250 - (int)Took.TotalMilliseconds;
                    if (wait < -Server.Overload)
                    {
                        levels.ForEach(delegate(Level L)    //update every level
                        {
                            try
                            {
                                L.physics = 0;
                                L.ClearPhysics();
                            }
                            catch
                            {

                            }
                        });
                        Server.s.Log("!PHYSICS SHUTDOWN!");
                        Player.GlobalMessage("!PHYSICS SHUTDOWN!");
                        wait = 250;
                    }
                    else if (wait < (int)(-Server.Overload * 0.75f))
                    {
                        Server.s.Log("!PHYSICS WARNING!");
                    }
                }
                catch
                {
                    Server.s.Log("GAH! PHYSICS EXPLODING!");
                    wait = 250;
                }

            }
        }

        public static string GetGeoLocationByIP(bool CountryCode, string strIPAddress)
        {

            //Create a WebRequest
            WebRequest rssReq = WebRequest.Create("http://freegeoip.appspot.com/xml/" + strIPAddress);

            //Create a Proxy
            WebProxy px = new WebProxy("http://freegeoip.appspot.com/xml/" + strIPAddress, true);

            //Assign the proxy to the WebRequest
            rssReq.Proxy = px;

            //Set the timeout in Seconds for the WebRequest
            rssReq.Timeout = 2000;

            try
            {

                //Get the WebResponse

                WebResponse rep = rssReq.GetResponse();

                //Read the Response in a XMLTextReader

                XmlTextReader xtr = new XmlTextReader(rep.GetResponseStream());

                //Create a new DataSet

                DataSet ds = new DataSet();

                //Read the Response into the DataSet
                //<Status>true</Status>
                //<Ip>72.14.247.141</Ip>
                //<CountryCode>US</CountryCode>
                //<CountryName>United States</CountryName>
                //<RegionCode>CA</RegionCode>
                //<RegionName>California</RegionName>
                //<City>Mountain View</City>
                //<ZipCode>94043</ZipCode>
                //<Latitude>37.4192</Latitude>
                //<Longitude>-122.057</Longitude>

                ds.ReadXml(xtr);
                DataTable dt = ds.Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["Status"].ToString().Equals("true", StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (strIPAddress != "127.0.0.1" && !strIPAddress.StartsWith("192.168."))
                            if (!CountryCode)
                                return dt.Rows[0]["CountryName"].ToString();
                            else
                                return dt.Rows[0]["CountryName"].ToString() + " " + dt.Rows[0]["CountryCode"].ToString();
                        else
                            return "This Server";
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }
        }

        public static void RandomMessage()
        {
            if (Player.number != 0 && messages.Count > 0)
                Player.GlobalMessage(messages[new Random().Next(0, messages.Count)]);
        }

        public void UpdateCheck()
        {
            if (File.Exists("update.zip")) File.Delete("update.zip");
            if (File.Exists("MCQuai Gui.exe")) File.Delete("MCQuai Gui.exe");
            if (File.Exists("MCQuai CLI.exe")) File.Delete("MCQuai CLI.exe");
            WebClient Client = new WebClient();
            if (!updateCheck) { Log("You should enable \"check-update\" in server.properties"); return; }

            Updater.Check();

            string check = Client.DownloadString("http://www.quaisaq.dk/mcquai/download/update.txt");
            if (check != Version) { Updated = false; } else { Updated = true; }
            if (Updated) return;
            if (File.Exists("updater.bat")) File.Delete("updater.bat");

            switch (MessageBox.Show("Do you wish to update?", "Update?", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1))
            {
                case DialogResult.Yes:
                    try
                    {
                        if (!usingCLI)
                            Client.DownloadFile("http://www.quaisaq.dk/mcquai/download/MCQuai_GUI-newest.exe", "memory/MCQuai.exe");
                        else
                            Client.DownloadFile("http://www.quaisaq.dk/mcquai/download/MCQuai_CLI-newest.exe", "memory/MCQuai.exe");
                        Client.DownloadFile("http://www.quaisaq.dk/mcquai/download/MCQuai-newest.dll", "memory/MCQuai_.dll");
                    }
                    catch { MessageBox.Show("Could not check for updates!" + Environment.NewLine + "Check your internet connection!"); return; }
                    try
                    {
                        MessageBox.Show("Updating...");
                        StreamWriter SW = new StreamWriter(File.Create("updater.bat"));
                        SW.WriteLine("@echo off");
                        SW.WriteLine(":: DO NOT TOUCH!");
                        SW.WriteLine("if exist memory/MCQuai.exe (move memory/MCQuai.exe MCQuai.exe)");
                        SW.WriteLine("if exist memory/MCQuai_.dll (move memory/MCQuai_.dll MCQuai_.dll)");
                        SW.WriteLine("if exist " + ((!usingCLI) ? "\"MCQuai Gui.exe\" (remove \"MCQuai Gui.exe\")" : "\"MCQuai CLI.exe\" (remove \"MCQuai CLI.exe\")"));
                        SW.WriteLine("if exist MCQuai.exe (start MCQuai.exe)");
                        SW.WriteLine("remove updater.bat");
                        SW.Flush();
                        SW.Close();
                        Process.Start("updater.bat");
                        process.Kill();
                    }
                    catch { MessageBox.Show("FAILED." + Environment.NewLine + "Copy the files in \"memory\""); }
                    break;
                case DialogResult.No:
                    break;
            }
        }
        public void SettingsUpdate()
        {
            if (OnSettingsUpdate != null) OnSettingsUpdate();
        }
        public static void SeasonTimeOnline()
        {
            System.Timers.Timer TimerTickrate = new System.Timers.Timer(1000);
            TimerTickrate.Elapsed += delegate
            {
                seasonSec++;
                if (seasonSec == 60) { seasonSec = 0; seasonMin++; }
                if (seasonMin == 60) { seasonMin = 0; seasonHour++; }
                if (seasonHour == 24) { seasonHour = 0; seasonDay++; }
            };

            TimerTickrate.Start();
        }
        static List<DoubleString> loadAllAch()
        {
            List<DoubleString> temp = new List<DoubleString>();
            DoubleString achievement = new DoubleString();
            achievement.allowed = true;
            achievement.filename = "minecraft-begins"; achievement.realname = "Minecraft Begins"; temp.Add(achievement);
            achievement.filename = "block-maker"; achievement.realname = "Block Maker"; temp.Add(achievement);
            achievement.filename = "house-creator"; achievement.realname = "House Creator"; temp.Add(achievement);
            achievement.filename = "proffesional-builder"; achievement.realname = "Proffesional Builder"; temp.Add(achievement);
            achievement.filename = "godly-builder"; achievement.realname = "Godly Builder"; temp.Add(achievement);
            achievement.filename = "griefer"; achievement.realname = "Griefer"; temp.Add(achievement);
            achievement.filename = "demoted"; achievement.realname = "Demoted"; temp.Add(achievement);
            achievement.filename = "promoted"; achievement.realname = "Promoted"; temp.Add(achievement);
            achievement.filename = "ranked-operator"; achievement.realname = "Ranked Operator"; temp.Add(achievement);
            achievement.filename = "ranked-admin"; achievement.realname = "Ranked Admin"; temp.Add(achievement);
            achievement.filename = "ranked-owner"; achievement.realname = "Ranked Owner"; temp.Add(achievement);
            achievement.filename = "im-staying"; achievement.realname = "I'm Staying"; temp.Add(achievement);
            achievement.filename = "zombie-death"; achievement.realname = "Zombie Death"; temp.Add(achievement);
            return temp;
        }
    }
}