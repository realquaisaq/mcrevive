/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
namespace MCRevive.Gui
{
    partial class GuiWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GuiWindow));
            this.menuPlayers = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.rankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.demoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kickToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.banStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.banToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iPBanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.banAndIPBanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoTimeStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.oKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendToMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLevels = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.unloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visitPermissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buildPermissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadMapStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.oKToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabExtra = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pnlPlayers = new System.Windows.Forms.Panel();
            this.lblPlayerScale3 = new System.Windows.Forms.Label();
            this.lblPlayerScale2 = new System.Windows.Forms.Label();
            this.lblPlayerScale1 = new System.Windows.Forms.Label();
            this.lblPlayerScale0 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblTrafficScale0 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlTraffic = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTrafficScale1 = new System.Windows.Forms.Label();
            this.lblTrafficScale2 = new System.Windows.Forms.Label();
            this.tabLogs = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnRefreshErrorLog = new MCRevive.Gui.CButton();
            this.btnOpenErrorLog = new MCRevive.Gui.CButton();
            this.txtErrorLog = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dtpError = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRefreshServerLog = new MCRevive.Gui.CButton();
            this.btnOpenServerLog = new MCRevive.Gui.CButton();
            this.txtServerLog = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtpLog = new System.Windows.Forms.DateTimePicker();
            this.tabChangelog = new System.Windows.Forms.TabPage();
            this.grpChangelogNew = new System.Windows.Forms.GroupBox();
            this.rtxtChangelog2 = new System.Windows.Forms.RichTextBox();
            this.grpChangelog = new System.Windows.Forms.GroupBox();
            this.rtxtChangelog1 = new System.Windows.Forms.RichTextBox();
            this.tabMaps = new System.Windows.Forms.TabPage();
            this.mdpMap = new MCRevive.Gui.MapDrawPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.newMapLiType = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.newMapTxtName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.newMapLiSize = new System.Windows.Forms.ListBox();
            this.newMapBtnCreate = new MCRevive.Gui.CButton();
            this.btnMapCenter = new MCRevive.Gui.CButton();
            this.liMaps = new System.Windows.Forms.ListBox();
            this.trkMapZoom = new System.Windows.Forms.TrackBar();
            this.prgMapDraw = new System.Windows.Forms.ProgressBar();
            this.tabMain = new System.Windows.Forms.TabPage();
            this.txtCommandInput = new System.Windows.Forms.TextBox();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.liClients = new System.Windows.Forms.ListBox();
            this.lblCommand = new System.Windows.Forms.Label();
            this.grpboxCmdLog = new System.Windows.Forms.GroupBox();
            this.txtCmdLog = new System.Windows.Forms.TextBox();
            this.lblLevels = new System.Windows.Forms.Label();
            this.lblURL = new System.Windows.Forms.Label();
            this.liLevels = new System.Windows.Forms.ListBox();
            this.lblChat = new System.Windows.Forms.Label();
            this.lblPlayers = new System.Windows.Forms.Label();
            this.grpboxLog = new System.Windows.Forms.GroupBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.btnProperties = new MCRevive.Gui.CButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tapPlayer = new System.Windows.Forms.TabPage();
            this.btnUnbanIP = new MCRevive.Gui.CButton();
            this.btnUnban = new MCRevive.Gui.CButton();
            this.btnChangeRank = new MCRevive.Gui.CButton();
            this.btnKick = new MCRevive.Gui.CButton();
            this.btnBan = new MCRevive.Gui.CButton();
            this.btnBanIp = new MCRevive.Gui.CButton();
            this.btnKickban = new MCRevive.Gui.CButton();
            this.tapServer = new System.Windows.Forms.TabPage();
            this.btnRestart = new MCRevive.Gui.CButton();
            this.btnReloadMJS = new MCRevive.Gui.CButton();
            this.btnReloadCmds = new MCRevive.Gui.CButton();
            this.btnShutdown = new MCRevive.Gui.CButton();
            this.btnReloadRanks = new MCRevive.Gui.CButton();
            this.btnReloadProperties = new MCRevive.Gui.CButton();
            this.tabOther = new System.Windows.Forms.TabPage();
            this.btnWOM = new MCRevive.Gui.CButton();
            this.btnMJSLog = new MCRevive.Gui.CButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtConsoleName = new System.Windows.Forms.TextBox();
            this.btnRules = new MCRevive.Gui.CButton();
            this.btnFolder = new MCRevive.Gui.CButton();
            this.tabAll = new System.Windows.Forms.TabControl();
            this.tabServerlist = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.dgvServers = new System.Windows.Forms.DataGridView();
            this.clmName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmPlayers = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOwner = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabRoutines = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.btnRoutineDelete = new MCRevive.Gui.CButton();
            this.btnRoutineActionDelete = new MCRevive.Gui.CButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.txtRoutineActionAddInfo = new System.Windows.Forms.TextBox();
            this.btnRoutineActionAdd = new MCRevive.Gui.CButton();
            this.liRoutinesAction = new System.Windows.Forms.ListBox();
            this.liRoutineActionList = new System.Windows.Forms.ListBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.liRoutineActions = new System.Windows.Forms.ListBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtRoutineLog = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button5 = new MCRevive.Gui.CButton();
            this.liRoutines = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtRoutine = new System.Windows.Forms.TextBox();
            this.btnRoutineAdd = new MCRevive.Gui.CButton();
            this.liRoutineExec = new System.Windows.Forms.ListBox();
            this.numRoutineExec = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.menuPlayers.SuspendLayout();
            this.menuLevels.SuspendLayout();
            this.tabExtra.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabLogs.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabChangelog.SuspendLayout();
            this.grpChangelogNew.SuspendLayout();
            this.grpChangelog.SuspendLayout();
            this.tabMaps.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkMapZoom)).BeginInit();
            this.tabMain.SuspendLayout();
            this.grpboxCmdLog.SuspendLayout();
            this.grpboxLog.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tapPlayer.SuspendLayout();
            this.tapServer.SuspendLayout();
            this.tabOther.SuspendLayout();
            this.tabAll.SuspendLayout();
            this.tabServerlist.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServers)).BeginInit();
            this.tabRoutines.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRoutineExec)).BeginInit();
            this.SuspendLayout();
            // 
            // menuPlayers
            // 
            this.menuPlayers.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rankToolStripMenuItem,
            this.kickToolStripMenuItem,
            this.banStripMenuItem1,
            this.undoToolStripMenuItem,
            this.sendToMapToolStripMenuItem});
            this.menuPlayers.Name = "contextMenuStrip1";
            this.menuPlayers.Size = new System.Drawing.Size(142, 114);
            this.menuPlayers.Opening += new System.ComponentModel.CancelEventHandler(this.menuPlayers_Opening);
            // 
            // rankToolStripMenuItem
            // 
            this.rankToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.promoteToolStripMenuItem,
            this.demoteToolStripMenuItem,
            this.otherToolStripMenuItem});
            this.rankToolStripMenuItem.Name = "rankToolStripMenuItem";
            this.rankToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.rankToolStripMenuItem.Text = "Rank";
            // 
            // promoteToolStripMenuItem
            // 
            this.promoteToolStripMenuItem.Name = "promoteToolStripMenuItem";
            this.promoteToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.promoteToolStripMenuItem.Text = "Promote";
            this.promoteToolStripMenuItem.Click += new System.EventHandler(this.promoteToolStripMenuItem_Click);
            // 
            // demoteToolStripMenuItem
            // 
            this.demoteToolStripMenuItem.Name = "demoteToolStripMenuItem";
            this.demoteToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.demoteToolStripMenuItem.Text = "Demote";
            this.demoteToolStripMenuItem.Click += new System.EventHandler(this.demoteToolStripMenuItem_Click);
            // 
            // otherToolStripMenuItem
            // 
            this.otherToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lolToolStripMenuItem});
            this.otherToolStripMenuItem.Name = "otherToolStripMenuItem";
            this.otherToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.otherToolStripMenuItem.Text = "Other";
            // 
            // lolToolStripMenuItem
            // 
            this.lolToolStripMenuItem.Name = "lolToolStripMenuItem";
            this.lolToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.lolToolStripMenuItem.Text = "needs to be here";
            // 
            // kickToolStripMenuItem
            // 
            this.kickToolStripMenuItem.Name = "kickToolStripMenuItem";
            this.kickToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.kickToolStripMenuItem.Text = "Kick";
            this.kickToolStripMenuItem.Click += new System.EventHandler(this.kickToolStripMenuItem_Click);
            // 
            // banStripMenuItem1
            // 
            this.banStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.banToolStripMenuItem,
            this.iPBanToolStripMenuItem,
            this.banAndIPBanToolStripMenuItem});
            this.banStripMenuItem1.Name = "banStripMenuItem1";
            this.banStripMenuItem1.Size = new System.Drawing.Size(141, 22);
            this.banStripMenuItem1.Text = "Ban";
            // 
            // banToolStripMenuItem
            // 
            this.banToolStripMenuItem.Name = "banToolStripMenuItem";
            this.banToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.banToolStripMenuItem.Text = "Ban";
            this.banToolStripMenuItem.Click += new System.EventHandler(this.banToolStripMenuItem_Click);
            // 
            // iPBanToolStripMenuItem
            // 
            this.iPBanToolStripMenuItem.Name = "iPBanToolStripMenuItem";
            this.iPBanToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.iPBanToolStripMenuItem.Text = "IP Ban";
            this.iPBanToolStripMenuItem.Click += new System.EventHandler(this.iPBanToolStripMenuItem_Click);
            // 
            // banAndIPBanToolStripMenuItem
            // 
            this.banAndIPBanToolStripMenuItem.Name = "banAndIPBanToolStripMenuItem";
            this.banAndIPBanToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.banAndIPBanToolStripMenuItem.Text = "Ban and IP Ban";
            this.banAndIPBanToolStripMenuItem.Click += new System.EventHandler(this.banAndIPBanToolStripMenuItem_Click);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoTimeStripTextBox1,
            this.toolStripSeparator3,
            this.oKToolStripMenuItem});
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            // 
            // undoTimeStripTextBox1
            // 
            this.undoTimeStripTextBox1.MaxLength = 9;
            this.undoTimeStripTextBox1.Name = "undoTimeStripTextBox1";
            this.undoTimeStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.undoTimeStripTextBox1.Text = "30";
            this.undoTimeStripTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.undoTimeStripTextBox1_KeyDown);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(157, 6);
            // 
            // oKToolStripMenuItem
            // 
            this.oKToolStripMenuItem.Name = "oKToolStripMenuItem";
            this.oKToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.oKToolStripMenuItem.Text = "OK";
            this.oKToolStripMenuItem.Click += new System.EventHandler(this.oKToolStripMenuItem_Click);
            // 
            // sendToMapToolStripMenuItem
            // 
            this.sendToMapToolStripMenuItem.Name = "sendToMapToolStripMenuItem";
            this.sendToMapToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.sendToMapToolStripMenuItem.Text = "Send to map";
            // 
            // menuLevels
            // 
            this.menuLevels.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unloadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.visitPermissionToolStripMenuItem,
            this.buildPermissionToolStripMenuItem,
            this.toolStripSeparator1,
            this.loadToolStripMenuItem});
            this.menuLevels.Name = "menuLevels";
            this.menuLevels.Size = new System.Drawing.Size(163, 120);
            this.menuLevels.Opening += new System.ComponentModel.CancelEventHandler(this.menuLevels_Opening);
            // 
            // unloadToolStripMenuItem
            // 
            this.unloadToolStripMenuItem.Name = "unloadToolStripMenuItem";
            this.unloadToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.unloadToolStripMenuItem.Text = "Unload";
            this.unloadToolStripMenuItem.Click += new System.EventHandler(this.unloadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // visitPermissionToolStripMenuItem
            // 
            this.visitPermissionToolStripMenuItem.Name = "visitPermissionToolStripMenuItem";
            this.visitPermissionToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.visitPermissionToolStripMenuItem.Text = "Visit permission";
            // 
            // buildPermissionToolStripMenuItem
            // 
            this.buildPermissionToolStripMenuItem.Name = "buildPermissionToolStripMenuItem";
            this.buildPermissionToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.buildPermissionToolStripMenuItem.Text = "Build permission";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(159, 6);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.loadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadMapStripTextBox,
            this.toolStripSeparator2,
            this.oKToolStripMenuItem1});
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.loadToolStripMenuItem.Text = "Load";
            // 
            // loadMapStripTextBox
            // 
            this.loadMapStripTextBox.Name = "loadMapStripTextBox";
            this.loadMapStripTextBox.Size = new System.Drawing.Size(100, 23);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(157, 6);
            // 
            // oKToolStripMenuItem1
            // 
            this.oKToolStripMenuItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.oKToolStripMenuItem1.Name = "oKToolStripMenuItem1";
            this.oKToolStripMenuItem1.Size = new System.Drawing.Size(160, 22);
            this.oKToolStripMenuItem1.Text = "OK";
            this.oKToolStripMenuItem1.Click += new System.EventHandler(this.oKToolStripMenuItem1_Click);
            // 
            // tabExtra
            // 
            this.tabExtra.Controls.Add(this.groupBox6);
            this.tabExtra.Controls.Add(this.groupBox5);
            this.tabExtra.Location = new System.Drawing.Point(4, 22);
            this.tabExtra.Name = "tabExtra";
            this.tabExtra.Padding = new System.Windows.Forms.Padding(3);
            this.tabExtra.Size = new System.Drawing.Size(676, 540);
            this.tabExtra.TabIndex = 6;
            this.tabExtra.Text = "Extra";
            this.tabExtra.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.pnlPlayers);
            this.groupBox6.Controls.Add(this.lblPlayerScale3);
            this.groupBox6.Controls.Add(this.lblPlayerScale2);
            this.groupBox6.Controls.Add(this.lblPlayerScale1);
            this.groupBox6.Controls.Add(this.lblPlayerScale0);
            this.groupBox6.Location = new System.Drawing.Point(6, 298);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(664, 148);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Players on your server";
            // 
            // pnlPlayers
            // 
            this.pnlPlayers.Location = new System.Drawing.Point(9, 19);
            this.pnlPlayers.Name = "pnlPlayers";
            this.pnlPlayers.Size = new System.Drawing.Size(617, 120);
            this.pnlPlayers.TabIndex = 6;
            // 
            // lblPlayerScale3
            // 
            this.lblPlayerScale3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPlayerScale3.AutoSize = true;
            this.lblPlayerScale3.Location = new System.Drawing.Point(639, 19);
            this.lblPlayerScale3.Name = "lblPlayerScale3";
            this.lblPlayerScale3.Size = new System.Drawing.Size(19, 13);
            this.lblPlayerScale3.TabIndex = 9;
            this.lblPlayerScale3.Text = "15";
            // 
            // lblPlayerScale2
            // 
            this.lblPlayerScale2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPlayerScale2.AutoSize = true;
            this.lblPlayerScale2.Location = new System.Drawing.Point(639, 54);
            this.lblPlayerScale2.Name = "lblPlayerScale2";
            this.lblPlayerScale2.Size = new System.Drawing.Size(19, 13);
            this.lblPlayerScale2.TabIndex = 7;
            this.lblPlayerScale2.Text = "10";
            // 
            // lblPlayerScale1
            // 
            this.lblPlayerScale1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPlayerScale1.AutoSize = true;
            this.lblPlayerScale1.Location = new System.Drawing.Point(645, 92);
            this.lblPlayerScale1.Name = "lblPlayerScale1";
            this.lblPlayerScale1.Size = new System.Drawing.Size(13, 13);
            this.lblPlayerScale1.TabIndex = 8;
            this.lblPlayerScale1.Text = "5";
            // 
            // lblPlayerScale0
            // 
            this.lblPlayerScale0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPlayerScale0.AutoSize = true;
            this.lblPlayerScale0.Location = new System.Drawing.Point(645, 126);
            this.lblPlayerScale0.Name = "lblPlayerScale0";
            this.lblPlayerScale0.Size = new System.Drawing.Size(13, 13);
            this.lblPlayerScale0.TabIndex = 6;
            this.lblPlayerScale0.Text = "0";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblTrafficScale0);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.pnlTraffic);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.lblTrafficScale1);
            this.groupBox5.Controls.Add(this.lblTrafficScale2);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(664, 266);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "MCRevive network traffic";
            // 
            // lblTrafficScale0
            // 
            this.lblTrafficScale0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTrafficScale0.AutoSize = true;
            this.lblTrafficScale0.Location = new System.Drawing.Point(642, 234);
            this.lblTrafficScale0.Name = "lblTrafficScale0";
            this.lblTrafficScale0.Size = new System.Drawing.Size(19, 13);
            this.lblTrafficScale0.TabIndex = 11;
            this.lblTrafficScale0.Text = "0k";
            this.lblTrafficScale0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Lime;
            this.label6.Location = new System.Drawing.Point(576, 250);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Outgoing";
            // 
            // pnlTraffic
            // 
            this.pnlTraffic.Location = new System.Drawing.Point(6, 19);
            this.pnlTraffic.Name = "pnlTraffic";
            this.pnlTraffic.Size = new System.Drawing.Size(620, 228);
            this.pnlTraffic.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.GreenYellow;
            this.label7.Location = new System.Drawing.Point(696, 250);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Outgoing";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(6, 250);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Incoming";
            // 
            // lblTrafficScale1
            // 
            this.lblTrafficScale1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTrafficScale1.AutoSize = true;
            this.lblTrafficScale1.Location = new System.Drawing.Point(636, 126);
            this.lblTrafficScale1.Name = "lblTrafficScale1";
            this.lblTrafficScale1.Size = new System.Drawing.Size(25, 13);
            this.lblTrafficScale1.TabIndex = 12;
            this.lblTrafficScale1.Text = "10k";
            this.lblTrafficScale1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTrafficScale2
            // 
            this.lblTrafficScale2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTrafficScale2.AutoSize = true;
            this.lblTrafficScale2.Location = new System.Drawing.Point(636, 19);
            this.lblTrafficScale2.Name = "lblTrafficScale2";
            this.lblTrafficScale2.Size = new System.Drawing.Size(25, 13);
            this.lblTrafficScale2.TabIndex = 10;
            this.lblTrafficScale2.Text = "20k";
            this.lblTrafficScale2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabLogs
            // 
            this.tabLogs.Controls.Add(this.groupBox3);
            this.tabLogs.Controls.Add(this.groupBox2);
            this.tabLogs.Location = new System.Drawing.Point(4, 22);
            this.tabLogs.Name = "tabLogs";
            this.tabLogs.Size = new System.Drawing.Size(676, 540);
            this.tabLogs.TabIndex = 4;
            this.tabLogs.Text = "System Logs";
            this.tabLogs.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox3.Controls.Add(this.btnRefreshErrorLog);
            this.groupBox3.Controls.Add(this.btnOpenErrorLog);
            this.groupBox3.Controls.Add(this.txtErrorLog);
            this.groupBox3.Controls.Add(this.panel3);
            this.groupBox3.Location = new System.Drawing.Point(390, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(283, 534);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Error logs";
            // 
            // btnRefreshErrorLog
            // 
            this.btnRefreshErrorLog.Location = new System.Drawing.Point(6, 505);
            this.btnRefreshErrorLog.Name = "btnRefreshErrorLog";
            this.btnRefreshErrorLog.Size = new System.Drawing.Size(133, 23);
            this.btnRefreshErrorLog.TabIndex = 5;
            this.btnRefreshErrorLog.Text = "Refresh";
            this.btnRefreshErrorLog.UseVisualStyleBackColor = true;
            this.btnRefreshErrorLog.Click += new System.EventHandler(this.btnRefreshErrorLog_Click);
            // 
            // btnOpenErrorLog
            // 
            this.btnOpenErrorLog.Location = new System.Drawing.Point(144, 505);
            this.btnOpenErrorLog.Name = "btnOpenErrorLog";
            this.btnOpenErrorLog.Size = new System.Drawing.Size(133, 23);
            this.btnOpenErrorLog.TabIndex = 4;
            this.btnOpenErrorLog.Text = "Open Log";
            this.btnOpenErrorLog.UseVisualStyleBackColor = true;
            this.btnOpenErrorLog.Click += new System.EventHandler(this.btnOpenErrorLog_Click);
            // 
            // txtErrorLog
            // 
            this.txtErrorLog.BackColor = System.Drawing.Color.White;
            this.txtErrorLog.Location = new System.Drawing.Point(6, 78);
            this.txtErrorLog.Multiline = true;
            this.txtErrorLog.Name = "txtErrorLog";
            this.txtErrorLog.ReadOnly = true;
            this.txtErrorLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtErrorLog.Size = new System.Drawing.Size(271, 421);
            this.txtErrorLog.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gainsboro;
            this.panel3.Controls.Add(this.dtpError);
            this.panel3.Location = new System.Drawing.Point(6, 19);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(271, 53);
            this.panel3.TabIndex = 1;
            // 
            // dtpError
            // 
            this.dtpError.Location = new System.Drawing.Point(33, 17);
            this.dtpError.Name = "dtpError";
            this.dtpError.Size = new System.Drawing.Size(200, 20);
            this.dtpError.TabIndex = 0;
            this.dtpError.ValueChanged += new System.EventHandler(this.dtpError_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox2.Controls.Add(this.btnRefreshServerLog);
            this.groupBox2.Controls.Add(this.btnOpenServerLog);
            this.groupBox2.Controls.Add(this.txtServerLog);
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Location = new System.Drawing.Point(4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(380, 534);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Server Logs";
            // 
            // btnRefreshServerLog
            // 
            this.btnRefreshServerLog.Location = new System.Drawing.Point(6, 505);
            this.btnRefreshServerLog.Name = "btnRefreshServerLog";
            this.btnRefreshServerLog.Size = new System.Drawing.Size(181, 23);
            this.btnRefreshServerLog.TabIndex = 6;
            this.btnRefreshServerLog.Text = "Refresh";
            this.btnRefreshServerLog.UseVisualStyleBackColor = true;
            this.btnRefreshServerLog.Click += new System.EventHandler(this.btnRefreshServerLog_Click);
            // 
            // btnOpenServerLog
            // 
            this.btnOpenServerLog.Location = new System.Drawing.Point(193, 505);
            this.btnOpenServerLog.Name = "btnOpenServerLog";
            this.btnOpenServerLog.Size = new System.Drawing.Size(181, 23);
            this.btnOpenServerLog.TabIndex = 5;
            this.btnOpenServerLog.Text = "Open Log";
            this.btnOpenServerLog.UseVisualStyleBackColor = true;
            this.btnOpenServerLog.Click += new System.EventHandler(this.btnOpenServerLog_Click);
            // 
            // txtServerLog
            // 
            this.txtServerLog.BackColor = System.Drawing.Color.White;
            this.txtServerLog.Location = new System.Drawing.Point(6, 79);
            this.txtServerLog.Multiline = true;
            this.txtServerLog.Name = "txtServerLog";
            this.txtServerLog.ReadOnly = true;
            this.txtServerLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtServerLog.Size = new System.Drawing.Size(368, 420);
            this.txtServerLog.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.dtpLog);
            this.panel2.Location = new System.Drawing.Point(6, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(368, 53);
            this.panel2.TabIndex = 1;
            // 
            // dtpLog
            // 
            this.dtpLog.Location = new System.Drawing.Point(84, 17);
            this.dtpLog.Name = "dtpLog";
            this.dtpLog.Size = new System.Drawing.Size(200, 20);
            this.dtpLog.TabIndex = 0;
            this.dtpLog.ValueChanged += new System.EventHandler(this.dtpLog_ValueChanged);
            // 
            // tabChangelog
            // 
            this.tabChangelog.Controls.Add(this.grpChangelogNew);
            this.tabChangelog.Controls.Add(this.grpChangelog);
            this.tabChangelog.Location = new System.Drawing.Point(4, 22);
            this.tabChangelog.Name = "tabChangelog";
            this.tabChangelog.Padding = new System.Windows.Forms.Padding(3);
            this.tabChangelog.Size = new System.Drawing.Size(676, 540);
            this.tabChangelog.TabIndex = 1;
            this.tabChangelog.Text = "Changelog";
            this.tabChangelog.UseVisualStyleBackColor = true;
            // 
            // grpChangelogNew
            // 
            this.grpChangelogNew.Controls.Add(this.rtxtChangelog2);
            this.grpChangelogNew.Location = new System.Drawing.Point(6, 273);
            this.grpChangelogNew.Name = "grpChangelogNew";
            this.grpChangelogNew.Size = new System.Drawing.Size(664, 261);
            this.grpChangelogNew.TabIndex = 3;
            this.grpChangelogNew.TabStop = false;
            this.grpChangelogNew.Text = "Changes since";
            // 
            // rtxtChangelog2
            // 
            this.rtxtChangelog2.BackColor = System.Drawing.SystemColors.Window;
            this.rtxtChangelog2.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtChangelog2.Location = new System.Drawing.Point(6, 19);
            this.rtxtChangelog2.Name = "rtxtChangelog2";
            this.rtxtChangelog2.ReadOnly = true;
            this.rtxtChangelog2.Size = new System.Drawing.Size(652, 236);
            this.rtxtChangelog2.TabIndex = 1;
            this.rtxtChangelog2.Text = "";
            this.rtxtChangelog2.WordWrap = false;
            // 
            // grpChangelog
            // 
            this.grpChangelog.Controls.Add(this.rtxtChangelog1);
            this.grpChangelog.Location = new System.Drawing.Point(6, 6);
            this.grpChangelog.Name = "grpChangelog";
            this.grpChangelog.Size = new System.Drawing.Size(664, 261);
            this.grpChangelog.TabIndex = 2;
            this.grpChangelog.TabStop = false;
            this.grpChangelog.Text = "Changelog";
            // 
            // rtxtChangelog1
            // 
            this.rtxtChangelog1.BackColor = System.Drawing.SystemColors.Window;
            this.rtxtChangelog1.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtChangelog1.Location = new System.Drawing.Point(6, 19);
            this.rtxtChangelog1.Name = "rtxtChangelog1";
            this.rtxtChangelog1.ReadOnly = true;
            this.rtxtChangelog1.Size = new System.Drawing.Size(652, 236);
            this.rtxtChangelog1.TabIndex = 0;
            this.rtxtChangelog1.Text = "";
            this.rtxtChangelog1.WordWrap = false;
            // 
            // tabMaps
            // 
            this.tabMaps.Controls.Add(this.mdpMap);
            this.tabMaps.Controls.Add(this.groupBox1);
            this.tabMaps.Controls.Add(this.btnMapCenter);
            this.tabMaps.Controls.Add(this.liMaps);
            this.tabMaps.Controls.Add(this.trkMapZoom);
            this.tabMaps.Controls.Add(this.prgMapDraw);
            this.tabMaps.Location = new System.Drawing.Point(4, 22);
            this.tabMaps.Name = "tabMaps";
            this.tabMaps.Padding = new System.Windows.Forms.Padding(3);
            this.tabMaps.Size = new System.Drawing.Size(676, 540);
            this.tabMaps.TabIndex = 2;
            this.tabMaps.Text = "Maps";
            this.tabMaps.UseVisualStyleBackColor = true;
            // 
            // mdpMap
            // 
            this.mdpMap.BackColor = System.Drawing.Color.CornflowerBlue;
            this.mdpMap.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.mdpMap.Location = new System.Drawing.Point(210, 6);
            this.mdpMap.Name = "mdpMap";
            this.mdpMap.Size = new System.Drawing.Size(460, 336);
            this.mdpMap.TabIndex = 12;
            this.mdpMap.Progress_Changed += new MCRevive.Gui.MapDrawPanel.ProgressEventHandler(this.mdpMap_Progress_Changed);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.newMapLiType);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.newMapTxtName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.newMapLiSize);
            this.groupBox1.Controls.Add(this.newMapBtnCreate);
            this.groupBox1.Location = new System.Drawing.Point(6, 346);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(664, 188);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Create a new map";
            // 
            // newMapLiType
            // 
            this.newMapLiType.FormattingEnabled = true;
            this.newMapLiType.Items.AddRange(new object[] {
            "Flat",
            "Desert",
            "Island",
            "Forest",
            "Mountains",
            "Ocean"});
            this.newMapLiType.Location = new System.Drawing.Point(153, 32);
            this.newMapLiType.Name = "newMapLiType";
            this.newMapLiType.Size = new System.Drawing.Size(162, 147);
            this.newMapLiType.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(191, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Generation type:";
            // 
            // newMapTxtName
            // 
            this.newMapTxtName.Location = new System.Drawing.Point(415, 13);
            this.newMapTxtName.Name = "newMapTxtName";
            this.newMapTxtName.Size = new System.Drawing.Size(206, 20);
            this.newMapTxtName.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(349, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Map name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Width x Height x Depth";
            // 
            // newMapLiSize
            // 
            this.newMapLiSize.FormattingEnabled = true;
            this.newMapLiSize.Items.AddRange(new object[] {
            "--- Tiny:",
            "64 x 64 x 64",
            "--- Small:",
            "128 x 64 x 128",
            "128 x 128 x 128",
            "--- Average:",
            "256 x 64 x 128",
            "256 x 128 x 256",
            "--- Large:",
            "512 x 64 x 512",
            "512 x 128 x 512",
            "512 x 256 x 512",
            "--- Huge:",
            "1024 x 64 x 1024",
            "1024 x 128 x 1024",
            "--- Extreme:",
            "2048 x 32 x 2048",
            "2048 x 64 x 2048"});
            this.newMapLiSize.Location = new System.Drawing.Point(6, 32);
            this.newMapLiSize.Name = "newMapLiSize";
            this.newMapLiSize.Size = new System.Drawing.Size(141, 147);
            this.newMapLiSize.TabIndex = 4;
            // 
            // newMapBtnCreate
            // 
            this.newMapBtnCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newMapBtnCreate.Location = new System.Drawing.Point(321, 42);
            this.newMapBtnCreate.Name = "newMapBtnCreate";
            this.newMapBtnCreate.Size = new System.Drawing.Size(337, 126);
            this.newMapBtnCreate.TabIndex = 3;
            this.newMapBtnCreate.Text = "Create";
            this.newMapBtnCreate.UseVisualStyleBackColor = true;
            // 
            // btnMapCenter
            // 
            this.btnMapCenter.Location = new System.Drawing.Point(7, 313);
            this.btnMapCenter.Name = "btnMapCenter";
            this.btnMapCenter.Size = new System.Drawing.Size(158, 28);
            this.btnMapCenter.TabIndex = 11;
            this.btnMapCenter.Text = "Update and center";
            this.btnMapCenter.UseVisualStyleBackColor = true;
            this.btnMapCenter.Click += new System.EventHandler(this.btnMapCenter_Click);
            // 
            // liMaps
            // 
            this.liMaps.ContextMenuStrip = this.menuLevels;
            this.liMaps.FormattingEnabled = true;
            this.liMaps.Location = new System.Drawing.Point(7, 6);
            this.liMaps.Name = "liMaps";
            this.liMaps.Size = new System.Drawing.Size(158, 303);
            this.liMaps.TabIndex = 0;
            this.liMaps.SelectedIndexChanged += new System.EventHandler(this.liMaps_SelectedIndexChanged);
            // 
            // trkMapZoom
            // 
            this.trkMapZoom.LargeChange = 1;
            this.trkMapZoom.Location = new System.Drawing.Point(171, 6);
            this.trkMapZoom.Maximum = 5;
            this.trkMapZoom.Minimum = -1;
            this.trkMapZoom.Name = "trkMapZoom";
            this.trkMapZoom.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trkMapZoom.Size = new System.Drawing.Size(45, 336);
            this.trkMapZoom.TabIndex = 14;
            this.trkMapZoom.Value = 1;
            this.trkMapZoom.Scroll += new System.EventHandler(this.trkMapZoom_Scroll);
            // 
            // prgMapDraw
            // 
            this.prgMapDraw.Location = new System.Drawing.Point(210, 330);
            this.prgMapDraw.Name = "prgMapDraw";
            this.prgMapDraw.Size = new System.Drawing.Size(460, 10);
            this.prgMapDraw.TabIndex = 4;
            // 
            // tabMain
            // 
            this.tabMain.BackColor = System.Drawing.SystemColors.Window;
            this.tabMain.Controls.Add(this.txtCommandInput);
            this.tabMain.Controls.Add(this.txtUrl);
            this.tabMain.Controls.Add(this.txtInput);
            this.tabMain.Controls.Add(this.liClients);
            this.tabMain.Controls.Add(this.lblCommand);
            this.tabMain.Controls.Add(this.grpboxCmdLog);
            this.tabMain.Controls.Add(this.lblLevels);
            this.tabMain.Controls.Add(this.lblURL);
            this.tabMain.Controls.Add(this.liLevels);
            this.tabMain.Controls.Add(this.lblChat);
            this.tabMain.Controls.Add(this.lblPlayers);
            this.tabMain.Controls.Add(this.grpboxLog);
            this.tabMain.Controls.Add(this.btnProperties);
            this.tabMain.Controls.Add(this.tabControl1);
            this.tabMain.Location = new System.Drawing.Point(4, 22);
            this.tabMain.Name = "tabMain";
            this.tabMain.Padding = new System.Windows.Forms.Padding(3);
            this.tabMain.Size = new System.Drawing.Size(676, 540);
            this.tabMain.TabIndex = 0;
            this.tabMain.Text = "Main";
            // 
            // txtCommandInput
            // 
            this.txtCommandInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCommandInput.Location = new System.Drawing.Point(86, 483);
            this.txtCommandInput.Name = "txtCommandInput";
            this.txtCommandInput.Size = new System.Drawing.Size(409, 20);
            this.txtCommandInput.TabIndex = 29;
            this.txtCommandInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCommandInput_KeyDown);
            // 
            // txtUrl
            // 
            this.txtUrl.BackColor = System.Drawing.SystemColors.Window;
            this.txtUrl.Location = new System.Drawing.Point(48, 6);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.ReadOnly = true;
            this.txtUrl.Size = new System.Drawing.Size(622, 20);
            this.txtUrl.TabIndex = 4;
            // 
            // txtInput
            // 
            this.txtInput.Location = new System.Drawing.Point(62, 511);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(433, 20);
            this.txtInput.TabIndex = 12;
            this.txtInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInput_KeyDown);
            // 
            // liClients
            // 
            this.liClients.ContextMenuStrip = this.menuPlayers;
            this.liClients.FormattingEnabled = true;
            this.liClients.Location = new System.Drawing.Point(517, 50);
            this.liClients.Name = "liClients";
            this.liClients.Size = new System.Drawing.Size(153, 199);
            this.liClients.TabIndex = 1;
            this.liClients.SelectedIndexChanged += new System.EventHandler(this.liClients_SelectedIndexChanged);
            this.liClients.MouseDown += new System.Windows.Forms.MouseEventHandler(this.liClients_MouseDown);
            this.liClients.MouseMove += new System.Windows.Forms.MouseEventHandler(this.liClients_MouseMove);
            // 
            // lblCommand
            // 
            this.lblCommand.AutoSize = true;
            this.lblCommand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCommand.Location = new System.Drawing.Point(19, 486);
            this.lblCommand.Name = "lblCommand";
            this.lblCommand.Size = new System.Drawing.Size(65, 13);
            this.lblCommand.TabIndex = 19;
            this.lblCommand.Text = "Command:";
            // 
            // grpboxCmdLog
            // 
            this.grpboxCmdLog.Controls.Add(this.txtCmdLog);
            this.grpboxCmdLog.Location = new System.Drawing.Point(9, 231);
            this.grpboxCmdLog.Name = "grpboxCmdLog";
            this.grpboxCmdLog.Size = new System.Drawing.Size(486, 131);
            this.grpboxCmdLog.TabIndex = 2;
            this.grpboxCmdLog.TabStop = false;
            this.grpboxCmdLog.Text = "Command Log";
            // 
            // txtCmdLog
            // 
            this.txtCmdLog.BackColor = System.Drawing.SystemColors.Window;
            this.txtCmdLog.Location = new System.Drawing.Point(6, 19);
            this.txtCmdLog.Multiline = true;
            this.txtCmdLog.Name = "txtCmdLog";
            this.txtCmdLog.ReadOnly = true;
            this.txtCmdLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCmdLog.Size = new System.Drawing.Size(474, 106);
            this.txtCmdLog.TabIndex = 0;
            // 
            // lblLevels
            // 
            this.lblLevels.AutoSize = true;
            this.lblLevels.Location = new System.Drawing.Point(514, 252);
            this.lblLevels.Name = "lblLevels";
            this.lblLevels.Size = new System.Drawing.Size(74, 13);
            this.lblLevels.TabIndex = 18;
            this.lblLevels.Text = "Loaded maps:";
            // 
            // lblURL
            // 
            this.lblURL.AutoSize = true;
            this.lblURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblURL.Location = new System.Drawing.Point(6, 9);
            this.lblURL.Name = "lblURL";
            this.lblURL.Size = new System.Drawing.Size(36, 13);
            this.lblURL.TabIndex = 3;
            this.lblURL.Text = "URL:";
            // 
            // liLevels
            // 
            this.liLevels.ContextMenuStrip = this.menuLevels;
            this.liLevels.FormattingEnabled = true;
            this.liLevels.Location = new System.Drawing.Point(517, 268);
            this.liLevels.Name = "liLevels";
            this.liLevels.Size = new System.Drawing.Size(153, 199);
            this.liLevels.TabIndex = 17;
            this.liLevels.MouseDown += new System.Windows.Forms.MouseEventHandler(this.liLevels_MouseDown);
            this.liLevels.MouseMove += new System.Windows.Forms.MouseEventHandler(this.liLevels_MouseMove);
            // 
            // lblChat
            // 
            this.lblChat.AutoSize = true;
            this.lblChat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChat.Location = new System.Drawing.Point(19, 514);
            this.lblChat.Name = "lblChat";
            this.lblChat.Size = new System.Drawing.Size(37, 13);
            this.lblChat.TabIndex = 11;
            this.lblChat.Text = "Chat:";
            // 
            // lblPlayers
            // 
            this.lblPlayers.AutoSize = true;
            this.lblPlayers.Location = new System.Drawing.Point(514, 29);
            this.lblPlayers.Name = "lblPlayers";
            this.lblPlayers.Size = new System.Drawing.Size(44, 13);
            this.lblPlayers.TabIndex = 16;
            this.lblPlayers.Text = "Players:";
            // 
            // grpboxLog
            // 
            this.grpboxLog.Controls.Add(this.txtLog);
            this.grpboxLog.Location = new System.Drawing.Point(6, 31);
            this.grpboxLog.Name = "grpboxLog";
            this.grpboxLog.Size = new System.Drawing.Size(489, 194);
            this.grpboxLog.TabIndex = 3;
            this.grpboxLog.TabStop = false;
            this.grpboxLog.Text = "Chat Log";
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.SystemColors.Window;
            this.txtLog.Location = new System.Drawing.Point(6, 19);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(477, 169);
            this.txtLog.TabIndex = 16;
            // 
            // btnProperties
            // 
            this.btnProperties.Location = new System.Drawing.Point(517, 483);
            this.btnProperties.Name = "btnProperties";
            this.btnProperties.Size = new System.Drawing.Size(153, 51);
            this.btnProperties.TabIndex = 14;
            this.btnProperties.Text = "Properties";
            this.btnProperties.UseVisualStyleBackColor = true;
            this.btnProperties.Click += new System.EventHandler(this.btnProperties_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tapPlayer);
            this.tabControl1.Controls.Add(this.tapServer);
            this.tabControl1.Controls.Add(this.tabOther);
            this.tabControl1.Location = new System.Drawing.Point(12, 368);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(483, 94);
            this.tabControl1.TabIndex = 15;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabAll_SelectedIndexChanged);
            // 
            // tapPlayer
            // 
            this.tapPlayer.Controls.Add(this.btnUnbanIP);
            this.tapPlayer.Controls.Add(this.btnUnban);
            this.tapPlayer.Controls.Add(this.btnChangeRank);
            this.tapPlayer.Controls.Add(this.btnKick);
            this.tapPlayer.Controls.Add(this.btnBan);
            this.tapPlayer.Controls.Add(this.btnBanIp);
            this.tapPlayer.Controls.Add(this.btnKickban);
            this.tapPlayer.Location = new System.Drawing.Point(4, 22);
            this.tapPlayer.Name = "tapPlayer";
            this.tapPlayer.Padding = new System.Windows.Forms.Padding(3);
            this.tapPlayer.Size = new System.Drawing.Size(475, 68);
            this.tapPlayer.TabIndex = 0;
            this.tapPlayer.Text = "Player Controls";
            this.tapPlayer.UseVisualStyleBackColor = true;
            // 
            // btnUnbanIP
            // 
            this.btnUnbanIP.Location = new System.Drawing.Point(6, 37);
            this.btnUnbanIP.Name = "btnUnbanIP";
            this.btnUnbanIP.Size = new System.Drawing.Size(146, 25);
            this.btnUnbanIP.TabIndex = 11;
            this.btnUnbanIP.Text = "UnbanIP";
            this.btnUnbanIP.UseVisualStyleBackColor = true;
            this.btnUnbanIP.Click += new System.EventHandler(this.btnUnbanIP_Click);
            // 
            // btnUnban
            // 
            this.btnUnban.Location = new System.Drawing.Point(326, 37);
            this.btnUnban.Name = "btnUnban";
            this.btnUnban.Size = new System.Drawing.Size(143, 25);
            this.btnUnban.TabIndex = 12;
            this.btnUnban.Text = "Unban";
            this.btnUnban.Click += new System.EventHandler(this.btnUnban_Click);
            // 
            // btnChangeRank
            // 
            this.btnChangeRank.Location = new System.Drawing.Point(158, 37);
            this.btnChangeRank.Name = "btnChangeRank";
            this.btnChangeRank.Size = new System.Drawing.Size(162, 25);
            this.btnChangeRank.TabIndex = 9;
            this.btnChangeRank.Text = "Change Rank";
            this.btnChangeRank.UseVisualStyleBackColor = true;
            this.btnChangeRank.Click += new System.EventHandler(this.btnChangeRank_Click);
            // 
            // btnKick
            // 
            this.btnKick.Location = new System.Drawing.Point(8, 6);
            this.btnKick.Name = "btnKick";
            this.btnKick.Size = new System.Drawing.Size(114, 25);
            this.btnKick.TabIndex = 5;
            this.btnKick.Text = "Kick";
            this.btnKick.UseVisualStyleBackColor = true;
            this.btnKick.Click += new System.EventHandler(this.btnKick_Click);
            // 
            // btnBan
            // 
            this.btnBan.Location = new System.Drawing.Point(246, 6);
            this.btnBan.Name = "btnBan";
            this.btnBan.Size = new System.Drawing.Size(106, 25);
            this.btnBan.TabIndex = 6;
            this.btnBan.Text = "Ban";
            this.btnBan.UseVisualStyleBackColor = true;
            this.btnBan.Click += new System.EventHandler(this.btnBan_Click);
            // 
            // btnBanIp
            // 
            this.btnBanIp.Location = new System.Drawing.Point(358, 6);
            this.btnBanIp.Name = "btnBanIp";
            this.btnBanIp.Size = new System.Drawing.Size(111, 25);
            this.btnBanIp.TabIndex = 8;
            this.btnBanIp.Text = "BanIP";
            this.btnBanIp.UseVisualStyleBackColor = true;
            this.btnBanIp.Click += new System.EventHandler(this.btnBanIp_Click);
            // 
            // btnKickban
            // 
            this.btnKickban.Location = new System.Drawing.Point(128, 6);
            this.btnKickban.Name = "btnKickban";
            this.btnKickban.Size = new System.Drawing.Size(112, 25);
            this.btnKickban.TabIndex = 7;
            this.btnKickban.Text = "Kickban";
            this.btnKickban.UseVisualStyleBackColor = true;
            this.btnKickban.Click += new System.EventHandler(this.btnKickban_Click);
            // 
            // tapServer
            // 
            this.tapServer.Controls.Add(this.btnRestart);
            this.tapServer.Controls.Add(this.btnReloadMJS);
            this.tapServer.Controls.Add(this.btnReloadCmds);
            this.tapServer.Controls.Add(this.btnShutdown);
            this.tapServer.Controls.Add(this.btnReloadRanks);
            this.tapServer.Controls.Add(this.btnReloadProperties);
            this.tapServer.Location = new System.Drawing.Point(4, 22);
            this.tapServer.Name = "tapServer";
            this.tapServer.Padding = new System.Windows.Forms.Padding(3);
            this.tapServer.Size = new System.Drawing.Size(475, 68);
            this.tapServer.TabIndex = 1;
            this.tapServer.Text = "Server Controls";
            this.tapServer.UseVisualStyleBackColor = true;
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(162, 37);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(159, 25);
            this.btnRestart.TabIndex = 15;
            this.btnRestart.Text = "Restart Server";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // btnReloadMJS
            // 
            this.btnReloadMJS.Location = new System.Drawing.Point(6, 7);
            this.btnReloadMJS.Name = "btnReloadMJS";
            this.btnReloadMJS.Size = new System.Drawing.Size(150, 25);
            this.btnReloadMJS.TabIndex = 14;
            this.btnReloadMJS.Text = "Reload MJS";
            this.btnReloadMJS.UseVisualStyleBackColor = true;
            this.btnReloadMJS.Click += new System.EventHandler(this.btnReloadMJS_Click);
            // 
            // btnReloadCmds
            // 
            this.btnReloadCmds.Location = new System.Drawing.Point(327, 7);
            this.btnReloadCmds.Name = "btnReloadCmds";
            this.btnReloadCmds.Size = new System.Drawing.Size(142, 25);
            this.btnReloadCmds.TabIndex = 13;
            this.btnReloadCmds.Text = "Reload Commands";
            this.btnReloadCmds.UseVisualStyleBackColor = true;
            this.btnReloadCmds.Click += new System.EventHandler(this.btnReloadCmds_Click);
            // 
            // btnShutdown
            // 
            this.btnShutdown.Location = new System.Drawing.Point(162, 7);
            this.btnShutdown.Name = "btnShutdown";
            this.btnShutdown.Size = new System.Drawing.Size(159, 25);
            this.btnShutdown.TabIndex = 12;
            this.btnShutdown.Text = "Shutdown Server";
            this.btnShutdown.UseVisualStyleBackColor = true;
            this.btnShutdown.Click += new System.EventHandler(this.btnShutdown_Click);
            // 
            // btnReloadRanks
            // 
            this.btnReloadRanks.Location = new System.Drawing.Point(6, 37);
            this.btnReloadRanks.Name = "btnReloadRanks";
            this.btnReloadRanks.Size = new System.Drawing.Size(150, 25);
            this.btnReloadRanks.TabIndex = 11;
            this.btnReloadRanks.Text = "Reload Ranks";
            this.btnReloadRanks.UseVisualStyleBackColor = true;
            this.btnReloadRanks.Click += new System.EventHandler(this.btnReloadRanks_Click);
            // 
            // btnReloadProperties
            // 
            this.btnReloadProperties.Location = new System.Drawing.Point(327, 37);
            this.btnReloadProperties.Name = "btnReloadProperties";
            this.btnReloadProperties.Size = new System.Drawing.Size(142, 25);
            this.btnReloadProperties.TabIndex = 10;
            this.btnReloadProperties.Text = "Reload Properties";
            this.btnReloadProperties.UseVisualStyleBackColor = true;
            this.btnReloadProperties.Click += new System.EventHandler(this.btnReloadProperties_Click);
            // 
            // tabOther
            // 
            this.tabOther.Controls.Add(this.btnWOM);
            this.tabOther.Controls.Add(this.btnMJSLog);
            this.tabOther.Controls.Add(this.label1);
            this.tabOther.Controls.Add(this.txtConsoleName);
            this.tabOther.Controls.Add(this.btnRules);
            this.tabOther.Controls.Add(this.btnFolder);
            this.tabOther.Location = new System.Drawing.Point(4, 22);
            this.tabOther.Name = "tabOther";
            this.tabOther.Size = new System.Drawing.Size(475, 68);
            this.tabOther.TabIndex = 2;
            this.tabOther.Text = "Other Controls";
            this.tabOther.UseVisualStyleBackColor = true;
            // 
            // btnWOM
            // 
            this.btnWOM.Location = new System.Drawing.Point(163, 3);
            this.btnWOM.Name = "btnWOM";
            this.btnWOM.Size = new System.Drawing.Size(156, 30);
            this.btnWOM.TabIndex = 5;
            this.btnWOM.Text = "WOM Direct";
            this.btnWOM.UseVisualStyleBackColor = true;
            // 
            // btnMJSLog
            // 
            this.btnMJSLog.Location = new System.Drawing.Point(3, 35);
            this.btnMJSLog.Name = "btnMJSLog";
            this.btnMJSLog.Size = new System.Drawing.Size(235, 30);
            this.btnMJSLog.TabIndex = 4;
            this.btnMJSLog.Text = "Open MJS Log";
            this.btnMJSLog.UseVisualStyleBackColor = true;
            this.btnMJSLog.Click += new System.EventHandler(this.btnMJSLog_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(243, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Console State:";
            // 
            // txtConsoleName
            // 
            this.txtConsoleName.Location = new System.Drawing.Point(325, 41);
            this.txtConsoleName.Name = "txtConsoleName";
            this.txtConsoleName.Size = new System.Drawing.Size(144, 20);
            this.txtConsoleName.TabIndex = 2;
            this.txtConsoleName.TextChanged += new System.EventHandler(this.txtConsoleName_TextChanged);
            this.txtConsoleName.LostFocus += new System.EventHandler(this.txtConsoleName_LostFocus);
            // 
            // btnRules
            // 
            this.btnRules.Location = new System.Drawing.Point(325, 3);
            this.btnRules.Name = "btnRules";
            this.btnRules.Size = new System.Drawing.Size(144, 30);
            this.btnRules.TabIndex = 1;
            this.btnRules.Text = "Change Rules";
            this.btnRules.UseVisualStyleBackColor = true;
            this.btnRules.Click += new System.EventHandler(this.btnRules_Click);
            // 
            // btnFolder
            // 
            this.btnFolder.Location = new System.Drawing.Point(3, 3);
            this.btnFolder.Name = "btnFolder";
            this.btnFolder.Size = new System.Drawing.Size(154, 30);
            this.btnFolder.TabIndex = 0;
            this.btnFolder.Text = "Open MCRevive Folder";
            this.btnFolder.UseVisualStyleBackColor = true;
            this.btnFolder.Click += new System.EventHandler(this.btnFolder_Click);
            // 
            // tabAll
            // 
            this.tabAll.Controls.Add(this.tabMain);
            this.tabAll.Controls.Add(this.tabMaps);
            this.tabAll.Controls.Add(this.tabChangelog);
            this.tabAll.Controls.Add(this.tabLogs);
            this.tabAll.Controls.Add(this.tabServerlist);
            this.tabAll.Controls.Add(this.tabExtra);
            this.tabAll.Controls.Add(this.tabRoutines);
            this.tabAll.Location = new System.Drawing.Point(12, 12);
            this.tabAll.Name = "tabAll";
            this.tabAll.SelectedIndex = 0;
            this.tabAll.Size = new System.Drawing.Size(684, 566);
            this.tabAll.TabIndex = 21;
            this.tabAll.SelectedIndexChanged += new System.EventHandler(this.tabAll_SelectedIndexChanged);
            // 
            // tabServerlist
            // 
            this.tabServerlist.Controls.Add(this.groupBox12);
            this.tabServerlist.Location = new System.Drawing.Point(4, 22);
            this.tabServerlist.Name = "tabServerlist";
            this.tabServerlist.Size = new System.Drawing.Size(676, 540);
            this.tabServerlist.TabIndex = 8;
            this.tabServerlist.Text = "Serverlist";
            this.tabServerlist.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.dgvServers);
            this.groupBox12.Location = new System.Drawing.Point(3, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(670, 534);
            this.groupBox12.TabIndex = 0;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "MCRevive servers";
            // 
            // dgvServers
            // 
            this.dgvServers.AllowUserToAddRows = false;
            this.dgvServers.AllowUserToDeleteRows = false;
            this.dgvServers.AllowUserToResizeRows = false;
            this.dgvServers.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvServers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvServers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmName,
            this.clmPlayers,
            this.clmOwner,
            this.clmDesc});
            this.dgvServers.Location = new System.Drawing.Point(6, 19);
            this.dgvServers.MultiSelect = false;
            this.dgvServers.Name = "dgvServers";
            this.dgvServers.ReadOnly = true;
            this.dgvServers.RowTemplate.ReadOnly = true;
            this.dgvServers.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvServers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvServers.Size = new System.Drawing.Size(658, 509);
            this.dgvServers.TabIndex = 0;
            // 
            // clmName
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.clmName.DefaultCellStyle = dataGridViewCellStyle1;
            this.clmName.HeaderText = "Name";
            this.clmName.Name = "clmName";
            this.clmName.ReadOnly = true;
            this.clmName.Width = 115;
            // 
            // clmPlayers
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clmPlayers.DefaultCellStyle = dataGridViewCellStyle2;
            this.clmPlayers.HeaderText = "Players";
            this.clmPlayers.Name = "clmPlayers";
            this.clmPlayers.ReadOnly = true;
            this.clmPlayers.ToolTipText = "This is how many players that play on the server";
            // 
            // clmOwner
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clmOwner.DefaultCellStyle = dataGridViewCellStyle3;
            this.clmOwner.HeaderText = "Owner";
            this.clmOwner.Name = "clmOwner";
            this.clmOwner.ReadOnly = true;
            // 
            // clmDesc
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clmDesc.DefaultCellStyle = dataGridViewCellStyle4;
            this.clmDesc.HeaderText = "Description";
            this.clmDesc.Name = "clmDesc";
            this.clmDesc.ReadOnly = true;
            this.clmDesc.Width = 300;
            // 
            // tabRoutines
            // 
            this.tabRoutines.Controls.Add(this.groupBox11);
            this.tabRoutines.Controls.Add(this.groupBox10);
            this.tabRoutines.Controls.Add(this.groupBox9);
            this.tabRoutines.Controls.Add(this.groupBox8);
            this.tabRoutines.Controls.Add(this.groupBox7);
            this.tabRoutines.Controls.Add(this.groupBox4);
            this.tabRoutines.Location = new System.Drawing.Point(4, 22);
            this.tabRoutines.Name = "tabRoutines";
            this.tabRoutines.Padding = new System.Windows.Forms.Padding(3);
            this.tabRoutines.Size = new System.Drawing.Size(676, 540);
            this.tabRoutines.TabIndex = 9;
            this.tabRoutines.Text = "Routines";
            this.tabRoutines.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.btnRoutineDelete);
            this.groupBox11.Controls.Add(this.btnRoutineActionDelete);
            this.groupBox11.Location = new System.Drawing.Point(201, 276);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(232, 48);
            this.groupBox11.TabIndex = 40;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Delete something";
            // 
            // btnRoutineDelete
            // 
            this.btnRoutineDelete.Enabled = false;
            this.btnRoutineDelete.Location = new System.Drawing.Point(9, 19);
            this.btnRoutineDelete.Name = "btnRoutineDelete";
            this.btnRoutineDelete.Size = new System.Drawing.Size(103, 23);
            this.btnRoutineDelete.TabIndex = 1;
            this.btnRoutineDelete.Text = "---> Delete routine";
            this.btnRoutineDelete.UseVisualStyleBackColor = true;
            this.btnRoutineDelete.Click += new System.EventHandler(this.btnRoutineDelete_Click);
            // 
            // btnRoutineActionDelete
            // 
            this.btnRoutineActionDelete.Enabled = false;
            this.btnRoutineActionDelete.Location = new System.Drawing.Point(118, 19);
            this.btnRoutineActionDelete.Name = "btnRoutineActionDelete";
            this.btnRoutineActionDelete.Size = new System.Drawing.Size(108, 23);
            this.btnRoutineActionDelete.TabIndex = 0;
            this.btnRoutineActionDelete.Text = "Delete action <---";
            this.btnRoutineActionDelete.UseVisualStyleBackColor = true;
            this.btnRoutineActionDelete.Click += new System.EventHandler(this.btnRoutineActionDelete_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.txtRoutineActionAddInfo);
            this.groupBox10.Controls.Add(this.btnRoutineActionAdd);
            this.groupBox10.Controls.Add(this.liRoutinesAction);
            this.groupBox10.Controls.Add(this.liRoutineActionList);
            this.groupBox10.Location = new System.Drawing.Point(201, 118);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(232, 152);
            this.groupBox10.TabIndex = 39;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Add action to routine";
            // 
            // txtRoutineActionAddInfo
            // 
            this.txtRoutineActionAddInfo.Enabled = false;
            this.txtRoutineActionAddInfo.Location = new System.Drawing.Point(6, 95);
            this.txtRoutineActionAddInfo.Name = "txtRoutineActionAddInfo";
            this.txtRoutineActionAddInfo.Size = new System.Drawing.Size(220, 20);
            this.txtRoutineActionAddInfo.TabIndex = 3;
            this.txtRoutineActionAddInfo.TextChanged += new System.EventHandler(this.txtRoutineActionAddInfo_TextChanged);
            // 
            // btnRoutineActionAdd
            // 
            this.btnRoutineActionAdd.Enabled = false;
            this.btnRoutineActionAdd.Location = new System.Drawing.Point(6, 121);
            this.btnRoutineActionAdd.Name = "btnRoutineActionAdd";
            this.btnRoutineActionAdd.Size = new System.Drawing.Size(220, 23);
            this.btnRoutineActionAdd.TabIndex = 2;
            this.btnRoutineActionAdd.Text = "Add action --->";
            this.btnRoutineActionAdd.UseVisualStyleBackColor = true;
            this.btnRoutineActionAdd.Click += new System.EventHandler(this.btnRoutineActionAdd_Click);
            // 
            // liRoutinesAction
            // 
            this.liRoutinesAction.Enabled = false;
            this.liRoutinesAction.FormattingEnabled = true;
            this.liRoutinesAction.Location = new System.Drawing.Point(139, 20);
            this.liRoutinesAction.Name = "liRoutinesAction";
            this.liRoutinesAction.Size = new System.Drawing.Size(87, 69);
            this.liRoutinesAction.TabIndex = 1;
            this.liRoutinesAction.SelectedIndexChanged += new System.EventHandler(this.liRoutinesAction_SelectedIndexChanged);
            // 
            // liRoutineActionList
            // 
            this.liRoutineActionList.Enabled = false;
            this.liRoutineActionList.FormattingEnabled = true;
            this.liRoutineActionList.Items.AddRange(new object[] {
            "Broadcast message",
            "Execute command",
            "Run MJS script",
            "Save map",
            "Load map",
            "Unload map",
            "Restart server"});
            this.liRoutineActionList.Location = new System.Drawing.Point(6, 20);
            this.liRoutineActionList.Name = "liRoutineActionList";
            this.liRoutineActionList.Size = new System.Drawing.Size(127, 69);
            this.liRoutineActionList.TabIndex = 0;
            this.liRoutineActionList.SelectedIndexChanged += new System.EventHandler(this.liRoutineActionList_SelectedIndexChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.liRoutineActions);
            this.groupBox9.Location = new System.Drawing.Point(439, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(225, 318);
            this.groupBox9.TabIndex = 38;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Routine actions";
            // 
            // liRoutineActions
            // 
            this.liRoutineActions.Enabled = false;
            this.liRoutineActions.FormattingEnabled = true;
            this.liRoutineActions.Location = new System.Drawing.Point(6, 19);
            this.liRoutineActions.Name = "liRoutineActions";
            this.liRoutineActions.Size = new System.Drawing.Size(213, 290);
            this.liRoutineActions.TabIndex = 1;
            this.liRoutineActions.SelectedIndexChanged += new System.EventHandler(this.liRoutineActions_SelectedIndexChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.txtRoutineLog);
            this.groupBox8.Location = new System.Drawing.Point(8, 330);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(662, 204);
            this.groupBox8.TabIndex = 37;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Logs";
            // 
            // txtRoutineLog
            // 
            this.txtRoutineLog.Location = new System.Drawing.Point(6, 19);
            this.txtRoutineLog.Multiline = true;
            this.txtRoutineLog.Name = "txtRoutineLog";
            this.txtRoutineLog.ReadOnly = true;
            this.txtRoutineLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRoutineLog.Size = new System.Drawing.Size(650, 179);
            this.txtRoutineLog.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button5);
            this.groupBox7.Controls.Add(this.liRoutines);
            this.groupBox7.Location = new System.Drawing.Point(6, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(189, 318);
            this.groupBox7.TabIndex = 36;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Routines";
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(8, 289);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(173, 23);
            this.button5.TabIndex = 1;
            this.button5.Text = "Edit";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // liRoutines
            // 
            this.liRoutines.FormattingEnabled = true;
            this.liRoutines.Location = new System.Drawing.Point(6, 19);
            this.liRoutines.Name = "liRoutines";
            this.liRoutines.Size = new System.Drawing.Size(175, 264);
            this.liRoutines.TabIndex = 0;
            this.liRoutines.SelectedIndexChanged += new System.EventHandler(this.liRoutines_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.txtRoutine);
            this.groupBox4.Controls.Add(this.btnRoutineAdd);
            this.groupBox4.Controls.Add(this.liRoutineExec);
            this.groupBox4.Controls.Add(this.numRoutineExec);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Location = new System.Drawing.Point(201, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(232, 106);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Add new routine";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "Name:";
            // 
            // txtRoutine
            // 
            this.txtRoutine.Location = new System.Drawing.Point(53, 19);
            this.txtRoutine.Name = "txtRoutine";
            this.txtRoutine.Size = new System.Drawing.Size(173, 20);
            this.txtRoutine.TabIndex = 4;
            this.txtRoutine.Text = "NewRoutine";
            this.txtRoutine.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtRoutine_MouseClick);
            this.txtRoutine.TextChanged += new System.EventHandler(this.txtRoutine_TextChanged);
            // 
            // btnRoutineAdd
            // 
            this.btnRoutineAdd.Enabled = false;
            this.btnRoutineAdd.Location = new System.Drawing.Point(6, 70);
            this.btnRoutineAdd.Name = "btnRoutineAdd";
            this.btnRoutineAdd.Size = new System.Drawing.Size(130, 23);
            this.btnRoutineAdd.TabIndex = 3;
            this.btnRoutineAdd.Text = "<--- Create";
            this.btnRoutineAdd.UseVisualStyleBackColor = true;
            this.btnRoutineAdd.Click += new System.EventHandler(this.btnRoutineAdd_Click);
            // 
            // liRoutineExec
            // 
            this.liRoutineExec.FormattingEnabled = true;
            this.liRoutineExec.Items.AddRange(new object[] {
            "minutes",
            "hours",
            "days"});
            this.liRoutineExec.Location = new System.Drawing.Point(142, 44);
            this.liRoutineExec.Name = "liRoutineExec";
            this.liRoutineExec.Size = new System.Drawing.Size(84, 43);
            this.liRoutineExec.TabIndex = 2;
            this.liRoutineExec.SelectedIndexChanged += new System.EventHandler(this.liRoutineExec_SelectedIndexChanged);
            // 
            // numRoutineExec
            // 
            this.numRoutineExec.Location = new System.Drawing.Point(87, 44);
            this.numRoutineExec.Name = "numRoutineExec";
            this.numRoutineExec.Size = new System.Drawing.Size(49, 20);
            this.numRoutineExec.TabIndex = 1;
            this.numRoutineExec.ValueChanged += new System.EventHandler(this.numRoutineExec_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 46);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Execute every";
            // 
            // GuiWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 590);
            this.Controls.Add(this.tabAll);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "GuiWindow";
            this.Text = "<server name here>";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Window_FormClosing);
            this.Load += new System.EventHandler(this.Window_Load);
            this.menuPlayers.ResumeLayout(false);
            this.menuLevels.ResumeLayout(false);
            this.tabExtra.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabLogs.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabChangelog.ResumeLayout(false);
            this.grpChangelogNew.ResumeLayout(false);
            this.grpChangelog.ResumeLayout(false);
            this.tabMaps.ResumeLayout(false);
            this.tabMaps.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkMapZoom)).EndInit();
            this.tabMain.ResumeLayout(false);
            this.tabMain.PerformLayout();
            this.grpboxCmdLog.ResumeLayout(false);
            this.grpboxCmdLog.PerformLayout();
            this.grpboxLog.ResumeLayout(false);
            this.grpboxLog.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tapPlayer.ResumeLayout(false);
            this.tapServer.ResumeLayout(false);
            this.tabOther.ResumeLayout(false);
            this.tabOther.PerformLayout();
            this.tabAll.ResumeLayout(false);
            this.tabServerlist.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvServers)).EndInit();
            this.tabRoutines.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRoutineExec)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip menuLevels;
        private System.Windows.Forms.ToolStripMenuItem unloadToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip menuPlayers;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visitPermissionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buildPermissionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rankToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem demoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kickToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem banStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem banToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iPBanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem banAndIPBanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox undoTimeStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem oKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendToMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lolToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox loadMapStripTextBox;
        private System.Windows.Forms.ToolStripMenuItem oKToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.TabPage tabExtra;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label lblPlayerScale1;
        private System.Windows.Forms.Label lblPlayerScale3;
        private System.Windows.Forms.Panel pnlPlayers;
        private System.Windows.Forms.Label lblPlayerScale2;
        private System.Windows.Forms.Label lblPlayerScale0;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblTrafficScale2;
        private System.Windows.Forms.Label lblTrafficScale1;
        private System.Windows.Forms.Label lblTrafficScale0;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnlTraffic;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabLogs;
        private System.Windows.Forms.GroupBox groupBox3;
        private CButton btnRefreshErrorLog;
        private CButton btnOpenErrorLog;
        private System.Windows.Forms.TextBox txtErrorLog;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DateTimePicker dtpError;
        private System.Windows.Forms.GroupBox groupBox2;
        private CButton btnRefreshServerLog;
        private CButton btnOpenServerLog;
        private System.Windows.Forms.TextBox txtServerLog;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker dtpLog;
        private System.Windows.Forms.TabPage tabChangelog;
        private System.Windows.Forms.RichTextBox rtxtChangelog1;
        private System.Windows.Forms.TabPage tabMaps;
        private CButton btnMapCenter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox newMapLiType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox newMapTxtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox newMapLiSize;
        private CButton newMapBtnCreate;
        private System.Windows.Forms.ListBox liMaps;
        private System.Windows.Forms.TabPage tabMain;
        private System.Windows.Forms.TextBox txtCommandInput;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.ListBox liClients;
        private System.Windows.Forms.Label lblCommand;
        private System.Windows.Forms.GroupBox grpboxCmdLog;
        private System.Windows.Forms.TextBox txtCmdLog;
        private System.Windows.Forms.Label lblLevels;
        private System.Windows.Forms.Label lblURL;
        private System.Windows.Forms.ListBox liLevels;
        private System.Windows.Forms.Label lblChat;
        private System.Windows.Forms.Label lblPlayers;
        private System.Windows.Forms.GroupBox grpboxLog;
        private System.Windows.Forms.TextBox txtLog;
        private CButton btnProperties;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tapPlayer;
        private CButton btnUnbanIP;
        private CButton btnUnban;
        private CButton btnChangeRank;
        private CButton btnKick;
        private CButton btnBan;
        private CButton btnBanIp;
        private CButton btnKickban;
        private System.Windows.Forms.TabPage tapServer;
        private CButton btnRestart;
        private CButton btnReloadMJS;
        private CButton btnReloadCmds;
        private CButton btnShutdown;
        private CButton btnReloadRanks;
        private CButton btnReloadProperties;
        private System.Windows.Forms.TabPage tabOther;
        private CButton btnWOM;
        private CButton btnMJSLog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtConsoleName;
        private CButton btnRules;
        private CButton btnFolder;
        private System.Windows.Forms.TabControl tabAll;
        private System.Windows.Forms.TabPage tabServerlist;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.DataGridView dgvServers;
        private System.Windows.Forms.TabPage tabRoutines;
        private System.Windows.Forms.GroupBox groupBox11;
        private CButton btnRoutineDelete;
        private CButton btnRoutineActionDelete;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox txtRoutineActionAddInfo;
        private CButton btnRoutineActionAdd;
        private System.Windows.Forms.ListBox liRoutinesAction;
        private System.Windows.Forms.ListBox liRoutineActionList;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ListBox liRoutineActions;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txtRoutineLog;
        private System.Windows.Forms.GroupBox groupBox7;
        private CButton button5;
        private System.Windows.Forms.ListBox liRoutines;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtRoutine;
        private CButton btnRoutineAdd;
        private System.Windows.Forms.ListBox liRoutineExec;
        private System.Windows.Forms.NumericUpDown numRoutineExec;
        private System.Windows.Forms.Label label14;
        private MapDrawPanel mdpMap;
        private System.Windows.Forms.ProgressBar prgMapDraw;
        private System.Windows.Forms.TrackBar trkMapZoom;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmPlayers;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmOwner;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDesc;
        private System.Windows.Forms.RichTextBox rtxtChangelog2;
        private System.Windows.Forms.GroupBox grpChangelogNew;
        private System.Windows.Forms.GroupBox grpChangelog;
    }
}