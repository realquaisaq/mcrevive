﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Net;
using System.Diagnostics;

namespace MCRevive.Gui
{
    public partial class PropertiesWindow : Form
    {
        bool saved = false;
        List<GrpCommands.rankAllowance> commands = new List<GrpCommands.rankAllowance>();
        List<Block.Blocks> blocks = new List<Block.Blocks>();
        SelectionWindow AchForm = new SelectionWindow();

        public PropertiesWindow()
        {
            InitializeComponent();
        }

        public void PropertiesWindow_Load(object sender, EventArgs e)
        {
            if (!File.Exists("text/description.txt")) File.WriteAllText("text/description.txt", "A nice server.");
            txtServerDescription.Text = File.ReadAllLines("text/description.txt")[0];
            LoadProp("server");
            LoadProp("gui");
            LoadProp("antigrief");
            LoadProp("update");
            LoadProp("achievements");

            cmbForceAfter.SelectedIndex = 0;
            cmbChkHour.SelectedIndex = 0;
            if (!chkAchievements.Checked)
            {
                tt.SetToolTip(this.chkAchievements, "Should there be achievements on this server?\n\nClick to choose which achievements should be allowed on the server.");
                chkach = true;
            }
            AchForm.WK = SelectionWindow.WindowKind.Achievement;

            try
            {
                LoadCommands();
                LoadBlocks();
                LoadRevisions();
                UpdateRankList();
            }
            catch (Exception ex) { Server.ErrorLog(ex); Server.s.Log("Failed to load something."); }
        }

        public void PropertiesWindow_Unload(object sender, FormClosingEventArgs e)
        {
            if (!saved)
                if (MessageBox.Show("All unsaved changes will be lost." + Environment.NewLine + "Continue?", "Exit", MessageBoxButtons.YesNo) != DialogResult.Yes) { e.Cancel = true; return; }
            if (File.Exists("memory/commands.txt"))
                File.Delete("memory/commands.txt");
            if (File.Exists("memory/blocks.txt"))
                File.Delete("memory/blocks.txt");
            GuiWindow.PropertiesWindowUse = false;
        }

        public bool ValidString(string str, string allowed)
        {
            string allowedchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890" + allowed;
            foreach (char ch in str)
                if (allowedchars.IndexOf(ch) == -1)
                    return false;
            return true;
        }

        #region properties
        public void LoadProp(string givenPath)
        {
            givenPath = "properties/" + givenPath + ".properties";
            if (File.Exists(givenPath))
            {
                string[] lines = File.ReadAllLines(givenPath);

                foreach (string line in lines)
                {
                    if (line != "" && line[0] != '#')
                    {
                        //int index = line.IndexOf('=') + 1; // not needed if we use Split('=')
                        string key = line.Split('=')[0].Trim();
                        string value = line.Split('=')[1].Trim();
                        string color = "";

                        switch (key.ToLower())
                        {
                            case "the-owner":
                                if (Player.ValidName(value)) txtServerOwner.Text = value;
                                else txtServerOwner.Text = "Your Name Here";
                                break;
                            case "server-name":
                                if (ValidString(value, "![]:.,{}~-+()?_/\\\'\" ")) txtServerName.Text = value;
                                else txtServerName.Text = "[MCRevive] Default";
                                break;
                            case "motd":
                                if (ValidString(value, "![]&:.,{}~-+()?_/\\ ")) txtServerMOTD.Text = value;
                                else txtServerMOTD.Text = "Welcome to my server!";
                                break;
                            case "port":
                                try { txtServerPort.Text = Convert.ToInt32(value).ToString(); }
                                catch { txtServerPort.Text = "25565"; }
                                break;
                            case "verify-names":
                                chkVerifyNames.Checked = value.ToLower() == "true";
                                break;
                            case "public":
                                chkPublic.Checked = value.ToLower() == "true";
                                break;
                            case "global-chat":
                                chkWorldChat.Checked = value.ToLower() == "true";
                                break;
                            case "guest-goto":
                                chkGuestGoto.Checked = value.ToLower() == "true";
                                break;
                            case "op-promote":
                                chkOptoOp.Checked = value.ToLower() == "true";
                                break;
                            case "admin-promote":
                                chkAdmintoAdmin.Checked = value.ToLower() == "true";
                                break;
                            case "geolocator":
                                chkGeolocation.Checked = value.ToLower() == "true";
                                break;
                            case "owner-login":
                                if (ValidString(value, "![]&:.,{}~-+()?_/\\ ")) txtServerOwnerLogin.Text = value;
                                else txtServerOwnerLogin.Text = "Server owner appeared!";
                                break;
                            case "max-players":
                                try
                                {
                                    if (int.Parse(value) > 128)
                                    {
                                        value = "128";
                                    }
                                    else if (int.Parse(value) < 1)
                                    {
                                        value = "1";
                                    }
                                    txtServerMaxPlayers.Text = value;
                                }
                                catch
                                {
                                    Server.s.Log("max-players invalid! setting to default.");
                                    txtServerMaxPlayers.Text = "12";
                                }
                                break;
                            case "main-level":
                                txtMainLevel.Text = value;
                                break;
                            case "max-maps":
                                try
                                {
                                    if (Convert.ToByte(value) > 35)
                                    {
                                        value = "35";
                                    }
                                    else if (Convert.ToByte(value) < 1)
                                    {
                                        value = "1";
                                    }
                                    txtServerMaxMaps.Text = value;
                                }
                                catch
                                {
                                    Server.s.Log("max-maps invalid! setting to default.");
                                    txtServerMaxMaps.Text = "5";
                                }
                                break;
                            case "irc":
                                chkIRC.Checked = value.ToLower() == "true";
                                break;
                            case "irc-server":
                                txtIRCServer.Text = value;
                                break;
                            case "irc-nick":
                                txtIRCNick.Text = value;
                                break;
                            case "irc-channel":
                                txtIRCChannel.Text = value;
                                break;
                            case "irc-port":
                                txtIRCPort.Text = value;
                                break;
                            case "irc-identify":
                                chkIRCIdentify.Checked = value.ToLower() == "true";
                                break;
                            case "irc-password":
                                txtIRCPassword.Text = value;
                                break;
                            case "anti-tunnels":
                                ChkTunnels.Checked = value.ToLower() == "true";
                                break;
                            case "max-depth":
                                txtMaxDepth.Text = value;
                                break;

                            case "overload":
                                try
                                {
                                    if (Convert.ToInt16(value) > 5000)
                                    {
                                        value = "4000";
                                        Server.s.Log("Max overload is 5000.");
                                    }
                                    else if (Convert.ToInt16(value) < 500)
                                    {
                                        value = "500";
                                        Server.s.Log("Min overload is 500");
                                    }
                                    txtServerOverload.Text = value;
                                }
                                catch
                                {
                                    txtServerOverload.Text = "1500";
                                }
                                break;

                            case "backup-time":
                                if (Convert.ToInt32(value) > 1) txtBackup.Text = value; else txtBackup.Text = "300";
                                break;

                            case "default-color":
                                color = c.Parse(value);
                                if (color == "")
                                {
                                    color = c.Name(value); if (color != "") color = c.Parse(value); else { Server.s.Log("Could not find " + value); return; }
                                }
                                cmbDefaultColor.SelectedIndex = cmbDefaultColor.Items.IndexOf(c.Name(color)); break;
                            case "old-help":
                                chkHelp.Checked = value.ToLower() == "true";
                                break;
                            case "afk-minutes":
                                try { txtAFKTimer.Text = Convert.ToInt16(value).ToString(); }
                                catch { txtAFKTimer.Text = "10"; }
                                break;
                            case "afk-kick":
                                try { txtAFKKick.Text = Convert.ToInt16(value).ToString(); }
                                catch { txtAFKKick.Text = "45"; }
                                break;
                            case "anti-grief-system":
                                chkAntiGrief.Checked = value.ToLower() == "true";
                                break;
                            case "op-notify":
                                string[] split = value.ToLower().Split(':');
                                if (split.Length != 4) { Server.s.Log("failed to load: op-notify, setting to default."); break; }
                                chkOpNotify.Checked = (split[0].ToLower().Trim() == "true");
                                if (split[1].ToLower().Trim() == "destroyed") cmbGrief1.SelectedIndex = 0;
                                else if (split[1].ToLower().Trim() == "built") cmbGrief1.SelectedIndex = 1;
                                else { Server.s.Log("failed to load: op-notify, setting to default."); cmbGrief1.SelectedIndex = 0; break; }
                                try { cmbGriefTimes.SelectedIndex = (int.Parse(split[2].Trim()) - 1); }
                                catch { Server.s.Log("failed to load: op-notify, setting to default."); cmbGriefTimes.SelectedIndex = 5; break; }
                                if (split[3].ToLower().Trim() == "destroyed") cmbGrief2.SelectedIndex = 0;
                                else if (split[3].ToLower().Trim() == "built") cmbGrief2.SelectedIndex = 1;
                                else { Server.s.Log("failed to load: op-notify, setting to default."); cmbGrief2.SelectedIndex = 1; break; }
                                break;
                            case "max-blocks":
                                try { txtSpamBlockCount.Text = value; }
                                catch { Server.s.Log("failed to load: max-blocks, setting to default."); txtSpamBlockCount.Text = Player.spamBlockCount.ToString(); }
                                break;
                            case "block-time":
                                try { txtSpamBlockTimer.Text = value; }
                                catch { Server.s.Log("failed to load: block-time, setting to default."); txtSpamBlockTimer.Text = Player.spamBlockTimer.ToString(); }
                                break;
                            case "extra-ban":
                                chkExtraban.Checked = value.ToLower() == "true";
                                break;
                            case "check-update":
                                chkUpdate.Checked = value.ToLower() == "true";
                                break;
                            case "check-time":
                                if (value == "1") cmbChkHour.SelectedIndex = 0;
                                else if (value == "2") cmbChkHour.SelectedIndex = 1;
                                else if (value == "4") cmbChkHour.SelectedIndex = 2;
                                else if (value == "8") cmbChkHour.SelectedIndex = 3;
                                else if (value == "16") cmbChkHour.SelectedIndex = 4;
                                else if (value == "24") cmbChkHour.SelectedIndex = 5;
                                else if (value == "48") cmbChkHour.SelectedIndex = 6;
                                else Server.s.Log("failed to load: check-time, setting to default.");
                                break;
                            case "force-update":
                                chkForceUpdate.Checked = value.ToLower() == "true";
                                break;
                            case "update-time":
                                if (value == "10") cmbChkHour.SelectedIndex = 0;
                                else if (value == "60") cmbChkHour.SelectedIndex = 1;
                                else if (value == "900") cmbChkHour.SelectedIndex = 2;
                                else if (value == "1800") cmbChkHour.SelectedIndex = 3;
                                else if (value == "2700") cmbChkHour.SelectedIndex = 4;
                                else if (value == "3600") cmbChkHour.SelectedIndex = 5;
                                else Server.s.Log("failed to load: update-time, setting to default.");
                                break;
                            case "achievements":
                                chkAchievements.Checked = value.ToLower() == "true";
                                break;
                            case "check-port":
                                chkPortforward.Checked = value.ToLower() == "true";
                                break;
                            case "help-port":
                                chkHelpPortforward.Checked = value.ToLower() == "true";
                                break;
                            case "restart-on-error":
                                chkRestartOnError.Checked = value.ToLower() == "true";
                                break;
                            case "newlvl-help":
                                chkNewlvlHelp.Checked = value.ToLower() == "true";
                                break;
                        }
                    }
                }
            }
            rtxtCMDRanks.Text = "Available ranks are:" + Environment.NewLine;
            Group.GroupList.ForEach((grp) => {
                rtxtCMDRanks.Text += Environment.NewLine + grp.name + ((grp.name.Length >= 16) ? "\t" : (grp.name.Length <= 8) ? "\t\t" : "\t") + "(" + grp.Permission + ")";
            });
            rtxtBLKRanks.Text = rtxtCMDRanks.Text;
        }

        public void Save(string givenPath)
        {
            try
            {
                StreamWriter w = new StreamWriter(File.Create("properties/" + givenPath + ".properties"));
                if (givenPath == "server")
                {
                    w.WriteLine("# Edit the settings below to modify how your server operates. This is an explanation of what each setting does.");
                    w.WriteLine("#   the-owner\t\t=\tThe owner of the server (has to be set)");
                    w.WriteLine("#   server-name\t\t=\tThe name which displays on minecraft.net");
                    w.WriteLine("#   console-state\t=\tThe name of the console");
                    w.WriteLine("#   motd\t\t=\tThe message which displays when a player connects");
                    w.WriteLine("#   port\t\t=\tThe port to operate from");
                    w.WriteLine("#   verify-names\t=\tVerify the validity of names");
                    w.WriteLine("#   public\t\t=\tSet to true to appear in the public server list");
                    w.WriteLine("#   max-players\t\t=\tThe maximum number of connections");
                    w.WriteLine("#   main-level\t\t=\tWhat should the name of the servers main level be?");
                    w.WriteLine("#   max-maps\t\t=\tThe maximum number of maps loaded at once");
                    w.WriteLine("#   global-chat\t\t=\tSet to true to enable global chat");
                    w.WriteLine("#   guest-goto\t\t=\tSet to true to give guests goto and levels commands");
                    w.WriteLine("#   restart-on-error\t=\tThis setting determines whether the server should restart on errors, or not");
                    w.WriteLine("#   ");
                    w.WriteLine("#   irc\t\t\t=\tSet to true to enable the IRC bot");
                    w.WriteLine("#   irc-nick\t\t=\tThe name of the IRC bot");
                    w.WriteLine("#   irc-server\t\t=\tThe server to connect to");
                    w.WriteLine("#   irc-channel\t\t=\tThe channel to join");
                    w.WriteLine("#   irc-port\t\t=\tThe port to use to connect");
                    w.WriteLine("#   irc-identify\t=\t(true/false)Do you want the IRC bot to Identify itself with nickserv. Note: You will need to register it's name with nickserv manually.");
                    w.WriteLine("#   irc-password\t=\tThe password you want to use if you're identifying with nickserv");
                    w.WriteLine("#   ");
                    w.WriteLine("#   backup-time\t\t=\tThe number of seconds between automatic backups");
                    w.WriteLine("#   ");
                    w.WriteLine("#   admin-promote\t=\tAllows Admins to promote others to Admins");
                    w.WriteLine("#   op-promote\t\t=\tAllows Operators to promote others to Operators");
                    w.WriteLine("#   afk-minutes\t\t=\tHow many minutes you have to not be moving before you will be auto-set to be -afk-");
                    w.WriteLine("#   afk-kick\t\t=\tHow many minutes you have to be not moving before you will be kicked");
                    w.WriteLine("#   owner-login\t\t=\tTells the server what it should say instead of \"has joined the game\" (empty for default)");
                    w.WriteLine("#   newlvl-help\t\t=\tDetermines wheather /newlvl should help you create your levels, by shrinking/enlarging them");
                    w.WriteLine("#   ");
                    w.WriteLine("#   check-port\t\t=\tShould MCRevive try to check if your ports are forwarded? (if you are able to host)");
                    w.WriteLine("#   help-port\t\t=\tShould MCRevive try to portforward if the ports are not forwarded? (theres no garantee this will work)");
                    w.WriteLine("#   default-color\t=\tSet to \"& + number/letter\" to get that as the default text color (use \"/help color\" in game, to see the options of number/letter)");
                    w.WriteLine("#   geolocator\t\t=\tThis will tell the server weather it should track joining players or not");
                    w.WriteLine("#   log\t\t\t=\tTells the server whether it should save the Log or not");
                    w.WriteLine("#   old-help\t\t=\tDetermines whether the help command should be shown the old, or the new way");
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine("# Server Options:");
                    w.WriteLine("the-owner = " + txtServerOwner.Text);
                    w.WriteLine("server-name = " + txtServerName.Text);
                    w.WriteLine("console-state = " + Server.consolename);
                    w.WriteLine("motd = " + txtServerMOTD.Text);
                    w.WriteLine("port = " + txtServerPort.Text);
                    w.WriteLine("verify-names = " + chkVerifyNames.Checked);
                    w.WriteLine("public = " + chkPublic.Checked);
                    w.WriteLine("max-players = " + txtServerMaxPlayers.Text);
                    w.WriteLine("main-level = " + txtMainLevel.Text);
                    w.WriteLine("max-maps = " + txtServerMaxMaps.Text);
                    w.WriteLine("global-chat = " + chkWorldChat.Checked);
                    w.WriteLine("guest-goto = " + chkGuestGoto.Checked);
                    w.WriteLine("restart-on-error = " + chkRestartOnError.Checked);
                    w.WriteLine();
                    w.WriteLine("# IRC Bot Options:");
                    w.WriteLine("irc = " + chkIRC.Checked);
                    w.WriteLine("irc-nick = " + txtIRCNick.Text);
                    w.WriteLine("irc-server = " + txtIRCServer.Text);
                    w.WriteLine("irc-channel = " + txtIRCChannel.Text);
                    w.WriteLine("irc-port = " + txtIRCPort.Text);
                    w.WriteLine("irc-identify = " + chkIRCIdentify.Checked.ToString());
                    w.WriteLine("irc-password = " + txtIRCPassword.Text);
                    w.WriteLine();
                    w.WriteLine("# Antigrief Stuff:");
                    w.WriteLine("anti-tunnels = " + ChkTunnels.Checked);
                    w.WriteLine("max-depth = " + txtMaxDepth.Text);
                    w.WriteLine("overload = " + txtServerOverload.Text);
                    w.WriteLine();
                    w.WriteLine("# Backup Options:");
                    w.WriteLine("backup-time = 150");
                    w.WriteLine();
                    w.WriteLine("# Player Options:");
                    w.WriteLine("admin-promote = " + chkAdmintoAdmin.Checked);
                    w.WriteLine("op-promote = " + chkOptoOp.Checked);
                    w.WriteLine("afk-minutes = " + txtAFKTimer.Text);
                    w.WriteLine("afk-kick = " + txtAFKKick.Text);
                    w.WriteLine("owner-login = " + txtServerOwnerLogin.Text.Trim());
                    w.WriteLine("newlvl-help = " + chkNewlvlHelp.Checked);
                    w.WriteLine();
                    w.WriteLine("# Other:");
                    w.WriteLine("check-port = " + chkPortforward.Checked);
                    w.WriteLine("help-port = " + chkHelpPortforward.Checked);
                    w.WriteLine("default-color = " + (cmbDefaultColor.SelectedIndex != -1 ? cmbDefaultColor.SelectedItem.ToString() : Server.DefaultColor));
                    w.WriteLine("geolocator = " + chkGeolocation.Checked);
                    w.WriteLine("log = " + Server.reportBack);
                    w.WriteLine("old-help = " + chkHelp.Checked.ToString());
                }
                else if (givenPath == "gui")
                {
                    w.WriteLine("# Edit the settings below to modify how your server operates. This is an explanation of what each setting does.");
                    w.WriteLine("#   usingCLI\t\t=\tTells the program wether you use the CLI or the Gui");
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine("# CLI / Gui?");
                    w.WriteLine("usingCLI = False");
                }
                else if (givenPath == "antigrief")
                {
                    w.WriteLine("# Edit the settings below to modify how your server operates. This is an explanation of what each setting does.");
                    w.WriteLine("#   anti-tunnels\t=\tStops people digging below max-depth");
                    w.WriteLine("#   max-depth\t\t=\tThe maximum allowed depth to dig down");
                    w.WriteLine("#   overload\t\t=\tThe higher this is, the longer the physics is allowed to lag. Default 1500");
                    w.WriteLine("#   ");
                    w.WriteLine("#   anti-grief-system =\tUsing Anti-Grief-System?");
                    w.WriteLine("#   op-notify\t\t=\tTells the server whether it should tell op+ about suspected griefers");
                    w.WriteLine("#   max-blocks\t\t=\tHow many blocks modified on block-time before kick");
                    w.WriteLine("#   block-time\t\t=\tHow many seconds you have to modify max-blocks before kick");
                    w.WriteLine("#   ");
                    w.WriteLine("#   extra-ban\t\t=\tDetermines whether extra bans should be readen (all MCRevive servers)");
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine("# Antigrief Stuff:");
                    w.WriteLine("anti-tunnels = " + ChkTunnels.Checked);
                    w.WriteLine("max-depth = " + txtMaxDepth.Text);
                    w.WriteLine("overload = " + txtServerOverload.Text);
                    w.WriteLine();
                    w.WriteLine("# Anti Grief System:");
                    w.WriteLine("anti-grief-system = " + chkAntiGrief.Checked);
                    w.WriteLine("op-notify = " + chkOpNotify.Checked + " : " + cmbGrief1.Items[cmbGrief1.SelectedIndex] + " : "
                        + cmbGriefTimes.Items[cmbGriefTimes.SelectedIndex] + " : " + cmbGrief2.Items[cmbGrief2.SelectedIndex]);
                    w.WriteLine("max-blocks = " + txtSpamBlockCount.Text);
                    w.WriteLine("block-time = " + txtSpamBlockTimer.Text);
                    w.WriteLine();
                    w.WriteLine("# Other:");
                    w.WriteLine("extra-ban = " + chkExtraban.Checked);
                }
                else if (givenPath == "update")
                {
                    w.WriteLine("#   check-update\t=\tTells the server wether it should check for updates or not");
                    w.WriteLine("#   ");
                    w.WriteLine("#   force-update\t=\tDetermines whether the server auto updates as soon as an update is out");
                    w.WriteLine("#   update-time\t\t=\tThe time in seconds before force-update takes action");
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine("# Check?");
                    w.WriteLine("check-update = " + chkUpdate.Checked);
                    w.WriteLine("check-time = " + ((cmbChkHour.SelectedIndex == 0) ? "1" : (cmbChkHour.SelectedIndex == 1) ? "2"
                        : (cmbChkHour.SelectedIndex == 2) ? "4" : (cmbChkHour.SelectedIndex == 3) ? "8" : (cmbChkHour.SelectedIndex == 4)
                        ? "16" : (cmbChkHour.SelectedIndex == 5) ? "24" : (cmbChkHour.SelectedIndex == 6) ? "48" : "fail"));
                    w.WriteLine();
                    w.WriteLine("# Update Settings:");
                    w.WriteLine("force-update = " + chkForceUpdate.Checked);
                    w.WriteLine("update-time = " + ((cmbChkHour.SelectedIndex == 0) ? "10" : (cmbChkHour.SelectedIndex == 1) ? "60" :
                        (cmbChkHour.SelectedIndex == 2) ? "900" : (cmbChkHour.SelectedIndex == 3) ? "1800" : (cmbChkHour.SelectedIndex == 4)
                        ? "2700" : (cmbChkHour.SelectedIndex == 5) ? "3600" : "fail"));
                }
                else if (givenPath == "achievements")
                {
                    w.WriteLine("# This is a list of all the achievements in MCRevive");
                    w.WriteLine("# You may edit the settings below for your own desire");
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine("# Achievements?");
                    w.WriteLine("achievements = " + chkAchievements.Checked);
                    w.WriteLine();
                    w.WriteLine("# Achievements:");
                    Server.allAchievements.ForEach((ds) =>
                    {
                        w.WriteLine(ds.filename + " = " + Server.allAchievements.Find(a => a.filename == ds.filename).allowed);
                    });
                }
                w.Flush();
                w.Close();
                w.Dispose();
            }
            catch
            {
                Server.s.Log("SAVE FAILED! properties/" + givenPath + ".properties");
            }
        }

        #endregion
        #region Changes

        public static void removeDigit(KeyEventArgs e)
        {
            if (!char.IsDigit((char)e.KeyValue) && e.KeyCode != Keys.NumPad1 && e.KeyCode != Keys.NumPad2 && e.KeyCode != Keys.NumPad3 && e.KeyCode != Keys.NumPad4 && 
                e.KeyCode != Keys.NumPad5 && e.KeyCode != Keys.NumPad6 && e.KeyCode != Keys.NumPad7 && e.KeyCode != Keys.NumPad8 && e.KeyCode != Keys.NumPad9 &&
                e.KeyCode != Keys.Back && e.KeyCode != Keys.Delete && e.KeyCode != Keys.OemMinus && (char)e.KeyValue != '-') { e.SuppressKeyPress = true; }
        }

        private void txtServerPort_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtServerMaxPlayers_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtServerMaxMaps_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtBackup_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtIRCPort_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtAFKTimer_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtAFKKick_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtSpamBlockCount_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtSpamBlockTimer_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtMaxDepth_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtServerOverload_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtRankPermission_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtRankMaxBlocks_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtCMDLowest_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }
        private void txtBLKLowest_KeyDown(object sender, KeyEventArgs e) { removeDigit(e); }

        private void cmbDefaultColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDefaultColor.BackColor = Color.FromName(cmbDefaultColor.SelectedItem.ToString());
        }

        bool AchievementWindowUse = false;
        bool chkach = false;
        private void chkAchievements_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAchievements.Checked && chkach)
            {
                tt.SetToolTip(this.chkAchievements, "Should there be achievements on this server?\n\nClick twice to choose which achievements should be allowed on the server.");
                if (!AchievementWindowUse) { AchievementWindowUse = true; }
                AchForm.Show();
                AchForm.Top = this.Top + this.Height / 2 - AchForm.Height / 2;
                AchForm.Left = this.Left + this.Width / 2 - AchForm.Width / 2;
            }
            else
                chkach = true;
        }
        #endregion
        #region tabIRC
        private void chkIRCShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            txtIRCPassword.UseSystemPasswordChar = !chkIRCShowPassword.Checked;
        }
        #endregion
        #region tabRanks
        bool cancelContextRankPeople = false;

        private void UpdateRankList()
        {
            Group.saveGroups(Group.GroupList);
            Group.InitAll();
            Command.InitAll();
            GrpCommands.fillRanks();
            Block.SetBlocks();

            object sel = liRanks.SelectedItem ?? Group.GroupList[0].name;
            liRanks.Items.Clear();
            otherToolStripMenuItem.DropDownItems.Clear();

            for (int i = 0; i < Group.GroupList.Count - 1; ++i)
            {
                liRanks.Items.Add(Group.GroupList[i].name);
                otherToolStripMenuItem.DropDownItems.Add(Char.ToUpper(Group.GroupList[i].name[0]) + Group.GroupList[i].name.Substring(1).ToLower());
                otherToolStripMenuItem.DropDownItems[i].Click += new EventHandler(itmRankItem_Click);
            }

            if (liRanks.Items.Contains(sel))
                liRanks.SelectedItem = sel;
        }
        private void reloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateRankList();
        }

        private void liRanks_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (liRanks.SelectedIndex != -1)
            {
                Group g = Group.Find(liRanks.SelectedItem.ToString());
                txtRankName.Text = g.name;
                txtRankPermission.Text = g.Permission + "";
                txtRankMaxBlocks.Text = g.maxBlocks + "";
                txtRankPeople.Text = g.playerList.All().Count + "";
                cmbRankColor.SelectedIndex = cmbRankColor.Items.IndexOf(c.Name(g.color));
                liRankPeople.Items.Clear();
                Thread t = new Thread(new ThreadStart(delegate { g.playerList.All().ForEach((who) => { liRankPeople.Items.Add(who); }); }));
                promoteToolStripMenuItem.Enabled = (liRanks.SelectedIndex != liRanks.Items.Count - 1);
                demoteToolStripMenuItem.Enabled = (liRanks.SelectedIndex != 0);
                t.Start();
            }
        }

        private void menuRankPeople_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelContextRankPeople) e.Cancel = true;
        }
        private void liRankPeople_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int index = liRankPeople.IndexFromPoint(new Point(e.X, e.Y));
                if (index != -1) { liRankPeople.SelectedIndex = index; cancelContextRankPeople = false; }
                else cancelContextRankPeople = true;
            }
        }
        private void liRankPeople_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int index = liRankPeople.IndexFromPoint(new Point(e.X, e.Y));
                if (index != -1) { liRankPeople.SelectedIndex = index; cancelContextRankPeople = false; }
                else cancelContextRankPeople = true;
            }
        }

        private void itmRankItem_Click(object sender, EventArgs e)
        {
            Command.all.Find("setrank").Use(null, liRankPeople.SelectedItem + " " + sender);
            UpdateRankList();
        }
        private void promoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Command.all.Find("setrank").Use(null, liRankPeople.SelectedItem + " " + liRanks.Items[liRanks.SelectedIndex + 1]);
            UpdateRankList();
        }
        private void demoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Command.all.Find("setrank").Use(null, liRankPeople.SelectedItem + " " + liRanks.Items[liRanks.SelectedIndex - 1]);
            UpdateRankList();
        }

        private void cmbRankColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblRankColor.BackColor = Color.FromName(cmbRankColor.SelectedItem.ToString());
        }

        private void btnRankNew_Click(object sender, EventArgs e)
        {
            if (liRanks.Items.Contains(txtRankName.Text)) { MessageBox.Show("There already is a rank with this name!"); return; }
            Group.GroupList.ForEach((gr) =>
            {
                if (gr.Permission + "" == txtRankPermission.Text)
                {
                    MessageBox.Show("There already is a rank with this Permission Level");
                    return;
                }
            });
            Group grp = new Group(int.Parse(txtRankPermission.Text), int.Parse(txtRankMaxBlocks.Text), txtRankName.Text, c.Parse(cmbRankColor.SelectedItem.ToString()).ToCharArray()[1]);
            Group.GroupList.Add(grp);
            UpdateRankList();
        }

        private void btnRankSave_Click(object sender, EventArgs e)
        {
            if (liRanks.SelectedIndex == -1 || (!liRanks.Items.Contains(txtRankName.Text) && Group.Find(liRanks.SelectedItem.ToString()).Permission + "" != txtRankPermission.Text)) { btnRankNew_Click("", null); return; }
            Group grp = Group.Find(txtRankName.Text);
            if (grp == null) grp = Group.Find(txtRankPermission.Text);
            if (grp == null) { btnRankNew_Click("", null); return; }
            grp.name = txtRankName.Text;
            grp.maxBlocks = int.Parse(txtRankMaxBlocks.Text);
            grp.Permission = int.Parse(txtRankPermission.Text);
            grp.color = c.Parse(cmbRankColor.SelectedItem.ToString());
            UpdateRankList();
        }

        private void btnRankRemove_Click(object sender, EventArgs e)
        {
            Group g = Group.Find(liRanks.SelectedItem.ToString());
            if (g == null) { liRanks.Items.RemoveAt(liRanks.SelectedIndex); return; }
            Group.GroupList.Remove(g);
            Group.saveGroups(Group.GroupList);
        }
        #endregion
        #region tabUpdating

        public void LoadRevisions()
        {
            liUpdate.Items.Clear();
            string[] RList = new string[0];
            try
            {
                RList = Server.DownloadString("http://www.mcrevive.tk/download/_Revlist.php").Split(':');
                foreach (string R in RList)
                {
                    liUpdate.Items.Insert(0, R);
                }
                liUpdate.SelectedIndex = 0;
            }
            catch { }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (liUpdate.SelectedIndex >= 0)
            {
                try
                {
                    WebClient client = new WebClient();
                    client.DownloadFile("http://www.mcrevive.tk/download/MCRevive_GUI-" + liUpdate.SelectedItem + ".exe", "memory/MCRevive.exe");
                    client.DownloadFile("http://www.mcrevive.tk/download/MCRevive-" + liUpdate.SelectedItem + ".dll", "memory/MCRevive_.dll");
                }
                catch { Server.s.Log("Failed to download files, check your internet!"); return; }

                try
                {
                    switch (MessageBox.Show("Server will restart upon update." + Environment.NewLine + "Continue?", btnUpdate.Text,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                    {
                        case System.Windows.Forms.DialogResult.Yes:
                            Server.Restart();
                            break;
                    }

                }
                catch { MessageBox.Show("FAILED." + Environment.NewLine + "Copy the files in \"memory\""); }
            }
            else { MessageBox.Show("You must select an update, before you can install it"); }
        }
        #endregion
        #region tabCommands
        int lastCmd = -1;

        private void liCommands_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lastCmd != -1)
            {
                GrpCommands.rankAllowance rA = new GrpCommands.rankAllowance();
                rA.rank = int.Parse(txtCMDLowest.Text);
                rA.commandName = commands[lastCmd].commandName;
                commands[commands.IndexOf(commands.Find(c => c.commandName == rA.commandName))] = rA;
            }
            if (liCommands.SelectedIndex != -1) txtCMDLowest.Text = commands.Find(c => c.commandName == liCommands.SelectedItem.ToString()).rank.ToString();

            lastCmd = liCommands.SelectedIndex;
        }

        private void txtCMDLowest_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (int.Parse(txtCMDLowest.Text) > 150) txtCMDLowest.Text = "150";
                if (int.Parse(txtCMDLowest.Text) < -20) txtCMDLowest.Text = "-20";
            }
            catch { }
        }

        public void LoadCommands()
        {
            try
            {
                liCommands.Items.Clear();
                commands.Clear();
                GrpCommands.allowedCommands.ForEach((rA) =>
                {
                    if (!liCommands.Items.Contains(rA.commandName))
                    {
                        liCommands.Items.Add(rA.commandName);
                        commands.Add(rA);
                    }
                });
                if (liCommands.SelectedIndex == -1)
                    liCommands.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Server.ErrorLog(ex);
            }
        }
        public void SaveCommands()
        {
            GrpCommands.Save(commands);
            GrpCommands.fillRanks();
            LoadCommands();
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            Player.storedHelp = "";
            Player.storeHelp = true;
            Command.all.Find("help").Use(null, liCommands.Items[liCommands.SelectedIndex].ToString());
            Player.storeHelp = false;
            string messageInfo = "Help information for " + liCommands.Items[liCommands.SelectedIndex].ToString() + ":"
                + Environment.NewLine + Environment.NewLine + Player.storedHelp;
            MessageBox.Show(c.Remove(messageInfo));
        }
        #endregion
        #region tabBlocks

        int lastBlk = -1;

        private void liBlocks_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lastBlk != -1)
            {
                Block.Blocks bl = new Block.Blocks();
                bl.rank = int.Parse(txtCMDLowest.Text);
                bl.type = blocks[lastBlk].type;
                blocks[blocks.IndexOf(blocks.Find(b => b.type == bl.type))] = bl;
            }
            if (liBlocks.SelectedIndex != -1) txtBLKLowest.Text = blocks.Find(b => Block.Name(b.type) == liBlocks.SelectedItem.ToString()).rank.ToString();

            lastBlk = liBlocks.SelectedIndex;
        }

        private void txtBLKLowest_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (int.Parse(txtBLKLowest.Text) > 150) txtBLKLowest.Text = "150";
                if (int.Parse(txtBLKLowest.Text) < -20) txtBLKLowest.Text = "-20";
            }
            catch { }
        }

        public void LoadBlocks()
        {
            try
            {
                liBlocks.Items.Clear();
                blocks.Clear();
                Block.BlockList.ForEach((bl) =>
                {
                    if (!liBlocks.Items.Contains(Block.Name(bl.type)))
                    {
                        liBlocks.Items.Add(Block.Name(bl.type));
                        blocks.Add(bl);
                    }
                }); 
                if (liBlocks.SelectedIndex == -1)
                    liBlocks.SelectedIndex = 0;
            }
            catch (Exception ex) { Server.ErrorLog(ex); }
        }

        public void SaveBlocks()
        {
            Block.SaveBlocks(blocks);
            Block.SetBlocks();
            LoadBlocks();
        }

        #endregion
        #region underBar
        bool close = true;
        private void btnSave_Click(object sender, EventArgs e) { saveStuff(); if (close) this.Close(); else { close = true; saved = false; } }
        private void btnDiscard_Click(object sender, EventArgs e) { this.Close(); }
        private void btnApply_Click(object sender, EventArgs e) { saveStuff(); saved = false; }

        private void saveStuff()
        {
            if (txtServerOwner.Text == "Your Name Here") txtServerOwner.Text = "";
            if (!Player.checkDevS(txtServerOwner.Text) && txtServerOwnerLogin.Text.ToLower().Contains("developer"))
            {
                txtServerOwnerLogin.Text = "";
                MessageBox.Show("i can't let you type about Devs :P");
            }
            foreach (Control tP in tabControl.Controls)
                if (tP is TabPage)
                    foreach (Control ctrl in tP.Controls)
                        if (ctrl is TextBox)
                            if (ctrl.Text.Trim() == "")
                            {
                                MessageBox.Show("A textbox has been left empty. It must be filled." + Environment.NewLine + ctrl.Name);
                                close = false;
                                return;
                            }
            Save("achievements");
            Save("server");
            Save("gui");
            Save("antigrief");
            Save("update");
            SaveCommands();
            SaveBlocks();
            File.WriteAllText("text/description.txt", txtServerDescription.Text);
            MCRevive.Properties.Load("server");
            MCRevive.Properties.Load("antigrief");
            MCRevive.Properties.Load("update");
            Group.InitAll();
            Command.InitAll();
            GrpCommands.fillRanks();
            Block.SetBlocks();

            saved = true;
            Server.s.Log("Properties saved, and loaded!");
        }
        #endregion
    }
}
