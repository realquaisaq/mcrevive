﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MCRevive.Gui
{
    public static class Routines
    {
        public static List<Routine> routines = new List<Routine>();
        public static Routine getRoutine(string name)
        {
            Routine a = null;
            routines.ForEach((r) => { if (r.name == name) a = r; });
            return a;
        }
        public class Action
        {
            public string action = "";
            public string info = "";
            public string getString()
            {
                return action + " - " + info;
            }
            public Action(string action, string info)
            {
                //window.asdfMovies(action+" "+info);
                this.action = action;
                this.info = info;
            }
        }
        public class Routine
        {
            public string name = "";
            public int intervalSecs = 0;
            public List<Action> actions = new List<Action>();
            public Routine(string name, int interval)
            {
                this.intervalSecs = interval;
                this.name = name;
                Program.window.addTimerForRoutine(this);
            }
        }
        public static void save()
        {
            if (!Directory.Exists("routines")) Directory.CreateDirectory("routines");
            routines.ForEach((r) =>
            {
                string path = "routines\\" + r.name + ".txt";
                if (File.Exists(path)) File.Delete(path);
                File.Create(path).Close();
                File.WriteAllText(path, r.intervalSecs + Environment.NewLine);
                r.actions.ForEach((a) => { File.AppendAllText(path, a.getString() + Environment.NewLine); });
            });
        }
        public static void load()
        {
            int rNum = 0, aNum = 0;
            if (Directory.Exists("routines"))
            {
                foreach (string f in Directory.GetFiles("routines"))
                {
                    string[] lines = File.ReadAllLines(f);
                    //window.asdfMovies(f);
                    Routine r = new Routine(f.Replace(".txt", "").Replace("routines\\",""), Int32.Parse(lines[0]));
                    routines.Add(r);
                    for (int i = 1; i < lines.Length; i++)
                    {
                        //window.asdfMovies(i.ToString());
                        r.actions.Add(new Action(lines[i].Split('-')[0].TrimEnd(),lines[i].Split('-')[1].TrimStart()));
                        aNum++;
                    }
                    rNum++;
                }
            }
            Program.window.rLog(rNum + " routines totalling " + aNum + " actions were loaded.");
        }
    }
}