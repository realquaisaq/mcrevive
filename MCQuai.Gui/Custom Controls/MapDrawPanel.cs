﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace MCRevive.Gui
{
    public partial class MapDrawPanel : Panel
    {
        public static Color backColor = Color.CornflowerBlue;
        private Point StartMove = new Point(-1337, -1337);
        private Point ImageAnchor = new Point(0, 0);
        private bool ImageMove = false;
        public bool Cancel = false;
        public float ZoomLevel = 1;
        private Bitmap Image = null;
        private Size PosStringSize = Size.Empty;

        public delegate void ProgressEventHandler(byte progress);

        public MapDrawPanel()
        {
            InitializeComponent();
            this.BackColor = backColor;
        }

        #region Used Events
        private void _MouseDown(object sender, MouseEventArgs e)
        {
            StartMove = new Point(e.X, e.Y);
            ImageMove = true;
        }
        private void _MouseMove(object sender, MouseEventArgs e)
        {
            if (ImageMove && Image != null)
            {
                ImageAnchor.X += e.X - StartMove.X;
                ImageAnchor.Y += e.Y - StartMove.Y;
                DrawMap(ImageAnchor.X - e.X + StartMove.X, ImageAnchor.Y - e.Y + StartMove.Y);
                StartMove = new Point(e.X, e.Y);
            }
        }
        private void _MouseUp(object sender, MouseEventArgs e)
        {
            if (ImageMove && Image != null)
            {
                ImageAnchor.X += e.X - StartMove.X;
                ImageAnchor.Y += e.Y - StartMove.Y;
                DrawMap(ImageAnchor.X - e.X + StartMove.X, ImageAnchor.Y - e.Y + StartMove.Y);
            }
            ImageMove = false;
        }
        private void _Paint(object sender, PaintEventArgs e)
        {
            DrawMap(ImageAnchor.X, ImageAnchor.Y);
        }
        #endregion
        #region Functions
        public void DrawMap(string name, bool force = false)
        {
            if (!force && Directory.Exists("levels/images") && File.Exists("levels/images/" + name + ".bmp"))
            {
                Bitmap img = (Bitmap)Bitmap.FromFile("levels/images/" + name + ".bmp");
                Image = new Bitmap(img);
                img.Dispose();
                img = null;
                ImageAnchor = new Point((int)Math.Round(this.Width / 2F - Image.Width * ZoomLevel / 2F), (int)Math.Round(this.Height / 2F - Image.Height * ZoomLevel / 2F));
                DrawMap(ImageAnchor.X, ImageAnchor.Y);
            }
            else
            {
                Level l = Level.Find(name);
                if (l != null)
                    DrawMap(l.blocks, l.width, l.depth, l.height, l.name);
                else
                    return;
            }
        }
        public void DrawMap(int oldx, int oldy)
        {
            if (Image == null) return;
            Image img = new Bitmap(this.Width, this.Height);
            Graphics g = Graphics.FromImage(img);
            g.Clear(backColor);

            if (ZoomLevel == (int)ZoomLevel)
                g.InterpolationMode = InterpolationMode.NearestNeighbor;
            else if (ZoomLevel < 1)
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(Image, ImageAnchor.X, ImageAnchor.Y, Image.Width * ZoomLevel, Image.Height * ZoomLevel);
            if (ZoomLevel < 1)
            {
                g.DrawRectangle(new Pen(new SolidBrush(backColor)), ImageAnchor.X, ImageAnchor.Y, (float)Math.Floor(Image.Width * ZoomLevel), (float)Math.Floor(Image.Height * ZoomLevel));
                g.DrawRectangle(new Pen(new SolidBrush(backColor)), ImageAnchor.X + 1, ImageAnchor.Y + 1, (float)Math.Floor(Image.Width * ZoomLevel) - 2, (float)Math.Floor(Image.Height * ZoomLevel) - 2);
            }

            //g.FillRectangle(    //up
            //    new SolidBrush(backColor),
            //    Math.Max(oldx, ImageAnchor.X),
            //    Math.Min(oldy, ImageAnchor.Y),
            //    Math.Abs(oldx - ImageAnchor.X) + Image.Width * ZoomLevel,
            //    Math.Abs(oldy - ImageAnchor.Y));
            //g.FillRectangle(    //right
            //    new SolidBrush(backColor),
            //    Math.Min(oldx, ImageAnchor.X) + Image.Width * ZoomLevel,
            //    Math.Max(oldy, ImageAnchor.Y),
            //    Math.Abs(oldx - ImageAnchor.X),
            //    Math.Abs(oldy - ImageAnchor.Y) + Image.Height * ZoomLevel);
            //g.FillRectangle(    //down
            //    new SolidBrush(backColor),
            //    Math.Min(oldx, ImageAnchor.X),
            //    Math.Min(oldy, ImageAnchor.Y) + Image.Height * ZoomLevel,
            //    Math.Abs(oldx - ImageAnchor.X) + Image.Width * ZoomLevel,
            //    Math.Abs(oldy - ImageAnchor.Y));
            //g.FillRectangle(    //left
            //    new SolidBrush(backColor),
            //    Math.Min(oldx, ImageAnchor.X),
            //    Math.Min(oldy, ImageAnchor.Y),
            //    Math.Abs(oldx - ImageAnchor.X),
            //    Math.Abs(oldy - ImageAnchor.Y) + Image.Height * ZoomLevel);

            g.DrawString(Math.Round(Width / 2F - Image.Width * ZoomLevel / 2F - ImageAnchor.X) + ", " + Math.Round(Height / 2F - Image.Height * ZoomLevel / 2F - ImageAnchor.Y), new Font(FontFamily.Families[0], 14, FontStyle.Bold), new SolidBrush(Color.Yellow), 5, 5);
            PosStringSize = g.MeasureString(Math.Round(Width / 2F - Image.Width * ZoomLevel / 2F - ImageAnchor.X) + ", " + Math.Round(Height / 2F - Image.Height * ZoomLevel / 2F - ImageAnchor.Y), new Font(FontFamily.Families[0], 14, FontStyle.Bold)).ToSize();
                
            g.Dispose();

            Graphics gr = this.CreateGraphics();
            gr.DrawImage(img, 0, 0, this.Width, this.Height);
            gr.Dispose();
            img.Dispose();
        }
        public bool ChangeZoom(float zoom)
        {
            if (Image == null) return false;

            ImageAnchor.X = (int)Math.Ceiling(this.Width * 0.5D - (this.Width * 0.5D - ImageAnchor.X) * zoom / ZoomLevel);
            ImageAnchor.Y = (int)Math.Ceiling(this.Height * 0.5D - (this.Height * 0.5D - ImageAnchor.Y) * zoom / ZoomLevel);

            ZoomLevel = zoom;
            DrawMap(ImageAnchor.X, ImageAnchor.Y);
            return true;
        }
        public void DrawMap(ushort[] blocks, ushort width, ushort depth, ushort height, string name)
        {
            if (!Directory.Exists("levels/images")) Directory.CreateDirectory("levels/images");
            Thread t = new Thread(new ThreadStart(delegate
            {
                System.Diagnostics.Stopwatch st = new System.Diagnostics.Stopwatch();
                st.Start();
                Progress(0);
                Bitmap bmp = new Bitmap(width + height + 1, width + height + depth + 1);
                Graphics abc = Graphics.FromImage(bmp);
                abc.Clear(backColor);
                abc.Dispose();
                
                bool[,] taken = InitTaken(bmp);
                
                int i = 0;
                for (ushort y = depth; y > 0; --y)
                {
                    y--;
                    for (ushort x = width; x > 0; --x)
                    {
                        x--;
                        for (ushort z = 0; z < height; ++z, i++)
                            if (blocks[i] != 0)
                            {
                                int posx = width - x + z;
                                int posy = width + height + 1 - z - x + y;

                                Color c = Block.GetColor(blocks[i]);
                                if (c != Color.Transparent /*&& bmp.GetPixel(posx, posy) != c*/) //takes too much time, faster without
                                {
                                    bmp.SetPixel(posx, posy, c);
                                    bmp.SetPixel(posx, posy - 1, c);
                                }
                            }
                        x++;
                    }
                    if (y % 5 == 0)
                    {
                        if (Cancel)
                            break;
                        Progress((byte)Math.Floor(100 - 100 * y / (float)depth));
                    }
                    y++;
                }
                /*for (ushort y = (ushort)(height - 1); y >= 0 && y != 65535; --y)
                {
                    for (ushort x = 0; x < width; ++x)
                    {
                        for (ushort z = 0; z < depth; ++z)
                        {
                            int posx = width - x + z - 1;
                            int posy = width + depth + height - z - x - y - 2;

                            if (!taken[posx, posy])
                            {
                                Color c = Block.GetColor(blocks[x + z * width + y * width * height]);
                                if (c != Color.Transparent)
                                {
                                    taken[posx, posy] = true;
                                    taken[posx, posy - 1] = true;
                                    bmp.SetPixel(posx, posy, c);
                                    bmp.SetPixel(posx, posy - 1, c);
                                }
                            }
                        }
                    }
                    if (y % 5 == 0)
                    {
                        if (Cancel)
                            break;
                        Progress((byte)Math.Floor(100 - 100 * y / (float)height));
                    }
                }*/
                if (!Cancel)
                {
                    Progress(100);

                    ImageAnchor = new Point((int)Math.Round(this.Width / 2F - bmp.Width * ZoomLevel / 2F), (int)Math.Round(this.Height / 2F - bmp.Height * ZoomLevel / 2F));

                    Image = (Bitmap)bmp.Clone();

                    try
                    {
                        if (!Directory.Exists("levels/images"))
                            Directory.CreateDirectory("levels/images");
                        Image.Save("levels/images/" + name + ".bmp", ImageFormat.Bmp);
                    }
                    catch (Exception ex) { Server.ErrorLog(ex); }

                    DrawMap(ImageAnchor.X, ImageAnchor.Y);
                }
                else
                    Progress(0);
                Cancel = false;
                bmp.Dispose();
            }));
            t.Start();
        }
        private bool[,] InitTaken(Bitmap bmp)
        {
            bool[,] t = new bool[bmp.Width, bmp.Height];
            for (int x = 0; x < bmp.Width; ++x)
                for (int y = 0; y < bmp.Height; ++y)
                    t[x, y] = false;    
            return t;
        }
        #endregion
        #region Events
        
        public event ProgressEventHandler Progress_Changed;
        private void Progress(byte prg)
        {
            if (this.InvokeRequired)
            {
                ProgressEventHandler d = new ProgressEventHandler(Progress);
                this.Invoke(d, prg);
            }
            else
            {
                if (Progress_Changed != null)
                    Progress_Changed(prg);
            }
        }
        #endregion
    }
}
