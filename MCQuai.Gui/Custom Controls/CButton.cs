﻿using System;
using System.Windows.Forms;

namespace MCRevive.Gui
{
    public partial class CButton : Button
    {
        DateTime lastClick;
        public CButton()
        {
            InitializeComponent();
            lastClick = DateTime.Now;
        }
        protected override void OnClick(EventArgs e)
        {
            this.AccessibilityNotifyClients(AccessibleEvents.StateChange, -1);
            this.AccessibilityNotifyClients(AccessibleEvents.NameChange, -1);
            if (lastClick.AddSeconds(1) < DateTime.Now)
            {
                lastClick = DateTime.Now;
                base.OnClick(e);
            }
        }
    }
}
