﻿using System;
using System.Windows.Forms;

namespace MCRevive.Gui
{
    partial class PropertiesWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PropertiesWindow));
            this.btnSave = new MCRevive.Gui.CButton();
            this.btnDiscard = new MCRevive.Gui.CButton();
            this.btnApply = new MCRevive.Gui.CButton();
            this.tt = new System.Windows.Forms.ToolTip(this.components);
            this.btnCMDHelp = new MCRevive.Gui.CButton();
            this.txtCMDLowest = new System.Windows.Forms.TextBox();
            this.rtxtCMDRanks = new System.Windows.Forms.RichTextBox();
            this.chkHelp = new System.Windows.Forms.CheckBox();
            this.txtServerOverload = new System.Windows.Forms.TextBox();
            this.txtMaxDepth = new System.Windows.Forms.TextBox();
            this.ChkTunnels = new System.Windows.Forms.CheckBox();
            this.txtSpamBlockCount = new System.Windows.Forms.TextBox();
            this.txtSpamBlockTimer = new System.Windows.Forms.TextBox();
            this.chkAntiGrief = new System.Windows.Forms.CheckBox();
            this.chkOpNotify = new System.Windows.Forms.CheckBox();
            this.cmbGrief1 = new System.Windows.Forms.ComboBox();
            this.cmbGriefTimes = new System.Windows.Forms.ComboBox();
            this.cmbGrief2 = new System.Windows.Forms.ComboBox();
            this.chkExtraban = new System.Windows.Forms.CheckBox();
            this.chkOptoOp = new System.Windows.Forms.CheckBox();
            this.chkAdmintoAdmin = new System.Windows.Forms.CheckBox();
            this.txtServerName = new System.Windows.Forms.TextBox();
            this.txtServerMOTD = new System.Windows.Forms.TextBox();
            this.txtServerPort = new System.Windows.Forms.TextBox();
            this.chkWorldChat = new System.Windows.Forms.CheckBox();
            this.chkPublic = new System.Windows.Forms.CheckBox();
            this.chkVerifyNames = new System.Windows.Forms.CheckBox();
            this.txtIRCNick = new System.Windows.Forms.TextBox();
            this.txtIRCServer = new System.Windows.Forms.TextBox();
            this.txtIRCChannel = new System.Windows.Forms.TextBox();
            this.txtServerMaxPlayers = new System.Windows.Forms.TextBox();
            this.txtServerMaxMaps = new System.Windows.Forms.TextBox();
            this.txtBackup = new System.Windows.Forms.TextBox();
            this.txtAFKTimer = new System.Windows.Forms.TextBox();
            this.txtAFKKick = new System.Windows.Forms.TextBox();
            this.chkIRC = new System.Windows.Forms.CheckBox();
            this.cmbDefaultColor = new System.Windows.Forms.ComboBox();
            this.txtIRCPort = new System.Windows.Forms.TextBox();
            this.txtServerOwner = new System.Windows.Forms.TextBox();
            this.chkGuestGoto = new System.Windows.Forms.CheckBox();
            this.txtServerOwnerLogin = new System.Windows.Forms.TextBox();
            this.chkGeolocation = new System.Windows.Forms.CheckBox();
            this.rtxtBLKRanks = new System.Windows.Forms.RichTextBox();
            this.txtBLKLowest = new System.Windows.Forms.TextBox();
            this.btnBLKHelp = new MCRevive.Gui.CButton();
            this.txtServerDescription = new System.Windows.Forms.TextBox();
            this.chkNewlvlHelp = new System.Windows.Forms.CheckBox();
            this.chkPortforward = new System.Windows.Forms.CheckBox();
            this.chkRestartOnError = new System.Windows.Forms.CheckBox();
            this.chkHelpPortforward = new System.Windows.Forms.CheckBox();
            this.txtIRCPassword = new System.Windows.Forms.TextBox();
            this.chkIRCIdentify = new System.Windows.Forms.CheckBox();
            this.chkAchievements = new System.Windows.Forms.CheckBox();
            this.tabBlocks = new System.Windows.Forms.TabPage();
            this.lblBLKLowest = new System.Windows.Forms.Label();
            this.liBlocks = new System.Windows.Forms.ListBox();
            this.tabCommands = new System.Windows.Forms.TabPage();
            this.lblCMDLowest = new System.Windows.Forms.Label();
            this.liCommands = new System.Windows.Forms.ListBox();
            this.tabUpdate = new System.Windows.Forms.TabPage();
            this.chkUpdate = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbForceAfter = new System.Windows.Forms.ComboBox();
            this.cmbChkHour = new System.Windows.Forms.ComboBox();
            this.chkForceUpdate = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnUpdate = new MCRevive.Gui.CButton();
            this.liUpdate = new System.Windows.Forms.ListBox();
            this.tabAntiGrief = new System.Windows.Forms.TabPage();
            this.grpboxGriefSys = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblServerDepth = new System.Windows.Forms.Label();
            this.lblServerOverload = new System.Windows.Forms.Label();
            this.tapRank = new System.Windows.Forms.TabPage();
            this.btnRankRemove = new MCRevive.Gui.CButton();
            this.grpRankInfo = new System.Windows.Forms.GroupBox();
            this.liRankPeople = new System.Windows.Forms.ListBox();
            this.menuRankPeople = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.itmRank = new System.Windows.Forms.ToolStripMenuItem();
            this.promoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.demoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label23 = new System.Windows.Forms.Label();
            this.txtRankPeople = new System.Windows.Forms.TextBox();
            this.liRanks = new System.Windows.Forms.ListBox();
            this.menuRanks = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.reloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grpEditRanks = new System.Windows.Forms.GroupBox();
            this.lblRankColor = new System.Windows.Forms.Label();
            this.btnRankNew = new MCRevive.Gui.CButton();
            this.btnRankSave = new MCRevive.Gui.CButton();
            this.cmdRankGroup = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cmbRankColor = new System.Windows.Forms.ComboBox();
            this.txtRankMaxBlocks = new System.Windows.Forms.TextBox();
            this.txtRankPermission = new System.Windows.Forms.TextBox();
            this.txtRankName = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tapServer = new System.Windows.Forms.TabPage();
            this.groupBoxMapInfo = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMainLevel = new System.Windows.Forms.TextBox();
            this.lblServerMaxMaps = new System.Windows.Forms.Label();
            this.lblServerBackupTime = new System.Windows.Forms.Label();
            this.groupBoxMisc = new System.Windows.Forms.GroupBox();
            this.lblDefaultColor = new System.Windows.Forms.Label();
            this.lblServerDefaultColor = new System.Windows.Forms.Label();
            this.groupBoxAFK = new System.Windows.Forms.GroupBox();
            this.lblServerAFKKick = new System.Windows.Forms.Label();
            this.lblServerAFKTimer = new System.Windows.Forms.Label();
            this.groupBoxMain = new System.Windows.Forms.GroupBox();
            this.lblServerName = new System.Windows.Forms.Label();
            this.lblMOTD = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblServerOwnerLogin = new System.Windows.Forms.Label();
            this.lblServerMaxPlayers = new System.Windows.Forms.Label();
            this.lblServerOwner = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblIRCPort = new System.Windows.Forms.Label();
            this.lblIRCChannel = new System.Windows.Forms.Label();
            this.lblIRCServer = new System.Windows.Forms.Label();
            this.lblIRCNick = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabIRC = new System.Windows.Forms.TabPage();
            this.grpIRC = new System.Windows.Forms.GroupBox();
            this.grpIRCOptional = new System.Windows.Forms.GroupBox();
            this.chkIRCShowPassword = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tabBlocks.SuspendLayout();
            this.tabCommands.SuspendLayout();
            this.tabUpdate.SuspendLayout();
            this.tabAntiGrief.SuspendLayout();
            this.grpboxGriefSys.SuspendLayout();
            this.tapRank.SuspendLayout();
            this.grpRankInfo.SuspendLayout();
            this.menuRankPeople.SuspendLayout();
            this.menuRanks.SuspendLayout();
            this.grpEditRanks.SuspendLayout();
            this.tapServer.SuspendLayout();
            this.groupBoxMapInfo.SuspendLayout();
            this.groupBoxMisc.SuspendLayout();
            this.groupBoxAFK.SuspendLayout();
            this.groupBoxMain.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabIRC.SuspendLayout();
            this.grpIRC.SuspendLayout();
            this.grpIRCOptional.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(13, 461);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 25);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save and Close";
            this.tt.SetToolTip(this.btnSave, "Simply saves all properties, and closes the \"Propeties Window\"");
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDiscard
            // 
            this.btnDiscard.Location = new System.Drawing.Point(109, 461);
            this.btnDiscard.Name = "btnDiscard";
            this.btnDiscard.Size = new System.Drawing.Size(90, 25);
            this.btnDiscard.TabIndex = 2;
            this.btnDiscard.Text = "Discard";
            this.tt.SetToolTip(this.btnDiscard, "Close the \"Properties Window\", without saving anything");
            this.btnDiscard.UseVisualStyleBackColor = true;
            this.btnDiscard.Click += new System.EventHandler(this.btnDiscard_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(357, 461);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 25);
            this.btnApply.TabIndex = 3;
            this.btnApply.Text = "Apply";
            this.tt.SetToolTip(this.btnApply, "Save all settings, without closing the \"Properties Window\"");
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // tt
            // 
            this.tt.AutoPopDelay = 20000;
            this.tt.InitialDelay = 500;
            this.tt.IsBalloon = true;
            this.tt.OwnerDraw = true;
            this.tt.ReshowDelay = 100;
            this.tt.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.tt.ToolTipTitle = "Information";
            // 
            // btnCMDHelp
            // 
            this.btnCMDHelp.AutoSize = true;
            this.btnCMDHelp.Location = new System.Drawing.Point(270, 379);
            this.btnCMDHelp.Name = "btnCMDHelp";
            this.btnCMDHelp.Size = new System.Drawing.Size(127, 23);
            this.btnCMDHelp.TabIndex = 48;
            this.btnCMDHelp.Text = "Help Information";
            this.tt.SetToolTip(this.btnCMDHelp, "Shows information about the selected command");
            this.btnCMDHelp.UseVisualStyleBackColor = true;
            this.btnCMDHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // txtCMDLowest
            // 
            this.txtCMDLowest.Location = new System.Drawing.Point(89, 31);
            this.txtCMDLowest.Name = "txtCMDLowest";
            this.txtCMDLowest.Size = new System.Drawing.Size(175, 20);
            this.txtCMDLowest.TabIndex = 49;
            this.tt.SetToolTip(this.txtCMDLowest, "Sets the rank needed for the selected command");
            this.txtCMDLowest.TextChanged += new System.EventHandler(this.txtCMDLowest_TextChanged);
            this.txtCMDLowest.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCMDLowest_KeyDown);
            // 
            // rtxtCMDRanks
            // 
            this.rtxtCMDRanks.AcceptsTab = true;
            this.rtxtCMDRanks.BackColor = System.Drawing.SystemColors.Window;
            this.rtxtCMDRanks.Location = new System.Drawing.Point(3, 111);
            this.rtxtCMDRanks.Name = "rtxtCMDRanks";
            this.rtxtCMDRanks.ReadOnly = true;
            this.rtxtCMDRanks.Size = new System.Drawing.Size(261, 261);
            this.rtxtCMDRanks.TabIndex = 0;
            this.rtxtCMDRanks.Text = "Available ranks are:";
            this.tt.SetToolTip(this.rtxtCMDRanks, "Available ranks");
            // 
            // chkHelp
            // 
            this.chkHelp.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkHelp.AutoSize = true;
            this.chkHelp.Location = new System.Drawing.Point(3, 379);
            this.chkHelp.Name = "chkHelp";
            this.chkHelp.Size = new System.Drawing.Size(61, 23);
            this.chkHelp.TabIndex = 61;
            this.chkHelp.Text = "Old /help";
            this.tt.SetToolTip(this.chkHelp, "Determines whether the server should be running the old version of /help or the n" +
        "ewer one");
            this.chkHelp.UseVisualStyleBackColor = true;
            // 
            // txtServerOverload
            // 
            this.txtServerOverload.Location = new System.Drawing.Point(100, 209);
            this.txtServerOverload.Name = "txtServerOverload";
            this.txtServerOverload.Size = new System.Drawing.Size(146, 20);
            this.txtServerOverload.TabIndex = 47;
            this.tt.SetToolTip(this.txtServerOverload, "the higher this is, the longer the physics is allowed to lag");
            this.txtServerOverload.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtServerOverload_KeyDown);
            // 
            // txtMaxDepth
            // 
            this.txtMaxDepth.Location = new System.Drawing.Point(100, 177);
            this.txtMaxDepth.Name = "txtMaxDepth";
            this.txtMaxDepth.Size = new System.Drawing.Size(146, 20);
            this.txtMaxDepth.TabIndex = 45;
            this.tt.SetToolTip(this.txtMaxDepth, "the maximum depth guests can dig down");
            this.txtMaxDepth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaxDepth_KeyDown);
            // 
            // ChkTunnels
            // 
            this.ChkTunnels.Appearance = System.Windows.Forms.Appearance.Button;
            this.ChkTunnels.AutoSize = true;
            this.ChkTunnels.Location = new System.Drawing.Point(13, 150);
            this.ChkTunnels.Name = "ChkTunnels";
            this.ChkTunnels.Size = new System.Drawing.Size(81, 23);
            this.ChkTunnels.TabIndex = 44;
            this.ChkTunnels.Text = "Anti tunneling";
            this.tt.SetToolTip(this.ChkTunnels, "Stops people to dig below max-depth");
            this.ChkTunnels.UseVisualStyleBackColor = true;
            // 
            // txtSpamBlockCount
            // 
            this.txtSpamBlockCount.Location = new System.Drawing.Point(112, 49);
            this.txtSpamBlockCount.Name = "txtSpamBlockCount";
            this.txtSpamBlockCount.Size = new System.Drawing.Size(51, 20);
            this.txtSpamBlockCount.TabIndex = 38;
            this.tt.SetToolTip(this.txtSpamBlockCount, "How many blocks you may modify in \"block-time\" seconds, before you get kicked");
            this.txtSpamBlockCount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSpamBlockCount_KeyDown);
            // 
            // txtSpamBlockTimer
            // 
            this.txtSpamBlockTimer.Location = new System.Drawing.Point(224, 49);
            this.txtSpamBlockTimer.Name = "txtSpamBlockTimer";
            this.txtSpamBlockTimer.Size = new System.Drawing.Size(52, 20);
            this.txtSpamBlockTimer.TabIndex = 39;
            this.tt.SetToolTip(this.txtSpamBlockTimer, "how many seconds you have to modify \"max-blocks\" blocks, before you get kicked");
            this.txtSpamBlockTimer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSpamBlockTimer_KeyDown);
            // 
            // chkAntiGrief
            // 
            this.chkAntiGrief.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkAntiGrief.AutoSize = true;
            this.chkAntiGrief.Location = new System.Drawing.Point(6, 17);
            this.chkAntiGrief.Name = "chkAntiGrief";
            this.chkAntiGrief.Size = new System.Drawing.Size(97, 23);
            this.chkAntiGrief.TabIndex = 37;
            this.chkAntiGrief.Text = "Anti Grief System";
            this.tt.SetToolTip(this.chkAntiGrief, "Determines whether Anti Grief System is activated or not");
            this.chkAntiGrief.UseVisualStyleBackColor = true;
            // 
            // chkOpNotify
            // 
            this.chkOpNotify.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkOpNotify.Location = new System.Drawing.Point(25, 82);
            this.chkOpNotify.Name = "chkOpNotify";
            this.chkOpNotify.Size = new System.Drawing.Size(85, 23);
            this.chkOpNotify.TabIndex = 40;
            this.chkOpNotify.Text = "Notify OP+";
            this.tt.SetToolTip(this.chkOpNotify, "Let ops know?");
            this.chkOpNotify.UseVisualStyleBackColor = true;
            // 
            // cmbGrief1
            // 
            this.cmbGrief1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGrief1.FormattingEnabled = true;
            this.cmbGrief1.Items.AddRange(new object[] {
            "Destroyed",
            "Built"});
            this.cmbGrief1.Location = new System.Drawing.Point(246, 84);
            this.cmbGrief1.Name = "cmbGrief1";
            this.cmbGrief1.Size = new System.Drawing.Size(141, 21);
            this.cmbGrief1.TabIndex = 41;
            this.tt.SetToolTip(this.cmbGrief1, "Modified, Destroyed, Built?");
            // 
            // cmbGriefTimes
            // 
            this.cmbGriefTimes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGriefTimes.FormattingEnabled = true;
            this.cmbGriefTimes.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cmbGriefTimes.Location = new System.Drawing.Point(25, 107);
            this.cmbGriefTimes.Name = "cmbGriefTimes";
            this.cmbGriefTimes.Size = new System.Drawing.Size(99, 21);
            this.cmbGriefTimes.TabIndex = 42;
            this.tt.SetToolTip(this.cmbGriefTimes, "1, 2, 3, 4, 5, 6, 7, 8, 9, 10?");
            // 
            // cmbGrief2
            // 
            this.cmbGrief2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGrief2.FormattingEnabled = true;
            this.cmbGrief2.Items.AddRange(new object[] {
            "Destroyed",
            "Built"});
            this.cmbGrief2.Location = new System.Drawing.Point(246, 107);
            this.cmbGrief2.Name = "cmbGrief2";
            this.cmbGrief2.Size = new System.Drawing.Size(141, 21);
            this.cmbGrief2.TabIndex = 43;
            this.tt.SetToolTip(this.cmbGrief2, "Modified, Destroyed, Built?");
            // 
            // chkExtraban
            // 
            this.chkExtraban.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkExtraban.AutoSize = true;
            this.chkExtraban.Location = new System.Drawing.Point(320, 150);
            this.chkExtraban.Name = "chkExtraban";
            this.chkExtraban.Size = new System.Drawing.Size(77, 23);
            this.chkExtraban.TabIndex = 46;
            this.chkExtraban.Text = "Extra ban list";
            this.tt.SetToolTip(this.chkExtraban, "Determines whether the server should read from an online ban-list of griefers, ad" +
        "ded by Quaisaq himself");
            this.chkExtraban.UseVisualStyleBackColor = true;
            // 
            // chkOptoOp
            // 
            this.chkOptoOp.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkOptoOp.Location = new System.Drawing.Point(6, 6);
            this.chkOptoOp.Name = "chkOptoOp";
            this.chkOptoOp.Size = new System.Drawing.Size(398, 21);
            this.chkOptoOp.TabIndex = 35;
            this.chkOptoOp.Text = "Operators can promote others to Operators";
            this.chkOptoOp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tt.SetToolTip(this.chkOptoOp, "Determines whether Operators should be able to promote other people to Operators");
            this.chkOptoOp.UseVisualStyleBackColor = true;
            // 
            // chkAdmintoAdmin
            // 
            this.chkAdmintoAdmin.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkAdmintoAdmin.Location = new System.Drawing.Point(6, 28);
            this.chkAdmintoAdmin.Name = "chkAdmintoAdmin";
            this.chkAdmintoAdmin.Size = new System.Drawing.Size(398, 21);
            this.chkAdmintoAdmin.TabIndex = 36;
            this.chkAdmintoAdmin.Text = "Admins can promote others to Admins";
            this.chkAdmintoAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tt.SetToolTip(this.chkAdmintoAdmin, "Determines wether Admins should be able to promote other people to Admins");
            this.chkAdmintoAdmin.UseVisualStyleBackColor = true;
            // 
            // txtServerName
            // 
            this.txtServerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServerName.Location = new System.Drawing.Point(63, 19);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.Size = new System.Drawing.Size(320, 20);
            this.txtServerName.TabIndex = 1;
            this.tt.SetToolTip(this.txtServerName, "What should your servers name be?");
            // 
            // txtServerMOTD
            // 
            this.txtServerMOTD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServerMOTD.Location = new System.Drawing.Point(63, 45);
            this.txtServerMOTD.Name = "txtServerMOTD";
            this.txtServerMOTD.Size = new System.Drawing.Size(320, 20);
            this.txtServerMOTD.TabIndex = 2;
            this.tt.SetToolTip(this.txtServerMOTD, "What should the Message Of The Day (MOTD) be?");
            // 
            // txtServerPort
            // 
            this.txtServerPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServerPort.Location = new System.Drawing.Point(303, 149);
            this.txtServerPort.Name = "txtServerPort";
            this.txtServerPort.Size = new System.Drawing.Size(80, 20);
            this.txtServerPort.TabIndex = 3;
            this.tt.SetToolTip(this.txtServerPort, "What port is your server running on?\nDefault: 25565");
            this.txtServerPort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtServerPort_KeyDown);
            // 
            // chkWorldChat
            // 
            this.chkWorldChat.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkWorldChat.AutoSize = true;
            this.chkWorldChat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkWorldChat.Location = new System.Drawing.Point(89, 186);
            this.chkWorldChat.Name = "chkWorldChat";
            this.chkWorldChat.Size = new System.Drawing.Size(72, 23);
            this.chkWorldChat.TabIndex = 4;
            this.chkWorldChat.Text = "Global Chat";
            this.chkWorldChat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tt.SetToolTip(this.chkWorldChat, "Should everyone be able to speak together or only people in the same map?");
            this.chkWorldChat.UseVisualStyleBackColor = true;
            // 
            // chkPublic
            // 
            this.chkPublic.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkPublic.AutoSize = true;
            this.chkPublic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPublic.Location = new System.Drawing.Point(11, 186);
            this.chkPublic.Name = "chkPublic";
            this.chkPublic.Size = new System.Drawing.Size(46, 23);
            this.chkPublic.TabIndex = 6;
            this.chkPublic.Text = "Public";
            this.chkPublic.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tt.SetToolTip(this.chkPublic, "Should others be able to find your server on the official server list?");
            this.chkPublic.UseVisualStyleBackColor = true;
            // 
            // chkVerifyNames
            // 
            this.chkVerifyNames.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkVerifyNames.AutoSize = true;
            this.chkVerifyNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVerifyNames.Location = new System.Drawing.Point(298, 186);
            this.chkVerifyNames.Name = "chkVerifyNames";
            this.chkVerifyNames.Size = new System.Drawing.Size(79, 23);
            this.chkVerifyNames.TabIndex = 5;
            this.chkVerifyNames.Text = "Verify Names";
            this.chkVerifyNames.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tt.SetToolTip(this.chkVerifyNames, "Verify the validity of names?");
            this.chkVerifyNames.UseVisualStyleBackColor = true;
            // 
            // txtIRCNick
            // 
            this.txtIRCNick.Location = new System.Drawing.Point(61, 102);
            this.txtIRCNick.Name = "txtIRCNick";
            this.txtIRCNick.Size = new System.Drawing.Size(337, 20);
            this.txtIRCNick.TabIndex = 17;
            this.tt.SetToolTip(this.txtIRCNick, "Whats the name you want to have on the IRC Channel?");
            // 
            // txtIRCServer
            // 
            this.txtIRCServer.Location = new System.Drawing.Point(61, 76);
            this.txtIRCServer.Name = "txtIRCServer";
            this.txtIRCServer.Size = new System.Drawing.Size(337, 20);
            this.txtIRCServer.TabIndex = 16;
            this.tt.SetToolTip(this.txtIRCServer, "What server are your IRC Channel running on?");
            // 
            // txtIRCChannel
            // 
            this.txtIRCChannel.Location = new System.Drawing.Point(61, 50);
            this.txtIRCChannel.Name = "txtIRCChannel";
            this.txtIRCChannel.Size = new System.Drawing.Size(337, 20);
            this.txtIRCChannel.TabIndex = 14;
            this.tt.SetToolTip(this.txtIRCChannel, "Whats the name of the IRC Channel?");
            // 
            // txtServerMaxPlayers
            // 
            this.txtServerMaxPlayers.Location = new System.Drawing.Point(79, 149);
            this.txtServerMaxPlayers.Name = "txtServerMaxPlayers";
            this.txtServerMaxPlayers.Size = new System.Drawing.Size(108, 20);
            this.txtServerMaxPlayers.TabIndex = 9;
            this.tt.SetToolTip(this.txtServerMaxPlayers, "How many people should be able to play on the server at once?");
            this.txtServerMaxPlayers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtServerMaxPlayers_KeyDown);
            // 
            // txtServerMaxMaps
            // 
            this.txtServerMaxMaps.Location = new System.Drawing.Point(85, 42);
            this.txtServerMaxMaps.Name = "txtServerMaxMaps";
            this.txtServerMaxMaps.Size = new System.Drawing.Size(97, 20);
            this.txtServerMaxMaps.TabIndex = 11;
            this.tt.SetToolTip(this.txtServerMaxMaps, "What should be the maximum number of maps loaded at the same time?");
            this.txtServerMaxMaps.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtServerMaxMaps_KeyDown);
            // 
            // txtBackup
            // 
            this.txtBackup.Location = new System.Drawing.Point(85, 19);
            this.txtBackup.Name = "txtBackup";
            this.txtBackup.Size = new System.Drawing.Size(69, 20);
            this.txtBackup.TabIndex = 12;
            this.tt.SetToolTip(this.txtBackup, "how many seconds before auto-saving all maps?");
            this.txtBackup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBackup_KeyDown);
            // 
            // txtAFKTimer
            // 
            this.txtAFKTimer.Location = new System.Drawing.Point(71, 34);
            this.txtAFKTimer.Name = "txtAFKTimer";
            this.txtAFKTimer.Size = new System.Drawing.Size(88, 20);
            this.txtAFKTimer.TabIndex = 20;
            this.tt.SetToolTip(this.txtAFKTimer, "How many minutes of being AFK should there go, before you automaticly will be put" +
        " on the AFK list");
            this.txtAFKTimer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAFKTimer_KeyDown);
            // 
            // txtAFKKick
            // 
            this.txtAFKKick.Location = new System.Drawing.Point(71, 60);
            this.txtAFKKick.Name = "txtAFKKick";
            this.txtAFKKick.Size = new System.Drawing.Size(88, 20);
            this.txtAFKKick.TabIndex = 21;
            this.tt.SetToolTip(this.txtAFKKick, "How many minutes of being AFK should there go, before you automaticly will get ki" +
        "cked");
            this.txtAFKKick.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAFKKick_KeyDown);
            // 
            // chkIRC
            // 
            this.chkIRC.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIRC.AutoSize = true;
            this.chkIRC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIRC.Location = new System.Drawing.Point(6, 21);
            this.chkIRC.Name = "chkIRC";
            this.chkIRC.Size = new System.Drawing.Size(57, 23);
            this.chkIRC.TabIndex = 13;
            this.chkIRC.Text = "Use IRC";
            this.tt.SetToolTip(this.chkIRC, "Do you have an IRC Server?");
            this.chkIRC.UseVisualStyleBackColor = true;
            // 
            // cmbDefaultColor
            // 
            this.cmbDefaultColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultColor.FormattingEnabled = true;
            this.cmbDefaultColor.Items.AddRange(new object[] {
            "black",
            "navy",
            "green",
            "teal",
            "maroon",
            "purple",
            "gold",
            "silver",
            "gray",
            "blue",
            "lime",
            "aqua",
            "red",
            "pink",
            "yellow",
            "white"});
            this.cmbDefaultColor.Location = new System.Drawing.Point(83, 52);
            this.cmbDefaultColor.Name = "cmbDefaultColor";
            this.cmbDefaultColor.Size = new System.Drawing.Size(91, 21);
            this.cmbDefaultColor.TabIndex = 22;
            this.tt.SetToolTip(this.cmbDefaultColor, "What should be the normal text color on the server?");
            this.cmbDefaultColor.SelectedIndexChanged += new System.EventHandler(this.cmbDefaultColor_SelectedIndexChanged);
            // 
            // txtIRCPort
            // 
            this.txtIRCPort.Location = new System.Drawing.Point(61, 128);
            this.txtIRCPort.Name = "txtIRCPort";
            this.txtIRCPort.Size = new System.Drawing.Size(337, 20);
            this.txtIRCPort.TabIndex = 15;
            this.tt.SetToolTip(this.txtIRCPort, "What port is your IRC server running?\nDefault: 6667");
            this.txtIRCPort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIRCPort_KeyDown);
            // 
            // txtServerOwner
            // 
            this.txtServerOwner.Location = new System.Drawing.Point(79, 123);
            this.txtServerOwner.Name = "txtServerOwner";
            this.txtServerOwner.Size = new System.Drawing.Size(304, 20);
            this.txtServerOwner.TabIndex = 7;
            this.tt.SetToolTip(this.txtServerOwner, "Whats your username in Minecraft?");
            // 
            // chkGuestGoto
            // 
            this.chkGuestGoto.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGuestGoto.AutoSize = true;
            this.chkGuestGoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGuestGoto.Location = new System.Drawing.Point(192, 186);
            this.chkGuestGoto.Name = "chkGuestGoto";
            this.chkGuestGoto.Size = new System.Drawing.Size(71, 23);
            this.chkGuestGoto.TabIndex = 8;
            this.chkGuestGoto.Text = "Guest Goto";
            this.chkGuestGoto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tt.SetToolTip(this.chkGuestGoto, "Should Guests be allowed to use /goto and /levels?");
            this.chkGuestGoto.UseVisualStyleBackColor = true;
            // 
            // txtServerOwnerLogin
            // 
            this.txtServerOwnerLogin.Location = new System.Drawing.Point(79, 71);
            this.txtServerOwnerLogin.Name = "txtServerOwnerLogin";
            this.txtServerOwnerLogin.Size = new System.Drawing.Size(304, 20);
            this.txtServerOwnerLogin.TabIndex = 10;
            this.tt.SetToolTip(this.txtServerOwnerLogin, "What message should the server use when you log in?");
            // 
            // chkGeolocation
            // 
            this.chkGeolocation.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGeolocation.AutoSize = true;
            this.chkGeolocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGeolocation.Location = new System.Drawing.Point(6, 19);
            this.chkGeolocation.Name = "chkGeolocation";
            this.chkGeolocation.Size = new System.Drawing.Size(74, 23);
            this.chkGeolocation.TabIndex = 19;
            this.chkGeolocation.Text = "Geolocation";
            this.chkGeolocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tt.SetToolTip(this.chkGeolocation, "Should people that joins the server be tracked?");
            this.chkGeolocation.UseVisualStyleBackColor = true;
            // 
            // rtxtBLKRanks
            // 
            this.rtxtBLKRanks.AcceptsTab = true;
            this.rtxtBLKRanks.BackColor = System.Drawing.SystemColors.Window;
            this.rtxtBLKRanks.Location = new System.Drawing.Point(3, 111);
            this.rtxtBLKRanks.Name = "rtxtBLKRanks";
            this.rtxtBLKRanks.ReadOnly = true;
            this.rtxtBLKRanks.Size = new System.Drawing.Size(261, 261);
            this.rtxtBLKRanks.TabIndex = 52;
            this.rtxtBLKRanks.Text = "Available ranks are:";
            this.tt.SetToolTip(this.rtxtBLKRanks, "Available ranks");
            // 
            // txtBLKLowest
            // 
            this.txtBLKLowest.Location = new System.Drawing.Point(89, 31);
            this.txtBLKLowest.Name = "txtBLKLowest";
            this.txtBLKLowest.Size = new System.Drawing.Size(175, 20);
            this.txtBLKLowest.TabIndex = 54;
            this.tt.SetToolTip(this.txtBLKLowest, "Sets the rank needed for the selected command");
            this.txtBLKLowest.TextChanged += new System.EventHandler(this.txtBLKLowest_TextChanged);
            this.txtBLKLowest.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBLKLowest_KeyDown);
            // 
            // btnBLKHelp
            // 
            this.btnBLKHelp.AutoSize = true;
            this.btnBLKHelp.Location = new System.Drawing.Point(270, 386);
            this.btnBLKHelp.Name = "btnBLKHelp";
            this.btnBLKHelp.Size = new System.Drawing.Size(127, 23);
            this.btnBLKHelp.TabIndex = 53;
            this.btnBLKHelp.Text = "Help Information";
            this.tt.SetToolTip(this.btnBLKHelp, "Shows information about the selected command");
            this.btnBLKHelp.UseVisualStyleBackColor = true;
            // 
            // txtServerDescription
            // 
            this.txtServerDescription.Location = new System.Drawing.Point(79, 97);
            this.txtServerDescription.Name = "txtServerDescription";
            this.txtServerDescription.Size = new System.Drawing.Size(304, 20);
            this.txtServerDescription.TabIndex = 62;
            this.tt.SetToolTip(this.txtServerDescription, "This is the description that will pop up on the mcrevive serverlist");
            // 
            // chkNewlvlHelp
            // 
            this.chkNewlvlHelp.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkNewlvlHelp.AutoSize = true;
            this.chkNewlvlHelp.Location = new System.Drawing.Point(70, 379);
            this.chkNewlvlHelp.Name = "chkNewlvlHelp";
            this.chkNewlvlHelp.Size = new System.Drawing.Size(92, 23);
            this.chkNewlvlHelp.TabIndex = 62;
            this.chkNewlvlHelp.Text = "Help on /newlvl";
            this.tt.SetToolTip(this.chkNewlvlHelp, "Determines whether the server should be running the old version of /help or the n" +
        "ewer one");
            this.chkNewlvlHelp.UseVisualStyleBackColor = true;
            // 
            // chkPortforward
            // 
            this.chkPortforward.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkPortforward.AutoSize = true;
            this.chkPortforward.Location = new System.Drawing.Point(278, 19);
            this.chkPortforward.Name = "chkPortforward";
            this.chkPortforward.Size = new System.Drawing.Size(105, 23);
            this.chkPortforward.TabIndex = 66;
            this.chkPortforward.Text = "Check Portforward";
            this.chkPortforward.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tt.SetToolTip(this.chkPortforward, "Should MCRevive check wheater your ports are forwarded?");
            this.chkPortforward.UseVisualStyleBackColor = true;
            // 
            // chkRestartOnError
            // 
            this.chkRestartOnError.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkRestartOnError.AutoSize = true;
            this.chkRestartOnError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRestartOnError.Location = new System.Drawing.Point(86, 19);
            this.chkRestartOnError.Name = "chkRestartOnError";
            this.chkRestartOnError.Size = new System.Drawing.Size(93, 23);
            this.chkRestartOnError.TabIndex = 65;
            this.chkRestartOnError.Text = "Restart On Error";
            this.chkRestartOnError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tt.SetToolTip(this.chkRestartOnError, "Do you want MCRevive to restart if it crashes?\nThis will make sure that your serv" +
        "er stays online as much as possible.");
            this.chkRestartOnError.UseVisualStyleBackColor = true;
            // 
            // chkHelpPortforward
            // 
            this.chkHelpPortforward.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkHelpPortforward.Location = new System.Drawing.Point(278, 48);
            this.chkHelpPortforward.Name = "chkHelpPortforward";
            this.chkHelpPortforward.Size = new System.Drawing.Size(105, 23);
            this.chkHelpPortforward.TabIndex = 66;
            this.chkHelpPortforward.Text = "Help Portforward";
            this.chkHelpPortforward.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tt.SetToolTip(this.chkHelpPortforward, "Should MCRevive try to portforward, if the ports are not forwarded?");
            this.chkHelpPortforward.UseVisualStyleBackColor = true;
            // 
            // txtIRCPassword
            // 
            this.txtIRCPassword.Location = new System.Drawing.Point(68, 48);
            this.txtIRCPassword.Name = "txtIRCPassword";
            this.txtIRCPassword.Size = new System.Drawing.Size(198, 20);
            this.txtIRCPassword.TabIndex = 17;
            this.tt.SetToolTip(this.txtIRCPassword, "The password you want to use if you\'re identifying with nickserv");
            // 
            // chkIRCIdentify
            // 
            this.chkIRCIdentify.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIRCIdentify.AutoSize = true;
            this.chkIRCIdentify.Location = new System.Drawing.Point(6, 19);
            this.chkIRCIdentify.Name = "chkIRCIdentify";
            this.chkIRCIdentify.Size = new System.Drawing.Size(121, 23);
            this.chkIRCIdentify.TabIndex = 18;
            this.chkIRCIdentify.Text = "Identify With Nickserv";
            this.tt.SetToolTip(this.chkIRCIdentify, "Do you want the IRC bot to Identify itself with nickserv.\n\nNote: You will need to" +
        " register it\'s name with nickserv manually.");
            this.chkIRCIdentify.UseVisualStyleBackColor = true;
            // 
            // chkAchievements
            // 
            this.chkAchievements.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkAchievements.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAchievements.Location = new System.Drawing.Point(185, 19);
            this.chkAchievements.Name = "chkAchievements";
            this.chkAchievements.Size = new System.Drawing.Size(87, 23);
            this.chkAchievements.TabIndex = 64;
            this.chkAchievements.Text = "Achievements";
            this.chkAchievements.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkAchievements.UseVisualStyleBackColor = true;
            this.chkAchievements.CheckedChanged += new System.EventHandler(this.chkAchievements_CheckedChanged);
            // 
            // tabBlocks
            // 
            this.tabBlocks.Controls.Add(this.rtxtBLKRanks);
            this.tabBlocks.Controls.Add(this.txtBLKLowest);
            this.tabBlocks.Controls.Add(this.lblBLKLowest);
            this.tabBlocks.Controls.Add(this.btnBLKHelp);
            this.tabBlocks.Controls.Add(this.liBlocks);
            this.tabBlocks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabBlocks.Location = new System.Drawing.Point(4, 22);
            this.tabBlocks.Name = "tabBlocks";
            this.tabBlocks.Padding = new System.Windows.Forms.Padding(3);
            this.tabBlocks.Size = new System.Drawing.Size(410, 417);
            this.tabBlocks.TabIndex = 5;
            this.tabBlocks.Text = "Blocks";
            this.tabBlocks.UseVisualStyleBackColor = true;
            // 
            // lblBLKLowest
            // 
            this.lblBLKLowest.AutoSize = true;
            this.lblBLKLowest.Location = new System.Drawing.Point(10, 34);
            this.lblBLKLowest.Name = "lblBLKLowest";
            this.lblBLKLowest.Size = new System.Drawing.Size(75, 13);
            this.lblBLKLowest.TabIndex = 50;
            this.lblBLKLowest.Text = "Rank needed:";
            // 
            // liBlocks
            // 
            this.liBlocks.FormattingEnabled = true;
            this.liBlocks.Location = new System.Drawing.Point(270, 31);
            this.liBlocks.Name = "liBlocks";
            this.liBlocks.Size = new System.Drawing.Size(127, 342);
            this.liBlocks.TabIndex = 51;
            this.liBlocks.SelectedIndexChanged += new System.EventHandler(this.liBlocks_SelectedIndexChanged);
            // 
            // tabCommands
            // 
            this.tabCommands.Controls.Add(this.chkNewlvlHelp);
            this.tabCommands.Controls.Add(this.chkHelp);
            this.tabCommands.Controls.Add(this.rtxtCMDRanks);
            this.tabCommands.Controls.Add(this.txtCMDLowest);
            this.tabCommands.Controls.Add(this.lblCMDLowest);
            this.tabCommands.Controls.Add(this.btnCMDHelp);
            this.tabCommands.Controls.Add(this.liCommands);
            this.tabCommands.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCommands.Location = new System.Drawing.Point(4, 22);
            this.tabCommands.Name = "tabCommands";
            this.tabCommands.Size = new System.Drawing.Size(410, 417);
            this.tabCommands.TabIndex = 2;
            this.tabCommands.Text = "Commands";
            this.tabCommands.UseVisualStyleBackColor = true;
            // 
            // lblCMDLowest
            // 
            this.lblCMDLowest.AutoSize = true;
            this.lblCMDLowest.Location = new System.Drawing.Point(8, 34);
            this.lblCMDLowest.Name = "lblCMDLowest";
            this.lblCMDLowest.Size = new System.Drawing.Size(75, 13);
            this.lblCMDLowest.TabIndex = 0;
            this.lblCMDLowest.Text = "Rank needed:";
            // 
            // liCommands
            // 
            this.liCommands.FormattingEnabled = true;
            this.liCommands.Location = new System.Drawing.Point(270, 31);
            this.liCommands.Name = "liCommands";
            this.liCommands.Size = new System.Drawing.Size(127, 342);
            this.liCommands.Sorted = true;
            this.liCommands.TabIndex = 0;
            this.liCommands.SelectedIndexChanged += new System.EventHandler(this.liCommands_SelectedIndexChanged);
            // 
            // tabUpdate
            // 
            this.tabUpdate.Controls.Add(this.chkUpdate);
            this.tabUpdate.Controls.Add(this.label15);
            this.tabUpdate.Controls.Add(this.cmbForceAfter);
            this.tabUpdate.Controls.Add(this.cmbChkHour);
            this.tabUpdate.Controls.Add(this.chkForceUpdate);
            this.tabUpdate.Controls.Add(this.label14);
            this.tabUpdate.Controls.Add(this.btnUpdate);
            this.tabUpdate.Controls.Add(this.liUpdate);
            this.tabUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabUpdate.Location = new System.Drawing.Point(4, 22);
            this.tabUpdate.Name = "tabUpdate";
            this.tabUpdate.Size = new System.Drawing.Size(410, 417);
            this.tabUpdate.TabIndex = 4;
            this.tabUpdate.Text = "Updating";
            this.tabUpdate.UseVisualStyleBackColor = true;
            // 
            // chkUpdate
            // 
            this.chkUpdate.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUpdate.Location = new System.Drawing.Point(3, 112);
            this.chkUpdate.Name = "chkUpdate";
            this.chkUpdate.Size = new System.Drawing.Size(139, 23);
            this.chkUpdate.TabIndex = 10;
            this.chkUpdate.Text = "Check for updates";
            this.chkUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUpdate.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(101, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "every";
            // 
            // cmbForceAfter
            // 
            this.cmbForceAfter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbForceAfter.FormattingEnabled = true;
            this.cmbForceAfter.Items.AddRange(new object[] {
            "10 seconds",
            "1 minute",
            "15 minutes",
            "30 minutes",
            "45 minutes",
            "1 hour"});
            this.cmbForceAfter.Location = new System.Drawing.Point(140, 261);
            this.cmbForceAfter.Name = "cmbForceAfter";
            this.cmbForceAfter.Size = new System.Drawing.Size(95, 21);
            this.cmbForceAfter.TabIndex = 14;
            // 
            // cmbChkHour
            // 
            this.cmbChkHour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbChkHour.FormattingEnabled = true;
            this.cmbChkHour.Items.AddRange(new object[] {
            "1 hour",
            "2 hours",
            "4 hours",
            "8 hours",
            "16 hours",
            "1 day",
            "2 days"});
            this.cmbChkHour.Location = new System.Drawing.Point(140, 141);
            this.cmbChkHour.Name = "cmbChkHour";
            this.cmbChkHour.Size = new System.Drawing.Size(95, 21);
            this.cmbChkHour.TabIndex = 12;
            // 
            // chkForceUpdate
            // 
            this.chkForceUpdate.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkForceUpdate.Location = new System.Drawing.Point(3, 232);
            this.chkForceUpdate.Name = "chkForceUpdate";
            this.chkForceUpdate.Size = new System.Drawing.Size(139, 23);
            this.chkForceUpdate.TabIndex = 9;
            this.chkForceUpdate.Text = "Force Update";
            this.chkForceUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkForceUpdate.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(106, 264);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "after";
            // 
            // btnUpdate
            // 
            this.btnUpdate.AutoSize = true;
            this.btnUpdate.Location = new System.Drawing.Point(3, 15);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(404, 67);
            this.btnUpdate.TabIndex = 15;
            this.btnUpdate.Text = "Install Now";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // liUpdate
            // 
            this.liUpdate.FormattingEnabled = true;
            this.liUpdate.Location = new System.Drawing.Point(254, 84);
            this.liUpdate.Name = "liUpdate";
            this.liUpdate.Size = new System.Drawing.Size(153, 316);
            this.liUpdate.TabIndex = 8;
            // 
            // tabAntiGrief
            // 
            this.tabAntiGrief.Controls.Add(this.chkExtraban);
            this.tabAntiGrief.Controls.Add(this.grpboxGriefSys);
            this.tabAntiGrief.Controls.Add(this.ChkTunnels);
            this.tabAntiGrief.Controls.Add(this.lblServerDepth);
            this.tabAntiGrief.Controls.Add(this.txtMaxDepth);
            this.tabAntiGrief.Controls.Add(this.txtServerOverload);
            this.tabAntiGrief.Controls.Add(this.lblServerOverload);
            this.tabAntiGrief.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabAntiGrief.Location = new System.Drawing.Point(4, 22);
            this.tabAntiGrief.Name = "tabAntiGrief";
            this.tabAntiGrief.Size = new System.Drawing.Size(410, 417);
            this.tabAntiGrief.TabIndex = 3;
            this.tabAntiGrief.Text = "Anti Grief";
            this.tabAntiGrief.UseVisualStyleBackColor = true;
            // 
            // grpboxGriefSys
            // 
            this.grpboxGriefSys.Controls.Add(this.cmbGrief2);
            this.grpboxGriefSys.Controls.Add(this.label5);
            this.grpboxGriefSys.Controls.Add(this.cmbGriefTimes);
            this.grpboxGriefSys.Controls.Add(this.cmbGrief1);
            this.grpboxGriefSys.Controls.Add(this.label4);
            this.grpboxGriefSys.Controls.Add(this.chkOpNotify);
            this.grpboxGriefSys.Controls.Add(this.chkAntiGrief);
            this.grpboxGriefSys.Controls.Add(this.label12);
            this.grpboxGriefSys.Controls.Add(this.label3);
            this.grpboxGriefSys.Controls.Add(this.label13);
            this.grpboxGriefSys.Controls.Add(this.txtSpamBlockTimer);
            this.grpboxGriefSys.Controls.Add(this.label2);
            this.grpboxGriefSys.Controls.Add(this.txtSpamBlockCount);
            this.grpboxGriefSys.Controls.Add(this.label1);
            this.grpboxGriefSys.Controls.Add(this.label6);
            this.grpboxGriefSys.Controls.Add(this.label7);
            this.grpboxGriefSys.Controls.Add(this.label8);
            this.grpboxGriefSys.Controls.Add(this.label9);
            this.grpboxGriefSys.Controls.Add(this.label10);
            this.grpboxGriefSys.Controls.Add(this.label11);
            this.grpboxGriefSys.Location = new System.Drawing.Point(4, 3);
            this.grpboxGriefSys.Name = "grpboxGriefSys";
            this.grpboxGriefSys.Size = new System.Drawing.Size(393, 141);
            this.grpboxGriefSys.TabIndex = 0;
            this.grpboxGriefSys.TabStop = false;
            this.grpboxGriefSys.Text = "Anti Grief System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(130, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "times more than";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(116, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "when a player has";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(282, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "seconds - kick";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 87);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(10, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(169, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "blocks in";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "When modified";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(9, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "|";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(9, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "|";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(9, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "|";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(9, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "|";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(9, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "|";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(9, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "|";
            // 
            // lblServerDepth
            // 
            this.lblServerDepth.AutoEllipsis = true;
            this.lblServerDepth.AutoSize = true;
            this.lblServerDepth.Location = new System.Drawing.Point(55, 179);
            this.lblServerDepth.Name = "lblServerDepth";
            this.lblServerDepth.Size = new System.Drawing.Size(39, 13);
            this.lblServerDepth.TabIndex = 0;
            this.lblServerDepth.Text = "Depth:";
            // 
            // lblServerOverload
            // 
            this.lblServerOverload.AutoSize = true;
            this.lblServerOverload.Location = new System.Drawing.Point(41, 209);
            this.lblServerOverload.Name = "lblServerOverload";
            this.lblServerOverload.Size = new System.Drawing.Size(53, 13);
            this.lblServerOverload.TabIndex = 0;
            this.lblServerOverload.Text = "Overload:";
            // 
            // tapRank
            // 
            this.tapRank.Controls.Add(this.btnRankRemove);
            this.tapRank.Controls.Add(this.grpRankInfo);
            this.tapRank.Controls.Add(this.liRanks);
            this.tapRank.Controls.Add(this.grpEditRanks);
            this.tapRank.Controls.Add(this.chkAdmintoAdmin);
            this.tapRank.Controls.Add(this.chkOptoOp);
            this.tapRank.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tapRank.Location = new System.Drawing.Point(4, 22);
            this.tapRank.Name = "tapRank";
            this.tapRank.Padding = new System.Windows.Forms.Padding(3);
            this.tapRank.Size = new System.Drawing.Size(410, 417);
            this.tapRank.TabIndex = 1;
            this.tapRank.Text = "Rank";
            this.tapRank.UseVisualStyleBackColor = true;
            // 
            // btnRankRemove
            // 
            this.btnRankRemove.Location = new System.Drawing.Point(6, 377);
            this.btnRankRemove.Name = "btnRankRemove";
            this.btnRankRemove.Size = new System.Drawing.Size(175, 23);
            this.btnRankRemove.TabIndex = 42;
            this.btnRankRemove.Text = "Remove Rank";
            this.btnRankRemove.UseVisualStyleBackColor = true;
            this.btnRankRemove.Click += new System.EventHandler(this.btnRankRemove_Click);
            // 
            // grpRankInfo
            // 
            this.grpRankInfo.Controls.Add(this.liRankPeople);
            this.grpRankInfo.Controls.Add(this.label23);
            this.grpRankInfo.Controls.Add(this.txtRankPeople);
            this.grpRankInfo.Location = new System.Drawing.Point(187, 244);
            this.grpRankInfo.Name = "grpRankInfo";
            this.grpRankInfo.Size = new System.Drawing.Size(217, 165);
            this.grpRankInfo.TabIndex = 41;
            this.grpRankInfo.TabStop = false;
            this.grpRankInfo.Text = "Information";
            // 
            // liRankPeople
            // 
            this.liRankPeople.ContextMenuStrip = this.menuRankPeople;
            this.liRankPeople.FormattingEnabled = true;
            this.liRankPeople.Location = new System.Drawing.Point(6, 45);
            this.liRankPeople.Name = "liRankPeople";
            this.liRankPeople.ScrollAlwaysVisible = true;
            this.liRankPeople.Size = new System.Drawing.Size(201, 108);
            this.liRankPeople.TabIndex = 50;
            this.liRankPeople.MouseDown += new System.Windows.Forms.MouseEventHandler(this.liRankPeople_MouseDown);
            this.liRankPeople.MouseMove += new System.Windows.Forms.MouseEventHandler(this.liRankPeople_MouseMove);
            // 
            // menuRankPeople
            // 
            this.menuRankPeople.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itmRank});
            this.menuRankPeople.Name = "contextMenuStrip1";
            this.menuRankPeople.Size = new System.Drawing.Size(101, 26);
            this.menuRankPeople.Opening += new System.ComponentModel.CancelEventHandler(this.menuRankPeople_Opening);
            // 
            // itmRank
            // 
            this.itmRank.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.promoteToolStripMenuItem,
            this.demoteToolStripMenuItem,
            this.otherToolStripMenuItem});
            this.itmRank.Name = "itmRank";
            this.itmRank.Size = new System.Drawing.Size(100, 22);
            this.itmRank.Text = "Rank";
            // 
            // promoteToolStripMenuItem
            // 
            this.promoteToolStripMenuItem.Name = "promoteToolStripMenuItem";
            this.promoteToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.promoteToolStripMenuItem.Text = "Promote";
            this.promoteToolStripMenuItem.Click += new System.EventHandler(this.promoteToolStripMenuItem_Click);
            // 
            // demoteToolStripMenuItem
            // 
            this.demoteToolStripMenuItem.Name = "demoteToolStripMenuItem";
            this.demoteToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.demoteToolStripMenuItem.Text = "Demote";
            this.demoteToolStripMenuItem.Click += new System.EventHandler(this.demoteToolStripMenuItem_Click);
            // 
            // otherToolStripMenuItem
            // 
            this.otherToolStripMenuItem.Name = "otherToolStripMenuItem";
            this.otherToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.otherToolStripMenuItem.Text = "Other";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(108, 13);
            this.label23.TabIndex = 49;
            this.label23.Text = "People with this rank:";
            // 
            // txtRankPeople
            // 
            this.txtRankPeople.BackColor = System.Drawing.SystemColors.Window;
            this.txtRankPeople.Location = new System.Drawing.Point(120, 19);
            this.txtRankPeople.Name = "txtRankPeople";
            this.txtRankPeople.ReadOnly = true;
            this.txtRankPeople.Size = new System.Drawing.Size(91, 20);
            this.txtRankPeople.TabIndex = 49;
            // 
            // liRanks
            // 
            this.liRanks.ContextMenuStrip = this.menuRanks;
            this.liRanks.FormattingEnabled = true;
            this.liRanks.Location = new System.Drawing.Point(6, 55);
            this.liRanks.Name = "liRanks";
            this.liRanks.Size = new System.Drawing.Size(175, 316);
            this.liRanks.TabIndex = 40;
            this.liRanks.SelectedIndexChanged += new System.EventHandler(this.liRanks_SelectedIndexChanged);
            // 
            // menuRanks
            // 
            this.menuRanks.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reloadToolStripMenuItem});
            this.menuRanks.Name = "menuRanks";
            this.menuRanks.Size = new System.Drawing.Size(111, 26);
            // 
            // reloadToolStripMenuItem
            // 
            this.reloadToolStripMenuItem.Name = "reloadToolStripMenuItem";
            this.reloadToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.reloadToolStripMenuItem.Text = "Reload";
            this.reloadToolStripMenuItem.Click += new System.EventHandler(this.reloadToolStripMenuItem_Click);
            // 
            // grpEditRanks
            // 
            this.grpEditRanks.Controls.Add(this.lblRankColor);
            this.grpEditRanks.Controls.Add(this.btnRankNew);
            this.grpEditRanks.Controls.Add(this.btnRankSave);
            this.grpEditRanks.Controls.Add(this.cmdRankGroup);
            this.grpEditRanks.Controls.Add(this.label22);
            this.grpEditRanks.Controls.Add(this.label21);
            this.grpEditRanks.Controls.Add(this.label20);
            this.grpEditRanks.Controls.Add(this.label19);
            this.grpEditRanks.Controls.Add(this.cmbRankColor);
            this.grpEditRanks.Controls.Add(this.txtRankMaxBlocks);
            this.grpEditRanks.Controls.Add(this.txtRankPermission);
            this.grpEditRanks.Controls.Add(this.txtRankName);
            this.grpEditRanks.Controls.Add(this.label18);
            this.grpEditRanks.Location = new System.Drawing.Point(187, 55);
            this.grpEditRanks.Name = "grpEditRanks";
            this.grpEditRanks.Size = new System.Drawing.Size(217, 178);
            this.grpEditRanks.TabIndex = 39;
            this.grpEditRanks.TabStop = false;
            this.grpEditRanks.Text = "Edit Ranks";
            // 
            // lblRankColor
            // 
            this.lblRankColor.BackColor = System.Drawing.Color.Transparent;
            this.lblRankColor.Location = new System.Drawing.Point(191, 98);
            this.lblRankColor.Name = "lblRankColor";
            this.lblRankColor.Size = new System.Drawing.Size(19, 19);
            this.lblRankColor.TabIndex = 51;
            // 
            // btnRankNew
            // 
            this.btnRankNew.Location = new System.Drawing.Point(6, 149);
            this.btnRankNew.Name = "btnRankNew";
            this.btnRankNew.Size = new System.Drawing.Size(100, 23);
            this.btnRankNew.TabIndex = 50;
            this.btnRankNew.Text = "New Rank";
            this.btnRankNew.UseVisualStyleBackColor = true;
            this.btnRankNew.Click += new System.EventHandler(this.btnRankNew_Click);
            // 
            // btnRankSave
            // 
            this.btnRankSave.Location = new System.Drawing.Point(111, 149);
            this.btnRankSave.Name = "btnRankSave";
            this.btnRankSave.Size = new System.Drawing.Size(100, 23);
            this.btnRankSave.TabIndex = 49;
            this.btnRankSave.Text = "Save";
            this.btnRankSave.UseVisualStyleBackColor = true;
            this.btnRankSave.Click += new System.EventHandler(this.btnRankSave_Click);
            // 
            // cmdRankGroup
            // 
            this.cmdRankGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmdRankGroup.FormattingEnabled = true;
            this.cmdRankGroup.Location = new System.Drawing.Point(77, 125);
            this.cmdRankGroup.Name = "cmdRankGroup";
            this.cmdRankGroup.Size = new System.Drawing.Size(134, 21);
            this.cmdRankGroup.TabIndex = 48;
            this.cmdRankGroup.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(32, 128);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(39, 13);
            this.label22.TabIndex = 47;
            this.label22.Text = "Group:";
            this.label22.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(37, 100);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 13);
            this.label21.TabIndex = 46;
            this.label21.Text = "Color:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 48);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(60, 13);
            this.label20.TabIndex = 45;
            this.label20.Text = "Permission:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 44;
            this.label19.Text = "Max Blocks:";
            // 
            // cmbRankColor
            // 
            this.cmbRankColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRankColor.FormattingEnabled = true;
            this.cmbRankColor.Items.AddRange(new object[] {
            "black",
            "navy",
            "green",
            "teal",
            "maroon",
            "purple",
            "gold",
            "silver",
            "gray",
            "blue",
            "lime",
            "aqua",
            "red",
            "pink",
            "yellow",
            "white"});
            this.cmbRankColor.Location = new System.Drawing.Point(77, 97);
            this.cmbRankColor.Name = "cmbRankColor";
            this.cmbRankColor.Size = new System.Drawing.Size(108, 21);
            this.cmbRankColor.TabIndex = 43;
            this.cmbRankColor.SelectedIndexChanged += new System.EventHandler(this.cmbRankColor_SelectedIndexChanged);
            // 
            // txtRankMaxBlocks
            // 
            this.txtRankMaxBlocks.Location = new System.Drawing.Point(77, 71);
            this.txtRankMaxBlocks.Name = "txtRankMaxBlocks";
            this.txtRankMaxBlocks.Size = new System.Drawing.Size(134, 20);
            this.txtRankMaxBlocks.TabIndex = 42;
            this.txtRankMaxBlocks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRankMaxBlocks_KeyDown);
            // 
            // txtRankPermission
            // 
            this.txtRankPermission.Location = new System.Drawing.Point(77, 45);
            this.txtRankPermission.Name = "txtRankPermission";
            this.txtRankPermission.Size = new System.Drawing.Size(134, 20);
            this.txtRankPermission.TabIndex = 41;
            this.txtRankPermission.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRankPermission_KeyDown);
            // 
            // txtRankName
            // 
            this.txtRankName.Location = new System.Drawing.Point(77, 19);
            this.txtRankName.Name = "txtRankName";
            this.txtRankName.Size = new System.Drawing.Size(134, 20);
            this.txtRankName.TabIndex = 40;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(33, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 13);
            this.label18.TabIndex = 39;
            this.label18.Text = "Name:";
            // 
            // tapServer
            // 
            this.tapServer.Controls.Add(this.groupBoxMapInfo);
            this.tapServer.Controls.Add(this.groupBoxMisc);
            this.tapServer.Controls.Add(this.groupBoxAFK);
            this.tapServer.Controls.Add(this.groupBoxMain);
            this.tapServer.Controls.Add(this.lblName);
            this.tapServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tapServer.Location = new System.Drawing.Point(4, 22);
            this.tapServer.Name = "tapServer";
            this.tapServer.Padding = new System.Windows.Forms.Padding(3);
            this.tapServer.Size = new System.Drawing.Size(410, 417);
            this.tapServer.TabIndex = 0;
            this.tapServer.Text = "Server";
            this.tapServer.UseVisualStyleBackColor = true;
            // 
            // groupBoxMapInfo
            // 
            this.groupBoxMapInfo.Controls.Add(this.label17);
            this.groupBoxMapInfo.Controls.Add(this.txtMainLevel);
            this.groupBoxMapInfo.Controls.Add(this.lblServerMaxMaps);
            this.groupBoxMapInfo.Controls.Add(this.txtServerMaxMaps);
            this.groupBoxMapInfo.Controls.Add(this.lblServerBackupTime);
            this.groupBoxMapInfo.Controls.Add(this.txtBackup);
            this.groupBoxMapInfo.Location = new System.Drawing.Point(185, 318);
            this.groupBoxMapInfo.Name = "groupBoxMapInfo";
            this.groupBoxMapInfo.Size = new System.Drawing.Size(213, 91);
            this.groupBoxMapInfo.TabIndex = 71;
            this.groupBoxMapInfo.TabStop = false;
            this.groupBoxMapInfo.Text = "Map Information";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 68);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 67;
            this.label17.Text = "Main Level:";
            // 
            // txtMainLevel
            // 
            this.txtMainLevel.Location = new System.Drawing.Point(85, 65);
            this.txtMainLevel.Name = "txtMainLevel";
            this.txtMainLevel.Size = new System.Drawing.Size(122, 20);
            this.txtMainLevel.TabIndex = 66;
            // 
            // lblServerMaxMaps
            // 
            this.lblServerMaxMaps.AutoSize = true;
            this.lblServerMaxMaps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerMaxMaps.Location = new System.Drawing.Point(6, 45);
            this.lblServerMaxMaps.Name = "lblServerMaxMaps";
            this.lblServerMaxMaps.Size = new System.Drawing.Size(59, 13);
            this.lblServerMaxMaps.TabIndex = 0;
            this.lblServerMaxMaps.Text = "Max Maps:";
            // 
            // lblServerBackupTime
            // 
            this.lblServerBackupTime.AutoSize = true;
            this.lblServerBackupTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerBackupTime.Location = new System.Drawing.Point(6, 22);
            this.lblServerBackupTime.Name = "lblServerBackupTime";
            this.lblServerBackupTime.Size = new System.Drawing.Size(73, 13);
            this.lblServerBackupTime.TabIndex = 0;
            this.lblServerBackupTime.Text = "Backup Time:";
            // 
            // groupBoxMisc
            // 
            this.groupBoxMisc.Controls.Add(this.chkHelpPortforward);
            this.groupBoxMisc.Controls.Add(this.chkPortforward);
            this.groupBoxMisc.Controls.Add(this.chkGeolocation);
            this.groupBoxMisc.Controls.Add(this.lblDefaultColor);
            this.groupBoxMisc.Controls.Add(this.chkAchievements);
            this.groupBoxMisc.Controls.Add(this.chkRestartOnError);
            this.groupBoxMisc.Controls.Add(this.lblServerDefaultColor);
            this.groupBoxMisc.Controls.Add(this.cmbDefaultColor);
            this.groupBoxMisc.Location = new System.Drawing.Point(9, 228);
            this.groupBoxMisc.Name = "groupBoxMisc";
            this.groupBoxMisc.Size = new System.Drawing.Size(389, 84);
            this.groupBoxMisc.TabIndex = 69;
            this.groupBoxMisc.TabStop = false;
            this.groupBoxMisc.Text = "Misc.";
            // 
            // lblDefaultColor
            // 
            this.lblDefaultColor.BackColor = System.Drawing.Color.Transparent;
            this.lblDefaultColor.Location = new System.Drawing.Point(180, 52);
            this.lblDefaultColor.Name = "lblDefaultColor";
            this.lblDefaultColor.Size = new System.Drawing.Size(19, 19);
            this.lblDefaultColor.TabIndex = 0;
            // 
            // lblServerDefaultColor
            // 
            this.lblServerDefaultColor.AutoSize = true;
            this.lblServerDefaultColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerDefaultColor.Location = new System.Drawing.Point(6, 55);
            this.lblServerDefaultColor.Name = "lblServerDefaultColor";
            this.lblServerDefaultColor.Size = new System.Drawing.Size(71, 13);
            this.lblServerDefaultColor.TabIndex = 0;
            this.lblServerDefaultColor.Text = "Default Color:";
            // 
            // groupBoxAFK
            // 
            this.groupBoxAFK.Controls.Add(this.lblServerAFKKick);
            this.groupBoxAFK.Controls.Add(this.txtAFKKick);
            this.groupBoxAFK.Controls.Add(this.lblServerAFKTimer);
            this.groupBoxAFK.Controls.Add(this.txtAFKTimer);
            this.groupBoxAFK.Location = new System.Drawing.Point(9, 318);
            this.groupBoxAFK.Name = "groupBoxAFK";
            this.groupBoxAFK.Size = new System.Drawing.Size(170, 91);
            this.groupBoxAFK.TabIndex = 70;
            this.groupBoxAFK.TabStop = false;
            this.groupBoxAFK.Text = "AFK Options";
            // 
            // lblServerAFKKick
            // 
            this.lblServerAFKKick.AutoSize = true;
            this.lblServerAFKKick.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerAFKKick.Location = new System.Drawing.Point(6, 63);
            this.lblServerAFKKick.Name = "lblServerAFKKick";
            this.lblServerAFKKick.Size = new System.Drawing.Size(54, 13);
            this.lblServerAFKKick.TabIndex = 0;
            this.lblServerAFKKick.Text = "AFK Kick:";
            // 
            // lblServerAFKTimer
            // 
            this.lblServerAFKTimer.AutoSize = true;
            this.lblServerAFKTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerAFKTimer.Location = new System.Drawing.Point(6, 37);
            this.lblServerAFKTimer.Name = "lblServerAFKTimer";
            this.lblServerAFKTimer.Size = new System.Drawing.Size(59, 13);
            this.lblServerAFKTimer.TabIndex = 0;
            this.lblServerAFKTimer.Text = "AFK Timer:";
            // 
            // groupBoxMain
            // 
            this.groupBoxMain.Controls.Add(this.lblServerName);
            this.groupBoxMain.Controls.Add(this.txtServerName);
            this.groupBoxMain.Controls.Add(this.lblMOTD);
            this.groupBoxMain.Controls.Add(this.txtServerMOTD);
            this.groupBoxMain.Controls.Add(this.label16);
            this.groupBoxMain.Controls.Add(this.txtServerDescription);
            this.groupBoxMain.Controls.Add(this.chkPublic);
            this.groupBoxMain.Controls.Add(this.chkGuestGoto);
            this.groupBoxMain.Controls.Add(this.txtServerOwnerLogin);
            this.groupBoxMain.Controls.Add(this.lblServerOwnerLogin);
            this.groupBoxMain.Controls.Add(this.lblServerMaxPlayers);
            this.groupBoxMain.Controls.Add(this.txtServerMaxPlayers);
            this.groupBoxMain.Controls.Add(this.chkVerifyNames);
            this.groupBoxMain.Controls.Add(this.chkWorldChat);
            this.groupBoxMain.Controls.Add(this.lblServerOwner);
            this.groupBoxMain.Controls.Add(this.txtServerPort);
            this.groupBoxMain.Controls.Add(this.txtServerOwner);
            this.groupBoxMain.Controls.Add(this.lblPort);
            this.groupBoxMain.Location = new System.Drawing.Point(9, 6);
            this.groupBoxMain.Name = "groupBoxMain";
            this.groupBoxMain.Size = new System.Drawing.Size(389, 216);
            this.groupBoxMain.TabIndex = 71;
            this.groupBoxMain.TabStop = false;
            this.groupBoxMain.Text = "Server Information";
            // 
            // lblServerName
            // 
            this.lblServerName.AutoSize = true;
            this.lblServerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerName.Location = new System.Drawing.Point(6, 22);
            this.lblServerName.Name = "lblServerName";
            this.lblServerName.Size = new System.Drawing.Size(38, 13);
            this.lblServerName.TabIndex = 0;
            this.lblServerName.Text = "Name:";
            // 
            // lblMOTD
            // 
            this.lblMOTD.AutoSize = true;
            this.lblMOTD.Location = new System.Drawing.Point(6, 48);
            this.lblMOTD.Name = "lblMOTD";
            this.lblMOTD.Size = new System.Drawing.Size(42, 13);
            this.lblMOTD.TabIndex = 0;
            this.lblMOTD.Text = "MOTD:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 100);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 63;
            this.label16.Text = "Description:";
            // 
            // lblServerOwnerLogin
            // 
            this.lblServerOwnerLogin.AutoSize = true;
            this.lblServerOwnerLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerOwnerLogin.Location = new System.Drawing.Point(6, 74);
            this.lblServerOwnerLogin.Name = "lblServerOwnerLogin";
            this.lblServerOwnerLogin.Size = new System.Drawing.Size(67, 13);
            this.lblServerOwnerLogin.TabIndex = 0;
            this.lblServerOwnerLogin.Text = "OwnerLogin:";
            // 
            // lblServerMaxPlayers
            // 
            this.lblServerMaxPlayers.AutoSize = true;
            this.lblServerMaxPlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerMaxPlayers.Location = new System.Drawing.Point(6, 152);
            this.lblServerMaxPlayers.Name = "lblServerMaxPlayers";
            this.lblServerMaxPlayers.Size = new System.Drawing.Size(67, 13);
            this.lblServerMaxPlayers.TabIndex = 0;
            this.lblServerMaxPlayers.Text = "Max Players:";
            // 
            // lblServerOwner
            // 
            this.lblServerOwner.AutoSize = true;
            this.lblServerOwner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerOwner.Location = new System.Drawing.Point(6, 126);
            this.lblServerOwner.Name = "lblServerOwner";
            this.lblServerOwner.Size = new System.Drawing.Size(41, 13);
            this.lblServerOwner.TabIndex = 0;
            this.lblServerOwner.Text = "Owner:";
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPort.Location = new System.Drawing.Point(268, 152);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(29, 13);
            this.lblPort.TabIndex = 0;
            this.lblPort.Text = "Port:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(13, 17);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(0, 11);
            this.lblName.TabIndex = 0;
            // 
            // lblIRCPort
            // 
            this.lblIRCPort.AutoSize = true;
            this.lblIRCPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIRCPort.Location = new System.Drawing.Point(26, 131);
            this.lblIRCPort.Name = "lblIRCPort";
            this.lblIRCPort.Size = new System.Drawing.Size(29, 13);
            this.lblIRCPort.TabIndex = 0;
            this.lblIRCPort.Text = "Port:";
            // 
            // lblIRCChannel
            // 
            this.lblIRCChannel.AutoSize = true;
            this.lblIRCChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIRCChannel.Location = new System.Drawing.Point(6, 53);
            this.lblIRCChannel.Name = "lblIRCChannel";
            this.lblIRCChannel.Size = new System.Drawing.Size(49, 13);
            this.lblIRCChannel.TabIndex = 0;
            this.lblIRCChannel.Text = "Channel:";
            // 
            // lblIRCServer
            // 
            this.lblIRCServer.AutoSize = true;
            this.lblIRCServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIRCServer.Location = new System.Drawing.Point(14, 79);
            this.lblIRCServer.Name = "lblIRCServer";
            this.lblIRCServer.Size = new System.Drawing.Size(41, 13);
            this.lblIRCServer.TabIndex = 0;
            this.lblIRCServer.Text = "Server:";
            // 
            // lblIRCNick
            // 
            this.lblIRCNick.AutoSize = true;
            this.lblIRCNick.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIRCNick.Location = new System.Drawing.Point(23, 105);
            this.lblIRCNick.Name = "lblIRCNick";
            this.lblIRCNick.Size = new System.Drawing.Size(32, 13);
            this.lblIRCNick.TabIndex = 0;
            this.lblIRCNick.Text = "Nick:";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tapServer);
            this.tabControl.Controls.Add(this.tabIRC);
            this.tabControl.Controls.Add(this.tapRank);
            this.tabControl.Controls.Add(this.tabAntiGrief);
            this.tabControl.Controls.Add(this.tabUpdate);
            this.tabControl.Controls.Add(this.tabCommands);
            this.tabControl.Controls.Add(this.tabBlocks);
            this.tabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.Location = new System.Drawing.Point(14, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(418, 443);
            this.tabControl.TabIndex = 0;
            // 
            // tabIRC
            // 
            this.tabIRC.Controls.Add(this.grpIRC);
            this.tabIRC.Location = new System.Drawing.Point(4, 22);
            this.tabIRC.Name = "tabIRC";
            this.tabIRC.Size = new System.Drawing.Size(410, 417);
            this.tabIRC.TabIndex = 6;
            this.tabIRC.Text = "IRC";
            this.tabIRC.UseVisualStyleBackColor = true;
            // 
            // grpIRC
            // 
            this.grpIRC.Controls.Add(this.grpIRCOptional);
            this.grpIRC.Controls.Add(this.chkIRC);
            this.grpIRC.Controls.Add(this.lblIRCChannel);
            this.grpIRC.Controls.Add(this.txtIRCChannel);
            this.grpIRC.Controls.Add(this.lblIRCPort);
            this.grpIRC.Controls.Add(this.txtIRCPort);
            this.grpIRC.Controls.Add(this.lblIRCServer);
            this.grpIRC.Controls.Add(this.txtIRCServer);
            this.grpIRC.Controls.Add(this.lblIRCNick);
            this.grpIRC.Controls.Add(this.txtIRCNick);
            this.grpIRC.Location = new System.Drawing.Point(3, 3);
            this.grpIRC.Name = "grpIRC";
            this.grpIRC.Size = new System.Drawing.Size(404, 411);
            this.grpIRC.TabIndex = 68;
            this.grpIRC.TabStop = false;
            this.grpIRC.Text = "IRC Controls";
            // 
            // grpIRCOptional
            // 
            this.grpIRCOptional.Controls.Add(this.chkIRCShowPassword);
            this.grpIRCOptional.Controls.Add(this.chkIRCIdentify);
            this.grpIRCOptional.Controls.Add(this.txtIRCPassword);
            this.grpIRCOptional.Controls.Add(this.label25);
            this.grpIRCOptional.Location = new System.Drawing.Point(123, 191);
            this.grpIRCOptional.Name = "grpIRCOptional";
            this.grpIRCOptional.Size = new System.Drawing.Size(275, 97);
            this.grpIRCOptional.TabIndex = 18;
            this.grpIRCOptional.TabStop = false;
            this.grpIRCOptional.Text = "Optional";
            // 
            // chkIRCShowPassword
            // 
            this.chkIRCShowPassword.AutoSize = true;
            this.chkIRCShowPassword.Location = new System.Drawing.Point(68, 74);
            this.chkIRCShowPassword.Name = "chkIRCShowPassword";
            this.chkIRCShowPassword.Size = new System.Drawing.Size(53, 17);
            this.chkIRCShowPassword.TabIndex = 19;
            this.chkIRCShowPassword.Text = "Show";
            this.chkIRCShowPassword.UseVisualStyleBackColor = true;
            this.chkIRCShowPassword.CheckedChanged += new System.EventHandler(this.chkIRCShowPassword_CheckedChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(6, 51);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(56, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Password:";
            // 
            // PropertiesWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 491);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnDiscard);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PropertiesWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "PropertiesWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PropertiesWindow_Unload);
            this.Load += new System.EventHandler(this.PropertiesWindow_Load);
            this.tabBlocks.ResumeLayout(false);
            this.tabBlocks.PerformLayout();
            this.tabCommands.ResumeLayout(false);
            this.tabCommands.PerformLayout();
            this.tabUpdate.ResumeLayout(false);
            this.tabUpdate.PerformLayout();
            this.tabAntiGrief.ResumeLayout(false);
            this.tabAntiGrief.PerformLayout();
            this.grpboxGriefSys.ResumeLayout(false);
            this.grpboxGriefSys.PerformLayout();
            this.tapRank.ResumeLayout(false);
            this.grpRankInfo.ResumeLayout(false);
            this.grpRankInfo.PerformLayout();
            this.menuRankPeople.ResumeLayout(false);
            this.menuRanks.ResumeLayout(false);
            this.grpEditRanks.ResumeLayout(false);
            this.grpEditRanks.PerformLayout();
            this.tapServer.ResumeLayout(false);
            this.tapServer.PerformLayout();
            this.groupBoxMapInfo.ResumeLayout(false);
            this.groupBoxMapInfo.PerformLayout();
            this.groupBoxMisc.ResumeLayout(false);
            this.groupBoxMisc.PerformLayout();
            this.groupBoxAFK.ResumeLayout(false);
            this.groupBoxAFK.PerformLayout();
            this.groupBoxMain.ResumeLayout(false);
            this.groupBoxMain.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabIRC.ResumeLayout(false);
            this.grpIRC.ResumeLayout(false);
            this.grpIRC.PerformLayout();
            this.grpIRCOptional.ResumeLayout(false);
            this.grpIRCOptional.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CButton btnSave;
        private CButton btnDiscard;
        private CButton btnApply;
        private System.Windows.Forms.ToolTip tt;
        private TabPage tabBlocks;
        private RichTextBox rtxtBLKRanks;
        private TextBox txtBLKLowest;
        private Label lblBLKLowest;
        private ListBox liBlocks;
        private TabPage tabCommands;
        private CheckBox chkHelp;
        private RichTextBox rtxtCMDRanks;
        private TextBox txtCMDLowest;
        private Label lblCMDLowest;
        private ListBox liCommands;
        private TabPage tabUpdate;
        private ComboBox cmbForceAfter;
        private Label label14;
        private ComboBox cmbChkHour;
        private Label label15;
        private CheckBox chkUpdate;
        private CheckBox chkForceUpdate;
        private ListBox liUpdate;
        private TabPage tabAntiGrief;
        private CheckBox chkExtraban;
        private GroupBox grpboxGriefSys;
        private ComboBox cmbGrief2;
        private Label label5;
        private ComboBox cmbGriefTimes;
        private ComboBox cmbGrief1;
        private Label label4;
        private CheckBox chkOpNotify;
        private CheckBox chkAntiGrief;
        private Label label12;
        private Label label3;
        private Label label13;
        private TextBox txtSpamBlockTimer;
        private Label label2;
        private TextBox txtSpamBlockCount;
        private Label label1;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label label10;
        private Label label11;
        private CheckBox ChkTunnels;
        private Label lblServerDepth;
        private TextBox txtMaxDepth;
        private TextBox txtServerOverload;
        private Label lblServerOverload;
        private TabPage tapRank;
        private CheckBox chkAdmintoAdmin;
        private CheckBox chkOptoOp;
        private TabPage tapServer;
        private Label lblServerName;
        private CheckBox chkGeolocation;
        private TextBox txtServerOwnerLogin;
        private TextBox txtServerOwner;
        private TextBox txtIRCPort;
        private TextBox txtAFKKick;
        private TextBox txtAFKTimer;
        private TextBox txtBackup;
        private TextBox txtServerMaxMaps;
        private TextBox txtServerMaxPlayers;
        private TextBox txtIRCChannel;
        private TextBox txtIRCServer;
        private TextBox txtIRCNick;
        private TextBox txtServerPort;
        private TextBox txtServerMOTD;
        private TextBox txtServerName;
        private Label lblServerOwnerLogin;
        private CheckBox chkGuestGoto;
        private Label lblServerOwner;
        private Label lblIRCPort;
        private Label lblDefaultColor;
        private ComboBox cmbDefaultColor;
        private CheckBox chkIRC;
        private Label lblServerDefaultColor;
        private Label lblServerAFKKick;
        private Label lblServerAFKTimer;
        private Label lblServerBackupTime;
        private Label lblServerMaxMaps;
        private Label lblServerMaxPlayers;
        private Label lblIRCChannel;
        private Label lblIRCServer;
        private Label lblIRCNick;
        private CheckBox chkVerifyNames;
        private CheckBox chkPublic;
        private CheckBox chkWorldChat;
        private Label lblPort;
        private Label lblMOTD;
        private Label lblName;
        private TabControl tabControl;
        private Label label16;
        private TextBox txtServerDescription;
        private CheckBox chkAchievements;
        private CheckBox chkRestartOnError;
        private Label label17;
        private TextBox txtMainLevel;
        private GroupBox groupBoxMain;
        private GroupBox grpIRC;
        private GroupBox groupBoxMapInfo;
        private GroupBox groupBoxMisc;
        private GroupBox groupBoxAFK;
        private TabPage tabIRC;
        private ListBox liRanks;
        private GroupBox grpEditRanks;
        private TextBox txtRankMaxBlocks;
        private TextBox txtRankPermission;
        private TextBox txtRankName;
        private Label label18;
        private ComboBox cmbRankColor;
        private Label label19;
        private Label label21;
        private Label label20;
        private GroupBox grpRankInfo;
        private ComboBox cmdRankGroup;
        private Label label22;
        private TextBox txtRankPeople;
        private ListBox liRankPeople;
        private Label label23;
        private Label lblRankColor;
        private CheckBox chkPortforward;
        private CheckBox chkNewlvlHelp;
        private ContextMenuStrip menuRankPeople;
        private ToolStripMenuItem itmRank;
        private ToolStripMenuItem promoteToolStripMenuItem;
        private ToolStripMenuItem demoteToolStripMenuItem;
        private ToolStripMenuItem otherToolStripMenuItem;
        private ContextMenuStrip menuRanks;
        private ToolStripMenuItem reloadToolStripMenuItem;
        private CheckBox chkHelpPortforward;
        private Label label25;
        private TextBox txtIRCPassword;
        private GroupBox grpIRCOptional;
        private CheckBox chkIRCIdentify;
        private CheckBox chkIRCShowPassword;
        private CButton btnBLKHelp;
        private CButton btnCMDHelp;
        private CButton btnUpdate;
        private CButton btnRankNew;
        private CButton btnRankSave;
        private CButton btnRankRemove;

    }
}