﻿namespace MCRevive.Gui
{
    partial class RulesChangingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRules = new System.Windows.Forms.TextBox();
            this.RulesLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDiscard = new CButton();
            this.btnApply = new CButton();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRules
            // 
            this.txtRules.Location = new System.Drawing.Point(12, 29);
            this.txtRules.Multiline = true;
            this.txtRules.Name = "txtRules";
            this.txtRules.Size = new System.Drawing.Size(347, 203);
            this.txtRules.TabIndex = 0;
            // 
            // RulesLabel
            // 
            this.RulesLabel.AutoSize = true;
            this.RulesLabel.Location = new System.Drawing.Point(13, 13);
            this.RulesLabel.Name = "RulesLabel";
            this.RulesLabel.Size = new System.Drawing.Size(149, 13);
            this.RulesLabel.TabIndex = 1;
            this.RulesLabel.Text = "Please enter your server rules:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDiscard);
            this.panel1.Controls.Add(this.btnApply);
            this.panel1.Location = new System.Drawing.Point(12, 239);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(347, 45);
            this.panel1.TabIndex = 2;
            // 
            // btnDiscard
            // 
            this.btnDiscard.Location = new System.Drawing.Point(191, 13);
            this.btnDiscard.Name = "btnDiscard";
            this.btnDiscard.Size = new System.Drawing.Size(72, 23);
            this.btnDiscard.TabIndex = 2;
            this.btnDiscard.Text = "Discard";
            this.btnDiscard.UseVisualStyleBackColor = true;
            this.btnDiscard.Click += new System.EventHandler(this.btnDiscard_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(269, 13);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // RulesChangingWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 287);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.RulesLabel);
            this.Controls.Add(this.txtRules);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "RulesChangingWindow";
            this.Text = "Rules Changing Window";
            this.Load += new System.EventHandler(this.RulesChangingWindow_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRules;
        private System.Windows.Forms.Label RulesLabel;
        private System.Windows.Forms.Panel panel1;
        private CButton btnDiscard;
        private CButton btnApply;
    }
}