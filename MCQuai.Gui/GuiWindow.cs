﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Drawing;
using MonoTorrent.Client;
using System.ComponentModel;

namespace MCRevive.Gui
{
    public partial class GuiWindow : Form
    {
        public static MainLoop ml;

        public static bool ValidString(string str, string allowed)
        {
            string allowedchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890" + allowed;
            foreach (char ch in str)
            {
                if (allowedchars.IndexOf(ch) == -1)
                {
                    return false;
                }
            } return true;
        }

        Regex regex = new Regex(@"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\." +
                                "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$");
        // for cross thread use
        delegate void StringCallback(string s);
        delegate void LevelListCallBack(List<Level> levels);
        delegate void PlayerListCallback(List<Player> players);
        delegate void VoidDelegate();
        delegate void DoubleDelegate(double prg);
        System.Timers.Timer update = new System.Timers.Timer(1000);
        private string SelectedTab = "Main"; 

        //Selection Window
        SelectionWindow Selection;
        public static bool RankWindowUse = false;
        public static bool MJSWindowUse = false;

        //Properties Window
        Form PropertiesWindowForm;
        public static bool PropertiesWindowUse = false;

        //Extra Tab
        System.Windows.Forms.Timer updateGraphsTimer = new System.Windows.Forms.Timer();
        System.Timers.Timer MapTimer = new System.Timers.Timer(10000);
        private List<int> PlayerGraphList = new List<int>();
        private List<int> trafficsOut = new List<int>();
        private List<int> trafficsIn = new List<int>();
        private int trafficScale1 = 10, trafficScale2 = 20;
        private int playerScale1 = 10, playerScale2 = 20, playerScale3 = 30;

        //Other stuff
        System.Timers.Timer ServerlistTimer = new System.Timers.Timer(5000);
        string serverLogPath = "";
        string errorLogPath = "";

        public GuiWindow(string log = "", string cmdlog = "", string url = "")
        {
            InitializeComponent();
            SettingsUpdate();
            txtLog.Text = log;
            txtCmdLog.Text = cmdlog;
            txtUrl.Text = url;
        }

        private void Window_Load(object sender, EventArgs e)
        {
            PropertiesWindowUse = true;
            Server.s.OnLog += WriteLine;
            Server.s.OnCommand += newCommand;
            Server.s.OnMJS += newMJSLog;
            Server.s.HeartBeatFail += HeartBeatFail;
            Server.s.OnURLChange += UpdateUrl;
            Server.s.OnPlayerListChange += UpdateClientList;
            Server.s.OnSettingsUpdate += SettingsUpdate;
            Server.s.OnRanksChange += UpdateRanks;

            liLevels.Items.Add(Server.mainLevel.name);
            liMaps.Items.Add(Server.mainLevel.name);
            dtpError.Text = dtpError.Text;
            dtpLog.Text = dtpLog.Text;
            txtConsoleName.Text = Server.consolename;

            MapTimer.Elapsed += delegate { UpdateLevelList("'"); };
            MapTimer.Start();
            trafficsIn.Add(0);
            trafficsOut.Add(0);
            PlayerGraphList.Add(0);
            updateGraphsTimer.Tick += delegate { DrawPlayerGraph(); DrawTrafficGraph(); };
            updateGraphsTimer.Interval = 5000;
            updateGraphsTimer.Start();
            ServerlistTimer.Elapsed += delegate
            {
                ServerlistTimer.Interval = 60000;
                try
                {
                    string g = Server.DownloadString("http://www.mcrevive.tk/functions/servers.php");
                    if (g.Length > 2)
                    {
                        dgvServers.Rows.Clear();
                        foreach (string line in g.Split('\n'))
                        {
                            if (line.Length < 2) continue;
                            string[] r = line.Split('性');
                            dgvServersAddRow(r[0], r[1] + "/" + r[2], r[3], r[5]);
                        }
                    }
                }
                catch { Server.s.Log("Could not recieve serverlist from mcrevive.tk"); }
            };
            ServerlistTimer.Start();

            Routines.load();
            loadRoutines();
            UpdateRanks();

            if (!Server.Updated)
                if (Server.updateCheck)
                    Server.s.Log("Update found!");
            try
            {
                string newest = Server.DownloadString("http://www.mcrevive.tk/download/_Update.php");
                string changelog = "Can't get changelog...";
                try
                {
                    changelog = Server.DownloadString("http://www.mcrevive.tk/tools/getchangelog.php?v=" + Server.Version);
                    rtxtChangelog1.Text = changelog;
                    ChangelogFormat(rtxtChangelog1);
                }
                catch { }
                rtxtChangelog1.Text = changelog;
                if (newest != Server.Version)
                {
                    string changelogsince = Server.DownloadString("http://www.mcrevive.tk/tools/getchangelog.php?s=1&v=" + Server.Version);
                    rtxtChangelog2.Text = changelogsince;
                    ChangelogFormat(rtxtChangelog2);
                }
                else
                {
                    grpChangelogNew.Visible = false;
                    grpChangelog.Height = 528;
                    rtxtChangelog1.Height = 503;
                }
            }
            catch (Exception ex) { Server.s.Log(ex.ToString()); Server.s.Log("Failed to load changelog. Connection problems?"); }

            PropertiesWindowUse = false;

            Thread t = new Thread(new ThreadStart(delegate
            {
                if (Server.shuttingdown) { Thread.Sleep(10000); Server.Exit(); }
            }));
            t.Start();
        }
        void SettingsUpdate()
        {
            if (Server.shuttingdown) return;
            if (this.InvokeRequired)
            {
                VoidDelegate d = new VoidDelegate(SettingsUpdate);
                this.Invoke(d);
            }
            else
            {
                this.Text = Server.name + " - R" + Server.Version;
            }
        }

        void HeartBeatFail()
        {
            WriteLine("Recent Heartbeat Failed");
        }

        delegate void LogDelegate(string message);
        delegate void DVGDelegate(string name, string players, string owner, string description);

        public void WriteLine(string s)
        {
            if (txtLog.InvokeRequired)
            {
                LogDelegate d = new LogDelegate(WriteLine);
                this.Invoke(d, new object[] { s });
            }
            else
                txtLog.AppendText(Environment.NewLine + s);
        }
        public void newCommand(string p)
        {
            if (txtCmdLog.InvokeRequired)
            {
                LogDelegate d = new LogDelegate(newCommand);
                this.Invoke(d, new object[] { p });
            }
            else
                txtCmdLog.AppendText(Environment.NewLine + p);
        }
        public void newMJSLog(string p)
        {
            Program.MJSLog += Environment.NewLine + p;
        }
        public void dgvServersAddRow(string name, string players, string owner, string description)
        {
            if (dgvServers.InvokeRequired)
            {
                DVGDelegate d = new DVGDelegate(dgvServersAddRow);
                this.Invoke(d, new object[] { name, players, owner, description });
            }
            else
                dgvServers.Rows.Add(name, players, owner, description);
        }


        /// <summary>
        /// Updates the list of client names in the window
        /// </summary>
        /// <param name="players">The list of players to add</param>
        public void UpdateClientList(List<Player> players)
        {
            if (liClients.InvokeRequired)
            {
                PlayerListCallback d = new PlayerListCallback(UpdateClientList);
                this.Invoke(d, new object[] { players });
            }
            else
            {
                liClients.Items.Clear();
                Player.players.ForEach(delegate(Player p) { liClients.Items.Add(p.name); });
            }
        }

        public void UpdateLevelList(string blah)
        {
            if (liLevels.InvokeRequired)
            {
                LogDelegate ld = new LogDelegate(UpdateLevelList);
                this.Invoke(ld, new object[] { blah });
            }
            else
            {
                int oldLSelected = liLevels.SelectedIndex;
                int oldMSelected = liMaps.SelectedIndex;
                liLevels.Items.Clear();
                liMaps.Items.Clear();
                sendToMapToolStripMenuItem.DropDownItems.Clear();
                Server.levels.ForEach((lvl) =>
                {
                    liLevels.Items.Add(lvl.name);
                    liMaps.Items.Add(lvl.name);
                    sendToMapToolStripMenuItem.DropDownItems.Add(lvl.name, null, new EventHandler(liClientsSendMap_Click));
                });
                liMaps.SelectedIndex = oldMSelected >= liMaps.Items.Count ? 0 : oldMSelected;
                liLevels.SelectedIndex = oldLSelected >= liMaps.Items.Count ? 0 : oldLSelected;
            }
        }
        /// <summary>
        /// Places the server's URL at the top of the window
        /// </summary>
        /// <param name="s">The URL to display</param>
        public void UpdateUrl(string s)
        {
            if (txtUrl.InvokeRequired)
            {
                StringCallback d = new StringCallback(UpdateUrl);
                this.Invoke(d, new object[] { s });
            }
            else
                txtUrl.Text = s;
        }

        private void UpdateRanks()
        {
            otherToolStripMenuItem.DropDownItems.Clear();
            visitPermissionToolStripMenuItem.DropDownItems.Clear();
            buildPermissionToolStripMenuItem.DropDownItems.Clear();
            Group.GroupList.ForEach((grp) =>
            {
                string name = Char.ToUpper(grp.name[0]) + grp.name.Substring(1).ToLower();
                Bitmap img = new Bitmap(2, 2); for (int x = 0; x < 2; ++x) for (int y = 0; y < 2; ++y) img.SetPixel(x, y, Color.FromName(c.Name(grp.color)));

                if (grp.Permission < 120) otherToolStripMenuItem.DropDownItems.Add(name, img, new EventHandler(liClientsRank_Click));
                if (!cancelMapMenu) visitPermissionToolStripMenuItem.DropDownItems.Add(name, img, new EventHandler(liLevelsVisit_Click));
                if (!cancelMapMenu) buildPermissionToolStripMenuItem.DropDownItems.Add(name, img, new EventHandler(liLevelsBuild_Click));
            });
        }

        private void tabAll_SelectedIndexChanged(object sender, EventArgs e)
        {
            Application.DoEvents();
            SelectedTab = tabAll.SelectedTab.Text;
            if (SelectedTab == "Main")
            {
                txtLog.SelectionStart = txtLog.Text.Length;
                txtLog.ScrollToCaret();
                txtCmdLog.SelectionStart = txtCmdLog.Text.Length;
                txtCmdLog.ScrollToCaret();
            }
            else if (SelectedTab == "Maps")
            {
                if (MapShown != "") mdpMap.DrawMap(MapShown);
                else if (liMaps.Items.Count != 0)
                    liMaps.SelectedIndex = 0;
            }
            else if (SelectedTab == "System Logs")
            {
                dtpError_ValueChanged(null, null);
                dtpLog_ValueChanged(null, null);
            }
            else if (SelectedTab == "Extra")
            {
                DrawPlayerGraph();
                DrawTrafficGraph();
            }
        }

        private void Window_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Server.forceShutdown)
            {
                e.Cancel = MessageBox.Show("Are you sure you want to shut down the server?", "Exit", MessageBoxButtons.YesNo) != DialogResult.Yes;
                if (e.Cancel) return;
            }
            Routines.save();
            Server.shuttingdown = true;
            tabAll.SelectedIndex = 0;
            ServerlistTimer.Stop();
            updateGraphsTimer.Stop();
            Server.Exit();
        }
        #region Main Tab
        bool cancelPlayerMenu = false;
        bool cancelMapMenu = false;

        private void menuLevels_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelMapMenu)
            {
                liLevels.SelectedIndex = -1;
                unloadToolStripMenuItem.Enabled = false;
                saveToolStripMenuItem.Enabled = false;
                visitPermissionToolStripMenuItem.Enabled = false;
                visitPermissionToolStripMenuItem.DropDownItems.Clear();
                buildPermissionToolStripMenuItem.Enabled = false;
                buildPermissionToolStripMenuItem.DropDownItems.Clear();
            }
            else
            {
                UpdateRanks();
                unloadToolStripMenuItem.Enabled = true;
                saveToolStripMenuItem.Enabled = true;
                visitPermissionToolStripMenuItem.Enabled = true;
                buildPermissionToolStripMenuItem.Enabled = true;
            }
        }
        private void menuPlayers_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelPlayerMenu) { liClients.SelectedIndex = -1; e.Cancel = true; }
        }

        private void liClients_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (liClients.SelectedIndex != -1)
            {
                promoteToolStripMenuItem.Enabled = (Group.GroupList.IndexOf(Group.findPlayerGroup(liClients.SelectedItem + "")) != Group.GroupList.Count - 1);
                demoteToolStripMenuItem.Enabled = (Group.GroupList.IndexOf(Group.findPlayerGroup(liClients.SelectedItem + "")) != 0);
            }
        }
        private void liClients_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int index = liClients.IndexFromPoint(new Point(e.X, e.Y));
                if (index != -1)
                {
                    liClients.SelectedIndex = index;
                    Server.levels.ForEach((l) =>
                    {
                        for (int i = 0; i < sendToMapToolStripMenuItem.DropDownItems.Count; ++i)
                            if (sendToMapToolStripMenuItem.DropDownItems[i].Text == l.name)
                                sendToMapToolStripMenuItem.DropDownItems[i].Enabled = false;
                            else
                                sendToMapToolStripMenuItem.DropDownItems[i].Enabled = true;
                    });
                    cancelPlayerMenu = false;
                }
                else cancelPlayerMenu = true;
            }
        }
        private void liClients_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int index = liClients.IndexFromPoint(new Point(e.X, e.Y));
                if (index != -1) { liClients.SelectedIndex = index; cancelPlayerMenu = false; }
                else cancelPlayerMenu = true;
            }
        }

        private void liLevels_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int index = liLevels.IndexFromPoint(new Point(e.X, e.Y));
                if (index != -1) { liLevels.SelectedIndex = index; cancelMapMenu = false; }
                else cancelMapMenu = true;
            }
        }
        private void liLevels_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int index = liLevels.IndexFromPoint(new Point(e.X, e.Y));
                if (index != -1) { liLevels.SelectedIndex = index; cancelMapMenu = false; }
                else cancelMapMenu = true;
            }
        }

        private void demoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("demote").Use(null, liClients.SelectedItem.ToString()); }
            catch { }
        }
        private void promoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("promote").Use(null, liClients.SelectedItem.ToString()); }
            catch { }
        }
        private void liClientsRank_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("setrank").Use(null, liClients.SelectedItem + " " + sender); }
            catch { }
        }
        private void kickToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("kick").Use(null, liClients.SelectedItem.ToString()); }
            catch { }
        }
        private void banToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("ban").Use(null, liClients.SelectedItem.ToString()); }
            catch { }
        }
        private void iPBanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("ipban").Use(null, liClients.SelectedItem.ToString()); }
            catch { }
        }
        private void banAndIPBanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            banToolStripMenuItem_Click(null, null);
            iPBanToolStripMenuItem_Click(null, null);
        }
        private void undoTimeStripTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                oKToolStripMenuItem_Click(null, null);
            if (e.KeyCode != Keys.Delete && e.KeyCode != Keys.Back)
            {
                bool failed = true;
                try
                {
                    if (e.KeyData.ToString().ToLower().StartsWith("numpad"))
                    {
                        int.Parse(e.KeyData.ToString().Substring(6));
                        failed = false;
                    }
                    else
                        throw new Exception();
                }
                catch { if (Char.IsDigit((char)e.KeyValue)) failed = false; else failed = true; }
                if ((char)e.KeyValue == '0' && undoTimeStripTextBox1.Text == "") failed = true;
                if (failed)
                    e.SuppressKeyPress = true;
            }
        }
        private void oKToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("undo").Use(null, liClients.SelectedItem + " " + undoTimeStripTextBox1.Text); }
            catch { }
        }
        private void liClientsSendMap_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("move").Use(null, liClients.SelectedItem + " " + sender); }
            catch { }
        }

        private void unloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("unload").Use(null, liLevels.SelectedItem.ToString()); }
            catch { }
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("save").Use(null, liLevels.SelectedItem.ToString()); }
            catch { }
        }
        private void liLevelsVisit_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("pervisit").Use(null, liLevels.SelectedItem + " " + sender); }
            catch { }
        }
        private void liLevelsBuild_Click(object sender, EventArgs e)
        {
            try { Command.all.Find("perbuild").Use(null, liLevels.SelectedItem + " " + sender);}
            catch { }
        }
        private void oKToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                loadMapStripTextBox.Text = loadMapStripTextBox.Text.Replace(' ', '_');
                if (!File.Exists("levels/" + loadMapStripTextBox.Text + ".lvl") && !File.Exists("levels/" + loadMapStripTextBox.Text + ".mcqlvl"))
                {
                    MessageBox.Show("Level \"" + loadMapStripTextBox.Text + "\" doesn't exist!");
                    return;
                }
                Command.all.Find("load").Use(null, loadMapStripTextBox.Text);
                liMaps.Items.Add(loadMapStripTextBox.Text);
                liLevels.Items.Add(loadMapStripTextBox.Text);
            }
            catch { }
        }

        private void btnKick_Click(object sender, EventArgs e)
        {
            if (liClients.SelectedIndex >= 0)
            {
                Player p = Player.Find(liClients.Items[liClients.SelectedIndex].ToString());
                if (p != null)
                {
                    p.Kick("You were kicked by [console]!");
                    //IRCBot.Say(p.name + " was kicked by [console]!");
                    UpdateClientList(Player.players);
                }
            }
            else
                MessageBox.Show("You need to select someone!");
        }

        private void btnBan_Click(object sender, EventArgs e)
        {
            if (liClients.SelectedIndex >= 0)
            {
                string name = liClients.Items[liClients.SelectedIndex].ToString();
                Player who = Player.Find(name);
                if (who == null) { MessageBox.Show("Player is not online."); return; }
                if (who.group.name.ToLower() != "banned") { who.group.playerList.Remove(name); who.group.playerList.Save(who.group.fileName); }
                else { MessageBox.Show(name + " is already banned."); return; }

                Player.GlobalChat(who, who.color + who.name + Server.DefaultColor +
                    " is now &8banned" + Server.DefaultColor + "!", false);
                who.group = Group.Find("banned"); who.color = who.group.color; Player.GlobalDie(who, false);
                Player.GlobalSpawn(who, who.pos[0], who.pos[1], who.pos[2], who.rot[0], who.rot[1], false);

                who.group.playerList.Add(name); who.group.playerList.Save(who.group.fileName, false);
                Server.s.Log("BANNED: " + name.ToLower());
            }
            else
                MessageBox.Show("You need to select someone!");
        }

        private void btnUnban_Click(object sender, EventArgs e)
        {
            if (liClients.SelectedIndex >= 0)
            {
                string name = liClients.Items[liClients.SelectedIndex].ToString();
                Player who = Player.Find(name);
                if (who.group.name == "banned")
                {

                    if (who == null)
                    {
                        Player.GlobalMessage(name + " &8(banned)" + Server.DefaultColor +
                            " is now " + Group.standard.color + Group.standard.name + Server.DefaultColor + "!");
                    }
                    else
                    {
                        who.group.playerList.Remove(name); who.group.playerList.Save(who.group.fileName, false);
                        Player.GlobalChat(who, who.color + who.name + Server.DefaultColor + " is now " +
                            Group.standard.color + Group.standard.name + Server.DefaultColor + "!", false);
                        who.group = Group.standard; who.color = who.group.color; Player.GlobalDie(who, false);
                        Player.GlobalSpawn(who, who.pos[0], who.pos[1], who.pos[2], who.rot[0], who.rot[1], false);
                        Server.s.Log("UNBANNED: " + name.ToLower());
                    }
                }
                else { MessageBox.Show(name + " is not banned!"); }
            }
            else
                MessageBox.Show("You need to select someone!");
        }

        private void btnKickban_Click(object sender, EventArgs e)
        {
            if (liClients.SelectedIndex >= 0)
            {
                string name = liClients.Items[liClients.SelectedIndex].ToString();
                Player who = Player.Find(name);
                if (who == null) { MessageBox.Show("Player is not online."); }
                else
                {
                    who.Kick("You got served!");
                    Player.GlobalMessage("- " + who.color + who.name + Server.DefaultColor + " is now &8banned" + Server.DefaultColor + "!");
                    who.group = Group.Find("banned"); who.color = who.group.color; Player.GlobalDie(who, false);
                    Player.GlobalSpawn(who, who.pos[0], who.pos[1], who.pos[2], who.rot[0], who.rot[1], false);
                }
                who.group.playerList.Add(name); who.group.playerList.Save(who.group.fileName, false);
                Server.s.Log("BANNED: " + name.ToLower());
            }
            else
                MessageBox.Show("You need to select someone!");
        }

        private void btnBanIp_Click(object sender, EventArgs e)
        {
            if (liClients.SelectedIndex >= 0)
            {
                string message = liClients.Items[liClients.SelectedIndex].ToString();
                Player who = null;
                who = Player.Find(message);
                if (who != null)
                    message = who.ip;

                if (message.Equals("127.0.0.1")) { MessageBox.Show("You can't ip-ban the server!"); return; }
                if (!regex.IsMatch(message)) { MessageBox.Show("Not a valid ip!"); return; }
                if (Server.bannedIP.Contains(message)) { MessageBox.Show(message + " is already ip-banned."); return; }
                Player.GlobalMessage(message + " got &8ip-banned" + Server.DefaultColor + "!");
                //IRCBot.Say("IP-BANNED: " + message.ToLower() + " by console");
                Server.bannedIP.Add(message); Server.bannedIP.Save("banned-ip.txt", false);
                Server.s.Log("IP-BANNED: " + message.ToLower());
            }
            else
                MessageBox.Show("You need to select someone!");
        }

        private void btnUnbanIP_Click(object sender, EventArgs e)
        {
            if (liClients.SelectedIndex >= 0)
            {
                Regex regex = new Regex(@"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\." +
                                "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$");
                string name = liClients.Items[liClients.SelectedIndex].ToString();
                Player who = Player.Find(name);
                string ip = who.ip;

                if (!regex.IsMatch(ip)) { Server.s.Log("Not a valid ip!"); return; }
                if (ip == "127.0.0.1") { Server.s.Log("You can't do that..."); return; }
                if (!Server.bannedIP.Contains(ip)) { Server.s.Log(ip + " doesn't seem to be ipbanned..."); return; }
                Player.GlobalMessage(who.name + " got &8unip-banned" + Server.DefaultColor + "!");
                Server.bannedIP.Remove(ip); Server.bannedIP.Save("banned-ip.txt", false);
                Server.s.Log("IP-UNBANNED: " + ip.ToLower());
            }
            else
                MessageBox.Show("You need to select someone!");
        }

        private void btnChangeRank_Click(object sender, EventArgs e)
        {
            if (liClients.SelectedIndex >= 0)
            {
                Program.selected = liClients.Items[liClients.SelectedIndex].ToString();
                if (!RankWindowUse) { Selection = new SelectionWindow(); RankWindowUse = true; }
                Selection.WK = SelectionWindow.WindowKind.Rank;
                Selection.Show();
                Selection.Top = this.Top;
                Selection.Left = this.Left + 83;
            }
            else { MessageBox.Show("You need to select someone!"); }
        }
        private void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Player.GlobalMessage("&eConsole " + Server.DefaultColor + "[&a" + Server.consolename + Server.DefaultColor + "]&e:&f "
                    + Server.DefaultColor + txtInput.Text);
                WriteLine("<CONSOLE> " + txtInput.Text);
                txtInput.Clear();
            }
        }
        private void txtCommandInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCommandInput.Text[0] == '/' || txtCommandInput.Text[0] == '!') txtCommandInput.Text = txtCommandInput.Text.Remove(0, 1);
                if (txtCommandInput.Text.IndexOf(' ') == -1 && txtCommandInput.Text.ToLower() != "help") { txtCommandInput.Text += " "; }
                switch (txtCommandInput.Text.Split(' ')[0].ToLower())
                {
                    case "rank":
                    case "setrank":
                        if (txtCommandInput.Text.Split(' ').Length == 3)
                        {
                            Group group = Group.Find(txtCommandInput.Text.Split(' ')[2]);
                            if (group != null)
                            {
                                try { Command.all.Find("setrank").Use(null, txtCommandInput.Text.Split(' ')[1] + " " + group.name); }
                                catch { newCommand("Unknown error occured!"); break; }
                            }
                            else
                            {
                                newCommand("Unable to find group: " + txtCommandInput.Text.Split(' ')[2]);
                                break;
                            }
                            newCommand("console uses: " + txtCommandInput.Text);
                        }
                        else { txtCommandInput.Text = "help setrank"; txtCommandInput_KeyDown(sender, e); }
                        break;
                    case "load":
                        string name = txtCommandInput.Text.Split(' ')[1];
                        if (File.Exists("levels/" + name + ".lvl") || File.Exists("levels/" + name + ".mcqlvl"))
                            Server.levels.Add(Level.Load(name));
                        liMaps.Items.Add(name);
                        liLevels.Items.Add(name);
                        break;
                    case "ban":
                        try { Command.all.Find("ban").Use(null, txtCommandInput.Text.Replace("ban ", "")); }
                        catch { txtCommandInput.Text = "help ban"; txtCommandInput_KeyDown(sender, e); break; }
                        newCommand("console uses: " + txtCommandInput.Text);
                        break;
                    case "banip":
                        if (txtCommandInput.Text.Split(' ').Length != 2)
                        { txtCommandInput.Text = "help banip"; txtCommandInput_KeyDown(sender, e); break; }
                        try { Command.all.Find("banip").Use(null, txtCommandInput.Text.Remove(0, txtCommandInput.Text.IndexOf(' '))); }
                        catch { txtCommandInput.Text = "help banip"; txtCommandInput_KeyDown(sender, e); break; }
                        newCommand("console uses: " + txtCommandInput.Text);
                        break;
                    case "kick":
                        Player who = Player.Find(txtCommandInput.Text.Split(' ')[1]);
                        if (who == null)
                        { newCommand("Player not found."); break; }
                        try { Command.all.Find("kick").Use(null, who.name); }
                        catch { txtCommandInput.Text = "help kick"; txtCommandInput_KeyDown(sender, e); break; }
                        newCommand("console uses: " + txtCommandInput.Text);
                        break;
                    case "kickban":
                        Player player = Player.Find(txtCommandInput.Text.Split(' ')[1]);
                        if (player == null)
                        { newCommand("Player not found."); break; }
                        try { Command.all.Find("kickban").Use(null, txtCommandInput.Text.Replace("kickban ", "")); }
                        catch { txtCommandInput.Text = "help kickban"; txtCommandInput_KeyDown(sender, e); break; }
                        newCommand("console uses: " + txtCommandInput.Text);
                        break;
                    case "restart":
                        Server.Restart();
                        break;
                    case "unban":
                        Player player2 = Player.Find(txtCommandInput.Text.Split(' ')[1]);
                        if (player2 == null) { newCommand("Could not find player: " + txtCommandInput.Text.Split(' ')[1]); break; }
                        if (txtCommandInput.Text.Split(' ').Length > 2)
                        { txtCommandInput.Text = "help unban"; txtCommandInput_KeyDown(sender, e); break; }
                        try { Command.all.Find("unban").Use(null, player2.name); }
                        catch { txtCommandInput.Text = "help unban"; txtCommandInput_KeyDown(sender, e); break; }
                        newCommand("console uses: " + txtCommandInput.Text);
                        break;
                    case "unbanip":
                        Player player3 = null;
                        string ip = txtCommandInput.Text.Split(' ')[1];
                        if (txtCommandInput.Text.Split('.').Length != 4)
                        {
                            player3 = Player.Find(txtCommandInput.Text.Split(' ')[1]);
                            if (player3 == null)
                            {
                                newCommand("Invalid input: " + txtCommandInput.Text.Substring(txtCommandInput.Text.IndexOf(' ')));
                                break;
                            }
                            ip = player3.ip;
                        }
                        if (txtCommandInput.Text.Split(' ').Length > 2)
                        { txtCommandInput.Text = "help unabnip"; txtCommandInput_KeyDown(sender, e); break; }
                        try { Command.all.Find("unbanip").Use(null, ip); }
                        catch { txtCommandInput.Text = "help unbanip"; txtCommandInput_KeyDown(sender, e); break; }
                        newCommand("console uses: " + txtCommandInput.Text);
                        break;
                    case "help":
                        if (txtCommandInput.Text.Split(' ').Length == 1)
                        {
                            newCommand("Commands: setrank/rank, ban, banip, kick, kickban, restart, unban, unbanip.");
                            newCommand("Use \"/help <command>\" for more info about <command>");
                        }
                        if (txtCommandInput.Text.Split(' ').Length == 2)
                        {
                            switch (txtCommandInput.Text.Split(' ')[1])
                            {
                                case "rank":
                                case "setrank":
                                    newCommand("Setrank <player> <rank> - use \"setrank\" or \"rank\" to rank a player.");
                                    newCommand("Example: rank quaisaq owner");
                                    break;
                                case "ban":
                                    newCommand("Ban <player> [reason] - use \"ban\" to ban <player>.");
                                    newCommand("Example: ban quaispet Dont be rude");
                                    break;
                                case "banip":
                                    newCommand("Banip <IP> - use \"banip\" to ban an IP.");
                                    newCommand("Example: banip 85.52.163.36");
                                    break;
                                case "kick":
                                    newCommand("Kick <player> [reason] - use \"kick\" to kick a player because of [reason].");
                                    newCommand("Example: kick quaisaq Dont be rude");
                                    break;
                                case "kickban":
                                    newCommand("Kickban <player> - use \"kickban\" to kick and ban <player>");
                                    newCommand("Example: kickban quaispet");
                                    break;
                                case "restart":
                                    newCommand("Restart - use \"restart\" to restart the server.");
                                    newCommand("Example: restart");
                                    break;
                                case "unban":
                                    newCommand("Unban <player> - use \"unban\" to unban a banned player.");
                                    newCommand("Example: unban quaispet");
                                    break;
                                case "unbanip":
                                    newCommand("Unbanip <IP/online player> - use \"unbanip\" to unban a banned IP.");
                                    newCommand("Example: unbanip 85.52.164.36");
                                    break;
                                default:
                                    newCommand("Unknown command: " + txtCommandInput.Text.Split(' ')[1]);
                                    break;
                            }
                        }
                        break;
                    default:
                        newCommand("Unknown command: " + txtCommandInput.Text);
                        break;
                }
                txtCommandInput.Clear();
            }
        }

        private void btnReloadProperties_Click(object sender, EventArgs e)
        {
            MCRevive.Properties.LoadnSave("all");
        }

        private void btnReloadRanks_Click(object sender, EventArgs e)
        {
            Server.bannedIP = PlayerList.Load("banned-ip.txt", null);
            Server.ircControllers = PlayerList.Load("IRC_Controllers.txt", null);
            Group.GroupList.ForEach((grp) =>
            {
                if (grp.name != "nobody")
                    grp.playerList = PlayerList.Load(grp.fileName, grp);
            });
            Server.s.Log("Rank Reloaded!");
        }

        private void btnReloadCmds_Click(object sender, EventArgs e)
        {
            Group.InitAll();
            Command.InitAll();
            GrpCommands.fillRanks();
            Block.SetBlocks();
            Server.s.Log("Commands Reloaded!");
        }

        private void btnProperties_Click(object sender, EventArgs e)
        {
            if (!PropertiesWindowUse) { PropertiesWindowForm = new PropertiesWindow(); PropertiesWindowUse = true; }
            PropertiesWindowForm.Top = this.Top;
            PropertiesWindowForm.Left = this.Right + 10;
            PropertiesWindowForm.Show();
        }

        private void btnFolder_Click(object sender, EventArgs e)
        {
            Process.Start(Directory.GetParent("*.exe") + "");
        }

        private void btnShutdown_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnRestart_Click(object sender, EventArgs e)
        {
            Server.Restart();
        }

        private void btnRules_Click(object sender, EventArgs e)
        {
            RulesChangingWindow fr = new RulesChangingWindow();
            fr.Show();
        }

        private void txtConsoleName_LostFocus(object sender, EventArgs e)
        {
            if (txtConsoleName.Text == "") txtConsoleName.Text = "God";
        }

        private void txtConsoleName_TextChanged(object sender, EventArgs e)
        {
            Server.consolename = txtConsoleName.Text;
        }

        private void btnReloadMJS_Click(object sender, EventArgs e)
        {
            MCRevive.Properties.ReloadMJS();
        }

        private void btnMJSLog_Click(object sender, EventArgs e)
        {
            SelectionWindow Selection = new SelectionWindow();
            if (!MJSWindowUse) { MJSWindowUse = true; }
            Selection.WK = SelectionWindow.WindowKind.MJS;
            Selection.Show();
            Selection.Left = this.Left;
            Selection.Top = this.Top + 50;
        }
        #endregion
        #region Map Tab
        private int lastMapIndex = -1;
        private string MapShown = "";
        private bool MapDrawing = false;
        private void liMaps_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (liMaps.SelectedIndex != -1)
                {
                    MapShown = liMaps.SelectedItem.ToString();
                    if (lastMapIndex != liMaps.SelectedIndex) 
                        mdpMap.DrawMap(MapShown);
                    lastMapIndex = liMaps.SelectedIndex;
                }
            }
            catch (Exception ee)
            {
                Server.ErrorLog(ee);
                tabAll.SelectedIndex = 0;
            }
        }

        private void newMapBtnCreate_Click(object sender, EventArgs e)
        {
            string size = "", type = "";
            try
            {
                size = newMapLiSize.Items[newMapLiSize.SelectedIndex].ToString();
                if (!size.Contains(" x ")) { MessageBox.Show("Please choose a valid map size."); return; }
                size = size.Replace(" ", "");
            }
            catch
            {
                MessageBox.Show("Please choose a valid map size.");
                return;
            }
            try
            {
                type = newMapLiType.SelectedItem.ToString();
            }
            catch
            {
                MessageBox.Show("Please choose a valid map type.");
                return;
            }
            if (String.IsNullOrEmpty(newMapTxtName.Text.Trim())) { MessageBox.Show("Please enter a map name."); return; }
            if (String.IsNullOrEmpty(size.Trim()) || String.IsNullOrEmpty(type.Trim())) { MessageBox.Show("something went wrong."); return; }
            try
            {
                ushort sizeX = ushort.Parse(size.Split('x')[0]);
                ushort sizeY = ushort.Parse(size.Split('x')[1]);
                ushort sizeZ = ushort.Parse(size.Split('x')[2]);
                Command.all.Find("newlvl").Use(null, newMapTxtName.Text.Replace(" ", "_") + " " + sizeX + " " + sizeY + " " + sizeZ + " " + type);
            }
            catch (Exception ee)
            {
                Server.ErrorLog(ee);
            }
        }

        private void liMaps_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int index = liMaps.IndexFromPoint(new Point(e.X, e.Y));
                if (index != -1) { liMaps.SelectedIndex = index; cancelMapMenu = false; }
                else cancelMapMenu = true;
            }
        }
        private void liMaps_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                int index = liMaps.IndexFromPoint(new Point(e.X, e.Y));
                if (index != -1) { liMaps.SelectedIndex = index; cancelMapMenu = false; }
                else cancelMapMenu = true;
            }
        }

        private void btnMapCenter_Click(object sender, EventArgs e)
        {
            if (MapDrawing)
            {
                mdpMap.Cancel = true;
                while (mdpMap.Cancel) { Thread.Sleep(10); Application.DoEvents(); }
            }
            MapDrawing = true;
            mdpMap.DrawMap(MapShown, true);
        }

        private void trkMapZoom_Scroll(object sender, EventArgs e)
        {
            float s = trkMapZoom.Value;
            if (s == 2) s = 1.5F;
            else if (s == 0) s = 0.5F;
            else if (s == -1) s = 0.25F;
            else if (s != 1) s--;
            mdpMap.ChangeZoom(s);
        }
        private void mdpMap_Progress_Changed(byte prog)
        {
            prgMapDraw.Value = prog;
            if (prog == 0 && prgMapDraw.Location.Y == 330)
            {
                for (int i = 1; i <= 10; i++)
                {
                    prgMapDraw.Location = new Point(210, 330 + i);
                    Thread.Sleep(50);
                    Application.DoEvents();
                }
            }
            else if (prog == 100 && prgMapDraw.Location.Y == 340)
            {
                for (int i = 9; i >= 0; i--)
                {
                    prgMapDraw.Location = new Point(210, 330 + i);
                    Thread.Sleep(50);
                    Application.DoEvents();
                }
                MapDrawing = false;
            }
        }
        #endregion
        #region Extra
        private void DrawPlayerGraph()
        {
            PlayerGraphList.Insert(0, Player.number);
            if (PlayerGraphList.Count > pnlPlayers.Width / 10 + 1) PlayerGraphList.RemoveAt(pnlPlayers.Width / 10 + 1);
            if (Player.number > playerScale3)
            {
                lblPlayerScale1.Text = playerScale1.ToString();
                lblPlayerScale2.Text = playerScale2.ToString();
                lblPlayerScale3.Text = playerScale3.ToString();
            }
            else if (playerScale3 != 15 && Player.number <= playerScale3 / 2)
            {
                bool decreaseScale = true;
                for (int pos = 0; pos < PlayerGraphList.Count; ++pos)
                    if (PlayerGraphList[pos] > playerScale3 / 2)
                        decreaseScale = false;
                if (decreaseScale)
                {
                    lblPlayerScale1.Text = (playerScale1 / 2).ToString();
                    lblPlayerScale2.Text = (playerScale2 / 2).ToString();
                    lblPlayerScale3.Text = (playerScale3 / 2).ToString();
                }
            }

            if (SelectedTab == "Extra")
            {
                Image i = new Bitmap(pnlPlayers.Width, pnlPlayers.Height);
                Graphics g = Graphics.FromImage(i);
                g.Clear(Color.White);
                
                for (int val = pnlPlayers.Width - NextTrafficLine * 10 - 1; val >= 0; val -= 40)
                    g.DrawLine(new Pen(new SolidBrush(Color.Lavender)), val, 0, val, pnlPlayers.Height);

                for (int ii = 1; ii < 3; ii++)
                    g.DrawLine(new Pen(new SolidBrush(Color.Lavender)), 0, pnlPlayers.Height - pnlPlayers.Height / 3 * ii, pnlPlayers.Width - 1, pnlPlayers.Height - pnlPlayers.Height / 3 * ii);

                int num = 10;
                for (int val = 0; val < PlayerGraphList.Count - 1; ++val)
                {
                    g.DrawLine(new Pen(Color.Black), pnlPlayers.Width - num,
                        pnlPlayers.Height - (int)Math.Ceiling((double)pnlPlayers.Height / playerScale3 * PlayerGraphList[val + 1]) - 1,
                        pnlPlayers.Width - num + 10,
                        pnlPlayers.Height - (int)Math.Ceiling((double)pnlPlayers.Height / playerScale3 * PlayerGraphList[val]) - 1);
                    num += 10;
                }
                g.DrawString(PlayerGraphList[0].ToString(), new Font(btnBan.Font.FontFamily, 20, FontStyle.Bold), new SolidBrush(Color.Black), new PointF(5, 5));
                g.Dispose();

                Graphics gr = pnlPlayers.CreateGraphics();
                gr.DrawImage(i, 0, 0);
                gr.Dispose();
                i.Dispose();
            }
        }

        int NextTrafficLine = 1;
        private void DrawTrafficGraph()
        {
            trafficsIn.Insert(0, Player.InComeTraffic);
            trafficsOut.Insert(0, Player.OutGoneTraffic);
            Player.OutGoneTraffic = 0;
            Player.InComeTraffic = 0;
            if (trafficsIn.Count > pnlTraffic.Width / 10 + 1) trafficsIn.RemoveAt(pnlTraffic.Width / 10 + 1);
            if (trafficsOut.Count > pnlTraffic.Width / 10 + 1) trafficsOut.RemoveAt(pnlTraffic.Width / 10 + 1);
            while (trafficsOut[0] > trafficScale2 * 1000 || trafficsIn[0] > trafficScale2 * 1000)
            {
                lblTrafficScale2.Text = trafficScale2 * 2 + "k";
                lblTrafficScale1.Text = trafficScale1 * 2 + "k";
                trafficScale2 *= 2;
                trafficScale1 *= 2;
            }
            if (trafficScale1 != 10 && (trafficsOut[0] <= trafficScale2 * 500 || trafficsIn[0] <= trafficScale2 * 500))
            {
                bool decreaseScale = true;
                for (int pos = 0; pos < trafficsIn.Count; ++pos)
                    if (trafficsIn[pos] > trafficScale2 * 500 || trafficsOut[pos] > trafficScale2 * 500)
                        decreaseScale = false;

            decreaseAgain:
                if (decreaseScale)
                {
                    lblTrafficScale2.Text = trafficScale2 / 2 + "k";
                    lblTrafficScale1.Text = trafficScale1 / 2 + "k";
                    trafficScale2 /= 2;
                    trafficScale1 /= 2;
                }
                for (int pos = 0; pos < trafficsIn.Count; ++pos)
                    if (trafficsIn[pos] > trafficScale2 * 500 || trafficsOut[pos] > trafficScale2 * 500)
                        decreaseScale = false;
                if (decreaseScale)
                    goto decreaseAgain;
            }
            if (SelectedTab == "Extra")
            {
                Image i = new Bitmap(pnlTraffic.Width, pnlTraffic.Height);
                Graphics g = Graphics.FromImage(i);
                g.Clear(Color.White);
                
                int num = 10;
                for (int val = pnlTraffic.Width - NextTrafficLine * 10 - 1; val >= 0; val -= 40)
                    g.DrawLine(new Pen(new SolidBrush(Color.Lavender)), val, 0, val, pnlTraffic.Height);

                for (int ii = 1; ii < 4; ii++)
                    g.DrawLine(new Pen(new SolidBrush(Color.Lavender)), 0, pnlTraffic.Height - pnlTraffic.Height / 4 * ii, pnlTraffic.Width - 1, pnlTraffic.Height - pnlTraffic.Height / 4 * ii);

                for (int val = 0; val < trafficsIn.Count - 1; ++val)
                {
                    int in1 = pnlTraffic.Height - (int)Math.Ceiling(pnlTraffic.Height / (trafficScale2 * 1000D) * trafficsIn[val + 1]);
                    int in2 = pnlTraffic.Height - (int)Math.Ceiling(pnlTraffic.Height / (trafficScale2 * 1000D) * trafficsIn[val]);
                    int out1 = pnlTraffic.Height - (int)Math.Ceiling(pnlTraffic.Height / (trafficScale2 * 1000D) * trafficsOut[val + 1]);
                    int out2 = pnlTraffic.Height - (int)Math.Ceiling(pnlTraffic.Height / (trafficScale2 * 1000D) * trafficsOut[val]);

                    int x1 = pnlTraffic.Width - num;
                    int x2 = x1 + 10;

                    g.FillPolygon(new SolidBrush(Color.FromArgb(200, 255, 200)), new Point[] { new Point(x1, out1 - 1), new Point(x2, out2 - 1), new Point(x2, pnlTraffic.Height), new Point(x1, pnlTraffic.Height) });
                    g.FillPolygon(new SolidBrush(Color.FromArgb(255, 200, 200)), new Point[] { new Point(x1, in1 - 2), new Point(x2, in2 - 2), new Point(x2, pnlTraffic.Height), new Point(x1, pnlTraffic.Height) });
                    g.DrawLine(new Pen(Color.Lime), x1, out1 - 1, x2, out2 - 1);
                    g.DrawLine(new Pen(Color.Red), x1, in1 - 2, x2, in2 - 2);
                    num += 10;
                }
                g.DrawString(trafficsIn[0].ToString(), new Font(btnBan.Font.FontFamily, 20, FontStyle.Bold), new SolidBrush(Color.Red), new PointF(5, 5));
                g.DrawString(trafficsOut[0].ToString(), new Font(btnBan.Font.FontFamily, 20, FontStyle.Bold), new SolidBrush(Color.Green), new PointF(pnlTraffic.Width - g.MeasureString(trafficsOut[0].ToString(), new Font(btnBan.Font.FontFamily, 20, FontStyle.Bold)).Width - 5, 5));
                g.Dispose();

                Graphics gr = pnlTraffic.CreateGraphics();
                gr.DrawImage(i, 0, 0);
                gr.Dispose();
                i.Dispose();

                NextTrafficLine++;
                if (NextTrafficLine == 4)
                    NextTrafficLine = 0;
            }
        }
        #endregion
        #region System Logs
        private void dtpLog_ValueChanged(object sender, EventArgs e)
        {
            serverLogPath = "logs/" + dtpLog.Value.ToString("yyyy-MM-dd") + ".txt";
            try { txtServerLog.Text = File.ReadAllText(serverLogPath); }
            catch { txtServerLog.Text = "Log file for this date doesn't exist."; }

            txtServerLog.SelectionStart = txtServerLog.Text.Length;
            txtServerLog.ScrollToCaret();
        }
        private void dtpError_ValueChanged(object sender, EventArgs e)
        {
            txtErrorLog.Text = "Processing...";
            try
            {
                if (File.Exists("logs/errors/" + dtpError.Value.ToString("yyyy-MM-dd") + ".txt"))
                {
                    errorLogPath = "logs/errors/" + dtpError.Value.ToString("yyyy-MM-dd") + ".txt";
                    txtErrorLog.Text = File.ReadAllText(errorLogPath);
                }
            }
            catch { txtErrorLog.Text = "This log doesn't exist!"; }

            txtErrorLog.SelectionStart = txtErrorLog.Text.Length;
            txtErrorLog.ScrollToCaret();
        }

        private void btnOpenServerLog_Click(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(Directory.GetParent(serverLogPath) + serverLogPath.Replace('\\', '/').Substring(serverLogPath.LastIndexOf('/'))))
                    Process.Start(Directory.GetParent(serverLogPath) + serverLogPath.Replace('\\', '/').Substring(serverLogPath.LastIndexOf('/')));
            }
            catch (Exception ex)
            {
                Server.ErrorLog(ex);
            }
        }

        private void btnOpenErrorLog_Click(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(Directory.GetParent(errorLogPath) + errorLogPath.Replace('\\', '/').Substring(errorLogPath.LastIndexOf('/'))))
                    Process.Start(Directory.GetParent(errorLogPath) + errorLogPath.Replace('\\', '/').Substring(errorLogPath.LastIndexOf('/')));
            }
            catch (Exception ex)
            {
                Server.ErrorLog(ex);
            }
        }

        private void btnRefreshErrorLog_Click(object sender, EventArgs e)
        {
            dtpError_ValueChanged("", e);
        }

        private void btnRefreshServerLog_Click(object sender, EventArgs e)
        {
            dtpLog_ValueChanged("", e);
        }
        #endregion
        #region Routines
        #region ListBox SelectedIndexChanges
        private void liRoutines_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (liRoutines.SelectedIndex != -1)
            {
                liRoutineActions.Items.Clear();
                liRoutineActions.Enabled = true;
                liRoutinesAction.Items.Clear();
                liRoutineActionList.Enabled = true;
                liRoutinesAction.Enabled = false;
                txtRoutineActionAddInfo.Text = "";
                txtRoutineActionAddInfo.Enabled = false;
                btnRoutineActionAdd.Enabled = false;

                btnRoutineDelete.Enabled = true;
                button5.Enabled = true;
                Routines.Routine r = selectedRoutine();
                liRoutineActions.Enabled = true;
                liRoutineActions.Items.Clear();
                        
                r.actions.ForEach((a) => {
                    liRoutineActions.Items.Add(a.getString());
                });
                goto endRoutineSearchLoop;
            }
            else
            {
                liRoutinesAction.Items.Clear();
                liRoutineActionList.Enabled = false;
                liRoutinesAction.Enabled = false;
                txtRoutineActionAddInfo.Text = "";
                txtRoutineActionAddInfo.Enabled = false;
                btnRoutineActionAdd.Enabled = false;

                btnRoutineDelete.Enabled = false;
                button5.Enabled = false;
                liRoutineActions.Items.Clear();
                liRoutineActions.Enabled = false;
            }
            btnRoutineDelete.Enabled = liRoutines.SelectedIndex != -1;
        endRoutineSearchLoop:
            updateCreateActionButton();
        }
        private void liRoutineActions_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRoutineActionDelete.Enabled = liRoutineActions.SelectedIndex != -1;
        }
        private void liRoutineActionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            liRoutinesAction.Items.Clear();
            liRoutinesAction.Enabled = false;
            txtRoutineActionAddInfo.Text = "";
            txtRoutineActionAddInfo.Enabled = false;
            if (liRoutineActionList.SelectedIndex != -1)
            {
                if (liRoutineActionList.Text == "Broadcast message")
                {
                    txtRoutineActionAddInfo.Enabled = true;
                    txtRoutineActionAddInfo.Text = "Thanks for playing on my server :D";
                    txtRoutineActionAddInfo.Focus();
                    txtRoutineActionAddInfo.Select(0, txtRoutineActionAddInfo.Text.Length);
                }
                if (liRoutineActionList.Text == "Run MJS script")
                {
                    liRoutinesAction.Enabled = true;
                    foreach (string d in Directory.GetDirectories("mjs"))
                    {
                        foreach (string f in Directory.GetFiles(d))
                        {
                            if (f.EndsWith(".mjs")) liRoutinesAction.Items.Add(f.Replace("mjs\\","").Replace(".mjs",""));
                        }
                    }
                }
                if (liRoutineActionList.Text == "Save map")
                {
                    liRoutinesAction.Enabled = true;
                    Server.levels.ForEach((m) =>
                    {
                        liRoutinesAction.Items.Add(m.name);
                    });
                    if (liRoutinesAction.Items.Count == 1) liRoutinesAction.SetSelected(0, true);
                }
                if (liRoutineActionList.Text == "Load map")
                {
                    liRoutinesAction.Enabled = true;
                    Server.levels.ForEach((m) =>
                    {
                        liRoutinesAction.Items.Add(m.name);
                    });
                    if (liRoutinesAction.Items.Count == 1) liRoutinesAction.SetSelected(0, true);
                }
                if (liRoutineActionList.Text == "Unload map")
                {
                    liRoutinesAction.Enabled = true;
                    Server.levels.ForEach((m) =>
                    {
                        liRoutinesAction.Items.Add(m.name);
                    });
                    if (liRoutinesAction.Items.Count == 1) liRoutinesAction.SetSelected(0, true);
                }
                if (liRoutineActionList.Text == "Restart server")
                {
                    liRoutinesAction.Enabled = true;
                    liRoutinesAction.Items.Add("Restart");
                    liRoutinesAction.SetSelected(0, true);
                }
                if (liRoutineActionList.Text == "Shutdown server")
                {
                    liRoutinesAction.Enabled = true;
                    liRoutinesAction.Items.Add("Shutdown");
                    liRoutinesAction.SetSelected(0, true);
                }
                if (liRoutineActionList.Text == "Execute command")
                {
                    txtRoutineActionAddInfo.Enabled = true;
                    txtRoutineActionAddInfo.Text = "/command";
                    txtRoutineActionAddInfo.Focus();
                    txtRoutineActionAddInfo.Select(1, 7);
                }
            }
            updateCreateActionButton();
        }
        private void liRoutinesAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateCreateActionButton();
        }
        private void liRoutineExec_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateCreateRoutineButton();
        }
        #endregion
        #region Button Clicks
        private void btnRoutineAdd_Click(object sender, EventArgs e)
        {
            int secs = 0;
            switch (liRoutineExec.SelectedIndex)
            {
                case 0:
                secs = (int)numRoutineExec.Value * 60;
                break;
                case 1:
                secs = (int)numRoutineExec.Value * 60 * 60;
                break;
                case 2:
                secs = (int)numRoutineExec.Value * 24 * 60 * 60;
                break;
            }
            if (secs == 0) { MessageBox.Show("Something went wrong :S ("+liRoutineExec.SelectedIndex+")"); return; }
            if (txtRoutine.Text == "NewRoutine")
            {
                int addNum = 0;
                string name = txtRoutine.Text;
                while (liRoutines.Items.Contains(name))
                {
                    addNum++;
                    name = txtRoutine.Text + addNum;
                }
                txtRoutine.Text = name;
            }
            Routines.routines.Add(new Routines.Routine(txtRoutine.Text, secs));
            liRoutines.Items.Add(txtRoutine.Text);
            liRoutines.SelectedIndex = liRoutines.Items.Count - 1;
            numRoutineExec.Value = 0;
            txtRoutine.Text = "NewRoutine";
            updateCreateRoutineButton();
        }
        private void btnRoutineDelete_Click(object sender, EventArgs e)
        {
            if (liRoutines.SelectedIndex == -1) { MessageBox.Show("Select a routine to delete pl0x."); return; }
            Routines.Routine rr = selectedRoutine();
            if (rr != null)
            {
                Routines.routines.Remove(rr);
                liRoutines.Items.RemoveAt(liRoutines.SelectedIndex);
                removeTimerForRoutine(rr);
                File.Delete("routines\\"+rr.name+".txt");
            }
        }
        private void btnRoutineActionDelete_Click(object sender, EventArgs e)
        {
            if (liRoutineActions.SelectedIndex == -1) { MessageBox.Show("Select an action to delete pl0x."); return; }
            Routines.Action aa = null;
            Routines.Routine r = this.selectedRoutine();
            r.actions.ForEach((a) =>
            {
                if (a.getString() == liRoutineActions.Text)
                {
                    aa = a;
                }
            });
            r.actions.Remove(aa);
            liRoutineActions.Items.Remove(aa.getString());
            updateCreateActionButton();
        }
        private void btnRoutineActionAdd_Click(object sender, EventArgs e)
        {
            if (btnRoutineActionAdd.Enabled == false) return;
            Routines.Action a = new Routines.Action(liRoutineActionList.Text,liRoutinesAction.Enabled?liRoutinesAction.Text:txtRoutineActionAddInfo.Text);
            selectedRoutine().actions.Add(a);
            liRoutineActions.Items.Add(a.getString());
        }
        #endregion
        #region Other events
        private void numRoutineExec_ValueChanged(object sender, EventArgs e)
        {
            updateCreateRoutineButton();
        }
        private void txtRoutine_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtRoutine.Text == "NewRoutine")
            {
                txtRoutine.Text = "";
            }
            updateCreateRoutineButton();
        }
        private void txtRoutine_TextChanged(object sender, EventArgs e)
        {
            updateCreateRoutineButton();
        }
        private void txtRoutineActionAddInfo_TextChanged(object sender, EventArgs e)
        {
            updateCreateActionButton();
        }
        #endregion
        private void updateCreateRoutineButton()
        {
            if (numRoutineExec.Value == 0) { btnRoutineAdd.Enabled = false; return; }
            if (txtRoutine.Text.Trim() == "") { btnRoutineAdd.Enabled = false; return; }
            if (liRoutines.Items.Contains(txtRoutine.Text) && txtRoutine.Text != "NewRoutine") { btnRoutineAdd.Enabled = false; return; }
            if (liRoutineExec.SelectedIndex == -1) { btnRoutineAdd.Enabled = false; return; }
            btnRoutineAdd.Enabled = true;
        }
        private void updateCreateActionButton()
        {
            if (liRoutines.SelectedIndex == -1) { btnRoutineActionAdd.Enabled = false; return; }
            if (liRoutineActionList.SelectedIndex == -1) { btnRoutineActionAdd.Enabled = false; return; }
            if (txtRoutineActionAddInfo.Text == "" && liRoutinesAction.SelectedIndex == -1) { btnRoutineActionAdd.Enabled = false; return; }
            btnRoutineActionAdd.Enabled = true;
        }
        private void loadRoutines()
        {
            liRoutines.Items.Clear();
            Routines.routines.ForEach((r) =>
            {
                string intervalText = "";
                if (r.intervalSecs % (24 * 60 * 60) == 0)
                {
                    intervalText = (r.intervalSecs / 24 / 60 / 60) + " day" + (r.intervalSecs == 24 * 60 * 60 ? "" : "s");
                }
                else if (r.intervalSecs % (60 * 60) == 0)
                {
                    intervalText = (r.intervalSecs / 60 / 60) + " hour" + (r.intervalSecs == 60 * 60 ? "" : "s");
                }
                else
                {
                    intervalText = (r.intervalSecs / 60) + " minute" + (r.intervalSecs == 60 ? "" : "s");
                }
                liRoutines.Items.Add(r.name + " [" + intervalText + "]");
            });
        }
        public void asdfMovies(string msg)
        {
            MessageBox.Show(msg);
        }
        List<RoutineTimer> timers = new List<RoutineTimer>();
        public void addTimerForRoutine(Routines.Routine r)
        {
            timers.Add(new RoutineTimer(r));
        }
        public void removeTimerForRoutine(Routines.Routine r)
        {
            RoutineTimer rtrt = null;
            foreach (RoutineTimer rt in timers)
            {
                if (rt.routine == r)
                {
                    rtrt = rt;
                    goto doneFindingRoutineTimer;
                }
            }
        doneFindingRoutineTimer:
            rtrt.timer.Stop();
            timers.Remove(rtrt);
        }

        public void rLog(string s)
        {
            if (txtRoutineLog.InvokeRequired)
            {
                StringCallback d = new StringCallback(rLog);
                this.Invoke(d, new object[] { s });
            }
            else
                txtRoutineLog.AppendText(Environment.NewLine + txtRoutineLog.Text);
        }
        public class RoutineTimer
        {
            public Routines.Routine routine;
            public System.Timers.Timer timer = null;
            public RoutineTimer(Routines.Routine r)
            {
                if (r.intervalSecs <= 0) return;
                routine = r;
                timer = new System.Timers.Timer(routine.intervalSecs * 1000);
                timer.Elapsed += delegate
                {
                    try
                    {
                        Program.window.rLog("Starting execution of " + r.name + "...");
                        routine.actions.ForEach((a) =>
                        {
                            try
                            {
                                if (a.action == "Global message")
                                {
                                    Player.GlobalMessage(a.info);
                                }
                                else if (a.action == "Run MJS script")
                                {
                                    MJS.Script.Execute("mjs\\" + a.info, "");
                                }
                                else if (a.action == "Save map")
                                {
                                    Level.FindExact(a.info).saveChanges();
                                    Level.FindExact(a.info).Save();
                                }
                                else if (a.action == "Load map")
                                {
                                    Level.Load(a.info);
                                }
                                else if (a.action == "Unload map")
                                {
                                    Level.FindExact(a.info).Unload();
                                }
                                else if (a.action == "Restart server")
                                {
                                    Routines.save();
                                    Server.Restart();
                                }
                                else if (a.action == "Shutdown server")
                                {
                                    Routines.save();
                                    Server.Exit();
                                }
                                else if (a.action == "Execute command")
                                {
                                    Program.window.txtCommandInput.Text = a.info;
                                    Program.window.txtCommandInput_KeyDown(null, new KeyEventArgs(Keys.Enter));
                                }
                            }
                            catch (Exception e)
                            {
                                Program.window.rLog("## Execution of action " + routine.actions.IndexOf(a) + " out of " + routine.actions.Count + " failed:" + Environment.NewLine + "## " + e.Message);
                            }
                        });
                        Program.window.rLog("Execution of " + r.name + " finished.");
                    }
                    catch (Exception ex) { Server.ErrorLog(ex); } 
                };
                timer.Start();
            }
        }
        public void editRoutine(int interval, string name, Routines.Routine r)
        {
            if (r.intervalSecs != interval)
            {
                r.intervalSecs = interval;
                r.name = name;
                removeTimerForRoutine(r);
                addTimerForRoutine(r);
                rLog("Routine" + r.name + " was edited and its timer was restarted.");
            }
            else
            {
                r.name = name;
                rLog("Routine" + r.name + " was edited.");
            }
        }
        public void button5_Click(object sender, EventArgs e)
        {
            Routines.Routine rr = selectedRoutine();
            EditRoutineWindow w = new EditRoutineWindow();
            w.Show();
            w.SetDesktopLocation(this.Location.X+100,this.Location.Y+100);
            w.setVars(rr.intervalSecs, rr.name, this, rr);
        }
        private Routines.Routine selectedRoutine()
        {
            return Routines.getRoutine(liRoutines.Text.Split(new string[] { " [" }, StringSplitOptions.None)[0]);
        }
        #endregion

        private void ChangelogFormat(RichTextBox rtxt)
        {
            if (rtxt.Enabled)
            {
                rtxt.Enabled = false;
                List<int[]> lint = new List<int[]>(); //index, length, style
                if (rtxt.Text.IndexOf('<') != -1 && rtxt.Text.IndexOf('>') != -1)
                {
                    int st = rtxt.SelectionStart;
                    string tags = "bciru"; // b = 0 | c = 1 | i = 2 | r = 3 | u = 4
                    for (int c = 0; c < tags.Length; c++)
                    {
                        while (rtxt.Text.IndexOf("<" + tags[c] + ">") != -1)
                        {
                            for (int i = 0; i < lint.Count; ++i)
                                if (lint[i][0] > rtxt.Text.IndexOf("</" + tags[c] + ">"))
                                    lint[i][0] -= 7;
                                else if (lint[i][0] > rtxt.Text.IndexOf("<" + tags[c] + ">"))
                                    lint[i][0] -= 3;
                            lint.Add(new int[] { rtxt.Text.IndexOf("<" + tags[c] + ">"), rtxt.Text.IndexOf("</" + tags[c] + ">") - rtxt.Text.IndexOf("<" + tags[c] + ">") - 3, c });
                            rtxt.Text = rtxt.Text.Substring(0, rtxt.Text.IndexOf("<" + tags[c] + ">")) +
                                rtxt.Text.Substring(rtxt.Text.IndexOf("<" + tags[c] + ">") + 3, rtxt.Text.IndexOf("</" + tags[c] + ">") - rtxt.Text.IndexOf("<" + tags[c] + ">") - 3) +
                                rtxt.Text.Substring(rtxt.Text.IndexOf("</" + tags[c] + ">") + 4);
                        }
                    }

                    foreach (int[] i in lint)
                    {
                        rtxt.SelectionStart = i[0];
                        rtxt.SelectionLength = i[1];
                        if (i[2] == 0 || i[2] == 2 || i[2] == 4)
                            rtxt.SelectionFont = new Font(rtxt.Font, i[2] == 0 ? FontStyle.Bold : i[2] == 2 ? FontStyle.Italic : FontStyle.Underline);
                        else
                            rtxt.SelectionAlignment = i[2] == 1 ? HorizontalAlignment.Center : HorizontalAlignment.Right;
                    }
                    rtxt.SelectionLength = 0;
                    rtxt.SelectionStart = st;
                }
                rtxt.Enabled = true;
            }
        }
    }
}