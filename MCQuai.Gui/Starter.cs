﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MCRevive.Gui
{
    public partial class Starter : Form
    {
        public Starter()
        {
            InitializeComponent();
        }

        string log = DateTime.Now.ToString("(HH:mm:ss)") + " Starting Server...";
        string cmdlog = "All commands ready.";
        string Url = "";
        private void Starter_Load(object sender, EventArgs e)
        {
            t.Start();
        }
        private void WriteLine(string msg) { log += Environment.NewLine + msg; }
        private void newCommand(string msg) { cmdlog += Environment.NewLine + msg; }
        private void newMJSLog(string msg) { Program.MJSLog += Environment.NewLine + msg; }
        private void HeartBeatFail() { log += Environment.NewLine + "Recent Heartbeat Failed"; }
        private void UpdateUrl(string url) { Url = url; }

        private void t_Tick(object sender, EventArgs e)
        {
            t.Stop();
            lblProgress.Text = "Settings Events...";
            Application.DoEvents();
            new Server();
            Server.s.OnLog += WriteLine;
            Server.s.OnCommand += newCommand;
            Server.s.OnMJS += newMJSLog;
            Server.s.HeartBeatFail += HeartBeatFail;
            Server.s.OnURLChange += UpdateUrl;
            lblProgress.Text = "Booting Server...";
            Application.DoEvents();
            Server.s.Start();
            lblProgress.Text = "Initializing Gui...";
            Application.DoEvents();

            Program.URL += "?ip=" + Server.IP + "&port=" + Server.port + "&salt=" + Server.womSalt;

            int time = 0;
            while (!log.EndsWith("...") && !log.EndsWith("Done.") && !Server.shuttingdown)
            {
                Console.WriteLine(log);
                System.Threading.Thread.Sleep(100);
                Application.DoEvents();
                if (time > 50)
                    break;
            }
            
            Console.WriteLine(log);
            Program.window = new GuiWindow(log, cmdlog, Url);
            Program.window.Show();
            this.Hide();

            t.Dispose();
        }
    }
}
