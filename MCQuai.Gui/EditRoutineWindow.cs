﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MCRevive.Gui
{
    public partial class EditRoutineWindow : Form
    {
        GuiWindow gw;
        Routines.Routine r;
        public EditRoutineWindow()
        {
            InitializeComponent();
        }
        public void setVars(int interval, string name, GuiWindow gw, Routines.Routine r)
        {
            this.gw = gw;
            this.r = r;
            textBox1.Text = name;
            if (interval % (24 * 60 * 60) == 0)
            {
                listBox2.SetSelected(2, true);
                numericUpDown1.Value = interval / 24 / 60 / 60;
            }
            else if (interval % (60 * 60) == 0)
            {
                listBox2.SetSelected(1, true);
                numericUpDown1.Value = interval / 60 / 60;
            }
            else
            {
                listBox2.SetSelected(0, true);
                numericUpDown1.Value = interval / 60;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            int secs = 0;
            switch (listBox2.SelectedIndex)
            {
                case 0:
                secs = (int)numericUpDown1.Value * 60;
                break;
                case 1:
                secs = (int)numericUpDown1.Value * 60 * 60;
                break;
                case 2:
                secs = (int)numericUpDown1.Value * 24 * 60 * 60;
                break;
            }
            gw.editRoutine(secs, textBox1.Text, r);
            this.Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            updateSaveButton();
        }
        private void updateSaveButton()
        {
            if (textBox1.Text == "") { button1.Enabled = false; return; }
            if (numericUpDown1.Value == 0) { button1.Enabled = false; return; }
            if (Routines.getRoutine(textBox1.Text) != null && textBox1.Text != r.name) { button1.Enabled = false; return; }
            button1.Enabled = true;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            updateSaveButton();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateSaveButton();
        }

        private void EditRoutineWindow_Load(object sender, EventArgs e)
        {

        }
    }
}
