/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace MCRevive.Gui
{
    public class Program
    {
        public static string selected = "";
        public static string MJSLog = "";
        public static string URL = "https://direct.worldofminecraft.com/server.php";
        public static GuiWindow window = null;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Server.usingCLI = false;
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(Program.GlobalExHandler);
                Application.ThreadException += new ThreadExceptionEventHandler(Program.ThreadExHandler);
                Application.Run(new Starter());
            }
            catch (Exception ex) { MessageBox.Show(ex + ""); }
        }

        public static void GlobalExHandler(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            Server.ErrorLog(ex);
            Thread.Sleep(500);

            if (Server.restartOnError)
                Program.restartMe();
            else
                Program.restartMe(false);
        }
        public static void ThreadExHandler(object sender, ThreadExceptionEventArgs e)
        {
            Exception ex = e.Exception;
            Server.ErrorLog(ex);
            Thread.Sleep(500);

            if (Server.restartOnError)
                Program.restartMe();
            else
                Program.restartMe(false);
        }

        static public void restartMe(bool fullRestart = true)
        {
            Thread restartThread = new Thread(new ThreadStart(delegate
            {
                if (!Server.shuttingdown)
                {
                    saveAll();

                    Server.shuttingdown = true;

                    try { Server.listen.Close(); }
                    catch { }
                    Server.forceShutdown = true;
                    if (!fullRestart)
                    {
                        Server.s.Start();
                        Server.forceShutdown = false;
                    }
                    else
                        Server.Restart();
                }
            }));
            restartThread.Start();
        }
        static public void saveAll()
        {
            try
            {
                List<Player> kickList = new List<Player>();
                kickList.AddRange(Player.players);
                kickList.ForEach((p) => { p.Kick("Server restarted! Rejoin!"); });
            }
            catch (Exception ex) { Server.ErrorLog(ex); }
        }
    }
}