﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace MCRevive.Gui
{
    public partial class RulesChangingWindow : Form
    {
        public RulesChangingWindow()
        {
            InitializeComponent();
        }
        public void RulesChangingWindow_Load(object sender, EventArgs e)
        {
            if (!File.Exists("text/rules.txt")) File.WriteAllText("text/rules.txt", "No Griefing!");
            txtRules.Text = File.ReadAllText("text/rules.txt");
        }

        private void btnDiscard_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            File.WriteAllText("text/rules.txt", txtRules.Text.Replace("\n", Environment.NewLine));
            MessageBox.Show("Your servers rules have been saved!");
        }
    }
}
