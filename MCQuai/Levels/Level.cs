﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Data;
using System.Threading;

///WARNING! DO NOT CHANGE THE WAY THE LEVEL IS SAVED/LOADED!
///You MUST make it able to save and load as a new version other wise you will make old levels incompatible!
///I Already did that :D -Quaisaq

namespace MCRevive
{
    public class Level
    {
        public string name;
        public string creator;
        public ushort width; // x
        public ushort depth; // y       THIS IS STUPID, SHOULD HAVE BEEN Z
        public ushort height; // z      THIS IS STUPID, SHOULD HAVE BEEN Y

        //undo
        public int currentUndo = 0;
        public struct UndoPos { public int location; public int oldType, newType; public DateTime timePerformed; }
        public List<UndoPos> UndoBuffer = new List<UndoPos>();

        //block change record
        public System.Timers.Timer aboutTimer = new System.Timers.Timer(20000); // 20 seconds
        public struct BlockPos { public int pos; public DateTime TimePerformed; public bool deleted; public string name; public BlockPos(int _pos, DateTime dt, bool Deleted, string Name) { pos = _pos; TimePerformed = dt; deleted = Deleted; name = Name; } }
        public List<BlockPos> blockCache = new List<BlockPos>();
        public class PortalPos { public int pos, pos2; public string end; public PortalPos(int Position, int Position2, string End) { pos = Position; pos2 = Position2; end = End; } }

        //spawn
        public ushort spawnx, spawny, spawnz;
        public byte rotx, roty;

        public ushort jailx, jaily, jailz;
        public byte jailrotx, jailroty;

        public bool antispeedhack = false;
        public bool GrassDestroy = true;
        public Thread physThread;
        public bool physPause = false;
        public DateTime physResume;
        public System.Timers.Timer physTimer = new System.Timers.Timer(1000);
        public int physics = 0;
        public bool ai = true;
        public string type = "flat";
        public int speedPhysics = 250;
        public bool edgeWater = true;
        public int overload = 1500;
        public bool worldChat = true;
        public bool unload = true;
        public string motd = "ignore";
        public bool tetrisAllowed = false;

        public int permissionvisit = LevelPermission.Guest;// What ranks can go to this map (excludes banned)
        public int permissionbuild = LevelPermission.Guest;

        public ushort[] blocks;
        public struct Zone { public ushort smallX, smallY, smallZ, bigX, bigY, bigZ; public string Owner; }
        public List<Zone> ZoneList;

        public List<string> ListPlayers = new List<string>();  //A list that shows the current players in the map
        List<Check> ListCheck = new List<Check>();      //A list of blocks that need to be updated
        List<Update> ListUpdate = new List<Update>();   //A list of block to change after calculation

        public bool changed = false;
        public bool backedup = false;
        public int lastCheck = 0;
        public int lastUpdate = 0;

        //zombiemod
        public bool zombiemod = false;
        public bool zm_respawn = false;

        public Level(string n, ushort x, ushort y, ushort z, string type, string owner)
        {
            width = x; depth = y; height = z;

            if (width < 16) { width = 16; }
            if (depth < 16) { depth = 16; }
            if (height < 16) { height = 16; }

            if (width > 32000) { width = 32000; }
            if (depth > 32000) { depth = 32000; }
            if (height > 32000) { height = 32000; }

            name = n;
            creator = owner;
            blocks = new ushort[width * depth * height];
            blockCache.Capacity = width * depth * height;
            ZoneList = new List<Zone>();

            this.type = type;

            switch (type)
            {
                case "flat":
                case "pixel":
                case "box":
                    ushort half = (ushort)(depth / 2);
                    for (x = 0; x < width; ++x)
                        for (z = 0; z < height; ++z)
                            for (y = 0; y < depth; ++y)
                                switch (type)
                                {
                                    case "flat":
                                        if (y != half)
                                            SetTile(x, y, z, (y >= half) ? Block.air : Block.dirt);
                                        else
                                            SetTile(x, y, z, Block.grass);
                                        break;
                                    case "pixel":
                                        if (y == 0)
                                            SetTile(x, y, z, Block.blackrock);
                                        else if (x == 0 || x == width - 1 || z == 0 || z == height - 1)
                                            SetTile(x, y, z, Block.white);
                                        break;
                                    case "box":
                                        if (y == 1 || y == 2)
                                            SetTile(x, y, z, Block.dirt);
                                        else if (y == 3)
                                            SetTile(x, y, z, Block.grass);
                                        if (y == 0 || y == depth - 1)
                                            SetTile(x, y, z, Block.blackrock);
                                        else if (x == 0 || x == width - 1 || z == 0 || z == height - 1)
                                            SetTile(x, y, z, Block.white);
                                        break;
                                }
                    break;

                case "island":
                case "mountains":
                case "ocean":
                case "forest":
                case "desert":
                    Server.MapGen.GenerateMap(this, type);
                    break;

                case "empty":
                default:
                    break;
            }

            spawnx = (ushort)(width / 2);
            spawny = (ushort)(depth * 0.75f);
            spawnz = (ushort)(height / 2);
            rotx = 0; roty = 0;
            Backup();
            if (MJS.Event.Exists("MapGenerate")) { MJS.Event.Trigger("MapGenerate", "map $map " + name); return; }
            Server.s.Log("Level initialized");
        }
        public Spleef GetSpleef()
        {
            foreach (Spleef spl in Spleef.Spleefs)
            {
                if (spl.Map == this) return spl;
            }
            return null;
        }
        public bool Unload()
        {
            Spleef spl = GetSpleef();
            if (spl != null) spl.Unload();
            if (Server.mainLevel == this) return false;

            Player.players.ForEach((pl) =>
            {
                if (pl.level == this) Command.all.Find("goto").Use(pl, Server.mainLevel.name);
            });

            if (changed)
            {
                Save();
                saveChanges();
            }
            physThread.Abort();
            physThread.Join();
            aboutTimer.Stop();
            aboutTimer.Dispose();

            Server.levels.Remove(this);
            GC.Collect();
            GC.WaitForPendingFinalizers();

            Player.GlobalMessageOps("&3" + name + Server.DefaultColor + " was unloaded.");
            Server.s.Log(name + " was unloaded.");
            return true;
        }

        public void saveChanges()
        {
            if (blockCache.Count == 0) return;
            Memory.SaveBlocks(name, blockCache);
            blockCache.Clear();
        }

        public ushort GetTile(ushort x, ushort y, ushort z)
        {
            return x >= width || y >= depth || z >= height ? Block.Zero : blocks[PosToInt(x, y, z)];
        }
        public ushort GetTile(int b)
        {
            return b < 0 || b >= blocks.Length ? Block.Zero : blocks[b];
        }

        public void SetTile(ushort x, ushort y, ushort z, ushort type)
        {
            blocks[PosToInt(x, y, z)] = type;
        }

        public static Level Find(string levelName)
        {
            Level tempLevel = null;
            bool returnNull = false;
            foreach (Level level in Server.levels)
            {
                if (level.name.ToLower() == levelName) return level;
                if (level.name.ToLower().IndexOf(levelName.ToLower()) != -1)
                    if (tempLevel == null) tempLevel = level;
                    else returnNull = true;
            }
            if (returnNull == true) return null;
            if (tempLevel != null) return tempLevel;
            return null;
        }

        public static Level FindExact(string levelName)
        {
            return Server.levels.Find(lvl => levelName.ToLower() == lvl.name.ToLower());
        }

        //public void Blockchange(Player p, ushort x, ushort y, ushort z, ushort type, byte action) { Blockchange(p, x, y, z, type, action == 1); }
        public void Blockchange(Player p, ushort x, ushort y, ushort z, ushort type, bool addaction, bool stealth = false, bool memorize = true)
        {
            string errorLocation = "start";
        retry: try
            {
                if (x < 0 || y < 0 || z < 0) return;
                if (x >= width || y >= depth || z >= height) return;

                ushort b = GetTile(x, y, z);

                errorLocation = "Zone checking";

                bool AllowBuild = true, foundDel = false, inZone = false;
                string Owners = "";
                List<Zone> toDel = new List<Zone>();
                if (p != null)
                {
                    if ((p.group.Permission < LevelPermission.Admin && !p.ignorePermission) || p.zoneCheck || p.zoneDel)
                    {
                        if (ZoneList.Count == 0) AllowBuild = true;
                        else
                            foreach (Zone Zn in ZoneList)
                                if (Zn.smallX <= x && x <= Zn.bigX && Zn.smallY <= y && y <= Zn.bigY && Zn.smallZ <= z && z <= Zn.bigZ)
                                {
                                    inZone = true;
                                    if (p.zoneDel)
                                    {
                                        foreach (string line in File.ReadAllLines("memory/zones/" + p.level.name + ".zone"))
                                            if (line == Zn.smallX + "," + Zn.smallY + "," + Zn.smallZ + "&" + Zn.bigX + "," + Zn.bigY + "," + Zn.bigZ + "&" + Zn.Owner)
                                                File.WriteAllText("memory/zones/" + p.level.name + ".zone", File.ReadAllText("memory/zones/" + p.level.name + ".zone").Replace(line, ""));
                                        toDel.Add(Zn);

                                        p.SendBlockchange(x, y, z, b);
                                        Player.SendMessage(p, "Zone deleted for &b" + Zn.Owner);
                                        foundDel = true;
                                    }
                                    else
                                    {
                                        if (Zn.Owner.Substring(0, 3) == "grp")
                                            if (Group.Find(Zn.Owner.Substring(3)).Permission <= p.group.Permission && !p.zoneCheck)
                                            {
                                                AllowBuild = true;
                                                break;
                                            }
                                            else
                                            {
                                                AllowBuild = false;
                                                Owners += ", " + Zn.Owner.Substring(3);
                                            }
                                        else
                                            if (Zn.Owner.ToLower() == p.name.ToLower() && !p.zoneCheck)
                                            {
                                                AllowBuild = true;
                                                break;
                                            }
                                            else
                                            {
                                                AllowBuild = false;
                                                Owners += ", " + Zn.Owner;
                                            }
                                    }
                                }

                        if (p.zoneDel)
                        {
                            if (!foundDel) Player.SendMessage(p, "No zones found to delete.");
                            else
                                foreach (Zone Zn in toDel)
                                    ZoneList.Remove(Zn);
                            p.zoneDel = false;
                            return;
                        }

                        if (!AllowBuild || p.zoneCheck)
                        {
                            if (Owners != "") Player.SendMessage(p, "This zone belongs to &b" + Owners.Remove(0, 2) + ".");
                            else Player.SendMessage(p, "This zone belongs to no one.");

                            p.ZoneSpam = DateTime.Now;
                            p.SendBlockchange(x, y, z, b);

                            if (p.zoneCheck) if (!p.staticCommands) p.zoneCheck = false;
                            return;
                        }
                    }

                    errorLocation = "Map rank checking";
                    if (Owners == "" && p.group.Permission < this.permissionbuild && (!inZone || !AllowBuild) && !p.ignorePermission)
                    {
                        p.SendBlockchange(x, y, z, b);
                        p.SendMessage("Must be at least " + LevelPermission.ToName(permissionbuild) + " to build here");
                        return;
                    }
                }

                errorLocation = "Block sending";
                if (Block.Convert(b) != Block.Convert(type) && !stealth)
                    Player.GlobalBlockchange(this, x, y, z, type);

                if (b == Block.sponge && physics > 0 && type != Block.sponge) PhysSpongeRemoved(PosToInt(x, y, z));

                errorLocation = "Undo buffer filling";
                Player.UndoPos Pos;
                Pos.x = x; Pos.y = y; Pos.z = z; Pos.mapName = name;
                Pos.type = b; Pos.newtype = type; Pos.timePlaced = DateTime.Now;
                if (p != null)
                    p.UndoBuffer.Add(Pos);

                errorLocation = "";
                if (!memorize)
                    Memory.AddBlock(this, x, y, z, !addaction, p.name);
                
                errorLocation = "Setting tile";
                SetTile(x, y, z, type);               //Updates server level blocks

                if (p != null)
                {
                    p.seasonBlocks++;
                    p.totalBlocks++;
                    if (type == 0) p.blocksGriefed++;
                    else p.blocksBuild++;
                    if (type == 3 || type == 2) p.builtEco++;
                }
                errorLocation = "Growing grass";
                if (GetTile(x, (ushort)(y - 1), z) == Block.grass && GrassDestroy && !Block.LightPass(type)) { Blockchange(p, x, (ushort)(y - 1), z, Block.dirt, true); }

                errorLocation = "Adding physics";
                if (physics > 0) if (Block.Physics(type)) AddCheck(PosToInt(x, y, z));

                changed = true;
                backedup = false;
            }
            catch (OutOfMemoryException)
            {
                p.SendMessage("Undo buffer too big! Cleared!");
                p.UndoBuffer.Clear();
                goto retry;
            }
            catch (Exception e)
            {
                Server.ErrorLog(e);
                Player.GlobalMessageOps(p.name + " triggered a non-fatal error on " + name);
                Player.GlobalMessageOps("Error location: " + errorLocation);
                Server.s.Log(p.name + " triggered a non-fatal error on " + name);
                Server.s.Log("Error location: " + errorLocation);
            }
        }
        public void PhysBlockchange(ushort x, ushort y, ushort z, ushort type, bool overRide = false, string extraInfo = "")    //Block change made by physics
        {
            if (x < 0 || y < 0 || z < 0) return;
            if (x >= width || y >= depth || z >= height) return;
            ushort b = GetTile(x, y, z);

            try
            {
                if (!overRide)
                    if (Block.BlockList.Find(bl => bl.type == b).rank >= LevelPermission.Operator) return;

                if (Block.Convert(b) != Block.Convert(type))    //Should save bandwidth sending identical looking blocks, like air/op_air changes.
                    Player.GlobalBlockchange(this, x, y, z, type);

                if (b == Block.sponge && physics > 0 && type != Block.sponge)
                    PhysSpongeRemoved(PosToInt(x, y, z));

                try
                {
                    UndoPos uP;
                    uP.location = PosToInt(x, y, z);
                    uP.newType = type;
                    uP.oldType = b;
                    uP.timePerformed = DateTime.Now;

                    if (currentUndo > Server.physUndo)
                    {
                        currentUndo = 0;
                        UndoBuffer[currentUndo] = uP;
                    }
                    else if (UndoBuffer.Count < Server.physUndo)
                    {
                        currentUndo++;
                        UndoBuffer.Add(uP);
                    }
                    else
                    {
                        currentUndo++;
                        UndoBuffer[currentUndo] = uP;
                    }
                }
                catch { }

                SetTile(x, y, z, type);               //Updates server level blocks

                if (physics > 0)
                    if (Block.Physics(type) || extraInfo != "") AddCheck(PosToInt(x, y, z), extraInfo);
            }
            catch
            {
                SetTile(x, y, z, type);
            }
        }

        public void Save()
        {
            string path = "levels/" + name + ".mcqlvl";
            try
            {
                if (!Directory.Exists("levels")) { Directory.CreateDirectory("levels"); }

                FileStream fs = File.Create(path);
                GZipStream gs = new GZipStream(fs, CompressionMode.Compress);

                byte[] header = new byte[16];
                BitConverter.GetBytes(2).CopyTo(header, 0);
                gs.Write(header, 0, 2);

                BitConverter.GetBytes(width).CopyTo(header, 0);
                BitConverter.GetBytes(height).CopyTo(header, 2);
                BitConverter.GetBytes(depth).CopyTo(header, 4);
                BitConverter.GetBytes(spawnx).CopyTo(header, 6);
                BitConverter.GetBytes(spawnz).CopyTo(header, 8);
                BitConverter.GetBytes(spawny).CopyTo(header, 10);
                header[12] = rotx; header[13] = roty;
                header[14] = (byte)permissionvisit;
                header[15] = (byte)permissionbuild;
                gs.Write(header, 0, header.Length);
                byte[] level = new byte[blocks.Length * (int)(Block.Max / 255 + 1)];
                int currentPos = 0;
                for (int i = 0; i < blocks.Length * (int)(Block.Max / 255 + 1); ++i)
                {
                    try
                    {
                        level[currentPos] = (blocks[i] >= 255) ? (byte)255 : (byte)blocks[i];
                        for (int ii = 1; level[currentPos] == 255; ++ii)
                        {
                            level[currentPos + 1] = ((blocks[i] - (255 * ii)) >= 255) ? (byte)255 : (byte)(blocks[i] - (255 * ii));
                            currentPos++;
                        }
                        currentPos++;
                    }
                    catch { break; }
                }
                gs.Write(level, 0, level.Length);
                gs.Close();
                fs.Close();
                gs.Dispose();
                fs.Dispose();

                if (!Directory.Exists("levels/properties")) Directory.CreateDirectory("levels/properties");
                try
                {
                    StreamWriter sw = new StreamWriter(File.Create("levels/properties/" + name + ".properties"));
                    sw.WriteLine("#properties for: " + name);
                    sw.WriteLine("Physics = " + physics);
                    sw.WriteLine("AntiSpeedHack = " + antispeedhack);
                    sw.WriteLine("Creator = " + creator);
                    sw.WriteLine("EdgeWater = " + edgeWater);
                    sw.WriteLine("Type = " + type);
                    /*sw.WriteLine("ZombieMap = " + zombiemod);
                      sw.WriteLine("Allow-Magic = " + allowMagic);
                      sw.WriteLine("Destructive-Magic = " + destructiveMagic);*/
                    sw.Flush();
                    sw.Close();
                    sw.Dispose();
                }
                catch (Exception ex) { Server.ErrorLog(ex); }

                Server.s.Log("SAVED: Level \"" + name + "\". " + ListPlayers.Count + "/" + Player.players.Count + "/" + Server.players);
                changed = false;
                try
                {
                    File.Copy(path, path + ".backup", true);
                    Server.s.Log("And backed up");
                }
                catch
                {
                    Server.s.Log("Failed to make backup");
                }
            }
            catch
            {
                Server.s.Log("FAILED TO SAVE :" + name);
                Player.GlobalMessage("FAILED TO SAVE :" + name);
                return;
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        public int Backup(bool Forced = false, string backupName = "")
        {
            if (!backedup || Forced)
            {
                int backupNumber = 1; string backupPath = "levels/backup";
                if (Directory.Exists(backupPath + "/" + name))
                {
                    backupNumber = Directory.GetDirectories(backupPath + "/" + name).Length + 1;
                }
                else
                {
                    Directory.CreateDirectory(backupPath + "/" + name);
                }
                string path = backupPath + "/" + name + "/" + (backupName == "" ? backupNumber + "" : backupName);
                Directory.CreateDirectory(path);

                string BackPath = path + "/" + name;
                string current = "levels/" + name;
                if (File.Exists(current + ".mcqlvl") || File.Exists(current + ".lvl"))
                    try
                    {
                        File.Copy(current + ".mcqlvl", BackPath + ".mcqlvl", true);
                        backedup = true;
                        return backupNumber;
                    }
                    catch
                    {
                        try
                        {
                            File.Copy(current + ".lvl", BackPath + ".lvl", true);
                            backedup = true;
                            return backupNumber;
                        }
                        catch (Exception ex)
                        {
                            Server.ErrorLog(ex);
                            if (name != "temp") Server.s.Log("FAILED TO INCREMENTAL BACKUP: " + name);
                            return -1;
                        }
                    }
                return -1;
            }
            else
            {
                Server.s.Log("Level unchanged, skipping backup");
                return -1;
            }
        }
        public static Level Load(string givenName) { return Load(givenName, 0); }
        public static Level Load(string givenName, byte phys)
        {
            if (!File.Exists("memory/zones/" + givenName + ".zone"))
                File.WriteAllText("memory/zones/" + givenName + ".zone", "");

            string path = "levels/" + givenName;
            Level level = null;

            try
            {
                if (File.Exists(path + ".mcqlvl"))
                {
                    path += ".mcqlvl";
                    FileStream fs = File.OpenRead(path);
                    try
                    {
                        GZipStream gs = new GZipStream(fs, CompressionMode.Decompress);
                        byte[] ver = new byte[2];
                        gs.Read(ver, 0, ver.Length);
                        ushort version = BitConverter.ToUInt16(ver, 0);
                        if (version == 2)
                        {
                            byte[] header = new byte[16];
                            gs.Read(header, 0, header.Length);
                            ushort width = BitConverter.ToUInt16(header, 0);
                            ushort height = BitConverter.ToUInt16(header, 2);
                            ushort depth = BitConverter.ToUInt16(header, 4);
                            level = new Level(givenName /*"temp"*/, width, depth, height, "empty", null);
                            level.spawnx = BitConverter.ToUInt16(header, 6);
                            level.spawnz = BitConverter.ToUInt16(header, 8);
                            level.spawny = BitConverter.ToUInt16(header, 10);
                            level.rotx = header[12];
                            level.roty = header[13];
                            level.permissionvisit = (int)header[14];
                            level.permissionbuild = (int)header[15];
                        }
                        else
                        {
                            Server.s.Log("ERROR loading level.");
                            return null;
                        }

                        level.name = givenName;
                        level.setPhysics(phys);

                        byte[] blocks = new byte[level.width * level.height * level.depth * (int)(Block.Max / 255 + 1)];
                        gs.Read(blocks, 0, blocks.Length);
                        gs.Close();

                        int currentPos = 0;
                        for (int i = 0; i < blocks.Length / (int)(Block.Max / 255 + 1); ++i)
                        {
                            try
                            {
                                level.blocks[i] = blocks[currentPos];
                                for (; blocks[currentPos] == 255; )
                                {
                                    currentPos++;
                                    level.blocks[i] += blocks[currentPos];
                                }
                                currentPos++;
                            }
                            catch (Exception ex) { Server.s.Log(ex + ""); break; }
                        }
                    }
                    catch (Exception ex) { Server.ErrorLog(ex); return null; }
                    finally { fs.Close(); }
                }
                else if (File.Exists(path + ".lvl"))
                {
                    path += ".lvl";
                    FileStream fs = File.OpenRead(path);
                    try
                    {
                        GZipStream gs = new GZipStream(fs, CompressionMode.Decompress);
                        byte[] ver = new byte[2];
                        gs.Read(ver, 0, ver.Length);
                        ushort version = BitConverter.ToUInt16(ver, 0);
                        if (version == 1874)
                        {
                            byte[] header = new byte[16]; gs.Read(header, 0, header.Length);
                            ushort width = BitConverter.ToUInt16(header, 0);
                            ushort height = BitConverter.ToUInt16(header, 2);
                            ushort depth = BitConverter.ToUInt16(header, 4);
                            level = new Level("temp", width, depth, height, "empty", null);
                            level.spawnx = BitConverter.ToUInt16(header, 6);
                            level.spawnz = BitConverter.ToUInt16(header, 8);
                            level.spawny = BitConverter.ToUInt16(header, 10);
                            level.rotx = header[12];
                            level.roty = header[13];
                            level.permissionvisit = (int)header[14];
                            level.permissionbuild = (int)header[15];
                        }
                        else
                        {
                            byte[] header = new byte[12]; gs.Read(header, 0, header.Length);
                            ushort width = version;
                            ushort height = BitConverter.ToUInt16(header, 0);
                            ushort depth = BitConverter.ToUInt16(header, 2);
                            level = new Level("temp", width, depth, height, "grass", null);
                            level.spawnx = BitConverter.ToUInt16(header, 4);
                            level.spawnz = BitConverter.ToUInt16(header, 6);
                            level.spawny = BitConverter.ToUInt16(header, 8);
                            level.rotx = header[10]; level.roty = header[11];
                        }

                        level.name = givenName;
                        level.setPhysics(phys);

                        byte[] blocks = new byte[level.width * level.height * level.depth];
                        gs.Read(blocks, 0, blocks.Length);
                        gs.Close();
                        for (int i = 0; i < blocks.Length; ++i)
                            level.blocks[i] = blocks[i];
                    }
                    catch (Exception ex) { Server.ErrorLog(ex); return null; }
                    finally { fs.Close(); }
                }

                else { Server.s.Log("ERROR loading level."); return null; }

                foreach (string line in File.ReadAllLines("memory/zones/" + givenName + ".zone"))
                {
                    if (line != "" && line[0] != '#')
                    {
                        string[] vars = line.Split('&');
                        Zone Zn;

                        Zn.smallX = ushort.Parse(vars[0].Split(',')[0]);
                        Zn.smallY = ushort.Parse(vars[0].Split(',')[1]);
                        Zn.smallZ = ushort.Parse(vars[0].Split(',')[2]);
                        Zn.bigX = ushort.Parse(vars[1].Split(',')[0]);
                        Zn.bigY = ushort.Parse(vars[1].Split(',')[1]);
                        Zn.bigZ = ushort.Parse(vars[1].Split(',')[2]);
                        Zn.Owner = vars[2];
                        level.ZoneList.Add(Zn);
                    }
                }

                level.jailx = (ushort)(level.spawnx * 32); level.jaily = (ushort)(level.spawny * 32); level.jailz = (ushort)(level.spawnz * 32);
                level.jailrotx = level.rotx; level.jailroty = level.roty;
                level.backedup = true;
                level.physThread = new Thread(new ThreadStart(level.Physics));

                try
                {
                    string foundLocation;
                    foundLocation = "levels/properties/" + level.name + ".properties";

                    foreach (string line in File.ReadAllLines(foundLocation))
                    {
                        try
                        {
                            if (line != "" && line[0] != '#')
                            {
                                string key = line.Split('=')[0].Trim();
                                string value = line.Split('=')[1].Trim();

                                switch (key.ToLower())
                                {
                                    case "physics": level.setPhysics(int.Parse(value)); break;
                                    case "antispeedhack": level.antispeedhack = value.ToLower() == "true"; break;
                                    case "creator": level.creator = value.ToLower(); break;
                                    case "edgewater": level.edgeWater = value.ToLower() == "true"; break;
                                    case "type": level.type = value.ToLower(); break;
                                    //case "allow-magic": level.allowMagic = (value.ToLower() == "true"); break;
                                    //case "destructive-magic": level.destructiveMagic = (value.ToLower()) == "true"); break;
                                }
                            }
                        }
                        catch (Exception e) { Server.ErrorLog(e); }
                    }
                }
                catch { }
                if (!Directory.Exists("memory/blocks")) Directory.CreateDirectory("memory/blocks");
                if (!File.Exists("memory/blocks/" + level.name + ".txt")) File.Create("memory/blocks/" + level.name + ".txt").Close();

                level.aboutTimer.Elapsed += delegate { if (level.changed)  level.saveChanges(); };
                level.aboutTimer.Start();

                Server.s.Log("Level \"" + level.name + "\" loaded.");
                return level;
            }
            catch (Exception ex) { Server.ErrorLog(ex); return null; }
        }

        public void ChatLevel(string message)
        {
            Player.players.ForEach((pl) =>
            {
                if (pl.level == this)
                    pl.SendMessage(message);
            });
        }

        public void setPhysics(int newValue)
        {
            if (physics == 0 && newValue != 0)
                for (int i = 0; i < blocks.Length; ++i)
                    if (Block.NeedRestart(blocks[i]))
                        AddCheck(i);

            physics = newValue;
        }

        public void Physics()
        {
            int wait = speedPhysics;
            while (true)
            {
                try
                {
                retry: if (wait > 0) Thread.Sleep(wait);
                    if (physics == 0 || ListCheck.Count == 0) goto retry;

                    DateTime Start = DateTime.Now;

                    if (physics > 0) CalcPhysics();

                    TimeSpan Took = DateTime.Now - Start;
                    wait = (int)speedPhysics - (int)Took.TotalMilliseconds;

                    if (wait < (int)(-overload * 0.75f))
                    {
                        Level Cause = this;

                        if (wait < -overload)
                        {
                            Cause.ClearPhysics();

                            Player.GlobalMessage("Physics shutdown on &b" + Cause.name);
                            Server.s.Log("Physics shutdown on " + name);

                            wait = speedPhysics;
                        }
                        else
                        {
                            foreach (Player p in Player.players)
                            {
                                if (p.level == this) p.SendMessage("Physics warning!");
                            }
                            Server.s.Log("Physics warning on " + name);
                        }
                    }
                }
                catch
                {
                    wait = speedPhysics;
                }
            }
        }
        public int PosToInt(ushort x, ushort y, ushort z)
        {
            if (x < 0) { return -1; }
            if (x >= width) { return -1; }
            if (y < 0) { return -1; }
            if (y >= depth) { return -1; }
            if (z < 0) { return -1; }
            if (z >= height) { return -1; }
            return x + z * width + y * width * height;
            //alternate method: (height * y + z) * width + x;
        }
        public void IntToPos(int pos, out ushort x, out ushort y, out ushort z)
        {
            y = (ushort)(pos / width / height);
            pos -= y * width * height;
            z = (ushort)(pos / width);
            pos -= z * width;
            x = (ushort)pos;
        }
        public int IntOffset(int pos, int x, int y, int z)
        {
            return pos + x + z * width + y * width * height;
        }

        #region ==Physics==
        public struct Pos { public ushort x, z; }

        public string foundInfo(ushort x, ushort y, ushort z)
        {
            Check foundCheck = null;
            try
            {
                foundCheck = ListCheck.Find(Check => Check.b == PosToInt(x, y, z));
            }
            catch { }
            if (foundCheck != null)
                return foundCheck.extraInfo;
            return "";
        }

        public void CalcPhysics()
        {
            try
            {
                if (physics > 0)
                {
                    ushort x, y, z; int mx, my, mz;

                    Random rand = new Random();
                    lastCheck = ListCheck.Count;
                    ListCheck.ForEach(delegate(Check C)
                    {
                        try
                        {
                            IntToPos(C.b, out x, out y, out z);
                            bool InnerChange = false; //bool skip = false;
                            int storedRand = 0;
                            Player foundPlayer = null; int foundNum = 75, currentNum, newNum, oldNum;
                            string foundInfo = C.extraInfo;

                        newPhysic: if (foundInfo != "")
                            {
                                int currentLoop = 0;
                                if (!foundInfo.Contains("wait")) if (blocks[C.b] == Block.air) C.extraInfo = "";

                                bool drop = false; int dropnum = 0;
                                bool wait = false; int waitnum = 0;
                                bool dissipate = false; int dissipatenum = 0;
                                bool revert = false; byte reverttype = 0;
                                bool explode = false; int explodenum = 0;
                                bool rainbow = false; int rainbownum = 0;

                                foreach (string s in C.extraInfo.Split(' '))
                                {
                                    if (currentLoop % 2 == 0)
                                    { //Type of code
                                        switch (s)
                                        {
                                            case "wait":
                                                wait = true;
                                                waitnum = int.Parse(C.extraInfo.Split(' ')[currentLoop + 1]);
                                                break;
                                            case "drop":
                                                drop = true;
                                                dropnum = int.Parse(C.extraInfo.Split(' ')[currentLoop + 1]);
                                                break;
                                            case "dissipate":
                                                dissipate = true;
                                                dissipatenum = int.Parse(C.extraInfo.Split(' ')[currentLoop + 1]);
                                                break;
                                            case "revert":
                                                revert = true;
                                                reverttype = Byte.Parse(C.extraInfo.Split(' ')[currentLoop + 1]);
                                                break;
                                            case "explode":
                                                explode = true;
                                                explodenum = int.Parse(C.extraInfo.Split(' ')[currentLoop + 1]);
                                                break;

                                            case "rainbow":
                                                rainbow = true;
                                                rainbownum = int.Parse(C.extraInfo.Split(' ')[currentLoop + 1]);
                                                break;
                                        }
                                    } currentLoop++;
                                }

                            startCheck:
                                if (wait)
                                {
                                    if (waitnum <= C.time)
                                    {
                                        wait = false;
                                        C.extraInfo = C.extraInfo.Substring(0, C.extraInfo.IndexOf("wait ")) + C.extraInfo.Substring(
                                            C.extraInfo.IndexOf(' ', C.extraInfo.IndexOf("wait ") + 5) + 1);
                                        goto startCheck;
                                    }
                                    else
                                    {
                                        C.time++;
                                        foundInfo = "";
                                        goto newPhysic;
                                    }
                                }
                                else
                                {
                                    if (rainbow)
                                        if (C.time < 4)
                                        {
                                            C.time++;
                                        }
                                        else
                                        {
                                            if (rainbownum > 2)
                                            {
                                                if (blocks[C.b] < Block.red || blocks[C.b] > Block.darkpink)
                                                {
                                                    AddUpdate(C.b, Block.red, true);
                                                }
                                                else
                                                {
                                                    if (blocks[C.b] == Block.darkpink) AddUpdate(C.b, Block.red);
                                                    else AddUpdate(C.b, (ushort)(blocks[C.b] + 1));
                                                }
                                            }
                                            else
                                            {
                                                AddUpdate(C.b, (ushort)rand.Next(21, 33));
                                            }
                                        }
                                    else
                                    {
                                        if (revert) { AddUpdate(C.b, reverttype); C.extraInfo = ""; }
                                        if (dissipate) if (rand.Next(1, 100) <= dissipatenum) { AddUpdate(C.b, Block.air); C.extraInfo = ""; }
                                        if (explode) if (rand.Next(1, 100) <= explodenum) { MakeExplosion(x, y, z, 0, 2); C.extraInfo = ""; }
                                        if (drop)
                                            if (rand.Next(1, 100) <= dropnum)
                                                if (GetTile(x, (ushort)(y - 1), z) == Block.air || GetTile(x, (ushort)(y - 1), z) == Block.lava || GetTile(x, (ushort)(y - 1), z) == Block.water)
                                                {
                                                    if (rand.Next(1, 100) < int.Parse(C.extraInfo.Split(' ')[1]))
                                                    {
                                                        AddUpdate(PosToInt(x, (ushort)(y - 1), z), blocks[C.b], false, C.extraInfo);
                                                        AddUpdate(C.b, Block.air); C.extraInfo = "";
                                                    }
                                                }

                                    }
                                }
                            }
                            else
                            {
                                switch (blocks[C.b])
                                {
                                    case Block.air:         //Placed air
                                        //initialy checks if block is valid
                                        PhysAir(PosToInt((ushort)(x + 1), y, z));
                                        PhysAir(PosToInt((ushort)(x - 1), y, z));
                                        PhysAir(PosToInt(x, y, (ushort)(z + 1)));
                                        PhysAir(PosToInt(x, y, (ushort)(z - 1)));
                                        PhysAir(PosToInt(x, (ushort)(y + 1), z));  //Check block above the air

                                        //Edge of map water
                                        if (edgeWater && y < depth / 2 && y >= (depth / 2) - 2 && (x == 0 || x == width - 1 || z == 0 || z == height - 1))
                                                AddUpdate(C.b, Block.water);

                                        if (!C.extraInfo.Contains("wait")) C.time = 255;
                                        break;


                                    case Block.dirt:     //Dirt
                                        if (C.time > 80)   //To grass
                                        {
                                            if (Block.LightPass(GetTile(x, (ushort)(y + 1), z)))
                                            {
                                                AddUpdate(C.b, Block.grass);
                                            }
                                            C.time = 255;
                                        }
                                        else
                                        {
                                            C.time++;
                                        }
                                        break;

                                    case Block.water:         //Active_water
                                    case Block.activedeathwater:
                                        //initialy checks if block is valid
                                        if (!PhysSpongeCheck(C.b))
                                        {
                                            if (GetTile(x, (ushort)(y + 1), z) != Block.Zero) { PhysSandCheck(PosToInt(x, (ushort)(y + 1), z)); }
                                            PhysWater(PosToInt((ushort)(x + 1), y, z), blocks[C.b]);
                                            PhysWater(PosToInt((ushort)(x - 1), y, z), blocks[C.b]);
                                            PhysWater(PosToInt(x, y, (ushort)(z + 1)), blocks[C.b]);
                                            PhysWater(PosToInt(x, y, (ushort)(z - 1)), blocks[C.b]);
                                            PhysWater(PosToInt(x, (ushort)(y - 1), z), blocks[C.b]);
                                        }
                                        else
                                        {
                                            AddUpdate(C.b, Block.air);  //was placed near sponge
                                        }
                                        if (C.extraInfo.IndexOf("wait") == -1) C.time = 255;
                                        break;
                                    #region fire
                                    case Block.fire:
                                        if (C.time < 2) { C.time++; break; }

                                        storedRand = rand.Next(1, 20);
                                        if (storedRand < 2 && C.time % 2 == 0)
                                        {
                                            storedRand = rand.Next(1, 18);

                                            if (storedRand <= 3 && GetTile((ushort)(x - 1), y, z) == Block.air)
                                                AddUpdate(PosToInt((ushort)(x - 1), y, z), Block.fire);
                                            else if (storedRand <= 6 && GetTile((ushort)(x + 1), y, z) == Block.air)
                                                AddUpdate(PosToInt((ushort)(x + 1), y, z), Block.fire);
                                            else if (storedRand <= 9 && GetTile(x, (ushort)(y - 1), z) == Block.air)
                                                AddUpdate(PosToInt(x, (ushort)(y - 1), z), Block.fire);
                                            else if (storedRand <= 12 && GetTile(x, (ushort)(y + 1), z) == Block.air)
                                                AddUpdate(PosToInt(x, (ushort)(y + 1), z), Block.fire);
                                            else if (storedRand <= 15 && GetTile(x, y, (ushort)(z - 1)) == Block.air)
                                                AddUpdate(PosToInt(x, y, (ushort)(z - 1)), Block.fire);
                                            else if (storedRand <= 18 && GetTile(x, y, (ushort)(z + 1)) == Block.air)
                                                AddUpdate(PosToInt(x, y, (ushort)(z + 1)), Block.fire);
                                        }

                                        if (Block.LavaKill(GetTile((ushort)(x - 1), y, (ushort)(z - 1))))
                                        {
                                            if (GetTile((ushort)(x - 1), y, z) == Block.air)
                                                AddUpdate(PosToInt((ushort)(x - 1), y, z), Block.fire);
                                            if (GetTile(x, y, (ushort)(z - 1)) == Block.air)
                                                AddUpdate(PosToInt(x, y, (ushort)(z - 1)), Block.fire);
                                        }
                                        if (Block.LavaKill(GetTile((ushort)(x + 1), y, (ushort)(z - 1))))
                                        {
                                            if (GetTile((ushort)(x + 1), y, z) == Block.air)
                                                AddUpdate(PosToInt((ushort)(x + 1), y, z), Block.fire);
                                            if (GetTile(x, y, (ushort)(z - 1)) == Block.air)
                                                AddUpdate(PosToInt(x, y, (ushort)(z - 1)), Block.fire);
                                        }
                                        if (Block.LavaKill(GetTile((ushort)(x - 1), y, (ushort)(z + 1))))
                                        {
                                            if (GetTile((ushort)(x - 1), y, z) == Block.air)
                                                AddUpdate(PosToInt((ushort)(x - 1), y, z), Block.fire);
                                            if (GetTile(x, y, (ushort)(z + 1)) == Block.air)
                                                AddUpdate(PosToInt(x, y, (ushort)(z + 1)), Block.fire);
                                        }
                                        if (Block.LavaKill(GetTile((ushort)(x + 1), y, (ushort)(z + 1))))
                                        {
                                            if (GetTile((ushort)(x + 1), y, z) == Block.air)
                                                AddUpdate(PosToInt((ushort)(x + 1), y, z), Block.fire);
                                            if (GetTile(x, y, (ushort)(z + 1)) == Block.air)
                                                AddUpdate(PosToInt(x, y, (ushort)(z + 1)), Block.fire);
                                        }
                                        if (Block.LavaKill(GetTile(x, (ushort)(y - 1), (ushort)(z - 1))))
                                        {
                                            if (GetTile(x, (ushort)(y - 1), z) == Block.air)
                                                AddUpdate(PosToInt(x, (ushort)(y - 1), z), Block.fire);
                                            if (GetTile(x, y, (ushort)(z - 1)) == Block.air)
                                                AddUpdate(PosToInt(x, y, (ushort)(z - 1)), Block.fire);
                                        }
                                        else if (GetTile(x, (ushort)(y - 1), z) == Block.grass)
                                            AddUpdate(PosToInt(x, (ushort)(y - 1), z), Block.dirt);

                                        if (Block.LavaKill(GetTile(x, (ushort)(y + 1), (ushort)(z - 1))))
                                        {
                                            if (GetTile(x, (ushort)(y + 1), z) == Block.air)
                                                AddUpdate(PosToInt(x, (ushort)(y + 1), z), Block.fire);
                                            if (GetTile(x, y, (ushort)(z - 1)) == Block.air)
                                                AddUpdate(PosToInt(x, y, (ushort)(z - 1)), Block.fire);
                                        }
                                        if (Block.LavaKill(GetTile(x, (ushort)(y - 1), (ushort)(z + 1))))
                                        {
                                            if (GetTile(x, (ushort)(y - 1), z) == Block.air)
                                                AddUpdate(PosToInt(x, (ushort)(y - 1), z), Block.fire);
                                            if (GetTile(x, y, (ushort)(z + 1)) == Block.air)
                                                AddUpdate(PosToInt(x, y, (ushort)(z + 1)), Block.fire);
                                        }
                                        if (Block.LavaKill(GetTile(x, (ushort)(y + 1), (ushort)(z + 1))))
                                        {
                                            if (GetTile(x, (ushort)(y + 1), z) == Block.air)
                                                AddUpdate(PosToInt(x, (ushort)(y + 1), z), Block.fire);
                                            if (GetTile(x, y, (ushort)(z + 1)) == Block.air)
                                                AddUpdate(PosToInt(x, y, (ushort)(z + 1)), Block.fire);
                                        }
                                        if (Block.LavaKill(GetTile((ushort)(x - 1), (ushort)(y - 1), z)))
                                        {
                                            if (GetTile(x, (ushort)(y - 1), z) == Block.air)
                                                AddUpdate(PosToInt(x, (ushort)(y - 1), z), Block.fire);
                                            if (GetTile((ushort)(x - 1), y, z) == Block.air)
                                                AddUpdate(PosToInt((ushort)(x - 1), y, z), Block.fire);
                                        }
                                        if (Block.LavaKill(GetTile((ushort)(x - 1), (ushort)(y + 1), z)))
                                        {
                                            if (GetTile(x, (ushort)(y + 1), z) == Block.air)
                                                AddUpdate(PosToInt(x, (ushort)(y + 1), z), Block.fire);
                                            if (GetTile((ushort)(x - 1), y, z) == Block.air)
                                                AddUpdate(PosToInt((ushort)(x - 1), y, z), Block.fire);
                                        }
                                        if (Block.LavaKill(GetTile((ushort)(x + 1), (ushort)(y - 1), z)))
                                        {
                                            if (GetTile(x, (ushort)(y - 1), z) == Block.air)
                                                AddUpdate(PosToInt(x, (ushort)(y - 1), z), Block.fire);
                                            if (GetTile((ushort)(x + 1), y, z) == Block.air)
                                                AddUpdate(PosToInt((ushort)(x + 1), y, z), Block.fire);
                                        }
                                        if (Block.LavaKill(GetTile((ushort)(x + 1), (ushort)(y + 1), z)))
                                        {
                                            if (GetTile(x, (ushort)(y + 1), z) == Block.air)
                                                AddUpdate(PosToInt(x, (ushort)(y + 1), z), Block.fire);
                                            if (GetTile((ushort)(x + 1), y, z) == Block.air)
                                                AddUpdate(PosToInt((ushort)(x + 1), y, z), Block.fire);
                                        }

                                        if (physics >= 2)
                                        {
                                            if (C.time < 4) { C.time++; break; }

                                            if (Block.LavaKill(GetTile((ushort)(x - 1), y, z)))
                                                AddUpdate(PosToInt((ushort)(x - 1), y, z), Block.fire);
                                            else if (GetTile((ushort)(x - 1), y, z) == Block.tnt)
                                                MakeExplosion((ushort)(x - 1), y, z, -1, 2);

                                            if (Block.LavaKill(GetTile((ushort)(x + 1), y, z)))
                                                AddUpdate(PosToInt((ushort)(x + 1), y, z), Block.fire);
                                            else if (GetTile((ushort)(x + 1), y, z) == Block.tnt)
                                                MakeExplosion((ushort)(x + 1), y, z, -1, 2);

                                            if (Block.LavaKill(GetTile(x, (ushort)(y - 1), z)))
                                                AddUpdate(PosToInt(x, (ushort)(y - 1), z), Block.fire);
                                            else if (GetTile(x, (ushort)(y - 1), z) == Block.tnt)
                                                MakeExplosion(x, (ushort)(y - 1), z, -1, 2);

                                            if (Block.LavaKill(GetTile(x, (ushort)(y + 1), z)))
                                                AddUpdate(PosToInt(x, (ushort)(y + 1), z), Block.fire);
                                            else if (GetTile(x, (ushort)(y + 1), z) == Block.tnt)
                                                MakeExplosion(x, (ushort)(y + 1), z, -1, 2);

                                            if (Block.LavaKill(GetTile(x, y, (ushort)(z - 1))))
                                                AddUpdate(PosToInt(x, y, (ushort)(z - 1)), Block.fire);
                                            else if (GetTile(x, y, (ushort)(z - 1)) == Block.tnt)
                                                MakeExplosion(x, y, (ushort)(z - 1), -1, 2);

                                            if (Block.LavaKill(GetTile(x, y, (ushort)(z + 1))))
                                                AddUpdate(PosToInt(x, y, (ushort)(z + 1)), Block.fire);
                                            else if (GetTile(x, y, (ushort)(z + 1)) == Block.tnt)
                                                MakeExplosion(x, y, (ushort)(z + 1), -1, 2);
                                        }

                                        C.time++;
                                        if (C.time > 5)
                                        {
                                            storedRand = (rand.Next(1, 10));
                                            if (storedRand <= 2) { AddUpdate(C.b, Block.coal); C.extraInfo = "drop 63 dissipate 10"; }
                                            else if (storedRand <= 4) { AddUpdate(C.b, Block.obsidian); C.extraInfo = "drop 63 dissipate 10"; }
                                            else if (storedRand <= 8) AddUpdate(C.b, Block.air);
                                            else C.time = 3;
                                        }

                                        break;
                                    #endregion
                                    case Block.lava:         //Active_lava
                                    case Block.activedeathlava:
                                        //initialy checks if block is valid
                                        if (C.time >= 4)
                                        {
                                            PhysLava(PosToInt((ushort)(x + 1), y, z), blocks[C.b]);
                                            PhysLava(PosToInt((ushort)(x - 1), y, z), blocks[C.b]);
                                            PhysLava(PosToInt(x, y, (ushort)(z + 1)), blocks[C.b]);
                                            PhysLava(PosToInt(x, y, (ushort)(z - 1)), blocks[C.b]);
                                            PhysLava(PosToInt(x, (ushort)(y - 1), z), blocks[C.b]);
                                            C.time = 255;
                                        }
                                        else
                                        { C.time++; }
                                        break;

                                    case Block.sand:    //Sand
                                        if (PhysSand(C.b, Block.sand))
                                        {
                                            PhysAir(PosToInt((ushort)(x + 1), y, z));
                                            PhysAir(PosToInt((ushort)(x - 1), y, z));
                                            PhysAir(PosToInt(x, y, (ushort)(z + 1)));
                                            PhysAir(PosToInt(x, y, (ushort)(z - 1)));
                                            PhysAir(PosToInt(x, (ushort)(y + 1), z));   //Check block above
                                        }
                                        C.time = 255;
                                        break;

                                    case Block.gravel:    //Gravel
                                        if (PhysSand(C.b, Block.gravel))
                                        {
                                            PhysAir(PosToInt((ushort)(x + 1), y, z));
                                            PhysAir(PosToInt((ushort)(x - 1), y, z));
                                            PhysAir(PosToInt(x, y, (ushort)(z + 1)));
                                            PhysAir(PosToInt(x, y, (ushort)(z - 1)));
                                            PhysAir(PosToInt(x, (ushort)(y + 1), z));   //Check block above
                                        }
                                        C.time = 255;
                                        break;

                                    case Block.sponge:    //SPONGE
                                        PhysSponge(C.b);
                                        C.time = 255;
                                        break;

                                    //Adv physics updating anything placed next to water or lava
                                    case Block.wood:     //Wood to die in lava
                                    case Block.shrub:     //Tree and plants follow
                                    case Block.trunk:    //Wood to die in lava
                                    case Block.leaf:    //Bushes die in lava
                                    case Block.yellowflower:
                                    case Block.redflower:
                                    case Block.mushroom:
                                    case Block.redmushroom:
                                    case Block.bookcase:    //bookcase
                                        if (physics == 2)   //Adv physics kills flowers and mushroos in water/lava
                                        {
                                            PhysAir(PosToInt((ushort)(x + 1), y, z));
                                            PhysAir(PosToInt((ushort)(x - 1), y, z));
                                            PhysAir(PosToInt(x, y, (ushort)(z + 1)));
                                            PhysAir(PosToInt(x, y, (ushort)(z - 1)));
                                            PhysAir(PosToInt(x, (ushort)(y + 1), z));   //Check block above
                                        }
                                        C.time = 255;
                                        break;

                                    case Block.staircasestep:
                                        PhysStair(C.b);
                                        C.time = 255;
                                        break;

                                    case Block.wood_float:   //wood_float
                                        PhysFloatwood(C.b);
                                        C.time = 255;
                                        break;

                                    case Block.lava_fast:         //lava_fast
                                        //initialy checks if block is valid
                                        PhysLava(PosToInt((ushort)(x + 1), y, z), Block.lava_fast);
                                        PhysLava(PosToInt((ushort)(x - 1), y, z), Block.lava_fast);
                                        PhysLava(PosToInt(x, y, (ushort)(z + 1)), Block.lava_fast);
                                        PhysLava(PosToInt(x, y, (ushort)(z - 1)), Block.lava_fast);
                                        PhysLava(PosToInt(x, (ushort)(y - 1), z), Block.lava_fast);
                                        C.time = 255;
                                        break;

                                    //Special blocks that are not saved
                                    case Block.air_flood:   //air_flood
                                        if (C.time < 1)
                                        {
                                            PhysAirFlood(PosToInt((ushort)(x + 1), y, z), Block.air_flood);
                                            PhysAirFlood(PosToInt((ushort)(x - 1), y, z), Block.air_flood);
                                            PhysAirFlood(PosToInt(x, y, (ushort)(z + 1)), Block.air_flood);
                                            PhysAirFlood(PosToInt(x, y, (ushort)(z - 1)), Block.air_flood);
                                            PhysAirFlood(PosToInt(x, (ushort)(y - 1), z), Block.air_flood);
                                            PhysAirFlood(PosToInt(x, (ushort)(y + 1), z), Block.air_flood);

                                            C.time++;
                                        }
                                        else
                                        {
                                            AddUpdate(C.b, 0);    //Turn back into normal air
                                            C.time = 255;
                                        }
                                        break;

                                    case Block.door_air:   //door_air         Change any door blocks nearby into door_air
                                    case Block.door2_air:   //door_air         Change any door blocks nearby into door_air
                                    case Block.door3_air:   //door_air         Change any door blocks nearby into door_air
                                    case Block.door4_air:   //door_air         Change any door blocks nearby into door_air
                                    case Block.door5_air:   //door_air         Change any door blocks nearby into door_air
                                    case Block.door6_air:   //door_air         Change any door blocks nearby into door_air
                                    case Block.door7_air:   //door_air         Change any door blocks nearby into door_air
                                    case Block.door8_air:   //door_air         Change any door blocks nearby into door_air
                                    case Block.door10_air:   //door_air         Change any door blocks nearby into door_air
                                    case Block.door12_air:
                                    case Block.door13_air:
                                        AnyDoor(C, x, y, z, 16); break;
                                    case Block.door11_air:
                                    case Block.door14_air:
                                        AnyDoor(C, x, y, z, 4, true); break;
                                    case Block.door9_air:   //door_air         Change any door blocks nearby into door_air
                                        AnyDoor(C, x, y, z, 4); break;


                                    case Block.air_flood_layer:   //air_flood_layer
                                        if (C.time < 1)
                                        {
                                            PhysAirFlood(PosToInt((ushort)(x + 1), y, z), Block.air_flood_layer);
                                            PhysAirFlood(PosToInt((ushort)(x - 1), y, z), Block.air_flood_layer);
                                            PhysAirFlood(PosToInt(x, y, (ushort)(z + 1)), Block.air_flood_layer);
                                            PhysAirFlood(PosToInt(x, y, (ushort)(z - 1)), Block.air_flood_layer);

                                            C.time++;
                                        }
                                        else
                                        {
                                            AddUpdate(C.b, 0);    //Turn back into normal air
                                            C.time = 255;
                                        }
                                        break;

                                    case Block.air_flood_down:   //air_flood_down
                                        if (C.time < 1)
                                        {
                                            PhysAirFlood(PosToInt((ushort)(x + 1), y, z), Block.air_flood_down);
                                            PhysAirFlood(PosToInt((ushort)(x - 1), y, z), Block.air_flood_down);
                                            PhysAirFlood(PosToInt(x, y, (ushort)(z + 1)), Block.air_flood_down);
                                            PhysAirFlood(PosToInt(x, y, (ushort)(z - 1)), Block.air_flood_down);
                                            PhysAirFlood(PosToInt(x, (ushort)(y - 1), z), Block.air_flood_down);

                                            C.time++;
                                        }
                                        else
                                        {
                                            AddUpdate(C.b, 0);    //Turn back into normal air
                                            C.time = 255;
                                        }
                                        break;

                                    case Block.air_flood_up:   //air_flood_up
                                        if (C.time < 1)
                                        {
                                            PhysAirFlood(PosToInt((ushort)(x + 1), y, z), Block.air_flood_up);
                                            PhysAirFlood(PosToInt((ushort)(x - 1), y, z), Block.air_flood_up);
                                            PhysAirFlood(PosToInt(x, y, (ushort)(z + 1)), Block.air_flood_up);
                                            PhysAirFlood(PosToInt(x, y, (ushort)(z - 1)), Block.air_flood_up);
                                            PhysAirFlood(PosToInt(x, (ushort)(y + 1), z), Block.air_flood_up);

                                            C.time++;
                                        }
                                        else
                                        {
                                            AddUpdate(C.b, 0);    //Turn back into normal air
                                            C.time = 255;
                                        }
                                        break;


                                    case Block.smalltnt:
                                        if (physics < 2) this.PhysBlockchange(x, y, z, Block.air);

                                        if (physics >= 2)
                                        {
                                            rand = new Random();

                                            if (C.time < 5 && physics == 2)
                                            {
                                                C.time += 1;
                                                if (this.GetTile(x, (ushort)(y + 1), z) == Block.lavastill)
                                                    this.PhysBlockchange(x, (ushort)(y + 1), z, Block.air);
                                                else
                                                    this.PhysBlockchange(x, (ushort)(y + 1), z, Block.lavastill);
                                                break;
                                            }

                                            MakeExplosion(x, y, z, 0, 2);
                                        }
                                        else { this.PhysBlockchange(x, y, z, Block.air); }
                                        break;

                                    case Block.bigtnt:
                                        if (physics < 2) this.PhysBlockchange(x, y, z, Block.air);

                                        if (physics >= 2)
                                        {
                                            rand = new Random();

                                            if (C.time < 5 && physics == 2)
                                            {
                                                C.time += 1;
                                                if (this.GetTile(x, (ushort)(y + 1), z) == Block.lavastill) this.PhysBlockchange(x, (ushort)(y + 1), z, Block.air); else this.PhysBlockchange(x, (ushort)(y + 1), z, Block.lavastill);
                                                if (this.GetTile(x, (ushort)(y - 1), z) == Block.lavastill) this.PhysBlockchange(x, (ushort)(y - 1), z, Block.air); else this.PhysBlockchange(x, (ushort)(y - 1), z, Block.lavastill);
                                                if (this.GetTile((ushort)(x + 1), y, z) == Block.lavastill) this.PhysBlockchange((ushort)(x + 1), y, z, Block.air); else this.PhysBlockchange((ushort)(x + 1), y, z, Block.lavastill);
                                                if (this.GetTile((ushort)(x - 1), y, z) == Block.lavastill) this.PhysBlockchange((ushort)(x - 1), y, z, Block.air); else this.PhysBlockchange((ushort)(x - 1), y, z, Block.lavastill);
                                                if (this.GetTile(x, y, (ushort)(z + 1)) == Block.lavastill) this.PhysBlockchange(x, y, (ushort)(z + 1), Block.air); else this.PhysBlockchange(x, y, (ushort)(z + 1), Block.lavastill);
                                                if (this.GetTile(x, y, (ushort)(z - 1)) == Block.lavastill) this.PhysBlockchange(x, y, (ushort)(z - 1), Block.air); else this.PhysBlockchange(x, y, (ushort)(z - 1), Block.lavastill);

                                                break;
                                            }

                                            MakeExplosion(x, y, z, 1, 2);
                                        }
                                        else { this.PhysBlockchange(x, y, z, Block.air); }
                                        break;

                                    case Block.tntexplosion:
                                        if (rand.Next(1, 11) <= 7) AddUpdate(C.b, Block.air);
                                        break;

                                    case Block.train:
                                        if (rand.Next(1, 10) <= 5) mx = 1; else mx = -1;
                                        if (rand.Next(1, 10) <= 5) my = 1; else my = -1;
                                        if (rand.Next(1, 10) <= 5) mz = 1; else mz = -1;

                                        for (int cx = (-1 * mx); cx != ((1 * mx) + mx); cx = cx + (1 * mx))
                                            for (int cy = (-1 * my); cy != ((1 * my) + my); cy = cy + (1 * my))
                                                for (int cz = (-1 * mz); cz != ((1 * mz) + mz); cz = cz + (1 * mz))
                                                {
                                                    if (GetTile((ushort)(x + cx), (ushort)(y + cy - 1), (ushort)(z + cz)) == Block.red && (GetTile((ushort)(x + cx), (ushort)(y + cy), (ushort)(z + cz)) == Block.air || GetTile((ushort)(x + cx), (ushort)(y + cy), (ushort)(z + cz)) == Block.water) && !InnerChange)
                                                    {
                                                        AddUpdate(PosToInt((ushort)(x + cx), (ushort)(y + cy), (ushort)(z + cz)), Block.train);
                                                        AddUpdate(PosToInt(x, y, z), Block.air);
                                                        AddUpdate(IntOffset(C.b, 0, -1, 0), Block.traintail, true, "wait 3 revert " + Block.red);

                                                        InnerChange = true;
                                                        break;
                                                    }
                                                    else if (GetTile((ushort)(x + cx), (ushort)(y + cy - 1), (ushort)(z + cz)) == Block.op_air && (GetTile((ushort)(x + cx), (ushort)(y + cy), (ushort)(z + cz)) == Block.air || GetTile((ushort)(x + cx), (ushort)(y + cy), (ushort)(z + cz)) == Block.water) && !InnerChange)
                                                    {
                                                        AddUpdate(PosToInt((ushort)(x + cx), (ushort)(y + cy), (ushort)(z + cz)), Block.train);
                                                        AddUpdate(PosToInt(x, y, z), Block.air);
                                                        AddUpdate(IntOffset(C.b, 0, -1, 0), Block.traintail, true, "wait 3 revert " + Block.op_air);
                                                        InnerChange = true;
                                                        break;

                                                    }
                                                }
                                        break;
                                    case Block.traintail:
                                        if (C.extraInfo.IndexOf("wait") == -1)
                                            C.extraInfo = "revert " + Block.red;
                                        break;
                                    case Block.magma:
                                        C.time++;
                                        if (C.time < 3) break;

                                        if (GetTile(x, (ushort)(y - 1), z) == Block.air)
                                            AddUpdate(PosToInt(x, (ushort)(y - 1), z), Block.magma);
                                        else if (GetTile(x, (ushort)(y - 1), z) != Block.magma)
                                        {
                                            PhysLava(PosToInt((ushort)(x + 1), y, z), blocks[C.b]);
                                            PhysLava(PosToInt((ushort)(x - 1), y, z), blocks[C.b]);
                                            PhysLava(PosToInt(x, y, (ushort)(z + 1)), blocks[C.b]);
                                            PhysLava(PosToInt(x, y, (ushort)(z - 1)), blocks[C.b]);
                                        }

                                        if (physics > 1)
                                        {
                                            if (C.time > 10)
                                            {
                                                C.time = 0;

                                                if (Block.LavaKill(GetTile((ushort)(x + 1), y, z)))
                                                {
                                                    AddUpdate(PosToInt((ushort)(x + 1), y, z), Block.magma);
                                                    InnerChange = true;
                                                } 
                                                if (Block.LavaKill(GetTile((ushort)(x - 1), y, z)))
                                                {
                                                    AddUpdate(PosToInt((ushort)(x - 1), y, z), Block.magma);
                                                    InnerChange = true;
                                                } 
                                                if (Block.LavaKill(GetTile(x, y, (ushort)(z + 1))))
                                                {
                                                    AddUpdate(PosToInt(x, y, (ushort)(z + 1)), Block.magma);
                                                    InnerChange = true;
                                                } 
                                                if (Block.LavaKill(GetTile(x, y, (ushort)(z - 1))))
                                                {
                                                    AddUpdate(PosToInt(x, y, (ushort)(z - 1)), Block.magma);
                                                    InnerChange = true;
                                                } 
                                                if (Block.LavaKill(GetTile(x, (ushort)(y - 1), z)))
                                                {
                                                    AddUpdate(PosToInt(x, (ushort)(y - 1), z), Block.magma);
                                                    InnerChange = true;
                                                }

                                                if (InnerChange == true)
                                                {
                                                    if (Block.LavaKill(GetTile(x, (ushort)(y + 1), z)))
                                                        AddUpdate(PosToInt(x, (ushort)(y + 1), z), Block.magma);
                                                }
                                            }
                                        }
                                        break;
                                    case Block.firework:
                                        if (GetTile(x, (ushort)(y - 1), z) == Block.lavastill)
                                        {
                                            if (GetTile(x, (ushort)(y + 1), z) == Block.air)
                                            {
                                                if ((depth / 100) * 80 < y) mx = rand.Next(1, 20);
                                                else mx = 5;

                                                if (mx > 1)
                                                {
                                                    AddUpdate(PosToInt(x, (ushort)(y + 1), z), Block.firework);
                                                    AddUpdate(PosToInt(x, y, z), Block.lavastill);
                                                    C.extraInfo = "wait 2 dissipate 100";
                                                    break;
                                                }
                                            }
                                            Firework(x, y, z, 4); break;
                                        }
                                        break;
                                    case Block.birdblack:
                                    case Block.birdwhite:
                                    case Block.birdlava:
                                    case Block.birdwater:
                                        switch (rand.Next(1, 15))
                                        {
                                            case 1:
                                                if (GetTile(x, (ushort)(y - 1), z) == Block.air)
                                                    AddUpdate(PosToInt(x, (ushort)(y - 1), z), blocks[C.b]);
                                                else goto case 3;
                                                break;
                                            case 2:
                                                if (GetTile(x, (ushort)(y + 1), z) == Block.air)
                                                    AddUpdate(PosToInt(x, (ushort)(y + 1), z), blocks[C.b]);
                                                else goto case 6;
                                                break;
                                            case 3:
                                            case 4:
                                            case 5:
                                                if (GetTile((ushort)(x - 1), y, z) == Block.air)
                                                    AddUpdate(PosToInt((ushort)(x - 1), y, z), blocks[C.b]);
                                                else if (GetTile((ushort)(x - 1), y, z) == Block.op_air) { break; }
                                                else AddUpdate(C.b, Block.red, false, "dissipate 25");
                                                break;
                                            case 6:
                                            case 7:
                                            case 8:
                                                if (GetTile((ushort)(x + 1), y, z) == Block.air)
                                                    AddUpdate(PosToInt((ushort)(x + 1), y, z), blocks[C.b]);
                                                else if (GetTile((ushort)(x + 1), y, z) == Block.op_air) { break; }
                                                else AddUpdate(C.b, Block.red, false, "dissipate 25");
                                                break;
                                            case 9:
                                            case 10:
                                            case 11:
                                                if (GetTile(x, y, (ushort)(z - 1)) == Block.air)
                                                    AddUpdate(PosToInt(x, y, (ushort)(z - 1)), blocks[C.b]);
                                                else if (GetTile(x, y, (ushort)(z - 1)) == Block.op_air) { break; }
                                                else AddUpdate(C.b, Block.red, false, "dissipate 25");
                                                break;
                                            case 12:
                                            case 13:
                                            case 14:
                                            default:
                                                if (GetTile(x, y, (ushort)(z + 1)) == Block.air)
                                                    AddUpdate(PosToInt(x, y, (ushort)(z + 1)), blocks[C.b]);
                                                else if (GetTile(x, y, (ushort)(z + 1)) == Block.op_air) { break; }
                                                else AddUpdate(C.b, Block.red, false, "dissipate 25");
                                                break;
                                        }
                                        AddUpdate(C.b, Block.air);
                                        C.time = 255;

                                        break;

                                    case Block.snaketail:
                                        if (GetTile(IntOffset(C.b, -1, 0, 0)) != Block.snake || GetTile(IntOffset(C.b, 1, 0, 0)) != Block.snake || GetTile(IntOffset(C.b, 0, 0, 1)) != Block.snake ||
                                            GetTile(IntOffset(C.b, 0, 0, -1)) != Block.snake)
                                            C.extraInfo = "revert 0";
                                        break;
                                    case Block.snake:
                                        #region SNAKE
                                        if (ai)
                                            Player.players.ForEach(delegate(Player p)
                                            {
                                                if (p.level == this && !p.invincible)
                                                {
                                                    currentNum = Math.Abs((p.pos[0] / 32) - x) + Math.Abs((p.pos[1] / 32) - y) + Math.Abs((p.pos[2] / 32) - z);
                                                    if (currentNum < foundNum)
                                                    {
                                                        foundNum = currentNum;
                                                        foundPlayer = p;
                                                    }
                                                }
                                            });

                                    randomMovement_Snake:
                                        if (foundPlayer != null && rand.Next(1, 20) < 19)
                                        {
                                            currentNum = rand.Next(1, 10);
                                            foundNum = 0;

                                            switch (currentNum)
                                            {
                                                case 1:
                                                case 2:
                                                case 3:
                                                    if ((foundPlayer.pos[0] / 32) - x != 0)
                                                    {
                                                        newNum = PosToInt((ushort)(x + Math.Sign((foundPlayer.pos[0] / 32) - x)), y, z);
                                                        if (GetTile(newNum) == Block.air)
                                                            if (IntOffset(newNum, -1, 0, 0) == Block.grass || IntOffset(newNum, -1, 0, 0) == Block.dirt)
                                                                if (AddUpdate(newNum, blocks[C.b]))
                                                                    goto removeSelf_Snake;
                                                    }
                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 4;

                                                case 4:
                                                case 5:
                                                case 6:
                                                    if ((foundPlayer.pos[1] / 32) - y != 0)
                                                    {
                                                        newNum = PosToInt(x, (ushort)(y + Math.Sign((foundPlayer.pos[1] / 32) - y)), z);
                                                        if (GetTile(newNum) == Block.air)
                                                            if (newNum > 0)
                                                            {
                                                                if (IntOffset(newNum, 0, 1, 0) == Block.grass || IntOffset(newNum, 0, 1, 0) == Block.dirt && IntOffset(newNum, 0, 2, 0) == Block.air)
                                                                    if (AddUpdate(newNum, blocks[C.b]))
                                                                        goto removeSelf_Snake;
                                                            }
                                                            else
                                                                if (newNum < 0)
                                                                {
                                                                    if (IntOffset(newNum, 0, -2, 0) == Block.grass || IntOffset(newNum, 0, -2, 0) == Block.dirt && IntOffset(newNum, 0, -1, 0) == Block.air)
                                                                        if (AddUpdate(newNum, blocks[C.b]))
                                                                            goto removeSelf_Snake;
                                                                }
                                                    }
                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 7;

                                                case 7:
                                                case 8:
                                                case 9:
                                                    if ((foundPlayer.pos[2] / 32) - z != 0)
                                                    {
                                                        newNum = PosToInt(x, y, (ushort)(z + Math.Sign((foundPlayer.pos[2] / 32) - z)));
                                                        if (GetTile(newNum) == Block.air)
                                                            if (IntOffset(newNum, 0, 0, -1) == Block.grass || IntOffset(newNum, 0, 0, -1) == Block.dirt)
                                                                if (AddUpdate(newNum, blocks[C.b]))
                                                                    goto removeSelf_Snake;
                                                    }
                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 1;
                                                default:
                                                    foundPlayer = null; goto randomMovement_Snake;
                                            }
                                        }
                                        else
                                        {

                                            switch (rand.Next(1, 13))
                                            {
                                                case 1:
                                                case 2:
                                                case 3:
                                                    newNum = IntOffset(C.b, -1, 0, 0);
                                                    oldNum = PosToInt(x, y, z);

                                                    if (GetTile(IntOffset(newNum, 0, -1, 0)) == Block.air && GetTile(newNum) == Block.air)
                                                        newNum = IntOffset(newNum, 0, -1, 0);
                                                    else if (GetTile(newNum) == Block.air && GetTile(IntOffset(newNum, 0, 1, 0)) == Block.air) { }

                                                    else if (GetTile(IntOffset(newNum, 0, 2, 0)) == Block.air && GetTile(IntOffset(newNum, 0, 1, 0)) == Block.air)
                                                        newNum = IntOffset(newNum, 0, 1, 0);
                                                    //else skip = true;

                                                    if (AddUpdate(newNum, blocks[C.b]))
                                                    {
                                                        AddUpdate(IntOffset(oldNum, 0, 0, 0), Block.snaketail, true, "wait 5 revert " + Block.air.ToString());
                                                        goto removeSelf_Snake;
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 4) InnerChange = true; else goto case 4;
                                                    break;

                                                case 4:
                                                case 5:
                                                case 6:
                                                    newNum = IntOffset(C.b, 1, 0, 0);
                                                    oldNum = PosToInt(x, y, z);

                                                    if (GetTile(IntOffset(newNum, 0, -1, 0)) == Block.air && GetTile(newNum) == Block.air)
                                                        newNum = IntOffset(newNum, 0, -1, 0);
                                                    else if (GetTile(newNum) == Block.air && GetTile(IntOffset(newNum, 0, 1, 0)) == Block.air) { }

                                                    else if (GetTile(IntOffset(newNum, 0, 2, 0)) == Block.air && GetTile(IntOffset(newNum, 0, 1, 0)) == Block.air)
                                                        newNum = IntOffset(newNum, 0, 1, 0);
                                                    //else skip = true;

                                                    if (AddUpdate(newNum, blocks[C.b]))
                                                    {
                                                        AddUpdate(IntOffset(oldNum, 0, 0, 0), Block.snaketail, true, "wait 5 revert " + Block.air.ToString());
                                                        goto removeSelf_Snake;
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 4) InnerChange = true; else goto case 7;
                                                    break;

                                                case 7:
                                                case 8:
                                                case 9:
                                                    newNum = IntOffset(C.b, 0, 0, 1);
                                                    oldNum = PosToInt(x, y, z);

                                                    if (GetTile(IntOffset(newNum, 0, -1, 0)) == Block.air && GetTile(newNum) == Block.air)
                                                        newNum = IntOffset(newNum, 0, -1, 0);
                                                    else if (GetTile(newNum) == Block.air && GetTile(IntOffset(newNum, 0, 1, 0)) == Block.air) { }

                                                    else if (GetTile(IntOffset(newNum, 0, 2, 0)) == Block.air && GetTile(IntOffset(newNum, 0, 1, 0)) == Block.air)
                                                        newNum = IntOffset(newNum, 0, 1, 0);
                                                    //else skip = true;

                                                    if (AddUpdate(newNum, blocks[C.b]))
                                                    {
                                                        AddUpdate(IntOffset(oldNum, 0, 0, 0), Block.snaketail, true, "wait 5 revert " + Block.air.ToString());
                                                        goto removeSelf_Snake;
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 4) InnerChange = true; else goto case 10;
                                                    break;
                                                case 10:
                                                case 11:
                                                case 12:
                                                default:
                                                    newNum = IntOffset(C.b, 0, 0, -1);
                                                    oldNum = PosToInt(x, y, z);

                                                    if (GetTile(IntOffset(newNum, 0, -1, 0)) == Block.air && GetTile(newNum) == Block.air)
                                                        newNum = IntOffset(newNum, 0, -1, 0);
                                                    else if (GetTile(newNum) == Block.air && GetTile(IntOffset(newNum, 0, 1, 0)) == Block.air) { }

                                                    else if (GetTile(IntOffset(newNum, 0, 2, 0)) == Block.air && GetTile(IntOffset(newNum, 0, 1, 0)) == Block.air)
                                                        newNum = IntOffset(newNum, 0, 1, 0);
                                                    //else skip = true;

                                                    if (AddUpdate(newNum, blocks[C.b]))
                                                    {
                                                        AddUpdate(IntOffset(oldNum, 0, 0, 0), Block.snaketail, true, "wait 5 revert " + Block.air.ToString());
                                                        goto removeSelf_Snake;
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 4) InnerChange = true; else goto case 1;
                                                    break;
                                            }
                                        }

                                    removeSelf_Snake:
                                        if (!InnerChange)
                                            AddUpdate(C.b, Block.air);
                                        break;

                                        #endregion

                                    case Block.birdred:
                                    case Block.birdblue:
                                    case Block.birdkill:
                                        #region HUNTER BIRDS
                                        if (ai)
                                            Player.players.ForEach(delegate(Player p)
                                            {
                                                if (p.level == this && !p.invincible && !p.hidden)
                                                {
                                                    currentNum = Math.Abs((p.pos[0] / 32) - x) + Math.Abs((p.pos[1] / 32) - y) + Math.Abs((p.pos[2] / 32) - z);
                                                    if (currentNum < foundNum)
                                                    {
                                                        foundNum = currentNum;
                                                        foundPlayer = p;
                                                    }
                                                }
                                            });

                                    randomMovement:
                                        if (foundPlayer != null && rand.Next(1, 20) < 19)
                                        {
                                            currentNum = rand.Next(1, 10);
                                            foundNum = 0;

                                            switch (currentNum)
                                            {
                                                case 1:
                                                case 2:
                                                case 3:
                                                    if ((foundPlayer.pos[0] / 32) - x != 0)
                                                    {
                                                        newNum = PosToInt((ushort)(x + Math.Sign((foundPlayer.pos[0] / 32) - x)), y, z);
                                                        if (GetTile(newNum) == Block.air)
                                                            if (AddUpdate(newNum, blocks[C.b]))
                                                                goto removeSelf;
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 4;
                                                case 4:
                                                case 5:
                                                case 6:
                                                    if ((foundPlayer.pos[1] / 32) - y != 0)
                                                    {
                                                        newNum = PosToInt(x, (ushort)(y + Math.Sign((foundPlayer.pos[1] / 32) - y)), z);
                                                        if (GetTile(newNum) == Block.air)
                                                            if (AddUpdate(newNum, blocks[C.b]))
                                                                goto removeSelf;
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 7;
                                                case 7:
                                                case 8:
                                                case 9:
                                                    if ((foundPlayer.pos[2] / 32) - z != 0)
                                                    {
                                                        newNum = PosToInt(x, y, (ushort)(z + Math.Sign((foundPlayer.pos[2] / 32) - z)));
                                                        if (GetTile(newNum) == Block.air)
                                                            if (AddUpdate(newNum, blocks[C.b]))
                                                                goto removeSelf;
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 1;
                                                default:
                                                    foundPlayer = null; goto randomMovement;
                                            }
                                        }
                                        else
                                        {
                                            switch (rand.Next(1, 15))
                                            {
                                                case 1:
                                                    if (GetTile(x, (ushort)(y - 1), z) == Block.air)
                                                        if (AddUpdate(PosToInt(x, (ushort)(y - 1), z), blocks[C.b])) break; else goto case 3;
                                                    else goto case 3;
                                                case 2:
                                                    if (GetTile(x, (ushort)(y + 1), z) == Block.air)
                                                        if (AddUpdate(PosToInt(x, (ushort)(y + 1), z), blocks[C.b])) break; else goto case 6;
                                                    else goto case 6;
                                                case 3:
                                                case 4:
                                                case 5:
                                                    if (GetTile((ushort)(x - 1), y, z) == Block.air)
                                                        if (AddUpdate(PosToInt((ushort)(x - 1), y, z), blocks[C.b])) break; else goto case 9;
                                                    else goto case 9;
                                                case 6:
                                                case 7:
                                                case 8:
                                                    if (GetTile((ushort)(x + 1), y, z) == Block.air)
                                                        if (AddUpdate(PosToInt((ushort)(x + 1), y, z), blocks[C.b])) break; else goto case 12;
                                                    else goto case 12;
                                                case 9:
                                                case 10:
                                                case 11:
                                                    if (GetTile(x, y, (ushort)(z - 1)) == Block.air)
                                                        if (AddUpdate(PosToInt(x, y, (ushort)(z - 1)), blocks[C.b])) break; else InnerChange = true;
                                                    else InnerChange = true;
                                                    break;
                                                case 12:
                                                case 13:
                                                case 14:
                                                default:
                                                    if (GetTile(x, y, (ushort)(z + 1)) == Block.air)
                                                        if (AddUpdate(PosToInt(x, y, (ushort)(z + 1)), blocks[C.b])) break; else InnerChange = true;
                                                    else InnerChange = true;
                                                    break;
                                            }
                                        }

                                    removeSelf:
                                        if (!InnerChange)
                                            AddUpdate(C.b, Block.air);
                                        break;
                                        #endregion

                                    case Block.fishbetta:
                                    case Block.fishgold:
                                    case Block.fishsalmon:
                                    case Block.fishshark:
                                    case Block.fishsponge:
                                        #region FISH
                                        if (ai)
                                            Player.players.ForEach(delegate(Player p)
                                            {
                                                if (p.level == this && !p.invincible)
                                                {
                                                    currentNum = Math.Abs((p.pos[0] / 32) - x) + Math.Abs((p.pos[1] / 32) - y) + Math.Abs((p.pos[2] / 32) - z);
                                                    if (currentNum < foundNum)
                                                    {
                                                        foundNum = currentNum;
                                                        foundPlayer = p;
                                                    }
                                                }
                                            });
                                        ushort curWater = Block.water;

                                    randomMovement_fish:
                                        if (foundPlayer != null && rand.Next(1, 20) < 19)
                                        {
                                            currentNum = rand.Next(1, 10);
                                            foundNum = 0;

                                            switch (currentNum)
                                            {
                                                case 1:
                                                case 2:
                                                case 3:
                                                    if ((foundPlayer.pos[0] / 32) - x != 0)
                                                    {
                                                        if (blocks[C.b] == Block.fishbetta || blocks[C.b] == Block.fishshark)
                                                            newNum = PosToInt((ushort)(x + Math.Sign((foundPlayer.pos[0] / 32) - x)), y, z);
                                                        else
                                                            newNum = PosToInt((ushort)(x - Math.Sign((foundPlayer.pos[0] / 32) - x)), y, z);


                                                        if (GetTile(newNum) == Block.water || GetTile(newNum) == Block.waterstill)
                                                        {
                                                            curWater = GetTile(newNum);
                                                            if (AddUpdate(newNum, blocks[C.b]))
                                                                goto removeSelf_fish;
                                                        }
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 4;
                                                case 4:
                                                case 5:
                                                case 6:
                                                    if ((foundPlayer.pos[1] / 32) - y != 0)
                                                    {
                                                        if (blocks[C.b] == Block.fishbetta || blocks[C.b] == Block.fishshark)
                                                            newNum = PosToInt(x, (ushort)(y + Math.Sign((foundPlayer.pos[1] / 32) - y)), z);
                                                        else
                                                            newNum = PosToInt(x, (ushort)(y - Math.Sign((foundPlayer.pos[1] / 32) - y)), z);

                                                        if (GetTile(newNum) == Block.water || GetTile(newNum) == Block.waterstill)
                                                        {
                                                            curWater = GetTile(newNum);
                                                            if (AddUpdate(newNum, blocks[C.b]))
                                                                goto removeSelf_fish;
                                                        }
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 7;
                                                case 7:
                                                case 8:
                                                case 9:
                                                    if ((foundPlayer.pos[2] / 32) - z != 0)
                                                    {
                                                        if (blocks[C.b] == Block.fishbetta || blocks[C.b] == Block.fishshark)
                                                            newNum = PosToInt(x, y, (ushort)(z + Math.Sign((foundPlayer.pos[2] / 32) - z)));
                                                        else
                                                            newNum = PosToInt(x, y, (ushort)(z - Math.Sign((foundPlayer.pos[2] / 32) - z)));

                                                        if (GetTile(newNum) == Block.water || GetTile(newNum) == Block.waterstill)
                                                        {
                                                            curWater = GetTile(newNum);
                                                            if (AddUpdate(newNum, blocks[C.b]))
                                                                goto removeSelf_fish;
                                                        }
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 1;
                                                default:
                                                    foundPlayer = null; goto randomMovement_fish;
                                            }
                                        }
                                        else
                                        {
                                            switch (rand.Next(1, 15))
                                            {
                                                case 1:
                                                    if (GetTile(x, (ushort)(y - 1), z) == Block.water || GetTile(x, (ushort)(y - 1), z) == Block.waterstill)
                                                    {
                                                        curWater = GetTile(x, (ushort)(y - 1), z);
                                                        if (AddUpdate(PosToInt(x, (ushort)(y - 1), z), blocks[C.b])) break; else goto case 3;
                                                    }
                                                    else goto case 3;
                                                case 2:
                                                    if (GetTile(x, (ushort)(y + 1), z) == Block.water || GetTile(x, (ushort)(y + 1), z) == Block.waterstill)
                                                    {
                                                        curWater = GetTile(x, (ushort)(y + 1), z);
                                                        if (AddUpdate(PosToInt(x, (ushort)(y + 1), z), blocks[C.b])) break; else goto case 6;
                                                    }
                                                    else goto case 6;
                                                case 3:
                                                case 4:
                                                case 5:
                                                    if (GetTile((ushort)(x - 1), y, z) == Block.water || GetTile((ushort)(x - 1), y, z) == Block.waterstill)
                                                    {
                                                        curWater = GetTile((ushort)(x - 1), y, z);
                                                        if (AddUpdate(PosToInt((ushort)(x - 1), y, z), blocks[C.b])) break; else goto case 9;
                                                    }
                                                    else goto case 9;
                                                case 6:
                                                case 7:
                                                case 8:
                                                    if (GetTile((ushort)(x + 1), y, z) == Block.water || GetTile((ushort)(x + 1), y, z) == Block.waterstill)
                                                    {
                                                        curWater = GetTile((ushort)(x + 1), y, z);
                                                        if (AddUpdate(PosToInt((ushort)(x + 1), y, z), blocks[C.b])) break; else goto case 12;
                                                    }
                                                    else goto case 12;
                                                case 9:
                                                case 10:
                                                case 11:
                                                    if (GetTile(x, y, (ushort)(z - 1)) == Block.water || GetTile(x, (ushort)(y - 1), z) == Block.waterstill)
                                                    {
                                                        curWater = GetTile(x, y, (ushort)(z - 1));
                                                        if (AddUpdate(PosToInt(x, y, (ushort)(z - 1)), blocks[C.b])) break; else InnerChange = true;
                                                    }
                                                    else InnerChange = true;
                                                    break;
                                                case 12:
                                                case 13:
                                                case 14:
                                                default:
                                                    if (GetTile(x, y, (ushort)(z + 1)) == Block.water || GetTile(x, (ushort)(y - 1), z) == Block.waterstill)
                                                    {
                                                        curWater = GetTile(x, y, (ushort)(z + 1));
                                                        if (AddUpdate(PosToInt(x, y, (ushort)(z + 1)), blocks[C.b])) break; else InnerChange = true;
                                                    }
                                                    else InnerChange = true;
                                                    break;
                                            }
                                        }

                                    removeSelf_fish:
                                        if (!InnerChange)
                                            AddUpdate(C.b, curWater);
                                        break;
                                        #endregion
                                    case Block.fishlavashark:
                                        #region lavafish
                                        if (ai)
                                            Player.players.ForEach(delegate(Player p)
                                            {
                                                if (p.level == this && !p.invincible)
                                                {
                                                    currentNum = Math.Abs((p.pos[0] / 32) - x) + Math.Abs((p.pos[1] / 32) - y) + Math.Abs((p.pos[2] / 32) - z);
                                                    if (currentNum < foundNum)
                                                    {
                                                        foundNum = currentNum;
                                                        foundPlayer = p;
                                                    }
                                                }
                                            });

                                    randomMovement_lavafish:
                                        if (foundPlayer != null && rand.Next(1, 20) < 19)
                                        {
                                            currentNum = rand.Next(1, 10);
                                            foundNum = 0;

                                            switch (currentNum)
                                            {
                                                case 1:
                                                case 2:
                                                case 3:
                                                    if ((foundPlayer.pos[0] / 32) - x != 0)
                                                    {
                                                        if (blocks[C.b] == Block.fishlavashark)
                                                            newNum = PosToInt((ushort)(x + Math.Sign((foundPlayer.pos[0] / 32) - x)), y, z);
                                                        else
                                                            newNum = PosToInt((ushort)(x - Math.Sign((foundPlayer.pos[0] / 32) - x)), y, z);


                                                        if (GetTile(newNum) == Block.lava)
                                                            if (AddUpdate(newNum, blocks[C.b]))
                                                                goto removeSelf_lavafish;
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 4;
                                                case 4:
                                                case 5:
                                                case 6:
                                                    if ((foundPlayer.pos[1] / 32) - y != 0)
                                                    {
                                                        if (blocks[C.b] == Block.fishlavashark)
                                                            newNum = PosToInt(x, (ushort)(y + Math.Sign((foundPlayer.pos[1] / 32) - y)), z);
                                                        else
                                                            newNum = PosToInt(x, (ushort)(y - Math.Sign((foundPlayer.pos[1] / 32) - y)), z);

                                                        if (GetTile(newNum) == Block.lava)
                                                            if (AddUpdate(newNum, blocks[C.b]))
                                                                goto removeSelf_lavafish;
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 7;
                                                case 7:
                                                case 8:
                                                case 9:
                                                    if ((foundPlayer.pos[2] / 32) - z != 0)
                                                    {
                                                        if (blocks[C.b] == Block.fishlavashark)
                                                            newNum = PosToInt(x, y, (ushort)(z + Math.Sign((foundPlayer.pos[2] / 32) - z)));
                                                        else
                                                            newNum = PosToInt(x, y, (ushort)(z - Math.Sign((foundPlayer.pos[2] / 32) - z)));

                                                        if (GetTile(newNum) == Block.lava)
                                                            if (AddUpdate(newNum, blocks[C.b]))
                                                                goto removeSelf_lavafish;
                                                    }

                                                    foundNum++;
                                                    if (foundNum >= 3) goto default; else goto case 1;
                                                default:
                                                    foundPlayer = null; goto randomMovement_lavafish;
                                            }
                                        }
                                        else
                                        {
                                            switch (rand.Next(1, 15))
                                            {
                                                case 1:
                                                    if (GetTile(x, (ushort)(y - 1), z) == Block.lava)
                                                        if (AddUpdate(PosToInt(x, (ushort)(y - 1), z), blocks[C.b])) break; else goto case 3;
                                                    else goto case 3;
                                                case 2:
                                                    if (GetTile(x, (ushort)(y + 1), z) == Block.lava)
                                                        if (AddUpdate(PosToInt(x, (ushort)(y + 1), z), blocks[C.b])) break; else goto case 6;
                                                    else goto case 6;
                                                case 3:
                                                case 4:
                                                case 5:
                                                    if (GetTile((ushort)(x - 1), y, z) == Block.lava)
                                                        if (AddUpdate(PosToInt((ushort)(x - 1), y, z), blocks[C.b])) break; else goto case 9;
                                                    else goto case 9;
                                                case 6:
                                                case 7:
                                                case 8:
                                                    if (GetTile((ushort)(x + 1), y, z) == Block.lava)
                                                        if (AddUpdate(PosToInt((ushort)(x + 1), y, z), blocks[C.b])) break; else goto case 12;
                                                    else goto case 12;
                                                case 9:
                                                case 10:
                                                case 11:
                                                    if (GetTile(x, y, (ushort)(z - 1)) == Block.lava)
                                                        if (AddUpdate(PosToInt(x, y, (ushort)(z - 1)), blocks[C.b])) break; else InnerChange = true;
                                                    else InnerChange = true;
                                                    break;
                                                case 12:
                                                case 13:
                                                case 14:
                                                default:
                                                    if (GetTile(x, y, (ushort)(z + 1)) == Block.lava)
                                                        if (AddUpdate(PosToInt(x, y, (ushort)(z + 1)), blocks[C.b])) break; else InnerChange = true;
                                                    else InnerChange = true;
                                                    break;
                                            }
                                        }

                                    removeSelf_lavafish:
                                        if (!InnerChange)
                                            AddUpdate(C.b, Block.lava);
                                        break;

                                        #endregion


                                    case Block.rockethead:
                                        if (rand.Next(1, 10) <= 5) mx = 1; else mx = -1;
                                        if (rand.Next(1, 10) <= 5) my = 1; else my = -1;
                                        if (rand.Next(1, 10) <= 5) mz = 1; else mz = -1;

                                        for (int cx = -mx; cx != mx * 2 && !InnerChange; cx = cx + mx)
                                            for (int cy = -my; cy != my * 2 && !InnerChange; cy = cy + my)
                                                for (int cz = -mz; cz != mz * 2 && !InnerChange; cz = cz + mz)
                                                {
                                                    if (GetTile((ushort)(x + cx), (ushort)(y + cy), (ushort)(z + cz)) == Block.fire)
                                                    {
                                                        if (GetTile((ushort)(x - cx), (ushort)(y - cy), (ushort)(z - cz)) == Block.air || GetTile((ushort)(x - cx), (ushort)(y - cy), (ushort)(z - cz)) == Block.rocketstart)
                                                        {
                                                            PhysBlockchange(x, y, z, Block.fire);
                                                            AddUpdate(PosToInt((ushort)(x - cx), (ushort)(y - cy), (ushort)(z - cz)), Block.rockethead);
                                                        }
                                                        else if (GetTile((ushort)(x - cx), (ushort)(y - cy), (ushort)(z - cz)) == Block.fire) { }
                                                        else
                                                        {
                                                            if (physics >= 2) MakeExplosion(x, y, z, 2, 2);
                                                            else AddUpdate(PosToInt(x, y, z), Block.fire);
                                                        }
                                                        InnerChange = true;
                                                    }
                                                }
                                        break;
                                    default:    //non-special blocks are then ignored, maybe it would be better to avoid getting here and cutting down the list
                                        if (!C.extraInfo.Contains("wait")) C.time = 255;
                                        break;
                                }
                            }
                        }
                        catch
                        {
                            ListCheck.Remove(C);
                            //s.Log("Phys check issue");
                        }

                    });

                    ListCheck.RemoveAll(Check => Check.time == 255);  //Remove all that are finished with 255 time

                    lastUpdate = ListUpdate.Count;
                    ListUpdate.ForEach(delegate(Update C)
                    {
                        try
                        {
                            IntToPos(C.b, out x, out y, out z);
                            PhysBlockchange(x, y, z, C.type, false, C.extraInfo);
                        }
                        catch
                        {
                            Server.s.Log("Phys update issue");
                        }
                    });

                    ListUpdate.Clear();
                }
            }
            catch
            {
                Server.s.Log("Level physics error");
            }
        }
        public void AddCheck(int b, string extraInfo = "", bool overRide = false)
        {
            try
            {
                if (!ListCheck.Exists(Check => Check.b == b))
                {
                    ListCheck.Add(new Check(b, extraInfo));    //Adds block to list to be updated
                }
                else
                {
                    if (overRide)
                    {
                        foreach (Check C2 in ListCheck)
                        {
                            if (C2.b == b)
                            {
                                C2.extraInfo = extraInfo;
                                return;
                            }
                        }
                    }
                }
            }
            catch
            {
                //s.Log("Warning-PhysicsCheck");
                //ListCheck.Add(new Check(b));    //Lousy back up plan
            }
        }
        private bool AddUpdate(int b, ushort type, bool overRide = false, string extraInfo = "")
        {
            try
            {
                if (overRide == true)
                {
                    ushort x, y, z;
                    IntToPos(b, out x, out y, out z);
                    AddCheck(b, extraInfo); PhysBlockchange(x, y, z, type, true);
                    return true;
                }

                if (!ListUpdate.Exists(Update => Update.b == b))
                {
                    ListUpdate.Add(new Update(b, type, extraInfo));
                    return true;
                }
                else if (type == Block.sand || type == Block.gravel)
                {
                    ListUpdate.RemoveAll(Update => Update.b == b);
                    ListUpdate.Add(new Update(b, type, extraInfo));
                    return true;
                }

                return false;
            }
            catch
            {
                //s.Log("Warning-PhysicsUpdate");
                //ListUpdate.Add(new Update(b, (byte)type));    //Lousy back up plan
                return false;
            }
        }
        public void ClearPhysics()
        {
            ushort x, y, z;
            ListCheck.ForEach(delegate(Check C)    //checks though each block
            {
                IntToPos(C.b, out x, out y, out z);
                //attemps on shutdown to change blocks back into normal selves that are active, hopefully without needing to send into to clients.
                switch (blocks[C.b])
                {
                    case Block.air_flood:
                    case Block.air_flood_down:
                    case Block.air_flood_up:
                        blocks[C.b] = 0;
                        break;
                    case Block.air_flood_layer:
                        //blocks[C.b] = 111;
                        PhysBlockchange(x, y, z, Block.lava_fast);
                        break;
                }
            });

            ListCheck.Clear();
            ListUpdate.Clear();
        }
        //================================================================================================================
        private void PhysWater(int b, ushort type)
        {
            if (b == -1) { return; }
            switch (blocks[b])
            {
                case 0:
                    if (!PhysSpongeCheck(b))
                    {
                        AddUpdate(b, type);
                    }
                    break;

                case 10:    //hit active_lava
                case 112:    //hit lava_fast
                    if (!PhysSpongeCheck(b)) { AddUpdate(b, 1); }
                    break;

                case 6:
                case 37:
                case 38:
                case 39:
                case 40:
                    if (physics > 1)   //Adv physics kills flowers and mushrooms in water
                    {
                        if (!PhysSpongeCheck(b)) { AddUpdate(b, 0); }
                    }
                    break;

                case 12:    //sand
                case 13:    //gravel
                case 110:   //woodfloat
                    AddCheck(b);
                    break;

                default:
                    break;
            }
        }
        //================================================================================================================
        private void PhysLava(int b, ushort type)
        {
            if (b == -1) { return; }
            switch (blocks[b])
            {
                case 0:
                    AddUpdate(b, type);
                    break;

                case 8:    //hit active_water
                    AddUpdate(b, 1);
                    break;

                case 12:    //sand
                    if (physics == 2)   //Adv physics changes sand to glass next to lava
                    {
                        AddUpdate(b, 20);
                    }
                    else
                    {
                        AddCheck(b);
                    }
                    break;

                case 13:    //gravel
                    AddCheck(b);
                    break;

                case 5:
                case 6:
                case 17:
                case 18:
                case 37:
                case 38:
                case 39:
                case 40:
                    if (physics == 2)   //Adv physics kills flowers and mushrooms plus wood in lava
                    {
                        AddUpdate(b, 0);
                    }
                    break;

                default:
                    break;
            }
        }
        //================================================================================================================
        private void PhysAir(int b)
        {
            if (b == -1) { return; }
            switch (blocks[b])
            {
                case 8:     //active water
                case 10:    //active_lava
                case 12:    //sand
                case 13:    //gravel
                case 110:   //wood_float
                case 112:   //lava_fast
                    AddCheck(b);
                    break;

                default:
                    break;
            }
        }
        //================================================================================================================
        private bool PhysSand(int b, ushort type)   //also does gravel
        {
            if (b == -1) { return false; }
            int tempb = b;
            bool blocked = false;
            bool moved = false;

            do
            {
                tempb = IntOffset(tempb, 0, -1, 0);     //Get block below each loop
                if (GetTile(tempb) != Block.Zero)
                {
                    switch (blocks[tempb])
                    {
                        case 0:         //air lava water
                        case 8:
                        case 10:
                            moved = true;
                            break;

                        case 6:
                        case 37:
                        case 38:
                        case 39:
                        case 40:
                            if (physics == 2)   //Adv physics crushes plants with sand
                            { moved = true; }
                            else
                            { blocked = true; }
                            break;

                        default:
                            blocked = true;
                            break;
                    }
                    if (physics == 2) { blocked = true; }
                }
                else
                { blocked = true; }
            }
            while (!blocked);

            if (moved)
            {
                AddUpdate(b, 0);
                if (physics == 2)
                { AddUpdate(tempb, type); }
                else
                { AddUpdate(IntOffset(tempb, 0, 1, 0), type); }
            }

            return moved;
        }

        private void PhysSandCheck(int b)   //also does gravel
        {
            if (b == -1) { return; }
            switch (blocks[b])
            {
                case 12:    //sand
                case 13:    //gravel
                case 110:   //wood_float
                    AddCheck(b);
                    break;

                default:
                    break;
            }
        }
        //================================================================================================================
        private void PhysStair(int  b)
        {
            int tempb = IntOffset(b, 0, -1, 0);     //Get block below
            if (GetTile(tempb) != Block.Zero)
            {
                if (GetTile(tempb) == Block.staircasestep)
                {
                    AddUpdate(b, 0);
                    AddUpdate(tempb, 43);
                }
            }
        }
        //================================================================================================================
        private bool PhysSpongeCheck(int b)         //return true if sponge is near
        {
            int temp = 0;
            for (int x = -2; x <= +2; ++x)
            {
                for (int y = -2; y <= +2; ++y)
                {
                    for (int z = -2; z <= +2; ++z)
                    {
                        temp = IntOffset(b, x, y, z);
                        if (GetTile(temp) != Block.Zero)
                        {
                            if (GetTile(temp) == 19) { return true; }
                        }
                    }
                }
            }
            return false;
        }
        //================================================================================================================
        private void PhysSponge(int b)         //turn near water into air when placed
        {
            int temp = 0;
            for (int x = -2; x <= +2; ++x)
            {
                for (int y = -2; y <= +2; ++y)
                {
                    for (int z = -2; z <= +2; ++z)
                    {
                        temp = IntOffset(b, x, y, z);
                        if (GetTile(temp) != Block.Zero)
                        {
                            if (GetTile(temp) == 8) { AddUpdate(temp, 0); }
                        }
                    }
                }
            }

        }
        //================================================================================================================
        public void PhysSpongeRemoved(int b)         //Reactivates near water
        {
            //TODO Calc only edge
            int temp = 0;
            for (int x = -3; x <= +3; ++x)
            {
                for (int y = -3; y <= +3; ++y)
                {
                    for (int z = -3; z <= +3; ++z)
                    {
                        temp = IntOffset(b, x, y, z);
                        if (GetTile(temp) != Block.Zero)
                        {
                            if (GetTile(temp) == 8) { AddCheck(temp); }
                        }
                    }
                }
            }

        }
        //================================================================================================================
        private void PhysFloatwood(int b)
        {
            int tempb = IntOffset(b, 0, -1, 0);     //Get block below
            if (GetTile(tempb) != Block.Zero)
            {
                if (GetTile(tempb) == 0)
                {
                    AddUpdate(b, 0);
                    AddUpdate(tempb, 110);
                    return;
                }
            }

            tempb = IntOffset(b, 0, 1, 0);     //Get block above
            if (GetTile(tempb) != Block.Zero)
            {
                if (GetTile(tempb) == 8)
                {
                    AddUpdate(b, 8);
                    AddUpdate(tempb, 110);
                    return;
                }
            }
        }
        //================================================================================================================
        private void PhysAirFlood(int b, ushort type)
        {
            if (b == -1) { return; }
            switch (blocks[b])
            {
                case 8:
                case 10:
                case 112:   //lava_fast
                    AddUpdate(b, type);
                    break;

                default:
                    break;
            }
        }
        //================================================================================================================
        private void PhysReplace(int b, ushort typeA, ushort typeB)     //replace any typeA with typeB
        {
            if (b == -1) { return; }
            if (blocks[b] == typeA)
            {
                AddUpdate(b, typeB);
            }
        }
        #endregion

        //================================================================================================================

        public void AnyDoor(Check C, ushort x, ushort y, ushort z, int timer, bool instaUpdate = false)
        {
            if (C.time == 0)
            {
                try { PhysDoor((ushort)(x + 1), y, z, instaUpdate); }
                catch { }
                try { PhysDoor((ushort)(x - 1), y, z, instaUpdate); }
                catch { }
                try { PhysDoor(x, y, (ushort)(z + 1), instaUpdate); }
                catch { }
                try { PhysDoor(x, y, (ushort)(z - 1), instaUpdate); }
                catch { }
                try { PhysDoor(x, (ushort)(y - 1), z, instaUpdate); }
                catch { }
                try { PhysDoor(x, (ushort)(y + 1), z, instaUpdate); }
                catch { }

                try
                {
                    if (blocks[C.b] == Block.door8_air)
                    {
                        for (int xx = -1; xx <= 1; xx++)
                        {
                            for (int yy = -1; yy <= 1; yy++)
                            {
                                for (int zz = -1; zz <= 1; zz++)
                                {
                                    ushort b = GetTile(IntOffset(C.b, xx, yy, zz));
                                    if (b == Block.rocketstart)
                                    {
                                        AddUpdate(IntOffset(C.b, xx * 3, yy * 3, zz * 3), Block.rockethead);
                                        AddUpdate(IntOffset(C.b, xx * 2, yy * 2, zz * 2), Block.fire);
                                    }
                                    else if (b == Block.firework)
                                    {
                                        AddUpdate(IntOffset(C.b, xx, yy + 1, zz), Block.lavastill, false, "dissipate 100");
                                        AddUpdate(IntOffset(C.b, xx, yy + 2, zz), Block.firework);
                                    }
                                    else if (b == Block.tnt)
                                    {
                                        MakeExplosion((ushort)(x + xx), (ushort)(y + yy), (ushort)(z + zz), 0, 1);
                                    }
                                }
                            }
                        }
                    }
                }
                catch { }
            }
            if (C.time < timer) C.time++;
            else
            {
                AddUpdate(C.b, Block.SaveConvert(blocks[C.b]));    //turn back into door
                C.time = 255;
            }
        }

        public void PhysDoor(ushort x, ushort y, ushort z, bool instaUpdate)
        {
            int foundInt = PosToInt(x, y, z);
            ushort FoundAir = Block.DoorAirs(blocks[foundInt]);

            if (FoundAir != 0)
            {
                if (!instaUpdate) AddUpdate(foundInt, FoundAir);
                else PhysBlockchange(x, y, z, FoundAir);
                return;
            }
        }

        public void MakeExplosion(ushort x, ushort y, ushort z, int size, int allowedfrom)
        {
            int xx, yy, zz; Random rand = new Random(); ushort b;

            if (physics < allowedfrom) return;
            AddUpdate(PosToInt(x, y, z), Block.tntexplosion, true);

            for (xx = (x - (size + 1)); xx <= (x + (size + 1)); ++xx)
                for (yy = (y - (size + 1)); yy <= (y + (size + 1)); ++yy)
                    for (zz = (z - (size + 1)); zz <= (z + (size + 1)); ++zz)
                        try
                        {
                            b = GetTile((ushort)xx, (ushort)yy, (ushort)zz);
                            if (b == Block.tnt)
                            {
                                AddUpdate(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), Block.smalltnt);
                            }
                            else if (b != Block.smalltnt && b != Block.bigtnt)
                            {
                                if (rand.Next(1, 11) <= 4 && GetTile((ushort)xx, (ushort)yy, (ushort)zz) != Block.blackrock) AddUpdate(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), Block.tntexplosion);
                                else if (rand.Next(1, 11) <= 8 && GetTile((ushort)xx, (ushort)yy, (ushort)zz) != Block.blackrock) AddUpdate(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), Block.air);
                                else AddCheck(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), "drop 50 dissipate 8");
                            }
                            else
                            {
                                AddCheck(PosToInt((ushort)xx, (ushort)yy, (ushort)zz));
                            }
                        }
                        catch { }

            for (xx = (x - (size + 2)); xx <= (x + (size + 2)); ++xx)
                for (yy = (y - (size + 2)); yy <= (y + (size + 2)); ++yy)
                    for (zz = (z - (size + 2)); zz <= (z + (size + 2)); ++zz)
                    {
                        b = GetTile((ushort)xx, (ushort)yy, (ushort)zz);
                        if (rand.Next(1, 10) < 7)
                            if (Block.Convert(b) != Block.tnt)
                            {
                                if (rand.Next(1, 11) <= 4) AddUpdate(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), Block.tntexplosion);
                                else if (rand.Next(1, 11) <= 8) AddUpdate(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), Block.air);
                                else AddCheck(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), "drop 50 dissipate 8");
                            }
                        if (b == Block.tnt)
                        {
                            AddUpdate(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), Block.smalltnt);
                        }
                        else if (b == Block.smalltnt || b == Block.bigtnt)
                        {
                            AddCheck(PosToInt((ushort)xx, (ushort)yy, (ushort)zz));
                        }
                    }

            for (xx = (x - (size + 3)); xx <= (x + (size + 3)); ++xx)
                for (yy = (y - (size + 3)); yy <= (y + (size + 3)); ++yy)
                    for (zz = (z - (size + 3)); zz <= (z + (size + 3)); ++zz)
                    {
                        b = GetTile((ushort)xx, (ushort)yy, (ushort)zz);
                        if (rand.Next(1, 10) < 3)
                            if (Block.Convert(b) != Block.tnt)
                            {
                                if (rand.Next(1, 11) <= 4) AddUpdate(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), Block.tntexplosion);
                                else if (rand.Next(1, 11) <= 8) AddUpdate(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), Block.air);
                                else AddCheck(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), "drop 50 dissipate 8");
                            }
                        if (b == Block.tnt)
                        {
                            AddUpdate(PosToInt((ushort)xx, (ushort)yy, (ushort)zz), Block.smalltnt);
                        }
                        else if (b == Block.smalltnt || b == Block.bigtnt)
                        {
                            AddCheck(PosToInt((ushort)xx, (ushort)yy, (ushort)zz));
                        }
                    }
        }

        public void Firework(ushort x, ushort y, ushort z, int size)
        {
            ushort xx, yy, zz; Random rand = new Random(); int storedRand1, storedRand2;

            if (physics == 0) return;
            storedRand1 = rand.Next(21, 36);
            storedRand2 = rand.Next(21, 36);
            AddUpdate(PosToInt(x, y, z), Block.air, true);

            for (xx = (ushort)(x - (size + 1)); xx <= (ushort)(x + (size + 1)); ++xx)
                for (yy = (ushort)(y - (size + 1)); yy <= (ushort)(y + (size + 1)); ++yy)
                    for (zz = (ushort)(z - (size + 1)); zz <= (ushort)(z + (size + 1)); ++zz)
                        if (GetTile(xx, yy, zz) == Block.air)
                            if (rand.Next(1, 40) < 2)
                                AddUpdate(PosToInt(xx, yy, zz), (byte)rand.Next(Math.Min(storedRand1, storedRand2), Math.Max(storedRand1, storedRand2)), false, "drop 100 dissipate 50");

        }
    }

    //------------------------------------------------------------------------------------------------
    public class Check
    {
        public int b;
        public byte time;
        public string extraInfo = "";
        public Check(int b, string extraInfo = "")
        {
            this.b = b;
            time = 0;
            this.extraInfo = extraInfo;
        }
    }
    //------------------------------------------------------------------------------------------------
    public class Update
    {
        public int b;
        public ushort type;
        public string extraInfo = "";
        public Update(int b, ushort type, string extraInfo = "")
        {
            this.b = b;
            this.type = type;
            this.extraInfo = extraInfo;
        }
    }
    //------------------------------------------------------------------------------------------------
    public static class LevelPermission
    {
        public static int Banned = -20;
        public static int Guest = 1;
        public static int Builder = 20;
        public static int AdvBuilder = 40;
        public static int Operator = 80;
        public static int Admin = 90;
        public static int Owner = 100;

        public static int NoOne = 150;
        public static int Null = 255;

        public static int FromName(string name)
        {
            List<Group> tempList = new List<Group>();
            tempList.AddRange(Group.GroupList);
            Group tempGrp = null; bool returnNull = false;

            foreach (Group g in tempList)
            {
                if (g.name.ToLower() == name.ToLower()) return g.Permission;
                if (g.name.ToLower().IndexOf(name.ToLower()) != -1)
                {
                    if (tempGrp == null) tempGrp = g;
                    else returnNull = true;
                }
            }

            if (returnNull) return LevelPermission.Null;
            if (tempGrp != null) return tempGrp.Permission;
            return LevelPermission.Null;
        }

        public static string ToName(int perm)
        {
            foreach (Group g in Group.GroupList)
                if (perm == g.Permission)
                    return g.name;

            return "Null";
        }
    }
}