﻿
namespace MCRevive
{
    class AchRankOperator : Achievement
    {
        public override string name { get { return "Operator Power"; } }
        public override string type { get { return "rank"; } }
        public AchRankOperator() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.group.Permission >= LevelPermission.Operator; }
        public override string[] Info(Player p, bool earned)
        {
            return new string[] {
                "Get a &aPermission level of 80 or higher" + Server.DefaultColor + ".",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "To earn this achievement, you need to be a nice person, talk to people, and improve your skills at the game." + (char)0 + 
                "(unless you are playing on a bad server)"
            };
        }
    }
}
