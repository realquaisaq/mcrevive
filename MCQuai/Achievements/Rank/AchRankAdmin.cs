﻿
namespace MCRevive
{
    class AchRankAdmin : Achievement
    {
        public override string name { get { return "Admin Power"; } }
        public override string type { get { return "rank"; } }
        public AchRankAdmin() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.group.Permission >= LevelPermission.Admin; }
        public override string[] Info(Player p, bool earned)
        {
            return new string[] {
                "Get a &aPermission level of 90 or higher" + Server.DefaultColor + ".",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "To earn this achievement, you need to be a nice person, talk to people, and improve your skills at the game." + (char)0 + 
                "(unless you are playing on a bad server)"
            };
        }
    }
}
