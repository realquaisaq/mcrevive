﻿
namespace MCRevive
{
    class AchDemoted : Achievement
    {
        public override string name { get { return "Demoted"; } }
        public override string type { get { return "rank"; } }
        public AchDemoted() { }
        public override void Load() { }
        public override bool Check(Player p) { return false; } //this is handled in CmdSetrank
        public override string[] Info(Player p, bool earned)
        {
            return new string[] {
                "&aGet demoted" + Server.DefaultColor + " for the first time.",
                "You have &4earned " + Server.DefaultColor + "this achievement!",
                "You have &anot earned " + Server.DefaultColor + "this achievement!"
            };
        }
    }
}
