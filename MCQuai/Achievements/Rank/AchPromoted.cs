﻿
namespace MCRevive
{
    class AchPromoted : Achievement
    {
        public override string name { get { return "Promoted"; } }
        public override string type { get { return "rank"; } }
        public AchPromoted() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.group.Permission > LevelPermission.Guest; }
        public override string[] Info(Player p, bool earned)
        {
            return new string[] {
                "&aGet promoted" + Server.DefaultColor + " for the first time.",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You still haven't been promoted? Well thats awkward..."
            };
        }
    }
}
