﻿
namespace MCRevive
{
    class AchGriefer : Achievement
    {
        public override string name { get { return "Griefer"; } }
        public override string type { get { return "build"; } }
        public AchGriefer() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.blocksGriefed >= 10000; }
        public override string[] Info(Player p, bool earned)
        {
            long i = 10000 - p.blocksGriefed;
            return new string[] {
                "&aBreak 10000 " + Server.DefaultColor + "blocks.",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to break &4" + i + " " + Server.DefaultColor + " block" + (i != 1 ? "s" : "") + " more to earn this achievement."
            };
        }
    }
}
