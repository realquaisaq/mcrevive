﻿
namespace MCRevive
{
    class AchEcoFriend : Achievement
    {
        public override string name { get { return "Eco Friend"; } }
        public override string type { get { return "build"; } }
        public AchEcoFriend() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.builtEco >= 10000; }
        public override string[] Info(Player p, bool earned)
        {
            long i = 10000 - p.builtEco;
            return new string[] {
                "&aCreate 10000 " + Server.DefaultColor + "blocks of grass and/or dirt.",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to &4create " + i + Server.DefaultColor + " more grass and/or dirt block" + (i != 1 ? "s" : "") + ", to earn this achievement."
            };
        }
    }
}
