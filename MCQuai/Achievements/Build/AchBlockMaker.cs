﻿
namespace MCRevive
{
    class AchBlockMaker : Achievement
    {
        public override string name { get { return "Block Maker"; } }
        public override string type { get { return "build"; } }
        public AchBlockMaker() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.blocksBuild >= 10000; }
        public override string[] Info(Player p, bool earned)
        {
            long i = 10000 - p.blocksBuild;
            return new string[] {
                "&aPlace 10000" + Server.DefaultColor + " blocks.",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to build &4" + i + " " + Server.DefaultColor + "block" + (i != 1 ? "s" : "") + " more to earn this achievement."
            };
        }
    }
}
