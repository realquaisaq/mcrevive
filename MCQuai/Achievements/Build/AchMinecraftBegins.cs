﻿
namespace MCRevive
{
    class AchMinecraftBegins : Achievement
    {
        public override string name { get { return "Minecraft Begins"; } }
        public override string type { get { return "build"; } }
        public AchMinecraftBegins() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.blocksBuild > 0; }
        public override string[] Info(Player p, bool earned)
        {
            return new string[] {
                "&aPlace your first block" + Server.DefaultColor + ".",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to place &41 " + Server.DefaultColor + "blocks more to earn this achievement."
            };
        }
    }
}
