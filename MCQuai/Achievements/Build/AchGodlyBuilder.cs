﻿
namespace MCRevive
{
    class AchGodlyBuilder : Achievement
    {
        public override string name { get { return "Godly Builder"; } }
        public override string type { get { return "build"; } }
        public AchGodlyBuilder() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.totalBlocks >= 100000000000; }
        public override string[] Info(Player p, bool earned)
        {
            long i = 10000000 - p.blocksBuild;
            return new string[] {
                "&aModify 100000000000 " + Server.DefaultColor + "blocks.",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to modify &4" + i + " " + Server.DefaultColor + " block" + (i != 1 ? "s" : "") + " more to earn this achievement."
            };
        }
    }
}
