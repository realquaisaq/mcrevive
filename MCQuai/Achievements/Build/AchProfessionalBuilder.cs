﻿
namespace MCRevive
{
    class AchProfessionalBuilder : Achievement
    {
        public override string name { get { return "Professional Builder"; } }
        public override string type { get { return "build"; } }
        public AchProfessionalBuilder() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.blocksBuild >= 1000000001; }
        public override string[] Info(Player p, bool earned)
        {
            long i = 1000000001 - p.blocksBuild;
            return new string[] {
                "&aPlace 1000000001" + Server.DefaultColor + " blocks, because &a1000000000" + Server.DefaultColor + " is not enough",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to build &4" + i + " " + Server.DefaultColor + " block" + (i != 1 ? "s" : "") + " more to earn this achievement."
            };
        }
    }
}