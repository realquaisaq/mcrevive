﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.IO;

namespace MCRevive
{
    public abstract class Achievement
    {
        public static List<Achievement> all = new List<Achievement>();
        public static List<Achievement> custom = new List<Achievement>();
        public abstract string name { get; }
        public abstract string type { get; }
        public abstract void Load();
        public abstract bool Check(Player p);
        public abstract string[] Info(Player p, bool earned);
        public bool activated = true;

        public static Achievement Find(string Name)
        {
            bool found = false;
            Achievement ach = null;
            foreach (Achievement a in all)
            {
                if (a.name == Name) return a;
                if (a.name.Contains(Name)) if (!found) { ach = a; found = true; } else ach = null;
            }
            return ach;
        }

        public static void InitAll()
        {
            custom = new List<Achievement>();
            try
            {
                foreach (string s in Directory.GetFiles("addons/achievements"))
                {

                }
            }
            catch (Exception ex) { Server.ErrorLog(ex); custom = null; }
            all = new List<Achievement>();
            //Building Achievements
            all.Add(new AchMinecraftBegins());
            all.Add(new AchBlockMaker());
            all.Add(new AchHouseCreator());
            all.Add(new AchProfessionalBuilder());
            all.Add(new AchGodlyBuilder());
            all.Add(new AchGriefer());
            all.Add(new AchEcoFriend());

            //Ranking Achievements
            all.Add(new AchPromoted());
            all.Add(new AchDemoted());
            all.Add(new AchRankOperator());
            all.Add(new AchRankAdmin());

            //Server-wise Achievements
            all.Add(new AchImStaying());
            all.Add(new AchServerFan());
            all.Add(new AchAddicted());
            all.Add(new Ach350logins());
            
            //Other Achievements
            all.Add(new AchSurvivor());
            all.Add(new AchLoudMouth());
            all.Add(new AchSpawner());
            all.Add(new AchBully());
            all.Add(new AchNinja());
            all.Add(new AchFlyer());
            all.Add(new AchYourFace());
            if (custom != null) all.AddRange(custom.ToArray());
        }
    }
    class PlayerAchievement
    {
        Achievement achievement;
        bool earned;

        public PlayerAchievement(Achievement Achievement, bool Earned)
        {
            achievement = Achievement;
            earned = Earned;
        }
    }
}
