﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class AchLoudMouth : Achievement
    {
        public override string name { get { return "loud mouth"; } }
        public override string type { get { return "other"; } }
        public AchLoudMouth() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.capsRage >= 5000; }
        public override string[] Info(Player p, bool earned)
        {
            long i = 5000 - p.capsRage;
            return new string[] {
                "Type &a5000 " + Server.DefaultColor + "letters in upper-case.",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to type &4" + i + Server.DefaultColor + " more upper-case letter" + (i != 1 ? "s" : "") + " to earn this achievement."
            };
        }
    }
}
