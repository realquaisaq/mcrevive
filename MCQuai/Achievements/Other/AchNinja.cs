﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class AchNinja : Achievement
    {
        public override string name { get { return "ninja"; } }
        public override string type { get { return "other"; } }
        public AchNinja() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.slapper >= 500; }
        public override string[] Info(Player p, bool earned)
        {
            int i = 500 - p.slapper;
            return new string[] {
                "Slap someone &a500" + Server.DefaultColor + " times.",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to slap &4" + i + Server.DefaultColor + " more " + (i != 1 ? "people" : "person") + ", to earn this achievement."
            };
        }
    }
}
