﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class AchYourFace : Achievement
    {
        public override string name { get { return "your face"; } }
        public override string type { get { return "other"; } }
        public AchYourFace() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.slapped >= 50; }
        public override string[] Info(Player p, bool earned)
        {
            int i = 50 - p.slapped;
            return new string[] {
                "Get slapped by someone &a50" + Server.DefaultColor + " times.",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to spawn &4" + i + Server.DefaultColor + " more bot" + (i != 1 ? "s" : "") + " to earn this achievement."
            };
        }
    }
}
