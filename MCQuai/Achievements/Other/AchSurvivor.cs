﻿
namespace MCRevive
{
    class AchSurvivor : Achievement
    {
        public override string name { get { return "survivor"; } }
        public override string type { get { return "other"; } }
        public AchSurvivor() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.completedGames >= 3; }
        public override string[] Info(Player p, bool earned)
        {
            int i = 3 - p.completedGames;
            return new string[] {
                "Survive &a3 lava survival games" + Server.DefaultColor + ".",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to survive &4" + i + Server.DefaultColor + " more lava survival game" + (i != 1 ? "s" : "") + ", to earn this achievement."
            };
        }
    }
}