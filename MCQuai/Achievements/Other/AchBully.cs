﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class AchBully : Achievement
    {
        public override string name { get { return "bully"; } }
        public override string type { get { return "other"; } }
        public AchBully() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.slapper >= 50; }
        public override string[] Info(Player p, bool earned)
        {
            int i = 50 - p.slapper;
            return new string[] {
                "Slap someone &a50" + Server.DefaultColor + " times.",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to slap &4" + i + Server.DefaultColor + " more " + (i != 1 ? "people" : "person") + ", to earn this achievement."
            };
        }
    }
}
