﻿
namespace MCRevive
{
    class AchFlyer : Achievement
    {
        public override string name { get { return "Flyer"; } }
        public override string type { get { return "other"; } }
        public AchFlyer() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.flys >= 15; }
        public override string[] Info(Player p, bool earned)
        {
            int i = 15 - p.flys;
            return new string[] {
                "Use the command &aFly 15 times" + Server.DefaultColor + ".",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to use the fly command &4" + i + Server.DefaultColor + "more time" + (i != 1 ? "s" : "") + ", to earn this achievement."
            };
        }
    }
}