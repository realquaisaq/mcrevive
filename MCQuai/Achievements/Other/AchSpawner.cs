﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class AchSpawner : Achievement
    {
        public override string name { get { return "spawner"; } }
        public override string type { get { return "other"; } }
        public AchSpawner() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.spawnedBots >= 200; }
        public override string[] Info(Player p, bool earned)
        {
            int i = 200 - p.spawnedBots;
            return new string[] {
                "Spawn &a200 bots" + Server.DefaultColor +  ".",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to spawn &4" + i + Server.DefaultColor + " more bot" + (i != 1 ? "s" : "") + " to earn this achievement."
            };
        }
    }
}
