﻿
namespace MCRevive
{
    class AchServerFan : Achievement
    {
        public override string name { get { return "Server Fan"; } }
        public override string type { get { return "server"; } }
        public AchServerFan() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.TimeOnServer[0] >= 24; }
        public override string[] Info(Player p, bool earned)
        {
            int i = 24 - p.TimeOnServer[0];
            return new string[] {
                "&aPlay for 24 hours" + Server.DefaultColor + ".",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to play for &4" + i + " more hour" + (i != 1 ? "s" : "") + ", to earn this achievement."
            };
        }
    }
}