﻿namespace MCRevive
{
    class Ach350logins : Achievement
    {
        public override string name { get { return "350 Logins"; } }
        public override string type { get { return "server"; } }
        public Ach350logins() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.totalLogin >= 350; }
        public override string[] Info(Player p, bool earned)
        {
            int i = 350 - p.totalLogin;
            return new string[] {
                "&aLog in 350 times" + Server.DefaultColor + ".",
                "You have earned the achivement 350 Logins!",
                "You need to log in &4" + i + " time" + (i != 1 ? "s" : "") + Server.DefaultColor + " more, to earn this achievement."
            };
        }
    }
}

