﻿
namespace MCRevive
{
    class AchAddicted : Achievement
    {
        public override string name { get { return "Addicted"; } }
        public override string type { get { return "server"; } }
        public AchAddicted() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.TimeOnServer[0] >= 100; }
        public override string[] Info(Player p, bool earned)
        {
            int i = 100 - p.TimeOnServer[0];
            return new string[] {
                "&aPlay for 100 hours" + Server.DefaultColor + ".",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to play for &4" + i + " more hour" + (i != 1 ? "s" : "") + ", to earn this achievement."
            };
        }
    }
}