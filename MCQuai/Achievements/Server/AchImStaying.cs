﻿
namespace MCRevive
{
    class AchImStaying : Achievement
    {
        public override string name { get { return "I'm Staying"; } }
        public override string type { get { return "server"; } }
        public AchImStaying() { }
        public override void Load() { }
        public override bool Check(Player p) { return p.totalLogin >= 50; }
        public override string[] Info(Player p, bool earned)
        {
            int i = 50 - p.totalLogin;
            return new string[] {
                "&aLog in 50 times" + Server.DefaultColor + ".",
                "You have &aearned " + Server.DefaultColor + "this achievement!",
                "You need to log in &4" + i + " time" + (i != 1 ? "s" : "") + Server.DefaultColor + " more, to earn this achievement."
            };
        }
    }
}
