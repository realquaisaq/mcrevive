﻿using System;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Threading;

namespace MCRevive
{
    class Updater
    {
        public static System.Timers.Timer updatecheck = new System.Timers.Timer(Server.updateCheckTime * 3600000); // 1000 * 60 * 60 = an hour

        public static void Start()
        {
            if (!updatecheck.Enabled)
            {
                updatecheck.Elapsed += delegate { Check(true, true); };
                updatecheck.Start();
            }
            updatecheck.Enabled = true;
        }

        public static bool Check(bool notify, bool update, bool Override = false)
        {
            if (Server.updateCheck || Override)
            {
                int tries = 0;
            retry:
                string check = Server.DownloadString("http://www.mcrevive.tk/download/_Update.php");
                if (check == null)
                {
                    if (tries++ > 3)
                    {
                        Server.s.Log("Could not check for updates. Check the internet connection.");
                        return true;
                    }
                    goto retry;
                }
                Server.Updated = float.Parse(check) <= float.Parse(Server.Version);
                if (Server.Updated)
                    return true;

                if (update)
                {
                    float i = Server.updateTime;
                    string s = "second" + (i != 1 ? "s" : "");
                    if (i > 60) { i /= 60F; s = "minute" + ((i != 1) ? "s" : ""); }
                    if (i > 60) { i /= 60F; s = "hour" + ((i != 1) ? "s" : ""); }
                    if (notify)
                        Player.GlobalMessage("Update found! Updating in " + Math.Round(i) + " " + s + ".");
                    System.Threading.Thread.Sleep((int)i * 1000);
                    Update();
                }
                else if (notify)
                    Server.s.Log("New version of MCRevive detected.");
                return false;
            }
            return true;
        }
        public static void Update()
        {
            try
            {
                WebClient Client = new WebClient();
                if (!Server.usingCLI)
                    Client.DownloadFile("http://www.mcrevive.tk/download/MCRevive_GUI-newest.exe", "memory/MCRevive.exe");
                else
                    Client.DownloadFile("http://www.mcrevive.tk/download/MCRevive_CLI-newest.exe", "memory/MCRevive.exe");
                Client.DownloadFile("http://www.mcrevive.tk/download/MCRevive-newest.dll", "memory/MCRevive_.dll");
            }
            catch (Exception ex)
            {
                Server.s.Log("Could not download updates!");
                Server.s.Log("Check internet connection!");
                Server.ErrorLog(ex);
                return;
            }
            try { Server.Restart(); }
            catch { Server.s.Log("Copy the files in \"memory\""); }
        }
    }
}
