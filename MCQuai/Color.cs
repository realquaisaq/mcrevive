﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Drawing;

namespace MCRevive
{
    public static class c
    {
        public const string black = "&0";
        public const string navy = "&1";
        public const string green = "&2";
        public const string teal = "&3";
        public const string maroon = "&4";
        public const string purple = "&5";
        public const string gold = "&6";
        public const string silver = "&7";
        public const string gray = "&8";
        public const string blue = "&9";
        public const string lime = "&a";
        public const string aqua = "&b";
        public const string red = "&c";
        public const string pink = "&d";
        public const string yellow = "&e";
        public const string white = "&f";
        public const string rainbow = "&r";
        public static string Parse(string str)
        {
            switch (str.ToLower())
            {
                case "black":
                case black: return black;
                case "navy":
                case navy: return navy;
                case "green":
                case green: return green;
                case "teal":
                case teal: return teal;
                case "maroon":
                case maroon: return maroon;
                case "purple":
                case purple: return purple;
                case "gold":
                case gold: return gold;
                case "silver":
                case silver: return silver;
                case "gray":
                case gray: return gray;
                case "blue":
                case blue: return blue;
                case "lime":
                case lime: return lime;
                case "aqua":
                case aqua: return aqua;
                case "red":
                case red: return red;
                case "pink":
                case pink: return pink;
                case "yellow":
                case yellow: return yellow;
                case "white":
                case white: return white;
                case "rainbow":
                case rainbow: return rainbow;
                default: return "";
            }
        }

        public static string Name(string str)
        {
            switch (str)
            {
                case black:
                case "black": return "black";
                case navy:
                case "navy": return "navy";
                case green:
                case "green": return "green";
                case teal:
                case "teal": return "teal";
                case maroon:
                case "maroon": return "maroon";
                case purple:
                case "purple": return "purple";
                case gold:
                case "gold": return "gold";
                case silver:
                case "silver": return "silver";
                case gray:
                case "gray": return "gray";
                case blue:
                case "blue": return "blue";
                case lime:
                case "lime": return "lime";
                case aqua:
                case "aqua": return "aqua";
                case red:
                case "red": return "red";
                case pink:
                case "pink": return "pink";
                case yellow:
                case "yellow": return "yellow";
                case white:
                case "white": return "white";
                case rainbow:
                case "rainbow": return "rainbow";
                default: return "";
            }
        }

        public static string Remove(string str)
        {
            for (int i = 0; i <= 9; i++)
                str = str.Replace("&" + i, "");
            for (char c = 'a'; c <= 'f'; c++)
                str = str.Replace("&" + c, "").Replace("&" + c.ToString().ToUpper(), "");
            return str.Replace("&r", "").Replace("&R", "");
        }

        public static string Convert(string str)
        {
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    str = str.Replace("%" + i, "&" + i);
                    str = str.Replace("&" + i + "&", "&");
                    str = str.Replace("&" + i + " &", " &");
                }
                for (char ch = 'a'; ch <= 'f'; ch++)
                {
                    str = str.Replace("%" + ch, "&" + ch).Replace("%" + char.ToUpper(ch), "&" + ch);
                    str = str.Replace("&" + ch + "&", "&").Replace("&" + char.ToUpper(ch) + "&", "&");
                    str = str.Replace("&" + ch + " &", " &").Replace("&" + char.ToUpper(ch) + " &", " &");
                }
                if (str.Contains("%r") || str.Contains("&r") || str.Contains("%R") || str.Contains("&R"))
                {
                    string newstr = str.Replace("%r", "&r").Replace("%R", "&r");
                    Random rand = new Random();
                    char[] colors = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
                    string rainbow = newstr.Remove(newstr.IndexOf("&r"));
                    newstr = newstr.Substring(newstr.IndexOf("&r"));

                    while (newstr.Contains("&r"))
                    {
                        string container = newstr.Substring(2);
                        string newrainbow = "..";
                        int pos = 0;
                        foreach (char ch in container)
                        {
                            if (ch == '&')
                                break;
                            //if (ch != ' ')
                            //{
                                char col = colors[rand.Next(colors.Length - 1)];
                                while (col == newrainbow[pos + 1])
                                    col = colors[rand.Next(colors.Length - 1)];
                            newrainbow += ch != ' ' ? "&" + col + ch : " ";
                            //}
                            //newrainbow += ch;
                            pos += ch == ' ' ? 1 : 3;
                        }
                        rainbow += newrainbow.Substring(2);
                        newstr = container.Substring(pos / 3);
                    }
                    str = rainbow + newstr;
                }
            }
            catch (Exception ex) { Server.ErrorLog(ex); }
            return str;
        }
    }
}