﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using MonoTorrent.Client;

namespace MCRevive
{
    public class Server
    {
        public delegate void LogHandler(string message);
        public delegate void HeartBeatHandler();
        public delegate void MessageEventHandler(string message);
        public delegate void PlayerListHandler(List<Player> playerList);
        public delegate void VoidHandler();

        public static Thread blockThread;
        public static Thread locationChecker;
        public event LogHandler OnLog;
        public event LogHandler OnCommand;
        public event LogHandler OnMJS;
        public event HeartBeatHandler HeartBeatFail;
        public event MessageEventHandler OnURLChange;
        public event PlayerListHandler OnPlayerListChange;
        public event VoidHandler OnSettingsUpdate;
        public event VoidHandler OnRanksChange;

        public const string Version = "22";
        public const bool Beta = true;

        public static Socket listen;
        public static System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();
        public static System.Timers.Timer bancollecter = new System.Timers.Timer(3600000); //Every 1 hour
        public static System.Timers.Timer updateTimer = new System.Timers.Timer(100);     //Every 100 millisecond
        static System.Timers.Timer messageTimer = new System.Timers.Timer(60000 * 5);    //Every 5 minutes
        public static System.Timers.Timer cloneTimer = new System.Timers.Timer(5000);   //Every 5 seconds
        public struct achievements { public string filename, realname; public bool allowed; };
        public static System.Net.WebClient webcli = new System.Net.WebClient();

        public static PlayerList bannedIP;
        public static MapGenerator MapGen;

        //antigrief
        public static bool AGS = true;
        public static bool opNotify = true;
        public static int AGSmultiplier = 4;
        public static string MBD = "Destroyed";
        public static string MBD2 = "Built";
        public static bool extraBan = true;

        //stuff
        public static string URL = "";
        public static PlayerList ircControllers;

        public static Level mainLevel = null;
        public static List<Level> levels;
        public static List<string> afkset = new List<string>();
        public static List<string> messages = new List<string>();
        public static List<string> reviews = new List<string>();
        public static bool chatmod = false;

        #region Server Settings
        public const byte version = 7;
        public static string salt = "";
        public static string womSalt = "";
        public static string IP = "";

        public static string FirstServerBootDate;
        public static string FirstServerBootTime;
        public static DateTime session;
        public static string TheOwner = "";
        public static string consolename = "God";
        public static string name = "[MCRevive] Default";
        public static string motd = "Welcome to the MCRevive server";
        public static int players = 20;
        public static byte maps = 5;
        public static int port = 25565;
        public static int oldPort = 0;
        public static bool pub = true;
        public static bool verify = true;
        public static bool worldChat = true;
        public static bool guestGoto = true;
        public static bool checkPortforward = true;
        public static bool helpPort = true;
        public static bool newlvlHelp = true;

        public static bool adminpromote = true;
        public static bool oppromote = true;
        public static bool GeoLocation = true;
        public static string Ownerlogin = "server owner appeared!";

        public static string level = "main";

        public static List<achievements> allAchievements = loadAllAch();
        public static bool allowAchievements = true;
        public static bool loadingRaks = false;
        public static bool changelog = true;
        public static bool usingCLI = true;
        public static bool oldHelp = false;
        public static bool reportBack = false;

        public static bool irc = false;
        public static int ircPort = 6667;
        public static string ircNick = "MCRevive";
        public static string ircServer = "irc.esper.net";
        public static string ircChannel = "#mcrevive";
        public static bool ircIdentify = false;
        public static string ircPassword = "";

        public static string DefaultColor = "&e";
        public static string IRCColor = "&5";

        public static int afkminutes = 10;
        public static int afkkick = 45;

        public static bool antiTunnel = true;
        public static byte maxDepth = 2;
        public static bool physicsRestart = true;
        public static int physUndo = 60000;
        public static int totalUndo = 3000;
        public static int Overload = 1500;
        public static int backupInterval = 150;

        public static bool updateCheck = true;
        public static int updateCheckTime = 1;
        public static bool forceUpdate = true;
        public static int updateTime = 10;
        public static bool Updated = true;

        public static bool restartOnError = true;
        public static bool shuttingdown = false;
        public static bool forceShutdown = false;
        
        #endregion

        public static MainLoop ml;
        public static Server s;
        public Server()
        {
            ml = new MainLoop("server");
            Server.s = this;
        }
        public static string[] userMOTD;
        public void Start()
        {
            try
            {
                if (!Directory.Exists("logs")) Directory.CreateDirectory("logs");
                if (!Directory.Exists("logs/errors")) Directory.CreateDirectory("logs/errors");
                Properties.LoadnSave("all");
                if (!Directory.Exists("text")) Directory.CreateDirectory("text");
                if (!File.Exists("text/description.txt")) File.WriteAllText("text/description.txt", "A Nice Server");
                if (!Directory.Exists("memory")) Directory.CreateDirectory("memory");
                if (!Directory.Exists("memory/server")) Directory.CreateDirectory("memory/server");
                if (!Directory.Exists("memory/undo")) Directory.CreateDirectory("memory/undo");
                if (!Directory.Exists("memory/undoPrevious")) Directory.CreateDirectory("memory/undoPrevious");
                if (!Directory.Exists("memory/player")) Directory.CreateDirectory("memory/player");
                if (!Directory.Exists("memory/zones/")) Directory.CreateDirectory("memory/zones");
                if (!Directory.Exists("memory/portals")) Directory.CreateDirectory("memory/portals");
                if (!File.Exists("memory/server/firstboot.txt")) File.WriteAllText("memory/server/firstboot.txt", DateTime.Now.Ticks.ToString());
                if (!File.Exists("memory/server/version.txt")) File.WriteAllText("memory/server/version.txt", Server.Version);
                else if (File.ReadAllText("memory/server/version.txt") != Server.Version)
                {
                    File.WriteAllText("memory/server/version.txt", Server.Version);
                    Server.Restart();
                }

                if (!Directory.Exists("mjs")) Directory.CreateDirectory("mjs");
                if (!Directory.Exists("mjs/commands"))
                {
                    Directory.CreateDirectory("mjs/commands");
                    MJS.Script.WriteExampleScripts();
                }
                if (!Directory.Exists("mjs/events")) Directory.CreateDirectory("mjs/events");
                if (Directory.Exists("mjs/threads")) Directory.Delete("mjs/threads", true); 
                Directory.CreateDirectory("mjs/threads");

                if (File.Exists("restart.bat")) File.Delete("restart.bat");
                if (File.Exists("updater.bat")) File.Delete("updater.bat");

                if (!File.Exists("memory/server/readme.txt"))
                {
                    StreamWriter SW = new StreamWriter(File.Create("README.txt"));
                    SW.WriteLine("This is a simple tutorial on how to set up you MCRevive server and tips to make it better!");
                    SW.WriteLine();
                    SW.WriteLine("1. Run MCRevive.exe which is located in the downloaded folder.");
                    if (!usingCLI)
                    {
                        SW.WriteLine("2. Wait for the server to start up, and the files to be generated.");
                        SW.WriteLine("3. Click the properties button and edit what you want in the applicable fields.");
                        SW.WriteLine("4. Remember to set yourself as \"the owner\" of the server.");
                        SW.WriteLine("4. Press \"Save and Close\" when you have finished and close the GUI.");
                    }
                    else
                    {
                        SW.WriteLine("2. Wait for the server to start up, and the files to be generated.");
                        SW.WriteLine("3. Shut the server again.");
                        SW.WriteLine("4. In the folder \"properties\", open the .properties files with Notepad and edit the settings. Then Save.");
                        SW.WriteLine("5. Open the \"ranks\" folder and in the owner.txt place you username(Example: quaisaq). Then save it.");
                        SW.WriteLine("6. Run MCRevive.exe again and your server is now live and ready to go.");
                    }
                    SW.WriteLine();
                    SW.WriteLine("**Tips and Tricks**");
                    SW.WriteLine("1. Add custom ranks and be nice to your players.");
                    SW.WriteLine("2. Sign up on the MCRevive Forums, located at http://mcrevive.tk/forum/.");
                    SW.WriteLine("3. Have Fun!!!!!!!!");
                    SW.Flush();
                    SW.Close();
                }
                if (File.Exists("README.txt") && !File.Exists("memory/server/README.txt"))
                    File.Copy("README.txt", "memory/server/README.txt");
                if (!File.Exists("Interop.NATUPNPLib.dll"))
                {
                    s.Log("You are missing a library file, downloading...");
                    webcli.DownloadFile("http://www.mcrevive.tk/download/__Interop.NATUPNPLib.dll", "Interop.NATUPNPLib.dll");
                    Restart();
                }
                if (!File.Exists("Meebey.SmartIrc4net.dll"))
                {
                    s.Log("You are missing a library file, downloading...");
                    webcli.DownloadFile("http://www.mcrevive.tk/download/__Meebey.SmartIrc4net.dll", "Meebey.SmartIrc4net.dll");
                    Restart();
                }
                //////if (!File.Exists("MJS.dll"))
                //////{
                //////    s.Log("You are missing a .dll file, downloading...");
                //////    new WebClient().DownloadFile("http://www.mcrevive.tk/download/__MJS.dll", "MJS.dll");
                //////    Restart();
                //////}

                Updater.updatecheck.Enabled = false;
                UpdateCheck();
                Group.InitAll();
                Command.InitAll();
                GrpCommands.fillRanks();
                Block.SetBlocks();
            }
            catch (Exception ex) { ErrorLog(ex); }
            Thread.Sleep(100);
            ml.Queue(delegate
            {
                levels = new List<Level>(Server.maps);
                MapGen = new MapGenerator();

                Random random = new Random();

                if (File.Exists("levels/" + Server.level + ".mcqlvl") || File.Exists("levels/" + Server.level + ".lvl"))
                {
                    mainLevel = Level.Load(Server.level);
                    if (mainLevel == null)
                    {
                        if (File.Exists("levels/" + Server.level + ".mcqlvl.backup"))
                        {
                            Log("Atempting to load backup.");
                            File.Copy("levels/" + Server.level + ".mcqlvl.backup", "levels/" + Server.level + ".mcqlvl", true);
                            mainLevel = Level.Load(Server.level);
                            if (mainLevel == null)
                            {
                                Log("BACKUP FAILED!");
                                Console.ReadKey(); return;
                            }
                        }
                        else if (File.Exists("levels/" + Server.level + ".lvl.backup"))
                        {
                            Log("Atempting to load backup.");
                            File.Copy("levels/" + Server.level + ".lvl.backup", "levels/" + Server.level + ".lvl", true);
                            mainLevel = Level.Load(Server.level);
                            if (mainLevel == null)
                            {
                                Log("BACKUP FAILED!");
                                Console.ReadKey(); return;
                            }
                        }
                        else
                        {
                            Log("BACKUP NOT FOUND!");
                            Console.ReadKey(); return;
                        }
                    }
                }
                else
                {
                    Log("mainlevel not found, generating.");
                    mainLevel = new Level(Server.level, 128, 64, 128, "flat", null);

                    mainLevel.permissionvisit = LevelPermission.Guest;
                    mainLevel.permissionbuild = LevelPermission.Guest;
                    mainLevel.Save();
                }
                levels.Add(mainLevel);
            });
            ml.Queue(delegate
            {
                bannedIP = PlayerList.Load("banned-ip.txt", null);
                ircControllers = PlayerList.Load("IRC_Controllers.txt", null);

                Group.GroupList.ForEach((grp) => {
                    if (grp.name != "nobody")
                        grp.playerList = PlayerList.Load(grp.fileName, grp);
                });
                UpdateRanks();
            });
            ml.Queue(delegate
            {
                if (File.Exists("motd.txt")) File.Copy("motd.txt", "text/motd.txt");
                if (File.Exists("text/motd.txt"))
                {
                    userMOTD = File.ReadAllLines("text/motd.txt");
                    string storeName = "", storeMOTD = "";
                    for (int i = 0; i < userMOTD.Length; i++)
                        try
                        {
                            storeName = userMOTD[i].Substring(userMOTD[i].IndexOf("NAME: ") + 5, userMOTD[i].IndexOf("MOTD: ") - 5);
                            storeMOTD = userMOTD[i].Substring(userMOTD[i].IndexOf("MOTD: ") + 5);

                            if (storeName.Length < 64) storeName = storeName.PadRight(64, ' ');
                            else if (storeName.Length > 64) storeName = storeName.Substring(0, 63);

                            if (storeMOTD.Length < 64) storeMOTD = storeMOTD.PadRight(64, ' ');
                            else if (storeMOTD.Length > 64) storeMOTD = storeMOTD.Substring(0, 63);

                            userMOTD[i] = storeName + storeMOTD;
                        }
                        catch { }
                }
            });
            ml.Queue(delegate
            {
                if (File.Exists("autoload.txt")) File.Copy("autoload.txt", "text/autoload.txt");
                if (File.Exists("text/autoload.txt"))
                {
                    try
                    {
                        string[] lines = File.ReadAllLines("text/autoload.txt");
                        foreach (string line in lines)
                            try
                            {
                                string _line = line.Trim();
                                if (_line == "" || _line[0] == '#') continue;

                                string key = line.Split('=')[0].Trim();
                                string value;
                                try { value = line.Split('=')[1].Trim(); }
                                catch { value = "0"; }

                                if (!key.Equals("main"))
                                    Command.all.Find("load").Use(null, key + " " + value);
                                else
                                    try
                                    {
                                        int temp = int.Parse(value);
                                        if (temp >= 0 && temp <= 2)
                                            mainLevel.physics = temp;
                                    }
                                    catch { Server.s.Log("Physics variable invalid"); }
                            }
                            catch { Server.s.Log(line + " failed."); }
                    }
                    catch { Server.s.Log("autoload.txt error"); }
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                else { }
            });

            ml.Queue(delegate
            {
                Server.s.Log("Creating listening socket on port " + Server.port + "...");
                if (Setup())
                    oldPort = port;
                else
                {
                    s.Log("Could not create socket connection.");
                    s.Log("Shutting down in 10 seconds...");
                    shuttingdown = true;
                    return;
                }
                if (checkPortforward && !PortForwarded() && helpPort)
                {
                    s.Log("Trying to forward ports...");
                    try
                    {
                        NATUPNPLib.UPnPNATClass cls = new NATUPNPLib.UPnPNATClass();
                        cls.StaticPortMappingCollection.Add(Server.port, "TCP", Server.port, GetLocalIP(), true, "MCRevive_Server_TCP-" + Server.port);
                        cls.StaticPortMappingCollection.Add(Server.port, "UDP", Server.port, GetLocalIP(), true, "MCRevive_Server_UDP-" + Server.port);
                        Server.s.Log("Portforward successful!");
                    }
                    catch { s.Log("Portforwarding failed."); s.Log("Make sure that UPnP is enabled in your router."); }
                }
                Memory.UpdateBlocks();
                Memory.UpdatePortals();
                s.Log("Done.");
                //MJSStarter.Start();
            });

            if (TheOwner == "")
                s.Log("Remember to set the-owner in server.properties!");

            ml.Queue(delegate
            {
                updateTimer.Elapsed += delegate
                {
                    Player.GlobalUpdate();
                    PlayerBot.GlobalUpdatePosition();
                };
                updateTimer.Start();
            });

            Heartbeat.Init();
            
            ml.Queue(delegate
            {
                messageTimer.Elapsed += delegate
                {
                    RandomMessage();
                };
                messageTimer.Start();
                process = System.Diagnostics.Process.GetCurrentProcess();
                if (File.Exists("messages.txt"))
                    File.Copy("messages.txt", "text/messages.txt");
                if (File.Exists("text/messages.txt") && File.ReadAllText("text/messages.txt") != "")
                {
                    StreamReader r = File.OpenText("text/messages.txt");
                    while (!r.EndOfStream)
                        messages.Add(r.ReadLine());
                }
                else
                    File.WriteAllText("text/messages.txt", "This server is running &3MCRevive" + DefaultColor + "." + Environment.NewLine + "download &3MCRevive" + DefaultColor + " at &4www.mcrevive.tk");

                if (Server.irc)
                    new IRCBot();

                new AutoSaver(Server.backupInterval);     //2 and a half min

                locationChecker = new Thread(new ThreadStart(delegate
                {
                    while (true)
                    {
                        Thread.Sleep(3);
                        foreach (Player p in Player.players)
                        {
                            try
                            {
                                if (p.frozen)
                                {
                                    p.SendPos(p.pos[0], p.pos[1], p.pos[2], p.rot[0], p.rot[1]);
                                    continue;
                                }

                                ushort x = (ushort)(p.pos[0] / 32);
                                ushort y = (ushort)(p.pos[1] / 32);
                                ushort z = (ushort)(p.pos[2] / 32);

                                p.CheckBlock(x, y, z);
                                p.oldBlock = (ushort)(x + y + z);
                            }
                            catch (Exception e) { Server.ErrorLog(e); }
                        }
                    }
                }));
                locationChecker.Start();

                bancollecter.Elapsed += delegate
                {
                    try { webcli.DownloadFile("http://www.mcrevive.tk/download/_Banlist.php", "memory/server/ban-list.txt"); }
                    catch { File.WriteAllText("memory/server/ban-list.txt", ""); }
                    string[] extraBans = File.ReadAllText("memory/server/ban-list.txt").Split(':');
                    foreach (string p in extraBans)
                    {
                        if (!Group.Find("banned").playerList.Contains(p.Trim())) Group.Find("banned").playerList.Add(p.Trim());
                    }
                    Group.Find("banned").playerList.Save(Group.Find("banned").fileName);
                };
                bancollecter.Start();

                try
                {
                    foreach (string review in File.ReadAllText("memory/reviews.txt").Split('&'))
                        reviews.Add(review.Split(' ')[0] + "&" + review.Split(' ')[3].Replace('@', ' '));
                }
                catch { }
            });
            session = DateTime.Now;
        }

        static bool PortForwarded()
        {
            try
            {
                s.Log("Checking if port is forwarded...");
                Server.IP = Server.DownloadString("http://mcrevive.tk/tools/ip.php");
                bool forwarded = Server.DownloadString("http://ports.yougetsignal.com/check-port.php?remoteAddress=" + Server.IP + "&portNumber=" + Server.port).Contains("open");
                s.Log("Port " + Server.port + " is " + (forwarded ? "open!" : "closed!"));
                return forwarded;
            }
            catch (Exception ex)
            {
                s.Log("Failed to check portforward.");
                s.Log("Check System Tab for more information.");
                ErrorLog(ex);
                return true;
            }
        }
        public static string GetLocalIP()
        {
            string IP = null;
            foreach (System.Net.IPAddress IPAddress in System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList)
            {
                if (IPAddress.AddressFamily.ToString() == "InterNetwork")
                    IP = IPAddress.ToString();
            }
            return IP;
        }

        static bool Setup()
        {
            try
            {
                System.Net.IPEndPoint endpoint = new System.Net.IPEndPoint(System.Net.IPAddress.Any, Server.port);
                listen = new Socket(endpoint.Address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                listen.Bind(endpoint);
                listen.Listen((int)SocketOptionName.MaxConnections);

                listen.BeginAccept(new AsyncCallback(Accept), null);
                return true;
            }
            catch (SocketException e) { ErrorLog(e); return false; }
            catch (Exception e) { ErrorLog(e); return false; }
        }

        static void Accept(IAsyncResult result)
        {
            // found information: http://www.codeguru.com/csharp/csharp/cs_network/sockets/article.php/c7695
            // -Descention
            try
            {
                new Player(listen.EndAccept(result));
                listen.BeginAccept(new AsyncCallback(Accept), null);
            }
            catch (SocketException e) { ErrorLog(e); }
            catch (Exception e) { ErrorLog(e); }
        }

        public static void Exit(bool kill = true)
        {
            try
            {
                Server.s.Log("Shutting down...");
                if (MJS.Event.Exists("ServerShutdown")) MJS.Event.Trigger("ServerShutdown", "");
                //////MJS.Event.SaveShit();
            }
            catch { }
            try
            {
                Player.players.ForEach(delegate(Player p) { p.Kick("Server shutdown."); });
                Player.connections.ForEach(delegate(Player p) { p.Kick("Server shutdown."); });
            }
            catch { }
            try
            {
                Server.levels.ForEach((l) =>
                {
                    if (l.physThread != null)
                        l.physThread.Abort();
                });
            }
            catch { }
            if (helpPort)
                try
                {
                    NATUPNPLib.UPnPNATClass cls = new NATUPNPLib.UPnPNATClass();
                    Server.s.Log("Closing forwarded ports...");
                    cls.StaticPortMappingCollection.Remove(Server.port, "TCP");
                    cls.StaticPortMappingCollection.Remove(Server.port, "UDP");
                }
                catch { }

            if (process != null)
                Server.s.Log("Process Terminated");
            if (kill)
                process.Kill();
        }

        public static void Restart()
        {
            if (!shuttingdown)
            {
                try
                {
                    Player.players.ForEach(delegate(Player p) { p.Kick("Server restarted! Rejoin!"); });
                    Player.connections.ForEach(delegate(Player p) { p.Kick("Server restarted! Rejoin!"); });
                }
                catch { }
                try
                {
                    Server.levels.ForEach((l) =>
                    {
                        if (l.physThread != null)
                            l.physThread.Abort();
                    });
                }
                catch { }

                if (File.Exists("updater.bat")) File.Delete("updater.bat");
                if (File.Exists("updater.sh")) File.Delete("updater.sh");

                if (Environment.OSVersion.ToString().ToLower().Contains("windows"))
                {
                    StreamWriter SW = new StreamWriter(File.Create("updater.bat"));
                    SW.WriteLine("@echo off");
                    SW.WriteLine("if exist memory\\MCRevive.exe (move memory\\MCRevive.exe MCRevive.exe)");
                    SW.WriteLine("if exist memory\\MCRevive_.dll (move memory\\MCRevive_.dll MCRevive_.dll)");
                    SW.WriteLine("if exist MCRevive.exe (start MCRevive.exe)");
                    SW.WriteLine("if not exist MCRevive.exe (echo MCRevive.exe could not be found, please redownload from the website)");
                    SW.WriteLine("if not exist MCRevive.exe (pause)");
                    SW.Flush();
                    SW.Close();
                    SW.Dispose();
                    Process.Start("updater.bat");
                }
                else
                {
                    StreamWriter SW = new StreamWriter(File.Create("updater.sh"));
                    SW.WriteLine("if [ -e memory/MCRevive.exe ]");
                    SW.WriteLine("then");
                    SW.WriteLine("mv -i memory/MCRevive.exe MCRevive.exe");
                    SW.WriteLine("fi");
                    SW.WriteLine("if [ -e memory/MCRevive_.dll ]");
                    SW.WriteLine("then");
                    SW.WriteLine("mv -i memory/MCRevive_.dll MCRevive_.dll");
                    SW.WriteLine("fi");
                    SW.WriteLine("if [ -e MCRevive.exe ]");
                    SW.WriteLine("then");
                    SW.WriteLine("mono MCRevive.exe");
                    SW.WriteLine("else");
                    SW.WriteLine("echo MCRevive.exe could not be found, please redownload from the website");
                    SW.WriteLine("pause");
                    SW.WriteLine("fi");
                    SW.Flush();
                    SW.Close();
                    SW.Dispose();
                    Process.Start("updater.sh", Server.process.Id.ToString());
                }
                process.Kill();
            }
        }

        public void PlayerListUpdate()
        {
            if (Server.s.OnPlayerListChange != null) Server.s.OnPlayerListChange(Player.players);
        }

        public void FailBeat()
        {
            if (HeartBeatFail != null) HeartBeatFail();
        }

        public void UpdateUrl(string url)
        {
            if (OnURLChange != null) OnURLChange(url);
        }
        public void UpdateRanks()
        {
            if (OnRanksChange != null) OnRanksChange();
        }

        public void Log(string message)
        {
            if (OnLog != null) OnLog(DateTime.Now.ToString("(HH:mm:ss) ") + message);
            Logger.LogMessage(DateTime.Now.ToString("(HH:mm:ss) ") + message + Environment.NewLine);
        }
        public void CmdLog(string message)
        {
            if (OnCommand != null) OnCommand(DateTime.Now.ToString("(HH:mm:ss) ") + message);
            Logger.LogMessage(DateTime.Now.ToString("(HH:mm:ss) ") + message + Environment.NewLine);
        }
        public void MJSLog(string message)
        {
            if (OnMJS != null) OnMJS(DateTime.Now.ToString("(HH:mm:ss) ") + message);
            Logger.LogMessage(DateTime.Now.ToString("(HH:mm:ss) ") + message + Environment.NewLine);
        }
        public static void ErrorLog(Exception ex)
        {
            if (!Server.usingCLI) s.Log("An Error occured! For more information, go to System Logs tab");
            else s.Log("An Error occured! For more information, read \"logs/errors/" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt\"");
            Logger.LogError(ex);

            try { Server.DownloadString("http://www.mcrevive.tk/tools/error.php?msg=" + ex.ToString().Replace("\r", "").Replace('\n', '性')); }
            catch { }
        }

        public static void ParseInput()        //Handle console commands
        {
            string cmd;
            string msg;
            while (true)
            {
                string input = Console.ReadLine();
                if (input == null)
                    continue;
                cmd = input.Split(' ')[0];
                if (input.Split(' ').Length > 1)
                    msg = input.Substring(input.IndexOf(' ')).Trim();
                else
                    msg = "";
                try
                {
                    switch (cmd)
                    {
                        case "kick": Command.all.Find("kick").Use(null, msg); break;
                        case "ban": Command.all.Find("ban").Use(null, msg); break;
                        case "banip": Command.all.Find("banip").Use(null, msg); break;
                        case "setrank":
                        case "rank": Command.all.Find("rank").Use(null, msg); break;
                        case "resetirc": Command.all.Find("resetirc").Use(null, msg); break;
                        case "update": Command.all.Find("update").Use(null, msg == "" ? "newest" : msg); break;
                        case "restart":
                            Restart();
                            break;
                        case "name": Console.WriteLine(Player.players[0].GetType().GetField("name").GetValue(Player.players[0])); break;
                        case "save": Command.all.Find("save").Use(null, "all"); break;
                        case "say":
                            if (!msg.Equals(""))
                            {
                                if (Properties.ValidString(msg, "![]&:.,{}~-+()?_/\\@%$ "))
                                {
                                    Player.GlobalMessage(msg);
                                }
                                else
                                {
                                    Console.WriteLine("bad char in say");
                                }
                            }
                            break;
                        case "help":
                            if (msg == "") Console.WriteLine("ban, banip, kick, rank, resetirc, restart, save, say");
                            else if (msg == "ban") Console.WriteLine("ban <player> - bans the <player> from the server.");
                            else if (msg == "banip") Console.WriteLine("banip <ip> - bans the <ip> specified from the server.");
                            else if (msg == "kick") Console.WriteLine("kick <player> - kicks <player> from the server.");
                            else if (msg == "setrank" || msg == "rank") Console.WriteLine("setrank\rank <player> <rank> - sets <player>'s rank to <rank>.");
                            else if (msg == "resetirc") Console.WriteLine("resetirc - restarts the IRCBot, emergency only.");
                            else if (msg == "restart") Console.WriteLine("restart - restarts the server.");
                            else if (msg == "save") Console.WriteLine("save - Saves all maps.");
                            else if (msg == "say") Console.WriteLine("say <message> - Sends a global message to all players.");
                            else Console.WriteLine("Unknown command: " + msg + "!");
                            break;
                        case "folder":
                            Process.Start(Directory.GetParent("*") + "");
                            break;
                        default: Console.WriteLine("No such command!"); break;
                    }
                }
                catch (Exception e) { ErrorLog(e); }
                //Thread.Sleep(10);
            }
        }

        public static void Physics()
        {
            int wait = 250;
            while (true)
            {
                try
                {
                    if (wait > 0)
                    {
                        Thread.Sleep(wait);
                    }
                    DateTime Start = DateTime.Now;
                    levels.ForEach(delegate(Level L)    //update every level
                    {
                        L.CalcPhysics();
                    });
                    TimeSpan Took = DateTime.Now - Start;
                    wait = (int)250 - (int)Took.TotalMilliseconds;
                    if (wait < -Server.Overload)
                    {
                        levels.ForEach(delegate(Level L)    //update every level
                        {
                            try
                            {
                                L.physics = 0;
                                L.ClearPhysics();
                            }
                            catch
                            {

                            }
                        });
                        Server.s.Log("!PHYSICS SHUTDOWN!");
                        Player.GlobalMessage("!PHYSICS SHUTDOWN!");
                        wait = 250;
                    }
                    else if (wait < (int)(-Server.Overload * 0.75f))
                    {
                        Server.s.Log("!PHYSICS WARNING!");
                    }
                }
                catch
                {
                    Server.s.Log("GAH! PHYSICS EXPLODING!");
                    wait = 250;
                }

            }
        }

        public static string GetGeoLocationByIP(string strIPAddress)
        {
            try
            {
                if (strIPAddress == "127.0.0.1" || strIPAddress.StartsWith("192.168.")) strIPAddress = Server.IP;
                string[] info = Server.DownloadString("http://freegeoip.net/csv/" + strIPAddress).Replace("\"", "").Split(',');
                return info[2];
            }
            catch { return ""; }
        }

        public static void RandomMessage()
        {
            if (Player.number != 0 && messages.Count > 0)
                Player.GlobalMessage(messages[new Random().Next(0, messages.Count)]);
        }

        public void UpdateCheck()
        {
            if (!updateCheck) { Log("You should enable \"check-update\" in server.properties"); return; }
            Updater.Start();

            int tries = 0;
            retry:
            string check = Server.DownloadString("http://www.mcrevive.tk/download/_Update.php");
            if (check == null)
            {
                if (tries++ > 3)
                {
                    Server.s.Log("Could not check for updates. Check the internet connection.");
                    return;
                }
                goto retry;
            }
            
            if (float.Parse(check) > float.Parse(Version)) Updated = false;
            else Updated = true;

            if (Updated) return;
            if (File.Exists("memory/server/noupdate.txt"))
            {
                Server.s.Log("Update found!");
                Server.s.Log("Use the command '/update newest' to get the latest update.");
                return;
            }

            if (!usingCLI)
            {
                if (MessageBox.Show("Do you wish to update?", "Update?", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    try
                    {
                        webcli.DownloadFile("http://www.mcrevive.tk/download/MCRevive_GUI-newest.exe", "memory/MCRevive.exe");
                        webcli.DownloadFile("http://www.mcrevive.tk/download/MCRevive-newest.dll", "memory/MCRevive_.dll");
                    }
                    catch
                    {
                        MessageBox.Show("Could not download updates!" + Environment.NewLine + "Check internet connection!");
                        Server.s.Log("Go to www.mcrevive.tk to download the files manually.");
                        return;
                    }
                    Restart();
                }
            }
            else
            {
                Server.s.Log("Update Found!");
                if (forceUpdate)
                {
                    Server.s.Log("Downloading...");
                    try
                    {
                        webcli.DownloadFile("http://www.mcrevive.tk/download/MCRevive_CLI-newest.exe", "memory/MCRevive.exe");
                        webcli.DownloadFile("http://www.mcrevive.tk/download/MCRevive-newest.dll", "memory/MCRevive_.dll");
                    }
                    catch
                    {
                        Server.s.Log("Could not download the files!");
                        Server.s.Log("Go to www.mcrevive.tk to download the files manually.");
                        return;
                    }
                }
                else
                {
                    Server.s.Log("Type 'update' in the console, to update to the newest version of MCRevive.");
                    return;
                }
                Restart();
            }
        }
        public void SettingsUpdate()
        {
            if (OnSettingsUpdate != null) OnSettingsUpdate();
        }
        public static bool vocal(char ch)
        {
            string vocals = "aeiouy";
            bool vocal = false;
            foreach (char cha in vocals)
                if (ch == cha) vocal = true;
            return vocal;
        }
        static List<achievements> loadAllAch()
        {
            List<achievements> temp = new List<achievements>();
            achievements ach = new achievements();
            ach.allowed = true;
            ach.filename = "minecraft-begins"; ach.realname = "Minecraft Begins"; temp.Add(ach);
            ach.filename = "block-maker"; ach.realname = "Block Maker"; temp.Add(ach);
            ach.filename = "house-creator"; ach.realname = "House Creator"; temp.Add(ach);
            ach.filename = "proffesional-builder"; ach.realname = "Proffesional Builder"; temp.Add(ach);
            ach.filename = "godly-builder"; ach.realname = "Godly Builder"; temp.Add(ach);
            ach.filename = "griefer"; ach.realname = "Griefer"; temp.Add(ach);
            ach.filename = "demoted"; ach.realname = "Demoted"; temp.Add(ach);
            ach.filename = "promoted"; ach.realname = "Promoted"; temp.Add(ach);
            ach.filename = "ranked-operator"; ach.realname = "Ranked Operator"; temp.Add(ach);
            ach.filename = "ranked-admin"; ach.realname = "Ranked Admin"; temp.Add(ach);
            ach.filename = "ranked-owner"; ach.realname = "Ranked Owner"; temp.Add(ach);
            ach.filename = "im-staying"; ach.realname = "I'm Staying"; temp.Add(ach);
            ach.filename = "zombie-death"; ach.realname = "Zombie Death"; temp.Add(ach);
            ach.filename = "survivor"; ach.realname = "Survivor"; temp.Add(ach);
            ach.filename = "eco-friend"; ach.realname = "Eco Friend"; temp.Add(ach);
            ach.filename = "loud-mouth"; ach.realname = "Loud Mouth"; temp.Add(ach);
            ach.filename = "spawner"; ach.realname = "Spawner"; temp.Add(ach);
            //ach.filename = "demolist"; ach.realname = "Demolist"; temp.Add(ach);
            ach.filename = "server-fan"; ach.realname = "Server Fan"; temp.Add(ach);
            ach.filename = "addicted"; ach.realname = "Addicted"; temp.Add(ach);
            ach.filename = "flyer"; ach.realname = "Flyer"; temp.Add(ach);
            ach.filename = "bully"; ach.realname = "Bully"; temp.Add(ach);
            ach.filename = "ninja"; ach.realname = "Ninja"; temp.Add(ach);
            ach.filename = "your-face"; ach.realname = "Your Face, God Damit"; temp.Add(ach);
            return temp;
        }
        public static string DownloadString(string url)
        {
            string s = null;
            try
            {
                WebClient c = new WebClient();
                c.Encoding = System.Text.Encoding.UTF8;
                s = c.DownloadString(url);
                Player.InComeTraffic += s.Length;

                if (s.TrimEnd().EndsWith("<!-- End Of Analytics Code -->"))
                {
                    int i = s.LastIndexOf('\n', s.Length - 150);
                    for (; (s[i] == '\n' || s[i] == '\r') && i != 0; i--) { }
                    s = s.Remove(i + 1);
                }

                c.Dispose();
            }
            catch { }
            return s;
        }
    }
}