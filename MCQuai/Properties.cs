﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License
*/
using System;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Collections.Generic;

namespace MCRevive
{
    public static class Properties
    {
        public static void LoadnSave(string text = "")
        {
            if (text == "all" || text == "")
            {
                Load("server"); Save("server");
                Load("gui"); Save("gui");
                Load("antigrief"); Save("antigrief");
                Load("update"); Save("update");
                Load("achievements"); Save("achievements");
            }
            else if (text == "server" || text == "gui" || text == "antigrief" || text == "update" || text == "achievements") { Load(text); Save(text); }
            else { LoadnSave("all"); }
            Server.s.Log("Properties loaded, and saved!");
        }

        public static int i = 0;
        static List<Server.achievements> achTemp = Server.allAchievements;

        public static void Load(string givenPath)
        {
            if (!Directory.Exists("properties"))
                Directory.CreateDirectory("properties");
            string newPath = "properties/" + givenPath + ".properties";
            if (givenPath == "server")
            {
                string rndchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                Random rnd = new Random();
                Server.salt = "";
                for (int i = 0; i < 16; ++i) { Server.salt += rndchars[rnd.Next(rndchars.Length)]; }
            }

            if (File.Exists(newPath))
            {
                string[] lines = File.ReadAllLines(newPath);
                if (givenPath == "achievements") { if (lines.Length < 10) { Save(givenPath); } Server.allAchievements = new List<Server.achievements>(); }
                foreach (string line in lines)
                {
                    if (line != "" && line[0] != '#')
                    {
                        //int index = line.IndexOf('=') + 1; // not needed if we use Split('=')
                        string key = line.Split('=')[0].Trim();
                        string value = line.Split('=')[1].Trim();
                        string color = "";

                        if (givenPath == "achievements")
                        {
                            Server.achievements ds = new Server.achievements();
                            ds.filename = key.ToLower();
                            ds.realname = (key[0].ToString().ToUpper() + ((key.Contains("-")) ?
                                key.Substring(1, key.IndexOf('-') - 1).ToLower() + " " + key.Split('-')[1][0].ToString().ToUpper() + key.Substring(key.IndexOf('-') + 2).ToLower() :
                                key.Substring(1).ToLower())).Replace("Im ", "I'm ");
                            ds.allowed = value.ToLower() == "true";

                            if (achTemp.Find(a => a.filename == key).realname != null)
                            {
                                Server.allAchievements.Add(ds);
                            }
                        }

                        switch (key.ToLower())
                        {
                            case "the-owner":
                                if (value == "")
                                {
                                    Server.TheOwner = "";
                                }
                                else if (Player.ValidName(value))
                                {
                                    Server.TheOwner = value.ToLower();
                                }
                                else
                                {
                                    Server.s.Log("owners name is invalid! ");
                                    Server.TheOwner = "";
                                }
                                break;
                            case "server-name":
                                if (ValidString(value, "![]:.,{}~-+()?_/\\\'\" "))
                                {
                                    Server.name = value;
                                }
                                else
                                {
                                    Server.s.Log("server-name invalid! setting to default.");
                                }
                                break;
                            case "motd":
                                if (ValidString(value, "![]&:.,{}~-+()?_/\\ "))
                                {
                                    Server.motd = value;
                                }
                                else
                                {
                                    Server.s.Log("motd invalid! setting to default.");
                                }
                                break;
                            case "port":
                                try
                                {
                                    Server.port = Convert.ToInt32(value);
                                }
                                catch
                                {
                                    Server.s.Log("port invalid! setting to default.");
                                }
                                break;
                            case "verify-names":
                                Server.verify = value.ToLower() == "true";
                                break;
                            case "public":
                                Server.pub = value.ToLower() == "true";
                                break;
                            case "global-chat":
                                Server.worldChat = value.ToLower() == "true";
                                break;
                            case "guest-goto":
                                Server.guestGoto = value.ToLower() == "true";
                                break;
                            case "admin-promote":
                                Server.adminpromote = value.ToLower() == "true";
                                break;
                            case "op-promote":
                                Server.oppromote = value.ToLower() == "true";
                                break;
                            case "default-color":
                                color = c.Parse(value);
                                if (color == "")
                                {
                                    try { color = c.Name(value); if (color != "") color = c.Parse(value); }
                                    catch { Server.s.Log("Could not find " + value); return; }
                                }
                                Server.DefaultColor = c.Parse(color);
                                break;
                            case "geolocator":
                                Server.GeoLocation = value.ToLower() == "true";
                                break;
                            case "owner-login":
                                if (ValidString(value, "![]&:.,{}~-+()?_/\\@%$ "))
                                    Server.Ownerlogin = value.ToLower() == "-false-" || value == "" ? "joined the game." : value;
                                else
                                {
                                    Server.s.Log("Owner-login invalid!");
                                    Server.Ownerlogin = "joined the game.";
                                }
                                break;
                            case "wom-salt":
                                if (ValidString(value, "") && (value.Length == 16 || value.Length == 0))
                                    Server.womSalt = value;
                                else
                                {
                                    Server.s.Log("wom-salt is invalid!");
                                    Server.s.Log("Generating new...");
                                    Server.womSalt = Server.salt;
                                }
                                break;
                            case "max-players":
                                try
                                {
                                    if (int.Parse(value) > 128)
                                    {
                                        value = "128";
                                        Server.s.Log("Max players has been lowered to 128.");
                                    }
                                    else if (int.Parse(value) < 1)
                                    {
                                        value = "1";
                                        Server.s.Log("Max players has been increased to 1.");
                                    }
                                    Server.players = int.Parse(value);
                                }
                                catch
                                {
                                    Server.s.Log("max-players invalid! setting to default.");
                                }
                                break;
                            case "main-level":
                                Server.level = value;
                                break;
                            case "max-maps":
                                try
                                {
                                    if (Convert.ToByte(value) > 20)
                                    {
                                        value = "20";
                                        Server.s.Log("Max maps has been lowered to 20.");
                                    }
                                    else if (Convert.ToByte(value) < 1)
                                    {
                                        value = "1";
                                        Server.s.Log("Max maps has been increased to 1.");
                                    }
                                    Server.maps = Convert.ToByte(value);
                                }
                                catch
                                {
                                    Server.s.Log("max-maps invalid! setting to default.");
                                }
                                break;
                            case "irc":
                                Server.irc = value.ToLower() == "true";
                                break;
                            case "irc-server":
                                Server.ircServer = value;
                                break;
                            case "irc-nick":
                                Server.ircNick = value;
                                break;
                            case "irc-channel":
                                Server.ircChannel = value;
                                break;
                            case "irc-port":
                                try
                                {
                                    Server.ircPort = Convert.ToInt32(value);
                                }
                                catch
                                {
                                    Server.s.Log("irc-port invalid! setting to default.");
                                }
                                break;
                            case "irc-identify":
                                try
                                {
                                    Server.ircIdentify = Convert.ToBoolean(value);
                                }
                                catch
                                {
                                    Server.s.Log("irc-identify boolean value invalid!" +
                                    "Setting to the default of: " + Server.ircIdentify + ".");
                                }
                                break;
                            case "irc-password":
                                Server.ircPassword = value;
                                break;
                            case "anti-tunnels":
                                Server.antiTunnel = value.ToLower() == "true";
                                break;
                            case "max-depth":
                                try
                                {
                                    Server.maxDepth = Convert.ToByte(value);
                                }
                                catch
                                {
                                    Server.s.Log("maxDepth invalid! setting to default.");
                                }
                                break;
                            case "overload":
                                try
                                {
                                    if (Convert.ToInt16(value) > 5000)
                                    {
                                        value = "5000";
                                        Server.s.Log("Max overload is 5000.");
                                    }
                                    else if (Convert.ToInt16(value) < 500)
                                    {
                                        value = "500";
                                        Server.s.Log("Min overload is 500");
                                    }
                                    Server.Overload = Convert.ToInt16(value);
                                }
                                catch
                                {
                                    Server.s.Log("Overload invalid! setting to default.");
                                }
                                break;
                            case "check-port":
                                Server.checkPortforward = value.ToLower() == "true";
                                break;
                            case "log":
                                Server.reportBack = value.ToLower() == "true";
                                break;
                            case "old-help":
                                Server.oldHelp = value.ToLower() == "true";
                                break;
                            case "backup-time":
                                if (Convert.ToInt32(value) > 1)
                                {
                                    Server.backupInterval = Convert.ToInt32(value);
                                }
                                break;
                            case "afk-minutes":
                                try { Server.afkminutes = Convert.ToInt32(value); }
                                catch
                                {
                                    Server.s.Log("afk-minutes invalid! setting to default.");
                                    Server.afkminutes = 10;
                                }
                                break;

                            case "afk-kick":
                                try { Server.afkkick = Convert.ToInt32(value); }
                                catch
                                {
                                    Server.s.Log("afk-kick invalid! setting to default.");
                                    Server.afkkick = 45;
                                }
                                break;
                            case "usingcli":
                                bool olduse = Server.usingCLI;
                                Server.usingCLI = value.ToLower() == "true";
                                if (olduse != Server.usingCLI)
                                {
                                    WebClient Client = new WebClient();
                                    if (!Server.usingCLI)
                                        Client.DownloadFile("http://www.mcrevive.tk/download/MCRevive_GUI-" + Server.Version + ".exe", "memory/MCRevive.exe");
                                    else
                                        Client.DownloadFile("http://www.mcrevive.tk/download/MCRevive_CLI-" + Server.Version + ".exe", "memory/MCRevive.exe");

                                    Server.Restart();
                                }
                                break;
                            case "anti-grief-system":
                                Server.AGS = value.ToLower() == "true";
                                break;
                            case "op-notify":
                                string[] split = value.ToLower().Split(':');
                                if (split.Length != 4) { Server.s.Log("failed to load: op-notify, setting to default."); break; }
                                Server.opNotify = (split[0].ToLower().Trim() == "true");
                                if (split[1].ToLower().Trim() == "destroyed") Server.MBD = "Destroyed";
                                else if (split[1].ToLower().Trim() == "built") Server.MBD = "Built";
                                else { Server.s.Log("failed to load: op-notify, setting to default."); }
                                try { Server.AGSmultiplier = (int.Parse(split[2].Trim())); }
                                catch { Server.s.Log("failed to load: op-notify, setting to default."); }
                                if (split[3].ToLower().Trim() == "destroyed") Server.MBD2 = "Destroyed";
                                else if (split[3].ToLower().Trim() == "built") Server.MBD2 = "Built";
                                else { Server.s.Log("failed to load: op-notify, setting to default."); break; }
                                break;
                            case "max-blocks":
                                try { Player.spamBlockCount = int.Parse(value); }
                                catch { Server.s.Log("failed to load: max-blocks, setting to default."); }
                                break;
                            case "block-time":
                                try { Player.spamBlockTimer = int.Parse(value); }
                                catch { Server.s.Log("failed to load: block-time, setting to default."); }
                                break;
                            case "extra-ban":
                                Server.extraBan = value.ToLower() == "true";
                                break;
                            case "check-update":
                                Server.updateCheck = value.ToLower() == "true";
                                break;
                            case "check-time":
                                try { Server.updateCheckTime = int.Parse(value); }
                                catch { Server.s.Log("failed to load: check-time, setting to default."); }
                                break;
                            case "force-update":
                                Server.forceUpdate = value.ToLower() == "true";
                                break;
                            case "update-time":
                                try { Server.updateTime = int.Parse(value); }
                                catch { Server.s.Log("failed to load: update-time, setting to default."); }
                                break;
                            case "achievements":
                                Server.allowAchievements = value.ToLower() == "true";
                                break;
                            case "console-state":
                                Server.consolename = (value.Trim() != "") ? value : "God";
                                break;
                            case "restart-on-error":
                                Server.restartOnError = value.ToLower() == "true";
                                break;
                            case "spawn-level":
                                Server.level = value.ToLower();
                                break;
                            case "newlvl-help":
                                Server.newlvlHelp = value.ToLower() == "true";
                                break;
                            case "help-port":
                                Server.helpPort = value.ToLower() == "true";
                                break;
                        }
                    }
                }
                Server.s.SettingsUpdate();
            }
            else
            {
                Save("server");
                Save("gui");
                Save("antigrief");
                Save("update");
                Save("achievements");
            }
        }
        public static bool ValidString(string str, string allowed)
        {
            string allowedchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890" + allowed;
            foreach (char ch in str)
            {
                if (allowedchars.IndexOf(ch) == -1)
                {
                    return false;
                }
            } return true;
        }

        public static void Save(string givenPath)
        {
            string newPath = "properties/" + givenPath + ".properties";
            if (!Directory.Exists("properties"))
                Directory.CreateDirectory("properties");
            if (File.Exists("server.properties"))
            {
                File.Copy("server.properties", "properties/server.properties", true);
                File.Delete("server.properties");
            }
            try
            {
                StreamWriter w = new StreamWriter(File.Create(newPath));
                if (givenPath != "achievements") w.WriteLine("# Edit the settings below to modify how your server operates. This is an explanation of what each setting does.");
                if (givenPath == "server")
                {
                    w.WriteLine("#   the-owner\t\t=\tThe owner of the server (has to be set)");
                    w.WriteLine("#   server-name\t\t=\tThe name which displays on minecraft.net");
                    w.WriteLine("#   port\t\t=\tThe port to operate from");
                    w.WriteLine("#   console-state\t=\tThe name of the console");
                    w.WriteLine("#   motd\t\t=\tThe message which displays when a player connects");
                    w.WriteLine("#   verify-names\t=\tVerify the validity of names");
                    w.WriteLine("#   public\t\t=\tSet to true to appear in the public server list");
                    w.WriteLine("#   max-players\t\t=\tThe maximum number of connections");
                    w.WriteLine("#   main-level\t\t=\tWhat should the name of the servers main level be?");
                    w.WriteLine("#   max-maps\t\t=\tThe maximum number of maps loaded at once");
                    w.WriteLine("#   global-chat\t\t=\tSet to true to enable global chat");
                    w.WriteLine("#   guest-goto\t\t=\tSet to true to give guests goto and levels commands");
                    w.WriteLine("#   restart-on-error\t=\tThis setting determines whether the server should restart on errors, or not");
                    w.WriteLine("#   wom-salt\t\t=\tThis is the salt sent to wom direct");
                    w.WriteLine("#   ");
                    w.WriteLine("#   irc\t\t\t=\tSet to true to enable the IRC bot");
                    w.WriteLine("#   irc-nick\t\t=\tThe name of the IRC bot");
                    w.WriteLine("#   irc-server\t\t=\tThe server to connect to");
                    w.WriteLine("#   irc-channel\t\t=\tThe channel to join");
                    w.WriteLine("#   irc-port\t\t=\tThe port to use to connect");
                    w.WriteLine("#   irc-identify\t=\t(true/false)Do you want the IRC bot to Identify itself with nickserv. Note: You will need to register it's name with nickserv manually.");
                    w.WriteLine("#   irc-password\t=\tThe password you want to use if you're identifying with nickserv");
                    w.WriteLine("#   ");
                    w.WriteLine("#   backup-time\t\t=\tThe number of seconds between automatic backups");
                    w.WriteLine("#   ");
                    w.WriteLine("#   admin-promote\t=\tAllows Admins to promote others to Admins");
                    w.WriteLine("#   op-promote\t\t=\tAllows Operators to promote others to Operators");
                    w.WriteLine("#   afk-minutes\t\t=\tHow many minutes you have to not be moving before you will be auto-set to be -afk-");
                    w.WriteLine("#   afk-kick\t\t=\tHow many minutes you have to be not moving before you will be kicked");
                    w.WriteLine("#   owner-login\t\t=\tTells the server what it should say instead of \"has joined the game\" (empty for default)");
                    w.WriteLine("#   newlvl-help\t\t=\tDetermines wheather /newlvl should help you create your levels, by shrinking/enlarging them");
                    w.WriteLine("#   ");
                    w.WriteLine("#   check-port\t\t=\tShould MCRevive try to check if your ports are forwarded? (if you are able to host)");
                    w.WriteLine("#   help-port\t\t=\tShould MCRevive try to portforward if the ports are not forwarded? (theres no garantee this will work)");
                    w.WriteLine("#   default-color\t=\tSet to \"& + number/letter\" to get that as the default text color (use \"/help color\" in game, to see the options of number/letter)");
                    w.WriteLine("#   geolocator\t\t=\tThis will tell the server weather it should track joining players or not");
                    w.WriteLine("#   log\t\t\t=\tTells the server whether it should save the Log or not");
                    w.WriteLine("#   old-help\t\t=\tDetermines whether the help command should be shown the old, or the new way");
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine("# Server Options:");
                    w.WriteLine("the-owner = " + Server.TheOwner);
                    w.WriteLine("server-name = " + Server.name);
                    w.WriteLine("port = " + Server.port.ToString());
                    w.WriteLine("console-state = " + Server.consolename);
                    w.WriteLine("motd = " + Server.motd);
                    w.WriteLine("verify-names = " + Server.verify.ToString().ToLower());
                    w.WriteLine("public = " + Server.pub.ToString().ToLower());
                    w.WriteLine("max-players = " + Server.players.ToString());
                    w.WriteLine("main-level = " + Server.level);
                    w.WriteLine("max-maps = " + Server.maps.ToString());
                    w.WriteLine("global-chat = " + Server.worldChat.ToString().ToLower());
                    w.WriteLine("guest-goto = " + Server.guestGoto.ToString().ToLower());
                    w.WriteLine("restart-on-error = " + Server.restartOnError);
                    w.WriteLine("wom-salt = " + Server.womSalt);
                    w.WriteLine();
                    w.WriteLine("# IRC Options:");
                    w.WriteLine("irc = " + Server.irc.ToString().ToLower());
                    w.WriteLine("irc-nick = " + Server.ircNick);
                    w.WriteLine("irc-server = " + Server.ircServer);
                    w.WriteLine("irc-channel = " + Server.ircChannel);
                    w.WriteLine("irc-port = " + Server.ircPort.ToString());
                    w.WriteLine("irc-identify = " + Server.ircIdentify.ToString());
                    w.WriteLine("irc-password = " + Server.ircPassword);
                    w.WriteLine();
                    w.WriteLine("# Backup Options:");
                    w.WriteLine("backup-time = 150");
                    w.WriteLine();
                    w.WriteLine("# Player Options:");
                    w.WriteLine("admin-promote = " + Server.adminpromote);
                    w.WriteLine("op-promote = " + Server.oppromote);
                    w.WriteLine("afk-minutes = " + Server.afkminutes);
                    w.WriteLine("afk-kick = " + Server.afkkick);
                    w.WriteLine("owner-login = " + Server.Ownerlogin);
                    w.WriteLine("newlvl-help = " + Server.newlvlHelp);
                    w.WriteLine();
                    w.WriteLine("# Other:");
                    w.WriteLine("check-port = " + Server.checkPortforward);
                    w.WriteLine("help-port = " + Server.helpPort);
                    w.WriteLine("default-color = " + c.Name(Server.DefaultColor));
                    w.WriteLine("geolocator = " + Server.GeoLocation.ToString().ToLower());
                    w.WriteLine("log = " + Server.reportBack);
                    w.WriteLine("old-help = " + Server.oldHelp);
                }
                else if (givenPath == "gui")
                {
                    w.WriteLine("#   usingCLI\t\t=\tTells the program wether you use the CLI or the Gui");
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine("# CLI / Gui?");
                    w.WriteLine("usingCLI = " + Server.usingCLI);
                }
                else if (givenPath == "antigrief")
                {
                    w.WriteLine("#   anti-tunnels\t=\tStops people digging below max-depth");
                    w.WriteLine("#   max-depth\t\t=\tThe maximum allowed depth to dig down");
                    w.WriteLine("#   overload\t\t=\tThe higher this is, the longer the physics is allowed to lag. Default 1500");
                    w.WriteLine("#   ");
                    w.WriteLine("#   anti-grief-system =\tUsing Anti-Grief-System?");
                    w.WriteLine("#   op-notify\t\t=\tTells the server whether it should tell op+ about suspected griefers");
                    w.WriteLine("#   max-blocks\t\t=\tHow many blocks modified on block-time before kick");
                    w.WriteLine("#   block-time\t\t=\tHow many seconds you have to modify max-blocks before kick");
                    w.WriteLine("#   ");
                    w.WriteLine("#   extra-ban\t\t=\tDetermines whether allover bans shall be readen and writen (all MCRevive servers)");
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine("# Antigrief Stuff:");
                    w.WriteLine("anti-tunnels = " + Server.antiTunnel.ToString().ToLower());
                    w.WriteLine("max-depth = " + Server.maxDepth.ToString());
                    w.WriteLine("overload = " + Server.Overload.ToString());
                    w.WriteLine();
                    w.WriteLine("# Anti Grief System:");
                    w.WriteLine("anti-grief-system = " + Server.AGS.ToString());
                    w.WriteLine("op-notify = " + Server.opNotify.ToString() + " : " + Server.MBD + " : " + Server.AGSmultiplier + " : " + Server.MBD2);
                    w.WriteLine("max-blocks = " + Player.spamBlockCount);
                    w.WriteLine("block-time = " + Player.spamBlockTimer);
                    w.WriteLine();
                    w.WriteLine("# Other:");
                    w.WriteLine("extra-ban = " + Server.extraBan.ToString());
                }
                else if (givenPath == "update")
                {
                    w.WriteLine("#   check-update\t=\tTells the server wether it should check for updates or not");
                    w.WriteLine("#   ");
                    w.WriteLine("#   force-update\t=\tDetermines whether the server auto updates as soon as an update is out");
                    w.WriteLine("#   update-time\t\t=\tThe time in seconds before force-update takes action");
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine("# Check?");
                    w.WriteLine("check-update = " + Server.updateCheck);
                    w.WriteLine("check-time = " + Server.updateCheckTime);
                    w.WriteLine();
                    w.WriteLine("# Update Settings:");
                    w.WriteLine("force-update = " + Server.forceUpdate);
                    w.WriteLine("update-time = " + Server.updateTime);
                }
                else if (givenPath == "achievements")
                {
                    w.WriteLine("# This is a list of all the achievements in MCRevive");
                    w.WriteLine("# You may edit the settings below for your own desire");
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine("# Achievements?");
                    w.WriteLine("achievements = " + Server.allowAchievements);
                    w.WriteLine();
                    w.WriteLine("# Achievements:");
                    achTemp.ForEach((ds) =>
                    {
                        w.WriteLine(ds.filename + " = " + achTemp.Find(a => a.filename == ds.filename).allowed);
                    });
                }
                w.Flush();
                w.Close();
                w.Dispose();
            }
            catch
            {
                Server.s.Log("SAVE FAILED! " + newPath);
            }
        }
        public static void ReloadMJS()
        {
            MJS.Script.ReloadMJS();
        }
    }
}