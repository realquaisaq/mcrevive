﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.IO.Compression;
using System.IO;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.ComponentModel;

namespace MCRevive
{
    public sealed class Player
    {
        public static int OutGoneTraffic = 0;
        public static int InComeTraffic = 0;
        public static List<Player> players = new List<Player>();
        public static Dictionary<string, string> left = new Dictionary<string, string>();
        public static List<Player> connections = new List<Player>(Server.players);
        public static List<string> devs = new List<string>();
        public static int number { get { return players.Count; } }
        static System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
        static MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        public struct Achievement { public string name; public bool earned; }
        Thread commThread;
        public static Thread voteThread;

        public static bool storeHelp = false;
        public static string storedHelp = "";

        Socket socket;
        System.Timers.Timer loginTimer = new System.Timers.Timer(1000);                 // 1  second
        public System.Timers.Timer pingTimer = new System.Timers.Timer(2000);           // 2  seconds
        public System.Timers.Timer achievementTimer = new System.Timers.Timer(2000);    // 2  seconds
        public System.Timers.Timer AGStimer = new System.Timers.Timer(1000);            // 1  second
        System.Timers.Timer extraTimer = new System.Timers.Timer(12000);                // 12 seconds
        System.Timers.Timer timeSaver = new System.Timers.Timer(60000);                 // 60 seconds
        public System.Timers.Timer afkTimer = new System.Timers.Timer(2000);            // 2  seconds
        public int afkCount = 0;
        public DateTime afkStart;

        public DateTime ZoneSpam;
        public bool zoneCheck = false;
        public bool zoneDel = false;
        public bool megaBoid = false;
        public bool cmdTimer = false;

        byte[] buffer = new byte[0];
        byte[] tempbuffer = new byte[0xFF];
        public bool disconnected = false;

        public string name;
        public string realname;
        public string ip;
        public byte id;
        /*public int hp;
        public int mana;
        public int pvpLevel;
        public int xp; */
        public ushort[] speedPrevPos = new ushort[3] { 0, 0, 0 };
        public bool ignorePermission = false;
        public bool ignoreGrief = false;
        public string color;
        public string firstLogin = "";
        public string lastLogin = "";
        public int totalLogin;
        public Group group;
        public bool hidden = false;
        public bool opchat = false;
        public bool whisper = false;
        public bool voice = false;
        public string whisperTo = "";
        public bool painting = false;
        public bool speedy = false;

        public bool invincible = false;
        public bool frozen = false;
        public bool muted = false;
        public bool griefed = false;
        public bool joker = false;
        public bool jailed = false;
        public bool aiming = false;
        public bool isFlying = false;
        public bool isZombie = false;
        public bool needsReview = false;
        public bool viewing = false;
        public bool onTrain = false;
        public bool trainGrab = false;
        public bool voteGiven = false;

        public string prefix;
        public string prefixcolor;
        public string title;
        public string titlecolor;

        public DateTime lastDeath;
        public int totalDeaths = 0;
        public int zombieDeaths = 0;
        public long blocksBuild = 0;
        public long blocksGriefed = 0;
        public long totalBlocks = 0;
        public long seasonBlocks = 0;
        public byte BlockAction = 0;  //0-Nothing 1-solid 2-redstone 3-other 4-tnt 5-bigtnt
        public bool deletemode = false;

        public int achievementsEarned = 0;
        public bool checkingAchievements = false;
        public int completedGames = 0;
        public long builtEco = 0;
        public int slapper = 0;
        public int slapped = 0;
        public long capsRage = 0;
        public int spawnedBots = 0;
        public int TNTsExploded = 0;
        public int[] TimeOnServer = { 0, 0 };
        public int flys = 0;

        public ushort modeType = 0;
        public bool propertiesloaded = true;
        public ushort[] bindings = new ushort[Block.Max];
        public Level level = Server.mainLevel;
        public bool Loading = true;     //True if player is loading a map.
        public bool staticCommands = false;
        public string lastCMD = "";
        public static List<string> possibleVotes = new List<string>();
        public static int[] votes = new int[10];
        public static int voteType = 0; //0 = kick,ban,mute | 1 = cmd | 2 = custom, with multi vote | 3 = custom without multivote

        public List<Achievement> Achievements = Achievementsloaded();
        public delegate void BlockchangeEventHandler(Player p, ushort x, ushort y, ushort z, ushort type, byte action);
        public event BlockchangeEventHandler Blockchange = null;
        public void ClearBlockchange() { Blockchange = null; }
        public bool HasBlockchange() { return (Blockchange == null); }
        public object blockchangeObject = null;
        public ushort[] lastClick = new ushort[3] { 0, 0, 0 };

        // Position and rotation
        public ushort[] pos = new ushort[3] { 0, 0, 0 };
        ushort[] oldpos = new ushort[3] { 0, 0, 0 };
        ushort[] basepos = new ushort[3] { 0, 0, 0 };
        public byte[] rot = new byte[2] { 0, 0 };
        byte[] oldrot = new byte[2] { 0, 0 };

        // Anti-speedhack
        public ushort[] pos250msAgo = new ushort[3] { 0, 0, 0 };
        public System.Timers.Timer antiSpeedhackTimer;// = new System.Timers.Timer(1000);
        
        //Copy
        public List<CopyPos> CopyBuffer = new List<CopyPos>();
        public struct CopyPos { public ushort x, y, z; public ushort type; }
        public bool copyAir = false;
        public int[] copyoffset = new int[3] { 0, 0, 0 };
        public ushort[] copystart = new ushort[3] { 0, 0, 0 };

        //Undo
        public struct UndoPos { public ushort x, y, z; public ushort type, newtype; public string mapName; public DateTime timePlaced; }
        public List<UndoPos> UndoBuffer = new List<UndoPos>();
        public List<UndoPos> RedoBuffer = new List<UndoPos>();

        public static int LastLogoff = 0;
        public ushort oldBlock = 0;
        public string storedMessage = "";

        public bool showPortals = false;
        public bool showMBs = false;

        // grief/spam detection
        public static int spamBlockCount = 35;
        public static int spamBlockTimer = 5;
        Queue<DateTime> spamBlockLog = new Queue<DateTime>(spamBlockCount);

        public static int spamChatCount = 3;
        public static int spamChatTimer = 4;
        Queue<DateTime> spamChatLog = new Queue<DateTime>(spamChatCount);

        bool loggedIn = false;
        public Player(Socket s)
        {
            try
            {
                socket = s;
                ip = socket.RemoteEndPoint.ToString().Split(':')[0];
                if (ip != "127.0.0.1" && ip != "204.44.116.176")
                    Server.s.Log(ip + " connected.");

                if (Server.bannedIP.Contains(ip)) { Kick("You're banned!"); return; }
                if (connections.Count >= 10) { Kick("Too many connections!"); return; }

                for (byte i = 0; i < 128; ++i)
                    bindings[i] = i;

                socket.BeginReceive(tempbuffer, 0, tempbuffer.Length, SocketFlags.None, new AsyncCallback(Receive), this);

                loginTimer.Elapsed += delegate
                {
                    if (!Loading)
                    {
                        loginTimer.Stop();
                        if (!File.Exists("text/welcome.txt"))
                        {
                            Server.s.Log("Could not find Welcome.txt. Using default.");
                            File.WriteAllText("text/welcome.txt", "Welcome to my server!");
                        }

                        try
                        {
                            foreach (string w in File.ReadAllLines("text/welcome.txt"))
                                SendMessage(w);

                            if (Server.reviews.Count > 0 && group.Permission >= LevelPermission.Operator)
                            {
                                Thread.Sleep(2000);
                                SendMessage(Group.findPlayerGroup(Server.reviews[0].Split('&')[0]).color + Server.reviews[0].Split('&')[0]
                                    + Server.DefaultColor + " wants an op to review their creation.");
                                SendMessage("Use \"/review " + Server.reviews[0].Split('&')[0] + "\" to go there.");
                            }
                        }
                        catch { }
                        extraTimer.Start();
                    }
                };
                loginTimer.Start();

                pingTimer.Elapsed += delegate { SendPing(); };
                pingTimer.Start();
                achievementTimer.Elapsed += delegate { if (!Loading && Server.allowAchievements) CheckAchievement(); };
                achievementTimer.Start();
                AGStimer.Elapsed += delegate { checkGrief(); };
                AGStimer.Start();
                timeSaver.Elapsed += delegate { if (!Loading) { TimeOnServer[1]++; if (TimeOnServer[1] >= 60) { TimeOnServer[0]++; TimeOnServer[1] -= 60; } } };
                timeSaver.Start();

                afkTimer.Elapsed += delegate
                {
                    try
                    {
                        if (realname == "" || name == "" || group == null) return;
                        if (Server.afkset.Contains(realname))
                        {
                            afkCount = 0;
                            if (Server.afkkick > 0)
                                if (this.group.Permission < LevelPermission.Operator)
                                    if (!ignorePermission)
                                        if (afkStart.AddMinutes(Server.afkkick) < DateTime.Now)
                                            Kick("Auto-kick, AFK for " + Server.afkkick + " minutes");
                            if (oldpos[0] != pos[0] || oldpos[1] != pos[1] || oldpos[2] != pos[2] || oldrot[0] != rot[0] || oldrot[1] != rot[1])
                                Command.all.Find("afk").Use(this, "");
                        }
                        else
                        {
                            if (oldpos[0] == pos[0] && oldpos[1] == pos[1] && oldpos[2] == pos[2] && oldrot[0] == rot[0] && oldrot[1] == rot[1])
                                afkCount++;
                            else
                                afkCount = 0;

                            if (afkCount > Server.afkminutes * 30)
                            {
                                Command.all.Find("afk").Use(this, "auto: Not moved for " + Server.afkminutes + " minutes");
                                afkCount = 0;
                            }
                        }
                    }
                    catch (Exception ex) { Server.ErrorLog(ex); }
                };
                if (Server.afkminutes > 0) afkTimer.Start();

                connections.Add(this);
            }
            catch (Exception e)
            {
                Server.ErrorLog(e);
            }
        }
        #region == PROPERTIES ==
        public void loadSettings()
        {
            try
            {
                string[] lines = File.ReadAllLines("memory/player/save/" + realname + ".txt");
                foreach (string line in lines)
                {
                    if (line != "")
                    {
                        string key = line.Split('=')[0].Trim();
                        string value = line.Split('=')[1].Trim();

                        switch (key.ToLower())
                        {
                            case "totallogins":
                                totalLogin = int.Parse(value);
                                break;
                            case "firstlogin":
                                firstLogin = value;
                                break;
                            case "totaldeaths":
                                totalDeaths = int.Parse(value);
                                break;
                            case "zombiedeaths":
                                zombieDeaths = int.Parse(value);
                                break;
                            case "color":
                                color = (value != "") ? value : group.color;
                                break;
                            case "title":
                                title = value;
                                break;
                            case "titlecolor":
                                titlecolor = value;
                                break;
                            case "buildblocks":
                                blocksBuild = long.Parse(value);
                                break;
                            case "griefedblocks":
                                blocksGriefed = long.Parse(value);
                                break;
                            case "completedgames":
                                completedGames = int.Parse(value);
                                break;
                            case "builteco":
                                builtEco = long.Parse(value);
                                break;
                            case "slapsgiven":
                                slapper = int.Parse(value);
                                break;
                            case "slapstaken":
                                slapped = int.Parse(value);
                                break;
                            case "capsrage":
                                capsRage = long.Parse(value);
                                break;
                            case "spawnedbots":
                                spawnedBots = int.Parse(value);
                                break;
                            case "tntsexploded":
                                TNTsExploded = int.Parse(value);
                                break;
                            case "timeonserver":
                                TimeOnServer[0] = int.Parse(value.Split('&')[0]);
                                TimeOnServer[1] = int.Parse(value.Split('&')[1]);
                                break;
                            case "flys":
                                flys = int.Parse(value);
                                break;
                        }
                    }
                }
                totalBlocks = blocksBuild + blocksGriefed;
                SetPrefix();
                loadAchievements();
                saveSettings();
            }
            catch { if (File.Exists("memory/player/save/" + realname + ".txt")) Server.s.Log("Unable to load player-data!"); saveSettings(); loadSettings(); }
        }

        public void saveSettings()
        {
            try
            {
                if (!Directory.Exists("memory")) Directory.CreateDirectory("memory");
                if (!Directory.Exists("memory/player")) Directory.CreateDirectory("memory/player");
                if (!Directory.Exists("memory/player/save")) Directory.CreateDirectory("memory/player/save");
                StreamWriter SW = new StreamWriter(File.Create("memory/player/save/" + realname + ".txt"));
                SW.WriteLine("IP = " + ip);
                SW.WriteLine("Achievements = " + achievementsEarned);
                SW.WriteLine("FirstLogin = " + ((firstLogin == "") ?
                    DateTime.Now.ToString("yyyy-MM-dd") + Server.DefaultColor + " at &a" + DateTime.Now.ToString("HH:mm:ss") : firstLogin));
                SW.WriteLine("LastLogin = " + lastLogin);
                SW.WriteLine("TotalLogins = " + totalLogin);
                SW.WriteLine("TotalDeaths = " + totalDeaths);
                SW.WriteLine("ZombieDeaths = " + zombieDeaths);
                SW.WriteLine("Color = " + color);
                SW.WriteLine("Title = " + title);
                SW.WriteLine("TitleColor = " + titlecolor);
                SW.WriteLine("BuildBlocks = " + blocksBuild);
                SW.WriteLine("GriefedBlocks = " + blocksGriefed);
                SW.WriteLine("CompletedGames = " + completedGames);
                SW.WriteLine("BuiltEco = " + builtEco);
                SW.WriteLine("SlapsGiven = " + slapper);
                SW.WriteLine("SlapsTaken = " + slapped);
                SW.WriteLine("CapsRage = " + capsRage);
                SW.WriteLine("SpawnedBots = " + spawnedBots);
                SW.WriteLine("TNTsExploded = " + TNTsExploded);
                SW.WriteLine("TimeOnServer = " + TimeOnServer[0] + "&" + TimeOnServer[1]);
                SW.WriteLine("Flys = " + flys);
                SW.Flush();
                SW.Close();
                saveAchievements();
            }
            catch { }
        }

        static List<Achievement> Achievementsloaded()
        {
            List<Achievement> temp = new List<Achievement>();
            Achievement ach = new Achievement();
            ach.earned = false;
            foreach (Server.achievements ds in Server.allAchievements)
            {
                ach.name = ds.realname;
                temp.Add(ach);
            }
            return temp;
        }
        public void loadAchievements()
        {
            if (Server.allowAchievements)
            {
                try
                {
                    if (!File.Exists("memory/player/achievements/" + realname + ".txt")) { saveAchievements(); }
                    string[] lines = File.ReadAllLines("memory/player/achievements/" + realname + ".txt");
                    Achievements = new List<Achievement>();
                    foreach (string line in lines)
                    {
                        if (line != "")
                        {
                            Achievement ach = new Achievement();
                            string key = line.Split('=')[0].Trim();
                            string value = line.Split('=')[1].Trim();

                            ach.name = Server.allAchievements.Find(a => a.filename == key).realname;

                            ach.earned = (value.ToLower() == "true");
                            Achievements.Add(ach);
                        }
                    }
                    saveAchievements();
                }
                catch { Server.s.Log("Unable to load achievement-data!"); saveAchievements(); loadAchievements(); }
            }
        }

        public void saveAchievements()
        {
            if (Server.allowAchievements)
            {
                try
                {
                    if (!Directory.Exists("memory")) Directory.CreateDirectory("memory");
                    if (!Directory.Exists("memory/player")) Directory.CreateDirectory("memory/player");
                    if (!Directory.Exists("memory/player/achievements")) Directory.CreateDirectory("memory/player/achievements");
                    StreamWriter SW = new StreamWriter(File.Create("memory/player/achievements/" + realname + ".txt"));
                    foreach (Server.achievements ds in Server.allAchievements)
                    {
                        SW.WriteLine(ds.filename + " = " + Achievements.Find(a => a.name == ds.realname).earned);
                    }
                    SW.Flush();
                    SW.Close();
                }
                catch { }
            }
        }
        #endregion
        #region == INCOMING ==
        static void Receive(IAsyncResult result)
        {
            Player p = (Player)result.AsyncState;
            if (p.disconnected)
                return;
            try
            {
                int length = p.socket.EndReceive(result);
                if (length == 0) { p.Disconnect(); return; }

                byte[] b = new byte[p.buffer.Length + length];
                Buffer.BlockCopy(p.buffer, 0, b, 0, p.buffer.Length);
                Buffer.BlockCopy(p.tempbuffer, 0, b, p.buffer.Length, length);

                p.buffer = p.HandleMessage(b);
                p.socket.BeginReceive(p.tempbuffer, 0, p.tempbuffer.Length, SocketFlags.None, new AsyncCallback(Receive), p);
            }
            catch (SocketException)
            {
                p.Disconnect();
            }
            catch (Exception e)
            {
                Server.ErrorLog(e);
                p.Kick("Error!");
            }
        }
        byte[] HandleMessage(byte[] buffer)
        {
            InComeTraffic += buffer.Length;
            try
            {
                int length = 0;
                byte msg = buffer[0];

                // Get the length of the message by checking the first byte
                switch (msg)
                {
                    case 0:
                        length = 130;
                        break; // login
                    case 5:
                        if (!loggedIn)
                            goto default;
                        length = 8;
                        break; // blockchange
                    case 8:
                        if (!loggedIn)
                            goto default;
                        length = 9;
                        break; // input
                    case 13:
                        if (!loggedIn)
                            goto default;
                        length = 65;
                        break; // chat
                    default:
                        Kick("Unhandled message id \"" + msg + "\"!");
                        return new byte[0];
                }
                if (buffer.Length > length)
                {
                    byte[] message = new byte[length];
                    Buffer.BlockCopy(buffer, 1, message, 0, length);

                    byte[] tempbuffer = new byte[buffer.Length - length - 1];
                    Buffer.BlockCopy(buffer, length + 1, tempbuffer, 0, buffer.Length - length - 1);

                    buffer = tempbuffer;

                    // Thread thread = null; 
                    switch (msg)
                    {
                        case 0:
                            HandleLogin(message);
                            break;
                        case 5:
                            if (!loggedIn)
                                break;
                            HandleBlockchange(message);
                            break;
                        case 8:
                            if (!loggedIn)
                                break;
                            HandleInput(message);
                            break;
                        case 13:
                            if (!loggedIn)
                                break;
                            HandleChat(message);
                            break;
                    }
                    //thread.Start((object)message);
                    if (buffer.Length > 0)
                        buffer = HandleMessage(buffer);
                    else
                        return new byte[0];
                }
            }
            catch (Exception e)
            {
                Server.ErrorLog(e);
            }
            return buffer;
        }
        void HandleLogin(byte[] message)
        {
            try
            {
                //byte[] message = (byte[])m;
                if (loggedIn)
                    return;

                byte version = message[0];
                name = enc.GetString(message, 1, 64).Trim();
                group = Group.findPlayerGroup(name);
                string verify = enc.GetString(message, 65, 32).Trim();
                byte type = message[129];

                if (name == Server.TheOwner || checkDevS(name)) { ignorePermission = true; ignoreGrief = true; }
                if (group == Group.FindPerm(LevelPermission.Banned) && !ignorePermission)
                {
                    Kick("You're banned!");
                    return;
                }
                if (Player.players.Count >= Server.players && !ignorePermission) { Kick("Server full!"); return; }
                if (version != Server.version) { Kick("Wrong version!"); return; }
                if (name.Length > 16 || !ValidName(name)) { Kick("Illegal name!"); return; }

                if (Server.verify)
                {
                    if (verify == "--" || verify != BitConverter.ToString(md5.ComputeHash(enc.GetBytes(Server.salt + name))).Replace("-", "").ToLower().TrimStart('0'))
                        if (ip != "127.0.0.1" && !ip.StartsWith("192.168.") && !ignorePermission)
                        {
                            Kick("Login failed! Try again.");
                            return;
                        }
                }
                Server.s.Log(ip + " logging in as " + name + ".");
                realname = name;
                foreach (Player p in players)
                {
                    if (p.name == name)
                    {
                        if (Server.verify) { p.Kick("Someone logged in as you!"); break; }
                        else { Kick("Already logged in!"); return; }
                    }
                }
                try { left.Remove(name.ToLower()); }
                catch { }

                SendMotd();
                SendMap();
                Loading = true;

                if (disconnected) return;

                loggedIn = true;
                id = FreeId();

                players.Add(this);
                connections.Remove(this);

                Server.s.PlayerListUpdate();
                level.ListPlayers.Add(this.name);
                if (Directory.Exists("text/titles")) Directory.Delete("text/titles", true);
                if (Directory.Exists("memory/titles")) Directory.Delete("memory/titles", true);
                lastLogin = DateTime.Now.ToString("yyyy-MM-dd") + Server.DefaultColor + " at &a" + DateTime.Now.ToString("HH:mm:ss");
                lastDeath = DateTime.Now;
                prefixcolor = group.color;
                if (group == Group.Find("bots")) { title = "Bot"; titlecolor = "&4"; prefixcolor = "&6"; SetPrefix(); }
                loadSettings();
                totalLogin++;
                if (checkSupporter(name) && title == "") { title = "Sup"; titlecolor = "&b"; SetPrefix(); }

                try
                {
                    ushort x = (ushort)((0.5 + level.spawnx) * 32);
                    ushort y = (ushort)(level.spawny * 32);
                    ushort z = (ushort)((0.5 + level.spawnz) * 32);
                    pos = new ushort[3] { x, y, z }; rot = new byte[2] { level.rotx, level.roty };

                    GlobalSpawn(this, x, y, z, rot[0], rot[1], true);
                    foreach (Player p in players)
                    {
                        if (p.level == level && p != this && !p.hidden)
                            SendSpawn(p.id, p.color + p.name, p.pos[0], p.pos[1], p.pos[2], p.rot[0], p.rot[1]);
                    }
                    foreach (PlayerBot pB in PlayerBot.playerbots)
                    {
                        if (pB.level == level)
                            SendSpawn(pB.id, pB.color + pB.name, pB.pos[0], pB.pos[1], pB.pos[2], pB.rot[0], pB.rot[1]);
                    }
                }
                catch (Exception e)
                {
                    Server.ErrorLog(e);
                    Server.s.Log("Error spawning player \"" + name + "\"");
                }

                bool canGetGeo = true;
                if (Server.GetGeoLocationByIP(ip) == "") canGetGeo = false;

                //////MJS.Player.players.Add(new MJS.Player(name, title, titlecolor, MJS.Map.maps.Find(m => m.name == level.name), MJS.Rank.ranks.Find(r => r.permission == group.Permission)));
                if (MJS.Event.Exists("PlayerJoin")) MJS.Event.Trigger("PlayerJoin", "player $player " + name);

                if (checkDevS(this.name))
                    GlobalChat(this, "&a+ " + prefix + color + name + Server.DefaultColor + ", &4MCRevive developer" + Server.DefaultColor +
                        " appeared!" + ((Server.GeoLocation && canGetGeo) ? " From &3" + Server.GetGeoLocationByIP(ip) : ""), false);
                else if (checkSupporter(name))
                    GlobalChat(this, "&a+ " + prefix + color + name + Server.DefaultColor + ", &3MCRevive supporter" + Server.DefaultColor
                            + " appeared!" + ((Server.GeoLocation && canGetGeo) ? " From &3" + Server.GetGeoLocationByIP(ip) : ""), false);
                else if (Server.TheOwner == this.name)
                    GlobalChat(this, "&a+ " + prefix + color + name + Server.DefaultColor + ", " + Server.Ownerlogin +
                        ((Server.GeoLocation && canGetGeo) ? ((Server.Ownerlogin[Server.Ownerlogin.Length - 1] != '!' &&
                            Server.Ownerlogin[Server.Ownerlogin.Length - 1] != '.') ? " from" : " From") + " &3" + Server.GetGeoLocationByIP(ip) : ""), false);
                else
                    GlobalChat(this, "&a+ " + prefix + color + name + Server.DefaultColor + " joined the game." +
                        ((Server.GeoLocation && canGetGeo) ? " From &3" + Server.GetGeoLocationByIP(ip) : ""), false);
                IRCBot.Say(name + " joined the game.");

                string temp = "Lately known as: ";
                bool found = false;
                if (ip != "127.0.0.1" && !ip.StartsWith("192.168."))
                {
                    foreach (KeyValuePair<string, string> prev in left)
                    {
                        if (prev.Value == ip)
                        {
                            found = true;
                            temp += " " + prev.Key;
                        }
                    }
                    if (found)
                    {
                        GlobalMessageOps(temp);
                        Server.s.Log(temp);
                        IRCBot.Say(temp);
                    }
                }
                Loading = false;
            }
            catch (Exception e)
            {
                Server.ErrorLog(e);
                Player.GlobalMessage("An error occurred: " + e.Message);
            }
        }

        void HandleBlockchange(byte[] message)
        {

            if (group.name == "bots") { return; } //connected bots cant do block changes
            //byte[] message = (byte[])m;
            if (!loggedIn)
                return;
            if (CheckBlockSpam())
                return;

            ushort x = NTHO(message, 0);
            ushort y = NTHO(message, 2);
            ushort z = NTHO(message, 4);
            byte action = message[6];
            byte type = message[7];

            manualChange(x, y, z, action, type);
        }

        public void SetPrefix()
        {
            prefix = (title == "") ? "" : (titlecolor == "") ? prefixcolor + "[" + title +
                prefixcolor + "] " : prefixcolor + "[" + titlecolor + title + prefixcolor + "] ";
        }

        private bool checkOp()
        {
            return group.name != "operator" && group.name != "admin" && group.name != "owner";
        }

        static bool pendingGrief = false;
        public static void checkGrief()
        {
            if (Server.AGS && Server.opNotify && Player.players.Count > 0 && !pendingGrief)
            {
                pendingGrief = true;
                foreach (Player all in players)
                {
                    while (all.Loading) { }
                    if (!all.griefed)
                    {
                        long b = all.blocksBuild;
                        long g = all.blocksGriefed;
                    go:
                        if (Server.MBD == "Destroyed")
                        {
                        retry:
                            if (Server.MBD2 == "Built")
                            {
                                if (b < 1) b = 1;
                                if (g > b * (long)Server.AGSmultiplier)
                                {
                                    GlobalMessageOps(all.color + all.name + Server.DefaultColor + " is a suspected griefer!");
                                    GlobalMessageOps("use &a/blocks " + all.name + Server.DefaultColor + " to check.");
                                    Server.s.Log(all.name + " is a suspected griefer!");
                                    all.griefed = true;
                                }
                            }
                            else { Server.MBD2 = "Built"; goto retry; }
                        }
                        else if (Server.MBD == "Built")
                        {
                        retry:
                            if (Server.MBD2 == "Destroyed")
                            {
                                if (g < 1) g = 1;
                                if (b > g * (long)Server.AGSmultiplier)
                                {
                                    GlobalMessageOps(all.color + all.name + Server.DefaultColor + " is a suspected griefer!");
                                    GlobalMessageOps("use &a/blocks " + all.name + Server.DefaultColor + " to check.");
                                    Server.s.Log(all.name + " is a suspected griefer!");
                                    all.griefed = true;
                                }
                            }
                            else { Server.MBD2 = "Destroyed"; goto retry; }
                        }
                        else { Server.MBD = "Destroyed"; goto go; }
                    }
                }
                pendingGrief = false;
            }
        }

        public void manualChange(ushort x, ushort y, ushort z, byte action, ushort type)
        {
            int section = 0;
            try
            {
                ushort b = level.GetTile(x, y, z);
                if (type > 49)
                {
                    Kick("Unknown block type!");
                    return;
                }
                section++;
                if (jailed || group.name.ToLower() == "banned") { SendBlockchange(x, y, z, b); return; }

                section++;
                lastClick[0] = x;
                lastClick[1] = y;
                lastClick[2] = z;

                section++;
                if (level.permissionbuild == LevelPermission.Null) level.permissionbuild = LevelPermission.Guest;
                if (level.permissionvisit == LevelPermission.Null) level.permissionvisit = LevelPermission.Guest;
                if (group.Permission < level.permissionbuild && !ignorePermission)
                {
                    SendMessage("Your not allowed to edit this map.");
                    SendBlockchange(x, y, z, b);
                    return;
                }
                
                section++;
                if (Blockchange != null)
                {
                    Blockchange(this, x, y, z, type, action);
                    return;
                }

                section++;
                if (group.Permission <= LevelPermission.Guest && !ignorePermission)
                {
                    int Diff = 0;

                    Diff = Math.Abs((int)(pos[0] / 32) - x);
                    Diff += Math.Abs((int)(pos[1] / 32) - y);
                    Diff += Math.Abs((int)(pos[2] / 32) - z);

                    if (Diff > 12)
                    {
                        Server.s.Log(name + " attempted to build with a " + Diff.ToString() + " distance offset");
                        GlobalMessageOps("To Ops &f-" + color + name + "&f- attempted to build with a " + Diff.ToString() + " distance offset");
                        SendMessage("You can't build that far away.");
                        SendBlockchange(x, y, z, b); return;
                    }

                    if (Server.antiTunnel)
                    {
                        if (y < level.depth / 2 - Server.maxDepth && !ignorePermission)
                        {
                            SendMessage("You're not allowed to build this far down!");
                            SendBlockchange(x, y, z, b); return;
                        }
                    }
                }

                section++;
                if (!Block.canPlace(this, type))
                {
                    SendMessage("You can't place this block type!");
                    SendBlockchange(x, y, z, b);
                    return;
                }

                section++;
                if (b >= 200 && b < 220)
                {
                    SendMessage("Block is active, you cant disturb it!");
                    SendBlockchange(x, y, z, b);
                    return;
                }

                section++;
                if (action > 1) { Kick("Unknown block action!"); }

                section++;
                int oldType = type;
                type = bindings[type];
                //Ignores updating blocks that are the same and send block only to the player
                if (b == (painting || action == 1 ? type : 0))
                {
                    if (painting || oldType != type) { SendBlockchange(x, y, z, b); } return;
                }

                section++;
                Memory.AddBlock(level, x, y, z, !painting && action == 0, name);

                if (!painting && action == 0)
                    deleteBlock(b, type, x, y, z);
                else
                    placeBlock(b, type, x, y, z);
            }
            catch (Exception e)
            {
                Server.s.Log(name + " has triggered a block change error");
                GlobalMessageOps(name + " has triggered a block change error");
                IRCBot.Say(name + " has triggered a block change error");
                Server.ErrorLog(e); Player.GlobalMessage("An error occurred in section " + section + " : " + e.Message);
            }
        }
        public void HandlePortal(Player p, ushort x, ushort y, ushort z, ushort b)
        {
            if (!Block.Portal(b)) return;
            try
            {
                Level.PortalPos pP = Memory.LoadPortal(level.name, level.PosToInt(x, y, z));
                if (pP == null)
                    return;

                if (level.name != pP.end)
                {
                    ignorePermission = true;
                    Level thisLevel = level;
                    Command.all.Find("goto").Use(this, pP.end);
                    while (Loading) { }
                    if (thisLevel == level) { p.SendMessage("The map the portal goes to doesn't exist"); return; }
                    if (name != Server.TheOwner && !checkDevS(name)) ignorePermission = false;
                }
                else
                    SendBlockchange(x, y, z, b);

                ushort xx, yy, zz;
                level.IntToPos(pP.pos2, out xx, out yy, out zz);

                Command.all.Find("move").Use(this, this.name + " " + xx + " " + yy + " " + zz);
            }
            catch (Exception ex) { Server.ErrorLog(ex); p.SendMessage("Portal had no exit."); return; }
        }

        public void HandleDeath(ushort b, string customMessage, bool explode = false)
        {
            if (lastDeath.AddSeconds(2) < DateTime.Now)
            {
                lastDeath = DateTime.Now;
                if (invincible) return;

                ushort x = (ushort)(pos[0] / 32);
                ushort y = (ushort)(pos[1] / 32);
                ushort z = (ushort)(pos[2] / 32);

                switch (b)
                {
                    case Block.Zero:
                        if (PlayerBot.AdvFind(customMessage).isZombie)
                        {
                            if (!this.isZombie && this.level.zombiemod)
                            {
                                Command.all.Find("nick").Use(this, "#" + name + " SuperZombie");
                                GlobalMessageLevel(level, color + name + Server.DefaultColor + " is now a zombie!");
                                isZombie = true;
                                SendMessage("You are now a zombie! Kill other people by walking into them");
                            }
                            else if (this.isZombie && this.level.zm_respawn && this.level.zombiemod)
                            {
                                SendMessage("You were killed, you will respawn again in 10 seconds");
                                Command.all.Find("move").Use(this, "0 -2 0");
                                Thread t = new Thread(new ThreadStart(delegate
                                {
                                    Thread.Sleep(10000);
                                    Command.all.Find("spawn").Use(this, "");
                                }));
                                t.Start();
                            }
                            else if (this.isZombie && this.level.zombiemod)
                            {
                                SendMessage("You are dead now. You can run after the other players, or go to another map");
                                GlobalDie(this, true);
                            }
                            else
                            {
                                GlobalMessageLevel(level, color + name + Server.DefaultColor + " has been killed by a zombie!");
                                Command.all.Find("spawn").Use(this, "");
                            }
                            zombieDeaths++;
                        }
                        else if (PlayerBot.AdvFind(customMessage).isCreeper)
                        {
                            level.Blockchange(null, pos[0], pos[1], pos[2], Block.tnt, true);
                            if (explode) level.MakeExplosion(x, y, z, 1, 1);
                            GlobalMessageLevel(level, color + name + Server.DefaultColor + " has been blown up by a creeper!");
                            Command.all.Find("spawn").Use(this, "");
                        }
                        else
                        {
                            Command.all.Find("spawn").Use(this, "");
                            GlobalMessageLevel(level, color + name + Server.DefaultColor + " was killed by " + customMessage);
                        }
                        break;
                    case Block.deathair: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + " walked into &cnerve gas" + Server.DefaultColor + " and suffocated."); break;
                    case Block.deathwater:
                    case Block.activedeathwater: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + " stepped in &dcold water" + Server.DefaultColor + " and froze."); break;
                    case Block.deathlava:
                    case Block.activedeathlava: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + " stood in &cmagma" + Server.DefaultColor + " and melted."); break;
                    case Block.magma: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + " was hit by &cflowing magma" + Server.DefaultColor + " and melted."); break;
                    case Block.birdkill: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + " was hit by a &cphoenix" + Server.DefaultColor + " and burnt."); break;
                    case Block.fishshark: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + " was eaten by a &cshark" + Server.DefaultColor + "."); break;
                    case Block.fire: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + " burnt to a &ccrisp" + Server.DefaultColor + "."); break;
                    case Block.rockethead: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + " was hit by a &cmissile" + Server.DefaultColor + "."); break;
                    case Block.fishlavashark: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + " was eaten by a ... &cLAVA SHARK" + Server.DefaultColor + "?!"); break;
                    case Block.train: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + " was hit by a &ctrain" + Server.DefaultColor + "."); break;
                    case Block.stone: GlobalMessageLevel(this.level, this.color + this.name + Server.DefaultColor + customMessage); break;
                }
                if (explode) level.MakeExplosion(x, y, z, b == Block.rockethead ? 0 : 1, 1);
                Command.all.Find("spawn").Use(this, "");
            }
        }

        private void deleteBlock(ushort b, ushort type, ushort x, ushort y, ushort z)
        {
            if (Block.Portal(b)) { HandlePortal(this, x, y, z, b); return; }
            if (deletemode) { level.Blockchange(this, x, y, z, 0, false, false, false); return; }

            switch (b)
            {
                case Block.door:
                case Block.door2:
                case Block.door3:
                case Block.door4:
                case Block.door5:
                case Block.door6:
                case Block.door7:
                case Block.door8:
                case Block.door9:
                case Block.door10:
                    if (level.physics != 0)
                        level.Blockchange(this, x, y, z, Block.DoorAirs(b), true);
                    else
                        SendBlockchange(x, y, z, b);
                    break;
                case Block.door_air:
                case Block.door2_air:
                case Block.door3_air:
                case Block.door4_air:
                case Block.door5_air:
                case Block.door6_air:
                case Block.door7_air:
                case Block.door8_air:
                case Block.door9_air:
                case Block.door10_air:
                    break;
                case Block.rocketstart:
                    if (level.physics < 2)
                    {
                        SendBlockchange(x, y, z, b);
                    }
                    else
                    {
                        int newZ = 0, newX = 0, newY = 0;

                        SendBlockchange(x, y, z, Block.rocketstart);
                        if (rot[0] < 48 || rot[0] > (256 - 48))
                            newZ = -1;
                        else if (rot[0] > (128 - 48) && rot[0] < (128 + 48))
                            newZ = 1;

                        if (rot[0] > (64 - 48) && rot[0] < (64 + 48))
                            newX = 1;
                        else if (rot[0] > (192 - 48) && rot[0] < (192 + 48))
                            newX = -1;

                        if (rot[1] >= 192 && rot[1] <= (192 + 32))
                            newY = 1;
                        else if (rot[1] <= 64 && rot[1] >= 32)
                            newY = -1;

                        if (192 <= rot[1] && rot[1] <= 196 || 60 <= rot[1] && rot[1] <= 64) { newX = 0; newZ = 0; }

                        level.PhysBlockchange((ushort)(x + newX * 2), (ushort)(y + newY * 2), (ushort)(z + newZ * 2), Block.rockethead);
                        level.PhysBlockchange((ushort)(x + newX), (ushort)(y + newY), (ushort)(z + newZ), Block.fire);
                    }
                    break;
                case Block.firework:
                    if (level.physics != 0 && level.GetTile(x, (ushort)(y + 3), z) != Block.firework && level.GetTile(x, (ushort)(y + 2), z) != Block.lavastill)
                    {
                        level.PhysBlockchange(x, (ushort)(y + 2), z, Block.firework);
                        level.PhysBlockchange(x, (ushort)(y + 1), z, Block.lavastill);
                    }
                    SendBlockchange(x, y, z, b);

                    break;
                default:
                    level.Blockchange(this, x, y, z, Block.air, false);
                    break;
            }
        }

        private void placeBlock(ushort b, ushort type, ushort x, ushort y, ushort z)
        {
            switch (BlockAction)
            {
                case 0:     //normal
                    if (level.physics == 0)
                    {
                        switch (type)
                        {
                            case Block.dirt: //instant dirt to grass
                                level.Blockchange(this, x, y, z, Block.grass, true);
                                break;
                            case Block.staircasestep:    //stair handler
                                if (level.GetTile(x, (ushort)(y - 1), z) == Block.staircasestep)
                                {
                                    SendBlockchange(x, y, z, Block.air);    //send the air block back only to the user.
                                    level.Blockchange(this, x, (ushort)(y - 1), z, Block.staircasefull, true);
                                    break;
                                }
                                level.Blockchange(this, x, y, z, type, type != Block.air);
                                break;
                            default:
                                level.Blockchange(this, x, y, z, type, type != Block.air);
                                break;
                        }
                    }
                    else
                        level.Blockchange(this, x, y, z, type, type != Block.air);

                    if (!Block.LightPass(type))
                    {
                        if (level.GetTile(x, (ushort)(y - 1), z) == Block.grass)
                        {
                            level.PhysBlockchange(x, (ushort)(y - 1), z, Block.dirt);
                        }
                    }

                    break;
                case 1:     //solid
                    if (b == Block.blackrock) { SendBlockchange(x, y, z, b); return; }
                    level.Blockchange(this, x, y, z, Block.blackrock, true);
                    break;
                case 3:     //other
                    if (b == modeType) { SendBlockchange(x, y, z, b); return; }
                    level.Blockchange(this, x, y, z, type, type != Block.air);
                    break;
                case 4:    //Small TNT
                    level.Blockchange(this, x, y, z, Block.smalltnt, true);
                    break;
                case 5:    //Small TNT
                    level.Blockchange(this, x, y, z, Block.bigtnt, true);
                    break;
                default:
                    Server.s.Log(name + " is breaking something");
                    BlockAction = 0;
                    break;
            }
        }

        public void CheckBlock(ushort x, ushort y, ushort z)
        {
            y = (ushort)Math.Round((decimal)(((y * 32) + 4) / 32));

            ushort b = this.level.GetTile(x, y, z);
            ushort b1 = this.level.GetTile(x, (ushort)((int)y - 1), z);

            if (Block.Mover(b) || Block.Mover(b1))
            {
                if (Block.DoorAirs(b) != 0)
                    level.PhysBlockchange(x, y, z, Block.DoorAirs(b));
                if (Block.DoorAirs(b1) != 0)
                    level.PhysBlockchange(x, (ushort)(y - 1), z, Block.DoorAirs(b1));

                if ((x + y + z) != oldBlock)
                    if (b == Block.air_portal || b == Block.water_portal || b == Block.lava_portal)
                        HandlePortal(this, x, y, z, b);
                    else if (b1 == Block.air_portal || b1 == Block.water_portal || b1 == Block.lava_portal)
                        HandlePortal(this, x, (ushort)((int)y - 1), z, b1);
            }
            if (Block.Death(b)) HandleDeath(b, "");
            else if (Block.Death(b1)) HandleDeath(b1, "");
        }

        void HandleInput(object m)
        {
            if (!loggedIn || trainGrab || frozen)
                return;

            byte[] message = (byte[])m;
            if (!loggedIn)
                return;

            byte thisid = message[0];
            ushort x = NTHO(message, 1);
            ushort y = NTHO(message, 3);
            ushort z = NTHO(message, 5);
            byte rotx = message[7];
            byte roty = message[8];
            pos = new ushort[3] { x, y, z };
            rot = new byte[2] { rotx, roty };
            if (!speedy) return; // This is a few clock pulses faster than an if statement around the speedy stuff :)
            if (level.antispeedhack) { speedy = false; SendMessage("You went to an anti-speedhack map. Speedy is now disabled."); return; } // Yeah.
            if (pos[0] == 0 && pos[1] == 0 && pos[2] == 0) { speedPrevPos = pos; return; }
            pos[0] = Convert.ToUInt16(2 * pos[0] - speedPrevPos[0]);
            pos[1] = Convert.ToUInt16(2 * pos[1] - speedPrevPos[1]);
            pos[2] = Convert.ToUInt16(2 * pos[2] - speedPrevPos[2]);
            speedPrevPos = pos;
            SendPos(pos[0], pos[1], pos[2], rot[0], rot[1]);
        }
        void HandleChat(byte[] message)
        {
            try
            {
                if (!loggedIn)
                    return;

                //byte[] message = (byte[])m;
                string text = enc.GetString(message, 1, 64).Trim();
            again:
                if (storedMessage != "")
                {
                    if (!text.EndsWith(">") && !text.EndsWith("<"))
                    {
                        text = storedMessage.Replace("|>|", " ").Replace("|<|", "") + text;
                        storedMessage = "";
                    }
                }
                if (text.EndsWith(">"))
                {
                    storedMessage += text.Remove(text.Length - 1) + "|>|";
                    SendMessage("Message appended!");
                    return;
                }
                else if (text.EndsWith("<"))
                {
                    storedMessage += text.Remove(text.Length - 1) + "|<|";
                    SendMessage("Message appended!");
                    return;
                }

                text = Regex.Replace(text, @"\s\s+", " ");
                if (text.Length == 0)
                    return;
                for (int ch = 0; ch < text.Length; ++ch)
                {
                    if (text[ch] < 32 || text[ch] >= 127 || (text[ch] == '&' && ch != 0))
                    {
                        Kick("Illegal character in chat message!");
                        return;
                    }
                }
                afkCount = 0;
                if (text != "/afk")
                    if (Server.afkset.Contains(name))
                        Command.all.Find("afk").Use(this, "");

                if (text[0] == '/')
                {
                    text = text.Remove(0, 1);

                    int pos = text.IndexOf(' ');
                    if (pos == -1)
                    {
                        HandleCommand(text.ToLower(), "");
                        return;
                    }
                    string cmd = text.Substring(0, pos).ToLower();
                    string msg = text.Substring(pos + 1);
                    HandleCommand(cmd, msg);
                    return;
                }
                if (text[0] == '!')
                {
                    if (voteThread.IsAlive)
                    {
                        if (text.IndexOf(',') == -1) text += ",";
                        string[] Votes = text.Substring(1).Trim().Replace(", ", ",").Replace(" ,", ",").Split(',');
                        if (voteType != 2 && Votes[1] != "")
                        {
                            SendMessage("You can only vote once!");
                            return;
                        }
                        foreach (string who in Votes)
                            if (who != "")
                                if (!voteGiven || voteType == 2)
                                if (possibleVotes.Contains(who))
                                {
                                    voteGiven = true;
                                    votes[possibleVotes.IndexOf(who)]++;
                                    SendMessage("One more vote for: " + who);
                                }
                                else
                                {
                                    try
                                    {
                                        if (possibleVotes[int.Parse(who) - 1] != null)
                                        {
                                            voteGiven = true;
                                            votes[int.Parse(who) - 1]++;
                                            SendMessage("One more vote for: " + possibleVotes[int.Parse(who) - 1]);
                                        }
                                    }
                                    catch
                                    {
                                        SendMessage(who + " is not a valid answer.");
                                    }
                                }
                    }
                    else if (BlockAction == 2)
                        try
                        {
                            ushort delay = ushort.Parse(text.Substring(1));
                            if (delay > 4 || delay < 1) { SendMessage("delay can only be between 0 and 5."); return; }
                            modeType = delay;
                            SendMessage("redstone repeaters now have the delay of " + delay + ".");
                        }
                        catch
                        {
                            SendMessage("There is no vote running.");
                        }
                    else
                        SendMessage("There is no vote running.");
                    return;
                }
                if (MJS.Event.Exists("PlayerChat")) { MJS.Event.Trigger("PlayerChat", "player $player " + name + " string $message " + text.Replace(" ", "[space]")); return; }
                if (Server.chatmod && !this.voice) { this.SendMessage("Chat moderation is on, you cannot speak."); return; }
                if (muted) { this.SendMessage("You cannot speak, you are muted"); return; }
                if (text[0] == '@' || whisper)
                {
                    string newtext = text.Remove(0, 1).Trim();

                    if (whisperTo == "")
                    {
                        int pos = newtext.IndexOf(' ');
                        if (pos != -1)
                        {
                            string to = newtext.Substring(0, pos);
                            string msg = newtext.Substring(pos + 1);
                            HandleQuery(to, msg); return;
                        }
                        else
                        {
                            SendMessage("No message entered");
                            return;
                        }
                    }
                    else
                    {
                        HandleQuery(whisperTo, newtext);
                        return;
                    }
                }
                if (text[0] == '#' || opchat)
                {
                    string newtext = (text[0] == '#') ? text.Remove(0, 1).Trim() : text.Trim();

                    GlobalMessageOps("To Ops &f-" + prefix + color + name + "&f- " + newtext);
                    if (group.name != "operator" && group.name != "admin" && group.name != "owner")
                        SendMessage("To Ops &f-" + prefix + color + name + "&f- " + newtext);
                    Server.s.Log("(OPs): " + name + ": " + newtext);
                    return;
                }
                if (this.joker)
                {
                    if (File.Exists("text/joker.txt"))
                    {
                        Server.s.Log("<JOKER>: " + this.name + ": " + text);
                        Player.GlobalMessageOps(Server.DefaultColor + "<&aJ&bO&cK&5E&9R" + Server.DefaultColor +
                            ">: " + this.color + this.name + ":&f " + text);
                        FileInfo jokertxt = new FileInfo("text/joker.txt");
                        StreamReader stRead = jokertxt.OpenText();
                        List<string> lines = new List<string>();
                        Random rnd = new Random();
                        int i = 0;

                        while (!(stRead.Peek() == -1))
                            lines.Add(stRead.ReadLine());

                        i = rnd.Next(lines.Count);

                        stRead.Close();
                        stRead.Dispose();
                        text = lines[i];
                    }
                    else { File.WriteAllText("text/joker.txt", "Sup"); }

                }
                if (text[0] == '%')
                {
                    bool color = false;
                    if (text[1] == 'r') color = true;
                    for (int i = 0; i <= 9; i++) if (text[1] == char.Parse(i.ToString())) color = true;
                    for (char ch = 'a'; ch <= 'f'; ch++) if (text[1] == ch) color = true;
                    if (color) { text = "&" + text.Substring(1); goto again; }
                    string newtext = text.Remove(0, 1).Trim();
                    if (!Server.worldChat)
                        GlobalChatWorld(this, newtext, true);
                    else
                        GlobalChat(this, newtext);
                    Server.s.Log("<" + name + "> " + newtext);
                    IRCBot.Say("<" + name + "> " + newtext);
                    return;
                }
                Server.s.Log("<" + name + "> " + text);

                for (char ch = 'A'; ch <= 'Z'; ch++)
                    foreach (char cha in text)
                        if (cha == ch) capsRage++;

                if (Server.worldChat)
                {
                    GlobalChat(this, text);
                }
                else
                {
                    GlobalChatLevel(this, text, true);
                }

                IRCBot.Say(name + ": " + text);
            }
            catch (Exception e) { Server.ErrorLog(e); Player.GlobalMessage("An error occurred: " + e.Message); }
        }
        public void HandleCommand(string cmd, string message)
        {
            try
            {
                if (cmd == "") { SendMessage("No command entered."); return; }
                if (jailed) { SendMessage("You cannot use any commands while jailed."); return; }
                // First check if command exists in the mjs folder, so people can rescript core commands
                if (File.Exists("mjs/commands/" + cmd.ToLower() + ".mjs"))
                {
                    Server.s.MJSLog("MJS script commands/" + cmd.ToLower() + ".mjs was triggered by " + name);
                    MJS.Script.Execute("mjs/commands/" + cmd.ToLower() + ".mjs", "player $player " + name + " string $message " + message);
                    return;
                }
                string foundShortcut = Command.all.FindShort(cmd);
                if (foundShortcut != "") cmd = foundShortcut;
                Command command = Command.all.Find(cmd, this);
                if (command != null)
                {
                    if (group.CanExecute(command) || ignorePermission)
                    {
                        if (command.name != "repeat") lastCMD = cmd + " " + message;
                        if (this.joker == true || this.muted == true)
                        {
                            if (command.name == "me")
                            {
                                SendMessage("Cannot use /me while muted or jokered.");
                                return;
                            }
                        }
                        Server.s.CmdLog(name + " uses /" + cmd + " " + message);
                        this.commThread = new Thread(new ThreadStart(delegate
                        {
                            try
                            {
                                command.Use(this, message);
                            }
                            catch (Exception e)
                            {
                                Server.ErrorLog(e);
                                Player.SendMessage(this, "An error occured when using the command!");
                            }
                        }));
                        commThread.Start();
                    }
                    else { SendMessage("You are not allowed to use \"" + cmd + "\"!"); }
                }
                else if (Block.Number(cmd.ToLower()) != Block.Zero)
                {
                    HandleCommand("mode", cmd.ToLower());
                }
                else
                {
                    bool retry = true;

                    //extra shortcuts
                    if (cmd == "addbot") { cmd = "botadd"; }
                    else if (cmd == "removebot") { cmd = "botremove"; }
                    else if (cmd == "setbot") { cmd = "botset"; }
                    else if (cmd == "summonbot") { cmd = "botsummon"; }
                    else if (cmd == "ipban") { cmd = "banip"; }
                    else if (cmd == "cut") { cmd = "copy"; message = "cut"; }
                    else if (cmd == "circle") { cmd = "spheroid"; message = "circle"; }
                    else if (cmd == "portals") { cmd = "portal"; }
                    else if (cmd == "viewranks") { cmd = "viewrank"; }
                    else if (cmd == "zm") { cmd = "megaboid"; }
                    else if (cmd == "promote") { cmd = "setrank"; }
                    else if (cmd == "megacuboid") { cmd = "megaboid"; }
                    else if (cmd == "printimage") { cmd = "imageprint"; }

                    //other softwares
                    else if (cmd.Equals("bhb") || cmd.Equals("hbox")) { cmd = "cuboid"; message = "hollow"; }
                    else if (cmd.Equals("blb") || cmd.Equals("box")) { cmd = "cuboid"; }
                    else if (cmd.Equals("sphere")) { cmd = "spheroid"; }
                    else if (cmd.Equals("cmdlist") || cmd.Equals("cmds") || cmd.Equals("cmdhelp")) { cmd = "help"; }
                    else if (cmd.Equals("worlds") || cmd.Equals("maps")) { cmd = "levels"; }
                    else if (cmd.Equals("mapsave") || cmd.Equals("lvlsave")) { cmd = "save"; }
                    else if (cmd.Equals("mapload") || cmd.Equals("lvlload")) { cmd = "load"; }
                    else if (cmd.Equals("materials")) { cmd = "blocks"; message = ""; }

                    //groups
                    else if (cmd.EndsWith("s"))
                    {
                        foreach (Group grp in Group.GroupList)
                        {
                            if (grp.name.Contains(cmd.Remove(cmd.Length - 1))) { cmd = "viewrank"; message = grp.name; }
                        }
                        if (cmd != "viewrank") retry = false;
                    }
                    else retry = false;

                    if (retry) HandleCommand(cmd, message);
                    else SendMessage("Unknown command \"" + cmd + "\"!");
                }
            }
            catch (Exception e) { Server.ErrorLog(e); SendMessage("Command failed."); }
        }
        void HandleQuery(string to, string message)
        {
            Player p = Find(to);
            if (p == this) { SendMessage("Trying to talk to yourself, huh?"); return; }
            if (p != null)
            {
                Server.s.Log(name + " @" + p.name + ": " + message);
                p.SendChat(this, Server.DefaultColor + "[<] " + p.prefix + p.color + p.name + ": &f" + message);
                SendChat(p, "&9[>] " + this.prefix + this.color + this.name + ": &f" + message);
            }
            else { SendMessage("Player \"" + to + "\" doesn't exist!"); }
        }
        #endregion
        #region == OUTGOING ==
        public void SendRaw(byte id, byte[] send)
        {
            OutGoneTraffic += send.Length;
            byte[] buffer = new byte[send.Length + 1];
            buffer[0] = id;

            Buffer.BlockCopy(send, 0, buffer, 1, send.Length);

            int tries = 0;

        retry: try
            {
                socket.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, delegate(IAsyncResult result) { }, null);
            }
            catch (SocketException)
            {
                tries++;
                if (tries > 2)
                    Disconnect();
                else goto retry;
            }
        }
        public static void SendMessage(Player p, string message)
        {
            if (p == null)
            {
                message = c.Remove(message);
                if (storeHelp) { storedHelp += message + Environment.NewLine; }
                else { Server.s.Log(message); IRCBot.Say(message); }
                return;
            }
            p.SendMessage(p.id, Server.DefaultColor + message);
        }
        public void SendMessage(string message)
        {
            SendMessage(id, Server.DefaultColor + message);
        }
        public void SendChat(Player p, string message)
        {
            if (p == null) { Server.s.Log(message); return; }
            SendMessage(p, message);
        }
        public void SendMessage(byte id, string message)
        {
            if (ZoneSpam.AddSeconds(2) > DateTime.Now && message.Contains("This zone belongs to ")) return;

            byte[] buffer = new byte[65];
            buffer[0] = id;

            message = c.Convert(message);
            message = message.TrimEnd(' ');

            message = message.Replace("$name", name);
            message = message.Replace("$date", DateTime.Now.ToString("yyyy-MM-dd"));
            message = message.Replace("$time", DateTime.Now.ToString("HH:mm:ss"));
            message = message.Replace("$ip", ip);
            message = message.Replace("$color", color);
            message = message.Replace("$rank", group.name);
            message = message.Replace("$level", level.name);
            message = message.Replace("$server", Server.name);
            message = message.Replace("$motd", Server.motd);
            message = message.Replace("$irc", Server.ircServer + " > " + Server.ircChannel);

            byte[] stored = new byte[1];
            int totalTries = 0;
        retryTag: try
            {
                foreach (string line in Wordwrap(message))
                {
                    string newLine = line;
                    if (newLine.TrimEnd(' ')[newLine.TrimEnd(' ').Length - 1] < '!')
                    {
                        newLine += '\'';
                    }
                    if (newLine == "") continue;
                    StringFormat(newLine, 64).CopyTo(buffer, 1);
                    SendRaw(13, buffer);
                }
            }
            catch (Exception e)
            {
                message = (message.StartsWith("&f") ? "" : "&f") + message;
                totalTries++;
                if (totalTries < 10) goto retryTag;
                else Server.ErrorLog(e);
            }
        }
        public void SendMotd()
        {
            byte[] buffer = new byte[130];
            buffer[0] = (byte)8;
            StringFormat(Server.name, 64).CopyTo(buffer, 1);
            StringFormat(Server.motd, 64).CopyTo(buffer, 65);

            if (Block.canPlace(this, Block.blackrock))
                buffer[129] = 100;
            else
                buffer[129] = 0;

            SendRaw(0, buffer);
        }

        public void SendUserMOTD()
        {
            byte[] buffer = new byte[130];
            Random rand = new Random();
            buffer[0] = Server.version;
            if (level.motd == "ignore") { StringFormat(Server.name, 64).CopyTo(buffer, 1); StringFormat(Server.motd, 64).CopyTo(buffer, 65); }
            else StringFormat(level.motd, 128).CopyTo(buffer, 1);

            if (Block.canPlace(this.group.Permission, Block.blackrock))
                buffer[129] = 100;
            else
                buffer[129] = 0;
            SendRaw(0, buffer);
        }

        public void SendMap()
        {
            SendRaw(2, new byte[0]);
            byte[] buffer = new byte[level.blocks.Length + 4];
            BitConverter.GetBytes(IPAddress.HostToNetworkOrder(level.blocks.Length)).CopyTo(buffer, 0);
            //ushort xx; ushort yy; ushort z;z

            for (int i = 0; i < level.blocks.Length; ++i)
            {
                buffer[4 + i] = Block.Convert(level.blocks[i]);
            }

            buffer = GZip(buffer);
            int number = (int)Math.Ceiling(((double)buffer.Length) / 1024);
            for (int i = 1; buffer.Length > 0; ++i)
            {
                short length = (short)Math.Min(buffer.Length, 1024);
                byte[] send = new byte[1027];
                HTNO(length).CopyTo(send, 0);
                Buffer.BlockCopy(buffer, 0, send, 2, length);
                byte[] tempbuffer = new byte[buffer.Length - length];
                Buffer.BlockCopy(buffer, length, tempbuffer, 0, buffer.Length - length);
                buffer = tempbuffer;
                send[1026] = (byte)(i * 100 / number);
                SendRaw(3, send);
                if (ip == "127.0.0.1") { }
                else if (Server.updateTimer.Interval > 1000) Thread.Sleep(100);
                else Thread.Sleep(10);
            } buffer = new byte[6];
            HTNO((short)level.width).CopyTo(buffer, 0);
            HTNO((short)level.depth).CopyTo(buffer, 2);
            HTNO((short)level.height).CopyTo(buffer, 4);
            SendRaw(4, buffer);
            Loading = false;

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        public void SendSpawn(byte id, string name, ushort x, ushort y, ushort z, byte rotx, byte roty)
        {
            rot = new byte[2] { rotx, roty };
            byte[] buffer = new byte[73]; buffer[0] = id;
            StringFormat(name, 64).CopyTo(buffer, 1);
            HTNO(x).CopyTo(buffer, 65);
            HTNO(y).CopyTo(buffer, 67);
            HTNO(z).CopyTo(buffer, 69);
            buffer[71] = rotx; buffer[72] = roty;
            SendRaw(7, buffer);
            InitAntiSpeedhack();
        }
        public void InitAntiSpeedhack(int timeToStart = 10000)
        {
            try
            {
                pos250msAgo = new ushort[] { 0, 0, 0 };
                try { if (antiSpeedhackTimer.Enabled) antiSpeedhackTimer.Stop(); }
                catch { }
                if (level.antispeedhack)
                {
                    //Server.s.Log("Anti-speedhack for " + name + " is initializing...");
                    System.Timers.Timer AntiSpeedhackStarter = new System.Timers.Timer(timeToStart);
                    AntiSpeedhackStarter.Elapsed += delegate
                    {
                        antiSpeedhackTimer = new System.Timers.Timer(250);
                        antiSpeedhackTimer.Elapsed += delegate
                        {
                            if ((pos250msAgo[0] != 0 || pos250msAgo[1] != 0 || pos250msAgo[2] != 0) && group.Permission < LevelPermission.Operator && !this.ignorePermission)
                            {
                                //double speed = Math.Round(MJS.XMath.Pythagoras(pos1sAgo[0] - pos[0], pos1sAgo[1] - pos[1], pos1sAgo[2] - pos[2]));
                                double speed = Math.Round(Math.Sqrt((pos250msAgo[0] - pos[0]) * (pos250msAgo[0] - pos[0]) + (pos250msAgo[2] - pos[2]) * (pos250msAgo[2] - pos[2])));
                                //if (speed != 0) Server.s.Log(name + ": " + speed + " @ [" + pos[0] + "," + pos[1] + "," + pos[2] + "] [" + pos250msAgo[0] + "," + pos250msAgo[1] + "," + pos250msAgo[2] + "]");
                                if (speed > 87)
                                {
                                    SendPos(pos250msAgo[0], pos250msAgo[1], pos250msAgo[2], rot[0], rot[1]);
                                    SendMessage("Speedhacking is not allowed on this map.");
                                    SendMessage("If you are using the WoM client, &auncheck Enable hacks" + Server.DefaultColor + " at the top of the window.");
                                }
                                else
                                    pos250msAgo = pos;
                            }
                            else
                            {
                                pos250msAgo = pos;
                            }
                        };
                        antiSpeedhackTimer.Start();
                        AntiSpeedhackStarter.Stop();
                        AntiSpeedhackStarter.Dispose();
                    };
                    AntiSpeedhackStarter.Start();
                }
            }
            catch (Exception e)
            {
                Server.s.Log("Error while starting anti-speedhack on player " + name + ": " + e.Message);
            }
        }
        public void SendPos(ushort x, ushort y, ushort z, byte rotx, byte roty)
        {
            byte id = 255;
            if (x < 0) x = 32;
            if (y < 0) y = 32;
            if (z < 0) z = 32;
            if (x > level.width * 32) x = (ushort)(level.width * 32 - 32);
            if (z > level.height * 32) z = (ushort)(level.height * 32 - 32);
            if (x > 32767) x = 32730;
            if (y > 32767) y = 32730;
            if (z > 32767) z = 32730;

            pos[0] = x; pos[1] = y; pos[2] = z;
            rot[0] = rotx; rot[1] = roty;

            byte[] buffer = new byte[9]; buffer[0] = id;
            HTNO(x).CopyTo(buffer, 1);
            HTNO(y).CopyTo(buffer, 3);
            HTNO(z).CopyTo(buffer, 5);
            buffer[7] = rotx; buffer[8] = roty;
            SendRaw(8, buffer);
        }
        //TODO: Figure a way to SendPos without changing rotation
        public void SendDie(byte id) { SendRaw(0x0C, new byte[1] { id }); }
        public void SendBlockchange(ushort x, ushort y, ushort z, ushort type)
        {
            if (x < 0 || y < 0 || z < 0) return;
            if (x >= level.width || y >= level.depth || z >= level.height) return;

            byte[] buffer = new byte[7];
            HTNO(x).CopyTo(buffer, 0);
            HTNO(y).CopyTo(buffer, 2);
            HTNO(z).CopyTo(buffer, 4);
            buffer[6] = Block.Convert(type);
            SendRaw(6, buffer);
        }
        void SendKick(string message) { SendRaw(14, StringFormat(message, 64)); }
        void SendPing() { /*pingDelay = 0; pingDelayTimer.Start();*/ SendRaw(1, new byte[0]); }
        void UpdatePosition()
        {

            //pingDelayTimer.Stop();

            // Shameless copy from JTE's Server
            byte changed = 0;   //Denotes what has changed (x,y,z, rotation-x, rotation-y)
            // 0 = no change - never happens with this code.
            // 1 = position has changed
            // 2 = rotation has changed
            // 3 = position and rotation have changed
            // 4 = Teleport Required (maybe something to do with spawning)
            // 5 = Teleport Required + position has changed
            // 6 = Teleport Required + rotation has changed
            // 7 = Teleport Required + position and rotation has changed
            //NOTE: Players should NOT be teleporting this often. This is probably causing some problems.
            if (oldpos[0] != pos[0] || oldpos[1] != pos[1] || oldpos[2] != pos[2])
                changed |= 1;

            if (oldrot[0] != rot[0] || oldrot[1] != rot[1])
            {
                changed |= 2;
            }
            if (Math.Abs(pos[0] - basepos[0]) > 32 || Math.Abs(pos[1] - basepos[1]) > 32 || Math.Abs(pos[2] - basepos[2]) > 32)
                changed |= 4;

            if ((oldpos[0] == pos[0] && oldpos[1] == pos[1] && oldpos[2] == pos[2]) && (basepos[0] != pos[0] || basepos[1] != pos[1] || basepos[2] != pos[2]))
                changed |= 4;


            byte[] buffer = new byte[0]; byte msg = 0;
            if ((changed & 4) != 0)
            {
                msg = 8; //Player teleport - used for spawning or moving too fast
                buffer = new byte[9]; buffer[0] = id;
                HTNO(pos[0]).CopyTo(buffer, 1);
                HTNO(pos[1]).CopyTo(buffer, 3);
                HTNO(pos[2]).CopyTo(buffer, 5);
                buffer[7] = rot[0]; buffer[8] = rot[1];
            }
            else if (changed == 1)
            {
                try
                {
                    msg = 10; //Position update
                    buffer = new byte[4]; buffer[0] = id;
                    Buffer.BlockCopy(System.BitConverter.GetBytes((sbyte)(pos[0] - oldpos[0])), 0, buffer, 1, 1);
                    Buffer.BlockCopy(System.BitConverter.GetBytes((sbyte)(pos[1] - oldpos[1])), 0, buffer, 2, 1);
                    Buffer.BlockCopy(System.BitConverter.GetBytes((sbyte)(pos[2] - oldpos[2])), 0, buffer, 3, 1);
                }
                catch { }
            }
            else if (changed == 2)
            {
                msg = 11; //Orientation update
                buffer = new byte[3]; buffer[0] = id;
                buffer[1] = rot[0]; buffer[2] = rot[1];
            }
            else if (changed == 3)
            {
                try
                {
                    msg = 9; //Position and orientation update
                    buffer = new byte[6]; buffer[0] = id;
                    Buffer.BlockCopy(System.BitConverter.GetBytes((sbyte)(pos[0] - oldpos[0])), 0, buffer, 1, 1);
                    Buffer.BlockCopy(System.BitConverter.GetBytes((sbyte)(pos[1] - oldpos[1])), 0, buffer, 2, 1);
                    Buffer.BlockCopy(System.BitConverter.GetBytes((sbyte)(pos[2] - oldpos[2])), 0, buffer, 3, 1);
                    buffer[4] = rot[0]; buffer[5] = rot[1];
                }
                catch { }
            }

            oldpos = pos; oldrot = rot;
            if (changed != 0)
                foreach (Player p in players)
                {
                    if (p != this && p.level == level)
                    {
                        p.SendRaw(msg, buffer);
                    }
                }
        }
        #endregion
        #region == GLOBAL MESSAGES ==
        public static void GlobalBlockchange(Level level, ushort x, ushort y, ushort z, ushort type)
        {
            players.ForEach(delegate(Player p) { if (p.level == level) { p.SendBlockchange(x, y, z, type); } });
        }
        public static void GlobalChat(Player from, string message) { GlobalChat(from, message, true); }
        public static void GlobalChat(Player from, string message, bool showname)
        {
            if (showname) { message = from.prefix + from.color + from.name + ": &f" + message; }
            players.ForEach(delegate(Player p) { if (p.level.worldChat) p.SendChat(p, message); });
        }
        public static void GlobalChatLevel(Player from, string message, bool showname)
        {
            if (showname) { message = "<Level>" + from.prefix + from.color + from.name + ": &f" + message; }
            players.ForEach(delegate(Player p) { if (p.level == from.level) p.SendChat(p, Server.DefaultColor + message); });
        }
        public static void GlobalChatWorld(Player from, string message, bool showname)
        {
            if (showname) { message = "<World>" + from.prefix + from.color + from.name + ": &f" + message; }
            players.ForEach(delegate(Player p) { if (p.level.worldChat) p.SendChat(p, Server.DefaultColor + message); });
        }
        public static void GlobalMessage(string message)
        {
            players.ForEach(delegate(Player p) { p.SendMessage(message); });
        }
        public static void GlobalMessageLevel(Level l, string message)
        {
            players.ForEach(delegate(Player p) { if (p.level == l) p.SendMessage(message); });
        }
        public static void GlobalMessageOps(string message)     //Send a global messege to ops only
        {
            players.ForEach(delegate(Player p)
            {
                if (p.group.Permission >= LevelPermission.Operator)
                {
                    SendMessage(p, message);
                }
            });
        }
        public static void GlobalSpawn(Player from, ushort x, ushort y, ushort z, byte rotx, byte roty, bool self)
        {
            players.ForEach(delegate(Player p)
            {
                if (p.Loading && p != from) { return; }
                if (p.level != from.level || (from.hidden && !self)) { return; }
                if (p != from) { p.SendSpawn(from.id, from.color + from.name, x, y, z, rotx, roty); }
                else if (self)
                {
                    p.pos = new ushort[3] { x, y, z }; p.rot = new byte[2] { rotx, roty };
                    p.oldpos = p.pos; p.basepos = p.pos; p.oldrot = p.rot;
                    p.SendSpawn(from.id, from.color + from.name, x, y, z, rotx, roty);
                    p.SendDie(from.id);
                    p.SendPos(x, y, z, rotx, roty);
                }
            });
        }
        public static void GlobalDie(Player from, bool self)
        {
            players.ForEach(delegate(Player p)
            {
                if (p.level != from.level || (from.hidden && !self)) { return; }
                if (p != from) { p.SendDie(from.id); }
                else if (self) { p.SendDie(255); }
            });
        }
        public static void GlobalUpdate() { players.ForEach(delegate(Player p) { if (!p.hidden) { p.UpdatePosition(); } }); }
        #endregion
        #region == DISCONNECTING ==
        public void Disconnect() { leftGame(); }
        public void Kick(string kickString) { leftGame(kickString); }

        public void leftGame(string kickString = "", bool skip = false)
        {
            if (disconnected)
            {
                if (connections.Contains(this))
                    connections.Remove(this);
                return;
            }
            disconnected = true;
            try
            {
                pingTimer.Stop();
                afkTimer.Stop();
                try { antiSpeedhackTimer.Stop(); }
                catch { }
                achievementTimer.Stop();
                AGStimer.Stop();
                timeSaver.Stop();
                
                afkCount = 0;
                afkStart = DateTime.Now;
                
                try
                {
                    this.level.ListPlayers.Remove(this.name);
                }
                catch (Exception e) { Server.ErrorLog(e); }

                players.ForEach((who) =>
                {
                    who.saveSettings();
                });

                if (Server.afkset.Contains(name))
                    Server.afkset.Remove(name);
                if (kickString == "") kickString = "Disconnected.";
                SendKick(kickString);

                if (loggedIn)
                {
                    aiming = false;
                    isFlying = false;

                    GlobalDie(this, false);
                    if (kickString == "Disconnected." || kickString.IndexOf("Server shutdown") != -1)
                    {
                        if (MJS.Event.Exists("PlayerLeave")) MJS.Event.Trigger("PlayerLeave", "player $player " + name);

                        if (!hidden) { GlobalChat(this, "&c- " + color + prefix + name + Server.DefaultColor + " disconnected.", false); }
                        IRCBot.Say(name + " left the game.");
                        Server.s.Log(name + " disconnected.");
                    }
                    else
                    {
                        if (MJS.Event.Exists("PlayerKick")) MJS.Event.Trigger("PlayerKick", "player $player " + name + " string $message " + kickString.Replace(" ", "[space]"));

                        GlobalChat(this, "&c- " + color + prefix + name + Server.DefaultColor + " kicked (" + kickString + ").", false);
                        IRCBot.Say(name + " kicked (" + kickString + ").");
                        Server.s.Log(name + " kicked (" + kickString + ").");
                    }

                    //////MJS.Player.players.Remove(MJS.Player.players.Find(p => p.name == name));
                    players.Remove(this);
                    Server.s.PlayerListUpdate();
                    left.Add(this.name.ToLower(), this.ip);

                    try
                    {
                        if (!Directory.Exists("memory/undo")) Directory.CreateDirectory("memory/undo");
                        if (!Directory.Exists("memory/undoPrevious")) Directory.CreateDirectory("memory/undoPrevious");
                        DirectoryInfo di = new DirectoryInfo("memory/undo");
                        if (di.GetDirectories("*").Length >= Server.totalUndo)
                        {
                            Directory.Delete("memory/undoPrevious", true);
                            Directory.Move("memory/undo", "memory/undoPrevious");
                            Directory.CreateDirectory("memory/undo");
                        }

                        if (!Directory.Exists("memory/undo/" + name)) Directory.CreateDirectory("memory/undo/" + name);
                        di = new DirectoryInfo("memory/undo/" + name);
                        StreamWriter w = new StreamWriter(File.Create("memory/undo/" + realname + "/" + di.GetFiles("*.undo").Length + ".undo"));

                        foreach (UndoPos uP in UndoBuffer)
                        {
                            w.Write(uP.mapName + "^" +
                                    uP.x + "^" + uP.y + "^" + uP.z + "^" +
                                    uP.timePlaced + "^" +
                                    uP.type + "^" + uP.newtype + "&");
                        }
                        w.Flush();
                        w.Close();
                    }
                    catch (Exception e) { Server.ErrorLog(e); }

                    UndoBuffer.Clear();
                }
                else
                {
                    connections.Remove(this);
                    if (ip != "127.0.0.1" && ip != "198.199.98.246")
                        Server.s.Log(ip + " disconnected.");
                }
            }
            catch (Exception e) { Server.ErrorLog(e); }
        }

        #endregion
        #region == CHECKING ==
        public static List<Player> GetPlayers() { return new List<Player>(players); }
        public static bool Exists(string name)
        {
            foreach (Player p in players)
            { if (p.name.ToLower() == name.ToLower()) { return true; } } return false;
        }

        public static bool Exists(byte id)
        {
            foreach (Player p in players)
            { if (p.id == id) { return true; } } return false;
        }

        public static Player FindExact(string name)
        {
            foreach (Player p in players)
            {
                if (p.name.ToLower() == name.ToLower()) return p;
            }
            return null;
        }
        public static Player Find(string name)
        {
            List<Player> tempList = new List<Player>();
            tempList.AddRange(players);
            Player tempPlayer = null;
            bool returnNull = false;

            foreach (Player p in tempList)
            {
                if (p.name.ToLower() == name.ToLower()) return p;
                if (p.name.ToLower().IndexOf(name.ToLower()) != -1)
                {
                    if (tempPlayer == null) tempPlayer = p;
                    else returnNull = true;
                }
            }

            if (returnNull == true) return null;
            if (tempPlayer != null) return tempPlayer;
            return null;
        }

        public static bool checkSupporter(string s)
        {
            s = s.ToLower();
            if (s == "quaispet" || s == "x1dutch1x")
                return true;
            return false;
        }
        public static bool checkDevS(string p)
        {
            p = p.ToLower();
            if (p == "quaisaq" || p == "2k10")
                return true;
            return false;
        }

        public void CheckAchievement()
        {
            if (!checkingAchievements)
            {
                checkingAchievements = true;
                List<Achievement> checkAch = Achievements;
                Achievement ach = new Achievement();

                Achievements = new List<Achievement>();
                foreach (Server.achievements achievement in Server.allAchievements)
                {
                    ach.name = achievement.realname;

                    if (ach.name == "Minecraft Begins" && blocksBuild > 0) ach.earned = true;
                    else if (ach.name == "Block Maker" && blocksBuild >= 10000) ach.earned = true;
                    else if (ach.name == "House Creator" && blocksBuild >= 1000000) ach.earned = true;
                    else if (ach.name == "Proffesional Builder" && blocksBuild > 1000000000) ach.earned = true;
                    else if (ach.name == "Godly Builder" && totalBlocks > 99999999999) ach.earned = true;
                    else if (ach.name == "Griefer" && blocksGriefed >= 10000) ach.earned = true;
                    else if (ach.name == "Demoted" && group.Permission < LevelPermission.Guest) ach.earned = true;
                    else if (ach.name == "Promoted" && group.Permission > LevelPermission.Guest) ach.earned = true;
                    else if (ach.name == "Ranked Operator" && group.Permission >= LevelPermission.Operator) ach.earned = true;
                    else if (ach.name == "Ranked Admin" && group.Permission >= LevelPermission.Admin) ach.earned = true;
                    else if (ach.name == "Ranked Owner" && group.Permission >= LevelPermission.Owner) ach.earned = true;
                    else if (ach.name == "I'm Staying" && totalLogin >= 50) ach.earned = true;
                    else if (ach.name == "Survivor" && completedGames >= 3) ach.earned = true;
                    else if (ach.name == "Eco Friend" && builtEco >= 10000) ach.earned = true;
                    else if (ach.name == "Loud Mouth" && capsRage >= 5000) ach.earned = true;
                    else if (ach.name == "Spawner" && spawnedBots >= 200) ach.earned = true;
                    //else if (ach.name == "Demolist" && TNTsExploded >= 500) ach.earned = true;
                    else if (ach.name == "Server Fan" && TimeOnServer[0] >= 24) ach.earned = true;
                    else if (ach.name == "Addicted" && TimeOnServer[0] >= 100) ach.earned = true;
                    else if (ach.name == "Flyer" && flys >= 15) ach.earned = true;
                    else if (ach.name == "Bully" && slapper >= 50) ach.earned = true;
                    else if (ach.name == "Ninja" && slapped >= 500) ach.earned = true;
                    else if (ach.name == "Your Face, God Damit" && slapped >= 50) ach.earned = true;
                    else if (checkAch.Find(a => a.name.ToLower() == ach.name.ToLower()).earned) ach.earned = true;
                    else ach.earned = false;

                    Achievements.Add(ach);
                }

                achievementsEarned = 0;
                foreach (Achievement a in Achievements)
                {
                    if (a.earned) achievementsEarned++;
                }

                foreach (Achievement a in Achievements)
                {
                    if (!checkAch.Find(ac => ac.name == a.name).earned && Achievements.Find(ac => ac.name == a.name).earned
                        && Server.allAchievements.Find(ac => ac.realname == a.name).allowed)
                    {
                        MessageAchievement(a.name);
                    }
                }
                checkingAchievements = false;
            }
        }

        public static void Vote(Player p, string vote, List<string> availableVotes)
        {
            votes = new int[11];
            List<string> avaVotes = new List<string>();
            if (voteType == 0)
            {
                GlobalMessage(p.color + p.name + Server.DefaultColor + " started a vote!");
                GlobalMessage("Should " + availableVotes[0] + " be &4" + vote + ((vote == "mute") ? "d" : (vote == "ban") ? "ned" : "ed") + Server.DefaultColor + "?");
                GlobalMessage("1) &aYes");
                GlobalMessage("2) &cNo");
                avaVotes.Add("yes");
                avaVotes.Add("no");
            }
            else if (voteType == 1)
            {
                GlobalMessage(p.color + p.name + Server.DefaultColor + " started a vote!");
                GlobalMessage("Should the command &b" + vote + Server.DefaultColor + " be executed?");
                GlobalMessage("1) &aYes");
                GlobalMessage("2) &cNo");
                avaVotes.Add("yes");
                avaVotes.Add("no");
            }
            else
            {
                GlobalMessage(p.color + p.name + Server.DefaultColor + " started a vote!");
                GlobalMessage(vote);
                for (int i = 1; i <= availableVotes.Count; ++i)
                {
                    GlobalMessage(i + ") " + availableVotes[i - 1][0].ToString().ToUpper() + availableVotes[i - 1].ToString().Remove(0, 1));
                    avaVotes.Add(c.Remove(availableVotes[i - 1].ToLower()));
                }
            }
            GlobalMessage((voteType != 2) ? "Use \"!{vote}\" to vote, remember: only 1 vote."
                : "Use \"!{vote}, {maybe another vote}, {maybe even more}\" to vote!");

            while (avaVotes.Count <= 10)
                avaVotes.Add("");
            while (avaVotes.Count > 10)
                avaVotes.RemoveAt(10);

            possibleVotes = avaVotes;
            voteThread = new Thread(new ThreadStart(delegate
            {
                try
                {
                    for (int i = 0; i < 30; ++i)
                        if (votes[10] != -1)
                            Thread.Sleep(1000);
                        else
                            return;
                    int ii = 0;
                    int most = 0;

                    for (int i = 0; i < votes.Length; ++i)
                    {
                        if (votes[i] > ii) { most = i; ii = votes[i]; }
                    }

                    if (ii == 0)
                    {
                        GlobalMessage("Nobody voted, vote dropped.");
                        return;
                    }

                    GlobalMessage("30 seconds has passed. There are " + ii + " votes.");



                    List<int> founds = new List<int>();
                    foreach (int votess in votes)
                    {
                        if (!founds.Contains(votess) && votess != 0) founds.Add(votess);
                    }

                    if (founds.Count == 1)
                    {
                        GlobalMessage("The most voted thing was...");
                        Thread.Sleep(2000);

                        GlobalMessage(possibleVotes[most] + "!");

                        if (voteType == 1 && possibleVotes[most] == "yes")
                            Command.all.Find(vote.Split(' ')[0]).Use(p, vote.Substring(vote.IndexOf(' ') + 1));
                        else if (vote == "mute")
                        {
                            if (Player.Find(availableVotes[0]).muted)
                            {
                                if (possibleVotes[most] == "yes")
                                    GlobalMessage(availableVotes[0] + " is already muted!");
                                else
                                    Command.all.Find("mute").Use(null, availableVotes[0]);
                            }
                            else if (possibleVotes[most] == "yes")
                                Command.all.Find("mute").Use(null, availableVotes[0]);
                        }

                        else if (voteType == 0 && possibleVotes[most] == "yes")
                            Command.all.Find(vote).Use(null, availableVotes[0]);
                    }
                    else if (voteType == 1)
                    {
                        GlobalMessage("The votes were equal, so the command wont be run.");
                    }
                    else if (voteType == 0)
                    {
                        GlobalMessage("The votes were equal, so " + availableVotes[0] + " wont be &4" + vote + ((vote == "mute") ? "d" : (vote == "ban") ? "ned" : "ed") + Server.DefaultColor + ".");
                    }
                    else
                    {
                        GlobalMessage("The total votes were: ");
                        for (int i = 0; i < 10; ++i)
                        {
                            if (possibleVotes[i] != "")
                                GlobalMessage(possibleVotes[i] + ": " + votes[i]);
                        }
                    }
                    foreach (Player who in players)
                        who.voteGiven = false;
                }
                catch (Exception ex)
                {
                    Server.ErrorLog(ex);
                }
            }));
            voteThread.Start();
        }
        #endregion
        #region == OTHER ==
        static byte FreeId()
        {
            for (byte i = 0; i < Server.players; ++i)
            {
                bool used = false;
                players.ForEach((p) =>
                {
                    if (p.id == i)
                        used = true;
                });
                if (!used)
                    return i;
            }
            return (byte)0;
        }
        static byte[] StringFormat(string str, int size)
        {
            str = str.Trim();
            byte[] bytes = new byte[size];
            bytes = enc.GetBytes(str.PadRight(size).Substring(0, size));
            return bytes;
        }
        static List<string> Wordwrap(string message)
        {
            List<string> lines = new List<string>();
            message = Regex.Replace(message, @"(&[0-9a-f])+(&[0-9a-f])", "$2");
            message = Regex.Replace(message, @"(&[0-9a-f])+ +(&[0-9a-f])", "$2");
            message = Regex.Replace(message, @"(&[0-9a-f])+$", "");
            
            int limit = 64;
            string color = "";

            while (message.Length > 0)
            {
                if (lines.Count > 0)
                    if (message.TrimStart(' ')[0] == '&') message = "> " + message.Trim();
                    else message = "> " + color + message.Trim();

                if (message.Length <= limit) { lines.Add(message); break; }
                for (int i = limit - 1; i > limit - 20; --i)
                    if (message[i] == ' ')
                    {
                        lines.Add(message.Substring(0, i));
                        goto Next;
                    }

            retry:
                if (message.Length == 0 || limit == 0) { return lines; }

                try
                {
                    if (message.Substring(limit - 2, 1) == "&" || message.Substring(limit - 1, 1) == "&")
                    {
                        message = message.Remove(limit - 2, 1);
                        limit -= 2;
                        goto retry;
                    }
                    else if (message[limit - 1] < 32 || message[limit - 1] > 127)
                    {
                        message = message.Remove(limit - 1, 1);
                        limit -= 1;
                    }
                }
                catch { return lines; }
                lines.Add(message.Substring(0, limit));

            Next: message = message.Substring(lines[lines.Count - 1].Length);
                if (lines.Count == 1) limit = 60;

                int index = lines[lines.Count - 1].LastIndexOf('&');
                if (index != -1)
                {
                    if (index < lines[lines.Count - 1].Length - 1)
                    {
                        char next = lines[lines.Count - 1][index + 1];
                        if ("0123456789abcdef".IndexOf(next) != -1) { color = "&" + next; }
                        if (index == lines[lines.Count - 1].Length - 1)
                        {
                            lines[lines.Count - 1] = lines[lines.Count - 1].Substring(0, lines[lines.Count - 1].Length - 2);
                        }
                    }
                    else if (message.Length != 0)
                    {
                        char next = message[0];
                        if ("0123456789abcdef".IndexOf(next) != -1)
                        {
                            color = "&" + next;
                        }
                        lines[lines.Count - 1] = lines[lines.Count - 1].Substring(0, lines[lines.Count - 1].Length - 1);
                        message = message.Substring(1);
                    }
                }
            }
            //for (int i = 0; i < lines.Count; i++)
            //    lines[i] = c.Remove(lines[i]);
            
            return lines;
        }
        public static bool ValidName(string name)
        {
            string allowedchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._";
            foreach (char ch in name) { if (allowedchars.IndexOf(ch) == -1) { return false; } } return true;
        }
        public static byte[] GZip(byte[] bytes)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            GZipStream gs = new GZipStream(ms, CompressionMode.Compress, true);
            gs.Write(bytes, 0, bytes.Length);
            gs.Close();
            ms.Position = 0;
            bytes = new byte[ms.Length];
            ms.Read(bytes, 0, (int)ms.Length);
            ms.Close();
            return bytes;
        }
        #endregion
        #region == Host <> Network ==
        public byte[] HTNO(ushort x)
        {
            byte[] y = BitConverter.GetBytes(x); Array.Reverse(y); return y;
        }
        ushort NTHO(byte[] x, int offset)
        {
            byte[] y = new byte[2];
            Buffer.BlockCopy(x, offset, y, 0, 2); Array.Reverse(y);
            return BitConverter.ToUInt16(y, 0);
        }
        byte[] HTNO(short x)
        {
            byte[] y = BitConverter.GetBytes(x); Array.Reverse(y); return y;
        }
        #endregion

        public void MessageAchievement(string achievement)
        {
            GlobalMessage(Group.findPlayerGroup(name).color + name + Server.DefaultColor + " has earned the achievement &a" + achievement + Server.DefaultColor + ".");
        }

        bool CheckBlockSpam()
        {
            if (spamBlockLog.Count >= spamBlockCount && Server.AGS)
            {
                DateTime oldestTime = spamBlockLog.Dequeue();
                double spamTimer = DateTime.Now.Subtract(oldestTime).TotalSeconds;
                if (spamTimer < spamBlockTimer && !ignoreGrief)
                {
                    this.Kick("You were kicked by antigrief system. Slow down.");
                    GlobalMessage(c.red + name + " was kicked for suspected griefing.");
                    Server.s.Log(name + " was kicked for block spam (" + spamBlockCount + " blocks in " + spamTimer + " seconds)");
                    return true;
                }
            }
            spamBlockLog.Enqueue(DateTime.Now);
            return false;
        }
    }
}