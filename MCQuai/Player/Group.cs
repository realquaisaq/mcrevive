﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;
using System.Collections.Generic;

namespace MCRevive
{
    public class Group
    {
        public string name;
        public string trueName;
        public string color;
        public int Permission = LevelPermission.Null;
        public int maxBlocks;
        public CommandList commands;
        public string fileName;
        public PlayerList playerList;
        public bool CanExecute(Command cmd) { return commands.Contains(cmd); }

        public Group()
        {
            Permission = LevelPermission.Null;
        }
        public Group(int Perm, int maxB, string fullName, char newColor)
        {
            Permission = Perm;
            maxBlocks = maxB;
            trueName = fullName;
            name = trueName.ToLower();
            color = "&" + newColor;
            fileName = fullName.ToLower() + ((fullName.ToLower() != "banned" && fullName.ToLower() != "nobody") ? "s" : "") + ".txt";
            if (name != "nobody")
                playerList = PlayerList.Load(fileName, this);
            else
                playerList = new PlayerList();
        }

        public void fillCommands()
        {
            CommandList _commands = new CommandList();
            GrpCommands.AddCommands(out _commands, Permission);
            commands = _commands;
        }

        public static List<Group> GroupList = new List<Group>();
        public static Group standard;
        public static void InitAll()
        {
            GroupList = new List<Group>();

            if (File.Exists("properties/ranks.properties"))
            {
                string[] lines = File.ReadAllLines("properties/ranks.properties");

                Group thisGroup = new Group();
                int gots = 0;

                foreach (string s in lines)
                {
                    try
                    {
                        if (s != "" && s[0] != '#')
                        {
                            if (s.Split('=').Length == 2)
                            {
                                string key = s.Split('=')[0].Trim();
                                string value = s.Split('=')[1].Trim();

                                if (thisGroup.name == "" && key.ToLower() != "rankname")
                                {
                                    Server.s.Log("Hitting an error at " + s + " of ranks.properties");
                                }
                                else
                                {
                                    switch (key.ToLower())
                                    {
                                        case "rankname":
                                            gots = 0;
                                            thisGroup = new Group();

                                            if (value.ToLower() == "developers" || value.ToLower() == "devs")
                                                Server.s.Log("You are not a developer. Stop pretending you are.");
                                            else if (GroupList.Find(grp => grp.name == value.ToLower()) == null)
                                                thisGroup.trueName = value;
                                            else
                                                Server.s.Log("Cannot add the rank " + value + " twice");
                                            break;
                                        case "permission":
                                            int foundPermission;

                                            try
                                            {
                                                foundPermission = int.Parse(value);
                                            }
                                            catch { Server.s.Log("Invalid permission on " + s); break; }

                                            if (thisGroup.Permission != LevelPermission.Null)
                                            {
                                                Server.s.Log("Setting permission again on " + s);
                                                gots--;
                                            }

                                            bool allowed = true;
                                            if (GroupList.Find(grp => grp.Permission == foundPermission) != null)
                                                allowed = false;

                                            if (foundPermission > 119 || foundPermission < -50)
                                            {
                                                Server.s.Log("Permission must be between -50 and 119 for ranks");
                                                break;
                                            }

                                            if (allowed)
                                            {
                                                gots++;
                                                thisGroup.Permission = foundPermission;
                                            }
                                            else
                                            {
                                                Server.s.Log("Cannot have 2 ranks set at permission level " + value);
                                            }
                                            break;
                                        case "limit":
                                            int foundLimit;

                                            try
                                            {
                                                foundLimit = int.Parse(value);
                                            }
                                            catch { Server.s.Log("Invalid limit on " + s); break; }

                                            gots++;
                                            thisGroup.maxBlocks = foundLimit;
                                            break;
                                        case "color":
                                            char foundChar;
                                            if (value[0] == '&' || value[0] == '%') { value = value.Remove(0, 1); }
                                            try
                                            {
                                                foundChar = char.Parse(value);
                                            }
                                            catch { Server.s.Log("Incorrect color on " + s); break; }

                                            if ((foundChar >= '0' && foundChar <= '9') || (foundChar >= 'a' && foundChar <= 'f'))
                                            {
                                                gots++;
                                                thisGroup.color = foundChar.ToString();
                                            }
                                            else
                                            {
                                                Server.s.Log("Invalid color code at " + s);
                                            }
                                            break;
                                    }

                                    if (gots >= 3)
                                    {
                                        GroupList.Add(new Group(thisGroup.Permission, thisGroup.maxBlocks, thisGroup.trueName, thisGroup.color[0]));
                                    }
                                }
                            }
                            else
                            {
                                Server.s.Log("In ranks.properties, the line " + s + " is wrongly formatted");
                            }
                        }
                    }
                    catch { }
                }
            }

            if (GroupList.Find(grp => grp.Permission == -20) == null) GroupList.Add(new Group(-20, 1, "Banned", '8'));
            if (GroupList.Find(grp => grp.Permission == 1) == null && GroupList.Find(grp => grp.name == "Guest") == null)
                GroupList.Add(new Group(1, 1, "Guest", '7'));
            if (GroupList.Find(grp => grp.Permission == 20) == null && GroupList.Find(grp => grp.name == "Builder") == null)
                GroupList.Add(new Group(20, 500, "Builder", 'a'));
            if (GroupList.Find(grp => grp.Permission == 40) == null && GroupList.Find(grp => grp.name == "AdvBuilder") == null)
                GroupList.Add(new Group(40, 5000, "AdvBuilder", '2'));
            if (GroupList.Find(grp => grp.Permission == 80) == null && GroupList.Find(grp => grp.name == "Operator") == null)
                GroupList.Add(new Group(80, 50000, "Operator", '3'));
            if (GroupList.Find(grp => grp.Permission == 90) == null && GroupList.Find(grp => grp.name == "Admin") == null)
                GroupList.Add(new Group(90, 500000, "Admin", '4'));
            if (GroupList.Find(grp => grp.Permission == 100) == null && GroupList.Find(grp => grp.name == "Owner") == null)
                GroupList.Add(new Group(100, 5000000, "Owner", '6'));
            GroupList.Add(new Group(LevelPermission.NoOne, 0, "Nobody", '0'));

            bool swap = true; Group storedGroup;
            while (swap)
            {
                swap = false;
                for (int i = 0; i < GroupList.Count - 1; i++)
                    if (GroupList[i].Permission > GroupList[i + 1].Permission)
                    {
                        swap = true;
                        storedGroup = GroupList[i];
                        GroupList[i] = GroupList[i + 1];
                        GroupList[i + 1] = storedGroup;
                    }
            }

            standard = Group.Find(LevelPermission.Guest + "");

            SortGroups();
            saveGroups(GroupList);
            Server.s.UpdateRanks();
        }
        public static void saveGroups(List<Group> givenList)
        {
            StreamWriter SW = new StreamWriter(File.Create("properties/ranks.properties"));
            SW.WriteLine("#RankName = string");
            SW.WriteLine("#     The name of the rank, use capitalization.");
            SW.WriteLine("#");
            SW.WriteLine("#Permission = number");
            SW.WriteLine("#     The \"permission\" of the rank. It's a number.");
            SW.WriteLine("#		There are pre-defined permissions already set. (for the old ranks)");
            SW.WriteLine("#		Banned = -20, Guest = 1, Builder = 20, AdvBuilder = 40, Operator = 60");
            SW.WriteLine("#		Admin = 80, Owner = 100, Nobody = 120");
            SW.WriteLine("#		Must be greater than -50 and less than 120");
            SW.WriteLine("#		The higher the number, the more commands do (such as undo allowing more seconds)");
            SW.WriteLine("#Limit = number");
            SW.WriteLine("#     The command limit for the rank (can be changed in-game with /maxlimit)");
            SW.WriteLine("#		Must be greater than 0 and less than 10000000");
            SW.WriteLine("#Color = char");
            SW.WriteLine("#     A single letter or number denoting the color of the rank");
            SW.WriteLine("#	    Possibilities:");
            SW.WriteLine("#		    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f");
            SW.WriteLine();
            SW.WriteLine();

            foreach (Group grp in givenList)
            {
                if (grp.name != "nobody")
                {
                    SW.WriteLine("RankName = " + grp.trueName);
                    SW.WriteLine("Permission = " + (int)grp.Permission);
                    SW.WriteLine("Limit = " + grp.maxBlocks);
                    SW.WriteLine("Color = " + grp.color[1]);
                    SW.WriteLine();
                }
            }

            SW.Flush();
            SW.Close();
        }

        public static bool Exists(string name)
        {
            name = name.ToLower();
            foreach (Group gr in GroupList)
            {
                if (gr.name == name.ToLower()) { return true; }
            } return false;
        }
        public static Group Find(string name)
        {
            int perm = LevelPermission.Null;
            try
            {
                perm = LevelPermission.FromName(LevelPermission.ToName(int.Parse(name)));
            }
            catch
            {
                perm = LevelPermission.FromName(name);
            }

            foreach (Group gr in GroupList)
            {
                if (gr.Permission == perm) { return gr; }
            }
            return null;
        }
        public static Group FindPerm(int perm)
        {
            for (int i = 0; i < GroupList.Count; ++i)
                if (GroupList[i].Permission == perm)
                    return GroupList[i];
            return null;
        }
        public static Group findPlayerGroup(string playerName)
        {
            foreach (Group grp in Group.GroupList)
            {
                if (grp.playerList.Contains(playerName)) return grp;
            }
            return Group.standard;
        }

        public static void SortGroups()
        {
            List<Group> newList = new List<Group>();
            int[] perm = { 200, -50 };

            foreach (Group g in GroupList)
            {
                foreach (Group gr in GroupList)
                    if (gr.Permission < perm[0] && gr.Permission > perm[1])
                        perm[0] = gr.Permission;

                perm[1] = perm[0];
                perm[0] = 200;
                newList.Add(Group.FindPerm(perm[1]));
            }

            foreach (Player pl in Player.players)
            {
                pl.group = GroupList.Find(g => g.name == pl.group.name);
            }
        }
        public static string concatList(bool includeColor = true, bool skipExtra = false, bool permissions = false)
        {
            string returnString = "";
            foreach (Group grp in Group.GroupList)
            {
                if (!skipExtra || (grp.Permission > LevelPermission.Guest && grp.Permission < LevelPermission.NoOne))
                    if (includeColor)
                    {
                        returnString += ", " + grp.color + grp.name + Server.DefaultColor;
                    }
                    else if (permissions)
                    {
                        returnString += ", " + ((int)grp.Permission).ToString();
                    }
                    else
                        returnString += ", " + grp.name;
            }

            if (includeColor) returnString = returnString.Remove(returnString.Length - 2);

            return returnString.Remove(0, 2);
        }
    }

    public class GrpCommands
    {
        public struct rankAllowance { public string commandName; public int rank; }
        public static List<rankAllowance> allowedCommands;
        public static List<string> foundCommands = new List<string>();

        public static int defaultRanks(string command)
        {
            Command cmd = Command.all.Find(command);

            if (cmd != null) return cmd.defaultRank;
            else return LevelPermission.Null;
        }
        public static void fillRanks()
        {
            int section = 0;
            try
            {
                foundCommands = Command.all.commandNames();
                allowedCommands = new List<rankAllowance>();

                rankAllowance allowVar;
                section++;
                for (int i = 0; i < Command.all.commands.Count; ++i)
                {
                    allowVar = new rankAllowance();
                    allowVar.commandName = Command.all.commands[i].name;
                    allowVar.rank = Command.all.commands[i].defaultRank;
                    allowedCommands.Add(allowVar);
                }
                section++;
                if (File.Exists("properties/commands.properties"))
                {
                    string[] lines = File.ReadAllLines("properties/commands.properties");

                    foreach (string line in lines)
                    {
                        section = 10;
                        if (line == "") { section = 11; }
                        else if (line[0] == '#') { section = 12; }
                        else
                        {
                            section = 15;
                            bool isInt = true;
                            string key = line.Split('=')[0].Trim().ToLower();
                            string value = line.Split('=')[1].Trim().ToLower();

                            try
                            {
                                int.Parse(value);
                            }
                            catch { isInt = false; }
                            section = 20;
                            if (!foundCommands.Contains(key))
                            {
                                Server.s.Log("Incorrect command name: " + key);
                            }
                            else if (isInt && LevelPermission.ToName(int.Parse(value)) == "Null")
                            {
                                Server.s.Log("Incorrect value given for " + key + ", using default value.");
                            }
                            else if (!isInt && LevelPermission.FromName(value) == LevelPermission.Null)
                            {
                                Server.s.Log("Incorrect value given for " + key + ", using default value.");
                            }
                            else
                            {
                                section = 30;
                                allowVar.commandName = key;
                                allowVar.rank = int.Parse(value);

                                int current = 0;
                                foreach (rankAllowance aV in allowedCommands)
                                {
                                    if (key == aV.commandName) { allowedCommands[current] = allowVar; break; }
                                    current++;
                                }
                            }
                        }
                    }
                    section = 50;
                    Save(allowedCommands);
                }
                else Save(allowedCommands);
                section = 100;
                foreach (Group grp in Group.GroupList)
                {
                    grp.fillCommands();
                }
            }
            catch { Server.s.Log("LOAD FAILED! properties/commands.properties"); Server.s.Log(section + ""); }
        }

        public static void Save(List<rankAllowance> givenList)
        {
            try
            {
                StreamWriter w = new StreamWriter(File.Create("properties/commands.properties"));
                w.WriteLine("#   This file contains a reference to every command found in the server software");
                w.WriteLine("#   Use this file to specify which ranks get which commands" + Environment.NewLine);
                w.Write("#   Current ranks: "); foreach (Group grp in Group.GroupList) { w.Write((grp.name == Group.GroupList[0].name) ? grp.name : ", " + grp.name); }
                w.WriteLine("");
                w.WriteLine("");
                foreach (rankAllowance aV in givenList)
                {
                    w.WriteLine(aV.commandName + ((aV.commandName.Length > 16) ? "\t" : ((aV.commandName.Length < 8)) ? "\t\t" : "\t") + "=\t" + aV.rank);
                }
                w.Flush();
                w.Close();
                w.Dispose();
            }
            catch
            {
                Server.s.Log("SAVE FAILED! commands.properties");
            }
        }
        public static string getInts(List<int> givenList)
        {
            string returnString = ""; bool foundOne = false;
            foreach (int Perm in givenList)
            {
                foundOne = true;
                returnString += "," + Perm;
            }
            if (foundOne) returnString = returnString.Remove(0, 1);
            return returnString;
        }
        public static void AddCommands(out CommandList commands, int perm)
        {
            commands = new CommandList();

            foreach (rankAllowance aV in allowedCommands)
                if (aV.rank <= perm) commands.Add(Command.all.FindExact(aV.commandName));
        }
    }
}