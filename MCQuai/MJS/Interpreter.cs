﻿/* This source file is originally written by Jesbus
 * and may be used in other software and edited
 * freely as long as you put Jesbus's name in the
 * credits of the compilation and as long as this
 * comment remains fully intact.
 * 
 * The MJS interpreter is currently used for
 * command and event scripting in MCRevive
 * (Minecraft Classic server software).
 */

using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.Data;
using System.Security.Cryptography;
using System.Linq;
using System.Collections;

namespace MCRevive.MJS
{
    static class XMath
    {
        public static double Parse(string input)
        {
            ExpressionParser MathParser = new ExpressionParser();
            Hashtable MathParseHashes = new Hashtable();
            return MathParser.Parse(input, MathParseHashes);
        }
        public static double Pythagoras(double a, double b)
        {
            a = Math.Abs(a);
            b = Math.Abs(b);
            return Math.Sqrt(a * a + b * b);
        }
        public static double Pythagoras(double a, double b, double c)
        {
            return Pythagoras(a, Pythagoras(b, c));
        }
        public static double Max(double a, double b, double c)
        {
            return Math.Max(a, Math.Max(b, c));
        }
        public static double Max(double a, double b, double c, double d)
        {
            return Math.Max(Math.Max(a, b), Math.Max(c, d));
        }
    }
    public static class Script
    {
        //public static List<VarExtension> VarExtensions = new List<VarExtension>();
        public static List<Thread> ScriptThreads = new List<Thread>();

        public static void Execute(string file, string arguments, bool deleteWhenFinished = false)
        {
            if (!File.Exists(file)) { Server.s.MJSLog("Script file " + file + " does not exist."); return; }
            Thread scriptThread = new Thread(new ThreadStart(delegate
            {
                string[] lines = File.ReadAllLines(file);
                string[] blocks = new string[255];

                string[] numberVarNames = new string[255];
                double[] numberVars = new double[255];
                int numberVarCount = 0;

                string[] stringVarNames = new string[255];
                string[] stringVars = new string[255];
                int stringVarCount = 0;

                string[] mapVarNames = new string[255];
                Level[] mapVars = new Level[255];
                int mapVarCount = 0;

                string[] playerVarNames = new string[255];
                Player[] playerVars = new Player[255];
                int playerVarCount = 0;

                int i = 0;
                int j = 0;

                double parseTryer;

                #region CREATE ARGUMENT VARIABLES
                string varType = "";
                string varName = "";
                string varValue = "";
                foreach (string argPart in arguments.Split(' '))
                {
                    if (i % 3 == 0) varType = argPart;
                    else if (i % 3 == 1) varName = argPart.Remove(0, 1);
                    else
                    {
                        varValue = argPart.Replace("[space]", " ");
                        if (varType == "number")
                        {
                            if (!System.Double.TryParse(varValue, out parseTryer))
                            {
                                Server.s.MJSLog("Could not execute script " + file + ": argument \"" + varType + " $" + varName + " " + varValue + "\" contains an error.");
                                goto stopScript;
                            }
                            numberVars[numberVarCount] = parseTryer;
                            numberVarNames[numberVarCount] = varName;
                            numberVarCount++;
                        }
                        else if (varType == "string")
                        {
                            stringVarNames[stringVarCount] = varName;
                            stringVars[stringVarCount] = varValue;
                            stringVarCount++;
                        }
                        else if (varType == "map")
                        {
                            mapVars[mapVarCount] = Level.Find(varValue);
                            if (mapVars[mapVarCount] == null)
                            {
                                Server.s.MJSLog("Could not execute script " + file + ": no map called " + varValue + " is loaded.");
                                goto stopScript;
                            }
                            mapVarNames[mapVarCount] = varName;
                            mapVarCount++;
                        }
                        else if (varType == "player")
                        {
                            //Server.s.Log(varName + "=" + varValue);
                            playerVars[playerVarCount] = Player.Find(varValue);
                            if (playerVars[playerVarCount] == null)
                            {
                                Server.s.MJSLog("Could not execute script " + file + ": no player called " + varValue + " is connected.");
                                goto stopScript;
                            }
                            //Server.s.MJSLog("player:" + playerVars[playerVarCount].name);
                            mapVars[mapVarCount] = playerVars[playerVarCount].level;
                            mapVarNames[mapVarCount] = varName + ".$map";
                            mapVarCount++;
                            playerVarNames[playerVarCount] = varName;
                            playerVarCount++;
                        }
                        else
                        {
                            Server.s.MJSLog("Could not execute script " + file + ": Variable type " + varType + " does not exist in MCfresh.");
                            goto stopScript;
                        }
                    }
                    i++;
                }
                #endregion

                int curBlock = 0;
                string[] block_type = new string[255];
                bool[] block_skipping = new bool[255];
                bool[] block_executeElse = new bool[255];
                int[] block_inBlockCount = new int[255];
                int[] block_repeatCurBlock = new int[255];
                int[] block_repeatStartLine = new int[255];
                block_type[0] = "main";
                block_skipping[0] = false;
                block_executeElse[0] = false;

                bool writingTempThreadFile = false;
                int blocksInThreadCount = 0;
                StreamWriter tempThreadFileWriter = null;
                string tempThreadFileName = "";

                bool ifNot = false;
                bool inTryBlock = false;
                bool skipToCatch = false;
                bool executeCatch = false;
                int catch_inBlockCount = 0;
                int tryCurBlock = 0;
                int lineNum = 0;
                string lineError;
                bool fatalError;
                while (lineNum < lines.Length)
                {
                    try
                    {
                        lineError = "";
                        fatalError = false;
                        ifNot = false;
                        string line = lines[lineNum].Trim();
                        if (line.StartsWith("//") || line.StartsWith("#") || line.StartsWith("'")) goto nextLine; // So peoplezz can adzz commentzz
                        #region BLOCK HANDLING
                        if (writingTempThreadFile)
                        {
                            if (line == "{") { blocksInThreadCount++; tempThreadFileWriter.WriteLine(line); }
                            else if (line == "}" && blocksInThreadCount != 0) { blocksInThreadCount--; tempThreadFileWriter.WriteLine(line); }
                            else if (line == "}")
                            {
                                tempThreadFileWriter.Flush();
                                tempThreadFileWriter.Close();
                                writingTempThreadFile = false;
                                curBlock--;
                                Execute(tempThreadFileName, "", true);
                            }
                            else { tempThreadFileWriter.WriteLine(line); }
                            goto nextLine;
                        }
                        if (skipToCatch)
                        {
                            if (line.ToLower() == "catch") { skipToCatch = false; executeCatch = true; }
                            goto nextLine;
                        }
                        if (block_type[curBlock] == "catch" && !executeCatch)
                        {
                            if (line == "{") catch_inBlockCount++;
                            else if (line == "}" && catch_inBlockCount != 0) catch_inBlockCount--;
                            else if (line == "}")
                            {
                                executeCatch = false;
                                block_type[curBlock] = null;
                                curBlock--;
                            }
                            goto nextLine;
                        }
                        if (curBlock != 0)
                        {
                            if (block_skipping[curBlock - 1])
                            {
                                if (line == "{") block_inBlockCount[curBlock]++;
                                else if (line == "}" && block_inBlockCount[curBlock] != 0)
                                {
                                    block_inBlockCount[curBlock]--;
                                }
                                else if (line == "}")
                                {
                                    block_skipping[curBlock - 1] = false;
                                    curBlock--;
                                }
                                goto nextLine;
                            }
                        }
                        if (line.ToLower() == "else")
                        {
                            block_type[curBlock + 1] = "else";
                            goto nextLine;
                        }
                        if (line == "{")
                        {
                            curBlock++;
                            if (block_type[curBlock] == "if")
                            {
                                if (block_executeElse[curBlock - 1])
                                {
                                    block_skipping[curBlock - 1] = true;
                                    block_inBlockCount[curBlock] = 0;
                                }
                                else
                                {
                                    block_skipping[curBlock - 1] = false;
                                }
                            }
                            else if (block_type[curBlock] == "else")
                            {
                                if (!block_executeElse[curBlock - 1])
                                {
                                    block_skipping[curBlock - 1] = true;
                                    block_inBlockCount[curBlock] = 0;
                                }
                                else
                                {
                                    block_skipping[curBlock - 1] = false;
                                }
                            }
                            else if (block_type[curBlock] == "repeat")
                            {
                                block_repeatCurBlock[curBlock - 1] = curBlock - 1;
                                block_repeatStartLine[curBlock - 1] = lineNum - 1;
                                block_skipping[curBlock - 1] = false;
                                block_inBlockCount[curBlock - 1] = 0;
                            }
                            else if (block_type[curBlock] == "thread")
                            {
                                i = 0;
                                while (File.Exists("mjs/threads/temp" + i + ".mjs"))
                                {
                                    i++;
                                }
                                tempThreadFileWriter = new StreamWriter(File.Create("mjs/threads/temp" + i + ".mjs"));
                                tempThreadFileName = "mjs/threads/temp" + i + ".mjs";
                                writingTempThreadFile = true;
                            }
                            goto nextLine;
                        }
                        if (line == "}")
                        {
                            if (block_type[curBlock] == "repeat")
                            {
                                lineNum = block_repeatStartLine[curBlock];

                                curBlock--;
                                goto nextLine;
                            }
                            else
                            {
                                if (block_type[curBlock] == "try") inTryBlock = false;
                                block_inBlockCount[curBlock] = 0;
                                block_type[curBlock] = null;
                                block_executeElse[curBlock] = false;
                                block_skipping[curBlock] = false;
                            }
                            curBlock--;
                            goto nextLine;
                        }
                        if (line.ToLower() == "exit")
                        {
                            lineNum = lines.Length;
                            goto stopScript;
                        }
                        string[] lineParts = line.Split(' ');
                        if (lineParts.Length != 1)
                        {
                            if (lineParts[1] == "not")
                            {
                                ifNot = true;
                                i = 1;
                                while (i < lineParts.Length)
                                {
                                    i++;
                                    if (i < lineParts.Length) lineParts[i - 1] = lineParts[i];
                                }
                            }
                        }
                        #endregion
                        #region GROUP THE REMAINS OF STRINGS THAT WERE SPLIT APART INTO ONE ELEMENT OF lineParts
                        i = 0;
                        bool inString = false;
                        int linePartStringStart = 0;
                        int nulls = 0;
                        while (i < lineParts.Length)
                        {
                            if (!inString && lineParts[i].StartsWith("\"") && !lineParts[i].EndsWith("\""))
                            {
                                lineParts[i] += " ";
                                inString = true;
                                linePartStringStart = i;
                            }
                            else if (inString)
                            {
                                if (lineParts[i].EndsWith("\""))
                                {
                                    lineParts[linePartStringStart] += lineParts[i];
                                    inString = false;
                                }
                                else lineParts[linePartStringStart] += lineParts[i] + " ";
                                lineParts[i] = null;
                                nulls++;
                            }
                            i++;
                        }
                        /// REMOVE ALL NULL VALUES FROM lineParts:
                        string[] newLineParts = new string[lineParts.Length - nulls];
                        i = 0;
                        j = 0;
                        while (i < lineParts.Length)
                        {
                            if (lineParts[i] != null)
                            {
                                newLineParts[j] = lineParts[i];
                                j++;
                            }
                            i++;
                        }
                        lineParts = newLineParts;
                        #endregion
                        #region GROUP THE REMAINS OF MATH EXPRESSIONS THAT WERE SPLIT APART INTO ONE ELEMENT OF lineParts
                        i = 0;
                        bool inExpression = false;
                        int linePartExpressionStart = 0;
                        nulls = 0;
                        while (i < lineParts.Length)
                        {
                            if (!inExpression && lineParts[i].StartsWith("(") && !lineParts[i].EndsWith(")"))
                            {
                                inExpression = true;
                                linePartExpressionStart = i;
                            }
                            else if (inExpression)
                            {

                                if (lineParts[i].EndsWith(")"))
                                {
                                    lineParts[linePartExpressionStart] += lineParts[i];
                                    inString = false;
                                }
                                else lineParts[linePartExpressionStart] += lineParts[i];
                                lineParts[i] = null;
                                nulls++;
                            }
                            i++;
                        }
                        string[] newLineParts2 = new string[lineParts.Length - nulls];
                        i = 0;
                        j = 0;
                        while (i < lineParts.Length)
                        {
                            if (lineParts[i] != null)
                            {
                                newLineParts2[j] = lineParts[i];
                                j++;
                            }
                            i++;
                        }
                        lineParts = newLineParts2;
                        #endregion

                        // Update this for every new command:
                        #region CONVERSIONS AND VARIABLE TO VALUE REPLACEMENTS
                        if (lineParts[0] == "if") lineParts[0] = "If";
                        i = 0;
                        while (i < lineParts.Length && lineParts[i] != null)
                        {
                            string shouldBe = "";
                            string whatItIs = "";

                            if (lineParts[0] == "GlobalMessage" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "PlayerMessage" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "GenerateMap" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "GenerateMap" && (i == 3 || i == 4 || i == 5)) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "LoadMap" && lineParts[1].StartsWith("\"") && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "LoadMap" && i == 1) { shouldBe = "map"; goto done1; }
                            if (lineParts[0] == "UnloadMap" && i == 1) { shouldBe = "map"; goto done1; }
                            //if (lineParts[0] == "SaveMap" && i == 1) { shouldBe = "map"; goto done1; }
                            if (lineParts[0] == "ConsoleMessage" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "KickPlayer" && i == 1) { shouldBe = "player"; goto done1; }
                            if (lineParts[0] == "KickPlayer" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "MovePlayer" && i == 1) { shouldBe = "player"; goto done1; }
                            if (lineParts[0] == "MovePlayer" && i == 2) { shouldBe = "map"; goto done1; }
                            if (lineParts[0] == "CreateFile" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "CreateDirectory" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "DeleteFile" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "DeleteDirectory" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "TruncateFile" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "TruncateDirectory" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "FileWriteLine" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "FileWriteLine" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "ExecuteScript" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "TriggerEvent" && i == 1) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "SetBlock" && i == 2) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "SetBlock" && i == 3) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "SetBlock" && i == 4) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "SetBlock" && i == 5) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "GetBlock" && i == 2) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "GetBlock" && i == 3) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "GetBlock" && i == 4) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "GetBlock" && i == 5) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "SendBlock" && i == 2) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "SendBlock" && i == 3) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "SendBlock" && i == 4) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "SendBlock" && i == 5) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "Replace" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "Replace" && i == 3) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "Sleep" && i == 1) { shouldBe = "number"; goto done1; }
                            if (lineParts[0] == "Wait" && i == 1) { shouldBe = "number"; goto done1; }

                            if (lineParts[0] == "If" && lineParts[1] == "FileExists" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "DirectoryExists" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "FileContainsLine" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "FileContainsLine" && i == 3) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "FileContainsString" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "FileContainsString" && i == 3) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "FileContainsText" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "FileContainsText" && i == 3) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "FileContains" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "FileContains" && i == 3) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "FileRemoveLine" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "FileRemoveLine" && i == 3) { shouldBe = "string"; goto done1; }

                            if (lineParts[0] == "If" && lineParts[1] == "StringContains" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringContains" && i == 3) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringContainsString" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringContainsString" && i == 3) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringStartsWith" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringStartsWith" && i == 3) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringStartsWithString" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringStartsWithString" && i == 3) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringEndsWith" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringEndsWith" && i == 3) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringEndsWithString" && i == 2) { shouldBe = "string"; goto done1; }
                            if (lineParts[0] == "If" && lineParts[1] == "StringEndsWithString" && i == 3) { shouldBe = "string"; goto done1; }
							if (lineParts[0] == "If" && (lineParts[2] == "<" || lineParts[2] == ">" || lineParts[2] == "<=" || lineParts[2] == ">=") && i == 1) { shouldBe = "number"; goto done1; }
							if (lineParts[0] == "If" && (lineParts[2] == "<" || lineParts[2] == ">" || lineParts[2] == "<=" || lineParts[2] == ">=") && i == 3) { shouldBe = "number"; goto done1; }
							
                            if (lineParts[0] == "If" && (lineParts[2] == "==" || lineParts[2] == "!=") && (i == 1 || i == 3))
                            {
                                if (System.Double.TryParse(lineParts[1], out parseTryer) || System.Double.TryParse(lineParts[3], out parseTryer)) { shouldBe = "number"; goto done1; }
                                if (lineParts[1].StartsWith("\"") || lineParts[3].StartsWith("\"")) { shouldBe = "string"; goto done1; }
                                if (lineParts[1].StartsWith("$") && lineParts[3].StartsWith("$"))
                                {
                                    lineParts[1] = lineParts[1].Remove(0, 1);
                                    i = 0;
                                    while (i < 255)
                                    {
                                        if (mapVarNames[i] != null) { shouldBe = "map"; goto done1; }
                                        if (playerVarNames[i] != null) { shouldBe = "player"; goto done1; }
                                        if (stringVarNames[i] != null) { shouldBe = "string"; goto done1; }
                                        if (numberVarNames[i] != null) { shouldBe = "number"; goto done1; }
                                        i++;
                                    }
                                    lineError = "Variable $" + lineParts[1] + " does not exist.";
                                    fatalError = true;
                                    goto nextLine;
                                }
                                lineError = "Could not compare " + lineParts[1] + " with " + lineParts[3];
                                fatalError = true;
                                goto nextLine;
                            }
                            if (lineParts.Length > 2)
                            {
                                if (lineParts[0] == "If" && (lineParts[2] == "<" || lineParts[2] == ">" || lineParts[2] == "<=" || lineParts[2] == ">=") && (i == 1 || i == 3)) { shouldBe = "number"; goto done1; }
                                if ((lineParts[1] == "+=" || lineParts[1] == "-=" || lineParts[1] == "*=" || lineParts[1] == "/=" || lineParts[1] == "^=") && i == 2) { shouldBe = "number"; goto done1; }
                            }
                            if ((lineParts[0].ToLower() == "sleep" || lineParts[0].ToLower() == "wait") && i == 1) { shouldBe = "number"; goto done1; }
                            if (lineParts.Length >= 3)
                            {
                                if (lineParts[1] == "=" && i == 2) // Assigning value to variable
                                {
                                    lineParts[0] = lineParts[0].Remove(0, 1);
                                    j = 0;
                                    while (j < 255)
                                    {
                                        if (numberVarNames[j] == lineParts[0]) { shouldBe = "number"; }
                                        if (stringVarNames[j] == lineParts[0]) { shouldBe = "string"; }
                                        if (playerVarNames[j] == lineParts[0]) { if (lineParts[2].StartsWith("\"")) { shouldBe = "string"; } else if (lineParts[2].StartsWith("$")) { goto done3; } else { lineError = "Syntax error! After = a player variable or a string is expected."; } }
                                        if (mapVarNames[j] == lineParts[0]) { if (lineParts[2].StartsWith("\"")) { shouldBe = "string"; } else if (lineParts[2].StartsWith("$")) { goto done3; } else { lineError = "Syntax error! After = a map variable or a string is expected."; } }
                                        if (shouldBe != "")
                                        {
                                            lineParts[0] = "$" + lineParts[0];
                                            goto done1;
                                        }
                                        j++;
                                    }
                                    lineError = "Could not assign value to variable. Variable $" + lineParts[1] + " does not exist.";
                                    goto nextLine;
                                }
                            }
                            if (lineParts.Length >= 4)
                            {
                                if (lineParts[2] == "=" & i == 3) // Assigning value to variable after creating the variable
                                {
                                    //shouldBe = lineParts[0]; // as the type is already given in the first line part, easy-peasy <-- does not apply anymore :p
                                    switch (lineParts[0])
                                    {
                                        case "string":
                                            shouldBe = "string";
                                            break;
                                        case "number":
                                            shouldBe = "number";
                                            break;
                                        default:
                                            goto done3; // if a map or player variable is being declared, skip the conversions and replacements as they need to be done else in the kajlkfafadlkfkl
                                    }
                                    goto done1;
                                }
                            }
                            goto done3;
                        done1:
                            if (lineParts[i].StartsWith("\"")) { whatItIs = "string"; goto done2; }
                            else if (System.Double.TryParse(lineParts[i], out parseTryer)) { whatItIs = "number"; goto done2; }
                            else if (lineParts[i].StartsWith("(") && lineParts[i].EndsWith(")")) { whatItIs = "math"; goto done2; }
                            else if (!lineParts[i].StartsWith("$")) { whatItIs = "map/player"; goto done2; } // old >.>
                            else
                            {
                                //Server.s.Log(lineParts[i]);
                                lineParts[i] = lineParts[i].Remove(0, 1);
                                if (lineParts[i] == "$server.$defaultcolor") { lineParts[1] = "\"" + Server.DefaultColor + "\""; goto done2; }
                                //Server.s.Log(lineParts[i]);
                                j = 0;
                                while (j < 255)
                                {
                                    if (lineParts[i] == playerVarNames[j] + ".$rank.$name.$length") { lineParts[i] = playerVars[j].group.name.Length.ToString(); whatItIs = "number"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$rank.$name.$length") { lineParts[i] = playerVars[j].group.name.Length.ToString(); whatItIs = "number"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$map.$name.$length") { lineParts[i] = playerVars[j].level.name.Length.ToString(); whatItIs = "number"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$map.$owner.$length") { lineParts[i] = playerVars[j].level.creator.Length.ToString(); whatItIs = "number"; goto done2; }
                                    
                                    if (lineParts[i] == mapVarNames[j] + ".$name.$length") { lineParts[i] = mapVars[j].name.Length.ToString(); whatItIs = "number"; goto done2; }
                                    if (lineParts[i] == mapVarNames[j] + ".$players.$count") { lineParts[i] = mapVars[j].ListPlayers.Count.ToString(); whatItIs = "number"; goto done2; }
                                    if (lineParts[i] == mapVarNames[j] + ".$owner.$length") { lineParts[i] = mapVars[j].creator.Length.ToString(); whatItIs = "number"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$rank.$color") { lineParts[i] = "\"" + playerVars[j].group.color + "\""; whatItIs = "string"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$rank.$name") { lineParts[i] = "\"" + playerVars[j].group.name + "\""; whatItIs = "string"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$name.$length") { lineParts[i] = playerVars[j].name.Length.ToString(); whatItIs = "number"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$map.$name") { lineParts[i] = "\"" + playerVars[j].level.name + "\""; whatItIs = "string"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$map.$owner") { lineParts[i] = "\"" + playerVars[j].level.creator + "\""; whatItIs = "string"; goto done2; }

                                    if (lineParts[i] == mapVarNames[j] + ".$name") { lineParts[i] = "\"" + mapVars[j].name + "\""; whatItIs = "string"; goto done2; }
                                    if (lineParts[i] == mapVarNames[j] + ".$creator") { lineParts[i] = "\"" + mapVars[j].name + "\""; whatItIs = "string"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$name") { lineParts[i] = "\"" + playerVars[j].name + "\""; whatItIs = "string"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$titlecolor") { lineParts[i] = "\"" + playerVars[j].titlecolor + "\""; whatItIs = "string"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$title") { lineParts[i] = "\"" + playerVars[j].title + "\""; whatItIs = "string"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$map") { lineParts[i] = playerVars[j].level.name; whatItIs = "map"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j] + ".$rank") { lineParts[i] = playerVars[j].group.Permission.ToString(); whatItIs = "number"; goto done2; }
                                    if (lineParts[i] == stringVarNames[j] + ".$length") { lineParts[i] = stringVars[j].Length.ToString(); whatItIs = "number"; goto done2; }
                                    
                                    if (lineParts[i] == numberVarNames[j]) { lineParts[i] = numberVars[j].ToString(); whatItIs = "number"; goto done2; }
                                    if (lineParts[i] == stringVarNames[j]) { lineParts[i] = "\"" + stringVars[j].ToString() + "\""; whatItIs = "string"; goto done2; }
                                    if (lineParts[i] == playerVarNames[j]) { whatItIs = "player"; goto done2; } // lineParts[i] = playerVars[j].name; }
                                    if (lineParts[i] == mapVarNames[j]) { whatItIs = "map"; goto done2; } // lineParts[i] = mapVars[j].name; }
                                    j++;
                                }
                                if (whatItIs == "")
                                {
                                    lineError = "Could not find variable $" + lineParts[i];
                                    goto nextLine;
                                }
                            }
                            lineError = "Unknown syntax error.";
                            goto nextLine;
                        done2:
                            //if (whatItIs == "map/player" && (shouldBe == "map" || shouldBe == "player")) goto done3; <-- nope, removed that outta here
                            if (whatItIs == "string")
                            {
                                lineParts[i] = lineParts[i].Replace("$server.$defaultcolor", Server.DefaultColor);
                                j = 0;
                                while (j < 255)
                                {
                                    //try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$rank.$name.$length", playerVars[j].group.name.Length); } catch { }
                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$map.$name.$length", playerVars[j].level.name.ToString()); }
                                    catch { }
                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$map.$owner.$length", playerVars[j].level.name.ToString()); }
                                    catch { }
                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$rank.$name.$length", playerVars[j].group.name.ToString()); }
                                    catch { }
                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$rank.$color.$length", playerVars[j].group.color.Length.ToString()); }
                                    catch { }

                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + mapVarNames[j] + ".$map.$name", playerVars[j].level.name); }
                                    catch { }
                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$map.$owner", playerVars[j].level.creator); }
                                    catch { }
                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$rank.$name", playerVars[j].group.name); }
                                    catch { }
                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$rank.$color", playerVars[j].group.color); }
                                    catch { }
                                    try { if (mapVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + mapVarNames[j] + ".$players.$count", mapVars[j].ListPlayers.Count.ToString()); }
                                    catch { }
                                    try { if (mapVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + mapVarNames[j] + ".$name.$length", mapVars[j].name.Length.ToString()); }
                                    catch { }
                                    try { if (mapVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + mapVarNames[j] + ".$creator.$length", mapVars[j].creator.Length.ToString()); }
                                    catch { }

                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$name", playerVars[j].name); }
                                    catch { }
                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$rank", playerVars[j].group.color + playerVars[j].group.name); }
                                    catch { }
                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$titlecolor", playerVars[j].titlecolor); }
                                    catch { }
                                    try { if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$title", playerVars[j].title); }
                                    catch { }
                                    try { if (mapVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + mapVarNames[j] + ".$name", mapVars[j].name); }
                                    catch { }
                                    try { if (mapVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + mapVarNames[j] + ".$creator", mapVars[j].creator); }
                                    catch { }
                                    try { if (stringVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + stringVarNames[j] + ".$length", stringVars[j].Length.ToString()); }
                                    catch { }
                                    
                                    try { if (stringVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + stringVarNames[j], stringVars[j]); }
                                    catch { }
                                    try { if (numberVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + numberVarNames[j], numberVars[j].ToString()); } catch { }
                                    j++;
                                }
                            }
                            if (whatItIs == "math") ///// FIRST CONVERT ALL MATH TO NUMBER. THEN LATER CONVERT THE NUMBERS TO OTHER STUFF, IF NECESSARY :)
                            {
                                j = 0;
                                while (j < 255)
                                {
                                    if (numberVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + numberVarNames[j], numberVars[j].ToString());
                                    if (playerVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + playerVarNames[j] + ".$map.$players.$count", playerVars[j].level.ListPlayers.Count.ToString());
                                    if (mapVarNames[j] != null) lineParts[i] = lineParts[i].Replace("$" + mapVarNames[j] + ".$players.$count", mapVars[j].ListPlayers.Count.ToString());
                                    j++;
                                }
                                try
                                {
                                    lineParts[i] = XMath.Parse(lineParts[i]).ToString();
                                    whatItIs = "number";
                                }
                                catch (Exception e) // ehhh, does this work?
                                {
                                    lineError = "Could not parse math " + lineParts[i] + ": " + e.Message;
                                    goto nextLine;
                                }
                            }
                            if (whatItIs == shouldBe) goto done3;
                            if (whatItIs == "string" && shouldBe == "number")   //////// STRING --> NUMBER
                            {
                                if (!System.Double.TryParse(ParseString(lineParts[i]), out parseTryer))
                                {
                                    if (Block.Number(ParseString(lineParts[i])) == 50)
                                    {
                                        lineError = "Could not convert string " + lineParts[i] + " to number.";
                                        goto nextLine;
                                    }
                                    else
                                    {
                                        lineParts[i] = Convert.ToInt16(Block.Number(ParseString(lineParts[i]))).ToString();
                                    }
                                }
                                else
                                {
                                    lineParts[i] = ParseString(lineParts[i]);
                                }
                                goto done3;
                            }
                            #region removed conversions
                            /* //////////////////////////// MADE IMPOSSIBLE SINCE MAPS AND PLAYERS ARE ONLY VARIABLES
                            else if (whatItIs == "string" && shouldBe == "map") //////// STRING --> MAP
                            {
                                if (Map.Find(lineParts[i]) == null)
                                {
                                    lineError = "Could not convert string \"" + lineParts[i] + "\" to map, no such map is loaded.";
                                    goto nextLine;
                                }
                                else
                                {
                                    lineParts[i] = ParseString(lineParts[i]);
                                }
                            }
                            */
                            /*
                            else if (whatItIs == "string" && shouldBe == "player") ////// STRING --> PLAYER
                            {
                                if (Player.Find(lineParts[i]) == null)
                                {
                                    lineError = "Could not convert string \"" + lineParts[i] + "\" to player, no such player is connected.";
                                    goto nextLine;
                                }
                                else
                                {
                                    lineParts[i] = ParseString(lineParts[i]);
                                }
                            }
                            */
                            #endregion
                            else if (whatItIs == "number" && shouldBe == "string")
                            {
                                lineParts[i] = "\"" + lineParts[i] + "\"";
                            }
                            else
                            {
                                lineError = "Failed to convert " + whatItIs + " to " + shouldBe + ".";
                                goto nextLine;
                            }
                        done3:
                            i++;
                        }
                        #endregion

                        #region Try Catch
                        if (lineParts[0].ToLower() == "try")
                        {
                            if (inTryBlock)
                            {
                                lineError = "You can't have a try block inside another try block, sorry.";
                                block_type[curBlock + 1] = "nothing";
                                goto nextLine;
                            }
                            block_type[curBlock + 1] = "try";
                            inTryBlock = true;
                            executeCatch = false;
                            tryCurBlock = curBlock;
                            goto nextLine;
                        }
                        if (lineParts[0].ToLower() == "catch")
                        {
                            block_type[curBlock + 1] = "catch";
                            goto nextLine;
                        }
                        #endregion
                        #region Repeat StopRepeat
                        if (line.ToLower() == "repeat")
                        {
                            block_type[curBlock + 1] = "repeat";
                            goto nextLine;
                        }
                        if (line.ToLower() == "stop repeat" || line.ToLower() == "stop repeating" || line.ToLower() == "stoprepeat" || line.ToLower() == "stoprepeating")
                        {
                            i = lineNum;
                            double blocksLess = 0;
                            while (i >= 0)
                            {
                                if (lines[i].Trim() == "{") blocksLess++;
                                else if (lines[i].Trim() == "}") blocksLess--;
                                else if (lines[i].Trim().ToLower() == "repeat") goto repeatFound;
                                i--;
                            }
                            lineError = "How the hell can it stop repeating when its not repeating :')";
                            goto nextLine;
                        repeatFound:
                            curBlock -= (int)blocksLess - 1;
                            i = lineNum;
                            while (i <= lines.Length)
                            {
                                if (lines[i].Trim() == "{") blocksLess++;
                                else if (lines[i].Trim() == "}") blocksLess--;
                                if (blocksLess == 0) goto repeatEndFound;
                                i++;
                            }
                        repeatEndFound:
                            lineNum = i;
                            goto nextLine;
                        }
                        #endregion
                        #region Thread
                        if (line.ToLower() == "thread")
                        {
                            block_type[curBlock + 1] = "thread";
                            goto nextLine;
                        }
                        #endregion
                        if (lineParts.Length >= 2)
                        {
                            #region CREATE NUMBER VARIABLE
                            if (lineParts[0] == "number" && lineParts[1].StartsWith("$"))
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (numberVarNames[i] == lineParts[1] || stringVarNames[i] == lineParts[1] || mapVarNames[i] == lineParts[1] || playerVarNames[i] == lineParts[1])
                                    {
                                        lineError = "Could not create number variable $" + lineParts[1] + ", variable already exists.";
                                        goto nextLine;
                                    }
                                    i++;
                                }
                                numberVarNames[numberVarCount] = lineParts[1];
                                if (lineParts.Length > 3)
                                {
                                    if (lineParts[2] == "=")
                                    {
                                        if (!System.Double.TryParse(lineParts[3], out numberVars[numberVarCount]))
                                        {
                                            lineError = "Number expected but syntax error found.";
                                            goto nextLine;
                                        }
                                        //numberVars[numberVarCount] = Convert.ToDouble(lineParts[3]);
                                    }
                                }
                                numberVarCount++;
                                goto nextLine;
                            }
                            #endregion
                            #region CREATE STRING VARIABLE
                            if (lineParts[0] == "string" && lineParts[1].StartsWith("$"))
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (numberVarNames[i] == lineParts[1] || stringVarNames[i] == lineParts[1] || mapVarNames[i] == lineParts[1] || playerVarNames[i] == lineParts[1])
                                    {
                                        lineError = "Could not create string variable $" + lineParts[1] + ", variable already exists.";
                                        goto nextLine;
                                    }
                                    i++;
                                }
                                stringVarNames[stringVarCount] = lineParts[1];
                                if (lineParts.Length > 2)
                                {
                                    if (lineParts[2] == "=")
                                    {
                                        if (!lineParts[3].StartsWith("\"") || !lineParts[3].EndsWith("\""))
                                        {
                                            lineError = "String expected but syntax error found.";
                                            goto nextLine;
                                        }
                                        stringVars[stringVarCount] = ParseString(lineParts[3]);
                                    }
                                }
                                stringVarCount++;
                                goto nextLine;
                            }
                            #endregion
                            #region CREATE MAP VARIABLE
                            if (lineParts[0] == "map" && lineParts[1].StartsWith("$"))
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (numberVarNames[i] == lineParts[1] || stringVarNames[i] == lineParts[1] || mapVarNames[i] == lineParts[1] || playerVarNames[i] == lineParts[1])
                                    {
                                        lineError = "Could not create map variable $" + lineParts[1] + ", variable already exists.";
                                        goto nextLine;
                                    }
                                    i++;
                                }
                                mapVarNames[mapVarCount] = lineParts[1];
                                stringVarNames[stringVarCount] = lineParts[1] + ".$name";
                                stringVarCount++;
                                if (lineParts.Length > 2)
                                {
                                    if (lineParts[2] == "=")
                                    {
                                        mapVars[mapVarCount] = Level.Find(ParseString(lineParts[3]));
                                        if (mapVars[mapVarCount] == null)
                                        {
                                            lineError = "Could not find map \"" + lineParts[3] + "\"";
                                            goto nextLine;
                                        }
                                    }
                                }
                                mapVarCount++;
                                goto nextLine;
                            }
                            #endregion
                            #region CREATE PLAYER VARIABLE
                            if (lineParts[0] == "player" && lineParts[1].StartsWith("$"))
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (numberVarNames[i] == lineParts[1] || stringVarNames[i] == lineParts[1] || mapVarNames[i] == lineParts[1] || playerVarNames[i] == lineParts[1])
                                    {
                                        lineError = "Could not create player variable $" + lineParts[1] + ", variable already exists.";
                                        goto nextLine;
                                    }
                                    i++;
                                }
                                playerVarNames[playerVarCount] = lineParts[1];
                                mapVarNames[mapVarCount] = lineParts[1] + ".$map";
                                stringVarNames[stringVarCount] = lineParts[1] + ".$map.$name";
                                if (lineParts.Length > 2)
                                {
                                    if (lineParts[2] == "=")
                                    {
                                        if (lineParts[3].StartsWith("\"") && lineParts[3].EndsWith("\""))
                                        {
                                            playerVars[playerVarCount] = Player.Find(ParseString(lineParts[3]));
                                            if (playerVars[playerVarCount] == null)
                                            {
                                                lineError = "Could not find player \"" + lineParts[3] + "\"";
                                                goto nextLine;
                                            }
                                        }
                                        else if (lineParts[3].StartsWith("$"))
                                        {
                                            lineParts[1] = lineParts[1].Remove(0, 1);
                                            i = 0;
                                            while (i < 255)
                                            {
                                                if (playerVarNames[i] == lineParts[1]) goto varFound002;
                                                i++;
                                            }
                                            lineError = "Could not find player variable $" + lineParts[1] + ".";
                                            goto nextLine;
                                        varFound002:
                                            playerVars[playerVarCount] = playerVars[i];
                                        }
                                        mapVars[mapVarCount] = playerVars[playerVarCount].level;
                                        stringVars[stringVarCount] = playerVars[playerVarCount].level.name;
                                    }
                                }
                                mapVarCount++;
                                playerVarCount++;
                                stringVarCount++;
                                goto nextLine;
                            }
                            #endregion

                            #region NON-STATIC VARIABLE FUNCTIONS
                            if (lineParts[0].ToLower() == "addvar" || lineParts[0].ToLower() == "addvariable" || lineParts[0].ToLower() == "addnonstaticvar" || lineParts[0].ToLower() == "addnonstaticvariable")
                            {
                                string newVar = lineParts[1];
                                string extends = "";
                                if (newVar.StartsWith("player.$"))
                                {
                                    extends = "player";
                                }
                                else if (newVar.StartsWith("map.$"))
                                {
                                    extends = "map";
                                }
                                else
                                {
                                    lineError = "Syntax error!";
                                    goto nextLine;
                                }
                                newVar = newVar.Replace(extends+".$", "");
                                if (Directory.Exists("memory\\" + extends + "\\mjs\\" + newVar))
                                {
                                    lineError = "Variable already exists;";
                                    goto nextLine;
                                }
                                else
                                {
                                    Directory.CreateDirectory("memory\\" + extends + "\\mjs\\" + newVar);
                                }
                                goto nextLine;
                            }
                            #endregion
                            // Foundvar 7 t/m 9:
                            #region ASSIGN VALUE TO VARIABLES
                            if (lineParts[1] == "=")
                            {
                                i = 0;
                                varType = "";
                                lineParts[0] = lineParts[0].Remove(0, 1);
                                while (i < 255)
                                {
                                    if (numberVarNames[i] == lineParts[0]) { varType = "number"; goto foundVar7; }
                                    if (stringVarNames[i] == lineParts[0]) { varType = "string"; goto foundVar7; }
                                    if (playerVarNames[i] == lineParts[0]) { varType = "player"; goto foundVar7; }
                                    if (mapVarNames[i] == lineParts[0]) { varType = "map"; goto foundVar7; }
                                    i++;
                                }
                                lineError = "Could not assign variable, variable $" + lineParts[0] + " does not exist.";
                                goto nextLine;
                            foundVar7:
                                lineParts[0] = "$" + lineParts[0];
                                if (varType == "string") { stringVars[i] = ParseString(lineParts[2]); goto nextLine; }
                                if (varType == "number") { numberVars[i] = System.Double.Parse(lineParts[2]); goto nextLine; }
                                if (varType == "player")
                                {
                                    if (lineParts[2].StartsWith("$"))
                                    {
                                        lineParts[2] = lineParts[2].Remove(0, 1);
                                        j = 0;
                                        while (j < 255)
                                        {
                                            if (playerVarNames[j] == lineParts[2]) goto foundVar8;
                                            j++;
                                        }
                                        lineError = "Could not find player variable $" + lineParts[2] + ".";
                                        goto nextLine;
                                    foundVar8:
                                        playerVars[i] = playerVars[j];
                                        goto nextLine;
                                    }
                                    else if (lineParts[2].StartsWith("\""))
                                    {
                                        lineParts[2] = ParseString(lineParts[2]);
                                        playerVars[i] = Player.Find(lineParts[2]);
                                        if (playerVars[i] != null) lineError = "Could not assign player variable. No player " + lineParts[2] + " is connected.";
                                        goto nextLine;
                                    }
                                    // no extra else is needed. that error is already handled somewhere :)
                                    goto nextLine;
                                }
                                if (varType == "map")
                                {
                                    if (lineParts[2].StartsWith("$"))
                                    {
                                        lineParts[2] = lineParts[2].Remove(0, 1);
                                        j = 0;
                                        while (j < 255)
                                        {
                                            if (mapVarNames[j] == lineParts[2]) goto foundVar9;
                                            j++;
                                        }
                                        lineError = "Could not find map variable $" + lineParts[2] + ".";
                                        goto nextLine;
                                    foundVar9:
                                        mapVars[i] = mapVars[j];
                                        goto nextLine;
                                    }
                                    else if (lineParts[2].StartsWith("\""))
                                    {
                                        lineParts[2] = ParseString(lineParts[2]);
                                        mapVars[i] = Level.Find(lineParts[2]);
                                        if (mapVars[i] != null) lineError = "Could not assign player variable. No map " + lineParts[2] + " is loaded.";
                                        goto nextLine;
                                    }
                                    // no extra else is needed. that error is already handled somewhere :)
                                    goto nextLine;
                                }
                            }
                            #endregion
                            // Foundvar 10 t/m 16:
                            #region NUMBERS ++ -- += -= *= /= ^=
                            if (lineParts[1] == "++")
                            {
                                lineParts[0] = lineParts[0].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (lineParts[0] == numberVarNames[i]) goto foundVar10;
                                    i++;
                                }
                                lineError = "Could not find variable $" + lineParts[0] + " :(";
                                goto nextLine;
                            foundVar10:
                                numberVars[i]++;
                                goto nextLine;
                            }
                            if (lineParts[1] == "--")
                            {
                                lineParts[0] = lineParts[0].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (lineParts[0] == numberVarNames[i]) goto foundVar11;
                                    i++;
                                }
                                lineError = "Could not find variable $" + lineParts[0] + " :(";
                                goto nextLine;
                            foundVar11:
                                numberVars[i]--;
                                goto nextLine;
                            }
                            if (lineParts[1] == "+=")
                            {
                                lineParts[0] = lineParts[0].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (lineParts[0] == numberVarNames[i]) goto foundVar12;
                                    i++;
                                }
                                lineError = "Could not find variable $" + lineParts[0] + " :(";
                                goto nextLine;
                            foundVar12:
                                try
                                {
                                    numberVars[i] += System.Double.Parse(lineParts[2]);
                                }
                                catch (Exception e)
                                {
                                    lineError = "Failed to change number variable $" + lineParts[0] + ": " + e.Message;
                                }
                                goto nextLine;
                            }
                            if (lineParts[1] == "-=")
                            {
                                lineParts[0] = lineParts[0].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (lineParts[0] == numberVarNames[i]) goto foundVar13;
                                    i++;
                                }
                                lineError = "Could not find variable $" + lineParts[0] + " :(";
                                goto nextLine;
                            foundVar13:
                                try
                                {
                                    numberVars[i] -= System.Double.Parse(lineParts[2]);
                                }
                                catch (Exception e)
                                {
                                    lineError = "Failed to change number variable $" + lineParts[0] + ": " + e.Message;
                                }
                                goto nextLine;
                            }
                            if (lineParts[1] == "*=")
                            {
                                lineParts[0] = lineParts[0].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (lineParts[0] == numberVarNames[i]) goto foundVar14;
                                    i++;
                                }
                                lineError = "Could not find variable $" + lineParts[0] + " :(";
                                goto nextLine;
                            foundVar14:
                                try
                                {
                                    numberVars[i] *= System.Double.Parse(lineParts[2]);
                                }
                                catch (Exception e)
                                {
                                    lineError = "Failed to change number variable $" + lineParts[0] + ": " + e.Message;
                                }
                                goto nextLine;
                            }
                            if (lineParts[1] == "/=")
                            {
                                lineParts[0] = lineParts[0].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (lineParts[0] == numberVarNames[i]) goto foundVar15;
                                    i++;
                                }
                                lineError = "Could not find variable $" + lineParts[0] + " :(";
                                goto nextLine;
                            foundVar15:
                                try
                                {
                                    numberVars[i] /= System.Double.Parse(lineParts[2]);
                                }
                                catch (Exception e)
                                {
                                    lineError = "Failed to change number variable $" + lineParts[0] + ": " + e.Message;
                                }
                                goto nextLine;
                            }
                            if (lineParts[1] == "^=")
                            {
                                lineParts[0] = lineParts[0].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (lineParts[0] == numberVarNames[i]) goto foundVar16;
                                    i++;
                                }
                                lineError = "Could not find variable $" + lineParts[0] + " :(";
                                goto nextLine;
                            foundVar16:
                                try
                                {
                                    numberVars[i] = Math.Pow(numberVars[i], System.Double.Parse(lineParts[2]));
                                }
                                catch (Exception e)
                                {
                                    lineError = "Failed to change number variable $" + lineParts[0] + ": " + e.Message;
                                }
                                goto nextLine;
                            }
                            #endregion
                            #region GlobalMessage PlayerMessage ConsoleMessage
                            if (lineParts[0] == "GlobalMessage")
                            {
                                Player.GlobalMessage(ParseString(lineParts[1]));
                                goto nextLine;
                            }
                            if (lineParts[0] == "PlayerMessage")
                            {
                                //Server.s.Log(lineParts[1]);
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                //Server.s.Log(lineParts[1]);
                                i = 0;
                                while (i < 255)
                                {
                                    if (playerVarNames[i] == lineParts[1]) goto varFound001;
                                    i++;
                                }
                                lineError = "Could not find player variable $" + lineParts[1] + ".";
                                goto nextLine;
                            varFound001:
                                Server.s.MJSLog(playerVars[i].name);
                                playerVars[i].SendMessage(ParseString(lineParts[2]));
                                goto nextLine;
                            }
                            if (lineParts[0] == "ConsoleMessage")
                            {
                                Server.s.MJSLog(ParseString(lineParts[1]));
                                goto nextLine;
                            }
                            #endregion
                            // Foundvar 0 t/m 6:
                            #region GenerateMap LoadMap FindMap UnloadMap SaveMap DeleteMap
                            if (lineParts[0] == "GenerateMap")
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                ushort width, height, depth;
                                i = 0;
                                while (i < 255)
                                {
                                    if (mapVarNames[i] == lineParts[1]) goto foundVar0;
                                    i++;
                                }
                                Server.s.MJSLog("No variable of type map named " + lineParts[1] + " exists.");
                                goto nextLine;
                            foundVar0:
                                if (!System.UInt16.TryParse(lineParts[3], out width)) { lineError = "The map width must be an integer."; goto nextLine; };
                                if (!System.UInt16.TryParse(lineParts[4], out height)) { lineError = "The map height must be an integer."; goto nextLine; };
                                if (!System.UInt16.TryParse(lineParts[5], out depth)) { lineError = "The map depth must be an integer."; goto nextLine; };

                                mapVars[i] = new Level(ParseString(lineParts[2]), width, height, depth, "flat", null);
                                //Event.Trigger("MapGenerate", "string $name " + mapVars[i].name);
                                goto nextLine;
                            }
                            else if (lineParts[0] == "LoadMap")
                            {
                                Level loadedLevel = null;
                                if (lineParts[1].StartsWith("\""))
                                {
                                    lineParts[1] = ParseString(lineParts[1]);
                                    if (!File.Exists("maps/" + lineParts[1] + ".map"))
                                    {
                                        lineError = "File maps/" + lineParts[1] + ".map does not exist.";
                                        goto nextLine;
                                    }
                                    loadedLevel = Level.Load(lineParts[1]);
                                }
                                else// if (lineParts[1].StartsWith("$"))
                                { // damn, somehow the $ is already chopped off somewhere else. quick fix!
                                    //lineParts[1] = lineParts[1].Remove(0, 1);
                                    i = 0;
                                    while (i < 255)
                                    {
                                        if (mapVarNames[i] == lineParts[1]) goto foundVar1;
                                        i++;
                                    }
                                    lineError = "Failed to load map. No variable of type map named " + lineParts[1] + " exists.";
                                    goto nextLine;
                                foundVar1:
                                    Level.Load(mapVars[i].name);
                                }
                                //else
                                //{
                                //    lineError = "Unexpected characters \"" + lineParts[1] + "\"";
                                //    goto nextLine;
                                //}
                                if (lineParts.Length > 2)
                                {
                                    lineParts[2] = lineParts[2].Remove(0, 1);
                                    i = 0;
                                    while (i < 255)
                                    {
                                        if (mapVarNames[i] == lineParts[2]) goto foundVar2;
                                        i++;
                                    }
                                    lineError = "No variable of type map named " + lineParts[2] + " exists.";
                                    goto nextLine;
                                foundVar2:
                                    mapVars[i] = loadedLevel;
                                }
                                goto nextLine;
                            }
                            else if (lineParts[0] == "FindMap")
                            {
                                lineParts[2] = lineParts[2].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (mapVarNames[i] == lineParts[2]) goto foundVar3;
                                    i++;
                                }
                                lineError = "No variable of type map named " + lineParts[2] + " exists.";
                                goto nextLine;
                            foundVar3:
                                mapVars[i] = Level.Find(ParseString(lineParts[1]));
                                if (mapVars[i] == null)
                                {
                                    lineError = "Could not find map " + lineParts[1] + ". No such map is loaded.";
                                }
                                goto nextLine;
                            }
                            else if (lineParts[0] == "UnloadMap")
                            {
                                Level unloadedLevel;
                                if (lineParts[1].StartsWith("\""))
                                {
                                    lineParts[1] = ParseString(lineParts[1]);
                                    if (!File.Exists("maps/" + lineParts[1] + ".map"))
                                    {
                                        lineError = "Failed to unload map. File maps/" + lineParts[1] + ".map does not exist.";
                                        goto nextLine;
                                    }
                                    unloadedLevel = Level.Find(lineParts[1]);
                                    if (unloadedLevel == null)
                                    {
                                        lineError = "Failed to unload map. No map called " + lineParts[1] + " is loaded.";
                                    }
                                }
                                else if (lineParts[1].StartsWith("$"))
                                {
                                    lineParts[2] = lineParts[2].Remove(0, 1);
                                    i = 0;
                                    while (i < 255)
                                    {
                                        if (mapVarNames[i] == lineParts[2]) goto foundVar4;
                                        i++;
                                    }
                                    lineError = "Failed to unload map. No variable of type map named " + lineParts[2] + " exists.";
                                    goto nextLine;
                                foundVar4:
                                    mapVars[i].Unload();
                                }
                                else
                                {
                                    lineError = "Unexpected characters \"" + lineParts[1] + "\"";
                                    goto nextLine;
                                }
                                goto nextLine;
                            }
                            else if (lineParts[0] == "SaveMap")
                            {
                                Level savedLevel;
                                if (lineParts[1].StartsWith("\""))
                                {
                                    lineParts[1] = ParseString(lineParts[1]);
                                    if (!File.Exists("maps/" + lineParts[1] + ".map"))
                                    {
                                        lineError = "Failed to save map. File maps/" + lineParts[1] + ".map does not exist.";
                                        goto nextLine;
                                    }
                                    savedLevel = Level.Find(lineParts[1]);
                                    if (savedLevel == null)
                                    {
                                        lineError = "Failed to save map. No map called " + lineParts[1] + " is loaded.";
                                        goto nextLine;
                                    }
                                    savedLevel.Save();
                                }
                                else if (lineParts[1].StartsWith("$"))
                                {
                                    lineParts[1] = lineParts[1].Remove(0, 1);
                                    i = 0;
                                    while (i < 255)
                                    {
                                        if (mapVarNames[i] == lineParts[1]) goto foundVar5;
                                        i++;
                                    }
                                    lineError = "Failed to save map. No variable of type map named $" + lineParts[1] + " exists.";
                                    goto nextLine;
                                foundVar5:
                                    mapVars[i].Save();
                                }
                                else
                                {
                                    lineError = "Unexpected characters \"" + lineParts[1] + "\"";
                                    goto nextLine;
                                }
                                goto nextLine;
                            }
                            else if (lineParts[0] == "DeleteMap")
                            {
                                if (lineParts[1].StartsWith("\""))
                                {
                                    lineParts[1] = ParseString(lineParts[1]);
                                    if (!File.Exists("levels/" + lineParts[1] + ".lvl") && !File.Exists("levels/" + lineParts[1] + ".mcqlvl")) { lineError = "Error while attempting to delete map " + lineParts[1] + ": It doesnt exist!"; goto nextLine; }
                                    try { Level.Find(lineParts[1]).Unload(); }
                                    catch { }
                                    try { File.Delete("levels/" + lineParts[1] + ".mcqlvl"); }
                                    catch { File.Delete("levels/" + lineParts[1] + ".lvl"); }
                                }
                                else if (lineParts[1].StartsWith("$"))
                                {
                                    lineParts[1] = lineParts[1].Remove(0, 1);
                                    i = 0;
                                    while (i < 255)
                                    {
                                        if (mapVarNames[i] == lineParts[1]) goto foundVar6;
                                        i++;
                                    }
                                    lineError = "Failed to delete map. No variable of type map named " + lineParts[1] + " exists.";
                                    goto nextLine;
                                foundVar6:
                                    try { mapVars[i].Unload(); }
                                    catch { }
                                    try { File.Delete("levels/" + mapVars[i].name + ".mcqlvl"); }
                                    catch { File.Delete("levels/" + mapVars[i].name + ".lvl"); }
                                }
                                else
                                {
                                    lineError = "Unexpected characters \"" + lineParts[1] + "\"";
                                    goto nextLine;
                                }
                                goto nextLine;
                            }
                            #endregion
                            // Foundvar 17 t/m 19:
                            // Foundvar 23 t/m 25:
                            #region KickPlayer MovePlayer TeleportPlayer CommandPlayer
                            if (lineParts[0] == "KickPlayer")
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (playerVarNames[i] == lineParts[1]) goto foundVar17;
                                    i++;
                                }
                                lineError = "Could not find player variable $" + lineParts[1] + ".";
                                goto nextLine;
                            foundVar17:
                                if (lineParts.Length == 3) playerVars[i].Kick(ParseString(lineParts[2]));
                                else if (lineParts.Length == 2) playerVars[i].Kick("");
                            }
                            if (lineParts[0] == "MovePlayer")
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (playerVarNames[i] == lineParts[1]) goto foundVar18;
                                    i++;
                                }
                                lineError = "Could not find player variable $" + lineParts[1] + ".";
                                goto nextLine;
                            foundVar18:
                                Player p = playerVars[i];
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (mapVarNames[i] == lineParts[2]) goto foundVar19;
                                    i++;
                                }
                                lineError = "Could not find map variable $" + lineParts[2] + ".";
                                goto nextLine;
                            foundVar19:
                                string mapName = ParseString(lineParts[2]);
                                Level foundLevel = mapVars[i];
                                if (foundLevel != null)
                                {
                                    Level startLevel = p.level;

                                    GC.Collect();

                                    if (p.level == foundLevel)
                                    {
                                        lineError = "Could not move player " + p.name + " to map " + mapName + ", he is already there.";
                                        goto nextLine;
                                    }
                                    p.Loading = true;
                                    Player.players.ForEach((pl) => { if (p.level == pl.level && p != pl) p.SendDie(pl.id); });
                                    Player.GlobalDie(p, true);
                                    p.level = foundLevel;
                                    //startLevel.players--;
                                    //foundLevel.players++;
                                    p.SendUserMOTD();
                                    p.SendMap();

                                    GC.Collect();

                                    ushort x = (ushort)((0.5 + foundLevel.spawnx) * 32);
                                    ushort y = (ushort)((1 + foundLevel.spawny) * 32);
                                    ushort z = (ushort)((0.5 + foundLevel.spawnz) * 32);

                                    Player.GlobalSpawn(p, x, y, z, (byte)foundLevel.rotx, (byte)foundLevel.roty, true);


                                    Player.players.ForEach((pl) =>
                                    {
                                        if (pl.level == p.level && p != pl) p.SendSpawn(pl.id, "&f" + pl.name, pl.pos[0], pl.pos[1], pl.pos[2], pl.rot[0], pl.rot[1]);
                                    });

                                    p.Loading = false;
                                }
                                else
                                {
                                    lineError = "Could not move player " + p.name + " to map " + mapName + ", no such map is loaded.";
                                }
                                GC.Collect();
                                GC.WaitForPendingFinalizers();
                                goto nextLine;
                            }
                            if (lineParts[0] == "TeleportPlayer")
                            { // unchecked { p.SendPos((byte)-1, who.pos[0], who.pos[1], who.pos[2], who.rot[0], 0); }
                                Player p1, p2;
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (playerVarNames[i] == lineParts[1]) goto foundVar23;
                                    i++;
                                }
                                lineError = "Could not find player variable $" + lineParts[1] + ".";
                                goto nextLine;
                            foundVar23:
                                p1 = playerVars[i];
                                lineParts[2] = lineParts[2].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (playerVarNames[2] == lineParts[2]) goto foundVar24;
                                    i++;
                                }
                                lineError = "Could not find player variable $" + lineParts[2] + ".";
                                goto nextLine;
                            foundVar24:
                                p2 = playerVars[i];
                                unchecked { p1.SendPos(p2.pos[0], p2.pos[1], p2.pos[2], p2.rot[0], 0); }
                            }
                            if (lineParts[0] == "CommandPlayer")
                            {
                                Player p1;
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (playerVarNames[i] == lineParts[1]) goto foundVar25;
                                    i++;
                                }
                                lineError = "Could not find player variable $" + lineParts[1] + ".";
                                goto nextLine;
                            foundVar25:
                                p1 = playerVars[i];
                                Command cmd = Command.all.Find(lineParts[2]);
                                if (cmd != null)
                                {
                                    if (lineParts.Length == 3) cmd.Use(p1, "");
                                    else
                                    {
                                        try
                                        {
                                            cmd.Use(p1, ParseString(lineParts[3]));
                                        }
                                        catch { lineError = "Something went wrong while executing /" + lineParts[2] + " " + lineParts[3]; goto nextLine; }
                                    }
                                }
                                else { lineError = "Command " + lineParts[2] + " does not exist."; goto nextLine; }
                            }

                            #endregion
                            #region ExecuteScript TriggerEvent
                            if (lineParts[0] == "ExecuteScript")
                            {
                                string path = ParseString(lineParts[1]);
                                if (!path.StartsWith("mjs/") && !path.StartsWith("mjs\\")) path = "mjs/" + path;
                                if (!File.Exists(path)) { lineError = "Could not execute script " + path + ". File does not exist."; goto nextLine; }
                                if (lineParts.Length > 2)
                                {
                                    i = 2;
                                    string argumentString = "";
                                    while (i < lineParts.Length)
                                    {
                                        if (!lineParts[i].StartsWith("$")) { lineError = "Failed to execute script because when calling another script, all input must be variables."; goto nextLine; }
                                        lineParts[i] = lineParts[i].Remove(0, 1);
                                        varType = "";
                                        j = 0;
                                        while (j < 255)
                                        {
                                            if (numberVarNames[j] == lineParts[i]) { varType = "number"; goto varFound8; }
                                            if (stringVarNames[j] == lineParts[i]) { varType = "string"; goto varFound8; }
                                            if (playerVarNames[j] == lineParts[i]) { varType = "player"; goto varFound8; }
                                            if (mapVarNames[j] == lineParts[i]) { varType = "map"; goto varFound8; }
                                            j++;
                                        }
                                        lineError = "Could not find variable $" + lineParts[i] + ".";
                                        goto nextLine;
                                    varFound8:
                                        if (varType == "number") argumentString += varType + " $" + lineParts[i] + " " + numberVars[j].ToString();
                                        if (varType == "player") argumentString += varType + " $" + lineParts[i] + " " + playerVars[j].name;
                                        if (varType == "string") argumentString += varType + " $" + lineParts[i] + " " + stringVars[j].Replace(" ", "[space]");
                                        if (varType == "map") argumentString += varType + " $" + lineParts[i] + " " + mapVars[j].name;
                                        i++;
                                        if (i < lineParts[i].Length) argumentString += " ";
                                    }
                                    Execute(path, argumentString);
                                }
                                else Execute(path, "");
                                goto nextLine;
                            }
                            if (lineParts[0] == "TriggerEvent")
                            {
                                string path = ParseString(lineParts[1]);
                                if (!path.StartsWith("mjs/") && !path.StartsWith("mjs/events")) path = "mjs/events/" + path;
                                if (!path.StartsWith("mjs/") && !path.StartsWith("mjs/")) path = "mjs/" + path;
                                if (!path.EndsWith(".mjs")) path += ".mjs";
                                if (!File.Exists(path)) { lineError = "Could not execute script " + path + ". File does not exist."; goto nextLine; }
                                if (lineParts.Length > 2)
                                {
                                    i = 2;
                                    string argumentString = "";
                                    while (i < lineParts.Length)
                                    {
                                        if (!lineParts[i].StartsWith("$")) { lineError = "Failed to execute script because when calling another script, all input must be variables."; goto nextLine; }
                                        lineParts[i] = lineParts[i].Remove(0, 1);
                                        varType = "";
                                        j = 0;
                                        while (j < 255)
                                        {
                                            if (numberVarNames[j] == lineParts[i]) { varType = "number"; goto varFound8; }
                                            if (stringVarNames[j] == lineParts[i]) { varType = "string"; goto varFound8; }
                                            if (playerVarNames[j] == lineParts[i]) { varType = "player"; goto varFound8; }
                                            if (mapVarNames[j] == lineParts[i]) { varType = "map"; goto varFound8; }
                                            j++;
                                        }
                                        lineError = "Could not find variable $" + lineParts[i] + ".";
                                        goto nextLine;
                                    varFound8:
                                        if (varType == "number") argumentString += varType + " $" + lineParts[i] + " " + numberVars[j].ToString();
                                        if (varType == "player") argumentString += varType + " $" + lineParts[i] + " " + playerVars[j].name;
                                        if (varType == "string") argumentString += varType + " $" + lineParts[i] + " " + stringVars[j].Replace(" ", "[space]");
                                        if (varType == "map") argumentString += varType + " $" + lineParts[i] + " " + mapVars[j].name;
                                        i++;
                                        if (i < lineParts[i].Length) argumentString += " ";
                                    }
                                    Execute(path, argumentString);
                                }
                                else Execute(path, "");
                            }
                            #endregion
                            #region If
                            if (lineParts[0].ToLower() == "if")
                            {
                                bool isTrue = false;
                                if (lineParts[1] == "FileExists")
                                {
                                    if (File.Exists(ParseString(lineParts[2]))) isTrue = true;
                                    else isTrue = false;
                                }
                                else if (lineParts[1] == "VarExists")
                                {
                                    if (lineParts[2].StartsWith("player."))
                                    {
                                        lineParts[2] = lineParts[2].Remove(0, 7);
                                        /////isTrue = Player.variableNames.Contains(lineParts[2]);
                                        goto done101010;
                                    }
                                    if (lineParts[2].StartsWith("\"")) lineParts[2] = ParseString(lineParts[2]);
                                    if (lineParts[2].StartsWith("$")) lineParts[2] = lineParts[2].Remove(0, 1);
                                    i = 0;
                                    while (i < XMath.Max(mapVarCount, playerVarCount, stringVarCount, numberVarCount))
                                    {
                                        if (mapVarNames[i] == lineParts[2]) { isTrue = true; goto done101010; }
                                        if (stringVarNames[i] == lineParts[2]) { isTrue = true; goto done101010; }
                                        if (playerVarNames[i] == lineParts[2]) { isTrue = true; goto done101010; }
                                        if (numberVarNames[i] == lineParts[2]) { isTrue = true; goto done101010; }
                                        i++;
                                    }
                                    isTrue = false;
                                done101010:
                                    i = 0;
                                }
                                else if (lineParts[1] == "DirectoryExists")
                                {
                                    if (Directory.Exists(ParseString(lineParts[2]))) isTrue = true;
                                    else isTrue = false;
                                }
                                else if (lineParts[1] == "MapExists")
                                {
                                    if (File.Exists("levels/" + ParseString(lineParts[2]) + ".mcqlvl") || File.Exists("levels/" + ParseString(lineParts[2]) + ".lvl")) isTrue = true;
                                    else isTrue = false;
                                }
                                else if (lineParts[1] == "MapLoaded")
                                {
                                    if (Level.Find(ParseString(lineParts[2])) == null) isTrue = false;
                                    else isTrue = true;
                                }
                                else if (lineParts.Length >= 3 && (lineParts[2] == "<" || lineParts[2] == ">" || lineParts[2] == ">=" || lineParts[2] == "<="))
                                {
									if (lineParts[2]==">")
                                    {
                                        if (int.Parse(lineParts[1])>int.Parse(lineParts[3])) isTrue = true;
                                        else isTrue = false;
                                    }
									else if (lineParts[2]=="<")
                                    {
                                        if (int.Parse(lineParts[1])<int.Parse(lineParts[3])) isTrue = true;
                                        else isTrue = false;
                                    }
									else if (lineParts[2]=="<=")
                                    {
                                        if (int.Parse(lineParts[1])<=int.Parse(lineParts[3])) isTrue = true;
                                        else isTrue = false;
                                    }
									else if (lineParts[2]==">=")
                                    {
                                        if (int.Parse(lineParts[1])>=int.Parse(lineParts[3])) isTrue = true;
                                        else isTrue = false;
                                    }
								}
                                else if (lineParts.Length >= 3 && (lineParts[2] == "==" || lineParts[2] == "!="))
                                {
                                    if (lineParts[1].StartsWith("\""))
                                    {
                                        //Server.s.MJSLog("lololol");
                                        //Arr.Print(lineParts);
                                        if (lineParts[1] == lineParts[3]) isTrue = true;
                                        else isTrue = false;
                                    }
                                    else if (System.Double.TryParse(lineParts[1], out parseTryer))
                                    {
                                        if (lineParts[1] == lineParts[3]) isTrue = true;
                                        else isTrue = false;
                                    }
                                    else if (lineParts[1].StartsWith("$"))
                                    {
                                        lineParts[1] = lineParts[1].Remove(0, 1);
                                        lineParts[3] = lineParts[3].Remove(0, 1);
                                        i = 0;
                                        varType = "";
                                        while (i < 255)
                                        {
                                            if (lineParts[1] == mapVarNames[i]) { varType = "map"; goto varFound2001; }
                                            if (lineParts[1] == playerVarNames[i]) { varType = "player"; goto varFound2001; }
                                            i++;
                                        }
                                        lineError = "Could not find variable $" + lineParts[1] + "..... You really depressed the interpreter here. :(";
                                        goto nextLine;
                                    varFound2001:
                                        j = 0;
                                        string varType2 = "";
                                        while (j < 255)
                                        {
                                            if (lineParts[3] == mapVarNames[j]) { varType = "map"; goto varFound2002; }
                                            if (lineParts[3] == playerVarNames[j]) { varType = "player"; goto varFound2002; }
                                            j++;
                                        }
                                        lineError = "Could not find variable $" + lineParts[3] + "..... You really depressed the interpreter here.";
                                        goto nextLine;
                                    varFound2002:
                                        if (varType != varType2)
                                        {
                                            lineError = "You cant compare a map with a player. Add .$map after a player variable to get the map he/she is in.";
                                            goto nextLine;
                                        }
                                        if (varType == "map" && mapVars[i] == mapVars[j]) isTrue = true;
                                        else if (varType == "map") isTrue = false;
                                        else if (playerVars[i] == playerVars[j]) isTrue = true;
                                        else isTrue = false;
                                    }
                                    else
                                    {
                                        lineError = "Syntax error. Expected a serious value to compare, but got some random characters... :(";
                                    }
                                    if (lineParts[2] == "!=") isTrue ^= true;
                                    //goto nextLine;
                                }
                                else if (lineParts[1] == "FileContainsLine")
                                {
                                    lineParts[2] = ParseString(lineParts[2]);
                                    lineParts[3] = ParseString(lineParts[3]);
                                    if (!File.Exists(lineParts[2])) { lineError = "Could not open file " + lineParts[2] + ", it does not exist!"; goto nextLine; }
                                    if (File.ReadAllLines(lineParts[2]).Contains(lineParts[3])) isTrue = true;
                                    else isTrue = false;
                                }
                                else if (lineParts[1] == "FileContainsText" || lineParts[1] == "FileContainsString")
                                {
                                    lineParts[2] = ParseString(lineParts[2]);
                                    lineParts[3] = ParseString(lineParts[3]);
                                    if (!File.Exists(lineParts[2])) { lineError = "Could not open file " + lineParts[2] + ", it does not exist!"; goto nextLine; }
                                    if (File.ReadAllText(lineParts[2]).Contains(lineParts[3])) isTrue = true;
                                    else isTrue = false;
                                }
                                else if (lineParts[1] == "StringContains" || lineParts[1] == "StringContainsString")
                                {
                                    if (ParseString(lineParts[2]).Contains(ParseString(lineParts[3]))) isTrue = true;
                                    else isTrue = false;
                                }
                                else if (lineParts[1] == "StringStartsWith" || lineParts[1] == "StringStartsWithString")
                                {
                                    if (ParseString(lineParts[2]).StartsWith(ParseString(lineParts[3]))) isTrue = true;
                                    else isTrue = false;
                                }
                                else if (lineParts[1] == "StringEndsWith" || lineParts[1] == "StringEndsWithString")
                                {
                                    if (ParseString(lineParts[2]).StartsWith(ParseString(lineParts[3]))) isTrue = true;
                                    else isTrue = false;
                                }
                                else if (lineParts[1] == "PlayerConnected")
                                {
                                    if (Player.Find(ParseString(lineParts[2])) == null) isTrue = false;
                                    else isTrue = true;
                                }
                                block_type[curBlock + 1] = "if";
                                isTrue ^= ifNot; // I know, I'm a binary logic geek, and I really love Xor gates. :')
                                block_executeElse[curBlock] = !isTrue; // Because when the statement is true, it should execute the if block, not the else block.
                                goto nextLine;
                            }
                            #endregion
                            #region CreateDirectory CreateFile DeleteDirectory DeleteFile FileWriteLine FileRemoveLine
                            if (lineParts[0] == "CreateDirectory")
                            {
                                lineParts[1] = ParseString(lineParts[1]);
                                if (Directory.Exists(lineParts[1]))
                                {
                                    lineError = "Cant create directory " + lineParts[1] + ", it already exists!";
                                }
                                else
                                {
                                    Directory.CreateDirectory(lineParts[1]);
                                }
                                goto nextLine;
                            }
                            if (lineParts[0] == "CreateFile")
                            {
                                lineParts[1] = ParseString(lineParts[1]);
                                if (File.Exists(lineParts[1]))
                                {
                                    lineError = "Cant create file " + lineParts[1] + ", it already exists!";
                                }
                                else
                                {
                                    File.Create(lineParts[1]);
                                }
                                goto nextLine;
                            }
                            if (lineParts[0] == "DeleteDirectory")
                            {
                                lineParts[1] = ParseString(lineParts[1]);
                                if (!Directory.Exists(lineParts[1]))
                                {
                                    lineError = "Cant delete directory " + lineParts[1] + ", it doesn't exists!";
                                }
                                else
                                {
                                    Directory.Delete(lineParts[1], true);
                                }
                                goto nextLine;
                            }
                            if (lineParts[0] == "DeleteFile")
                            {
                                lineParts[1] = ParseString(lineParts[1]);
                                if (!File.Exists(lineParts[1]))
                                {
                                    lineError = "Cant delete file " + lineParts[1] + ", it doesn't exists!";
                                }
                                else
                                {
                                    File.Delete(lineParts[1]);
                                }
                                goto nextLine;
                            }
                            if (lineParts[0] == "TruncateFile")
                            {
                                lineParts[1] = ParseString(lineParts[1]);
                                if (!File.Exists(lineParts[1]))
                                {
                                    lineError = "Cant truncate file " + lineParts[1] + ", it doesn't exists!";
                                }
                                else
                                {
                                    File.Open(lineParts[1], FileMode.Truncate).Close();
                                }
                                goto nextLine;
                            }
                            if (lineParts[0] == "TruncateDirectory")
                            {
                                lineParts[1] = ParseString(lineParts[1]);
                                if (!Directory.Exists(lineParts[1]))
                                {
                                    lineError = "Cant truncate directory " + lineParts[1] + ", it doesn't exists!";
                                }
                                else
                                {
                                    Directory.Delete(lineParts[1], true);
                                    Directory.CreateDirectory(lineParts[1]);
                                }
                                goto nextLine;
                            }
                            if (lineParts[0] == "FileWriteLine")
                            {
                                lineParts[1] = ParseString(lineParts[1]);
                                lineParts[2] = ParseString(lineParts[2]);
                                if (!File.Exists(lineParts[1]))
                                {
                                    lineError = "Cant write to file " + lineParts[1] + ", it doesn't exists!";
                                }
                                else
                                {
                                    StreamWriter writer = new StreamWriter(File.Open(lineParts[1], FileMode.Append));
                                    writer.WriteLine(lineParts[2]);
                                    writer.Flush();
                                    writer.Close();
                                }
                                goto nextLine;
                            }
                            if (lineParts[0] == "FileRemoveLine")
                            {
                                lineParts[1] = ParseString(lineParts[1]);
                                lineParts[2] = ParseString(lineParts[2]);
                                if (!File.Exists(lineParts[1]))
                                {
                                    lineError = "Cant change file " + lineParts[1] + ", it doesn't exists!";
                                }
                                else
                                {
                                    string[] the_lines = File.ReadAllLines(lineParts[1]);
                                    File.Delete(lineParts[1]);
                                    File.Create(lineParts[1]);
                                    StreamWriter writer = new StreamWriter(File.Open(lineParts[1], FileMode.Append));
                                    foreach (string a_line in the_lines)
                                    {
                                        if (a_line != lineParts[2]) writer.WriteLine(lineParts[2]);
                                    }
                                    writer.Flush();
                                    writer.Close();
                                }
                                goto nextLine;
                            }
                            #endregion
                            #region SetBlock GetBlock SendBlock
                            if (lineParts[0] == "SetBlock")
                            {
                                Level theLevel;
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (mapVarNames[i] == lineParts[1]) { theLevel = mapVars[i]; goto varFound004; }
                                    i++;
                                }
                                lineError = "Failed to set block, could not find map variable $" + lineParts[1] + ".";
                                goto nextLine;
                            varFound004:
                                try
                                {
                                    theLevel.SetTile(System.UInt16.Parse(lineParts[2]), System.UInt16.Parse(lineParts[3]), System.UInt16.Parse(lineParts[4]), Convert.ToByte(System.Int16.Parse(lineParts[5])));
                                }
                                catch
                                {
                                    lineError = "Failed to set block. Probably failed to parse one of the integers.";
                                    goto nextLine;
                                }
                                goto nextLine;
                            }
                            if (lineParts[0] == "GetBlock")
                            {
                                Level theLevel;
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (mapVarNames[i] == lineParts[1]) { theLevel = mapVars[i]; goto varFound9; }
                                    i++;
                                }
                                lineError = "Failed to get block, could not find map variable $" + lineParts[1] + ".";
                                goto nextLine;
                            varFound9:
                                try
                                {
                                    theLevel.GetTile(System.UInt16.Parse(lineParts[2]), System.UInt16.Parse(lineParts[3]), System.UInt16.Parse(lineParts[4]));
                                }
                                catch
                                {
                                    lineError = "Failed to set block. Probably failed to parse one of the integers.";
                                    goto nextLine;
                                }
                            }
                            if (lineParts[0] == "SendBlock")
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (playerVarNames[i] == lineParts[1]) goto varFound006;
                                    i++;
                                }
                                lineError = "Could not find player variable $" + lineParts[1] + ".";
                                goto nextLine;
                            varFound006:
                                try
                                {
                                    playerVars[i].SendBlockchange((ushort)System.Int16.Parse(lineParts[2]), (ushort)System.Int16.Parse(lineParts[3]), (ushort)System.Int16.Parse(lineParts[4]), Convert.ToByte(System.Int16.Parse(lineParts[5])));
                                }
                                catch
                                {
                                    lineError = "Failed to send block. Probably failed to parse one of the integers.";
                                    goto nextLine;
                                }
                            }
                            #endregion
                            // Foundvar 20 t/m 22:
                            #region MakeLowerCase MakeUpperCase Replace
                            if (lineParts[0] == "MakeLowerCase")
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (stringVarNames[i] == lineParts[1]) { goto foundVar20; }
                                    i++;
                                }
                                lineError = "Could not find string variable $" + lineParts[1];
                                goto nextLine;
                            foundVar20:
                                stringVars[i] = stringVars[i].ToLower();
                                goto nextLine;
                            }
                            if (lineParts[0] == "MakeUpperCase")
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (stringVarNames[i] == lineParts[1]) { goto foundVar21; }
                                    i++;
                                }
                                lineError = "Could not find string variable $" + lineParts[1];
                                goto nextLine;
                            foundVar21:
                                stringVars[i] = stringVars[i].ToUpper();
                                goto nextLine;
                            }
                            if (lineParts[0] == "Replace")
                            {
                                lineParts[1] = lineParts[1].Remove(0, 1);
                                i = 0;
                                while (i < 255)
                                {
                                    if (stringVarNames[i] == lineParts[1]) { goto foundVar22; }
                                    i++;
                                }
                                lineError = "Could not find string variable $" + lineParts[1];
                                goto nextLine;
                            foundVar22:
                                stringVars[i] = stringVars[i].Replace(ParseString(lineParts[2]),ParseString(lineParts[3]));
                                goto nextLine;
                            }
                            #endregion
                            #region Sleep Wait Exit
                            if (lineParts[0].ToLower() == "sleep" || lineParts[0].ToLower() == "wait")
                            {
                                try
                                {
                                    Thread.Sleep(System.Int16.Parse(lineParts[1]));
                                }
                                catch
                                {
                                    lineError = "Sleeping failed: Could not parse integer " + lineParts[1];
                                }
                                goto nextLine;
                            }
                            if (lineParts[0].ToLower() == "exit")
                            {
                                goto stopScript;
                            }
                            #endregion
                        }
                        lineError = "Unknown command " + lineParts[0];
                    nextLine:
                        lineNum++;
                        if (lineError != "")
                        {
                            if (inTryBlock)
                            {
                                skipToCatch = true;
                                curBlock = tryCurBlock;
                            }
                            else if (!fatalError) Server.s.MJSLog("Non-fatal error in " + file + " on line " + lineNum + ": " + lineError);
                            else { Server.s.MJSLog("Fatal error in " + file + " on line " + lineNum + ": " + lineError); goto stopScript; }
                        }
                        //Server.s.MJSLog("Line " + (lineNum-1) + " ends with curBlock " + curBlock + ".");
                    }
                    catch (Exception e)
                    {
                        lineNum++;
                        if (inTryBlock)
                        {
                            skipToCatch = true;
                            curBlock = tryCurBlock;
                        }
                        else
                        {
                            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(e, true);
                            Server.s.MJSLog("Non-fatal but unknown error in " + file + " on line " + lineNum + ": " + e.Message);
                            Server.s.MJSLog("Line: " + trace.GetFrame(0).GetFileLineNumber() + " Column: " + trace.GetFrame(0).GetFileColumnNumber());
                        }
                    }
                }
            stopScript:
                i = 0;
                if (deleteWhenFinished) File.Delete(file);
                ScriptThreads.Remove(Thread.CurrentThread);
            }));
            ScriptThreads.Add(scriptThread);
            scriptThread.Start();
            return;
        }
        public static string ParseString(string str)
        {
            return str.Remove(str.Length - 1, 1).Remove(0, 1);
        }
        public static void WriteExampleScripts()
        {
            if (!File.Exists("mjs/commands/aboutMJS.mjs"))
            {
                StreamWriter sw = new StreamWriter(File.Create("mjs/commands/aboutMJS.mjs"));
                sw.WriteLine("PlayerMessage $player \"Dear $player.$name\", you just used a command written in MJS.");
                sw.WriteLine("PlayerMessage $player \"MJS is an easy-to-learn minecraft classic command and event scripting language.\"");
                sw.WriteLine("PlayerMessage $player \"A guide on MJS is under construction on mcrevive.tk\"");
                sw.Flush();
                sw.Close();
            }
        }
        public static void ReloadMJS()
        {
            Server.s.Log("Force-killing all " + ScriptThreads.Count + " running MJS scripts...");
            ScriptThreads.ForEach((t) =>
            {
                t.Abort();
            });
            ScriptThreads = new List<Thread>();
            Server.s.Log("Re-triggering ServerStart event...");
            Event.Trigger("ServerStart","");
            Server.s.Log("MJS reloaded.");
        }
    }
}