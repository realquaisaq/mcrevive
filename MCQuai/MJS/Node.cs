using System;

namespace MCRevive.MJS
{
  public class Node
  {
    public static int TYPE_VARIABLE = 1;
    public static int TYPE_CONSTANT = 2;
    public static int TYPE_EXPRESSION = 3;
    public static int TYPE_END = 4;
    public static int TYPE_UNDEFINED = -1;
    private string _operator = "";
    private int type = Node.TYPE_UNDEFINED;
    private double value = double.NaN;
    private string variable = "";
    private Node _arg1;
    private Node _arg2;
    private int args;

    static Node()
    {
    }

    public Node(string _operator, Node _arg1, Node _arg2)
    {
      this._arg1 = _arg1;
      this._arg2 = _arg2;
      this._operator = _operator;
      this.args = 2;
      this.type = Node.TYPE_EXPRESSION;
    }

    public Node(string _operator, Node _arg1)
    {
      this._arg1 = _arg1;
      this._operator = _operator;
      this.args = 1;
      this.type = Node.TYPE_EXPRESSION;
    }

    public Node(string variable)
    {
      this.variable = variable;
      this.type = Node.TYPE_VARIABLE;
    }

    public Node(double value)
    {
      this.value = value;
      this.type = Node.TYPE_CONSTANT;
    }

    public string getOperator()
    {
      return this._operator;
    }

    public double getValue()
    {
      return this.value;
    }

    public string getVariable()
    {
      return this.variable;
    }

    public int arguments()
    {
      return this.args;
    }

    public int getType()
    {
      return this.type;
    }

    public Node arg1()
    {
      return this._arg1;
    }

    public Node arg2()
    {
      return this._arg2;
    }
  }
}
