﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace MCRevive.MJS
{
    static class Event
    {
        public static bool Trigger(string Event, string Parameters)
        {
            Server.s.MJSLog("MJS --> Event " + Event + " was triggered with parameters: " + Parameters);
            if (!Exists(Event)) return false;
            Script.Execute("mjs/events/" + Event+ ".mjs", Parameters);
            return true;
        }
        public static bool Exists(string Event)
        {
            return File.Exists("mjs/events/" + Event + ".mjs");
        }
    }
}
