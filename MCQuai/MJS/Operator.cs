using System;

namespace MCRevive.MJS
{
  public class Operator
  {
    private string op = "";
    private int prec = int.MaxValue;
    private int args;

    public Operator(string _operator, int arguments, int precedence)
    {
      this.op = _operator;
      this.args = arguments;
      this.prec = precedence;
    }

    public int precedence()
    {
      return this.prec;
    }

    public string getOperator()
    {
      return this.op;
    }

    public int arguments()
    {
      return this.args;
    }
  }
}