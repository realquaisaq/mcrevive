﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;
using System.Text;
using System.Threading;

namespace MCRevive
{
    public static class Heartbeat
    {
        static string staticVars;
        static int heartbeats = 0;
        static System.Timers.Timer heartbeatTimer = new System.Timers.Timer(55000);

        public static void Init()
        {
            staticVars = "port=" + Server.port +
                            "&max=" + Server.players +
                            "&name=" + UrlEncode(Server.name) +
                            "&public=" + Server.pub +
                            "&version=" + Server.version;

            Thread backupThread = new Thread(new ThreadStart(delegate
            {
                heartbeatTimer.Elapsed += delegate
                {
                    heartbeats++;
                    try { Pump(Beat.Minecraft); }
                    catch (Exception e) { Server.ErrorLog(e); }
                    try { Pump(Beat.ClassiCube); }
                    catch (Exception e) { Server.ErrorLog(e); }
                    try { Pump(Beat.MCRevive); }
                    catch (Exception e) { Server.ErrorLog(e); }
                };
                heartbeatTimer.Start();

                try { Pump(Beat.Minecraft); }
                catch (Exception e) { Server.ErrorLog(e); }
                try { Pump(Beat.ClassiCube); }
                catch (Exception e) { Server.ErrorLog(e); }
                try { Pump(Beat.MCRevive); }
                catch (Exception e) { Server.ErrorLog(e); }
                heartbeats++;
            }));
            backupThread.Start();
        }

        public static bool Pump(Beat type)
        {
            string getVars = "";
            string url = type == Beat.Minecraft ? "https://minecraft.net/heartbeat.jsp?" : type == Beat.ClassiCube ? "http://www.classicube.net/heartbeat.jsp?" : "http://mcrevive.tk/tools/heartbeat.php?";
            int totalTries = 0;

        retry:
            try
            {
                if (type == Beat.MCRevive)
                {
                    getVars = "sname=" + UrlEncode(Server.name)
                        + "&maxp=" + Server.players
                        + "&nowp=" + Player.number
                        + "&owner=" + UrlEncode(Server.TheOwner)
                        + "&desc=" + UrlEncode(File.ReadAllLines("text/description.txt")[0])
                        + "&public=" + Server.pub
                        + "&version=" + Server.Version
                        + "&URL=" + Server.URL.Trim();
                }
                else
                    getVars = staticVars + "&salt=" + Server.salt + "&users=" + Player.number;

                string content = Server.DownloadString(url + getVars);

                if (String.IsNullOrEmpty(content))
                {
                    Server.s.Log("Server was not able to recieve the URL - " + type.ToString());
                    return false;
                }
                else if ((content.Contains(" ") || content.Contains("<")) && type == Beat.Minecraft)
                {
                    Server.s.Log("It seems that minecraft.net is offline.");
                    Server.s.Log("Because of this, your server is not accessible,");
                    Server.s.Log("until minecraft.net is back online.");
                    Server.s.Log("Though, Direct Connect and ClassiCube users are still able to connect.");
                    return false;
                }

                if (type == Beat.Minecraft)
                {
                    Server.s.UpdateUrl(content);
                    File.WriteAllText("text/externalurl.txt", content);
                    Server.URL = content.Substring(content.LastIndexOf('/') + 1);
                }
                return true;
            }
            catch (Exception ex)
            {
                totalTries++;
                if (totalTries < 3)
                    goto retry;
                Server.ErrorLog(ex);
                Server.s.Log("Heartbeat failed (" + type.ToString() + "), no internet connection?");
                return false;
            }
            
        }

        public static string UrlEncode(string input)
        {
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
            {
                if ((input[i] >= '0' && input[i] <= '9') ||
                    (input[i] >= 'a' && input[i] <= 'z') ||
                    (input[i] >= 'A' && input[i] <= 'Z') ||
                    input[i] == '-' || input[i] == '_' || input[i] == '.' || input[i] == '~')
                {
                    output.Append(input[i]);
                }
                else if (Array.IndexOf<char>(reservedChars, input[i]) != -1)
                {
                    output.Append('%').Append(((int)input[i]).ToString("X"));
                }
            }
            return output.ToString();
        }
        public static char[] reservedChars = { ' ', '!', '*', '\'', '(', ')', ';', ':', '@', '&', 
                                                 '=', '+', '$', ',', '/', '?', '%', '#', '[', ']' };
    }

    public enum Beat { Minecraft, MCRevive, ClassiCube }
}