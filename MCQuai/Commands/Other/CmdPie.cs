using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdPie : Command
    {
        public override string name { get { return "pie"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdPie() { }
        struct PlAndTime { public Player p; public DateTime dt; }
        List<PlAndTime> players = new List<PlAndTime>();
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
			Player player1 = Player.Find(message);
            if (player1 == null || player1.hidden)
            {
                Player.SendMessage(p, "Could not find player specified.");
				return;
            }
            if (players.Find(pa => pa.p == player1).p == player1 && players.Find(pa => pa.p == player1).dt > DateTime.Now)
            {
                Player.SendMessage(p, "you cannot pie anyone yet, you will have to wait " + (players.Find(pa => pa.p == player1).dt
                    - DateTime.Now).ToString().Remove(0, 7).Remove(1) + " more seconds, before you can use this command again.");
                return;
            }
            if (players.Find(pa => pa.p == player1).p != null)
                players.Remove(players.Find(pa => pa.p == player1));

            PlAndTime pat = new PlAndTime();
            pat.p = player1;
            pat.dt = DateTime.Now.AddSeconds(10);
            players.Add(pat);
			string pie = (p == null) ? "Console" : p.color + p.name + Server.DefaultColor;
            foreach (Player all in Player.players)
                if (all == player1)
                    Player.SendMessage(all, pie + " just pied you in the face! pie them back using \"/pie " + p.name + "\"");
                else
                    Player.SendMessage(all, pie + " just pied " + player1.color + player1.name + Server.DefaultColor + " in the face.");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/pie <name> - Pie a friend in the face.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}