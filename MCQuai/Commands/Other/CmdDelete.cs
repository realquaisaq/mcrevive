using System;

namespace MCRevive
{
    public class CmdDelete : Command
    {
        public override string name { get { return "delete"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdDelete() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            p.deletemode = !p.deletemode;
            if (p.deletemode)
                Player.SendMessage(p, "Delete mode: &aON");
            else
                Player.SendMessage(p, "Delete mode: &4OFF");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/delete - Deletes all blocks touched.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}