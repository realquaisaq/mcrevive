﻿using System;
using System.IO;

namespace MCRevive
{
    class CmdSendMap : Command
    {
        public override string name { get { return "sendmap"; } }
        public override string shortcut { get { return "sm"; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdSendMap() { }
        public override void Use(Player p, string message)
        {
            if (message == "" || message.Split(' ').Length > 2) { Help(p); return; }
            else if (message.Split(' ').Length == 1) { Command.all.Find("move").Use(p, message + " " + p.level.name); return; }

            Player who = Player.Find(message.Split(' ')[0]);
            if (who == null) { Player.SendMessage(p, "Could not find player: " + message.Split(' ')[0]); return; }
            Level lvl = Level.Find(message.Split(' ')[1]);
            if (lvl == null)
            {
                if (File.Exists("levels/" + message.Split(' ')[1] + ".mcqlvl") || File.Exists("levels/" + message.Split(' ')[1] + ".lvl"))
                    Command.all.Find("load").Use(p, message.Split(' ')[1]);
                lvl = Level.Find(message.Split(' ')[1]);
                if (lvl == null) { Player.SendMessage(p, "Could not find map: " + message.Split(' ')[1]); return; }
            }

            Command.all.Find("move").Use(p, who.name + " " + lvl.name);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/sendmap <player> [map] - Sends <player> to [map].");
            Player.SendMessage(p, "If [map] is not set, it will be the one you are in.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/sendmap quaisaq main'",
                "This will send quaisaq to the map main.",
                "",
                "&3'/sendmap quaisaq'",
                "This will send quaisaq to the map you are in."
            };
        }
    }
}
