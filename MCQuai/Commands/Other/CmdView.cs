using System;
using System.Collections.Generic;
using System.IO;

namespace MCRevive
{
    public class CmdView : Command
    {
        public override string name { get { return "view"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdView() { }
        public override void Use(Player p, string message)
        {
            if (message == "" && !p.viewing) { Help(p); return; }

            if (!p.viewing && message != "")
            {
                Level lvl = p.level;
                foreach (string b in File.ReadAllText("memory/blocks/" + lvl.name + ".txt").Split((char)15))
                    try
                    {
                        p.SendBlockchange((ushort)b[0], (ushort)b[1], (ushort)b[2], b.Split((char)32002).Length == 3 ? Block.red : Block.green);
                    }
                    catch { Player.SendMessage(p, "Failed to get a block."); }
                p.viewing = true;
            }
            else if (p.viewing && message == "")
            {
                Command.all.Find("reveal").Use(p, "");
                Player.SendMessage(p, "No longer viewing anyones blocks");
                while (p.Loading) { }
                p.SendPos(p.pos[0], p.pos[1], p.pos[2], p.rot[0], 0);
                p.viewing = false;
            }
            else
            {
                p.viewing = false;
                ushort[] pos = new ushort[] { p.pos[0], p.pos[1], p.pos[2], p.rot[0], p.rot[1] };
                Command.all.Find("reveal").Use(p, "");
                while (p.Loading) { }
                Command.all.Find("view").Use(p, message);
                p.SendPos(pos[0], pos[1], pos[2], (byte)pos[3], (byte)pos[4]);
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/view [name] - shows all blocks made by [name].");
            Player.SendMessage(p, "Placed blocks are green, while deleted blocks are red.");
            Player.SendMessage(p, "The name needs to be perfectly entered.");
            Player.SendMessage(p, "Use '/view' again to stop viewing [name]'s blocks.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}