/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;

namespace MCRevive
{
    public class CmdSummon : Command
    {
        public override string name { get { return "summon"; } }
        public override string shortcut { get { return "s"; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdSummon() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            if (message == "all")
            {
                foreach (Player all in Player.players)
                {
                    if (p.level != all.level) { Command.all.Find("goto").Use(all, p.level.name); while (all.Loading) { } }
                    all.SendPos(p.pos[0], p.pos[1], p.pos[2], p.rot[0], 0);
                    Player.SendMessage(all, "You were sumoned by " + p.color + p.name + Server.DefaultColor + ".");
                }
            }
            else
            {
                Player who = Player.Find(message);
                if (who == null) { Player.SendMessage(p, "Couldn't find player \"" + message + "\"!"); return; }
                if (p.level != who.level) { Command.all.Find("goto").Use(who, p.level.name); while (who.Loading) { } }
                who.SendPos(p.pos[0], p.pos[1], p.pos[2], p.rot[0], 0);
                Player.SendMessage(who, "You were summoned by " + p.color + p.name + Server.DefaultColor + ".");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/summon <player> - Summons a player to your position.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}