namespace MCQuai
{
	public class CmdRedstone : Command
	{
		public override string name { get { return "redstone"; } }
		public override string shortcut { get { return ""; } }
		public override string type { get { return "other"; } }
		public override int defaultRank { get { return LevelPermission.Builder; } }
		public CmdRepeat() { }
		Use(Player p, string message)
		{
			if (message == "")
			{
				if (p.mode == {insert redstone mode here})
					p.mode = {insert normal mode here};
				else
					p.mode = {insert redstone mode here};
				
				if (p.mode == {insert redstone mode here})
				{
					Player.SendMessage(p, "You will now place redstone blocks.");
					Player.SendMessage(p, "For a list of blocks, use \"/redstone blocks\".");
				}
				else
				{
					Player.SendMessage(p, "You will now place normal blocks.");
				}
			}
			else if (message == "blocks")
			{
				Player.SendMessage(p, "&4Wire - Red");
				Player.SendMessage(p, "&6Torch - Gold");
				Player.SendMessage(p, "&7Repeater - Step");
				Player.SendMessage(p, "&?Piston - ");
				Player.SendMessage(p, "&?Button - ");
				Player.SendMessage(p, "&?Lever - ");
			}
			else
				Help(p);
		}
		Help(Player p)
		{
			Player.SendMessage(p, "/redstone - Determines whether placing redstone blocks or not.");
			Player.SendMessage(p, "/redstone <\"blocks\"> - Shows the list of all redstone blocks.");
		}
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
	}
}