using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class CmdNick : Command
    {
        public override string name { get { return "nick"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdNick() { }
        public override void Use(Player p, string message)
        {
            bool stealth = false;
            if (message.Substring(0, 2) == "# ") message = "#" + message.Remove(0, 2);
            if (message[0] == '#') { stealth = true; message = message.Remove(0, 1); }
            if (message.Split(' ').Length != 2 || message == "") { Help(p); return; }
            Player who = Player.Find(message.Split(' ')[0]); String newName = "";

            newName = message.Split(' ')[1].Trim();
            if (who == null) { Player.SendMessage(p, "Player: " + message.Split(' ')[0] + " doesn't exist."); return; }
            if (Player.checkDevS(message)) { Player.SendMessage(p, "Woops, looks like someone wants to be famous. HUH?!?"); }

            string lastName = who.name;
            who.name = newName;
            if (!stealth) Player.GlobalChat(p, who.color + lastName + Server.DefaultColor + "'s name was changed to " + who.color + who.name, false);
            Player.GlobalDie(who, false);
            Player.GlobalSpawn(who, who.pos[0], who.pos[1], who.pos[2], who.rot[0], who.rot[1], false);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/nick [#]<player> <newName> - Changes the person's name.");
            Player.SendMessage(p, "If \"#\" is added, it will be a stealth nicking.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}