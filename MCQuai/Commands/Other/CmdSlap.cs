using System;

namespace MCRevive
{
    public class CmdSlap : Command
    {
        public override string name { get { return "slap"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdSlap() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            Player who = Player.Find(message);
            if (who == null)
            {
                Player.SendMessage(p, "Unable to find player: " + message);
                return;
            }
            if (p != null)
            {
                if (who.group.Permission > p.group.Permission && !p.ignorePermission)
                {
                    Player.SendMessage(p, "You cannot slap people above your rank!");
                    return;
                }
            }
            ushort yToGoto = 0;
            ushort x = (ushort)(who.pos[0] / 32);
            ushort y = (ushort)(who.pos[1] / 32);
            ushort z = (ushort)(who.pos[2] / 32);
            while (y <= p.level.depth)
            {
                if (!Block.Walkthrough(p.level.GetTile(x, y, z)) || p.level.GetTile(x, y, z) == Block.Zero)
                {
                    yToGoto = (ushort)(y - 1);
                    goto foundHeight;
                }
                y++;
            }
            foundHeight:
            Player.GlobalMessage(who.color + who.name + Server.DefaultColor + " was slapped &aupwards " + Server.DefaultColor + "by " + p.color + p.name);
            if (yToGoto==0) yToGoto = (ushort)(p.level.depth);
            who.slapped++;
            p.slapper++;
            who.SendPos(who.pos[0], (ushort)(yToGoto * 32), who.pos[2], who.rot[0], who.rot[1]);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/slap <player> - In case you really hate 'em.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}