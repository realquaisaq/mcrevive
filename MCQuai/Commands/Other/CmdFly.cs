using System;
using System.Collections.Generic;
using System.Threading;

namespace MCRevive
{
    class CmdFly : Command
    {
        public override string name { get { return "fly"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdFly() { }
        public override void Use(Player p, string message)
        {
            if (message != "" && message == "water") Help(p);
            p.isFlying = !p.isFlying;
            if (!p.isFlying) return;

            Player.SendMessage(p, "YOU CAN FLY! OMG!");
            p.flys++;

            Thread flyThread = new Thread(new ThreadStart(delegate
            {
                Pos pos;
                List<Pos> buffer = new List<Pos>();
                while (p.isFlying)
                {
                    Thread.Sleep(20);
                    try
                    {
                        List<Pos> tempBuffer = new List<Pos>();

                        ushort x = (ushort)((p.pos[0]) / 32);
                        ushort y = (ushort)((p.pos[1] - 60) / 32);
                        ushort z = (ushort)((p.pos[2]) / 32);

                        try
                        {
                            for (ushort xx = (ushort)(x - 2); xx <= x + 2; xx++)
                            {
                                for (ushort yy = (ushort)(y - 1); yy <= y + 1; yy++)
                                {
                                    for (ushort zz = (ushort)(z - 2); zz <= z + 2; zz++)
                                    {
                                        if (p.level.GetTile(xx, yy, zz) == Block.air)
                                        {
                                            pos.x = xx; pos.y = yy; pos.z = zz; if (yy != y + 1) pos.glass = 1; else pos.glass = 0;
                                            tempBuffer.Add(pos);
                                        }
                                    }
                                }
                            }

                            List<Pos> toRemove = new List<Pos>();
                            buffer.ForEach((cP) =>
                            {
                                if (!tempBuffer.Contains(cP))
                                {
                                    p.SendBlockchange(cP.x, cP.y, cP.z, Block.air);
                                    toRemove.Add(cP);
                                }
                            });

                            foreach (Pos cP in toRemove)
                            {
                                buffer.Remove(cP);
                            }

                            foreach (Pos cP in tempBuffer)
                            {
                                if (!buffer.Contains(cP))
                                {
                                    buffer.Add(cP);
                                    if (cP.glass != 1 && message == "water")
                                        p.SendBlockchange(cP.x, cP.y, cP.z, Block.waterstill);
                                    else if (cP.glass != 1) { }
                                    else p.SendBlockchange(cP.x, cP.y, cP.z, Block.glass);
                                }
                            }

                            tempBuffer.Clear();
                            toRemove.Clear();
                        }
                        catch { }
                    }
                    catch { }
                }

                buffer.ForEach((cP) =>
                {
                    p.SendBlockchange(cP.x, cP.y, cP.z, Block.air);
                });

                Player.SendMessage(p, "Stopped flying");
            }));
            flyThread.Start();
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/fly [\"water\"] - Allows you to fly");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }

        struct Pos { public ushort x, y, z, glass; }
    }
}