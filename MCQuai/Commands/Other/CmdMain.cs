
using System;

namespace MCRevive
{
    public class CmdMain : Command
    {
        public override string name { get { return "main"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdMain() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); }
            else
            {
				Command.all.Find("goto").Use(p, Server.mainLevel.name);
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/main - Returns you to the servers main level.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
