﻿using System;
using System.IO;

namespace MCRevive
{
    public class CmdTnt : Command
    {
        public override string name { get { return "tnt"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Builder; } }
        public CmdTnt() { }
        public override void Use(Player p, string message)
        {
            if (message.IndexOf(' ') != -1) { Help(p); return; }

            if (p.BlockAction == 4 || p.BlockAction == 5)
            {
                p.BlockAction = 0; Player.SendMessage(p, "TNT mode is now &cOFF" + Server.DefaultColor + ".");
            }
            else if (message.ToLower() == "small" || message == "")
            {
                p.BlockAction = 4; Player.SendMessage(p, "TNT mode is now &aON" + Server.DefaultColor + ".");
            }
            else if (message.ToLower() == "big")
            {
                if (p.group.Permission >= LevelPermission.Operator)
                {
                    p.BlockAction = 5; Player.SendMessage(p, "TNT mode is now &aON" + Server.DefaultColor + ".");
                }
                else
                {
                    Player.SendMessage(p, "This mode is reserved for OPs");
                }
            }
            else
            {
                Help(p);
            }

            p.painting = false;
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/tnt ['small'/'big'] - Creates exploding TNT.");
            Player.SendMessage(p, "Big TNT is reserved for OP+.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/tnt' or '/tnt small'",
                "This will make any block placed into exploding TNT.",
                "",
                "&3'/tnt big'",
                "This will make any block placed into powerful exploding TNT."
            };
        }
    }
}