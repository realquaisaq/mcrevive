﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class CmdClearChat : Command
    {
        public override string name { get { return "clearchat"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdClearChat() { }
        public override void Use(Player p, string message)
        {
            if (message.Split(' ').Length > 1) { Help(p); return; }
            else if (message == "")
                for (int i = 0; i < 18; ++i)
                    Player.SendMessage(p, ".");
            else if (p == null || p.group.Permission > LevelPermission.Operator || p.ignorePermission)
            {
                if (message.ToLower() == "global")
                {
                    for (int i = 0; i < 18; ++i)
                        Player.GlobalMessage(".");
                    Server.s.Log("Chat has been cleared");
                }
                else
                {
                    Player who = Player.Find(message);
                    if (who == null)
                        Player.SendMessage(p, "Player &c" + message + Server.DefaultColor + " could not be found.");
                    else
                        for (int i = 0; i < 18; ++i)
                            Player.SendMessage(who, ".");
                }
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/Clearchat - Clears your own chat.");
            Player.SendMessage(p, "/Clearchat <player> - Clears a players chat.");
            Player.SendMessage(p, "/Clearchat global - Clears everyones chat.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/Clearchat",
                "This will clear your chat",
                "",
                "&3'/Clearchat quaisaq'",
                "This will completly clear Quaisaq's chat.",
                "",
                "&3'/Clearchat global'",
                "This will clear everyone on the server's chat."
            };
        }
    }
}
