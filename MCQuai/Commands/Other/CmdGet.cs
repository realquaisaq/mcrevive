﻿using System;

namespace MCRevive
{
    public class CmdGet : Command
    {
        public override string name { get { return "get"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdGet() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            Player.SendMessage(p, "Build or break a block to display information.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Change);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/get - Tells you the name of the next block placed/clicked.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/get'",
                "After you have typed this, place or destroy a block.",
                "You will now see a message telling you what block that was.",
                "This can also be done using /about"
            };
        }
        public void Change(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            p.SendBlockchange(x, y, z, p.level.GetTile(x, y, z));
            Player.SendMessage(p, "This block is called &a" + Block.Name(action == 0 ? p.level.GetTile(x, y, z) : type) + Server.DefaultColor + ".");
            if (p.staticCommands) { p.Blockchange += new Player.BlockchangeEventHandler(Change); }
        }
    }
}