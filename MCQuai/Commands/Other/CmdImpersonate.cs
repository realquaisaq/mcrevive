using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class CmdImpersonate : Command
    {
        public override string name { get { return "impersonate"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdImpersonate() { }
        public override void Use(Player p, string message)
        {
            Player who = Player.Find(message.Split(' ')[0]);
            if (who != null)
            {
                if (Server.worldChat)
                {
                    Player.GlobalChat(who, message.Substring(message.IndexOf(' ')), true);
                }
                else
                {
                    Player.GlobalChatLevel(who, message.Substring(message.IndexOf(' ')), true);
                }
            }
            else if (Player.checkDevS(who.name))
            {
                Player.GlobalMessage(p.color + p.name + Server.DefaultColor + " attempted to impersonate a MCRevive Developer!");
                p.Kick("Do NOT attempt to impersonate a MCRevive developer!");
                return;
            }
            else { Player.SendMessage(p, "Unable to find player: " + message.Split(' ')[0]); }
				
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/Impersonate <player> <message> - forces <player> to say <message>.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
