using System;

namespace MCRevive
{
    class CmdViewRank : Command
    {
        public override string name { get { return "viewrank"; } }
        public override string shortcut { get { return "vr"; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdViewRank() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }

            Group foundGroup = Group.Find(message);
            if (foundGroup == null)
            {
                Player.SendMessage(p, "Could not find group: " + message);
                return;
            }


            string totalList = "";
            foreach (string s in foundGroup.playerList.All())
            {
                totalList += ", " + s;
            }

            if (totalList == "")
            {
                Player.SendMessage(p, "No one has the rank of " + foundGroup.color + foundGroup.name);
                return;
            }

            Player.SendMessage(p, "People with the rank of " + foundGroup.color + foundGroup.name + ":");
            Player.SendMessage(p, totalList.Remove(0, 2));
            Player.SendMessage(p, "&a" + foundGroup.playerList.All().Count + Server.DefaultColor +
                ((foundGroup.playerList.All().Count != 1) ? " people" : " person") + " has the rank " + foundGroup.color + foundGroup.name);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/Viewrank or /vr <rank> - tells the name of everyone who is <rank>");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
