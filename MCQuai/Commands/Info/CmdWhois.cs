/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;

namespace MCRevive
{
    public class CmdWhois : Command
    {
        public override string name { get { return "whois"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdWhois() { }
        public override void Use(Player p, string message)
        {
            Player who = null;
            if (message == "") { who = p; message = p.name; } else { who = Player.Find(message); }
            if (who != null && !who.hidden)
            {
                Player.SendMessage(p, who.color + who.name + Server.DefaultColor + " is on &b" + who.level.name);
                Player.SendMessage(p, who.color + who.prefix + who.name + Server.DefaultColor + " has: ");
                Player.SendMessage(p, "> > the rank of " + who.group.color + who.group.name + Server.DefaultColor + ".");
                Player.SendMessage(p, "> > modified &a" + who.totalBlocks + Server.DefaultColor + " blocks, &a" + who.seasonBlocks + Server.DefaultColor + " since logged in.");
                Player.SendMessage(p, "> > logged in &a" + who.totalLogin + Server.DefaultColor + " times.");
                Player.SendMessage(p, "> > logged in first time on &a" + who.firstLogin + Server.DefaultColor + ".");
                Player.SendMessage(p, "> > logged in last &a" + who.lastLogin + Server.DefaultColor + ".");
                Player.SendMessage(p, "> > earned &a" + who.achievementsEarned + Server.DefaultColor + " achievements.");

                bool skip = false;
                if (p != null) if (p.group.Permission <= LevelPermission.AdvBuilder) skip = true;
                if (!skip)
                {
                    string givenIP;
                    if (Server.bannedIP.Contains(who.ip)) givenIP = "&8" + who.ip + ", which is banned";
                    else givenIP = who.ip;
                    Player.SendMessage(p, "> > the IP of " + givenIP);
                }
                if (Player.checkDevS(who.name.ToLower()))
                {
                    Player.SendMessage(p, "> > Player is a &4Developer");
                }
            }
            else { Player.SendMessage(p, "\"" + message + "\" is offline! Using /whowas instead."); Command.all.Find("whowas").Use(p, message); }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/whois [player] - Displays information about someone.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}