using System;
using System.IO;

namespace MCRevive
{
    public class CmdDescription : Command
    {
        public override string name { get { return "description"; } }
        public override string shortcut { get { return "desc"; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdDescription() { }
        public override void Use(Player p, string message)
        {
            Player.SendMessage(p, "&bServer Description:");
            if (File.Exists("text/server description.txt") && !File.Exists("text/description.txt"))
                File.Move("text/server description.txt", "text/description.txt");
            if (!File.Exists("text/description.txt"))
                File.WriteAllText("text/description.txt", "A Nice Server");
            Player.SendMessage(p, File.ReadAllLines("text/description.txt")[0]);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/description - tells you the servers description.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
