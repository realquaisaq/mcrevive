/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;

namespace MCRevive
{
    public class CmdInfo : Command
    {
        public override string name { get { return "info"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdInfo() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); }
            else
            {
                Player.SendMessage(p, "&bMCRevive" + Server.DefaultColor + " Revision " + Server.Version + " written in &3C#" + Server.DefaultColor + " by Quaisaq.");
                Player.SendMessage(p, "Based on &bMCQuai" + Server.DefaultColor + " by Quaisaq, which was based on");
                Player.SendMessage(p, "&bMCSharp" + Server.DefaultColor + " by Voziv, and the rest of the &bMCSharp" + Server.DefaultColor + " team.");
                Player.SendMessage(p, "Official website: &4http://www.mcrevive.tk/");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/info - Displays the server software information.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
