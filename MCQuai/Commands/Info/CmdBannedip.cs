/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;

namespace MCRevive
{
    public class CmdBannedip : Command
    {
        public override string name { get { return "bannedip"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdBannedip() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            if (Server.bannedIP.All().Count > 0)
            {
                int o = 0;
                Server.bannedIP.All().ForEach(delegate(string name) { o++; message += ", " + name; });
                Player.SendMessage(p, Server.bannedIP.All().Count.ToString() + " IP" + ((Server.bannedIP.All().Count != 1) ? "s" : "") + "&8banned" + Server.DefaultColor + ": " + message.Remove(0, 2) + ".");
                Player.SendMessage(p, o + " players are IP-banned.");
            }
            else { Player.SendMessage(p, "No IP is banned."); }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/bannedip - Lists all banned IPs.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}