using System;

namespace MCRevive
{
    public class CmdDevs : Command
    {
        public override string name { get { return "devs"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdDevs() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            Player.SendMessage(p, "&9MCRevive Development Team: ");
            Player.SendMessage(p, "&4Quaisaq &e- Founder/Lead Developer");
            Player.SendMessage(p, "&4Jesbus &e- Scripting Developer");
            Player.SendMessage(p, "&42k10 &e- Learning Developer");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/devs - Displays the list of MCRevive developers.");
        }
        public override string[] Example(Player p)
        {
            return new string[] 
            {
                "Try using /dev"
            };
        }
    }
}