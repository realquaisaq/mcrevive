/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;

namespace MCRevive
{
    public class CmdWhowas : Command
    {
        public override string name { get { return "whowas"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdWhowas() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            string color = Group.Find("guest").color;
            string rank = Group.Find("guest").name;
            string lastLogin = "";
            string firstLogin = "";
            string title = "";
            bool modify = false;
            int totalLogin = 0;
            int achievementsEarned = 0;
            long blockModify = 0;
            long blocksGriefed = 0;
            long blocksBuild = 0;
            Player who = Player.Find(message);
            if (who != null && !who.hidden)
            {
                Player.SendMessage(p, who.color + who.name + Server.DefaultColor + " is online, using /whois instead.");
                Command.all.Find("whois").Use(p, message);
                return;
            }
            if (File.Exists("memory/player/save/" + message + ".txt"))
            {
                try
                {
                    modify = true;
                    string[] lines = File.ReadAllLines("memory/player/save/" + p.realname + ".txt");
                    foreach (string line in lines)
                    {
                        if (line != "")
                        {
                            string key = line.Split('=')[0].Trim();
                            string value = line.Split('=')[1].Trim();
                            switch (key.ToLower())
                            {
                                case "totallogins":
                                    totalLogin = int.Parse(value);
                                    break;
                                case "achievements":
                                    achievementsEarned = int.Parse(value);
                                    break;
                                case "firstlogin":
                                    firstLogin = value;
                                    break;
                                case "lastlogin":
                                    lastLogin = value;
                                    break;
                                case "blocksbuild":
                                    blocksBuild = long.Parse(value);
                                    break;
                                case "blocksgriefed":
                                    blocksGriefed = long.Parse(value);
                                    break;
                                case "title":
                                    title = value;
                                    break;
                            }
                            blockModify = blocksBuild + blocksGriefed;
                        }
                    }
                }
                catch { modify = false; }
            }
            Group group = Group.findPlayerGroup(message);
            rank = group.name;
            color = group.color;

            Player.SendMessage(p, color + message + Server.DefaultColor + " has: ");
            Player.SendMessage(p, "> > the rank of " + color + rank + ".");
            if (modify)
            {
                Player.SendMessage(p, "> > Modified &a" + blockModify + Server.DefaultColor + " blocks.");
                Player.SendMessage(p, "> > logged in &a" + totalLogin + Server.DefaultColor + " times.");
                Player.SendMessage(p, "> > logged in first time on &a" + firstLogin + Server.DefaultColor + ".");
                Player.SendMessage(p, "> > logged in last &a" + lastLogin + Server.DefaultColor + ".");
                Player.SendMessage(p, "> > earned &a" + achievementsEarned + Server.DefaultColor + " achievements.");
            }
            if (Player.checkDevS(message.ToLower()))
            {
                Player.SendMessage(p, "> > Player is a &4Developer");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/whowas <name> - Displays information about someone who left.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}