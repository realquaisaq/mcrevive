using System;
using System.IO;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdUnloaded : Command
    {
        public override string name { get { return "unloaded"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdUnloaded() { }
        public override void Use(Player p, string message)
        {
            try
            {
                List<string> levels = new List<string>(Server.levels.Count);

                string unloadedLevels = ""; int currentNum = 0; int maxMaps = 0;

                if (message != "")
                {
                    try { maxMaps = int.Parse(message) * 50; currentNum = maxMaps - 50; }
                    catch { Help(p); return; }
                }

                DirectoryInfo di = new DirectoryInfo("levels/");
                List<FileInfo> fi = new List<FileInfo>();
                fi.AddRange(di.GetFiles("*.lvl"));
                fi.AddRange(di.GetFiles("*.mcqlvl"));

                foreach (Level l in Server.levels) { levels.Add(l.name.ToLower()); }

                if (maxMaps == 0)
                {
                    foreach (FileInfo file in fi)
                    {
                        if (!levels.Contains(file.Name.Replace(".lvl", "").Replace(".mcqlvl", "").ToLower()))
                        {
                            unloadedLevels += ", " + file.Name.Replace(".lvl", "").Replace(".mcqlvl", "");
                        }
                    }
                    if (unloadedLevels != "")
                    {
                        Player.SendMessage(p, "Unloaded levels: ");
                        Player.SendMessage(p, "&4" + unloadedLevels.Remove(0, 2));
                        if (fi.Count > 50) { Player.SendMessage(p, "For a more structured list, use /unloaded <1/2/3/..>"); }
                    }
                    else Player.SendMessage(p, "No maps are unloaded");
                }
                else
                {
                    if (maxMaps > fi.Count) maxMaps = fi.Count;
                    if (currentNum > fi.Count) { Player.SendMessage(p, "No maps beyond number " + fi.Count); return; }

                    Player.SendMessage(p, "Unloaded levels (" + currentNum + " to " + maxMaps + "):");
                    for (int i = currentNum; i < maxMaps; i++)
                    {
                        if (!levels.Contains(fi[i].Name.Replace(".lvl", "").Replace(".mcqlvl", "").ToLower()))
                        {
                            unloadedLevels += ", " + fi[i].Name.Replace(".lvl", "").Replace(".mcqlvl", "");
                        }
                    }

                    if (unloadedLevels != "")
                    {
                        Player.SendMessage(p, "&4" + unloadedLevels.Remove(0, 2));
                    }
                    else Player.SendMessage(p, "No maps are unloaded");
                }
            }
            catch (Exception e) { Server.ErrorLog(e); Player.SendMessage(p, "An error occured"); }
            //Exception catching since it needs to be tested on Ocean Flatgrass
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/unloaded - Lists all unloaded levels.");
            Player.SendMessage(p, "/unloaded <1/2/3/..> - Shows a compact list.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}