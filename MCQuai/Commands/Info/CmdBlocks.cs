using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class CmdBlocks : Command
    {
        public override string name { get { return "blocks"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public override void Use(Player p, string message)
        {
            if (message == "") message = "1";
            bool player = false;
            try { int.Parse(message); }
            catch { player = true; }

            if (!player)
            {
                int page = int.Parse(message);
                List<string> blks = new List<string>();
                int blocks = 0;
                for (ushort i = 0; i < Block.Max; i++)
                    if (Block.Name(i).ToLower() != "unknown") { blks.Add(Block.Name(i)); blocks++; }
                if (page <= 0 || page > (int)(Math.Ceiling((double)(blks.Count / 25)))) { Player.SendMessage(p, "That page doesn't exist."); goto pages; }
                for (int msg = page * 25 - 25; msg < page * 25; ++msg)
                    try { message += Server.DefaultColor + ", " + Group.GroupList.Find(r => r.Permission == Block.BlockList.Find(b => b.type == Block.Number(blks[msg])).rank).color + blks[msg]; }
                    catch { }
                Player.SendMessage(p, message.Remove(0, 4));
            
            pages:
                string lastMsg = "";
                for (int pg = 1; pg <= (int)Math.Ceiling((double)(blks.Count / 25)); ++pg)
                    lastMsg += ", " + (pg == page ? "&a" : "") + pg + Server.DefaultColor;
                Player.SendMessage(p, "pages: " + lastMsg.Substring(2));
            }
            else
            {
                Player who = Player.Find(message.ToLower());
                if (who != null)
                {
                    Player.SendMessage(p, who.color + who.name + Server.DefaultColor + " has: ");
                    Player.SendMessage(p, "> > Built &a" + who.blocksBuild + Server.DefaultColor + " blocks.");
                    Player.SendMessage(p, "> > Destroyed &4" + who.blocksGriefed + Server.DefaultColor + " blocks.");
                    Player.SendMessage(p, "> > Modified &b" + (who.blocksBuild + who.blocksGriefed) + Server.DefaultColor + " blocks.");
                    if (Player.checkDevS(who.name)) Player.SendMessage(p, "> > Player is a %4Developer");
                }
                else Player.SendMessage(p, "Unable to find player: " + message);
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/blocks [player] - Shows all blocks available.");
            Player.SendMessage(p, "If [player] is added - shows block-info about player.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
