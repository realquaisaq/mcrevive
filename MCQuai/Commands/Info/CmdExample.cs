﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdExample : Command
    {
        public override string name { get { return "example"; } }
        public override string shortcut { get { return "ex"; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdExample() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            if (!message.Contains(" ")) message += " 1";
            int page = 1;
            try { page = int.Parse(message.Split(' ')[1]); }
            catch { }

            Command cmd = Command.all.Find(message.Split(' ')[0]);
            if (cmd == null)
                Player.SendMessage(p, "Could not find command '" + message.Split(' ')[0] + "'.");
            else
            {
                string[] messages = cmd.Example(p);
                if (messages[0] == "") { Player.SendMessage(p, "This command does not have an example yet."); return; }
                List<string[]> msgs = new List<string[]>();
                List<string> temp = new List<string>();

                foreach (string s in messages)
                {
                    if (s == "")
                    {
                        msgs.Add(temp.ToArray());
                        temp.Clear();
                        continue;
                    }
                    temp.Add(s);
                }
                msgs.Add(temp.ToArray());

                if (msgs.Count < page || page < 1)
                {
                    Player.SendMessage(p, "There is no page: " + page);
                    Player.SendMessage(p, "Theres " + msgs.Count + " pages.");
                }
                foreach (string s in msgs[page - 1])
                    Player.SendMessage(p, s);
                Player.SendMessage(p, "page: &a" + page + Server.DefaultColor + " of &b" + msgs.Count + Server.DefaultColor + ".");
            }
        }

        public override void Help(Player p)
        {
            Player.SendMessage(p, "/example <command> - Shows an example of <command>.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "Are you serious?"
            };
        }
        private string getColor(string name)
        {
            Command cmd = Command.all.Find(name);
            foreach (Group grp in Group.GroupList)
            {
                if (GrpCommands.allowedCommands.Find(aV => aV.commandName == cmd.name).rank == grp.Permission) return grp.color;
            }
            return "&f";
        }
    }
}