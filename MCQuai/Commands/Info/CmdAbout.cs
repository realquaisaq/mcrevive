/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;
using System.Data;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdAbout : Command
    {
        public override string name { get { return "about"; } }
        public override string shortcut { get { return "b"; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdAbout() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            Player.SendMessage(p, "Break/build a block to display information.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange);
        }

        public override void Help(Player p)
        {
            Player.SendMessage(p, "/about - Displays information about a block.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/about'" + Server.DefaultColor + " or&3 '/b'",
                "Click a block.",
                "You will now see who &acreated" + Server.DefaultColor + "/&4broke" + Server.DefaultColor + " that block.",
                "You will also see when the block was &aedited" + Server.DefaultColor + ".",
                "And the &aname" + Server.DefaultColor + " of the block."
            };
        }

        public void Blockchange(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            if (!p.staticCommands) p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            if (b == Block.Zero) { Player.SendMessage(p, "Invalid Block(" + x + "," + y + "," + z + ")!"); return; }
            p.SendBlockchange(x, y, z, b);

            string message = "Block (" + x + "," + y + "," + z + "): ";
            message += "&f" + b + " = " + Block.Name(b);
            Player.SendMessage(p, message + Server.DefaultColor + ".");
            message = p.level.foundInfo(x, y, z);
            if (message != "") Player.SendMessage(p, "Physics information: &a" + message);
            
            List<Level.BlockPos> l1 = Memory.LoadBlocks(p.level.name);
            l1.AddRange(p.level.blockCache);

            int pos = p.level.PosToInt(x, y, z);
            List<Level.BlockPos> l2 = new List<Level.BlockPos>();
            for (int i = 0; i < l1.Count; i++)
            {
                if (l1[i].pos == pos)
                    l2.Add(l1[i]);
            }

            if (l2.Count == 0)
                Player.SendMessage(p, "This block has not been modified since the map was cleared.");
            else
            {
                int amount = l2.Count;
                List<long> time = new List<long>(amount);
                List<Level.BlockPos> bp2 = new List<Level.BlockPos>();

                l2.ForEach((bP) =>
                {
                    if (!time.Contains(bP.TimePerformed.Ticks))
                        time.Add(bP.TimePerformed.Ticks);
                });
                time.Sort();

                for (int i = 0; i < time.Count; ++i)
                    bp2.Add(l2.Find(bP => bP.TimePerformed.Ticks == time[i]));

                bp2.ForEach((bp) =>
                {
                    Player.SendMessage(p, (!bp.deleted ? "&3Created by" : "&4Destroyed by") + Group.findPlayerGroup(bp.name).color + " " + bp.name + Server.DefaultColor + ".");
                    Player.SendMessage(p, "Date and time modified: &2" + bp.TimePerformed);
                });
                bp2.Clear();
            }
            Memory.Done();
        }
    }
}