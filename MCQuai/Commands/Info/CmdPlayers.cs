/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace MCRevive
{
    class CmdPlayers : Command
    {

        public override string name { get { return "players"; } }
        public override string shortcut { get { return "who"; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdPlayers() { }
        struct groupPlayers { public Group grp; public List<string> players; }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            List<groupPlayers> PlayerList = new List<groupPlayers>();
            int totalPlayers = 0;

            foreach (Group grp in Group.GroupList)
            {
                if (grp.name != "nobody")
                {
                    groupPlayers grpP;
                    grpP.grp = grp;
                    grpP.players = new List<string>();
                    PlayerList.Add(grpP);
                }
            }
            foreach (Player all in Player.players)
            {
                if (!all.hidden)
                {
                    totalPlayers++;
                    string foundName = all.name;

                    if (Server.afkset.Contains(all.name))
                    {
                        foundName += "-afk";
                    }

                    PlayerList.Find(g => g.grp == all.group).players.Add(foundName + " (" + all.level.name + ")");
                }
            }
            for (int i = PlayerList.Count - 1; i >= 0; i--)
            {
                groupPlayers grp = PlayerList.ToArray()[i];
                if (grp.players.Count == 0)
                    continue;
                string output = "";

                foreach (string player in grp.players)
                {
                    output += ", " + player;
                }

                if (output != "")
                    output = output.Remove(0, 2);
                output = grp.grp.color + getPlural(grp.grp.trueName) + ": " + output;
                Player.SendMessage(p, output);
            }
            Player.SendMessage(p, "There are " + totalPlayers + " players online.");
        }

        public string getPlural(string group)
        {
            try
            {
                string last2 = group.Substring(group.Length - 2).ToLower();
                if ((last2 != "ed" || group.Length <= 3) && last2[1] != 's')
                {
                    return group + "s";
                }
                return group;
            }
            catch
            {
                return group;
            }
        }

        public override void Help(Player p)
        {
            Player.SendMessage(p, "/players - Shows name and general rank of all players");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
