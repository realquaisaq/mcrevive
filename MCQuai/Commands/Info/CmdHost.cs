using System;
using System.IO;

namespace MCRevive
{
    public class CmdHost : Command
    {
        public override string name { get { return "host"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdHost() { }
        public override void Use(Player p, string message)
        {
            string ownersColor = Group.findPlayerGroup(Server.TheOwner).color;
            try
            {
                foreach (string line in File.ReadAllLines("memory/player/save/" + Server.TheOwner + ".txt"))
                {
                    if (line.Split('=')[0].Trim() == "color")
                        if (line.Split('=')[1].Trim()[0] == '&')
                        {
                            ownersColor = line.Split('=')[1].Trim().Substring(0, 1);
                            break;
                        }
                }
            }
            catch { }
            TimeSpan session = DateTime.Now - Server.session;
            Player.SendMessage(p, "Server state: &a" + Server.consolename);
            Player.SendMessage(p, "First time server online: " + File.ReadAllLines("memory/server/firstboot.txt")[0] + ".");
            Player.SendMessage(p, "Online this session: &4" + session.Days + Server.DefaultColor + " days, &3" +
                session.Hours + " " + Server.DefaultColor + "hours, &3" + 
                session.Minutes + " " + Server.DefaultColor + "minutes, and &3" +
                session.Seconds + " " + Server.DefaultColor + "seconds.");
            Player.SendMessage(p, "Server owner: " + ownersColor + Server.TheOwner);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/host - Tells you server state, the first online date of the server, ");
            Player.SendMessage(p, "online time this session, and the server owner.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
