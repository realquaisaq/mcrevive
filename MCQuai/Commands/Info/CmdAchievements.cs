using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdAchievements : Command
    {
        public override string name { get { return "achievements"; } }
        public override string shortcut { get { return "ach"; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdAchievements() { }
        public override void Use(Player p, string message)
        {
            p.CheckAchievement();
            if (message == "")
            {
                int allowedAchs = 0;
                Server.allAchievements.ForEach((ds) =>
                {
                    if (ds.allowed) allowedAchs++;
                });
                if (allowedAchs > 0 && Server.allowAchievements)
                {
                    Player.SendMessage(p, "List of achievements:");
                    string achievements = "";
                    p.Achievements.ForEach((ach) =>
                    {
                        string name = ach.name;
                        if (name == "Your Face") name = "Your Face, God Damit";
                        achievements += (Server.allAchievements.Find(a => a.realname == name).allowed) ? ", " + ((ach.earned) ? "&a" : "&4") + name + Server.DefaultColor : "";
                    });
                    Player.SendMessage(p, achievements.Remove(0, 2));
                }
                else
                {
                    Player.SendMessage(p, "No achievements allowed on this server");
                }
            }
            else
            {
                if (Server.allowAchievements)
                {
                    List<string> achievements = new List<string>();
                    Server.allAchievements.ForEach((ds) => { achievements.Add(ds.realname); });
                    achievements.Add("Im Staying"); achievements.Add("I Am Staying"); achievements.Add("Your Face God Damit");
                    int founds = 0;
                    string foundOne = "";
                    achievements.ForEach((ach) =>
                    {
                        if (ach.ToLower().Contains(message.ToLower()))
                        {
                            if (ach != "I Am Staying" && ach != "Im Staying" && ach != "Your Face God Damit") founds++;
                            foundOne = (ach == "Im Staying" || ach == "I Am Staying") ? "I'm Staying" :
                                (ach == "Your Face God Damit" || ach == "Your Face" || ach == "Your Face God Damit") ? "Your Face, God Damit" : ach;
                        }
                    });
                    
                    if (founds == 1)
                    {
                        Player.SendMessage(p, "&3" + foundOne + Server.DefaultColor + ": ");
                        bool earned = p.Achievements.Find(a => a.name == foundOne).earned;
                        if (!Server.allAchievements.Find(a => a.realname == foundOne).allowed)
                        {
                            Player.SendMessage(p, "This achievement has been removed from this server");
                            return;
                        }
                        switch (foundOne.ToLower())
                        {
                            case "minecraft begins":
                                Player.SendMessage(p, "&aPlace your first block" + Server.DefaultColor + ".");
                                Player.SendMessage(p, ((earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to build &41" + Server.DefaultColor + " block more, to earn this achievement."));
                                break;
                            case "block maker":
                                Player.SendMessage(p, "&aPlace 10000" + Server.DefaultColor + " blocks.");
                                Player.SendMessage(p, ((earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to build &4" + (10000 - p.blocksBuild) + Server.DefaultColor + " block" +
                                    ((10000 - p.blocksBuild != 1) ? "s" : "") + " more, to earn this achievement."));
                                break;
                            case "house creator":
                                Player.SendMessage(p, "&aPlace 10000000" + Server.DefaultColor + " blocks.");
                                Player.SendMessage(p, ((earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to build &4" + (10000000 - p.blocksBuild) + Server.DefaultColor + " block" +
                                    ((10000000 - p.blocksBuild != 1) ? "s" : "") + " more, to earn this achievement."));
                                break;
                            case "professional builder":
                                Player.SendMessage(p, "&aPlace 1000000001" + Server.DefaultColor + " blocks, because &a100000000" + Server.DefaultColor + " is too little");
                                Player.SendMessage(p, ((earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to build &4" + (1000000001 - p.blocksBuild) + Server.DefaultColor + " block" +
                                    ((1000000001 - p.blocksBuild != 1) ? "s" : "") + " more, to earn this achievement."));
                                break;
                            case "godly builder":
                                Player.SendMessage(p, "&aModify 100000000000" + Server.DefaultColor + " blocks.");
                                Player.SendMessage(p, ((earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to build &4" + (100000000000 - p.totalBlocks) + Server.DefaultColor + " block" +
                                    ((100000000000 - p.totalBlocks != 1) ? "s" : "") + " more, to earn this achievement."));
                                break;
                            case "griefer":
                                Player.SendMessage(p, "&aDestroy 10000" + Server.DefaultColor + " blocks.");
                                Player.SendMessage(p, ((earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to destroy &4" + (10000 - p.blocksGriefed) + Server.DefaultColor + " block" +
                                    ((10000 - p.blocksGriefed != 1) ? "s" : "") + " more, to earn this achievement."));
                                break;
                            case "promoted":
                                Player.SendMessage(p, "&aGet promoted" + Server.DefaultColor + " for the first time");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You still haven't earned this achievement.");
                                break;
                            case "demoted":
                                Player.SendMessage(p, "Lose a rank.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You still haven't earned this achievement.");
                                break;
                            case "ranked operator":
                                Player.SendMessage(p, "Get &aranked Operator" + Server.DefaultColor + ".");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You still haven't earned this achievement.");
                                break;
                            case "ranked admin":
                                Player.SendMessage(p, "Get &aranked Admin" + Server.DefaultColor + ".");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You still haven't earned this achievement.");
                                break;
                            case "ranked owner":
                                Player.SendMessage(p, "Become the &aowners best friend" + Server.DefaultColor + ".");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You are still not The Owner's best friend.");
                                break;
                            case "i'm staying":
                                Player.SendMessage(p, "Log in &a50 times" + Server.DefaultColor + " or more, to this server.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to log in &4" + (50 - p.totalLogin) + Server.DefaultColor + " time" +
                                    ((50 - p.totalLogin != 1) ? "s" : "") + " more, to earn this achievement.");
                                break;
                            case "zombie death":
                                Player.SendMessage(p, "Die to a zombie &a50 times" + Server.DefaultColor + ".");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to die to a zombie &4" + (50 - p.zombieDeaths) + Server.DefaultColor + " time" +
                                    ((50 - p.zombieDeaths != 1) ? "s" : "") + " more, to earn this achievement.");
                                break;
                            case "survivor":
                                Player.SendMessage(p, "Survive &a3" + Server.DefaultColor + " lava survival games.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to survive &4" + (3 - p.completedGames) + Server.DefaultColor + " more lava survival game" +
                                    ((3 - p.completedGames != 1) ? "s" : "") + ", to earn this achievement.");
                                break;
                            case "eco friend":
                                Player.SendMessage(p, "Create &a10000" + Server.DefaultColor + " blocks of grass and/or dirt.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to create &4" + (10000 - p.builtEco) + Server.DefaultColor + " more grass and/or dirt block" +
                                    ((10000 - p.builtEco != 1) ? "s" : "") + ", to earn this achievement.");
                                break;
                            case "loud mouth":
                                Player.SendMessage(p, "Type &a5000" + Server.DefaultColor + " letters in upper-case.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to type &4" + (5000 - p.capsRage) + Server.DefaultColor + " more upper-case letter" +
                                    ((5000 - p.capsRage != 1) ? "s" : "") + ", to earn this achievement.");
                                break;
                            case "spawner":
                                Player.SendMessage(p, "Spawn &a200" + Server.DefaultColor + " bots.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to spawn &4" + (200 - p.spawnedBots) + Server.DefaultColor + " more bot" +
                                    ((200 - p.spawnedBots != 1) ? "s" : "") + ", to earn this achievement.");
                                break;
                            case "server fan":
                                Player.SendMessage(p, "Play on the server for &a24" + Server.DefaultColor + " hours.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to play for &4" + (24 - p.TimeOnServer[0]) + " more hour" +
                                    ((24 - p.TimeOnServer[0] != 1) ? "s" : "") + ", to earn this achievement.");
                                break;
                            case "addicted":
                                Player.SendMessage(p, "Play on the server for &a100" + Server.DefaultColor + " hours.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to play for &4" + (100 - p.TimeOnServer[0]) + " more hour" +
                                    ((100 - p.TimeOnServer[0] != 1) ? "s" : "") + ", to earn this achievement.");
                                break;
                            case "flyer":
                                Player.SendMessage(p, "Use the command Fly &a15" + Server.DefaultColor + " times.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to use the fly command &4" + (15 - p.flys) + Server.DefaultColor + "more time" +
                                    ((15 - p.flys != 1) ? "s" : "") + ", to earn this achievement");
                                break;
                            case "bully":
                                Player.SendMessage(p, "Slap someone &a50" + Server.DefaultColor + " times.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to slap &4" + (50 - p.slapper) + Server.DefaultColor + " more " +
                                    ((50 - p.slapper != 1) ? "people" : "person") + ", to earn this achievement.");
                                break;
                            case "ninja":
                                Player.SendMessage(p, "Slap someone &a500" + Server.DefaultColor + " times.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to slap someone &4" + (500 - p.slapper) + Server.DefaultColor + " more " +
                                    ((500 - p.slapper != 1) ? "people" : "person") + ", to earn this achievement.");
                                break;
                            case "your face god damit":
                                Player.SendMessage(p, "Get slapped by someone &a50" + Server.DefaultColor + " times.");
                                Player.SendMessage(p, (earned) ? "You have &aearned" + Server.DefaultColor + " this achievement!" :
                                    "You need to get slapped by someone &4" + (50 - p.slapped) + Server.DefaultColor + " more time" +
                                    ((50 - p.slapped != 1) ? "s" : "") + ", to earn this achievement.");
                                break;
                        }
                    }
                    else
                    {
                        founds = 0;
                        Player.players.ForEach((all) =>
                        {
                            if (all.name.Contains(message))
                                founds++;
                        });
                        if (founds == 1)
                        {
                            int allowedAchs = 0;
                            Server.allAchievements.ForEach((ds) =>
                            {
                                if (ds.allowed) allowedAchs++;
                            });
                            if (allowedAchs > 0)
                            {
                                Player who = Player.Find(message);
                                Player.SendMessage(p, who.color + who.name + Server.DefaultColor + "'s achievements:");
                                string newmsg = "";
                                who.Achievements.ForEach((ach) =>
                                {
                                    newmsg += (Server.allAchievements.Find(a => a.realname == ach.name).allowed) ? ", " + ((ach.earned) ? "&a" : "&4") + ach.name + Server.DefaultColor : "";
                                });
                                Player.SendMessage(p, newmsg.Remove(0, 2));
                            }
                            else
                                Player.SendMessage(p, "No achievements allowed on this server");
                        }
                        else
                            Player.SendMessage(p, "Unknown achievement: " + message);
                    }
                }
                else
                    Player.SendMessage(p, "No achievements allowed on this server");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/Achievements - Shows you all the achievements.");
            Player.SendMessage(p, "/Achievements [achievement] - Shows info about [achievement].");
            Player.SendMessage(p, "/Achievements [player] - Shows [player]'s achievements.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
