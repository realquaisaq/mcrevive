﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    public class CmdMeasure : Command
    {
        public override string name { get { return "measure"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdMeasure() { }
        public override void Use(Player p, string message)
        {
            CatchPos cpos = new CatchPos();
            if (message.IndexOf(' ') == -1) message += " ";
                foreach (string blk in message.ToLower().Split(' '))
                    if (blk != "" && !cpos.toIgnore.Contains(Block.Number(blk)))
                        cpos.toIgnore.Add(Block.Number(blk));

            cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;

            Player.SendMessage(p, "Place two blocks to determine the edges.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/measure - Measures all the blocks between two points.");
            Player.SendMessage(p, "/measure [ignore(s)] - Enter blocks, to ignore them.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/measure'",
                "Select 2 points.",
                "You are now told how many blocks there is between",
                "the selected points.",
                "",
                "&3'/measure air stone'",
                "Select 2 points.",
                "You are now told how many blocks there is between",
                "the selected points.",
                "Except for air blocks and stone blocks."
            };
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos bp = (CatchPos)p.blockchangeObject;
            bp.x = x; bp.y = y; bp.z = z; p.blockchangeObject = bp;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos cpos = (CatchPos)p.blockchangeObject;

            ushort xx, yy, zz; int foundBlocks = 0;

            for (xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                    for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                    {
                        bool lol = false;
                        foreach (short ign in cpos.toIgnore)
                            if (p.level.GetTile(xx, yy, zz) == ign)
                                lol = true;

                        if (!lol) foundBlocks++;
                    }

            Player.SendMessage(p, "&a" + foundBlocks + Server.DefaultColor + " blocks are between (" + cpos.x + ", " + cpos.y + ", " + cpos.z + ") and (" + x + ", " + y + ", " + z + ")");
            if (p.staticCommands) p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        class CatchPos
        {
            public ushort x, y, z; public List<ushort> toIgnore = new List<ushort>();
        }
    }
}