/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;

namespace MCRevive
{
    public class CmdHelp : Command
    {
        public override string name { get { return "help"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "info"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdHelp() { }
        public override void Use(Player p, string message)
        {
            message = message.ToLower();
            try
            {
                if (message == "") {
                    if (Server.oldHelp)
                    {
                        string commandsFound = "";
                        Command.all.commands.ForEach((comm) =>
                        {
                            if (p.group.commands.All().Contains(comm) || p == null)
                                try { commandsFound += ", " + comm.name; }
                                catch { }
                        });
                        Player.SendMessage(p, "Available commands:");
                        Player.SendMessage(p, commandsFound.Remove(0, 2));
                        Player.SendMessage(p, "Type \"/help <command>\" for more help.");
                        Player.SendMessage(p, "Type \"/help shortcuts\" for shortcuts.");
                    }
                    else
                    {
                        Player.SendMessage(p, "Use &b/help ranks" + Server.DefaultColor + " for a list of ranks.");
                        Player.SendMessage(p, "Use &b/help build" + Server.DefaultColor + " for a list of building commands.");
                        Player.SendMessage(p, "Use &b/help mod" + Server.DefaultColor + " for a list of moderation commands.");
                        Player.SendMessage(p, "Use &b/help information" + Server.DefaultColor + " for a list of information commands.");
                        Player.SendMessage(p, "Use &b/help other" + Server.DefaultColor + " for a list of other commands.");
                        Player.SendMessage(p, "Use &b/help short" + Server.DefaultColor + " for a list of shortcuts.");
                        Player.SendMessage(p, "Use &b/help old" + Server.DefaultColor + " to view the Old help menu.");
                        Player.SendMessage(p, "Use &b/help [command] " + Server.DefaultColor + "to view more info.");
                    }
                    return;
                }

                else if (message == "moderation") message = "mod";
                else if (message == "information") message = "info";
                else if (message == "shortcut" || message == "shortcuts") message = "short";

                string bMessage = "";

                if (message == "ranks")
                    Group.GroupList.ForEach((grp) =>
                    {
                        if (grp.name != "nobody")
                            Player.SendMessage(p, grp.color + grp.name + " - &bCommand limit: " + grp.maxBlocks + " - &cPermission: " + (int)grp.Permission);
                    });
                else if (message == "build" || message == "game" || message == "mod" || message == "info" || message == "other") {
                    if (p != null)
                        p.group.commands.All().ForEach((comm) => {
                            if (comm.type.Contains(message)) bMessage += ", " + getColor(comm) + comm.name;
                        });
                    else
                        Command.all.commands.ForEach((comm) => {
                            if (comm.type.Contains(message)) bMessage += ", " + getColor(comm) + comm.name;
                        });

                    if (bMessage == "") { Player.SendMessage(p, "No commands of this type are available to you."); return; }
                    Player.SendMessage(p, (message == "build" ? "Building" : message == "game" ? "Game" : message == "mod" ? "Moderation" : message == "info" ? "Information" : "Other") + " commands you may use:");
                    Player.SendMessage(p, bMessage.Remove(0, 2) + ".");
                }
                else if (message == "short")
                {
                    if (p != null)
                        p.group.commands.All().ForEach((comm) => {
                            if (comm.shortcut != "") bMessage += ", &b" + comm.shortcut + " " + Server.DefaultColor + "[" + comm.name + "]";
                        });
                    else
                        Command.all.commands.ForEach((comm) => {
                            if (comm.shortcut != "") bMessage += ", &b" + comm.shortcut + " " + Server.DefaultColor + "[" + comm.name + "]";
                        });

                    Player.SendMessage(p, "Available shortcuts:");
                    Player.SendMessage(p, bMessage.Remove(0, 2) + ".");
                }
                else {
                    Command cmd = Command.all.Find(message);
                    if (cmd != null)
                    {
                        cmd.Help(p);
                        string foundRank = LevelPermission.ToName(GrpCommands.allowedCommands.Find(grpComm => grpComm.commandName == cmd.name).rank);
                        if (cmd.shortcut != "") Player.SendMessage(p, "Shortcut: &a" + cmd.shortcut + Server.DefaultColor + ".");
                        Player.SendMessage(p, "Rank needed: " + getColor(cmd) + foundRank + Server.DefaultColor + ".");
                        if (cmd.Example(p)[0] != "")
                            Player.SendMessage(p, "For more information do: &a/example " + cmd.name + Server.DefaultColor + ".");
                        return;
                    }
                    Player.SendMessage(p, "Could not find command specified.");
                }
            }
            catch (Exception e) { Server.ErrorLog(e); Player.SendMessage(p, "An error occured"); }
        }

        private string getColor(Command cmd)
        {
            Group g = Group.FindPerm(GrpCommands.allowedCommands.Find(aV => aV.commandName == cmd.name).rank);
            return g == null ? "&f" : g.color;
        }

        public override void Help(Player p)
        {
            Player.SendMessage(p, "/help [command] - Shows a list of commands or more detail for a specific command.");
            Player.SendMessage(p, "[] = not needed input. '' = exact input. <> = needed input.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "Try typing /help"
            };
        }
    }
}