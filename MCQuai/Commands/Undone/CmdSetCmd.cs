﻿using System;

namespace MCRevive
{
    class CmdSetCmd : Command
    {
        public override string name { get { return "setcmd"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdSetCmd() { }
        public override void Use(Player p, string message)
        {
            
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/setcmd <command> <rank> - Sets the permission of <command> to <rank>.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
