﻿// Spleef classes by Jesbus =]
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
namespace MCRevive
{
    public class Spleef
    {
        public static List<Spleef> Spleefs = new List<Spleef>();
        public Level Map;
        public ushort SmallX, SmallY, SmallZ, BigX, BigY, BigZ;
        public ushort[][][] RestoreInfo;
        public bool Started = false;
        public Thread Thread;
        public List<SpleefStartingPoint> StartingPoints = new List<SpleefStartingPoint>();
        public Spleef(Level l, ushort x1, ushort y1, ushort z1, ushort x2, ushort y2, ushort z2)
        {
            BigX = Math.Max(x1, x2);
            BigY = Math.Max(y1, y2);
            BigZ = Math.Max(z1, z2);
            SmallX = Math.Min(x1, x2);
            RestoreInfo = new ushort[BigX - SmallX + 1][][];
            while (SmallX <= BigX)
            {
                SmallY = Math.Min(y1, y2);
                RestoreInfo[SmallX] = new ushort[BigY - SmallY + 1][];
                while (SmallY <= BigY)
                {
                    SmallZ = Math.Min(z1, z2);
                    RestoreInfo[SmallX][SmallY] = new ushort[BigZ - SmallZ + 1];
                    while (SmallZ <= BigZ)
                    {
                        RestoreInfo[SmallX][SmallY][SmallZ] = l.GetTile(SmallX, SmallY, SmallZ);
                        SmallZ++;
                    }
                    SmallY++;
                }
                SmallX++;
            }
            Map = l;
            SmallX = Math.Min(x1, x2);
            SmallY = Math.Min(y1, y2);
            SmallZ = Math.Min(z1, z2);
            Spleefs.Add(this);
        }
        public void Start(int timeout = 10)
        {
            Started = true;
            Thread = new Thread(new ThreadStart(delegate
            {
                Restore();
                Player.GlobalMessageLevel(Map, "Spleef game will start soon. Type &a/spleef join&e to join.");
                Thread.Sleep(timeout*1000);
                Player.GlobalMessageLevel(Map, "10 seconds until spleef game start!");
                Thread.Sleep(5000);
                Player.GlobalMessageLevel(Map, "5");
                Thread.Sleep(1000);
                Player.GlobalMessageLevel(Map, "4");
                Thread.Sleep(1000);
                Player.GlobalMessageLevel(Map, "3");
                Thread.Sleep(1000);
                Player.GlobalMessageLevel(Map, "2");
                Thread.Sleep(1000);
                Player.GlobalMessageLevel(Map, "1");
                Thread.Sleep(900);
                int players = 0;
                foreach (SpleefStartingPoint ssp in StartingPoints)
                {
                    if (ssp.Occupied)
                    {
                        ssp.p.frozen = false;
                        players++;
                    }
                }
                if (players < 2)
                {
                    Player.GlobalMessageLevel(Map, "Spleef was aborted: Not enough players :(");
                    Started = false;
                    Thread.Abort();
                }
                Player.GlobalMessageLevel(Map, "Go!");
            }));
            Thread.Start();
        }
        public void Unload()
        {
            Restore();
        }
        public static void Delete(Level l, ushort x, ushort y, ushort z)
        {
            List<Spleef> TempSpleefs = Spleefs;
            foreach (Spleef spl in TempSpleefs)
            {
                if (x >= spl.SmallX && x <= spl.BigX)
                {
                    if (y >= spl.SmallY && y <= spl.BigY)
                    {
                        if (z >= spl.SmallZ && z <= spl.BigZ)
                        {
                            Spleefs.Remove(spl);
                            return;
                        }
                    }
                }
            }
        }
        public void Restore()
        {
            ushort x, y, z;
            x = SmallX;
            foreach (ushort[][] sss in RestoreInfo)
            {
                y = SmallY;
                foreach (ushort[] ss in sss)
                {
                    z = SmallZ;
                    foreach (ushort s in ss)
                    {
                        Map.SetTile(x, y, z, s);
                        z++;
                    }
                    y++;
                }
                x++;
            }
        }
    }
    public class SpleefStartingPoint
    {
        public ushort x, y, z;
        public byte rotX;
        public bool Occupied = false;
        public Player p;
        public SpleefStartingPoint(ushort X, ushort Y, ushort Z, byte RotX)
        {
            x = X;
            y = Y;
            z = Z;
            rotX = RotX;
        }
    }
}
