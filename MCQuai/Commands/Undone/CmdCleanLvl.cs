﻿using System;
using System.IO;

namespace MCRevive
{
    class CmdClearLvl : Command
    {
        public override string name { get { return "clearlvl"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdClearLvl() { }
        public override void Use(Player p, string message)
        {
            if (message.Split(' ').Length > 1) { Help(p); return; }
            Level lvl = Level.Find(message.Split(' ')[0]);
            if (lvl == null)
                if (message == "") lvl = p.level;
                else { Player.SendMessage(p, "Could not find map: " + message); return; }

            lvl.physics = 0;
            //lvl.blocks = new ushort[lvl.width * lvl.depth * lvl.height];
            //Player.SendMessage(p, lvl.type);
            switch (lvl.type.ToLower())
            {
                case "flat":
                case "pixel":
                case "box":
                    ushort half = (ushort)(lvl.depth / 2);
                    for (ushort x = 0; x < lvl.width; ++x)
                        for (ushort z = 0; z < lvl.height; ++z)
                            for (ushort y = 0; y < lvl.depth; ++y)
                                switch (lvl.type.ToLower())
                                {
                                    case "flat":
                                        if (y != half)
                                            lvl.SetTile(x, y, z, (y >= half) ? Block.air : Block.dirt);
                                        else
                                            lvl.SetTile(x, y, z, Block.grass);
                                        break;

                                    case "pixel":
                                        if (y == 0)
                                            lvl.SetTile(x, y, z, Block.blackrock);
                                        else if (x == 0 || x == lvl.width - 1 || z == 0 || z == lvl.height - 1)
                                            lvl.SetTile(x, y, z, Block.white);
                                        break;
                                    case "box":
                                        if (y == 1 || y == 2)
                                            lvl.SetTile(x, y, z, Block.dirt);
                                        else if (y == 3)
                                            lvl.SetTile(x, y, z, Block.grass);
                                        if (y == 0 || y == lvl.depth - 1)
                                            lvl.SetTile(x, y, z, Block.blackrock);
                                        else if (x == 0 || x == lvl.width - 1 || z == 0 || z == lvl.height - 1)
                                            lvl.SetTile(x, y, z, Block.white);
                                        break;
                                }
                    break;

                case "island":
                case "mountains":
                case "ocean":
                case "forest":
                case "desert":
                    Server.MapGen.GenerateMap(lvl, lvl.type);
                    break;

                case "empty":
                default:
                    Player.SendMessage(p, "This map has no type set.");
                    Player.SendMessage(p, "You can set the type using &a/map [map] <type>");
                    break;
            }

            Memory.ClearBlocks(lvl.name);
            Memory.ClearPortals(lvl.name);
            Player.SendMessage(p, "Done");
            Command.all.Find("reveal").Use(p, "all");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/cleanlvl [map] - Cleans a map, by regenerating it.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/cleanlvl'",
                "Cleans the map you are currently in.",
                "",
                "&3'/cleanlvl freebuild'",
                "Cleans the map 'freebuild'."
            };
        }
    }
}
