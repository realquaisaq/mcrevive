
using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;

namespace MCRevive
{
    public class CmdSpleef : Command
    {
        public override string name { get { return "spleef"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "game"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public override void Use(Player p, string message)
        {
            try
            {
                if (message == "") { Help(p); return; }
                Spleef spl;
                switch (message.Split(' ')[0])
                {
                    case "create":
                        if (p.level.GetSpleef() != null)
                        {
                            p.SendMessage("There is already a spleef game arena on this map. Use &a/spleef delete&e.");
                        }
                        p.SendMessage("Click or place two blocks to determine the edges of your spleef arena.");
                        p.ClearBlockchange();
                        p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
                        break;
                    case "sp":
                    case "startpoint":
                    case "startingpoint":
                        spl = p.level.GetSpleef();
                        spl.StartingPoints.Add(new SpleefStartingPoint(p.pos[0], p.pos[1], p.pos[2], p.rot[0]));
                        p.SendMessage("Spleef starting point was added.");
                        break;
                    case "start":
                        spl = p.level.GetSpleef();
                        if (spl.StartingPoints.Count < 2)
                            p.SendMessage("Could not start spleef: Not enough starting points created.");
                        else if (spl.Started)
                            p.SendMessage("Spleef is already running on this map. Use &a/spleef stop&e to stop.");
                        else
                            spl.Start();
                        break;
                    case "stop":
                        spl = p.level.GetSpleef();
                        if (spl.Started)
                        {
                            foreach (SpleefStartingPoint ssp in spl.StartingPoints) { ssp.p.frozen = false; }
                            spl.Restore();
                            spl.Thread.Abort();
                            spl.Started = false;
                        }
                        break;
                    case "join":
                        spl = p.level.GetSpleef();
                        if (!spl.Started)
                        {
                            p.SendMessage("Spleef on this map hasn't been started yet.");
                            return;
                        }
                        List<SpleefStartingPoint> TempSSP = spl.StartingPoints;
                        int i = 0;
                        foreach (SpleefStartingPoint ssp in TempSSP)
                        {
                            if (!ssp.Occupied)
                            {
                                SpleefStartingPoint ssp2 = spl.StartingPoints[i];
                                ssp2.p = p;
                                ssp2.Occupied = true;
                                p.SendPos(ssp2.x, ssp2.y, ssp2.z, ssp2.rotX, 0); 
                                return;
                            }
                            i++;
                        }
                        p.SendMessage("Sorry, spleef is full. Try again later =]");
                        break;
                }
            }
            catch (Exception e)
            {
                Server.s.Log(e.Message);
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/spleef create - Define the spleef arena that you built");
            Player.SendMessage(p, "/spleef sp - Define a start point for the spleef game");
            Player.SendMessage(p, "/spleef start [seconds] - Start the spleef game in a certain time");
            Player.SendMessage(p, "/spleef join - Join the spleef game on the map you're in");
            Player.SendMessage(p, "/spleef stop - Stop the spleef game");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos cpos = (CatchPos)p.blockchangeObject;
            cpos.x = x; cpos.y = y; cpos.z = z; p.blockchangeObject = cpos;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos cpos = (CatchPos)p.blockchangeObject;
            Spleef spl = new Spleef(p.level, x, y, z, cpos.x, cpos.y, cpos.z);
            Spleef.Spleefs.Add(spl);
            p.SendMessage("Spleef arena determined and saved.");
            p.SendMessage("Now create at least two spleef starting points with &a/spleef sp" + Server.DefaultColor + ".");
        }
        struct CatchPos
        {
            public ushort x, y, z;
        }
    }
}