﻿using System;
using System.Collections.Generic;
using System.Threading;

/* 
 * anyone have an idea for how to know the brick falling down?
 * Idea is that the person can move the brick by looking at its new location.
 * When you look up, it rotates.
 * 
 * look can be loaded with p.rot[0 or 1]
 * 360 * degree wanted / 255 == set rot.
 * 
 */

namespace MCRevive
{
    class CmdTetris : Command
    {
        public override string name { get { return "tetris"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "game"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdTetris() { }
        struct Pos { public ushort x, y, z; public ushort type; public Pos(ushort _x, ushort _y, ushort _z, ushort t) { x = _x; y = _y; z = _z; type = t; } };
        struct TetrisGame { public Level level; public Player player; public List<Pos> buffer; public TetrisGame(Level l, Player p) { level = l; player = p; buffer = new List<Pos>(); } }
        List<TetrisGame> TetrisGames = new List<TetrisGame>();
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            Level foundLevel = p.level;
            TetrisGames.Add(new TetrisGame(foundLevel, p));
            if (BuildTetrisField(p, foundLevel, (ushort)Math.Round((double)p.pos[0] / 32), (ushort)Math.Round((double)p.pos[1] / 32), (ushort)Math.Round((double)p.pos[2] / 32)))
            {
                while (true)
                {
                    Thread.Sleep(1000);

                }
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/tetris - Starts a tetris game.");
            Player.SendMessage(p, "Requires map to allow tetris.");
            Player.SendMessage(p, "You can allow tetris using &a'/map tetris'" + Server.DefaultColor + ".");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/tetris'",
                "This will start a game of tetris.",
                "Rememer, use &a'/map tetris'" + Server.DefaultColor + " to enable tetris."
            };
        }

        bool BuildTetrisField(Player p, Level l, ushort x, ushort y, ushort z)
        {
            try
            {
                x = (ushort)(x - 6);
                y = (ushort)(y - 8);
                if (l.GetTile((ushort)(x + 10), (ushort)(y + 16), (ushort)(z - 10)) == Block.Zero) throw new Exception();
                p.SendPos((ushort)((x + 6) * 32 - 16), (ushort)((y + 8) * 32), (ushort)(z * 32 - 16), 0, 0);
            }
            catch { Player.SendMessage(p, "The tetris game cannot fit in front of you."); return false; }

            for (ushort X = x; X <= x + 10; ++X)
                for (ushort Y = y; Y <= y + 16; ++Y)
                    for (ushort Z = z; Z >= z - 10; --Z)
                    {
                        TetrisGames.Find(tg => tg.level == l && tg.player == p).buffer.Add(new Pos(X, Y, Z, l.GetTile(X, Y, Z)));
                    }
            for (ushort X = x; X <= x + 10; ++X)
                for (ushort Y = y; Y <= y + 16; ++Y)
                    for (ushort Z = z; Z >= z - 10; --Z)
                    {
                        l.Blockchange(null, X, Y, Z, X == x || Z == z || Y == y || X == x + 10 || Y == y + 16 || Z == z - 10 || (X == x + 5 && Y == y + 6 && Z == z - 1) ? Block.op_glass : Z == z - 9 ? Block.opsidian : Block.op_air, true);
                    }
            p.SendPos((ushort)((x + 6) * 32 - 16), (ushort)((y + 8) * 32), (ushort)(z * 32 - 16), 0, 0);

            return true;
        }
    }
}
