using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;

namespace MCRevive
{
    public class CmdInfection : Command
    {
        public override string name { get { return "infection"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "game"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdInfection() { }
        List<string> InfectGames = new List<string>();
        public override void Use(Player p, string message)
        {
            if (message.IndexOf(' ') == -1) message += " ";
            string[] param = message.Split(' ');
            if (param[0] == "start" || param[0] == "")
            {
                Level foundLevel = p.level;
                Random rand = new Random();
                if (param[1] != "")
                {
                    Level temp = Level.Find(param[1]);
                    if (temp == null)
                        if (File.Exists("levels/" + param[1] + ".lvl") || File.Exists("levels/" + param[1] + ".mcqlvl"))
                        {
                            Command.all.Find("load").Use(p, param[1]);
                            temp = Level.Find(param[1]);
                            if (temp == null)
                                return;
                        }
                        else
                        {
                            Player.SendMessage(p, "Unable to find level: " + param[1] + ".");
                            return;
                        }
                    if (temp != null)
                        foundLevel = temp;
                }

                if (InfectGames.Contains(foundLevel.name))
                {
                    Player.SendMessage(p, "Theres already a game going on!");
                    return;
                }
                if (foundLevel.ListPlayers.Count < 3)
                {
                    Player.SendMessage(p, "Theres not enough players in " + foundLevel.name + " to play!");
                    return;
                }
                
                foreach (string player in foundLevel.ListPlayers)
                    Player.Find(player).isZombie = false;

                InfectGames.Add(foundLevel.name);
                Player.GlobalMessageLevel(foundLevel, "The Infection Game will start in 30 seconds!");
                Thread.Sleep(15000);
                Player.GlobalMessageLevel(foundLevel, "The Infection Game will start in 15 seconds!");
                Thread.Sleep(15000);
                int next = rand.Next(0, foundLevel.ListPlayers.Count - 1);
                Player.Find(foundLevel.ListPlayers[next]).isZombie = true;
                Player.GlobalMessageLevel(foundLevel, foundLevel.ListPlayers[next] + " is the zombie, RUN!");
                
                Thread go = new Thread(new ThreadStart(delegate
                {
                    DateTime GameStart = DateTime.Now;
                    while (InfectGames.Contains(foundLevel.name))
                    {
                        Thread.Sleep(200);
                        int infectedPlayers = 0;
                        
                        foreach (string pl in foundLevel.ListPlayers)
                        {
                            Player firstP = Player.Find(pl);
                            if (firstP.isZombie)
                            {
                                infectedPlayers++;
                                foreach (string play in foundLevel.ListPlayers)
                                {
                                    Player secondP = Player.Find(play);
                                    if (secondP == firstP || secondP.isZombie) break;
                                    bool xFine = false, yFine = false, zFine = false;
                                    for (int x = -1; x < 2; ++x)
                                        for (int y = -1; y < 2; ++y)
                                            for (int z = -1; z < 2; ++z)
                                            {
                                                if (firstP.pos[0] + x == secondP.pos[0])
                                                    xFine = true;
                                                if (firstP.pos[1] + y == secondP.pos[1])
                                                    yFine = true;
                                                if (firstP.pos[2] + z == secondP.pos[2])
                                                    zFine = true;
                                            }
                                    if (xFine && yFine && zFine)
                                    {
                                        secondP.isZombie = true;
                                        Player.GlobalMessageLevel(foundLevel, pl + " has zombified " + play + "!");
                                        Player.SendMessage(secondP, "You are now a zombie, go catch the others!");
                                        Command.all.Find("nick").Use(null, "#" + secondP.name + " " + secondP.name + "-&4Zombie&f");
                                    }
                                }
                            }
                        }

                        for (int ii = 1; ii <= 10; ++ii)
                            if (DateTime.Parse(GameStart.AddMinutes(ii).ToString("HH:mm")) < DateTime.Parse(DateTime.Now.ToString("HH:mm")))
                                Player.GlobalMessageLevel(foundLevel, "Theres " + ((10 - ii < 5) ? "only " : "") + (10 - ii) + " minutes left.");
                        
                        if (infectedPlayers + 1 == foundLevel.ListPlayers.Count || GameStart.AddMinutes(10) < DateTime.Now)
                        {
                            string winners = "noone";
                            foreach (string pla in foundLevel.ListPlayers)
                                if (!Player.Find(pla).isZombie) winners += ", " + pla;
                            Player.GlobalMessageLevel(foundLevel, "The Game has ended, the winner" + ((winners.IndexOf(',') != -1)
                                ? "s are: " : " is: ") + winners.Substring(2) + "."); 
                        }
                    }

                    foreach (string player in foundLevel.ListPlayers)
                    {
                        Player who = Player.Find(player);
                        who.isZombie = false;
                        Command.all.Find("nick").Use(null, "#" + who.name + " " + who.realname);
                    }

                    InfectGames.Remove(foundLevel.name);
                }));
                go.Start();
            }
            else if (param[0] == "stop")
            {
                Level foundLevel = p.level;
                if (param[1] != "")
                {
                    Level temp = Level.Find(param[1]);
                    if (temp == null)
                        if (File.Exists("levels/" + param[1] + ".lvl") || File.Exists("levels/" + param[1] + ".mcqlvl"))
                        {
                            Command.all.Find("load").Use(p, param[1]);
                            temp = Level.Find(param[1]);
                            if (temp == null)
                                return;
                        }
                        else
                        {
                            Player.SendMessage(p, "Unable to find level: " + param[1] + ".");
                            return;
                        }
                    foundLevel = temp;
                }
                if (InfectGames.IndexOf(InfectGames.Find(l => l == foundLevel.name)) == -1)
                {
                    Player.SendMessage(p, "Theres not running any game in " + foundLevel.name);
                    return;
                }

                foreach (string player in foundLevel.ListPlayers)
                {
                    Player who = Player.Find(player);
                    who.isZombie = false;
                    Command.all.Find("nick").Use(null, "#" + who.name + " " + who.realname);
                }

                InfectGames.Remove(foundLevel.name);
                if (p.level != foundLevel) Player.SendMessage(p, "The game in " + foundLevel.name + " has been stopped.");
                Player.GlobalMessageLevel(foundLevel, "The Infection Game has been stopped.");
            }
            else
            {
                Help(p);
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/infection [\"start\"] [map] - to start a game.");
            Player.SendMessage(p, "/infection <\"stop\"> [map] - to stop a game.");
            Player.SendMessage(p, "There needs to be 3 players to play Infection.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}