﻿using System;

namespace MCRevive
{
    class CmdDebug : Command
    {
        public override string name { get { return "debug"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.NoOne; } }
        public CmdDebug() { }
        public override void Use(Player p, string message)
        {
            p.SendMessage("1&a &b &c &d &e &f   ");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/debug - Debugs whatever.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                ""
            };
        }
    }
}
