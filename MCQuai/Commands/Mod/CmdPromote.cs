namespace MCRevive
{
    public class CmdPromote : Command
    {
        public override string name { get { return "promote"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "moderation"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdPromote() { }
        public override void Use(Player p, string message)
        {
            if (message == "" || message.IndexOf(' ') != -1) { Help(p); return; }
            Player who = Player.Find(message);
            if (who == null) { Player.SendMessage(p, "Could not find player: " + message); return; }
            if (p != null && p.group.Permission <= who.group.Permission && !p.ignorePermission) { Player.SendMessage(p, "You cant promote someone equal to or greater than your own rank."); return; }
            Group newgrp = Group.GroupList[Group.GroupList.IndexOf(who.group) + 1];
            if (newgrp.Permission > 120) { Player.SendMessage(p, "That player can't get a higher rank."); return; }
            Command.all.Find("setrank").Use(p, who.name + " " + newgrp.name);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/promote <player> - gives <player> a promtotion, if possible.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/promote quaisaq'",
                "This will promote the player quaisaq by 1 rank."
            };
        }
    }
}