
using System;

namespace MCRevive
{
    public class CmdAntispeedhack : Command
    {
        public override string name { get { return "antispeedhack"; } }
        public override string shortcut { get { return "ash"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdAntispeedhack() { }
        public override void Use(Player p, string message)
        {
            message = message.ToLower();
            if (message != "off" && message != "on") { Help(p); return; }
            if (message == "on")
            {
                if (!p.level.antispeedhack)
                {
                    p.level.antispeedhack = true;
                    Player.GlobalMessageLevel(p.level, "Anti-speedhack on map &a" + p.level.name + "&e is now &aEnabled&e.");
                    foreach (Player pp in Player.players)
                    {
                        if (pp.level == p.level)
                        {
                            pp.pos250msAgo = new ushort[] { 0, 0, 0 };
                            pp.InitAntiSpeedhack(500);
                        }
                    }
                }
                else
                {
                    p.SendMessage("Anti-speedhack for map " + p.level.name + " is already enabled.");
                }
            }
            else
            {
                if (p.level.antispeedhack)
                {
                    p.level.antispeedhack = false;
                    Player.GlobalMessageLevel(p.level, "Anti-speedhack on map &a" + p.level.name + "&e is now &aDisabled&e.");
                    foreach (Player pp in Player.players)
                    {
                        if (pp.level == p.level)
                        {
                            pp.InitAntiSpeedhack(500);
                        }
                    }
                }
                else
                {
                    p.SendMessage("Anti-speedhack for map " + p.level.name + " is already disabled.");
                }
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/antispeedhack [on/off] - Switch anti-speedhack for the current map on or off");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
