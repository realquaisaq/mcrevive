/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MCRevive
{
    class CmdRestore : Command
    {
        public override string name { get { return "restore"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdRestore() { }
        public override void Use(Player p, string message)
        {
            if (message.Split(' ').Length == 1) message += " ";
            Level foundLevel = p.level;
            if (message != " ")
            {
                string[] param = message.Split(' ');
                if (param.Length == 2)
                {
                    if (param[1] != "")
                    {
                        if (Level.Find(param[0]) != null) foundLevel = Level.Find(param[0]);
                        else
                        {
                            if (File.Exists("levels/" + param[0] + ".lvl") || File.Exists("levels/" + param[0] + ".mcqlvl"))
                            {
                                Command.all.Find("load").Use(p, param[0]);
                                Command.all.Find("restore").Use(p, param[0] + " " + param[1]);
                                return;
                            }
                            else
                            {
                                Player.SendMessage(p, "Could not find level: " + param[0]);
                                return;
                            }
                        }
                    }
                    if (param[1] == "") { param[1] = param[0]; param[0] = p.level.name; }
                    if (File.Exists("levels/backup/" + foundLevel.name + "/" + param[1] + "/" + foundLevel.name + ".lvl") || File.Exists("levels/backup/" + foundLevel.name + "/" + param[1] + "/" + foundLevel.name + ".mcqlvl"))
                    {
                        try
                        {
                            if (File.Exists("levels/backup/" + foundLevel.name + "/" + param[1] + "/" + foundLevel.name + ".mcqlvl"))
                                File.Copy("levels/backup/" + foundLevel.name + "/" + param[1] + "/" + foundLevel.name + ".mcqlvl", "levels/" + foundLevel.name + ".mcqlvl", true);
                            else
                                File.Copy("levels/backup/" + foundLevel.name + "/" + param[1] + "/" + foundLevel.name + ".lvl", "levels/" + foundLevel.name + ".lvl", true);
                            
                            Level temp = Level.Load(foundLevel.name);
                            temp.physThread.Start();
                            if (temp != null)
                            {
                                foundLevel.spawnx = temp.spawnx;
                                foundLevel.spawny = temp.spawny;
                                foundLevel.spawnz = temp.spawnz;

                                foundLevel.height = temp.height;
                                foundLevel.width = temp.width;
                                foundLevel.depth = temp.depth;

                                foundLevel.blocks = temp.blocks;
                                foundLevel.setPhysics(0);
                                foundLevel.ClearPhysics();

                                foreach (Player all in Player.players) if (all.level == foundLevel) Command.all.Find("reveal").Use(p, all.name);
                                Player.GlobalMessage(foundLevel.name[0].ToString().ToUpper() + foundLevel.name.Remove(0, 1) + " was restored to backup " + param[1]);
                            }
                            else
                            {
                                Server.s.Log("Restore nulled");
                                if (File.Exists("levels/" + foundLevel.name + ".mcqlvl.backup"))
                                    File.Copy("levels/" + foundLevel.name + ".mcqlvl.backup", "levels/" + foundLevel.name + ".mcqlvl", true);
                                else
                                    File.Copy("levels/" + foundLevel.name + ".lvl.backup", "levels/" + foundLevel.name + ".lvl", true);
                            }

                        }
                        catch { Server.s.Log("Restore fail"); }
                    }
                    else { Player.SendMessage(p, "Backup " + param[1] + " does not exist."); }
                }
            }
            else
            {
                try
                {
                    if (Directory.Exists("levels/backup/" + foundLevel.name))
                    {
                        string[] directories = Directory.GetDirectories("levels/backup/" + foundLevel.name);
                        int backupNumber = directories.Length;
                        Player.SendMessage(p, foundLevel.name + " has " + backupNumber + " backups.");

                        bool foundOne = false; string foundRestores = "";
                        foreach (string s in directories)
                        {
                            string directoryName = s.Substring(15 + foundLevel.name.Length);
                            try
                            {
                                int.Parse(directoryName);
                            }
                            catch
                            {
                                foundOne = true;
                                foundRestores += ", " + directoryName;
                            }
                        }

                        if (foundOne)
                        {
                            Player.SendMessage(p, "Custom-named restores:");
                            Player.SendMessage(p, "> " + foundRestores.Substring(2));
                        }
                    }
                    else
                    {
                        Player.SendMessage(p, p.level.name + " has no backups yet.");
                    }
                }
                catch (Exception ez) { Server.ErrorLog(ez); }
            }
        }

        public override void Help(Player p)
        {
            Player.SendMessage(p, "/restore [level] [number] - restores a previous backup of a map.");
            Player.SendMessage(p, "If nothing is added, output will be number of backups.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}