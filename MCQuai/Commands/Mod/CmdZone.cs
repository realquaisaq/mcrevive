﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MCRevive
{
    class CmdZone : Command
    {
        public override string name { get { return "zone"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdZone() { }
        public override void Use(Player p, string message)
        {
            CatchPos cpos;

            if (message == "" || message.ToLower() == "check")
            {
                p.zoneCheck = true;
                Player.SendMessage(p, "Place a block where you would like to check for zones.");
                return;
            }
            else if (p.group.Permission < LevelPermission.Operator && !p.ignorePermission)
            {
                Player.SendMessage(p, "Reserved for OP+");
                return;
            }

            if (message.IndexOf(' ') == -1)
            {
                if (message.ToLower() == "del")
                {
                    p.zoneDel = true;
                    Player.SendMessage(p, "Place a block where you would like to delete a zone.");
                    return;
                }
                else
                {
                    Help(p);
                    return;
                }
            }


            if (message.ToLower() == "del all")
            {
                if (p.group.Permission < LevelPermission.Admin && !p.ignorePermission)
                {
                    Player.SendMessage(p, "Only Admin+ may delete all zones at once");
                    return;
                }
                else
                {
                    for (int i = 0; i < p.level.ZoneList.Count; i++)
                    {
                        Level.Zone Zn = p.level.ZoneList.ToArray()[i];

                        foreach (string line in File.ReadAllLines("memory/zones/" + p.level.name + ".zone"))
                        {
                            if (line == Zn.smallX + "," + Zn.smallY + "," + Zn.smallZ + "&" + Zn.bigX + "," + Zn.bigY + "," + Zn.bigZ + "&" + Zn.Owner)
                                File.WriteAllText("memory/zones/" + p.level.name + ".zone", File.ReadAllText("memory/zones/" + p.level.name + ".zone").Replace(line, ""));
                        }

                        Player.SendMessage(p, "Zone deleted for &b" + Zn.Owner);
                        p.level.ZoneList.Remove(p.level.ZoneList[i]);
                        if (i == p.level.ZoneList.Count) { Player.SendMessage(p, "Finished removing all zones"); return; }
                        i--;
                    }
                }
            }

            if (Group.Find(message.Split(' ')[1]) != null)
            {
                message = message.Split(' ')[0] + " grp" + Group.Find(message.Split(' ')[1]).name;
            }

            if (message.Split(' ')[0].ToLower() == "add")
            {
                Player foundPlayer = Player.Find(message.Split(' ')[1]);
                if (foundPlayer == null)
                    cpos.Owner = message.Split(' ')[1].ToString();
                else
                    cpos.Owner = foundPlayer.name;
            }
            else { Help(p); return; }

            if (!Player.ValidName(cpos.Owner)) { Player.SendMessage(p, "INVALID NAME."); return; }

            cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;

            Player.SendMessage(p, "Place two blocks to determine the edges.");
            Player.SendMessage(p, "Zone for: &b" + cpos.Owner + Server.DefaultColor + ".");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/zone ['check'] - Checks for zones.");
            Player.SendMessage(p, "/zone add <name/rank> - Creates a zone for <name/rank+>.");
            Player.SendMessage(p, "/zone del ['all'] - Deletes the zone clicked or all zones in map");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/zone' or '/zone check'",
                "This will check for zones at the specified point.",
                "&3'/zone add quaisaq'",
                "select 2 points",
                "Now only quaisaq can build in that area.",
                "&3'/zone del'",
                "Deletes all zones touching selected block.",
                "&3'/zone del all'",
                "This will delete all zones in the current map."
            };
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos bp = (CatchPos)p.blockchangeObject;
            bp.x = x; bp.y = y; bp.z = z; p.blockchangeObject = bp;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }

        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos cpos = (CatchPos)p.blockchangeObject;

            Level.Zone Zn;

            Zn.smallX = Math.Min(cpos.x, x);
            Zn.smallY = Math.Min(cpos.y, y);
            Zn.smallZ = Math.Min(cpos.z, z);
            Zn.bigX = Math.Max(cpos.x, x);
            Zn.bigY = Math.Max(cpos.y, y);
            Zn.bigZ = Math.Max(cpos.z, z);
            Zn.Owner = cpos.Owner;

            p.level.ZoneList.Add(Zn);

            File.AppendAllText("memory/zones/" + p.level.name + ".zone", Zn.smallX + "," + Zn.smallY + "," + Zn.smallZ + "&"
                + Zn.bigX + "," + Zn.bigY + "," + Zn.bigZ + "&" + Zn.Owner + Environment.NewLine);
            Player.SendMessage(p, "Added zone for &b" + cpos.Owner);
        }

        struct CatchPos { public ushort x, y, z; public string Owner; }
    }
}