using System;
using System.IO;

namespace MCRevive
{
    public class CmdSetmain : Command
    {
        public override string name { get { return "setmain"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Owner; } }
        public CmdSetmain() { }
        public override void Use(Player p, string message)
        {
            if (message.Split(' ').Length != 1) { Help(p); return; }
            Level lvl = Level.Find(message);
            if (lvl == null)
            {
                if (!File.Exists("levels/" + message + ".lvl") && !File.Exists("levels/" + message + ".mcqlvl")) { Player.SendMessage(p, "Could not find level specified."); return; }
                lvl = Level.Load(message);
            }
            Server.mainLevel = lvl;
            Server.level = lvl.name;
            Server.s.Log("New spawn level was set by " + p.name + ": " + lvl.name);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/setmain <level> - Set the spawn level for the server.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}