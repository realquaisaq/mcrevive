﻿using System;
using System.IO;

namespace MCRevive
{
    class CmdXBan : Command
    {
        public override string name { get { return "xban"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdXBan() { }
        public override void Use(Player p, string message)
        {
            if (message == "" || message.Split(' ').Length != 1) { Help(p); return; }
            Player who = Player.Find(message);
            if (who != null)
            {
                Command.all.Find("ban").Use(p, who.name);
                Command.all.Find("ipban").Use(p, who.name);
                Command.all.Find("undo").Use(p, who.name + " 5000000");
                Command.all.Find("kick").Use(p, who.name);
                Command.all.Find("undo").Use(p, who.name + " 5000000");
            }
            else
            {
                string ip = "";
                bool exist = false;
                try
                {
                    foreach (string line in File.ReadAllLines("memory/player/save/" + message + ".txt"))
                    {
                        if (line != "")
                            if (line.Split('=')[0].ToLower().Trim() == "ip")
                                ip = line.Split('=')[1].ToLower().Trim();
                    }
                    exist = true;
                }
                catch { exist = false; }
                Command.all.Find("ban").Use(p, message);
                if (exist && ip != "") Command.all.Find("ipban").Use(p, ip);
                Command.all.Find("undo").Use(p, message + " 5000000");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/xban <name> - Ban, undo, IP-ban and kick <name>.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/xban quaisaq'",
                "This is will ban, IP-ban, undo and kick quaisaq."
            };
        }
    }
}
