using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdVote : Command
    {
        public override string name { get { return "vote"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdVote() { }
        public override void Use(Player p, string message)
        {
            if (message == "" || (message.IndexOf(' ') == -1 && message != "stop")) { Help(p); return; }
            string[] param = message.Split(' ');
            if (message.ToLower() == "stop" && (p.ignorePermission || p.group.Permission >= LevelPermission.Operator))
            {
                if (Player.voteThread.IsAlive)
                {
                    Player.votes[10] = -1;
                    Player.GlobalMessage("Vote stopped!");
                }
                else
                    Player.SendMessage(p, "There is not a vote going on right now.");
            }
            else if (param[0].ToLower() == "kick" || param[0].ToLower() == "ban" || param[0].ToLower() == "mute")
            {
                message = message.Remove(0, message.IndexOf(' ') + 1);
                Player who = Player.Find(param[1]);
                if (who == null) { Player.SendMessage(p, "Player " + param[1] + " could not be found."); return; }
                else if (who.group.Permission >= LevelPermission.Operator) { Player.SendMessage(p, "You cannot vote" + param[0].ToLower() + " someone with a permission above " + (LevelPermission.Operator - 1)); return; }
                if (message.IndexOf(' ') != -1) { Player.SendMessage(p, "you vote" + param[0].ToLower() + " more then 1 person at a time."); return; }
                Player.voteType = 0;
                Player.Vote(p, param[0].ToLower(), new List<string> { who.name });
                Player.SendMessage(p, "&cYou should vote aswell!");
            }
            else if (param[0][0] == '\'')
            {
                Command cmd = Command.all.Find(param[0].Replace("'", ""));
                if (cmd == null)
                {
                    Player.SendMessage(p, "Could not find command: " + param[0].Replace("'", ""));
                    return;
                }
                if (p.group.CanExecute(cmd))
                {
                    Player.voteType = 1;
                    Player.Vote(p, message.Replace("'", ""), new List<string> { "yes", "no" });
                    Player.SendMessage(p, "&cYou should vote aswell!");
                }
                else
                    Player.SendMessage(p, "Your rank isn't powerful enough to execute this command.");
            }
            else
            {
                if (message.IndexOf('*') == -1)
                    message += "*yes,no";
                if (message.Substring(message.IndexOf('*') + 1).IndexOf('*') == -1)
                    message += "*true";
                try { Player.voteType = bool.Parse(message.Split('*')[2]) ? 2 : 3; }
                catch { Player.voteType = 2; }

                List<string> str = new List<string>();
                foreach (string s in message.Split('*')[1].Split(','))
                    if (str.Count < 8)
                        str.Add(s.Trim().ToLower());
                    else
                    {
                        Player.SendMessage(p, "too many different votes.");
                        return;
                    }
                Player.Vote(p, message.Split('*')[0], str);
                Player.SendMessage(p, "&cYou should vote aswell!");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "&a/vote <type> <player>- kick, ban or mute a player.");
            Player.SendMessage(p, "&aTypes are: Kick, Ban, Mute.");
            Player.SendMessage(p, "&b/vote <'command'> <command message> - executes command.");
            Player.SendMessage(p, "&c/vote <question> [\"*\" answer1, answer2...] [\"*true/false\"] - custom question.");
            Player.SendMessage(p, "&cTrue/false is if you are allowed to vote more then once, default is true.");
            Player.SendMessage(p, "&aExample 1: \"/vote kick quaisaq\" will send a vote to everyone, determing wheater quaisaq should be kicked or not.");
            Player.SendMessage(p, "&bExample 2: \"/vote 'say' roflmao\" will execute the command say if enough people answers yes.");
            Player.SendMessage(p, "&cExample 3 (custom): \"/vote is quaisaq awesome? * yes, no, maybe");
            Player.SendMessage(p, "&dYou can answer a vote by typing \"!<answer>\", you can sepperate your votes using comma.");
            Player.SendMessage(p, "&dExample: \"!3, yes, 2\" - this would vote on vote-possibility 3 and 2, and also vote \"yes\".");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}