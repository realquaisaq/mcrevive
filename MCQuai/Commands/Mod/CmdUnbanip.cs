/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Text.RegularExpressions;

namespace MCRevive
{
    public class CmdUnbanip : Command
    {
        Regex regex = new Regex(@"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$");
        public override string name { get { return "unbanip"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdUnbanip() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            Player who = Player.Find(message);
            if (who != null) message = who.ip;
            if (!regex.IsMatch(message)) { Player.SendMessage(p, "Not a valid ip!"); return; }
            if (p.ip == message && !p.ignorePermission) { Player.SendMessage(p, "You shouldn't be able to use this command..."); return; }
            if (!Server.bannedIP.Contains(message)) { Player.SendMessage(p, message + " doesn't seem to be banned..."); return; }
            
            Player.GlobalMessage(message + " got &8unip-banned" + Server.DefaultColor + "!");
            Server.bannedIP.Remove(message);
            Server.bannedIP.Save("banned-ip.txt", false);
            Server.s.Log("IP-UNBANNED: " + message);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/unbanip <ip> - Un-bans an ip.");
            Player.SendMessage(p, "You can use a name, if the player is online");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/unbanip 123.45.67.89'",
                "This will unban the IP '123.45.67.89'.",
                "&a" + new string('-', 60),
                "&3'/unbanip quaisaq'",
                "If quaisaq is online, you can unban his IP, by using his name."
            };
        }
    }
}