using System;
using System.Collections.Generic;
using System.IO;

namespace MCRevive
{
    public class CmdUndo : Command
    {
        public override string name { get { return "undo"; } }
        public override string shortcut { get { return "u"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdUndo() { }
        public override void Use(Player p, string message)
        {
            ushort b; Int64 seconds = 30; Player who; Player.UndoPos Pos; int CurrentPos = 0;
            if (p != null) p.RedoBuffer.Clear();

            if (message == "") message = p.name + " 30";

            if (message.Split(' ').Length == 2)
            {
                if (message.Split(' ')[1].ToLower().Trim() == "all" &&
                    (p != null && (p.group.Permission >= Group.findPlayerGroup(message.Split(' ')[0]).Permission &&
                    p.group.Permission > LevelPermission.Operator) || p.ignorePermission))
                {
                    seconds = 5000000;
                }
                else if (p != null && p.group.Permission <= LevelPermission.Operator && message.Split(' ')[1].ToLower().Trim() == "all")
                {
                    Player.SendMessage(p, "Only people with a permission above " + LevelPermission.Operator + " can do that");
                }
                else
                {
                    try
                    {
                        seconds = Int64.Parse(message.Split(' ')[1]);
                    }
                    catch
                    {
                        Player.SendMessage(p, "Invalid seconds.");
                        return;
                    }
                }
            }
            else
            {
                try
                {
                    seconds = int.Parse(message);
                    if (p != null) message = p.name + " " + message;
                }
                catch
                {
                    seconds = 30;
                    message = message + " 30";
                }
            }

            if (seconds == 0) seconds = 5400;

            who = Player.Find(message.Split(' ')[0]);
            if (who != null)
            {
                if (p != null)
                {
                    if (who.group.Permission > p.group.Permission && who != p && !p.ignorePermission)
                    { Player.SendMessage(p, "Cannot undo a user of higher or equal rank"); return; }

                    if (who != p && p.group.Permission < LevelPermission.Operator && !p.ignorePermission)
                    { Player.SendMessage(p, "Only an OP+ may undo other people's actions"); return; }

                    if (p.group.Permission < LevelPermission.Builder && seconds > 120 && !p.ignorePermission)
                    { Player.SendMessage(p, "Guests may only undo 2 minutes."); return; }

                    else if (p.group.Permission < LevelPermission.AdvBuilder && seconds > 300 && !p.ignorePermission)
                    { Player.SendMessage(p, "Builders may only undo 5 minutes."); return; }

                    else if (p.group.Permission < LevelPermission.Operator && seconds > 1200 && !p.ignorePermission)
                    { Player.SendMessage(p, "AdvBuilders may only undo 600 seconds."); return; }

                    else if (p.group.Permission == LevelPermission.Operator && seconds > 5400 && !p.ignorePermission)
                    { Player.SendMessage(p, "Operators may only undo 5400 seconds."); return; }
                }

                for (CurrentPos = who.UndoBuffer.Count - 1; CurrentPos >= 0; --CurrentPos)
                {
                    try
                    {
                        Pos = who.UndoBuffer[CurrentPos];
                        Level foundLevel = Level.FindExact(Pos.mapName);
                        b = foundLevel.GetTile(Pos.x, Pos.y, Pos.z);
                        if (Pos.timePlaced.AddSeconds(seconds) >= DateTime.Now)
                        {
                            if (b == Pos.newtype || Block.Convert(b) == Block.water || Block.Convert(b) == Block.lava)
                            {
                                foundLevel.PhysBlockchange(Pos.x, Pos.y, Pos.z, Pos.type, true);

                                Pos.newtype = Pos.type; Pos.type = b;
                                if (p != null) p.RedoBuffer.Add(Pos);
                                who.UndoBuffer.RemoveAt(CurrentPos);
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch { }
                }

                if (p != who) Player.GlobalChat(p, who.color + who.name + Server.DefaultColor + "'s actions for the past &b" + seconds + " seconds were undone.", false);
                else Player.SendMessage(p, "Undid your actions for the past &b" + seconds + Server.DefaultColor + " seconds.");
                return;
            }
            else
            {
                if (p != null)
                {
                    if (p.group.Permission < LevelPermission.Operator && !p.ignorePermission) { Player.SendMessage(p, "Reserved for OP+"); return; }
                    if (seconds > 5400 && p.group.Permission == LevelPermission.Operator && !p.ignorePermission) { Player.SendMessage(p, "Only SuperOPs may undo more than 90 minutes."); return; }
                }

                bool FoundUser = false;

                try
                {
                    Player.SendMessage(p, "Player offline...");
                    DirectoryInfo di;
                    string[] fileContent;

                    p.RedoBuffer.Clear();
                    string temp = "memory/undo/" + message.Split(' ')[0];
                    int idk = 0;
                    while (idk < 2)
                    {
                        if (Directory.Exists(temp))
                        {
                            di = new DirectoryInfo(temp);

                            for (int i = di.GetFiles("*.undo").Length - 1; i >= 0; i--)
                            {
                                fileContent = File.ReadAllText(temp + "/" + i + ".undo").Split('&');
                                if (!undoBlah(fileContent, seconds, p)) break;
                            }
                            FoundUser = true;
                        }
                        idk++;
                        temp = "memory/undoPrevious/" + message.Split(' ')[0];
                    }
                    if (FoundUser) Player.GlobalChat(p, Group.findPlayerGroup(message.Split(' ')[0]).color + message.Split(' ')[0] +
                        Server.DefaultColor + "'s actions for the past &b" + seconds + Server.DefaultColor + " seconds were undone.", false);
                    else Player.SendMessage(p, "Could not find player specified.");
                }
                catch (Exception e)
                {
                    Server.ErrorLog(e);
                }
            }
        }

        public bool undoBlah(string[] fileContent, Int64 seconds, Player p)
        {
            Player.UndoPos Pos;
            foreach (string s in fileContent)
            {
                try
                {
                    Level foundLevel = Level.FindExact(s.Split('^')[0]);
                    if (foundLevel != null)
                    {
                        Pos.mapName = foundLevel.name;
                        Pos.x = Convert.ToUInt16(s.Split('^')[1]);
                        Pos.y = Convert.ToUInt16(s.Split('^')[2]);
                        Pos.z = Convert.ToUInt16(s.Split('^')[3]);

                        Pos.type = foundLevel.GetTile(Pos.x, Pos.y, Pos.z);

                        if (Pos.type == Convert.ToUInt16(s.Split('^')[6]) || Block.Convert(Pos.type) == Block.water || Block.Convert(Pos.type) == Block.lava || Pos.type == Block.grass)
                        {
                            Pos.newtype = Convert.ToUInt16(s.Split('^')[5]);
                            Pos.timePlaced = DateTime.Now;
                            foundLevel.PhysBlockchange(Pos.x, Pos.y, Pos.z, Pos.newtype, true);
                            if (p != null) p.RedoBuffer.Add(Pos);
                        }
                    }
                    else return false;
                }
                catch { Player.SendMessage(p, "ERROR"); }
            }

            return true;
        }

        public override void Help(Player p)
        {
            Player.SendMessage(p, "/undo [seconds] - Unodes your own actions, for the past [seconds].");
            Player.SendMessage(p, "/undo <player> [seconds] - Undoes the blockchanges made by [player] in the previous [seconds].");
            Player.SendMessage(p, "/undo <player> all - Will undo 138 hours for [player] <Admin+>");
            Player.SendMessage(p, "/undo <player> 0 - Will undo 30 minutes <Operator+>");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/undo'",
                "You will undo your own actions, for the last 30 seconds.",
                "&a" + new string('-', 60),
                "&3'/undo 45'",
                "You will undo your own actions, for the last 45 seconds.",
                "",
                "&3'/undo quaisaq'",
                "Will undo quaisaq's actions for the past 30 seconds.",
                "&a" + new string('-', 60),
                "&3'/undo quaisaq 120",
                "Will undo quaisaq's actions for the past 120 seconds.",
                "",
                "&3'/undo quaisaq 0'",
                "This will undo quaisaq's actions for the past 30 minutes.",
                (p == null || p.group.Permission > LevelPermission.Operator ? "&a" : "&4") + "OP+",
                "",
                "&3'/undo quaisaq all'",
                "This will undo quaisaq's actions for the past 128 hours.",
                (p == null || p.group.Permission > LevelPermission.Admin ? "&a" : "&4") + "ADMIN+"
            };
        }
    }
}