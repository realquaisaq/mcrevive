using System;

namespace MCRevive
{
    class CmdRestart : Command
    {
        public override string name { get { return "restart"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Owner; } }
        public CmdRestart() { }
        public override void Use(Player p, string message)
        {
            try
            {
                if (message == "") message = "10";
                int secs = int.Parse(message);
                System.Threading.Thread.Sleep(secs * 1000);
                System.Windows.Forms.Application.Restart();
            }
            catch
            {
                Player.SendMessage(p, "invalid seconds");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/restart [seconds] - restarts server after [seconds], default 10.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
