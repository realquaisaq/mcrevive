﻿using System;
using System.Collections.Generic;

namespace MCRevive
{
    class CmdMap : Command
    {
        public override string name { get { return "map"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdMap() { }
        List<string> possible = new List<string>(new string[] { "motd", "edgewater", "perbuild", "pervisit" });
        public override void Use(Player p, string message)
        {
            Level lvl = Level.Find(message == "" ? p.level.name : message.Split(' ')[0]);
            if (lvl == null)
                if (possible.Contains(message.Split(' ')[0].ToLower())) { message = p.level.name + " " + message; goto type; }
                else { Player.SendMessage(p, "Could not find map: " + message); return; }
            if (message == "" || message.Split(' ').Length == 1)
            {
                Player.SendMessage(p, "MOTD: " + lvl.motd.ToLower() == "ignore" ? Server.motd : lvl.motd);
                Player.SendMessage(p, "Edge Water: " + (lvl.edgeWater ? "&a" : "&4") + lvl.edgeWater);
                Player.SendMessage(p, "Perbuild: " + Group.Find(lvl.permissionbuild + "").color + LevelPermission.ToName(lvl.permissionbuild));
                Player.SendMessage(p, "Pervisit: " + Group.Find(lvl.permissionvisit + "").color + LevelPermission.ToName(lvl.permissionvisit));
                //Player.SendMessage(p, "Tetris allowed: " + (lvl.tetrisAllowed ? "&a" : "&4") + lvl.tetrisAllowed);
                return;
            }

        type:
            lvl = Level.Find(message.Split(' ')[0]);
            if (lvl == null) { Player.SendMessage(p, "Could not find map: " + message.Split(' ')[0]); return; }

            string value = message.Substring(message.IndexOf(message.Split(' ')[1]) + message.Split(' ')[1].Length).Trim();
            switch (message.Split(' ')[1].ToLower())
            {
                case "motd":
                    if (message.Split(' ').Length == 2)
                        lvl.motd = "ignore";
                    else if (value.Length > 126) { Player.SendMessage(p, "MOTD cannot be over 125 chars long."); return; }
                    else
                        lvl.motd = value;
                    Player.SendMessage(p, value == "ignore" ? "The MOTD for " + lvl.name + ", has been removed." : "The MOTD for " + lvl.name + ", has been changed.");
                    break;
                case "edgewater":
                    if (message.Split(' ').Length == 2)
                    {
                        lvl.edgeWater = !lvl.edgeWater;
                        Player.SendMessage(p, "Edge water is now " + (lvl.edgeWater ? "&aon" : "&4off") + Server.DefaultColor + ".");
                    }
                    else if (message.Split(' ').Length == 3)
                        switch (value.ToLower())
                        {
                            case "on":
                            case "true":
                            case "1":
                                lvl.edgeWater = true;
                                Player.SendMessage(p, "Edge water is now &aon" + Server.DefaultColor + ".");
                                break;
                            case "off":
                            case "false":
                            case "0":
                                lvl.edgeWater = false;
                                Player.SendMessage(p, "Edge water is now &4off" + Server.DefaultColor + ".");
                                break;
                            default:
                                Player.SendMessage(p, "Cannot parse value.");
                                Player.SendMessage(p, "&4" + new string('-', 60));
                                Help(p);
                                break;
                        }
                    else
                    {
                        Player.SendMessage(p, "Too many parameters!");
                        Player.SendMessage(p, "&4" + new string('-', 60));
                        Help(p);
                    }
                    break;
                case "perbuild":
                    if (message.Split(' ').Length == 3)
                    {
                        Group grp = Group.Find(value);
                        if (grp == null)
                        {
                            Player.SendMessage(p, "Invalid value for build permission.");
                            Player.SendMessage(p, "&4" + new string('-', 60));
                            Help(p);
                        }

                        lvl.permissionbuild = grp.Permission;
                        Player.SendMessage(p, "Build permission has been changed to" + grp.color + " " + grp.name + Server.DefaultColor + " in " + lvl.name);
                    }
                    else
                        Player.SendMessage(p, "You need to specify a [setting]");
                    break;
                case "pervisit":
                    if (message.Split(' ').Length == 3)
                    {
                        Group grp = Group.Find(value);
                        if (grp == null)
                        {
                            Player.SendMessage(p, "Invalid value for visit permission.");
                            Player.SendMessage(p, "&4" + new string('-', 60));
                            Help(p);
                        }

                        lvl.permissionvisit = grp.Permission;
                        Player.SendMessage(p, "Visit permission has been changed to" + grp.color + " " + grp.name + Server.DefaultColor + " in " + lvl.name);
                    }
                    else
                        Player.SendMessage(p, "You need to specify a [setting]");
                    break;
                //case "tetris":
                //    lvl.tetrisAllowed = !lvl.tetrisAllowed;
                //    Player.SendMessage(p, "You can " + (lvl.tetrisAllowed ? "now " : "no longer ") + "play tetris in " + lvl.name);
                //    break;
                default:
                    Player.SendMessage(p, "There is no attribute: " + message.Split(' ')[1]);
                    Player.SendMessage(p, "&4" + new string('-', 60));
                    Help(p);
                    return;
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/map [map] - Gets maps settings.");
            Player.SendMessage(p, "/map [map] <type> [setting] - Sets a map setting.");
            Player.SendMessage(p, "Types: &bMOTD" + Server.DefaultColor + ", &bedgewater" + Server.DefaultColor + ", perbuild and pervisit.");
            Player.SendMessage(p, "Settings in &baqua" + Server.DefaultColor + " does not need [setting].");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/map'",
                "Gets the settings of the current map.",
                "",
                "&3'/map main'",
                "Gets the settings of the map 'main'.",
                "",
                "&3'/map motd lol'",
                "Sets the MOTD of the current map, to 'lol'.",
                "",
                "&3'/map freebuild motd lol'",
                "Sets the MOTD of the map 'freebuild' to 'lol'",
            };
        }
    }
}