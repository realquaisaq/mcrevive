
using System;

namespace MCRevive
{
    public class CmdAntiGriefLimit : Command
    {
        public override string name { get { return "antigrieflimit"; } }
        public override string shortcut { get { return "agl"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Owner; } }
        public CmdAntiGriefLimit() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            try
            {
                if (message == "0")
                {
                    Server.AGS = false;
                    Player.GlobalMessage("Anti Grief System has been turned off.");
                    Server.s.Log(p.name + " has turned the Anti Greif System off.");
                    return;
                }
                else if (int.Parse(message) < 10)
                {
                    int messagecheck = int.Parse(message);
                    Player.spamBlockCount = messagecheck;
                    Player.SendMessage(p, "I cant believe you are this stupid...");
                    Player.SendMessage(p, "But well... its not me setting your limits :P");
                    if (messagecheck == Player.spamBlockCount)
                    {
                        Player.GlobalMessage("Anti Grief limit is now " + Player.spamBlockCount + ".");
                        Server.s.Log(p.name + " has set the Anti Grief limit to: " + Player.spamBlockCount + ".");
                    }

                }
                else if (int.Parse(message) >= 10)
                {
                    int messagecheck = int.Parse(message);
                    Player.spamBlockCount = messagecheck;
                    if (messagecheck == Player.spamBlockCount)
                    {
                        Player.GlobalMessage("Anti Grief limit is now " + Player.spamBlockCount + ".");
                        Server.s.Log(p.name + " has set the Anti Grief limit to: " + Player.spamBlockCount + ".");
                    }
                }
                else
                {
                    Player.SendMessage(p, "Antigrief limit has not been changed!");
                }
            }
            catch { Player.SendMessage(p, "Invalid input"); }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/antigrieflimit or /agl <number> - Sets the Anti Griefing System limit.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
