using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    public class CmdReview : Command
    {
        public override string name { get { return "review"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdReview() { }
        public override void Use(Player p, string message)
        {
            if (message != "" && p.group.Permission < LevelPermission.Operator) { Help(p); return; }
            else if (message != "") message = " " + message;
            if (message.Split(' ').Length == 2)
            {
                message = message.Remove(0, 1);
                Player who = Player.FindExact(message);
                string reviews = System.IO.File.ReadAllText("memory/reviews.txt");

                int i = 0;
                for (; i < Server.reviews.Count; i++)
                {
                    try
                    {
                        if (Server.reviews.ToArray()[i].Split('&')[0].ToLower() == message.ToLower())
                            break;
                    }
                    catch { break; }
                }
                if (reviews.Contains('&') && Server.reviews.ToArray()[i].Split('&')[0].ToLower() == message.ToLower())
                {
                    foreach (string review in reviews.Split('&'))
                    {
                        if (review.Split(' ')[0].ToLower() == message.ToLower())
                        {
                            Level level = Level.FindExact(review.Split(' ')[1]);
                            ushort x = ushort.Parse(review.Split(' ')[2].Split(',')[0]);
                            ushort y = ushort.Parse(review.Split(' ')[2].Split(',')[1]);
                            ushort z = ushort.Parse(review.Split(' ')[2].Split(',')[2]);

                            bool done = false;

                            if (level == null)
                            {
                                Command.all.Find("load").Use(p, review.Split(' ')[1]);
                                level = Level.FindExact(review.Split(' ')[1]);
                                if (level == null) return;
                            }

                            if (level.GetTile(x, y, z) == Block.air && (level.GetTile(x, (ushort)(y + 1), z) == Block.air || level.GetTile(x, (ushort)(y - 1), z) == Block.air))
                            {
                                if (p.level.name != level.name)
                                    Command.all.Find("goto").Use(p, level.name);
                                while (p.Loading) { }

                                Command.all.Find("move").Use(p, x + " " + y + " " + z);
                                done = true;
                            }
                            else
                            {
                                for (byte xx = 0; xx < 16; xx++)
                                {
                                    if (done) break;
                                    for (byte yy = 0; yy < 4; yy++)
                                    {
                                        for (byte zz = 0; zz < 16; zz++)
                                        {
                                            if (level.GetTile((ushort)(xx + x - 8), (ushort)(yy + y), (ushort)(zz + z - 8)) == Block.air)
                                            {
                                                if (level.GetTile((ushort)(xx + x - 8), (ushort)(yy + y + 1), (ushort)(zz + z - 8)) == Block.air ||
                                                    level.GetTile((ushort)(xx + x - 8), (ushort)(yy + y - 1), (ushort)(zz + z - 8)) == Block.air)
                                                {
                                                    if (p.level.name != level.name)
                                                        Command.all.Find("goto").Use(p, level.name);
                                                    while (p.Loading) { }

                                                    Command.all.Find("move").Use(p, (xx + x - 8) + " " + (yy + y) + " " + (z + zz - 8));
                                                    done = true;
                                                }
                                            }
                                            if (done)
                                                break;
                                        }
                                        if (done)
                                            break;
                                    }
                                }
                            }
                            if (!done)
                            {
                                Player.SendMessage(p, "There was not found any positions where you could stand.");
                                Player.SendMessage(p, "Its possible this was a scam.");
                            }
                        }
                    }
                }
                else
                {
                    Player.SendMessage(p, "That player has requested no reviews");
                    return;
                }

                System.IO.File.WriteAllText("memory/reviews.txt", reviews.Remove(reviews.IndexOf(message.ToLower()),
                    reviews.Substring(reviews.IndexOf(message.ToLower())).IndexOf('&') + 1));

                reviews = System.IO.File.ReadAllText("memory/reviews.txt");

                for (int ii = 0; ii < Server.reviews.Count; ii++)
                {
                    if (DateTime.Parse(Server.reviews[ii].Split('&')[1]).AddDays(2) < DateTime.Now)
                    {
                        System.IO.File.WriteAllText("memory/reviews.txt", reviews.Remove(reviews.IndexOf(Server.reviews[ii].Split('&')[0].ToLower()),
                            reviews.Substring(reviews.IndexOf(Server.reviews[ii].Split('&')[0].ToLower())).IndexOf('&') + 1));
                        Server.reviews.RemoveAt(ii);
                    }
                    if (Server.reviews[ii].Split('&')[0].ToLower() == message.ToLower()) Server.reviews.RemoveAt(ii);
                }
                try { who.needsReview = false; }
                catch { }

                if (Server.reviews.Count > 0)
                {
                    Player.SendMessage(p, Group.findPlayerGroup(Server.reviews[0].Split('&')[0]).color + Server.reviews[0].Split('&')[0]
                                    + Server.DefaultColor + " wants an op to review their creation.");
                    Player.SendMessage(p, "Use \"/review " + Server.reviews[0].Split('&')[0] + "\" to go there.");
                }
            }
            else
            {
                if (!p.needsReview)
                {
                    int i = 0;
                    foreach (Player all in Player.players)
                        if (all.group.Permission >= LevelPermission.Operator && !Server.afkset.Contains(all.name)) i++;
                    p.needsReview = true;
                    if (i > 0)
                    {
                        Player.SendMessage(p, "Your request has been sent to operators, They should be on their way.");
                        Player.GlobalMessageOps(p.color + p.name + Server.DefaultColor + " requests a review of their creation.");
                    }
                    else
                    {
                        Player.SendMessage(p, "Unforginently there is no operators online right now.");
                        Player.SendMessage(p, "Forginently your name has been added to a list of things");
                        Player.SendMessage(p, "that should be reviewed, and that is why we need you to select");
                        Player.SendMessage(p, "a block right beside your creation, so we know which one it is");
                        Player.SendMessage(p, "&aPlace a block to show what creation is yours.");
                        p.ClearBlockchange();
                        p.Blockchange += new Player.BlockchangeEventHandler(Blockchange);
                    }
                }
                else
                    Player.SendMessage(p, "You already have 1 request waiting, wait for that one to be answered!");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/review - Sends a request to Op's to come and view your building");
            try
            {
                if (p == null || p.group.Permission >= LevelPermission.Operator)
                {
                    Player.SendMessage(p, "/review <fullname> - teleports you to the creation that <fullname> has made");
                }
            }
            catch { }
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                ""
            };
        }
        public void Blockchange(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            if (b == Block.Zero) { Player.SendMessage(p, "Invalid Block(" + x + "," + y + "," + z + ")!"); return; }
            p.SendBlockchange(x, y, z, b);

            System.IO.File.AppendAllText("memory/reviews.txt", p.name.ToLower() + " " + p.level.name.ToLower() + " " + x + "," + y + "," + z + " "
                + DateTime.Now.ToString().Replace(' ', '@') + "&");
            Server.reviews.Add(p.name + "&" + DateTime.Now);

            Player.SendMessage(p, "Thank you");
        }
    }
}
