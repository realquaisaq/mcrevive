using System;
using System.IO;
using System.Data;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdRenameLvl : Command
    {
        public override string name { get { return "renamelvl"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdRenameLvl() { }
        public override void Use(Player p, string message)
        {
            if (message == "" || message.IndexOf(' ') == -1) { Help(p); return; }
            Level foundLevel = Level.Find(message.Split(' ')[0]);
            string newName = message.Split(' ')[1];
            string lvlName = foundLevel == null ? "" : foundLevel.name;
            if (File.Exists("levels/" + newName + ".lvl") || File.Exists("levels/" + newName + ".mcqlvl")) { Player.SendMessage(p, "Level already exists."); return; }
            if (foundLevel == Server.mainLevel) { Player.SendMessage(p, "Cannot rename the main level."); return; }
            if (foundLevel != null) foundLevel.Unload();
            else
                lvlName = message.Split(' ')[0];

            try
            {
                if (File.Exists("levels/" + lvlName + ".mcqlvl"))
                    File.Move("levels/" + lvlName + ".mcqlvl", "levels/" + newName + ".mcqlvl");
                else
                    File.Move("levels/" + lvlName + ".lvl", "levels/" + newName + ".lvl");
                
                try { File.Move("levels/properties/" + lvlName + ".properties", "levels/properties/" + newName + ".properties"); }
                catch { }
                try { File.Move("levels/properties/" + lvlName, "levels/properties/" + newName + ".properties"); }
                catch { }
                try { Directory.Move("memory/portals/" + lvlName, "memory/portals/" + newName); }
                catch { }
                try { File.Move("memory/zones/" + lvlName + ".zone", "memory/zones/" + newName + ".zone"); }
                catch { }

                Player.GlobalMessage("Renamed " + lvlName + " to " + newName);
            }
            catch (Exception e) { Player.SendMessage(p, "Error when renaming."); Server.ErrorLog(e); }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/renamelvl <level> <new name> - Renames <level> to <new name>");
            Player.SendMessage(p, "Portals going to <level> will not be lost");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}