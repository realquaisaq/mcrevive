using System;

namespace MCRevive
{
    public class CmdCrashserver : Command
    {
        public override string name { get { return "crashserver"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Banned; } }
        public CmdCrashserver() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            p.Kick("Server shutdown.");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/crashserver - crashes the server.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
