/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Text.RegularExpressions;

namespace MCRevive
{
    public class CmdBanip : Command
    {
        Regex regex = new Regex(@"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$");
        public override string name { get { return "banip"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdBanip() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            if (Player.Find(message) != null) message = Player.Find(message).ip;

            if (message == "127.0.0.1" || message.StartsWith("192.168.") || !regex.IsMatch(message)) { Player.SendMessage(p, "Not a valid ip!"); return; }
            if (p != null && p.ip == message) { Player.SendMessage(p, "You can't ip-ban yourself.!"); return; }
            if (Server.bannedIP.Contains(message)) { Player.SendMessage(p, message + " is already ip-banned."); return; }

            Server.bannedIP.Add(message);
            Server.bannedIP.Save("banned-ip.txt", false);
            Player.GlobalMessage(message + " got &8ip-banned" + Server.DefaultColor + "!");
            IRCBot.Say("IP-BANNED: " + message.ToLower() + " by " + (p == null ? " Console[" + Server.consolename + "]" : p.name));
            Server.s.Log("IP-BANNED: " + message.ToLower());
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/banip <ip/name> - Bans an ip, can also use the name of an online player.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/banip 123.45.67.89'",
                "This will ban the user with the IP '123.45.67.89'.",
                "&a" + new string('-', 60),
                "&3'/banip quaispet",
                "If Quaispet is online, you can ipban them using their name."
            };
        }
    }
}