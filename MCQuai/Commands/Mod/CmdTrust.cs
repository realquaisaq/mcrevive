using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class CmdTrust : Command
    {
        public override string name { get { return "trust"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdTrust() { }
        public override void Use(Player p, string message)
        {
            if (message.Split(' ').Length > 1) { Help(p); return; }
            Player who = Player.Find(message);

            if (who != null)
            {
                if (who.ignoreGrief)
                {
                    who.ignoreGrief = false;
                    Player.GlobalMessage("Anti Grief System will work on " + who.name + " again now.");
                }
                else
                {
                    who.ignoreGrief = true;
                    Player.GlobalMessage("Anti Grief System will no longer affect " + who.name + ".");
                }
            }
            else Player.SendMessage(p, "Unable to find player: " + message);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/Trust <player> - makes <player> immune to the Anti Grief System.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
