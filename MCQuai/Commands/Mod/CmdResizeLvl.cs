using System;
using System.IO;

namespace MCRevive
{
    public class CmdResizeLvl : Command
    {
        public override string name { get { return "resizelvl"; } }
        public override string shortcut { get { return "rlvl"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdResizeLvl() { }
        public override void Use(Player p, string message)
        {
            if (p == null) { Server.s.Log("The console cannot use this command!"); return; }
            if (!Directory.Exists("levels/backup/resized_levels/")) { Directory.CreateDirectory("levels/backup/resized_levels/"); }
            string[] split = message.Split(' ');
            if (split.Length != 4) { Help(p); return; }
            string levelName = split[0];
            Level level = Level.Find(levelName);
            if (level == null) { Player.SendMessage(p, "There is no level \"" + levelName + "\"!"); return; }
            if (level == Server.mainLevel) { Player.SendMessage(p, "You cannot resize the main level. Sorry for the inconvenience."); return; }
            string pLevel = p.level.name;
            int iOldX = level.width;
            int iOldY = level.height;
            int iOldZ = level.depth;
            try
            {
                p.ignorePermission = true;
                bool goBack = false;
                if (p.level == level) goBack = true;
                Command Goto = Command.all.Find("goto");
                Command click = Command.all.Find("click");
                int iNewX = Convert.ToInt32(split[1]);
                int iNewY = Convert.ToInt32(split[2]);
                int iNewZ = Convert.ToInt32(split[3]);
                if (iOldX - iNewX > 128 || iOldY - iNewY > 100 || iOldZ - iNewZ > 128) { Player.SendMessage(p, "New size is too much smaller than old size. Sorry about that."); return; }
                string tempLevelName = levelName + "_temp";
                Command.all.Find("newlvl").Use(p, tempLevelName + " " + iNewX + " " + iNewY + " " + iNewZ + " pixel");
                Command load = Command.all.Find("load");
                load.Use(null, tempLevelName);
                System.Threading.Thread.Sleep(2000);
                Level tempLevel = Level.Find(tempLevelName);
                tempLevel.unload = false;
                if (p.level != level) { Goto.Use(p, level.name); while (p.Loading) { } }
                string copyCommand = "air";
                Command.all.Find("copy").Use(p, copyCommand);
                int pMaxBlocks = p.group.maxBlocks;
                click.Use(p, "0 0 0");
                click.Use(p, (iOldX - 1) + " " + (iOldY - 1) + " " + (iOldZ - 1));
                Goto.Use(p, tempLevel.name);
                while (p.Loading) { }
                Command.all.Find("instboid").Use(p, "air walls");
                click.Use(p, "0 0 0");
                click.Use(p, (iNewX - 1) + " " + (iNewY - 1) + " " + (iNewZ - 1));
                Command.all.Find("instboid").Use(p, "air");
                click.Use(p, "1 0 1");
                click.Use(p, (iNewY - 2) + " " + (iNewY - 2) + " " + (iNewZ - 2));
                Command.all.Find("instpaste").Use(p, "");
                click.Use(p, "0 0 0");
                if (p.level.name != pLevel) { Goto.Use(p, pLevel); while (p.Loading) { } }
                string file1 = (File.Exists("levels/" + level.name + ".mcqlvl")) ? "levels/" + level.name + ".mcqlvl" : "levels/" + level.name + ".lvl";
                string file2 = "levels/backup/resized_" + file1;
                File.Copy(file1, file2, true);
                Command.all.Find("deletelvl").Use(p, level.name);
                System.Threading.Thread.Sleep(2000);
                Command.all.Find("renamelvl").Use(p, tempLevel.name + " " + levelName);
                Player.SendMessage(p, "The map \"" + levelName + "\" was resized. Backup of previous size was stored as '" + file2 + "'");
                if (goBack) Command.all.Find("goto").Use(p, levelName);
                p.ignorePermission = false;
            }
            catch (Exception ex) { Player.SendMessage(p, "Something went wrong. Are you sure you used valid dimensions?"); Server.ErrorLog(ex); }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/resizelvl or /rlvl - <level> <x> <y> <z> - Resizes <level> to the new dimensions.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}