using System;

namespace MCRevive
{
    public class CmdInvincible : Command
    {
        public override string name { get { return "invincible"; } }
        public override string shortcut { get { return "immortal"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdInvincible() { }
        public override void Use(Player p, string message)
        {
            if (message != "" && message != "#") { Help(p); return; }
            p.invincible = !p.invincible;
            if (p.invincible)
                Player.GlobalMessageOps((message == "#") ? "" : p.name + " is now invincible.");
            else
                Player.GlobalMessageOps((message == "#") ? "" : p.name + " is no longer invincible");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/invincible [\"#\"] - makes you invincible so you can not die (# for stealth).");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
