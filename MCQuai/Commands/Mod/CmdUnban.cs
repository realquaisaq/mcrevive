/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;

namespace MCRevive
{
    public class CmdUnban : Command
    {
        public override string name { get { return "unban"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdUnban() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            bool totalUnban = false;
            if (message[0] == '@')
            {
                totalUnban = true;
                message = message.Substring(1).Trim();
            }

            Player who = Player.Find(message);
            if (who == null)
            {
                if (Group.Find(LevelPermission.Banned + "").playerList.Contains(message))
                {
                    Player.SendMessage(p, message + " is not banned...");
                    return;
                }
                Player.GlobalMessage(message + " &8(banned)" + Server.DefaultColor + " is now " + Group.standard.color + Group.standard.name + Server.DefaultColor + "!");
                Group.Find(LevelPermission.Banned.ToString()).playerList.Remove(message);
            }
            else
            {
                if (who.group == Group.Find(LevelPermission.Banned.ToString()))
                {
                    Player.GlobalChat(who, who.color + who.prefix + who.name + Server.DefaultColor + " is now " + Group.standard.color + Group.standard.name + Server.DefaultColor + "!", false);
                    who.group = Group.standard; who.color = who.group.color; Player.GlobalDie(who, false);
                    Player.GlobalSpawn(who, who.pos[0], who.pos[1], who.pos[2], who.rot[0], who.rot[1], false);
                    Group.Find(LevelPermission.Banned.ToString()).playerList.Remove(who.name);
                }
                else
                {
                    Player.SendMessage(p, who.name + " is not banned!");
                    return;
                }
            }

            Group.Find(LevelPermission.Banned.ToString()).playerList.Save();
            if (totalUnban)
                Command.all.Find("unbanip").Use(p, message);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/unban <player> - Unbans a player.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}