using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdPatrol : Command
    {
        public override string name { get { return "patrol"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdPatrol() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            if (p == null) { Player.SendMessage(p, "In-game players only!"); return; }
            List<int> ids = new List<int>();
            int i = 0;
            foreach (Player pp in Player.players)
            {
                if (pp.group.Permission == LevelPermission.Guest) ids.Add(i);
                i++;
            }
            if (ids.Count == 0) { p.SendMessage("There are no guests online atm. Boring huh?"); return; }
            Command.all.Find("tp").Use(p, Player.players[RandomNumber(0, ids.Count-1)].name);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/patrol - Tp to a random guest. You never know what their doing.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
    }
}