﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    class CmdUpdate : Command
    {
        public override string name { get { return "update"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Owner; } }
        public CmdUpdate() { }
        public override void Use(Player p, string message)
        {
            bool isR = false;
            int newVer = 0;
            if (message == "") message = "check";
            try
            {
                newVer = int.Parse(Server.DownloadString("http://www.mcrevive.tk/download/_Update.php"));
            }
            catch
            {
                Player.SendMessage(p, "Could not find the newest version.");
                Player.SendMessage(p, "Aborting update.");
            }
            if (message.ToLower()[0] == 'r') message = message.Substring(1);
            try
            {
                if (message.ToLower() != "newest")
                    int.Parse(message);
                isR = true;
            }
            catch { }

            if (isR)
            {
                isR = false;
                if (message == Server.Version)
                {
                    Player.SendMessage(p, "The server is already running Revision " + message + ".");
                    return;
                }
                else if (message.ToLower() == "newest" && newVer <= float.Parse(Server.Version))
                {
                    Player.SendMessage(p, "You are already running the newest version of MCRevive.");
                    return;
                }
                string[] revisions = Server.DownloadString("http://www.mcrevive.tk/download/_Revlist.php").Split(':');
                string Revs = "";
                foreach (string rev in revisions)
                {
                    Revs += ", " + rev;
                    if (message == rev)
                        isR = true;
                }
                if (!isR)
                {
                    Player.SendMessage(p, message + " is not an available revision.");
                    Player.SendMessage(p, "Available revisions are: ");
                    Player.SendMessage(p, Revs.Substring(2));
                    return;
                }

                try
                {
                    WebClient Client = new WebClient();
                    if (!Server.usingCLI)
                        Client.DownloadFile("http://www.mcrevive.tk/download/MCRevive_GUI-" + message + ".exe", "memory/MCRevive.exe");
                    else
                        Client.DownloadFile("http://www.mcrevive.tk/download/MCRevive_CLI-" + message + ".exe", "memory/MCRevive.exe");
                    Client.DownloadFile("http://www.mcrevive.tk/download/MCRevive-" + message + ".dll", "memory/MCRevive_.dll");
                }
                catch (Exception ex)
                {
                    Player.SendMessage(p, "Could not download updates!");
                    Player.SendMessage(p, "Update Aborted!");
                    Server.s.Log("Failed to download update!");
                    Server.ErrorLog(ex);
                    return;
                }
                int i = Server.updateTime;
                string s = "second" + ((i != 1) ? "s" : "");
                if (i > 60) { i /= 60; s = "minute" + ((i != 1) ? "s" : ""); }
                if (i > 60) { i /= 60; s = "hour" + ((i != 1) ? "s" : ""); }
                System.IO.File.WriteAllText("noupdate.txt", "");
                Player.GlobalMessage("Update found! Updating in " + i + " " + s + ".");
                System.Threading.Thread.Sleep(i * 1000);

                try { Server.Restart(); }
                catch { Server.s.Log("Copy the files in the folder 'memory'"); }
            }
            else if (message.ToLower() == "check")
            {
                Player.SendMessage(p, "This Server's Version: " + (float.Parse(Server.Version) == newVer ? "&a" : float.Parse(Server.Version) > newVer ? "&3" : "&c") + Server.Version);
                Player.SendMessage(p, "Newest Server Version: &a" + newVer);
            }
            else
                Help(p);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/update ['check'] - Checks for updates.");
            Player.SendMessage(p, "/update [revision] - Installs MCRevive revision [revision].");
            Player.SendMessage(p, "'newest' is a valid revision.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/update check' or '/update':",
                "Checks if the current version of mcrevive is up to date.",
                "&3'/update 1.90':",
                "Will install MCRevive R1.90 to the server.",
                "&3'/update newest'",
                "Will install the newest MCRevive revision to the server."
            };
        }
    }
}
