
using System;

namespace MCRevive
{
    public class CmdKick : Command
    {
        public override string name { get { return "kick"; } }
        public override string shortcut { get { return "k"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdKick() { }

        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            Player who = Player.Find(message.Split(' ')[0]);
            if (who == null) { Player.SendMessage(p, "Could not find player: " + message.Split(' ')[0] + "."); return; }
            if (message.Split(' ').Length > 1)
                message = message.Substring(message.IndexOf(' ') + 1);
            else if (p == null)
                message = "You were kicked by " + Server.consolename + "!";
            else
                message = "You were kicked by " + p.name + "!";

            if (p != null)
                if (who == p && !p.ignorePermission)
                {
                    Player.SendMessage(p, "You cannot kick yourself!");
                    return;
                }
                else if (who.group.Permission >= p.group.Permission && !p.ignorePermission)
                {
                    Player.GlobalChat(p, p.color + p.name + Server.DefaultColor + " tried to kick " +
                        who.color + who.name + Server.DefaultColor + ", but failed.", false);
                    return;
                }

            who.Kick(message);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/kick <player> [message] - Kicks a player with an optional message.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}