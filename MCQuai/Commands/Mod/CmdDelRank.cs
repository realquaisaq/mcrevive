﻿using System;

namespace MCRevive
{
    class CmdDelRank : Command
    {
        public override string name { get { return "delrank"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Owner; } }
        public CmdDelRank() { }
        public override void Use(Player p, string message)
        {
            if (message.Split(' ').Length != 2) { Help(p); return; }
            string action = message.Split(' ')[1].ToLower();
            Group group = Group.Find(message.Split(' ')[0]);
            Group group2 = Group.Find(action);
            if (group == null) { Player.SendMessage(p, "The rank &c" + message.Split(' ')[0] + Server.DefaultColor + " could not be found."); return; }
            else if (group.name == Group.standard.name) { Player.SendMessage(p, "You can't remove the " + group.name + " rank!"); return; }
            else if (action != "up" && action != "down" && group2 == null) { Player.SendMessage(p, "Could not find rank &c" + action + Server.DefaultColor + "."); return; }
            else if (action == "up" && Group.GroupList.IndexOf(group) >= Group.GroupList.Count - 2) { Player.SendMessage(p, "You can't send people with this rank, up."); return; }
            else if (action == "down" && Group.GroupList.IndexOf(group) - 1 < 1) { Player.SendMessage(p, "You can't send people with this rank, down."); return; }

            Player.SendMessage(p, "Moving people...");
            string[] all = Group.GroupList.Find(g => g.name == group.name).playerList.All().ToArray();
            foreach (string who in all)
            {
                if (action == "up")
                    Group.GroupList[Group.GroupList.IndexOf(group) + 1].playerList.Add(who);
                else if (action == "down")
                    Group.GroupList[Group.GroupList.IndexOf(group) - 1].playerList.Add(who);
                else if (action == group2.name)
                    Group.GroupList[Group.GroupList.IndexOf(group2)].playerList.Add(who);
            }
            Player.SendMessage(p, "Deleting rank...");
            Group.GroupList.Remove(group);
            Group.saveGroups(Group.GroupList);
            Group.InitAll();
            Command.InitAll();
            GrpCommands.fillRanks();
            Block.SetBlocks();
            Player.SendMessage(p, "The rank " + group.name + " has been removed.");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/delrank <name> <'up'/'down'/new rank> - deletes a rank.");
            Player.SendMessage(p, "Usage: Up means that everyone that has that rank goes 1 rank up.");
            Player.SendMessage(p, "Same with down. new rank is used to determine the new rank that");
            Player.SendMessage(p, "people with the deleted rank should have instead.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/delrank engineer up'",
                "This will delete the rank 'engineer'",
                "Everyone that had that rank will go 1 rank up.",
                "",
                "&3'/delrank engineer down'",
                "This will delete the rank 'engineer'",
                "Everyone that had that rank will go 1 rank down.",
                "",
                "&3'/delrank engineer builder'",
                "This will delete the rank 'engineer'",
                "Everyone that had that rank will become builder."
            };
        }
    }
}
