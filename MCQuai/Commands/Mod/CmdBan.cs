using System;

namespace MCRevive
{
    public class CmdBan : Command
    {
        public override string name { get { return "ban"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdBan() { }
        public override void Use(Player p, string message)
        {
            try
            {
                if (message == "") { Help(p); return; }

                bool stealth = false;
                bool totalBan = false;

                if (message[0] == '#')
                {
                    message = message.Substring(1).Trim();
                    stealth = true;
                    Server.s.Log("Stealth Ban Attempted");
                }
                else if (message[0] == '@')
                {
                    totalBan = true;
                    message = message.Substring(1).Trim();
                }

                Player who = Player.Find(message);

                if (who == null)
                {
                    if (!Player.ValidName(message))
                    {
                        Player.SendMessage(p, "Invalid name \"" + message + "\".");
                        return;
                    }

                    Group foundGroup = Group.findPlayerGroup(message);

                    if (Player.checkDevS(message))
                    {
                        Player.SendMessage(p, "You can't ban a MCRevive Developer!");
                        Player.GlobalMessage(((p == null) ? "Console[&a" + Server.consolename + Server.DefaultColor + "]" : (p.color + p.name + Server.DefaultColor)) + " attempted to ban a MCRevive Developer!");
                        return;
                    }
                    else if (foundGroup.Permission >= LevelPermission.Operator && !p.ignorePermission)
                    {
                        Player.SendMessage(p, "You can't ban a" + ((Server.vocal(foundGroup.name[0])) ? "n " : " ") + foundGroup.name + "!");
                        return;
                    }
                    else if (foundGroup.Permission == LevelPermission.Banned)
                    {
                        Player.SendMessage(p, message + " is already banned.");
                        return;
                    }

                    foundGroup.playerList.Remove(message);
                    foundGroup.playerList.Save();

                    Player.GlobalMessage(message + " &f(offline)" + Server.DefaultColor + " is now &8banned" + Server.DefaultColor + "!");
                    Group.Find(LevelPermission.Banned.ToString()).playerList.Add(message);
                }
                else
                {
                    if (!Player.ValidName(who.name))
                    {
                        Player.SendMessage(p, "Invalid name \"" + who.name + "\".");
                        return;
                    }

                    if (Player.checkDevS(who.name))
                    {
                        Player.SendMessage(p, "You can't ban a MCRevive Developer!");
                        Player.GlobalMessage(p.color + p.name + Server.DefaultColor + " attempted to ban a MCRevive Developer!");
                        return;
                    }
                    if (who.group.Permission >= LevelPermission.Operator && !p.ignorePermission)
                    {
                        Player.SendMessage(p, "You can't ban a" + ((Server.vocal(who.group.name[0])) ? "n " : " ") + who.group.name + "!");
                        return;
                    }

                    if (who.group.Permission == LevelPermission.Banned)
                    {
                        Player.SendMessage(p, message + " is already banned.");
                        return;
                    }

                    who.group.playerList.Remove(message);
                    who.group.playerList.Save();

                    if (stealth) Player.GlobalMessageOps(who.color + who.name + Server.DefaultColor +
                        " is now STEALTH &8banned" + Server.DefaultColor + "!");
                    else Player.GlobalChat(who, who.color + who.name + Server.DefaultColor +
                        " is now &8banned" + Server.DefaultColor + "!", false);

                    who.group = Group.Find(LevelPermission.Banned.ToString());
                    who.color = who.group.color;
                    Player.GlobalDie(who, false);
                    Player.GlobalSpawn(who, who.pos[0], who.pos[1], who.pos[2], who.rot[0], who.rot[1], false);
                    Group.Find(LevelPermission.Banned.ToString()).playerList.Add(who.name);
                }
                Group.Find(LevelPermission.Banned.ToString()).playerList.Save();

                if (!stealth) IRCBot.Say(who.name + " was banned.");
                Server.s.Log((stealth ? "STEALTH " : "") + "BANNED: " + who.name);

                if (totalBan == true)
                {
                    Command.all.Find("undo").Use(p, message + " 0");
                    Command.all.Find("banip").Use(p, "@" + message);
                }
            }
            catch (Exception e) { Server.ErrorLog(e); }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/ban <player> - Bans a player without kicking him.");
            Player.SendMessage(p, "Add # before name to stealth ban.");
            Player.SendMessage(p, "Add @ before name to total ban.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/ban soccer101nic'",
                "This will ban soccer101nic from the server.",
                "&a" + new string('-', 60),
                "&3'/ban #soccer101nic'",
                "This will ban soccer101nic, without anyone but op+ can see it.",
                "&a" + new string('-', 60),
                "&3'/ban @soccer101nic'",
                "This will ban soccer101nic, where after they are ipbanned,",
                "after this, all their actions will be undone."
            };
        }
    }
}