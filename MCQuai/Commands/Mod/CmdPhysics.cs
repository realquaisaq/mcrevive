/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;

namespace MCRevive
{
    public class CmdPhysics : Command
    {
        public override string name { get { return "physics"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdPhysics() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { if (p != null) { Help(p); } return; }
            if (message.Split(' ').Length == 1) message += " ";
            string[] param = message.Split(' ');
            if (param.Length == 2)
            {
                Level foundLevel = p.level;
                if (param[1] != "")
                {
                    if (Level.Find(param[0]) != null) foundLevel = Level.Find(param[0]);
                    else
                    {
                        if (File.Exists("levels/" + param[0] + ".lvl") || File.Exists("levels/" + param[0] + ".mcqlvl"))
                        {
                            Command.all.Find("load").Use(p, param[0]);
                            Command.all.Find("physics").Use(p, param[0] + " " + param[1]);
                        }
                        else
                        {
                            Player.SendMessage(p, "Could not find level: " + param[0]);
                            return;
                        }
                    }
                }
                if (param[1] == "") { param[1] = param[0]; param[0] = p.level.name; }
                int temp = -1;
                try
                {
                    temp = int.Parse(param[1]);
                    if (temp >= 0 && temp <= 2)
                    {
                        foundLevel.physics = temp;
                        if (temp == 0) { foundLevel.ClearPhysics(); }
                        Player.GlobalMessageLevel(foundLevel, "Physics is now " +
                            ((temp == 0) ? "&cOFF" : (temp == 1) ? "&aNormal" : "&aAdvanced") +
                            Server.DefaultColor + " on &b" + foundLevel.name + Server.DefaultColor + ".");
                        Server.s.Log("Physics is now " + ((temp == 0) ? "OFF" : (temp == 1) ? "Normal" : "Advanced") + " on " + foundLevel.name + ".");
                        IRCBot.Say("Physics is now " + ((temp == 0) ? "OFF" : (temp == 1) ? "Normal" : "Advanced") + " on " + foundLevel.name + ".");
                    }
                }
                catch { Player.SendMessage(p, "Unknown physics option: " + param[1]); }
            }
        }

        public override void Help(Player p)
        {
            Player.SendMessage(p, "/physics [level] <0/1/2> - Set the levels physics, 0-Off 1-On 2-Advanced");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}