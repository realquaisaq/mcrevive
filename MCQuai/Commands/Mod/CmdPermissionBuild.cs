/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;

namespace MCRevive
{
    public class CmdPermissionBuild : Command
    {
        public override string name { get { return "perbuild"; } }
        public override string shortcut { get { return "pb"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdPermissionBuild() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            if (p.level.permissionbuild > p.group.Permission && !p.ignorePermission) { Player.SendMessage(p, "This maps build permission is too high for you to change."); return; }
            int t = message.Split(' ').Length;
            if (t != 1 && t != 2) { Help(p); return; }
            if (t == 1)
            {
                int Perm = LevelPermission.Null;
                try { Perm = int.Parse(message); }
                catch { Perm = LevelPermission.FromName(message); }
                if (Perm == LevelPermission.Null) { Player.SendMessage(p, "Not a valid rank"); return; }

                p.level.permissionbuild = Perm;
                string grp = LevelPermission.ToName(Perm);
                Server.s.Log(p.level.name + " build permission changed to " + Color(grp) + grp + ".");
                Player.GlobalMessageLevel(p.level, "build permission changed to " + Color(grp) + grp + ".");
            }
            else
            {
                int Perm = LevelPermission.Null;
                try { Perm = int.Parse(message); }
                catch { Perm = LevelPermission.FromName(message); }
                if (Perm == LevelPermission.Null) { Player.SendMessage(p, "Not a valid rank"); return; }

                Level l = Level.Find(message.Split(' ')[0]);
                if (l == null) { Player.SendMessage(p, "There is no map loaded named: " + message.Split(' ')[0]); }

                l.permissionbuild = Perm;
                string grp = LevelPermission.ToName(Perm);
                Server.s.Log("Build permission changed to " + grp + " on " + l.name + ".");
                Player.GlobalMessageLevel(l, "build permission changed to " + Color(grp) + grp + ".");
                if (p != null && p.level != l) { Player.SendMessage(p, "build permission changed to " + Color(grp) + grp + " on " + l.name + "."); }
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/Perbuild [level] <rank> - Sets build permission for a map.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/perbuild guest'",
                "You now have to be guest to build on the map you are in.",
                "",
                "&3'/perbuild main guest'",
                "This will set the level 'main's building permission to guest."
            };
        }
        string Color(string grp)
        {
            return Group.Find(grp).color;
        }
    }
}