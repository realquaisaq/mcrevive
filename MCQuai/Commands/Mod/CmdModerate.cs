using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCRevive
{
    public class CmdModerate : Command
    {
        public override string name { get { return "moderate"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdModerate() { }

        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }

            if (Server.chatmod)
            {
                Server.chatmod = false;
                Player.GlobalMessage("Chat moderation has been disabled, you may talk!");
            }
            else
            {
                Server.chatmod = true;
                Player.GlobalMessage("Chat moderation enabled, you may NOT talk!");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/moderate - Enables players to turn enable/disable the chat.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}