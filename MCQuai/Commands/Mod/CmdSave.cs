/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdSave : Command
    {
        public override string name { get { return "save"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdSave() { }
        public override void Use(Player p, string message)
        {
            if (p != null)
            {
                if (message == "")
                {
                    p.level.Save();
                    p.level.saveChanges();
                    Player.SendMessage(p, "Level \"" + p.level.name + "\" saved.");
                }
                else if (message == "all")
                {
                    foreach (Level l in Server.levels)
                    {
                        l.Save();
                        l.saveChanges();
                    }
                    Player.SendMessage(p, "All levels saved");
                }
                else if (Level.Find(message) != null)
                {
                    Level l = Level.Find(message);
                    l.Save();
                    l.saveChanges();
                    Player.SendMessage(p, "Level \"" + l.name + "\" saved.");
                }
                else { Help(p); return; }
            }
            else
            {
                foreach (Level l in Server.levels)
                {
                    l.Save();
                    l.saveChanges();
                }
            }
            if (Player.players.Count > 0)
            {
                foreach (Player all in Player.players)
                {
                    all.saveSettings();
                }
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/save [all/level] - Saves the level, and all player settings.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}