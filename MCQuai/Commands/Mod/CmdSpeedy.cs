
namespace MCRevive
{
    public class CmdSpeedy : Command
    {
        public override string name { get { return "speedy"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Builder; } }
        public CmdSpeedy() { }
        public override void Use(Player p, string message)
        {
            //////////////////// Not working yet. Ill get this working in a future update - Jesbus
            if (p.level.antispeedhack) { p.SendMessage("You can't use speedy on an anti-speedhack map."); return; }
            if (p.speedy)
            {
                p.speedy = false;
                p.SendMessage("Speedy is now switched off.");
            }
            else
            {
                p.speedy = true;
                p.SendMessage("Speedy is now switched on.");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/speedy - Toggle your speed up");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}