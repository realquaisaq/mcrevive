using System;

namespace MCRevive
{
    public class CmdTColor : Command
    {
        public override string name { get { return "tcolor"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Guest; } }
        public CmdTColor() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            string[] args = message.Split(' ');
            Player who = Player.Find(args[0]);
            if (who == null)
            {
                Player.SendMessage(p, "Could not find player.");
                return;
            }
            if (args.Length == 1)
            {
                who.titlecolor = "";
                Player.GlobalChat(who, Name(who.color + who.name) + " title color has been removed.", false);
                who.SetPrefix();
                return;
            }
            else
            {
                string color = c.Parse(args[1]);
                if (color == "")
                    Player.SendMessage(p, "There is no color \"" + args[1] + "\".");
                else if (color == who.titlecolor)
                    Player.SendMessage(p, who.name + " already has the title color \"" + color + c.Name(color) + "\".");
                else
                {
                    Player.GlobalChat(who, Name(who.color + who.name) + " title color has been changed to " + color + c.Name(color) + Server.DefaultColor + ".", false);
                    who.titlecolor = color;
                    who.SetPrefix();
                }
            }
        }

        public override void Help(Player p)
        {
            Player.SendMessage(p, "/tcolor <player> [color] - Gives <player> the title color of [color].");
            Player.SendMessage(p, "If no [color] is specified, title color is removed.");
            Player.SendMessage(p, "&0black &1navy &2green &3teal &4maroon &5purple &6gold &7silver");
            Player.SendMessage(p, "&8gray &9blue &alime &baqua &cred &dpink &eyellow &fwhite");
            Player.SendMessage(p, "&rrainbow");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
        static string Name(string name)
        {
            string ch = name[name.Length - 1].ToString().ToLower();
            if (ch == "s" || ch == "x") { return name + Server.DefaultColor + "'"; }
            else { return name + Server.DefaultColor + "'s"; }
        }
    }
}
