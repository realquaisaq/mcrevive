using System;

namespace MCRevive
{
    public class CmdTBColor : Command
    {
        public override string name { get { return "tbcolor"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdTBColor() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            string[] args = message.Split(' ');
            Player who = Player.Find(args[0]);
            if (who == null)
            {
                Player.SendMessage(p, "Could not find player: " + args[0] + ".");
                return;
            }
            if (args.Length == 1)
            {
                who.prefixcolor = who.group.color;
                Player.SendMessage(who, "Your title-bracket's color has been reset.");
                if (p != who) Player.SendMessage(p, Name(who.color + who.name) + " title-bracket's color has been reset.");
                who.SetPrefix();
                return;
            }
            else
            {
                string color = c.Parse(args[1]);
                if (color == "")
                    Player.SendMessage(p, "There is no color \"" + args[1] + "\".");
                else if (color == who.prefixcolor)
                    Player.SendMessage(p, Name(who.color + who.name) + " title-bracket's color is already " + color + c.Name(color) + Server.DefaultColor + ".");
                else
                {
                    Player.SendMessage(who, "Your title-bracket's color is now " + color + c.Name(color) + Server.DefaultColor + "!");
                    if (p != who) Player.SendMessage(p, Name(who.color + who.name) + " title-bracket's color is now " + color + c.Name(color) + Server.DefaultColor + ".");
                    who.prefixcolor = color;
                    who.SetPrefix();
                }
            }
        }

        public override void Help(Player p)
        {
            Player.SendMessage(p, "/tbcolor <player> [color] - Gives <player> the title color of [color].");
            Player.SendMessage(p, "If no [color] is specified, title-bracket color is removed.");
            Player.SendMessage(p, "&0black &1navy &2green &3teal &4maroon &5purple &6gold &7silver");
            Player.SendMessage(p, "&8gray &9blue &alime &baqua &cred &dpink &eyellow &fwhite");
            Player.SendMessage(p, "&rrainbow");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
        string Name(string name)
        {
            string ch = name[name.Length - 1].ToString().ToLower();
            if (ch == "s" || ch == "x") { return name + Server.DefaultColor + "'"; }
            else { return name + Server.DefaultColor + "'s"; }
        }
    }
}
