
using System;

namespace MCRevive
{
    class CmdMaxLimit : Command
    {
        public override string name { get { return "maxlimit"; } }
        public override string shortcut { get { return "ml"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdMaxLimit() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            int number = message.Split(' ').Length;
            if (number != 2) { Help(p); return; }
            else
            {
                string t = message.Split(' ')[0].Trim().ToLower();
                string s = message.Split(' ')[1].Trim().ToLower();
                int sint = 0;
                try
                {
                    long check = long.Parse(s);
                    if (check > 1000000000)
                    {
                        Player.SendMessage(p, "limit can't be higher than 1.000.000.000.");
                        return;
                    }
                    if (check < 0)
                    {
                        Player.SendMessage(p, "limit can't be lower than 0.");
                        return;
                    }
                }
                catch { Player.SendMessage(p, "Unable to read limit!"); return; }
                sint = int.Parse(s);
                Group group = Group.Find(t);
                if (group == null) { Player.SendMessage(p, "Unable to find group: " + t); return; }
                if (group.maxBlocks == sint)
                {
                    Player.SendMessage(p, "Max build limit is already " + sint + " for " + group.name + ".");
                    return;
                }

                if (p.group.Permission >= group.Permission)
                    group.maxBlocks = sint;
                else
                {
                    Player.SendMessage(p, "You need to be atleast " + LevelPermission.ToName(group.Permission)
                        + " to change this!"); return;
                }

                Player.GlobalMessage("Build limit has been changed to " + group.maxBlocks + " for " + group.name + ".");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/maxlimit or /ml - <rank> <number> - Sets the build limit.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
