namespace MCRevive
{
    public class CmdVoice : Command
    {
        public override string name { get { return "voice"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdVoice() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            p.voice = !p.voice;
            if (p.voice)
            {
                Player.SendMessage(p, "you can now speak");
            }
            else
            {
                Player.SendMessage(p, "you can no longer speak");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/voice - lets you talk, even with chat moderation on.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}