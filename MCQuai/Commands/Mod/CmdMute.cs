using System;

namespace MCRevive
{
    public class CmdMute : Command
    {
        public override string name { get { return "mute"; } }
        public override string shortcut { get { return "mute"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdMute() { }
        public override void Use(Player p, string message)
        {
            if (message == "" || message.Split(' ').Length > 2) { Help(p); return; }
            Player who = Player.Find(message);
            if (who == null)
            {
                Player.SendMessage(p, "Player MUST be online to mute/unmute them!");
                return;
            }
            else if (who == p && !p.ignorePermission)
            {
                Player.SendMessage(p, "You cannot mute nor unmute yourself.");
                return;
            }
            who.muted = !who.muted;
            if (who.muted)
                Player.GlobalMessage(who.color + who.name + Server.DefaultColor + " has been muted.");
            else
            {
                if (p != null && who.group.Permission >= p.group.Permission && !p.ignorePermission)
                {
                    Player.SendMessage(p, "Can not mute people equal or greater rank then you!");
                    who.muted = false;
                    return;
                }
                Player.GlobalMessage(who.color + who.name + Server.DefaultColor + " has been unmuted.");
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/mute <player> - Mutes or Un-Mutes a player!");
            Player.SendMessage(p, "You cannot mute nor unmute yourself.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}