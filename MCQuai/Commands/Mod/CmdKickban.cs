
using System;

namespace MCRevive
{
    public class CmdKickban : Command
    {
        public override string name { get { return "kickban"; } }
        public override string shortcut { get { return "kb"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdKickban() { }
        public override void Use(Player p, string message)
        {
            bool at = false;
            if (message == "") { Help(p); return; }
            if (message[0] == '@') { at = true; message = message.Substring(1); }
            Player who = Player.Find(message.Split(' ')[0]);

            if (at)
            {
                Command.all.Find("xban").Use(p, who.name + " " + message);
            }
            else
            {
                Command.all.Find("ban").Use(p, who.name);
                Command.all.Find("kick").Use(p, who.name + " " + message);
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/kickban [@]<player> [message] - Kicks and bans a player with an optional message.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}