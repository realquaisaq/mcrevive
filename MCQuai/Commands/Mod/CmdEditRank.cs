﻿using System;

namespace MCRevive
{
    class CmdEditRank : Command
    {
        public override string name { get { return "editrank"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Owner; } }
        public CmdEditRank() { }
        public override void Use(Player p, string message)
        {
            if (message.Split(' ').Length < 2 || message.Split(' ').Length > 3) { Help(p); return; }
            Group group = Group.Find(message.Split(' ')[0]);
            string action = message.Split(' ')[1].ToLower();
            string newVal = "";
            if (message.Split(' ').Length == 3) newVal = message.Split(' ')[2];
            if (group == null) { Player.SendMessage(p, "Could not find rank " + message.Split(' ')[0] + "."); return; }
            else if (Group.GroupList.IndexOf(group) - 1 < 1 || Group.GroupList.IndexOf(group) == Group.GroupList.Count - 1) { Player.SendMessage(p, "You cannot edit anything in this rank."); return; }
            else if (action != "name" && action != "permission" && action != "maxblocks") { Player.SendMessage(p, action + " is not a valid parameter!"); Help(p); return; }
            else if (action == "name" && newVal != "") { foreach (char ch in newVal) if (!Char.IsLetter(ch)) { Player.SendMessage(p, newVal + " is not a valid name!"); return; } }
            else if (newVal != "" && (action == "permission" || action == "maxblocks"))
                try
                {
                    if (int.Parse(newVal) < 1 && action == "maxblocks") { Player.SendMessage(p, "Maxblocks cannot be below 1"); return; }
                    else if ((int.Parse(newVal) < -20 || int.Parse(newVal) >= 120) && action == "permission") { Player.SendMessage(p, "permission has to be between or equal to -20 and 119"); return; }
                }
                catch { Player.SendMessage(p, action + " has to be a number!"); }

            if (message.Split(' ').Length == 2)
            {
                if (action == "name")
                    Player.SendMessage(p, "The name of " + group.color + group.name + Server.DefaultColor + " is " + group.name + ".");
                else if (action == "permission")
                    Player.SendMessage(p, "The permission of " + group.color + group.name + Server.DefaultColor + " is " + group.Permission + ".");
                else if (action == "maxblocks")
                    Player.SendMessage(p, "The maxblock of " + group.color + group.name + Server.DefaultColor + " is " + group.maxBlocks + ".");
                else
                    Player.SendMessage(p, "WTF IS GOING ON???");
            }
            else if (action == "name")
            {
                Player.SendMessage(p, group.color + group.name + Server.DefaultColor + "'s name is now " + newVal);
                Group.GroupList[Group.GroupList.IndexOf(group)].name = newVal;
            }
            else if (action == "permission")
            {
                Group.GroupList[Group.GroupList.IndexOf(group)].Permission = int.Parse(newVal);
                Player.SendMessage(p, group.color + group.name + Server.DefaultColor + "'s permission is now " + newVal);
            }
            else if (action == "maxblocks")
            {
                Group.GroupList[Group.GroupList.IndexOf(group)].maxBlocks = int.Parse(newVal);
                Player.SendMessage(p, "people with the rank " + group.color + group.name + Server.DefaultColor + " can now build " + newVal + " blocks at once.");
            }
            else
                Player.SendMessage(p, "WTF IS GOING ON???");

            Group.saveGroups(Group.GroupList);
            Group.InitAll();
            Command.InitAll();
            GrpCommands.fillRanks();
            Block.SetBlocks();
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/editrank <name> <'name'/'permission'/'maxblocks'> [new value] - edit one of the variables of a rank.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/editrank engineer name'",
                "This will show the name of the rank engineer",
                "Which ofcourse is 'engineer'",
                "&a----------------------------------------------",
                "&3'/editrank engineer name worker'",
                "This will make the rank 'engineer', become 'worker'",
                "",
                "&3'/editrank engineer permission'",
                "This will show the permission of the rank engineer",
                "&a----------------------------------------------",
                "&3'/editrank engineer permission 25'",
                "This will change the permission of the rank engineer to 25'",
                "",
                "&3'/editrank engineer maxblocks'",
                "This will show the maximum blocks build at once,",
                "for the rank engineer",
                "&a----------------------------------------------",
                "&3'/editrank engineer maxblocks 25000'",
                "This will delete the rank 'engineer'",
                "Everyone that had that rank will become builder."
            };
        }
    }
}