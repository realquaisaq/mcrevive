/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MCRevive
{
    class CmdDeleteLvl : Command
    {
        public override string name { get { return "deletelvl"; } }
        public override string shortcut { get { return "dlvl"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdDeleteLvl() { }
        public override void Use(Player p, string message)
        {
            int i = 0;
            if (message == "") { Help(p); return; }
            Level foundLevel = Level.Find(message);
            Server.s.Log("stuff" + ++i);
            if (foundLevel != null)
            {
                foundLevel.Unload();
                message = foundLevel.name;
            }

            if (foundLevel == Server.mainLevel) { Player.SendMessage(p, "Cannot delete the main level."); return; }

            try
            {
                if (!Directory.Exists("levels/deleted")) Directory.CreateDirectory("levels/deleted");

                Server.s.Log("stuff" + ++i);
                if (File.Exists("levels/" + message + ".lvl") || File.Exists("levels/" + message + ".mcqlvl"))
                {
                    if (File.Exists("levels/" + message + ".mcqlvl"))
                    {
                        Server.s.Log("stuff" + ++i);
                        int currentNum = 0;
                        while (File.Exists("levels/deleted/" + message + currentNum + ".mcqlvl")) currentNum++;

                        try
                        {
                            File.Move("levels/" + message + ".mcqlvl", "levels/deleted/" + message + currentNum + ".mcqlvl");
                        }
                        catch
                        {
                            File.Move("levels/" + message + ".lvl", "levels/deleted/" + message + currentNum + ".lvl");
                        }
                    }
                    else if (File.Exists("levels/" + message + ".lvl"))
                    {
                        int currentNum = 0;
                        while (File.Exists("levels/deleted/" + message + currentNum + ".lvl")) currentNum++;

                        try
                        {
                            File.Move("levels/" + message + ".lvl", "levels/deleted/" + message + currentNum + ".lvl");
                        }
                        catch
                        {
                            File.Move("levels/" + message + ".mcqlvl", "levels/deleted/" + message + currentNum + ".mcqlvl");
                        }
                    }
                    else
                    {
                        try
                        {
                            File.Move("levels/" + message + ".mcqlvl", "levels/deleted/" + message + ".mcqlvl");
                        }
                        catch
                        {
                            File.Move("levels/" + message + ".lvl", "levels/deleted/" + message + ".lvl");
                        }
                    }
                    Player.SendMessage(p, "Created backup.");

                    try { File.Delete("levels/properties/" + message + ".properties"); }
                    catch { }
                    try { File.Delete("levels/properties/" + message); }
                    catch { }
                    try { Directory.Delete("memory/portals/" + message); }
                    catch { }
                    try { File.Delete("memory/zones/" + message + ".zone"); }
                    catch { }

                    Player.GlobalMessage("Level " + message + " was deleted.");
                }
                else
                {
                    Player.SendMessage(p, "Could not find specified level.");
                }
            }
            catch (Exception e) { Player.SendMessage(p, "Error when deleting."); Server.ErrorLog(e); }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/deletelvl <map> - Completely deletes <map>");
            Player.SendMessage(p, "A backup of the map will be placed in the levels/deleted folder");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}
