﻿using System;

namespace MCRevive
{
    class CmdNewRank : Command
    {
        public override string name { get { return "newrank"; } }
        public override string shortcut { get { return "addrank"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Owner; } }
        public CmdNewRank() { }
        public override void Use(Player p, string message)
        {
            if (message.Split(' ').Length != 4) { Help(p); return; }
            foreach (char ch in message.Split(' ')[0]) if (!Char.IsLetter(ch)) { Player.SendMessage(p, "Invalid name."); return; }
            if (String.IsNullOrEmpty(c.Parse(message.Split(' ')[1]))) { Player.SendMessage(p, "Invalid color."); return; }
            try { int pe = int.Parse(message.Split(' ')[2]); if (pe > 119 || pe < -20) throw new Exception(); }
            catch { Player.SendMessage(p, "Invalid permission."); return; }
            try { int maxb = int.Parse(message.Split(' ')[3]); if (maxb < 0) throw new Exception(); }
            catch { Player.SendMessage(p, "Invalid max blocks."); return; }

            string rank = message.Split(' ')[0];
            char color = c.Parse(message.Split(' ')[1])[1];
            int permission = int.Parse(message.Split(' ')[2]);
            int maxbuild = int.Parse(message.Split(' ')[3]);

            foreach (Group grp in Group.GroupList)
            {
                if (grp.Permission == permission)
                {
                    Player.SendMessage(p, "Theres already another rank with that permission!");
                    Player.SendMessage(p, grp.color + grp.name + Server.DefaultColor + "!");
                    return;
                }
                else if (grp.name == rank)
                {
                    Player.SendMessage(p, "Theres already another rank with that name.");
                    return;
                }
            }
            Group.GroupList.Add(new Group(permission, maxbuild, rank, color));

            Group.saveGroups(Group.GroupList);
            Group.InitAll();
            Command.InitAll();
            GrpCommands.fillRanks();
            Block.SetBlocks();
            Player.SendMessage(p, "The rank " + "&" + color + rank + Server.DefaultColor + " has been added.");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/newrank <name> <color> <permission> <max blocks> - Creates a new rank.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/newrank engineer 50 250000'",
                "This will create a new rank called engineer.",
                "The engineer will have a permission of 50.",
                "The engineer will be able to do a 250000 blocked cuboid."
            };
        }
    }
}
