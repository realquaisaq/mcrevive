
namespace MCRevive
{
    public class CmdFreeze : Command
    {
        public override string name { get { return "freeze"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdFreeze() { }
        public override void Use(Player p, string message)
        {
            bool notOne = false;
            Player who = null;
            if (message == "") { Help(p); return; }
            if (message.ToLower() == "all") notOne = true;
            else who = Player.Find(message);
            if (!notOne)
            {
                if (who == null) { Player.SendMessage(p, "Could not find player."); return; }
                else if (who == p) { Player.SendMessage(p, "Cannot freeze yourself."); return; }
                else if (who.group.Permission >= p.group.Permission) { Player.SendMessage(p, "Cannot freeze someone of equal or greater rank."); return; }
            }
            if (notOne)
            {
                int type = 3;
                bool worked = true;
                foreach (Player all in Player.players)
                {
                    if (p.group.Permission > all.group.Permission && p != all)
                    {
                        all.frozen = !all.frozen;
                        if (type != 2)
                        {
                            if (!all.frozen) type = 0;
                            if (all.frozen) type = 1;
                            if (!all.frozen && type == 1) type = 2;
                            if (all.frozen && type == 0) type = 2;
                        }
                    }
                    else
                    {
                        worked = false;
                    }
                }
                Player.SendMessage(p, ((worked) ? "All players frozen" + ((type == 2) ? ", and some defrozen!" : "!") :
                    "Some players were not frozen because they were equal or greater rank then you!"));

                foreach (Player all in Player.players)
                {
                    if (worked) { if (p != all) Player.SendMessage(all, "All players have been frozen" + ((type == 2) ? ", and some have been defrozen!" : "!")); }
                    else { Player.SendMessage(all, "Some players have been frozen" + ((type == 2) ? ", and some have been defrozen!" : "!")); }
                }
            }
            else
            {
                if (!who.frozen)
                {
                    who.frozen = true;
                    Player.GlobalChat(null, who.color + who.name + Server.DefaultColor + " has been &bfrozen" + Server.DefaultColor + ".", false);
                }
                else
                {
                    who.frozen = false;
                    Player.GlobalChat(null, who.color + who.name + Server.DefaultColor + " has been &adefrozen" + Server.DefaultColor + ".", false);
                }
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/freeze <name> - Stops <name> from moving until unfrozen.");
            Player.SendMessage(p, "/freeze all - Freezes all the players on the server!");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}