
using System;

namespace MCRevive
{
    public class CmdAdminPromote : Command
    {
        public override string name { get { return "adminpromote"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Owner; } }
        public CmdAdminPromote() { }
        public override void Use(Player p, string message)
        {
            if (message == "1" || message == "true" || message == "yes") { Server.adminpromote = false; }
            else if (message == "0" || message == "false" || message == "no") { Server.adminpromote = true; }
            else if (message == "") { Help(p); return; }
            Server.adminpromote = !Server.adminpromote;

            Player.GlobalMessage("the admin promote/demote permission is now " + Server.adminpromote + ".");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/Adminpromote <true/false> - Tells the server if admins are allowed to promote other players to admins.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}