/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MCRevive
{
    class CmdRegenLvl : Command
    {
        public override string name { get { return "regenlvl"; } }
        public override string shortcut { get { return "rlvl"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdRegenLvl() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            Level fLvl = Level.Find(message);
            if (fLvl == null)
            {
                Player.SendMessage(p, "The level could not be found - it must be loaded first");
                return;
            }

            List<string> players = new List<string>(fLvl.ListPlayers.ToArray());
            foreach (string pp in players)
                Player.FindExact(pp).HandleCommand("goto", Server.mainLevel.name);

            // Save params needed to regen
            ushort w = fLvl.width, h = fLvl.height, d = fLvl.depth;
            string name = fLvl.name, type = fLvl.type, owner = fLvl.creator;

            // Delete level
            fLvl.Unload();
            File.Delete("levels\\" + fLvl.name + ".mcqlvl");
            Server.levels.Remove(fLvl);

            // Generate level
            Level lvl = new Level(name, w, d, h, type, owner);
            lvl.Save();
            Level.Load(name);

            // Move everyone currently in main, back
            foreach (string pp in players)
                Player.FindExact(pp).HandleCommand("goto", lvl.name);
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                ""
            };
        }
        public override void Help(Player p)
        {

        }
    }
}
