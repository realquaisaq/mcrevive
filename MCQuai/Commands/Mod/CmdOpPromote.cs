
using System;

namespace MCRevive
{
    public class CmdOpPromote : Command
    {
        public override string name { get { return "oppromote"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Owner; } }
        public CmdOpPromote() { }
        public override void Use(Player p, string message)
        {
            if (message == "true" || message == "1" || message == "yes") { Server.oppromote = false; }
            else if (message == "false" || message == "0" || message == "no") { Server.oppromote = true; }
            else if (message != "") { Help(p); return; }
            Server.oppromote = !Server.oppromote;
            
            Player.GlobalMessage("the op promote/demote permission is now " + Server.oppromote + ".");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/Oppromote - Tells the server if ops are allowed to promote other players to ops.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}