/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace MCRevive
{
    class CmdNewLvl : Command
    {
        public override string name { get { return "newlvl"; } }
        public override string shortcut { get { return "nlvl"; } }
        public override string type { get { return "mod"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdNewLvl() { }
        List<ushort> goodDimensions = new List<ushort>(new ushort[] { 16, 32, 48, 64, 80, 96, 112 }); // this, and everything dividable by 128 is good dimensions
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }

            string[] parameters = message.Split(' '); // Grab the parameters from the player's message
            if (parameters.Length == 5) // make sure there are 5 params
            {
                switch (parameters[4].ToLower())
                {
                    case "flat":
                    case "pixel":
                    case "island":
                    case "mountains":
                    case "ocean":
                    case "desert":
                    case "forest":
                    case "box":
                        break;

                    default:
                        Player.SendMessage(p, "Valid types: island, mountains, desert, forest, ocean, flat, pixel, box");
                        return;
                }
                if (parameters[0].IndexOf('$') != -1) { Player.SendMessage(p, "A map cannot have a $ in its name."); return; }

                // check size
                ushort x, y, z;
                try { x = ushort.Parse(parameters[1]); if (x > 32000) throw new Exception(""); }
                catch { Player.SendMessage(p, "Your x is invalid"); return; }
                try { y = ushort.Parse(parameters[2]); if (y > 32000) throw new Exception(""); }
                catch { Player.SendMessage(p, "Your y is invalid"); return; }
                try { z = ushort.Parse(parameters[3]); if (z > 32000) throw new Exception(""); }
                catch { Player.SendMessage(p, "Your z is invalid"); return; }


                if (x % 16 != 0 || y % 16 != 0 || z % 16 != 0)
                {
                    Player.SendMessage(p, "Bad world dimensions detected: " + x.ToString() + "x" + y.ToString() + "x" + z.ToString());
                    if (Server.newlvlHelp)
                    {
                        try
                        {
                            x = (ushort)((x - (x % 16)) + 16);
                            while (!goodDimensions.Contains(x))
                            {
                                if (x % 128 == 0) break;
                                else if (x % 128 < 64) x -= (ushort)(x % 128);
                                else x += (ushort)(128 - x % 128);
                            }
                            y = (ushort)((y - (y % 16)) + 16);
                            while (!goodDimensions.Contains(y))
                            {
                                if (y % 128 == 0) break;
                                if (y % 128 < 64) y -= (ushort)(y % 128);
                                else y += (ushort)(128 - y % 128);
                            }
                            z = (ushort)((z - (z % 16)) + 16);
                            while (!goodDimensions.Contains(z))
                            {
                                if (z % 128 == 0) break;
                                if (z % 128 < 64) z -= (ushort)(z % 128);
                                else z += (ushort)(128 - z % 128);
                            }
                            Player.SendMessage(p, "Dimensions fixed: &4" + x.ToString() + Server.DefaultColor + "x&4" + y.ToString() + Server.DefaultColor + "x&4" + z.ToString());
                        }
                        catch { Player.SendMessage(p, "Failed to fix dimensions."); }

                    }
                    else
                        Player.SendMessage(p, "&aEnable &bnewlvl-help " + Server.DefaultColor + "to get dimensions fixed automatically.");
                }
                
                // create a new level...
                try
                {
                    Level lvl = new Level(parameters[0], x, y, z, parameters[4], p != null ? p.name : null);
                    lvl.Save(); //... and save it.
                    lvl.Backup();
                }
                finally
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                Player.GlobalMessage("Level " + parameters[0] + " has been created"); // The player needs some form of confirmation.
            }
            else
            {
                Player.SendMessage(p, "Not enough parameters!"); // Yell at the player for failing
                Help(p);
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/newlvl <mapname> <x> <y> <z> <type> - creates a new level.");
            Player.SendMessage(p, "types are: island, mountains, desert, forest, ocean, flat, pixel, box");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                "&3'/newlvl freebuild 128 128 128 flat'",
                "This will create a new map with the name 'freebuild'.",
                "It will have the width, height and depth at 128.",
                "The type of the map will be 'flat'",
                "The types are as follows: island, mountains, desert,",
                "forest, ocean, flat, pixel, box"
            };
        }
    }
}
