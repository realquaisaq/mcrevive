﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;

namespace MCRevive
{
    public sealed class CommandList
    {
        public List<Command> commands = new List<Command>();
        public CommandList() { }
        public void Add(Command cmd) { commands.Add(cmd); }
        public void AddRange(List<Command> listCommands)
        {
            listCommands.ForEach(delegate(Command cmd) { commands.Add(cmd); });
        }
        public List<string> commandNames()
        {
            List<string> tempList = new List<string>();
            for (int i = 0; i < commands.Count; ++i)
                tempList.Add(commands[i].name);

            return tempList;
        }

        public bool Remove(Command cmd) { return commands.Remove(cmd); }
        public bool Contains(Command cmd) { return commands.Contains(cmd); }
        public bool Contains(string name)
        {
            name = name.ToLower();
            foreach (Command cmd in commands)
                if (cmd.name == name.ToLower())
                    return true;
            return false;
        }

        public Command Find(string name, Player p = null)
        {
            List<Command> tempList = new List<Command>();
            tempList.AddRange(commands);
            int lowest = 1000;
            Command lowestCmd = null;
            bool doubleOccurrance = true;
            foreach (Command c in tempList)
            {
                if (c.name.ToLower() == name.ToLower()) return c;
                if (c.shortcut.ToLower() == name.ToLower()) return c;
                if (Block.Number(name.ToLower()) != Block.Zero) return null;
                int l = FindReference.Levenshtein(name.ToLower(), c.name.ToLower());
                if (l < lowest && l < 3 && name.Length > 4) { lowest = l; lowestCmd = c; doubleOccurrance = false; }
                else if (l == lowest) { doubleOccurrance = true; }
            }
            if (lowestCmd != null && !doubleOccurrance)
            {
                Player.SendMessage(p, "Could not find command &a/" + name + Server.DefaultColor + ".");
                Player.SendMessage(p, "Using closest match: &a/" + lowestCmd.name + Server.DefaultColor + ".");
            }
            if (!doubleOccurrance) return lowestCmd;
            return null;
        }
        public Command FindExact(string name, Player p = null)
        {
            List<Command> tempList = new List<Command>();
            tempList.AddRange(commands);
            for (int i = 0; i < tempList.Count; ++i)
                if (tempList[i].name.ToLower() == name.ToLower())
                    return tempList[i];
            return null;
        }

        public string FindShort(string shortcut)
        {
            if (shortcut == "") return "";
            shortcut = shortcut.ToLower();
            
            foreach (Command cmd in commands)
            {
                if (cmd.shortcut == shortcut) return cmd.name;
            }
            return "";
        }

        public List<Command> All() { return new List<Command>(commands); }
    }
}