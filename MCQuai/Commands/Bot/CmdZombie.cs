﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace MCRevive
{
    class CmdZombie : Command
    {
        public override string name { get { return "zombie"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdZombie() { }
        public override void Use(Player p, string message)
        {
            message = message.ToLower().Replace("add", "");
            if (message == "")
            {
                byte i = 1;
                for (; PlayerBot.AdvFind("Zombie" + i) != null; i++) { }

                if (i <= 64)
                {
                    p.spawnedBots++;
                    PlayerBot.playerbots.Add(new PlayerBot("__Z0MBI3__", p.level, p.pos[0], p.pos[1], p.pos[2], p.rot[0], 0, "Zombie" + i));
                    PlayerBot Pb = PlayerBot.AdvFind("Zombie" + i);
                    Pb.hunt = true;
                    try { Pb.Waypoints.Clear(); }
                    catch { }
                    Pb.AIName = "";
                    Pb.kill = true;
                    Pb.isZombie = true;
                }
                else
                    Player.SendMessage(p, "there are too many bots on the server");
            }
            else
            {
                List<string> msgs = new List<string>(message.ToLower().Split(' '));
                if (msgs.Contains("del") || msgs.Contains("rem") || msgs.Contains("delete") || msgs.Contains("remove"))
                {
                    message = message.ToLower().Replace("del", "").Replace("rem", "").Replace("delete", "").Replace("remove", "");
                    if (message == "") message = "1";
                    try
                    {
                        byte num = byte.Parse(message);
                        for (byte i = 0; i < num; i++)
                        {
                            byte b = 0;
                            PlayerBot Pb = null;
                            try { retry: if (PlayerBot.playerbots[b].isZombie) Pb = PlayerBot.playerbots[b]; else { b++; goto retry; } }
                            catch { if (Pb == null) if (i > 0) Player.SendMessage(p, "theres no more zombies."); else Player.SendMessage(p, "There are no zombies."); return; }
                            Pb.removeBot();
                        }
                        Player.SendMessage(p, num + " zombie" + ((num != 1) ? "s" : "") + " has been removed from the server.");
                    }
                    catch
                    {
                        Player.SendMessage(p, "invalid number");
                    }
                }
                else
                {
                    if (message == "") message = "1";
                    try
                    {
                        byte num = byte.Parse(message);
                        if (num + PlayerBot.playerbots.Count > 64)
                        {
                            Player.SendMessage(p, "You cant add " + ((num > 1) ? "these bots" : "this bot") + " due to the 64 bot limit.");
                            Player.SendMessage(p, "You can add " + (64 - PlayerBot.playerbots.Count) + " more bot" + ((64 - PlayerBot.playerbots.Count != 1) ? "." : "s."));
                            return;
                        }
                        for (byte i = 0; i < num; i++)
                        {
                            Command.all.Find("zombie").Use(p, "");
                        }
                    }
                    catch
                    {
                        Player.SendMessage(p, "invalid number");
                    }
                }
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/Zombie [\"add\"] [amount] - Creates a/several zombies.");
            Player.SendMessage(p, "/Zombie <\"del\"> [amount] - Deletes a/several zombies.");
            Player.SendMessage(p, "there can be a max of 64 bots on a server");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/zombie add 5' or '/zombie 5':",
                "This will add 5 zombies to the map.",
                "&3'/zombie del 5'",
                "This will kill 5 zombies.",
                "&3'/zombie add' or '/zombie del'",
                "This will add or kill 1 zombie."
            };
        }
    }
}
