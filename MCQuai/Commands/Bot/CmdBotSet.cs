// source from MCLawl

using System;
using System.IO;

namespace MCRevive
{
    public class CmdBotSet : Command
    {
        public override string name { get { return "botset"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdBotSet() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }

            try
            {
                if (message.Split(' ').Length == 1)
                {
                    PlayerBot pB = PlayerBot.Find(message);
                    if (pB != null)
                    {
                        try { pB.Waypoints.Clear(); }
                        catch { }
                        pB.kill = false;
                        pB.hunt = false;
                        pB.AIName = "";
                        Player.SendMessage(p, pB.color + pB.name + Server.DefaultColor + "'s AI was turned off.");
                        return;
                    }
                    else
                    {
                        Player.SendMessage(p, "Theres no bot called: " + message);
                        return;
                    }
                }
                else if (message.Split(' ').Length != 2)
                {
                    Help(p); return;
                }

                PlayerBot Pb = PlayerBot.Find(message.Split(' ')[0]);
                if (Pb == null) { Player.SendMessage(p, "Could not find specified Bot"); return; }
                string foundPath = message.Split(' ')[1].ToLower();

                if (foundPath == "hunt")
                {
                    Pb.hunt = !Pb.hunt;
                    try { Pb.Waypoints.Clear(); }
                    catch { }
                    Pb.AIName = "";
                    if (p != null) Player.GlobalChatLevel(p, Pb.color + Pb.name + Server.DefaultColor + "'s hunt instinct: " + Pb.hunt, false);
                    return;
                }
                else if (foundPath == "kill")
                {
                    if (p != null && p.group.Permission < LevelPermission.Operator && !p.ignorePermission) { Player.SendMessage(p, "Only OP+ may toggle killer instinct."); return; }
                    Pb.kill = !Pb.kill;
                    if (p != null) Player.GlobalChatLevel(p, Pb.color + Pb.name + Server.DefaultColor + "'s kill instinct: " + Pb.kill, false);
                    return;
                }
            }
            catch { if (p != null) Player.SendMessage(p, "Error"); return; }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/botset <bot> <AI script> - Makes <bot> do <AI script>");
            Player.SendMessage(p, "AI scripts: Kill and Hunt");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
    }
}