﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;

namespace MCRevive
{
    public class CmdBotAdd : Command
    {
        public override string name { get { return "botadd"; } }
        public override string shortcut { get { return "ba"; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdBotAdd() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            if (PlayerBot.playerbots.Count >= 64) { Player.SendMessage(p, "There are too many bots, remove some first!"); return; }
            if (PlayerBot.FindExact(message) != null) { Player.SendMessage(p, "A bot with that name already exists!"); return; }
            if (!PlayerBot.ValidName(message)) { Player.SendMessage(p, "the name \"" + message + "\" is not valid!"); return; }
            p.spawnedBots++;
            PlayerBot.playerbots.Add(new PlayerBot(message, p.level, p.pos[0], p.pos[1], p.pos[2], p.rot[0], 0));
            Player.GlobalMessageLevel(p.level, PlayerBot.FindExact(message).color + PlayerBot.FindExact(message).name +
                Server.DefaultColor + ", the bot, has been added.");
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/botadd <name> - Add a new bot at your position.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/botadd quaisaq'",
                "This will add a bot named quaisaq."
            };
        }
    }
}