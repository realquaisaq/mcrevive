﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;

namespace MCRevive
{
    public class CmdBotRemove : Command
    {
        public override string name { get { return "botremove"; } }
        public override string shortcut { get { return "br"; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdBotRemove() { }
        public override void Use(Player p, string message)
        {
            if (message == "") { Help(p); return; }
            if (message == "all")
            {
                bool failed = false;
                for (int i = 0; i <=  PlayerBot.playerbots.Count; i++)
                {
                    try
                    {
                        PlayerBot Pb = PlayerBot.playerbots.ToArray()[0];
                        Pb.removeBot();
                        Server.s.Log("removed " + name + " bot");
                    }
                    catch { if (i != PlayerBot.playerbots.Count) failed = true; }
                }
                if (!failed)
                    Player.GlobalMessage("All bots in " + p.level.name + " has been removed!");
                else
                    Player.SendMessage(p, "Some bots was not removed!");
            }
            else
            {
                PlayerBot Pb = PlayerBot.Find(message);
                if (Pb == null) { Player.SendMessage(p, "There is no bot: " + message + "!"); return; }
                if (p.level != Pb.level) { Player.SendMessage(p, Pb.name + " is in a different map."); return; }
                Pb.removeBot();
                Player.SendMessage(p, "Removed bot: " + Pb.name);
            }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/botremove <name> - Removes <name> if on same map as you.");
            Player.SendMessage(p, "/botremove <'all'> - Removes all bots in the map you play on.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/botremove quaisaq'",
                "This will remove the bot quaisaq.",
                "&3'/botremove all'",
                "This will remove all the bots on the map you are playing on."
            };
        }
    }
}