using System;
using System.IO;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdCheckerboard : Command
    {
        public override string name { get { return "checkerboard"; } }
        public override string shortcut { get { return "cb"; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdCheckerboard() { }
        public override void Use(Player p, string message)
        {
            int number = message.Split(' ').Length;
            if (number < 2 && number > 5) { Help(p); return; }
            else
            {
                CatchPos cpos = new CatchPos();
                switch (message.Split(' ')[0])
                {
                    case "wire":
                    case "wires":
                        cpos.extra = "wire";
                        message = message.Remove(0, 5);
                        break;
                    case "hollow":
                    case "box":
                        cpos.extra = "hollow";
                        message = message.Remove(0, 7);
                        break;
                    case "solid":
                        cpos.extra = "";
                        break;
                    default:
                        cpos.extra = "";
                        break;
                }
                number = message.Split(' ').Length;
                ushort type1 = Block.Number(message.Split(' ')[0]);
                if (type1 == Block.Zero) { Player.SendMessage(p, "There is no block \"" + message.Split(' ')[0] + "\"."); return; }
                if (!Block.canPlace(p, type1) && !p.ignorePermission) { Player.SendMessage(p, "You are not allowed to place block " + Block.Name(type1) + "."); return; }
                
                ushort type2 = Block.Number(message.Split(' ')[1]);
                if (type2 == Block.Zero) { Player.SendMessage(p, "There is no block \"" + message.Split(' ')[1] + "\"."); return; }
                if (!Block.canPlace(p, type2) && !p.ignorePermission) { Player.SendMessage(p, "You are not allowed to place block " + Block.Name(type2) + "."); return; }
                ushort type3, type4;
                if (number >= 3)
                {
                    type3 = Block.Number(message.Split(' ')[2]);
                    if (type3 == Block.Zero) { Player.SendMessage(p, "There is no block \"" + message.Split(' ')[2] + "\"."); return; }
                    if (!Block.canPlace(p, type3) && !p.ignorePermission) { Player.SendMessage(p, "You are not allowed to place block " + Block.Name(type3) + "."); return; }
                }
                else type3 = Block.Zero;
                if (number >= 4)
                {
                    type4 = Block.Number(message.Split(' ')[3]);
                    if (type4 == Block.Zero) { Player.SendMessage(p, "There is no block \"" + message.Split(' ')[3] + "\"."); return; }
                    if (!Block.canPlace(p, type4) && !p.ignorePermission) { Player.SendMessage(p, "You are not allowed to place block " + Block.Name(type4) + "."); return; }
                }
                else type4 = Block.Zero;
                cpos.types = number;
                cpos.type = type1;
                cpos.type2 = type2;
                cpos.type3 = type3;
                cpos.type4 = type4;
                p.blockchangeObject = cpos;
            }
            Player.SendMessage(p, "Place two blocks to determine the edges.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/checkerboard <type> <block 1> <block 2> [block 3] [block 4] - Make a checkerboard!");
            Player.SendMessage(p, "Types: solid, hollow, wire.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos cpos = (CatchPos)p.blockchangeObject;
            cpos.x = x; cpos.y = y; cpos.z = z; p.blockchangeObject = cpos;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            try
            {
                p.ClearBlockchange();
                ushort b = p.level.GetTile(x, y, z);
                p.SendBlockchange(x, y, z, b);
                CatchPos cpos = (CatchPos)p.blockchangeObject;
                List<Pos> buffer = new List<Pos>();
                Level foundLevel = p.level;
                ushort startXX = Math.Min(cpos.x, x);
                ushort startYY = Math.Min(cpos.y, y);
                ushort startZZ = Math.Min(cpos.z, z);
                ushort endXX = Math.Max(cpos.x, x);
                ushort endYY = Math.Max(cpos.y, y);
                ushort endZZ = Math.Max(cpos.z, z);

                if (cpos.types == 2)
                {
                    for (ushort xx = startXX; xx <= endXX; ++xx)
                    {
                        for (ushort yy = startYY; yy <= endYY; ++yy)
                        {
                            for (ushort zz = startZZ; zz <= endZZ; ++zz)
                            {
                                if (cpos.extra == "" || ((cpos.extra == "hollow") && (xx == startXX || xx == endXX || yy == startYY || yy == endYY || zz == startZZ || zz == endZZ)) || (cpos.extra == "wire" && ((xx == startXX && yy == startYY) || (xx == startXX && zz == startZZ) || (yy == startYY && zz == startZZ) || (xx == startXX && yy == endYY) || (xx == startXX && zz == endZZ) || (yy == startYY && zz == endZZ) || (xx == endXX && yy == startYY) || (xx == endXX && zz == startZZ) || (yy == endYY && zz == startZZ) || (xx == endXX && yy == endYY) || (xx == endXX && zz == endZZ) || (yy == endYY && zz == endZZ))))
                                {
                                    if ((xx + yy + zz) % 2 == 0 && foundLevel.GetTile(xx, yy, zz) != cpos.type) buffer.Add(new Pos(xx, yy, zz, cpos.type));
                                        else if (foundLevel.GetTile(xx, yy, zz) != cpos.type2) buffer.Add(new Pos(xx, yy, zz, cpos.type2));
                                }
                            }
                        }
                    }
                }
                else if (cpos.types == 3)
                {
                    for (ushort xx = startXX; xx <= endXX; ++xx)
                    {
                        for (ushort yy = startYY; yy <= endYY; ++yy)
                        {
                            for (ushort zz = startZZ; zz <= endZZ; ++zz)
                            {
                                if (cpos.extra == "" || ((cpos.extra == "hollow") && (xx == startXX || xx == endXX || yy == startYY || yy == endYY || zz == startZZ || zz == endZZ)) || (cpos.extra == "wire" && ((xx == startXX && yy == startYY) || (xx == startXX && zz == startZZ) || (yy == startYY && zz == startZZ) || (xx == startXX && yy == endYY) || (xx == startXX && zz == endZZ) || (yy == startYY && zz == endZZ) || (xx == endXX && yy == startYY) || (xx == endXX && zz == startZZ) || (yy == endYY && zz == startZZ) || (xx == endXX && yy == endYY) || (xx == endXX && zz == endZZ) || (yy == endYY && zz == endZZ))))
                                {
                                    if (((xx + yy + zz) % 4 == 0 || (xx + yy + zz) % 4 == 2) && foundLevel.GetTile(xx, yy, zz) != cpos.type) buffer.Add(new Pos(xx, yy, zz, cpos.type));
                                    else if ((xx + yy + zz) % 4 == 1 && foundLevel.GetTile(xx, yy, zz) != cpos.type2) buffer.Add(new Pos(xx, yy, zz, cpos.type2));
                                    else if (foundLevel.GetTile(xx, yy, zz) != cpos.type3)  buffer.Add(new Pos(xx, yy, zz, cpos.type3));
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (ushort xx = startXX; xx <= endXX; ++xx)
                    {
                        for (ushort yy = startYY; yy <= endYY; ++yy)
                        {
                            for (ushort zz = startZZ; zz <= endZZ; ++zz)
                            {
                                if (cpos.extra == "" || ((cpos.extra == "hollow") && (xx == startXX || xx == endXX || yy == startYY || yy == endYY || zz == startZZ || zz == endZZ)) || (cpos.extra == "wire" && ((xx == startXX && yy == startYY) || (xx == startXX && zz == startZZ) || (yy == startYY && zz == startZZ) || (xx == startXX && yy == endYY) || (xx == startXX && zz == endZZ) || (yy == startYY && zz == endZZ) || (xx == endXX && yy == startYY) || (xx == endXX && zz == startZZ) || (yy == endYY && zz == startZZ) || (xx == endXX && yy == endYY) || (xx == endXX && zz == endZZ) || (yy == endYY && zz == endZZ))))
                                {
                                    if ((xx + yy + zz) % 4 == 0 && foundLevel.GetTile(xx, yy, zz) != cpos.type) buffer.Add(new Pos(xx, yy, zz, cpos.type));
                                    else if ((xx + yy + zz) % 4 == 1 && foundLevel.GetTile(xx, yy, zz) != cpos.type2) buffer.Add(new Pos(xx, yy, zz, cpos.type2));
                                    else if ((xx + yy + zz) % 4 == 2 && foundLevel.GetTile(xx, yy, zz) != cpos.type3) buffer.Add(new Pos(xx, yy, zz, cpos.type3));
                                    else if (foundLevel.GetTile(xx, yy, zz) != cpos.type4) buffer.Add(new Pos(xx, yy, zz, cpos.type4));
                                }
                            }
                        }
                    }
                }

                if (buffer.Count > p.group.maxBlocks && !p.ignorePermission)
                {
                    Player.SendMessage(p, "You tried to create " + buffer.Count + " blocks.");
                    Player.SendMessage(p, "You can only create " + p.group.maxBlocks + " blocks.");
                    return;
                }

                buffer.ForEach((pos) =>
                {
                    foundLevel.Blockchange(p, pos.x, pos.y, pos.z, pos.type, pos.type != Block.air);
                });
            }
            catch { }
        }

        struct Pos
        {
            public ushort x, y, z;
            public ushort type;
            public Pos(ushort _x = 0, ushort _y = 0, ushort _z = 0, ushort _type = Block.Zero) { x = _x; y = _y; z = _z; type = _type; }
        }
        struct CatchPos
        {
            public ushort x, y, z;
            public string extra;
            public int types;
            public ushort type;
            public ushort type2;
            public ushort type3;
            public ushort type4;
        }
    }
}