﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdReplace : Command
    {
        public override string name { get { return "replace"; } }
        public override string shortcut { get { return "r"; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdReplace() { }
        public override void Use(Player p, string message)
        {
            int number = message.Split(' ').Length;
            if (number != 2) { Help(p); return; }

            int pos = message.IndexOf(' ');
            string t = message.Substring(0, pos).ToLower();
            string t2 = message.Substring(pos + 1).ToLower();
            ushort type = Block.Number(t);
            if (type == 255) { Player.SendMessage(p, "There is no block \"" + t + "\"."); return; }
            if (!Block.canPlace(p, type) && !p.ignorePermission) { Player.SendMessage(p, "You are not allowed to place this block,"); return; }
            ushort type2 = Block.Number(t2);
            if (type2 == 255) { Player.SendMessage(p, "There is no block \"" + t2 + "\"."); return; }
            if (!Block.canPlace(p, type2) && !p.ignorePermission) { Player.SendMessage(p, "You are not allowed to place this block,"); return; }

            CatchPos cpos; cpos.type2 = type2; cpos.type = type;
            cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;
            Player.SendMessage(p, "Place two blocks to determine the edges.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/replace [type] [type2] - replace type with type2 inside a selected cuboid");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/replace stone air'",
                "Select 2 points",
                "All stone will be replaced with air in that area."
            };
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos bp = (CatchPos)p.blockchangeObject;
            bp.x = x; bp.y = y; bp.z = z; p.blockchangeObject = bp;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos cpos = (CatchPos)p.blockchangeObject;
            if (cpos.type != Block.Zero) type = cpos.type;
            List<Pos> buffer = new List<Pos>();
            Level l = p.level;

            for (ushort xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                for (ushort yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                    for (ushort zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                        if (p.level.GetTile(xx, yy, zz) == type)
                            BufferAdd(p, buffer, xx, yy, zz);

            if (buffer.Count > p.group.maxBlocks && !p.ignorePermission)
            {
                Player.SendMessage(p, "You tried to replace " + buffer.Count + " blocks.");
                Player.SendMessage(p, "You cannot replace more than " + p.group.maxBlocks + ".");
                return;
            }

            Player.SendMessage(p, buffer.Count + " blocks.");
            buffer.ForEach(delegate(Pos pos)
            {
                l.Blockchange(p, pos.x, pos.y, pos.z, cpos.type2, cpos.type2 != Block.air);
            });
        }
        void BufferAdd(Player p, List<Pos> list, ushort x, ushort y, ushort z)
        {
            Pos pos; pos.x = x; pos.y = y; pos.z = z; list.Add(pos);
        }

        struct Pos
        {
            public ushort x, y, z;
        }
        struct CatchPos
        {
            public ushort type;
            public ushort type2;
            public ushort x, y, z;
        }

    }
}
