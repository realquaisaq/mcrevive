﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.IO;

namespace MCRevive
{
    public class CmdSolid : Command
    {
        public override string name { get { return "solid"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdSolid() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            if (p.BlockAction == 1)
            {
                p.BlockAction = 0;
                Player.SendMessage(p, "Solid Mode: &cOFF" + Server.DefaultColor + ".");
            }
            else
            {
                p.BlockAction = 1;
                Player.SendMessage(p, "Solid Mode: &aON" + Server.DefaultColor + ".");
            }
            p.painting = false;
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/solid - Turns solid mode on/off.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/solid'",
                "This will make all blocks placed into adminium."
            };
        }
    }
}