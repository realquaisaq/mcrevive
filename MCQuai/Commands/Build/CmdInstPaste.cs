using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdInstPaste : Command
    {
        public override string name { get { return "instpaste"; } }
        public override string shortcut { get { return "iv"; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdInstPaste() { }
        public string loadname = "";
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }

            CatchPos cpos;
            cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;

            Player.SendMessage(p, "Place a block in the corner of where you want to paste."); p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/instpaste - Pastes the stored copy, almost instantly.");
            Player.SendMessage(p, "&4BEWARE: " + Server.DefaultColor + "The blocks will always be pasted in a set direction");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }

        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);

            Player.UndoPos Pos1;
            Level lvl = p.level;
            int onepercent = p.CopyBuffer.Count / 100;
            decimal at = 0;
            p.CopyBuffer.ForEach(delegate(Player.CopyPos pos)
            {
                Pos1.x = (ushort)(Math.Abs(pos.x) + x);
                Pos1.y = (ushort)(Math.Abs(pos.y) + y);
                Pos1.z = (ushort)(Math.Abs(pos.z) + z);

                for (int i = 10; i <= 100; i += 10)
                    if (onepercent * i == at)
                        Player.SendMessage(p, i + "% done.");
                if ((pos.type != Block.air || p.copyAir) && lvl.GetTile(Pos1.x, Pos1.y, Pos1.z) != Block.Zero)
                    lvl.Blockchange(p, (ushort)(Pos1.x + p.copyoffset[0]), (ushort)(Pos1.y + p.copyoffset[1]), (ushort)(Pos1.z + p.copyoffset[2]), pos.type, true, true);
                at++;
            });
            Command.all.Find("reveal").Use(p, "all");

            Player.SendMessage(p, "Pasted " + p.CopyBuffer.Count + " blocks.");

            if (p.staticCommands) p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }

        struct CatchPos { public ushort x, y, z; }
    }
}