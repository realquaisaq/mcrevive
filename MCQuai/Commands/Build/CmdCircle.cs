﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
namespace MCQuai
{
    public class CmdCircle : Command
    {
        public struct portalPos { public List<portPos> port; public short type; public bool Multi; }
        public struct portPos { public ushort x, y, z; public string portMapName; }

        public override string name { get { return "circle"; } }
        public override string shortcut { get { return "circ"; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdCircle() { }
        public override void Use(Player p, string message)
        {
            p.SendMessage("Select two bloxors to get yo ass rollin' on da rivah!");

        }
        /*
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, short type)
        {
            p.ClearBlockchange();
            short b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos bp = (CatchPos)p.blockchangeObject;
            bp.x = x; bp.y = y; bp.z = z; p.blockchangeObject = bp;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, short type)
        {
            p.ClearBlockchange();
            short b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos cpos = (CatchPos)p.blockchangeObject;
            if (cpos.type != -1) type = cpos.type;
            List<Pos> buffer = new List<Pos>();
            ushort xx; ushort yy; ushort zz;
            Bitmap b = new Bitmap(dif(cpos.x,x)+1,dif(cpos.y
                    buffer.Capacity = Math.Abs(cpos.x - x) * Math.Abs(cpos.y - y) * Math.Abs(cpos.z - z);
                    for (xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                        for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                            for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                            {
                                if (p.level.GetTile(xx, yy, zz) != type) { buffer.Add(new Pos(xx, yy, zz)); }
                            }
                    break;
            if (buffer.Count > p.group.maxBlocks && !p.ignorePermission)
            {
                Player.SendMessage(p, "You tried to cuboid " + buffer.Count + " blocks.");
                Player.SendMessage(p, "You cannot cuboid more than " + p.group.maxBlocks + ".");
                return;
            }

            Player.SendMessage(p, buffer.Count.ToString() + " blocks.");

            buffer.ForEach(delegate(Pos pos)
            {
                p.level.Blockchange(p, pos.x, pos.y, pos.z, type);
                MemoryCleaner.CleanAbout(p.level, pos.x, pos.y, pos.z);
                MemoryCleaner.CleanPortal(p.level, pos.x, pos.y, pos.z);
            });

            if (p.staticCommands) p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/circle [type] - Make a circle LIKE A BAWS");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                ""
            };
        }
        class Pos
        {
            public ushort x, y, z;
            public Pos(ushort X, ushort Y, ushort Z)
            {
                x = X;
                y = Y;
                z = Z;
            }
        }
        struct CatchPos
        {
            public short type;
            public ushort x, y, z;
        }
        public short dif(short s1, short s2)
        {
            return (short)Math.Abs(s1 - s2);
        }
         */
    }
}