using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdInstboid : Command
    {
        public override string name { get { return "instboid"; } }
        public override string shortcut { get { return "iz"; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public CmdInstboid() { }
        public override void Use(Player p, string message)
        {
            int number = message.Split(' ').Length;
            if (number > 2) { Help(p); return; }
            if (number == 2)
            {
                int pos = message.IndexOf(' ');
                string t = message.Substring(0, pos).ToLower();
                string s = message.Substring(pos + 1).ToLower();
                ushort type = Block.Number(t);
                if (type == 255) { Player.SendMessage(p, "There is no block \"" + t + "\"."); return; }
                if (!Block.canPlace(p, type) && !p.ignorePermission) { Player.SendMessage(p, "Cannot place that."); return; }

                SolidType solid;
                if (s == "solid") { solid = SolidType.solid; }
                else if (s == "hollow") { solid = SolidType.hollow; }
                else if (s == "walls") { solid = SolidType.walls; }
                else if (s == "holes") { solid = SolidType.holes; }
                else if (s == "wire") { solid = SolidType.wire; }
                else if (s == "random") { solid = SolidType.random; }
                else { Help(p); return; }
                CatchPos cpos; cpos.solid = solid; cpos.type = type;
                cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;
            }
            else if (message != "")
            {
                SolidType solid = SolidType.solid;
                message = message.ToLower();
                ushort type = Block.Zero;
                if (message == "solid") { solid = SolidType.solid; }
                else if (message == "hollow") { solid = SolidType.hollow; }
                else if (message == "walls") { solid = SolidType.walls; }
                else if (message == "holes") { solid = SolidType.holes; }
                else if (message == "wire") { solid = SolidType.wire; }
                else if (message == "random") { solid = SolidType.random; }
                else
                {
                    ushort t = Block.Number(message);
                    if (t == 255) { Player.SendMessage(p, "There is no block \"" + message + "\"."); return; }
                    if (!Block.canPlace(p, t)) { Player.SendMessage(p, "Cannot place that."); return; }

                    type = t;
                }
                CatchPos cpos; cpos.solid = solid; cpos.type = type;
                cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;
            }
            else
            {
                CatchPos cpos; cpos.solid = SolidType.solid; cpos.type = Block.Zero;
                cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;
            }
            Player.SendMessage(p, "Place two blocks to determine the edges.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/instboid - almost instant cuboid.");
            Player.SendMessage(p, "This should only be used when dealing with ENORMOUS cuboids.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos bp = (CatchPos)p.blockchangeObject;
            bp.x = x; bp.y = y; bp.z = z; p.blockchangeObject = bp;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos cpos = (CatchPos)p.blockchangeObject;
            if (cpos.type != Block.Zero) type = cpos.type; else type = p.bindings[type];
            List<Pos> buffer = new List<Pos>();

            ushort xx; ushort yy; ushort zz;

            switch (cpos.solid)
            {
                case SolidType.solid:
                    buffer.Capacity = Math.Abs(cpos.x - x) * Math.Abs(cpos.y - y) * Math.Abs(cpos.z - z);
                    for (xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                        for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                            for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                            {
                                if (p.level.GetTile(xx, yy, zz) != type) { BufferAdd(p, buffer, xx, yy, zz); }
                            }
                    break;
                case SolidType.hollow:
                    //todo work out if theres 800 blocks used before making the buffer
                    for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                        for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                        {
                            if (p.level.GetTile(cpos.x, yy, zz) != type) { BufferAdd(p, buffer, cpos.x, yy, zz); }
                            if (cpos.x != x) { if (p.level.GetTile(x, yy, zz) != type) { BufferAdd(p, buffer, x, yy, zz); } }
                        }
                    if (Math.Abs(cpos.x - x) >= 2)
                    {
                        for (xx = (ushort)(Math.Min(cpos.x, x) + 1); xx <= Math.Max(cpos.x, x) - 1; ++xx)
                            for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                            {
                                if (p.level.GetTile(xx, cpos.y, zz) != type) { BufferAdd(p, buffer, xx, cpos.y, zz); }
                                if (cpos.y != y) { if (p.level.GetTile(xx, y, zz) != type) { BufferAdd(p, buffer, xx, y, zz); } }
                            }
                        if (Math.Abs(cpos.y - y) >= 2)
                        {
                            for (xx = (ushort)(Math.Min(cpos.x, x) + 1); xx <= Math.Max(cpos.x, x) - 1; ++xx)
                                for (yy = (ushort)(Math.Min(cpos.y, y) + 1); yy <= Math.Max(cpos.y, y) - 1; ++yy)
                                {
                                    if (p.level.GetTile(xx, yy, cpos.z) != type) { BufferAdd(p, buffer, xx, yy, cpos.z); }
                                    if (cpos.z != z) { if (p.level.GetTile(xx, yy, z) != type) { BufferAdd(p, buffer, xx, yy, z); } }
                                }
                        }
                    }
                    break;
                case SolidType.walls:
                    for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                        for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                        {
                            if (p.level.GetTile(cpos.x, yy, zz) != type) { BufferAdd(p, buffer, cpos.x, yy, zz); }
                            if (cpos.x != x) { if (p.level.GetTile(x, yy, zz) != type) { BufferAdd(p, buffer, x, yy, zz); } }
                        }
                    if (Math.Abs(cpos.x - x) >= 2)
                    {
                        if (Math.Abs(cpos.z - z) >= 2)
                        {
                            for (xx = (ushort)(Math.Min(cpos.x, x) + 1); xx <= Math.Max(cpos.x, x) - 1; ++xx)
                                for (yy = (ushort)(Math.Min(cpos.y, y)); yy <= Math.Max(cpos.y, y); ++yy)
                                {
                                    if (p.level.GetTile(xx, yy, cpos.z) != type) { BufferAdd(p, buffer, xx, yy, cpos.z); }
                                    if (cpos.z != z) { if (p.level.GetTile(xx, yy, z) != type) { BufferAdd(p, buffer, xx, yy, z); } }
                                }
                        }
                    }
                    break;
                case SolidType.holes:
                    bool Checked = true, startZ, startY;

                    for (xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                    {
                        startY = Checked;
                        for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                        {
                            startZ = Checked;
                            for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                            {
                                Checked = !Checked;
                                if (Checked && p.level.GetTile(xx, yy, zz) != type) BufferAdd(p, buffer, xx, yy, zz);
                            } Checked = !startZ;
                        } Checked = !startY;
                    }
                    break;
                case SolidType.wire:
                    for (xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                    {
                        BufferAdd(p, buffer, xx, y, z);
                        BufferAdd(p, buffer, xx, y, cpos.z);
                        BufferAdd(p, buffer, xx, cpos.y, z);
                        BufferAdd(p, buffer, xx, cpos.y, cpos.z);
                    }
                    for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                    {
                        BufferAdd(p, buffer, x, yy, z);
                        BufferAdd(p, buffer, x, yy, cpos.z);
                        BufferAdd(p, buffer, cpos.x, yy, z);
                        BufferAdd(p, buffer, cpos.x, yy, cpos.z);
                    }
                    for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                    {
                        BufferAdd(p, buffer, x, y, zz);
                        BufferAdd(p, buffer, x, cpos.y, zz);
                        BufferAdd(p, buffer, cpos.x, y, zz);
                        BufferAdd(p, buffer, cpos.x, cpos.y, zz);
                    }
                    break;
                case SolidType.random:
                    Random rand = new Random();
                    for (xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                        for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                            for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                            {
                                if (rand.Next(1, 11) <= 5 && p.level.GetTile(xx, yy, zz) != type) { BufferAdd(p, buffer, xx, yy, zz); }
                            }
                    break;
            }

            Level lvl = p.level;
            if (buffer.Count < 10000)
            {
                Player.SendMessage(p, "There's under 10000 blocks in this cuboid...");
                Player.SendMessage(p, "using normal cuboid...");

                if (buffer.Count > p.group.maxBlocks && !p.ignorePermission)
                {
                    Player.SendMessage(p, "You tried to cuboid " + buffer.Count + " blocks.");
                    Player.SendMessage(p, "You cannot cuboid more than " + p.group.maxBlocks + ".");
                    return;
                }

                Player.SendMessage(p, buffer.Count + " blocks.");

                buffer.ForEach(delegate(Pos pos)
                {
                    lvl.Blockchange(p, pos.x, pos.y, pos.z, type, type != Block.air);
                });
                return;
            }

            if (buffer.Count > p.group.maxBlocks && !p.ignorePermission)
            {
                Player.SendMessage(p, "You tried to cuboid " + buffer.Count + " blocks.");
                Player.SendMessage(p, "You cannot cuboid more than " + p.group.maxBlocks + ".");
                return;
            }

            Player.SendMessage(p, buffer.Count + " blocks.");
            int onepercent = buffer.Count / 100;
            int at = 0;
            buffer.ForEach(delegate(Pos pos)
            {
                for (int i = onepercent > 5000 ? 5 : 10; i <= 100; i+= onepercent > 5000 ? 5 : 10)
                    if (onepercent * i == at)
                        Player.SendMessage(p, i + "% done.");
                lvl.Blockchange(p, pos.x, pos.y, pos.z, type, type != Block.air, true);
                at++;
            });
            Command.all.Find("reveal").Use(p, "all");
            if (p.staticCommands) p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        void BufferAdd(Player p, List<Pos> list, ushort x, ushort y, ushort z)
        {
            Pos pos; pos.x = x; pos.y = y; pos.z = z; list.Add(pos);
        }
        struct Pos
        {
            public ushort x, y, z;
        }
        struct CatchPos
        {
            public SolidType solid;
            public ushort type;
            public ushort x, y, z;
        }
        enum SolidType { solid, hollow, walls, holes, wire, random };
    }
}