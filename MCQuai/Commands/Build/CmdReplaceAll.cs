﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace MCRevive
{
    class CmdReplaceAll : Command
    {
        public override string name { get { return "replaceall"; } }
        public override string shortcut { get { return "ra"; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.Admin; } }
        public CmdReplaceAll() { }

        public override void Use(Player p, string message)
        {
            if (message.IndexOf(' ') == -1 || message.Split(' ').Length > 3) { Help(p); return; }
            if (p == null && message.Length != 3) { Help(p); return; }

            ushort b1, b2;
            Level foundLevel = (p != null) ? p.level : null;

            b1 = Block.Number(message.Split(' ')[0]);
            b2 = Block.Number(message.Split(' ')[1]);
            if (message.Split(' ').Length == 3)
                foundLevel = Level.Find(message.Split(' ')[2]);
            if (foundLevel == null)
                Player.SendMessage(p, "unable to find map: " + message.Split(' ')[3]);

            if (b1 == Block.Zero || b2 == Block.Zero) { Player.SendMessage(p, "Could not find specified blocks."); return; }
            if ((!Block.canPlace(p, b1) || !Block.canPlace(p, b2)) && !p.ignorePermission) { Player.SendMessage(p, "You are not allowed to place this block."); return; }
            ushort x, y, z;
            int currentBlock = 0;
            List<Pos> stored = new List<Pos>();
            Pos pos;

            foreach (byte b in foundLevel.blocks)
            {
                if (b == b1)
                {
                    foundLevel.IntToPos(currentBlock, out x, out y, out z);
                    pos.x = x; pos.y = y; pos.z = z;
                    stored.Add(pos);
                }
                currentBlock++;
            }

            if (stored.Count > (p.group.maxBlocks * 2) && !p.ignorePermission) { Player.SendMessage(p, "Cannot replace more than " + (p.group.maxBlocks * 2) + " blocks."); return; }

            Player.SendMessage(p, stored.Count + " blocks out of " + currentBlock + " were " + Block.Name(b1) + ".");

            stored.ForEach((Pos) =>
            {
                foundLevel.Blockchange(p, Pos.x, Pos.y, Pos.z, b2, b2 != Block.air);
            });
        }
        public struct Pos { public ushort x, y, z; }

        public override void Help(Player p)
        {
            Player.SendMessage(p, "/replaceall <block1> <block2> - Replaces all of <block1> with <block2> in a map");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/replaceall stone air'",
                "This will change all stone in the map into air."
            };
        }
    }
}
