using System;
using System.IO;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdFindcenter : Command
    {
        public override string name { get { return "findcenter"; } }
        public override string shortcut { get { return "fc"; } }
        public override string type { get { return "other"; } }
        public override int defaultRank { get { return LevelPermission.Builder; } }
        public CmdFindcenter() { }
        public override void Use(Player p, string message)
        {
            if (message != "") { Help(p); return; }
            
            Pos cpos; cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;
            Player.SendMessage(p, "Place two blocks to determine the edges.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/findcenter - Find the center between two given positions.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            Pos cpos = (Pos)p.blockchangeObject;
            cpos.x = x; cpos.y = y; cpos.z = z; p.blockchangeObject = cpos;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            Pos cpos = (Pos)p.blockchangeObject;

            // find center points
            double cx = Math.Floor(Convert.ToDouble(Math.Min(cpos.x, x) + Math.Max(cpos.x, x))) / 2;
            double cy = Math.Floor(Convert.ToDouble(Math.Min(cpos.y, y) + Math.Max(cpos.y, y))) / 2;
            double cz = Math.Floor(Convert.ToDouble(Math.Min(cpos.z, z) + Math.Max(cpos.z, z))) / 2;
            if (cx.ToString().IndexOf(".5") != -1)
                p.level.Blockchange(p, (ushort)(cx + .5 == (ushort)cx ? cx + 1 : cx - 1), (ushort)cy, (ushort)cz, Block.green, true);
            if (cy.ToString().IndexOf(".5") != -1)
                p.level.Blockchange(p, (ushort)cx, (ushort)(cy + .5 == (ushort)cy ? cy + 1 : cy - 1), (ushort)cz, Block.green, true);
            if (cz.ToString().IndexOf(".5") != -1)
                p.level.Blockchange(p, (ushort)cx, (ushort)cy, (ushort)(cz + .5 == (ushort)cy ? cz + 1 : cz - 1), Block.green, true);
            
            p.level.Blockchange(p, (ushort)cx, (ushort)cy, (ushort)cz, Block.green, true);

            // Jesbus ---> Repeat it with Ceil so the second center block can appear =D
            cx = Math.Ceiling(Convert.ToDouble(Math.Min(cpos.x, x) + Math.Max(cpos.x, x))) / 2;
            cy = Math.Ceiling(Convert.ToDouble(Math.Min(cpos.y, y) + Math.Max(cpos.y, y))) / 2;
            cz = Math.Ceiling(Convert.ToDouble(Math.Min(cpos.z, z) + Math.Max(cpos.z, z))) / 2;
            if (cx.ToString().IndexOf(".5") != -1)
                p.level.Blockchange(p, (ushort)(cx + .5 == (ushort)cx ? cx + 1 : cx - 1), (ushort)cy, (ushort)cz, Block.green, true);
            if (cy.ToString().IndexOf(".5") != -1)
                p.level.Blockchange(p, (ushort)cx, (ushort)(cy + .5 == (ushort)cy ? cy + 1 : cy - 1), (ushort)cz, Block.green, true);
            if (cz.ToString().IndexOf(".5") != -1)
                p.level.Blockchange(p, (ushort)cx, (ushort)cy, (ushort)(cz + .5 == (ushort)cy ? cz + 1 : cz - 1), Block.green, true);
            
            p.level.Blockchange(p, (ushort)cx, (ushort)cy, (ushort)cz, Block.green, true);
        }

        struct Pos
        {
            public ushort x, y, z;
        }
    }
}