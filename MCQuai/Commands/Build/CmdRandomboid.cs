﻿using System;
using System.IO;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdRandomboid : Command
    {
        public override string name { get { return "randomboid"; } }
        public override string shortcut { get { return "rb"; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdRandomboid() { }
        public override void Use(Player p, string message)
        {
            CatchPos cpos;
            cpos.types = new List<int>();
            if (message != "")
            {
                foreach (string typeStr in message.Split(' '))
                {
                    ushort type = Block.Number(typeStr);
                    cpos.types.Add(type);
                    if (type == 255) { Player.SendMessage(p, "There is no block \"" + typeStr + "\"."); return; }
                    if (!Block.canPlace(p, type) && !p.ignorePermission) { Player.SendMessage(p, "You are not allowed to place block " + Block.Name(type) + "."); return; }
                }
            }
            cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;
            Player.SendMessage(p, "Place two blocks to determine the edges.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/randomboid [block types] - Out of inspiration? Try this!");
            Player.SendMessage(p, "You can use any amount of block types. Even 0!");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/randomboid'",
                "Select 2 points.",
                "Everything in the area is now random blocks.",
                "&3'/randomboid red blue yellow'",
                "Select 2 points.",
                "Same as above, but only with red, blue or yellow."
            };
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos cpos = (CatchPos)p.blockchangeObject;
            cpos.x = x; cpos.y = y; cpos.z = z; p.blockchangeObject = cpos;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos cpos = (CatchPos)p.blockchangeObject;
            List<Pos> buffer = new List<Pos>();
            Level foundLevel = p.level;
            Random random = new Random();
            if (cpos.types.Count != 0)
            {
                for (ushort xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                {
                    for (ushort yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                    {
                        for (ushort zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                        {
                            buffer.Add(new Pos(xx, yy, zz, (ushort)cpos.types[RandomNumber(random, 0, cpos.types.Count - 1)]));
                        }
                    }
                }
            }
            else
            {
                for (ushort xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                {
                    for (ushort yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                    {
                        for (ushort zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                        {
                            buffer.Add(new Pos(xx, yy, zz, (byte)RandomNumber(random, 1, 49)));
                        }
                    }
                }
            }

            if (buffer.Count > p.group.maxBlocks && !p.ignorePermission)
            {
                Player.SendMessage(p, "You tried to cuboid " + buffer.Count + " blocks.");
                Player.SendMessage(p, "You can only cuboid " + p.group.maxBlocks + " blocks.");
                return;
            }

            buffer.ForEach((pos) =>
            {
                foundLevel.Blockchange(p, pos.x, pos.y, pos.z, pos.type, true);
            });
        }

        struct Pos
        {
            public ushort x, y, z;
            public ushort type;
            public Pos(ushort _x, ushort _y, ushort _z, ushort _type) { x = _x; y = _y; z = _z; type = _type; }
        }
        struct CatchPos
        {
            public ushort x, y, z;
            public List<int> types;
        }
        private int RandomNumber(Random random, int min, int max)
        {
            if (max != 49) return random.Next(min, max);
            int num;
            do
            {
                num = random.Next(min, max);
            }
            while (num == 8 || num == 10 || num == 7); // Blocks that should not be made with randomboid.
            return num;
        }
    }
}