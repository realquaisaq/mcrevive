﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;

namespace MCRevive
{
    public class CmdPortal : Command
    {
        public struct portalPos { public List<Level.PortalPos> port; public ushort type; public bool Multi; public portalPos(ushort Type, bool multi) { port = new List<Level.PortalPos>(); type = Type; Multi = multi; } }
        
        public override string name { get { return "portal"; } }
        public override string shortcut { get { return "o"; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdPortal() { }
        public override void Use(Player p, string message)
        {
            portalPos portalPos = new portalPos(Block.blue_portal, false);
            message = message.ToLower();

            if (message.IndexOf(' ') != -1)
                if (message.Split(' ')[1] == "multi")
                {
                    portalPos.Multi = true;
                    message = message.Split(' ')[0];
                }
                else
                {
                    Player.SendMessage(p, "Invalid parameters");
                    return;
                }

            if (message == "orange") { portalPos.type = Block.orange_portal; }
            else if (message == "air") { portalPos.type = Block.air_portal; }
            else if (message == "water") { portalPos.type = Block.water_portal; }
            else if (message == "lava") { portalPos.type = Block.lava_portal; }
            else if (message != "blue" && message != "") { Help(p); return; }

            p.ClearBlockchange();
            p.blockchangeObject = portalPos;
            Player.SendMessage(p, "Place the &aEntry block" + ((portalPos.Multi) ? "s" : "") + Server.DefaultColor + " for the portal");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(EntryChange);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/portal [type] - Activates Portal mode.");
            Player.SendMessage(p, "/portal <type> <'multi'> - Place Entry blocks until exit is wanted.");
            Player.SendMessage(p, "Available types: orange, blue, air, water, lava.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            {
                "&3'/portal' or '/portal blue' or '/portal orange'",
                "Select a block.",
                "Select another block.",
                "The block is now a portal block.",
                "If you click the first block, you will get teleported to the second.",
                "",
                "&3'/portal air' or '/portal water' or '/portal lava'",
                "Select a block.",
                "Select another block.",
                "if you walk into the first block, you will get teleported to the second.",
                "",
                "&3'/portal <any type, or nothing> multi'",
                "Keep placing blocks, untill you have covered the portal area.",
                "When you are done placing portals, make the exit.",
                "You make the exit by placing a red wool block."
            };
        }

        public void EntryChange(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            portalPos pP = (portalPos)p.blockchangeObject;
            if (pP.Multi && type == Block.red && pP.port.Count > 0) { ExitChange(p, x, y, z, type, 1); return; }

            Level.PortalPos Port = new Level.PortalPos(p.level.PosToInt(x, y, z), -1, "");

            ushort b = p.level.GetTile(x, y, z);
            p.level.Blockchange(p, x, y, z, !pP.port.Contains(Port) ? pP.type : b, action == 1);
            if (!pP.port.Contains(Port))
            {
                p.SendBlockchange(x, y, z, Block.green);
                pP.port.Add(Port);
            }
            else
                pP.port.Remove(Port);
            p.blockchangeObject = pP;

            if (!pP.Multi && action != 0)
            {
                p.Blockchange += new Player.BlockchangeEventHandler(ExitChange);
                Player.SendMessage(p, "&aEntry block placed.");
            }
            else if (action != 0)
            {
                p.Blockchange += new Player.BlockchangeEventHandler(EntryChange);
                Player.SendMessage(p, "&aEntry block placed. &cRed block for exit.");
            }
            else { p.Blockchange += new Player.BlockchangeEventHandler(EntryChange); }
        }
        public void ExitChange(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            portalPos pP = (portalPos)p.blockchangeObject;
            int pos = p.level.PosToInt(x, y, z);
            pP.port.ForEach((pp) => 
            {
                pp.end = p.level.name;
                pp.pos2 = pos;

                ushort xx, yy, zz;
                p.level.IntToPos(pp.pos, out xx, out yy, out zz);

                p.SendBlockchange(xx, yy, zz, p.level.GetTile(pp.pos));
            });

            Memory.SavePortals(p.level.name, pP.port);

            Player.SendMessage(p, "&3Exit" + Server.DefaultColor + " block placed");
            pP.port.Clear();

            if (p.staticCommands) { p.blockchangeObject = pP; p.Blockchange += new Player.BlockchangeEventHandler(EntryChange); }
        }
    }
}