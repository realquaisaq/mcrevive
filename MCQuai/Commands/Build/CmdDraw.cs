﻿using System;
using System.Collections.Generic;

namespace MCRevive
{
    class CmdDraw : Command
    {
        public override string name { get { return "draw"; } }
        public override string shortcut { get { return ""; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdDraw() { }
        public override void Use(Player p, string message)
        {
            message = message.ToLower();
            if (message == "" || message.Split(' ').Length > 2) { Help(p); return; }
            CatchPos cp = new CatchPos();
            if (message.IndexOf(' ') != -1)
            {
                cp.b = Block.Number(message.Split(' ')[1]);
                if (cp.b == Block.Zero) { Player.SendMessage(p, "There is no block: " + message.Split(' ')[1] + "."); return; }
            }
            else
                cp.b = Block.Zero;
            string type = message.Split(' ')[0];
            if (type == "circle") { Command.all.Find("circle").Use(p, message.Substring(message.IndexOf(' ') + 1)); return; }
            else if (type == "ellipse") cp.type = DrawType.Ellipse;
            else if (type == "sphere") { Command.all.Find("sphereoid").Use(p, message.Substring(message.IndexOf(' ') + 1)); return; }
            else if (type == "cube") { Command.all.Find("cuboid").Use(p, message.Substring(message.IndexOf(' ') + 1)); return; }
            else { Player.SendMessage(p, "The specified draw type doesn't exist!"); return; }

            p.blockchangeObject = cp;
            Player.SendMessage(p, "Place two blocks to determine the edges.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/draw <type> [info] - Draw <type> into the game.");
            Player.SendMessage(p, "Available types are: Circle, Ellipse, Sphere, Cube.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            { 
                ""
            };
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos bp = (CatchPos)p.blockchangeObject;
            bp.x = x; bp.y = y; bp.z = z; p.blockchangeObject = bp;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            List<Pos> buffer = new List<Pos>();
            Level foundLevel = p.level;
            CatchPos cpos = (CatchPos)p.blockchangeObject;
            if (cpos.b != Block.Zero) type = cpos.b;

            ushort mix = Math.Min(x, cpos.x), max = Math.Max(x, cpos.x), xdiff = (ushort)Math.Abs(max - mix);
            ushort miy = Math.Min(y, cpos.y), may = Math.Max(y, cpos.y), ydiff = (ushort)Math.Abs(may - miy);
            ushort miz = Math.Min(z, cpos.z), maz = Math.Max(z, cpos.z), zdiff = (ushort)Math.Abs(maz - miz);

            int steps = 30;
            bool[] first = { true, true }; // first removal X, first removal Z
            double Xradius = (double)(xdiff / 2);
            double Zradius = (double)(zdiff / 2);
            double centerX = (double)(mix + Xradius);
            double centerZ = (double)(miz + Zradius);
            ushort lastX = (ushort)(centerX + Xradius), nextX;
            ushort lastZ = (ushort)centerZ, nextZ;
            for (ushort yy = miy; yy <= may; ++yy)
                for (int i = 1; i <= steps; i++)
                {
                    nextX = (ushort)(centerX + (double)(Math.Cos(Math.PI * 2 / steps * i) * Xradius));
                    nextZ = (ushort)(centerZ + (double)(Math.Sin(Math.PI * 2 / steps * i) * Zradius));
                    Line(type, buffer, foundLevel, lastX, yy, lastZ, nextX, yy, nextZ);
                    lastX = nextX;
                    lastZ = nextZ;
                }

            if (buffer.Count * (ydiff + 1) > p.group.maxBlocks && !p.ignorePermission)
            {
                Player.SendMessage(p, "You tried to draw " + buffer.Count * (ydiff + 1) + " blocks.");
                Player.SendMessage(p, "You cannot draw more than " + p.group.maxBlocks + " blocks.");
                return;
            }
            Player.SendMessage(p, buffer.Count * (ydiff + 1) + " blocks.");

            buffer.ForEach((Pos) =>
            {
                for (ushort yy = miy; yy <= may; yy++)
                {
                    p.level.Blockchange(p, Pos.x, yy, Pos.z, type, type != Block.air);
                }
            });
            if (p.staticCommands) p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        void Line(ushort type, List<Pos> buffer, Level foundLevel, ushort x, ushort y, ushort z, ushort _x, ushort _y, ushort _z)
        {
            Pos pos = new Pos();
            int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc, err_1, err_2, dx2, dy2, dz2;
            int[] pixel = new int[3];

            pixel[0] = _x; pixel[1] = _y; pixel[2] = _z;
            dx = x - _x; dy = y - _y; dz = z - _z;

            x_inc = (dx < 0) ? -1 : 1; l = Math.Abs(dx);
            y_inc = (dy < 0) ? -1 : 1; m = Math.Abs(dy);
            z_inc = (dz < 0) ? -1 : 1; n = Math.Abs(dz);

            dx2 = l << 1; dy2 = m << 1; dz2 = n << 1;

            if ((l >= m) && (l >= n))
            {
                err_1 = dy2 - l;
                err_2 = dz2 - l;
                for (i = 0; i < l; i++)
                {
                    pos.x = (ushort)pixel[0];
                    pos.y = (ushort)pixel[1];
                    pos.z = (ushort)pixel[2];
                    if (foundLevel.GetTile(pos.x, pos.y, pos.z) != type) buffer.Add(pos);

                    if (err_1 > 0)
                    {
                        pixel[1] += y_inc;
                        err_1 -= dx2;
                    }
                    if (err_2 > 0)
                    {
                        pixel[2] += z_inc;
                        err_2 -= dx2;
                    }
                    err_1 += dy2;
                    err_2 += dz2;
                    pixel[0] += x_inc;
                }
            }
            else if ((m >= l) && (m >= n))
            {
                err_1 = dx2 - m;
                err_2 = dz2 - m;
                for (i = 0; i < m; i++)
                {
                    pos.x = (ushort)pixel[0];
                    pos.y = (ushort)pixel[1];
                    pos.z = (ushort)pixel[2];
                    if (foundLevel.GetTile(pos.x, pos.y, pos.z) != type) buffer.Add(pos);

                    if (err_1 > 0)
                    {
                        pixel[0] += x_inc;
                        err_1 -= dy2;
                    }
                    if (err_2 > 0)
                    {
                        pixel[2] += z_inc;
                        err_2 -= dy2;
                    }
                    err_1 += dx2;
                    err_2 += dz2;
                    pixel[1] += y_inc;
                }
            }
            else
            {
                err_1 = dy2 - n;
                err_2 = dx2 - n;
                for (i = 0; i < n; i++)
                {
                    pos.x = (ushort)pixel[0];
                    pos.y = (ushort)pixel[1];
                    pos.z = (ushort)pixel[2];
                    if (foundLevel.GetTile(pos.x, pos.y, pos.z) != type) buffer.Add(pos);

                    if (err_1 > 0)
                    {
                        pixel[1] += y_inc;
                        err_1 -= dz2;
                    }
                    if (err_2 > 0)
                    {
                        pixel[0] += x_inc;
                        err_2 -= dz2;
                    }
                    err_1 += dy2;
                    err_2 += dx2;
                    pixel[2] += z_inc;
                }
            }

            pos.x = (ushort)pixel[0];
            pos.y = (ushort)pixel[1];
            pos.z = (ushort)pixel[2];
            if (foundLevel.GetTile(pos.x, pos.y, pos.z) != type) buffer.Add(pos);
        }
        struct Pos { public ushort x, y, z; public Pos(ushort X, ushort Y, ushort Z) { x = X; y = Y; z = Z; } }
        enum DrawType { Circle, Ellipse, Sphere, Cube };
        struct CatchPos
        {
            public ushort b;
            public ushort x, y, z;
            public DrawType type;
        }
    }
}
