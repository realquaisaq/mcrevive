﻿
using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdSpheroid : Command
    {
        public override string name { get { return "spheroid"; } }
        public override string shortcut { get { return "e"; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.AdvBuilder; } }
        public CmdSpheroid() { }
        public override void Use(Player p, string message)
        {
            CatchPos cpos;

            cpos.x = 0; cpos.y = 0; cpos.z = 0;
            if (message.ToLower() == "vertical") message = "circle";
            if (message == "")
            {
                cpos.type = Block.Zero;
                cpos.solid = SolidType.solid;
            }
            else if (message.IndexOf(' ') == -1)
            {
                cpos.type = Block.Number(message);
                if ((cpos.type == Block.Zero || !Block.canPlace(p, cpos.type)) && !p.ignorePermission) { Player.SendMessage(p, "You are not allowed to place that."); return; }
                cpos.solid = SolidType.solid;
                if (cpos.type == Block.Zero)
                {
                    if (message.ToLower() == "circle")
                        cpos.solid = SolidType.circle;
                    else if (message.ToLower() == "hollow")
                        cpos.solid = SolidType.hollow;
                    else
                    {
                        Help(p);
                        return;
                    }
                }
            }
            else
            {
                cpos.type = Block.Number(message.Split(' ')[0]);
                cpos.solid = (message.Split(' ')[1].ToLower() == "circle") ? SolidType.circle : (message.Split(' ')[1].ToLower() == "hollow") ? SolidType.hollow : SolidType.solid;
                if ((cpos.type == Block.Zero) || ((message.Split(' ')[1].ToLower() != "circle" && message.Split(' ')[1].ToLower() != "hollow" && message.Split(' ')[1].ToLower() != "solid")))
                {
                    Help(p);
                    return;
                }
            }
            p.blockchangeObject = cpos;

            Player.SendMessage(p, "Place two blocks to determine the edges.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/spheroid [block] [type] - Create a sphere of blocks.");
            Player.SendMessage(p, "available types are: Solid, Hollow, Circle.");
        }
        public override string[] Example(Player p)
        {
            return new string[]
            { 
                "&3'/spheroid' or '/spheroid solid'" ,
                "This will create a sphere between 2 placed blocks.",
                "The block will be the one in your hand.",
                "&3'/spheroid stone' or '/spheroid stone solid'",
                "This will create a sphere of stone.",
                "&3'/spheroid hollow'",
                "Creates a hollow sphere between 2 placed blocks.",
                "&3'/spheroid stone hollow'",
                "Creates a hollow sphere of stone between 2 placed blocks.",
                "&3'/spheroid circle'",
                "This will create a circle between 2 placed blocks.",
                "&3'/spheroid stone circle'",
                "Creates a circle of stone between 2 placed blocks."
            }; 
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos bp = (CatchPos)p.blockchangeObject;
            bp.x = x; bp.y = y; bp.z = z; p.blockchangeObject = bp;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            Level foundLevel = p.level;
            CatchPos cpos = (CatchPos)p.blockchangeObject;
            if (cpos.type != Block.Zero) { type = cpos.type; }
            List<Pos> buffer = new List<Pos>();

            if (cpos.solid == SolidType.solid)
            {
                /* Courtesy of fCraft's awesome Open-Source'ness :D */

                // find start/end coordinates
                int sx = Math.Min(cpos.x, x);
                int ex = Math.Max(cpos.x, x);
                int sy = Math.Min(cpos.y, y);
                int ey = Math.Max(cpos.y, y);
                int sz = Math.Min(cpos.z, z);
                int ez = Math.Max(cpos.z, z);

                // find axis lengths
                double rx = (ex - sx + 1) / 2 + .25;
                double ry = (ey - sy + 1) / 2 + .25;
                double rz = (ez - sz + 1) / 2 + .25;

                double rx2 = 1 / (rx * rx);
                double ry2 = 1 / (ry * ry);
                double rz2 = 1 / (rz * rz);

                // find center points
                double cx = (ex + sx) / 2;
                double cy = (ey + sy) / 2;
                double cz = (ez + sz) / 2;

                for (int xx = sx; xx <= ex; xx += 8)
                    for (int yy = sy; yy <= ey; yy += 8)
                        for (int zz = sz; zz <= ez; zz += 8)
                            for (int z3 = 0; z3 < 8 && zz + z3 <= ez; z3++)
                                for (int y3 = 0; y3 < 8 && yy + y3 <= ey; y3++)
                                    for (int x3 = 0; x3 < 8 && xx + x3 <= ex; x3++)
                                    {
                                        // get relative coordinates
                                        double dx = (xx + x3 - cx);
                                        double dy = (yy + y3 - cy);
                                        double dz = (zz + z3 - cz);

                                        // test if it's inside ellipse
                                        if ((dx * dx) * rx2 + (dy * dy) * ry2 + (dz * dz) * rz2 <= 1)
                                        {
                                            buffer.Add(new Pos((ushort)(x3 + xx), (ushort)(yy + y3), (ushort)(zz + z3)));
                                        }
                                    }
                if (buffer.Count > p.group.maxBlocks && !p.ignorePermission)
                {
                    Player.SendMessage(p, "You tried to spheroid " + buffer.Count + " blocks.");
                    Player.SendMessage(p, "You cannot spheroid more than " + p.group.maxBlocks + " blocks.");
                    return;
                }
                Player.SendMessage(p, buffer.Count + " blocks.");

                buffer.ForEach((pos) =>
                {
                    foundLevel.Blockchange(p, pos.x, pos.y, pos.z, type, type != Block.air);
                });
            }
            else if (cpos.solid == SolidType.circle)
            {
                int radius = Math.Abs(cpos.x - x) / 2;
                int f = 1 - radius;
                int ddF_x = 1;
                int ddF_y = -2 * radius;
                int xx = 0;
                int zz = radius;

                int x0 = Math.Min(cpos.x, x) + radius;
                int z0 = Math.Min(cpos.z, z) + radius;

                Pos pos = new Pos();
                pos.x = (ushort)x0; pos.z = (ushort)(z0 + radius); buffer.Add(pos);
                pos.z = (ushort)(z0 - radius); buffer.Add(pos);
                pos.x = (ushort)(x0 + radius); pos.z = (ushort)z0; buffer.Add(pos);
                pos.x = (ushort)(x0 - radius); buffer.Add(pos);

                while (xx < zz)
                {
                    if (f >= 0)
                    {
                        zz--;
                        ddF_y += 2;
                        f += ddF_y;
                    }
                    xx++;
                    ddF_x += 2;
                    f += ddF_x;

                    pos.z = (ushort)(z0 + zz);
                    pos.x = (ushort)(x0 + xx); buffer.Add(pos);
                    pos.x = (ushort)(x0 - xx); buffer.Add(pos);
                    pos.z = (ushort)(z0 - zz);
                    pos.x = (ushort)(x0 + xx); buffer.Add(pos);
                    pos.x = (ushort)(x0 - xx); buffer.Add(pos);
                    pos.z = (ushort)(z0 + xx);
                    pos.x = (ushort)(x0 + zz); buffer.Add(pos);
                    pos.x = (ushort)(x0 - zz); buffer.Add(pos);
                    pos.z = (ushort)(z0 - xx);
                    pos.x = (ushort)(x0 + zz); buffer.Add(pos);
                    pos.x = (ushort)(x0 - zz); buffer.Add(pos);
                }

                int ydiff = Math.Abs(y - cpos.y) + 1;

                if (buffer.Count * ydiff > p.group.maxBlocks && !p.ignorePermission)
                {
                    Player.SendMessage(p, "You tried to spheroid " + buffer.Count * ydiff + " blocks.");
                    Player.SendMessage(p, "You cannot spheroid more than " + p.group.maxBlocks + " blocks.");
                    return;
                }
                Player.SendMessage(p, buffer.Count * ydiff + " blocks.");

                buffer.ForEach((Pos) =>
                {
                    for (ushort yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); yy++)
                        foundLevel.Blockchange(p, Pos.x, yy, Pos.z, type, type != Block.air);
                });
            }
            else if (cpos.solid == SolidType.hollow)
            {
                // find start/end coordinates
                ushort sx = Math.Min(cpos.x, x);
                ushort ex = Math.Max(cpos.x, x);
                ushort sy = Math.Min(cpos.y, y);
                ushort ey = Math.Max(cpos.y, y);
                ushort sz = Math.Min(cpos.z, z);
                ushort ez = Math.Max(cpos.z, z);

                double rx = (ex - sx) / 2;
                double ry = (ey - sy) / 2;
                double rz = (ez - sz) / 2;
                
                double cx = (ex + sx) / 2;
                double cy = (ey + sy) / 2;
                double cz = (ez + sz) / 2;
                bool[,,] datpos = new bool[p.level.width, p.level.height, p.level.depth];
                for (int hh = 0; hh < 360; hh++)
                {
                    double x1 = cx + Math.Cos((double)hh) * rx;
                    double y1 = cy + Math.Sin((double)hh) * ry;
                    double z1 = cz;
                    for (int v = 0; v < 360; v++)
                    {
                        ushort x2 = (ushort)(cx * Math.Cos((double)v) - x1 * Math.Cos((double)v) + cx);
                        ushort y2 = (ushort)(cy * Math.Cos((double)v) - y1 * Math.Cos((double)v) + cy + 1);
                        ushort z2 = (ushort)(z1 + Math.Sin((double)v) * rz);
                        if (!datpos[x2, y2, z2]) { buffer.Add(new Pos(x2, y2, z2)); datpos[x2, y2, z2] = true; }
                    }
                }

                if (buffer.Count > p.group.maxBlocks && !p.ignorePermission)
                {
                    Player.SendMessage(p, "You tried to spheroid " + buffer.Count + " blocks.");
                    Player.SendMessage(p, "You cannot spheroid more than " + p.group.maxBlocks + " blocks.");
                    return;
                }
                Player.SendMessage(p, buffer.Count + " blocks.");
                buffer.ForEach((pos) =>
                {
                    foundLevel.Blockchange(p, pos.x, pos.y, pos.z, type, type != Block.air);
                });
            }
            else
            {
                Help(p);
                return;
            }
            if (p.staticCommands) p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }

        struct Pos { public ushort x, y, z; public Pos(ushort X, ushort Y, ushort Z) { x = X; y = Y; z = Z; } }
        enum SolidType { solid, hollow, circle };
        struct CatchPos
        {
            public ushort type;
            public ushort x, y, z;
            public SolidType solid;
        }
    }
}