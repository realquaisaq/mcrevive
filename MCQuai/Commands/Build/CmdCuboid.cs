/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;

namespace MCRevive
{
    public class CmdCuboid : Command
    {
        public override string name { get { return "cuboid"; } }
        public override string shortcut { get { return "z"; } }
        public override string type { get { return "build"; } }
        public override int defaultRank { get { return LevelPermission.Builder; } }
        public CmdCuboid() { }
        public override void Use(Player p, string message)
        {
            int number = message.Split(' ').Length;
            if (number > 2) { Help(p); return; }
            if (number == 2)
            {
                int pos = message.IndexOf(' ');
                string t = message.Substring(0, pos).ToLower();
                string s = message.Substring(pos + 1).ToLower();
                ushort type = Block.Number(t);
                if (type == Block.Zero) { Player.SendMessage(p, "There is no block \"" + t + "\"."); return; }
                if (!Block.canPlace(p, type) && !p.ignorePermission) { Player.SendMessage(p, "Cannot place that."); return; }

                SolidType solid;
                if (s == "solid") { solid = SolidType.solid; }
                else if (s == "hollow") { solid = SolidType.hollow; }
                else if (s == "walls") { solid = SolidType.walls; }
                else if (s == "holes") { solid = SolidType.holes; }
                else if (s == "wire") { solid = SolidType.wire; }
                else if (s == "random") { solid = SolidType.random; }
                else { Help(p); return; }
                CatchPos cpos; cpos.solid = solid; cpos.type = type;
                cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;
            }
            else if (message != "")
            {
                SolidType solid = SolidType.solid;
                message = message.ToLower();
                ushort type = Block.Zero;
                if (message == "solid") { solid = SolidType.solid; }
                else if (message == "hollow") { solid = SolidType.hollow; }
                else if (message == "walls") { solid = SolidType.walls; }
                else if (message == "holes") { solid = SolidType.holes; }
                else if (message == "wire") { solid = SolidType.wire; }
                else if (message == "random") { solid = SolidType.random; }
                else
                {
                    ushort t = Block.Number(message);
                    if (t == 255) { Player.SendMessage(p, "There is no block \"" + message + "\"."); return; }
                    if (!Block.canPlace(p, t) && !p.ignorePermission) { Player.SendMessage(p, "Cannot place that."); return; }

                    type = t;
                }
                CatchPos cpos; cpos.solid = solid; cpos.type = type;
                cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;
            }
            else
            {
                CatchPos cpos; cpos.solid = SolidType.solid; cpos.type = Block.Zero;
                cpos.x = 0; cpos.y = 0; cpos.z = 0; p.blockchangeObject = cpos;
            }
            Player.SendMessage(p, "Place two blocks to determine the edges.");
            p.ClearBlockchange();
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/cuboid [type] <solid/hollow/walls/holes/wire/random> - creates a cuboid of blocks.");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
        public void Blockchange1(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
            p.ClearBlockchange();
            ushort b = p.level.GetTile(x, y, z);
            p.SendBlockchange(x, y, z, b);
            CatchPos bp = (CatchPos)p.blockchangeObject;
            bp.x = x; bp.y = y; bp.z = z; p.blockchangeObject = bp;
            p.Blockchange += new Player.BlockchangeEventHandler(Blockchange2);
        }
        public void Blockchange2(Player p, ushort x, ushort y, ushort z, ushort type, byte action)
        {
                p.ClearBlockchange();
                ushort b = p.level.GetTile(x, y, z);
                p.SendBlockchange(x, y, z, b);
                CatchPos cpos = (CatchPos)p.blockchangeObject;
                if (cpos.type != Block.Zero) type = cpos.type;
                List<Pos> buffer = new List<Pos>();
                ushort xx; ushort yy; ushort zz;

                switch (cpos.solid)
                {
                    case SolidType.solid:
                        buffer.Capacity = Math.Abs(cpos.x - x) * Math.Abs(cpos.y - y) * Math.Abs(cpos.z - z);
                        for (xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                            for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                                for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                                {
                                    if (p.level.GetTile(xx, yy, zz) != type) { buffer.Add(new Pos(xx, yy, zz)); }
                                }
                        break;
                    case SolidType.hollow:
                        //todo work out if theres 800 blocks used before making the buffer
                        for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                            for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                            {
                                if (p.level.GetTile(cpos.x, yy, zz) != type) { buffer.Add(new Pos(cpos.x, yy, zz)); }
                                if (cpos.x != x) { if (p.level.GetTile(x, yy, zz) != type) { buffer.Add(new Pos(x, yy, zz)); } }
                            }
                        if (Math.Abs(cpos.x - x) >= 2)
                        {
                            for (xx = (ushort)(Math.Min(cpos.x, x) + 1); xx <= Math.Max(cpos.x, x) - 1; ++xx)
                                for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                                {
                                    if (p.level.GetTile(xx, cpos.y, zz) != type) { buffer.Add(new Pos(xx, cpos.y, zz)); }
                                    if (cpos.y != y) { if (p.level.GetTile(xx, y, zz) != type) { buffer.Add(new Pos(xx, y, zz)); } }
                                }
                            if (Math.Abs(cpos.y - y) >= 2)
                            {
                                for (xx = (ushort)(Math.Min(cpos.x, x) + 1); xx <= Math.Max(cpos.x, x) - 1; ++xx)
                                    for (yy = (ushort)(Math.Min(cpos.y, y) + 1); yy <= Math.Max(cpos.y, y) - 1; ++yy)
                                    {
                                        if (p.level.GetTile(xx, yy, cpos.z) != type) { buffer.Add(new Pos(xx, yy, cpos.z)); }
                                        if (cpos.z != z) { if (p.level.GetTile(xx, yy, z) != type) { buffer.Add(new Pos(xx, yy, z)); } }
                                    }
                            }
                        }
                        break;
                    case SolidType.walls:
                        for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                            for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                            {
                                if (p.level.GetTile(cpos.x, yy, zz) != type) { buffer.Add(new Pos(cpos.x, yy, zz)); }
                                if (cpos.x != x) { if (p.level.GetTile(x, yy, zz) != type) { buffer.Add(new Pos(x, yy, zz)); } }
                            }
                        if (Math.Abs(cpos.x - x) >= 2)
                        {
                            if (Math.Abs(cpos.z - z) >= 2)
                            {
                                for (xx = (ushort)(Math.Min(cpos.x, x) + 1); xx <= Math.Max(cpos.x, x) - 1; ++xx)
                                    for (yy = (ushort)(Math.Min(cpos.y, y)); yy <= Math.Max(cpos.y, y); ++yy)
                                    {
                                        if (p.level.GetTile(xx, yy, cpos.z) != type) { buffer.Add(new Pos(xx, yy, cpos.z)); }
                                        if (cpos.z != z) { if (p.level.GetTile(xx, yy, z) != type) { buffer.Add(new Pos(xx, yy, z)); } }
                                    }
                            }
                        }
                        break;
                    case SolidType.holes:
                        bool Checked = true, startZ, startY;

                        for (xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                        {
                            startY = Checked;
                            for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                            {
                                startZ = Checked;
                                for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                                {
                                    Checked = !Checked;
                                    if (Checked && p.level.GetTile(xx, yy, zz) != type) buffer.Add(new Pos(xx, yy, zz));
                                } 
                                Checked = !startZ;
                            } 
                            Checked = !startY;
                        }
                        break;
                    case SolidType.wire:
                        for (xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                        {
                            buffer.Add(new Pos(xx, y, z));
                            buffer.Add(new Pos(xx, y, cpos.z));
                            buffer.Add(new Pos(xx, cpos.y, z));
                            buffer.Add(new Pos(xx, cpos.y, cpos.z));
                        }
                        for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                        {
                            buffer.Add(new Pos(x, yy, z));
                            buffer.Add(new Pos(x, yy, cpos.z));
                            buffer.Add(new Pos(cpos.x, yy, z));
                            buffer.Add(new Pos(cpos.x, yy, cpos.z));
                        }
                        for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                        {
                            buffer.Add(new Pos(x, y, zz));
                            buffer.Add(new Pos(x, cpos.y, zz));
                            buffer.Add(new Pos(cpos.x, y, zz));
                            buffer.Add(new Pos(cpos.x, cpos.y, zz));
                        }
                        break;
                    case SolidType.random:
                        Random rand = new Random();
                        for (xx = Math.Min(cpos.x, x); xx <= Math.Max(cpos.x, x); ++xx)
                            for (yy = Math.Min(cpos.y, y); yy <= Math.Max(cpos.y, y); ++yy)
                                for (zz = Math.Min(cpos.z, z); zz <= Math.Max(cpos.z, z); ++zz)
                                {
                                    if (rand.Next(1, 11) <= 5 && p.level.GetTile(xx, yy, zz) != type) { buffer.Add(new Pos(xx, yy, zz)); }
                                }
                        break;
                }

                if (buffer.Count > p.group.maxBlocks && !p.ignorePermission)
                {
                    Player.SendMessage(p, "You tried to cuboid " + buffer.Count + " blocks.");
                    Player.SendMessage(p, "You cannot cuboid more than " + p.group.maxBlocks + ".");
                    return;
                }

                Player.SendMessage(p, buffer.Count.ToString() + " blocks.");

                buffer.ForEach(delegate(Pos pos)
                {
                    p.level.Blockchange(p, pos.x, pos.y, pos.z, type, type != Block.air);
                });

            if (p.staticCommands) p.Blockchange += new Player.BlockchangeEventHandler(Blockchange1);
        }
        class Pos
        {
            public ushort x, y, z;
            public Pos(ushort X, ushort Y, ushort Z)
            {
                x = X;
                y = Y;
                z = Z;
            }
        }
        struct CatchPos
        {
            public SolidType solid;
            public ushort type;
            public ushort x, y, z;
        }
        enum SolidType { solid, hollow, walls, holes, wire, random };
    }
}