//________________________________________\\
// made by OMARATION, for MCRevive && MCKat \\  
//----------------------------------------\\
//    modified by Quaisaq, now it works   \\
//________________________________________\\

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MCRevive
{
    public class Cmdlsurv : Command
    {
        public override string name { get { return "lavasurvival"; } }
        public override string shortcut { get { return "ls"; } }
        public override string type { get { return "game"; } }
        public override int defaultRank { get { return LevelPermission.Operator; } }
        public override void Use(Player p, string message)
        {
            string[] param = message.Split(' ');
            if (message == "" || param.Length > 3 || param.Length < 2) { Help(p); return; }
            if (param[0] == "map")
            {
                if (param.Length != 3) { Help(p); return; }
                string lvl = param[2].ToLower();
                p.ignorePermission = true;
                if (param[1] == "small")
                {
                    Command.all.Find("newlvl").Use(p, lvl + " 32 64 32 mountains");
                }
                else if (param[1] == "medium")
                {
                    Command.all.Find("newlvl").Use(p, lvl + " 64 64 64 mountains");
                }
                else if (param[1] == "large")
                {
                    Command.all.Find("newlvl").Use(p, lvl + " 128 64 128 mountains");
                }
                else { Help(p); p.ignorePermission = false; return; }
                p.ignorePermission = false;
                Command.all.Find("load").Use(p, lvl);
                Command.all.Find("perbuild").Use(p, lvl + " Operator");
                Player.GlobalMessage("a " + param[1] + " lavasurvival map have been created with the name &2" + lvl + ".");
                return;
            }
            else if (param[0] == "start")
            {
                if (param.Length == 2) { message += " "; param = message.Split(' '); }
                if (param.Length == 3)
                {
                    if (param[2] == "") param[2] = "5";
                    Level lvl = Level.Find(param[1].ToLower());
                    if (lvl == null) { Player.SendMessage(p, "level: " + param[1] + " does not exist, or is unloaded."); return; }
                    if (games.IndexOf(games.Find(g => g.level == lvl)) != -1)
                    {
                        Player.SendMessage(p, "Theres already a game going on, wait for it to finish");
                        return;
                    }
                    double minutes = 5;
                    try { minutes = double.Parse(param[2].Replace(",", ".")); }
                    catch { Player.SendMessage(p, "Unable to read minutes."); return; }
                    if (minutes < 5) { Player.SendMessage(p, "The game has to be atleast 5 minutes long"); return; }

                    if (p.level != lvl) Command.all.Find("goto").Use(p, lvl.name);
                    while (p.Loading) { }

                    LavaSurvivalMemory lsm = new LavaSurvivalMemory(lvl);
                    Player.players.ForEach((all) =>
                    {
                        if (all.level == lvl)
                            lsm.players.Add(all);
                    });
                    games.Add(lsm);

                    Command.all.Find("physics").Use(p, "2");
                    Command.all.Find("save").Use(p, lvl.name);
                    Command.all.Find("perbuild").Use(p, lvl.name + " Guest");
                    Player.GlobalMessageLevel(lvl, "You've got a half minute to build. Use the time!");
                    Thread.Sleep(30000);
                    
                    if (games.IndexOf(games.Find(g => g.level == lvl)) == -1) return;
                    Command.all.Find("cuboid").Use(p, "active_hot_lava random");
                    Command.all.Find("click").Use(p, "0 " + (lvl.height - 1) + " 0");
                    Command.all.Find("click").Use(p, (lvl.width - 1) + " " + (lvl.height - 1) + " " + (lvl.depth - 1));

                    DateTime Dt = DateTime.Now.AddMinutes(minutes);
                    for (int ii = 0; ii != (int)minutes; )
                    {
                        if (games.IndexOf(games.Find(g => g.level == lvl)) == -1) return;
                        ii++;
                        while (Dt > DateTime.Now.AddMinutes(minutes - ii)) { }
                        Player.GlobalMessageLevel(lvl, "only " + (int)(minutes - ii) + " minutes left.");
                    }

                    Player.GlobalMessageLevel(lvl, "The round has finished WD to all survivors");
                    games.Find(g => g.level == lvl).players.ForEach((all) => { all.completedGames++; });
                    games.RemoveAt(games.IndexOf(games.Find(g => g.level == lvl)));
                    Command.all.Find("restore").Use(p, lvl.name + "1");
                }
            }
            else if (param[0] == "stop")
            {
                if (param.Length == 2)
                {
                    Level lvl = Level.Find(param[1]);
                    if (lvl == null) { Player.SendMessage(p, "Unable to find level: " + param[1]); return; }
                    if (games.IndexOf(games.Find(g => g.level == lvl)) == -1) { Player.SendMessage(p, "Theres no Lava Survival Game going on in: " + lvl.name); return; }

                    games.RemoveAt(games.IndexOf(games.Find(g => g.level == lvl)));
                    Player.GlobalMessage(p.name + " has stopped the game");
                    Command.all.Find("physics").Use(p, "0");
                    Command.all.Find("replaceall").Use(p, "active_hot_lava air");
                }
                else { Help(p); return; }
            }
            else { Help(p); return; }
        }
        public override void Help(Player p)
        {
            Player.SendMessage(p, "/lavasurvival map <mapsize> <mapname> to create an map to play on.");
            Player.SendMessage(p, "<mapsize> can be small, medium and large.");
            Player.SendMessage(p, "/lavasurvival start <mapname> [minutes] to start the game.");
            Player.SendMessage(p, "A game must be atleast 5 minutes long");
            Player.SendMessage(p, "/lavasurvival stop <mapname> to stop the game before time");
        }
        public override string[] Example(Player p)
        {
            return new string []
            {
                ""
            };
        }
        struct LavaSurvivalMemory { public Level level; public List<Player> players; public LavaSurvivalMemory(Level l) { level = l; players = new List<Player>(); } }
        List<LavaSurvivalMemory> games = new List<LavaSurvivalMemory>();
    }
}