﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/

namespace MCRevive
{
    public abstract class Command
    {
        public abstract string name { get; }
        public abstract string shortcut { get; }
        public abstract string type { get; }
        public abstract int defaultRank { get; }
        public abstract void Use(Player p, string message);
        public abstract void Help(Player p);
        public abstract string[] Example(Player p);

        public static CommandList all = new CommandList();
        public static void InitAll()
        {
            all = new CommandList();
            all.Add(new CmdAbort());
            all.Add(new CmdAbout());
            all.Add(new CmdAchievements());
            all.Add(new CmdAdminPromote());
            all.Add(new CmdAfk());
            all.Add(new CmdAntiGriefLimit());
            all.Add(new CmdAntispeedhack());
            all.Add(new CmdBan());
            all.Add(new CmdBanip());
            all.Add(new CmdBanned());
            all.Add(new CmdBannedip());
            all.Add(new CmdBind());
            all.Add(new CmdBlocks());
            all.Add(new CmdBotAdd());
            all.Add(new CmdBotRemove());
            all.Add(new CmdBotSet());
            all.Add(new CmdBotSummon());
            all.Add(new CmdClearLvl());
            all.Add(new CmdClearChat());
			all.Add(new CmdCheckerboard());
            all.Add(new CmdClick());
            all.Add(new CmdColor());
            all.Add(new CmdCopy());
            all.Add(new CmdCrashserver());
            all.Add(new CmdCreeper());
            all.Add(new CmdCuboid());
            all.Add(new CmdDebug());
            all.Add(new CmdDelete());
            all.Add(new CmdDeleteLvl());
            all.Add(new CmdDelRank());
            all.Add(new CmdDemote());
            all.Add(new CmdDescription());
            all.Add(new CmdDevs());
            all.Add(new CmdDraw());
            all.Add(new CmdEditRank());
            all.Add(new CmdExample());
            all.Add(new CmdFindcenter());
            all.Add(new CmdFixGrass());
            all.Add(new CmdFly());
            all.Add(new CmdFreeze());
            all.Add(new CmdGet());
            all.Add(new CmdGoto());
            all.Add(new CmdGun());
            all.Add(new CmdHelp());
            all.Add(new CmdHide());
            all.Add(new CmdHollow());
            all.Add(new CmdHost());
            all.Add(new CmdImagePrint());
            all.Add(new CmdImpersonate());
            //all.Add(new CmdInfection());
            all.Add(new CmdInfo());
            all.Add(new CmdInstboid());
            all.Add(new CmdInstPaste());
            all.Add(new CmdInvincible());
            all.Add(new CmdJail());
            all.Add(new CmdJoker());
            all.Add(new CmdKick());
            all.Add(new CmdKickban());
            //all.Add(new CmdKill());
            all.Add(new Cmdlsurv());
            all.Add(new CmdLevels());
            all.Add(new CmdLine());
            all.Add(new CmdLoad());
			all.Add(new CmdMain());
            all.Add(new CmdMap());
            all.Add(new CmdMapInfo());
            all.Add(new CmdMaxLimit());
            all.Add(new CmdMe());
            all.Add(new CmdMeasure());
            all.Add(new CmdMegaboid());
            all.Add(new CmdMode());
            all.Add(new CmdModerate());
            all.Add(new CmdMove());
            all.Add(new CmdMute());
            all.Add(new CmdNewLvl());
            all.Add(new CmdNewRank());
            all.Add(new CmdNick());
            all.Add(new CmdOpChat());
            all.Add(new CmdOpPromote());
            all.Add(new CmdPaint());
            all.Add(new CmdPaste());
            all.Add(new CmdPatrol());
            //all.Add(new CmdPause());
            all.Add(new CmdPermissionBuild());
            all.Add(new CmdPermissionVisit());
            all.Add(new CmdPhysics());
			all.Add(new CmdPie());
            all.Add(new CmdPlace());
            all.Add(new CmdPlayers());
            all.Add(new CmdPortal());
            all.Add(new CmdPromote());
            all.Add(new CmdRainbow());
            all.Add(new CmdRandomboid());
            all.Add(new CmdRedo());
            all.Add(new CmdRegenLvl());
            all.Add(new CmdRenameLvl());
            all.Add(new CmdRepeat());
            all.Add(new CmdReplace());
            all.Add(new CmdReplaceAll());
            all.Add(new CmdReplaceNot());
            all.Add(new CmdResetIRC());
            all.Add(new CmdResizeLvl());
			all.Add(new CmdRestart());
            all.Add(new CmdRestore());
            all.Add(new CmdReveal());
            all.Add(new CmdReview());
            all.Add(new CmdRide());
            all.Add(new CmdRules());
            all.Add(new CmdSave());
            all.Add(new CmdSay());
            //all.Add(new CmdSendCmd());
            all.Add(new CmdSendMap());
            //all.Add(new CmdSetCmd());
            all.Add(new CmdSetRank());
            all.Add(new CmdSetmain());
            all.Add(new CmdSetspawn());
            all.Add(new CmdSlap());
            all.Add(new CmdSolid());
            all.Add(new CmdSpawn());
            all.Add(new CmdSpeedy());
            all.Add(new CmdSpheroid());
            //all.Add(new CmdSpleef());
            all.Add(new CmdStatic());
            all.Add(new CmdSummon());
            all.Add(new CmdTBColor());
            all.Add(new CmdTColor());
            //all.Add(new CmdTetris());
            all.Add(new CmdTime());
            all.Add(new CmdTimer());
            all.Add(new CmdTitle());
            all.Add(new CmdTnt());
            all.Add(new CmdTp());
            //all.Add(new CmdTree());
            all.Add(new CmdTrust());
            all.Add(new CmdUnban());
            all.Add(new CmdUnbanip());
            all.Add(new CmdUndo());
            all.Add(new CmdUnload());
            all.Add(new CmdUnloaded());
            all.Add(new CmdUpdate());
            all.Add(new CmdView());
            all.Add(new CmdViewRank());
            all.Add(new CmdVoice());
            all.Add(new CmdVote());
            all.Add(new CmdWhisper());
            all.Add(new CmdWhois());
            all.Add(new CmdWhowas());
            all.Add(new CmdWrite());
            all.Add(new CmdXBan());
            all.Add(new CmdZone());
            all.Add(new CmdZombie());
        }
    }
}