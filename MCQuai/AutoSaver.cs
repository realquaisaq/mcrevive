﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.ComponentModel;


namespace MCRevive
{
    class AutoSaver
    {
        static int _interval;

        static int count = 1;
        public AutoSaver(int interval)
        {
            _interval = interval * 1000;

            Thread runner = new Thread(new ThreadStart(delegate
            {
                while (true)
                {
                    Thread.Sleep(_interval);
                    Server.ml.Queue(delegate { Run(); });
                }
            }));
            runner.Start();
        }

        public static void Run()
        {
            try
            {
                count--;
                foreach (Level l in Server.levels)
                    try
                    {
                        if (!l.changed) return;

                        l.Save();
                        if (count == 0)
                        {
                            int backupNumber = l.Backup();

                            if (backupNumber != -1)
                            {
                                l.ChatLevel("Backup " + backupNumber + " saved.");
                                Server.s.Log("Backup " + backupNumber + " saved for " + l.name);
                            }
                        }
                    }
                    catch
                    {
                        Server.s.Log("Backup for " + l.name + " has caused an error.");
                    }
                if (count <= 0)
                    count = 15;
            }
            catch (Exception e) { Server.ErrorLog(e); }

            try
            {
                Player.players.ForEach((all) => { all.saveSettings(); });
            }
            catch (Exception e) { Server.ErrorLog(e); }
            try
            {
                Server.levels.ForEach((level) =>
                {
                    if (level.unload)
                    {
                        bool hasplayers = false;
                        Player.players.ForEach((pl) => { if (pl.level == level) hasplayers = true; });
                        if (!hasplayers)
                            level.Unload();
                    }
                });
            }
            catch (Exception e) { Server.ErrorLog(e); }
        }
    }
}
