﻿/*
	Copyright 2010 MCSharp team. (modified for MCRevive) Licensed under the
	Educational Community License, Version 2.0 (the "License"); you may
	not use this file except in compliance with the License. You may
	obtain a copy of the License at
	
	http://www.osedu.org/licenses/ECL-2.0
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;

namespace MCRevive
{
    public class Block
    {
        public const ushort air = 0;
        public const ushort rock = 1;
        public const ushort grass = 2;
        public const ushort dirt = 3;
        public const ushort stone = 4;
        public const ushort wood = 5;
        public const ushort shrub = 6;
        public const ushort blackrock = 7;// adminium || bedrock
        public const ushort water = 8;
        public const ushort waterstill = 9;
        public const ushort lava = 10;
        public const ushort lavastill = 11;
        public const ushort sand = 12;
        public const ushort gravel = 13;
        public const ushort goldrock = 14;
        public const ushort ironrock = 15;
        public const ushort coal = 16;
        public const ushort trunk = 17;
        public const ushort leaf = 18;
        public const ushort sponge = 19;
        public const ushort glass = 20;
        public const ushort red = 21;
        public const ushort orange = 22;
        public const ushort yellow = 23;
        public const ushort lightgreen = 24;
        public const ushort green = 25;
        public const ushort aquagreen = 26;
        public const ushort cyan = 27;
        public const ushort lightblue = 28;
        public const ushort blue = 29;
        public const ushort purple = 30;
        public const ushort lightpurple = 31;
        public const ushort pink = 32;
        public const ushort darkpink = 33;
        public const ushort darkgrey = 34;
        public const ushort lightgrey = 35;
        public const ushort white = 36;
        public const ushort yellowflower = 37;
        public const ushort redflower = 38;
        public const ushort mushroom = 39;
        public const ushort redmushroom = 40;
        public const ushort goldsolid = 41;
        public const ushort iron = 42;
        public const ushort staircasefull = 43;
        public const ushort staircasestep = 44;
        public const ushort brick = 45;
        public const ushort tnt = 46;
        public const ushort bookcase = 47;
        public const ushort stonevine = 48;
        public const ushort obsidian = 49;
        public const ushort Zero = 255;

        //Custom blocks

        public const ushort op_glass = 100;
        public const ushort opsidian = 101;
        public const ushort op_brick = 102;
        public const ushort op_stone = 103;
        public const ushort op_cobblestone = 104;
        public const ushort op_air = 105;
        public const ushort op_water = 106;
        public const ushort op_lava = 107;
        public const ushort op_tree = 108;
        public const ushort op_wood = 109;

        public const ushort wood_float = 110;
        public const ushort lava_fast = 111;

        // doors
        public const ushort door = 112;
        public const ushort door2 = 113;
        public const ushort door3 = 114;
        public const ushort door4 = 115;
        public const ushort door5 = 116;
        public const ushort door6 = 117;
        public const ushort door7 = 118;
        public const ushort door8 = 119;
        public const ushort door9 = 120;
        public const ushort door10 = 121;
        public const ushort air_door = 122;
        public const ushort air_switch = 123;
        public const ushort water_door = 124;
        public const ushort lava_door = 125;
        public const ushort door_iron = 220;
        public const ushort door_dirt = 221;
        public const ushort door_grass = 222;
        public const ushort door_blue = 223;
        public const ushort door_book = 224;

        public const ushort door_air = 126;
        public const ushort door2_air = 127;
        public const ushort door3_air = 128;
        public const ushort door4_air = 129;
        public const ushort door5_air = 130;
        public const ushort door6_air = 131;
        public const ushort door7_air = 132;
        public const ushort door8_air = 133;
        public const ushort door9_air = 134;
        public const ushort door10_air = 135;
        public const ushort door11_air = 136;
        public const ushort door12_air = 137;
        public const ushort door13_air = 138;
        public const ushort door14_air = 139;
        public const ushort door_iron_air = 225;
        public const ushort door_dirt_air = 226;
        public const ushort door_grass_air = 227;
        public const ushort door_blue_air = 228;
        public const ushort door_book_air = 229;

        //portals
        public const ushort air_portal = 160;
        public const ushort water_portal = 161;
        public const ushort lava_portal = 162;
        public const ushort blue_portal = 163;
        public const ushort orange_portal = 164;

        //explosives and explosions
        public const ushort smalltnt = 182;
        public const ushort bigtnt = 183;
        public const ushort tntexplosion = 184;
        public const ushort fire = 185;
        public const ushort creeper_explosion = 186;
        public const ushort rocketstart = 187;
        public const ushort rockethead = 188;
        public const ushort firework = 189;

        //Death
        public const ushort deathlava = 190;
        public const ushort deathwater = 191;
        public const ushort deathair = 192;

        public const ushort activedeathwater = 193;
        public const ushort activedeathlava = 194;

        public const ushort magma = 195;

        //air_flood
        public const ushort air_flood = 200;
        public const ushort air_flood_layer = 201;
        public const ushort air_flood_down = 202;
        public const ushort air_flood_up = 203;

        public const ushort train = 230;
        public const ushort traintail = 231;

        //animals
        public const ushort birdwhite = 235;
        public const ushort birdblack = 236;
        public const ushort birdwater = 237;
        public const ushort birdlava = 238;
        public const ushort birdred = 239;
        public const ushort birdblue = 240;
        public const ushort birdkill = 242;

        public const ushort fishgold = 245;
        public const ushort fishsponge = 246;
        public const ushort fishshark = 247;
        public const ushort fishsalmon = 248;
        public const ushort fishbetta = 249;
        public const ushort fishlavashark = 250;

        public const ushort snake = 251;
        public const ushort snaketail = 252;
        
        //redstone
        public const ushort redstoneWireOn = 330;
        public const ushort redstoneWireOff = 331;
        public const ushort redstoneTorchOnNorth = 332;
        public const ushort redstoneTorchOffNorth = 333;
        public const ushort redstoneTorchOnSouth = 334;
        public const ushort redstoneTorchOffSouth = 335;
        public const ushort redstoneTorchOnWest = 336;
        public const ushort redstoneTorchOffWest = 337;
        public const ushort redstoneTorchOnEast = 338;
        public const ushort redstoneTorchOffEast = 339;
        public const ushort redstoneTorchOnNormal = 340;
        public const ushort redstoneTorchOffNormal = 341;
        public const ushort redstoneRepeaterOn1North = 342;
        public const ushort redstoneRepeaterOff1North = 343;
        public const ushort redstoneRepeaterOn1South = 344;
        public const ushort redstoneRepeaterOff1South = 345;
        public const ushort redstoneRepeaterOn1West = 346;
        public const ushort redstoneRepeaterOff1West = 347;
        public const ushort redstoneRepeaterOn1East = 348;
        public const ushort redstoneRepeaterOff1East = 349;
        public const ushort redstoneRepeaterOn2North = 350;
        public const ushort redstoneRepeaterOff2North = 351;
        public const ushort redstoneRepeaterOn2South = 352;
        public const ushort redstoneRepeaterOff2South = 353;
        public const ushort redstoneRepeaterOn2West = 354;
        public const ushort redstoneRepeaterOff2West = 355;
        public const ushort redstoneRepeaterOn2East = 256;
        public const ushort redstoneRepeaterOff2East = 357;
        public const ushort redstoneRepeaterOn3North = 358;
        public const ushort redstoneRepeaterOff3North = 359;
        public const ushort redstoneRepeaterOn3South = 360;
        public const ushort redstoneRepeaterOff3South = 361;
        public const ushort redstoneRepeaterOn3West = 362;
        public const ushort redstoneRepeaterOff3West = 363;
        public const ushort redstoneRepeaterOn3East = 264;
        public const ushort redstoneRepeaterOff3East = 365;
        public const ushort redstoneRepeaterOn4North = 366;
        public const ushort redstoneRepeaterOff4North = 367;
        public const ushort redstoneRepeaterOn4South = 368;
        public const ushort redstoneRepeaterOff4South = 369;
        public const ushort redstoneRepeaterOn4West = 370;
        public const ushort redstoneRepeaterOff4West = 371;
        public const ushort redstoneRepeaterOn4East = 272;
        public const ushort redstoneRepeaterOff4East = 373;
        public const ushort redstoneButtonOn = 374;
        public const ushort redstoneButtonOff = 375;
        public const ushort redstoneLeverOn = 376;
        public const ushort redstoneLeverOff = 377;
        public const ushort redstonePistonNorthOn = 378;
        public const ushort redstonePistonNorthOff = 379;
        public const ushort redstonePistonSouthOn = 380;
        public const ushort redstonePistonSouthOff = 381;
        public const ushort redstonePistonWestOn = 382;
        public const ushort redstonePistonWestOff = 383;
        public const ushort redstonePistonEastOn = 384;
        public const ushort redstonePistonEastOff = 385;

        public const ushort Max = 500;

        public enum redstoneKind { All, Wire, Torch, Repeater, Button, Lever, Piston, ToOff } 
        
        public static List<Blocks> BlockList = new List<Blocks>();
        public class Blocks
        {
            public ushort type;
            public int rank;
        }

        public static void SetBlocks()
        {
            BlockList = new List<Blocks>();
            Blocks b = new Blocks();
            b.rank = LevelPermission.Guest;

            for (ushort i = 0; i < Max; i++)
            {
                b = new Blocks();
                b.type = i;
                BlockList.Add(b);
            }

            List<Blocks> storedList = new List<Blocks>();

            BlockList.ForEach((bs) =>
            {
                b = new Blocks();
                b.type = bs.type;

                switch (bs.type)
                {
                    case Zero:
                        b.rank = LevelPermission.Admin;
                        break;

                    case blackrock:
                    case op_air:
                    case op_brick:
                    case op_cobblestone:
                    case op_glass:
                    case op_stone:
                    case op_water:
                    case op_lava:
                    case op_tree:
                    case op_wood:
                    case opsidian:
                    case air_flood:
                    case air_flood_down:
                    case air_flood_layer:
                    case air_flood_up:
                    case bigtnt:
                    case rocketstart:
                    case rockethead:
                    case birdred:
                    case birdkill:
                    case birdblue:
                    case fishgold:
                    case fishsponge:
                    case fishshark:
                    case fishsalmon:
                    case fishbetta:
                    case fishlavashark:
                    case snake:
                    case snaketail:

                        b.rank = LevelPermission.Operator;
                        break;

                    case wood_float:
                    case door_air:
                    case door2_air:
                    case door3_air:
                    case door4_air:
                    case door5_air:
                    case door6_air:
                    case door7_air:
                    case door8_air:
                    case door9_air:
                    case door10_air:
                    case door11_air:
                    case door12_air:
                    case door13_air:
                    case door14_air:
                    case door_iron_air:
                    case door_grass_air:
                    case door_dirt_air:
                    case door_blue_air:
                    case door_book_air:
                    case air_portal:
                    case water_portal:
                    case lava_portal:
                    case blue_portal:
                    case orange_portal:
                    case lava_fast:
                    case lava:
                    case water:
                    case magma:
                    case deathlava:
                    case deathwater:
                    case deathair:
                    case activedeathwater:
                    case activedeathlava:
                    case fire:
                    case tntexplosion:
                    case smalltnt:
                    case firework:
                    case train:
                    case traintail:
                    case birdwhite:
                    case birdblack:
                    case birdwater:
                    case birdlava:

                        b.rank = LevelPermission.AdvBuilder;
                        break;

                    case door:
                    case door2:
                    case door3:
                    case door4:
                    case door5:
                    case door6:
                    case door7:
                    case door8:
                    case door9:
                    case door10:
                    case air_door:
                    case air_switch:
                    case water_door:
                    case lava_door:
                    case door_iron:
                    case door_grass:
                    case door_dirt:
                    case door_blue:
                    case door_book:
                        
                    case redstoneWireOn:
                    case redstoneWireOff:
                    case redstoneTorchOnNorth:
                    case redstoneTorchOffNorth:
                    case redstoneTorchOnSouth:
                    case redstoneTorchOffSouth:
                    case redstoneTorchOnWest:
                    case redstoneTorchOffWest:
                    case redstoneTorchOnEast:
                    case redstoneTorchOffEast:
                    case redstoneTorchOnNormal:
                    case redstoneTorchOffNormal:
                    case redstoneRepeaterOn1North:
                    case redstoneRepeaterOff1North:
                    case redstoneRepeaterOn1South:
                    case redstoneRepeaterOff1South:
                    case redstoneRepeaterOn1West:
                    case redstoneRepeaterOff1West:
                    case redstoneRepeaterOn1East:
                    case redstoneRepeaterOff1East:
                    case redstoneRepeaterOn2North:
                    case redstoneRepeaterOff2North:
                    case redstoneRepeaterOn2South:
                    case redstoneRepeaterOff2South:
                    case redstoneRepeaterOn2West:
                    case redstoneRepeaterOff2West:
                    case redstoneRepeaterOn2East:
                    case redstoneRepeaterOff2East:
                    case redstoneRepeaterOn3North:
                    case redstoneRepeaterOff3North:
                    case redstoneRepeaterOn3South:
                    case redstoneRepeaterOff3South:
                    case redstoneRepeaterOn3West:
                    case redstoneRepeaterOff3West:
                    case redstoneRepeaterOn3East:
                    case redstoneRepeaterOff3East:
                    case redstoneRepeaterOn4North:
                    case redstoneRepeaterOff4North:
                    case redstoneRepeaterOn4South:
                    case redstoneRepeaterOff4South:
                    case redstoneRepeaterOn4West:
                    case redstoneRepeaterOff4West:
                    case redstoneRepeaterOn4East:
                    case redstoneRepeaterOff4East:
                    case redstoneButtonOn:
                    case redstoneButtonOff:
                    case redstoneLeverOn:
                    case redstoneLeverOff:
                    case redstonePistonNorthOn:
                    case redstonePistonNorthOff:
                    case redstonePistonSouthOn:
                    case redstonePistonSouthOff:
                    case redstonePistonWestOn:
                    case redstonePistonWestOff:
                    case redstonePistonEastOn:
                    case redstonePistonEastOff:

                        b.rank = LevelPermission.Builder;
                        break;

                    case lavastill:
                    case waterstill:

                        b.rank = LevelPermission.Guest;
                        break;

                    default:
                        b.rank = LevelPermission.Banned;
                        break;
                }

                storedList.Add(b);
            });

            if (File.Exists("properties/blocks.properties"))
            {
                string[] lines = File.ReadAllLines("properties/blocks.properties");
                foreach (string line in lines)
                {
                    try
                    {
                        if (line[0] != '#' && line != "")
                        {
                            Blocks newBlock = new Blocks();
                            newBlock.type = Block.Number(line.Split('=')[0].Trim());
                            try { newBlock.rank = int.Parse(line.Split('=')[1].Trim()); }
                            catch { newBlock.rank = LevelPermission.Null; }
                            if (newBlock.rank != LevelPermission.Null)
                                storedList[storedList.FindIndex(sL => sL.type == newBlock.type)] = newBlock;
                            else
                                throw new Exception();
                        }
                    }
                    catch { if (line != "") Server.s.Log("Could not find the rank given on " + line + ". Using default"); }
                }
            }

            BlockList.Clear();
            BlockList.AddRange(storedList);
            SaveBlocks(BlockList);
        }
        public static void SaveBlocks(List<Blocks> givenList)
        {
            try
            {
                StreamWriter w = new StreamWriter(File.Create("properties/blocks.properties"));
                w.WriteLine("#   This file didicates what ranks may use what blocks.");
                w.WriteLine("#   Current ranks: Banned, Guest, Builder, AdvBuilder, Operator, Admin, Owner, NoOne.");
                w.WriteLine("");

                givenList.ForEach((bs) =>
                {
                    if (Block.Name(bs.type).ToLower() != "unknown")
                    {
                        w.WriteLine(Block.Name(bs.type) + ((Block.Name(bs.type).Length > 16) ? "\t" :
                            ((Block.Name(bs.type).Length < 8)) ? "\t\t" : "\t") + "=\t" + bs.rank);
                    }
                });
                w.Flush();
                w.Close();
                w.Dispose();
            }
            catch (Exception e) { Server.ErrorLog(e); }
        }

        public static bool AllowBreak(ushort type)
        {
            switch (type)
            {
                case Block.blue_portal:
                case Block.orange_portal:

                case Block.door:
                case Block.door2:
                case Block.door3:
                case Block.door4:
                case Block.door5:
                case Block.door6:
                case Block.door7:
                case Block.door8:
                case Block.door9:
                case Block.door10:

                case Block.smalltnt:
                case Block.bigtnt:
                case Block.firework:
                    return true;

                default:
                    return false;
            }
        }

        public static bool canPlace(Player p, ushort b) { return canPlace(p.group.Permission, b); }
        public static bool canPlace(int givenPerm, ushort givenBlock)
        {
            foreach (Blocks b in BlockList)
                if (givenBlock == b.type)
                {
                    if (b.rank <= givenPerm) return true;
                    return false;
                }
            return false;
        }

        public static bool Mover(ushort type)
        {
            switch (type)
            {
                case Block.air_portal:
                case Block.water_portal:
                case Block.lava_portal:

                case Block.air_switch:
                case Block.water_door:
                case Block.lava_door:
                    return true;

                default:
                    return false;
            }
        }

        public static bool LightPass(ushort type)
        {
            switch (type)
            {
                case Block.air:
                case Block.glass:
                case Block.op_air:
                case Block.op_glass:
                case Block.leaf:
                case Block.redflower:
                case Block.yellowflower:
                case Block.mushroom:
                case Block.redmushroom:
                case Block.shrub:
                case Block.door3:
                    return true;

                default:
                    return false;
            }
        }

        public static bool Walkthrough(ushort type)
        {
            switch (type)
            {
                case air:
                case water:
                case waterstill:
                case lava:
                case lavastill:
                case yellowflower:
                case redflower:
                case mushroom:
                case redmushroom:
                case shrub:
                    return true;

                default:
                    return false;
            }
        }

        public static bool Portal(ushort type)
        {
            switch (type)
            {
                case Block.blue_portal:
                case Block.orange_portal:
                case Block.air_portal:
                case Block.water_portal:
                case Block.lava_portal:
                    return true;

                default:
                    return false;
            }
        }

        public static bool Death(ushort type)
        {
            switch (type)
            {
                case tntexplosion:
                case deathwater:
                case deathlava:
                case deathair:
                case activedeathlava:
                case activedeathwater:
                case magma:
                case birdkill:
                case fishshark:
                case fishlavashark:
                case train:
                case snake:
                case fire:
                case rockethead:
                    return true;

                default:
                    return false;
            }
        }

        public static bool RightClick(ushort type, bool countAir = false)
        {
            if (countAir && type == Block.air) return true;

            switch (type)
            {
                case Block.water:
                case Block.lava:
                case Block.waterstill:
                case Block.lavastill:
                    return true;

                default:
                    return false;
            }
        }

        public static bool LavaKill(ushort type)
        {
            switch (type)
            {
                case wood:
                case shrub:
                case trunk:
                case leaf:
                case sponge:
                case red:
                case orange:
                case yellow:
                case lightgreen:
                case green:
                case aquagreen:
                case cyan:
                case lightblue:
                case blue:
                case purple:
                case lightpurple:
                case pink:
                case darkpink:
                case darkgrey:
                case lightgrey:
                case white:
                case yellowflower:
                case redflower:
                case mushroom:
                case redmushroom:
                case bookcase:
                    return true;

                default:
                    return false;
            }
        }
        public static bool WaterKill(ushort type)
        {
            switch (type)
            {
                case air:
                case shrub:
                case leaf:
                case yellowflower:
                case redflower:
                case mushroom:
                case redmushroom:
                    return true;

                default:
                    return false;
            }
        }

        public static bool NeedRestart(ushort type)
        {
            switch (type)
            {
                case train:
                case traintail:
                case snake:
                case snaketail:
                case fire:
                case rockethead:
                case firework:
                case birdblack:
                case birdblue:
                case birdkill:
                case birdlava:
                case birdred:
                case birdwater:
                case birdwhite:
                case fishbetta:
                case fishgold:
                case fishsalmon:
                case fishshark:
                case fishlavashark:
                case fishsponge:
                case tntexplosion:
                    return true;

                default:
                    return false;
            }
        }

        public static bool ActiveateableByRedstone (ushort type, byte allOnOff = 0, redstoneKind rType = redstoneKind.All)
        {
	        if (type > 389 || type < 330) return false;
	        else if (allOnOff == 0 && rType == redstoneKind.All) return true;
	        else if (allOnOff == 1 && type % 2 == 0 && rType == redstoneKind.All) return true;
	        else if (allOnOff == 2 && type % 2 == 1 && rType == redstoneKind.All) return true;
	
	        else if (allOnOff == 0 && type >= redstoneRepeaterOn1North && 
		        type <= redstoneRepeaterOff4East && rType == redstoneKind.Repeater) return true;
	        else if (allOnOff == 1 && type >= redstoneRepeaterOn1North && 
		        type <= redstoneRepeaterOff4East && type % 2 == 0 && rType == redstoneKind.Repeater) return true;
	        else if (allOnOff == 2 && type >= redstoneRepeaterOn1North && 
		        type <= redstoneRepeaterOff4East && type % 2 == 1 && rType == redstoneKind.Repeater) return true;
	
	        else if (allOnOff == 0 && (type == redstoneWireOn || type == redstoneWireOff) && rType == redstoneKind.Wire) return true;
	        else if (allOnOff == 1 && type == redstoneWireOn && rType == redstoneKind.Wire) return true;
	        else if (allOnOff == 2 && type == redstoneWireOff && rType == redstoneKind.Wire) return true;
	
	        else if (allOnOff == 0 && type >= redstonePistonNorthOn && 
		        type <= redstonePistonEastOff && rType == redstoneKind.Piston) return true;
	        else if (allOnOff == 1 && type >= redstonePistonNorthOn && 
		        type <= redstonePistonEastOff && type % 2 == 0 && rType == redstoneKind.Piston) return true;
	        else if (allOnOff == 2 && type >= redstonePistonNorthOn && 
		        type <= redstonePistonEastOff && type % 2 == 1 && rType == redstoneKind.Piston) return true;
	        return false;
        }

        public static void SwitchPower(Level lvl, int pos, bool power)
        {
	        ushort type = lvl.GetTile(pos);
	        ushort x, y, z;
	        if (type > 389 || type < 330) return;
	
	        if (type % 2 == 1 && power) type -= 1;
	        else if (type % 2 == 0 && !power) type += 1;
            lvl.IntToPos(pos, out x, out y, out z);
	
	        lvl.SetTile(x, y, z, type);
        }

        public static bool Physics(ushort type)   //returns false if placing block cant actualy cause any physics to happen
        {
            switch (type)
            {
                case rock:
                case stone:
                case blackrock:
                case waterstill:
                case lavastill:
                case goldrock:
                case ironrock:
                case coal:
                case red:
                case orange:
                case yellow:
                case lightgreen:
                case green:
                case aquagreen:
                case cyan:
                case lightblue:
                case blue:
                case purple:
                case lightpurple:
                case pink:
                case darkpink:
                case darkgrey:
                case lightgrey:
                case white:
                case goldsolid:
                case iron:
                case staircasefull:
                case brick:
                case tnt:
                case stonevine:
                case obsidian:

                case op_glass:
                case opsidian:
                case op_brick:
                case op_stone:
                case op_cobblestone:
                case op_air:
                case op_water:
                case op_lava:
                case op_tree:
                case op_wood:

                case door:
                case door2:
                case door3:
                case door4:
                case door5:
                case door6:
                case door7:
                case door8:
                case door9:
                case door10:
                case door_iron:
                case door_dirt:
                case door_grass:
                case door_blue:
                case door_book:
                case air_door:
                case air_switch:
                case water_door:
                case lava_door:

                case blue_portal:
                case orange_portal:
                case air_portal:
                case water_portal:
                case lava_portal:

                case deathair:
                case deathlava:
                case deathwater:

                    return false;

                default:
                    return true;
            }
        }

        public static string Name(ushort type)
        {
            switch (type)
            {
                case air: return "air";
                case rock: return "stone";
                case grass: return "grass";
                case dirt: return "dirt";
                case stone: return "cobblestone";
                case wood: return "wood";
                case shrub: return "plant";
                case blackrock: return "adminium";
                case water: return "active_water";
                case waterstill: return "water";
                case lava: return "active_lava";
                case lavastill: return "lava";
                case sand: return "sand";
                case gravel: return "gravel";
                case goldrock: return "gold_ore";
                case ironrock: return "iron_ore";
                case coal: return "coal";
                case trunk: return "tree";
                case leaf: return "leaves";
                case sponge: return "sponge";
                case glass: return "glass";
                case red: return "red";
                case orange: return "orange";
                case yellow: return "yellow";
                case lightgreen: return "greenyellow";
                case green: return "green";
                case aquagreen: return "springgreen";
                case cyan: return "cyan";
                case lightblue: return "blue";
                case blue: return "blueviolet";
                case purple: return "indigo";
                case lightpurple: return "purple";
                case pink: return "magenta";
                case darkpink: return "pink";
                case darkgrey: return "black";
                case lightgrey: return "gray";
                case white: return "white";
                case yellowflower: return "yellow_flower";
                case redflower: return "red_flower";
                case mushroom: return "brown_shroom";
                case redmushroom: return "red_shroom";
                case goldsolid: return "gold";
                case iron: return "iron";
                case staircasefull: return "double_stair";
                case staircasestep: return "stair";
                case brick: return "brick";
                case tnt: return "tnt";
                case bookcase: return "bookcase";
                case stonevine: return "mossy_cobblestone";
                case obsidian: return "obsidian";
                case op_glass: return "op_glass";
                case opsidian: return "opsidian";
                case op_brick: return "op_brick";
                case op_stone: return "op_stone";
                case op_cobblestone: return "op_cobblestone";
                case op_air: return "op_air";
                case op_water: return "op_water";
                case op_lava: return "op_lava";
                case op_tree: return "op_tree";
                case op_wood: return "op_wood";

                case wood_float: return "wood_float";
                case lava_fast: return "lava_fast";

                case door: return "door";
                case door2: return "door2";
                case door3: return "door3";
                case door4: return "door4";
                case door5: return "door5";
                case door6: return "door6";
                case door7: return "door7";
                case door8: return "door8";
                case door9: return "door9";
                case door10: return "door10";
                case door_iron: return "door_iron";
                case door_grass: return "door_grass";
                case door_dirt: return "door_dirt";
                case door_blue: return "door_blue";
                case door_book: return "door_book";

                case air_portal: return "air_portal";
                case water_portal: return "water_portal";
                case lava_portal: return "lava_portal";
                case blue_portal: return "blue_portal";
                case orange_portal: return "orange_portal";

                case smalltnt: return "small_tnt";
                case bigtnt: return "big_tnt";
                case tntexplosion: return "tnt_explosion";
                case fire: return "fire";
                case rocketstart: return "rocketstart";
                case rockethead: return "rockethead";
                case firework: return "firework";

                case deathlava: return "hot_lava";
                case deathwater: return "cold_water";
                case deathair: return "nerve_gas";
                case activedeathwater: return "active_cold_water";
                case activedeathlava: return "active_hot_lava";
                case magma: return "magma";
                    
                //case redstoneWireOn: return "redstone_wire-on";
                //case redstoneWireOff: return "redstone_wire-off";
                //case redstoneTorchOnNorth: return "redstone_torch_north-on";
                //case redstoneTorchOnSouth: return "redstone_torch_south-on";
                //case redstoneTorchOnWest: return "redstone_torch_west-on";
                //case redstoneTorchOnEast: return "redstone_torch_east-on";
                //case redstoneTorchOnNormal: return "redstone_torch-on";
                //case redstoneTorchOffNorth: return "redstone_torch_north-off";
                //case redstoneTorchOffSouth: return "redstone_torch_south-off";
                //case redstoneTorchOffWest: return "redstone_torch_west-off";
                //case redstoneTorchOffEast: return "redstone_torch_east-off";
                //case redstoneTorchOffNormal: return "redstone_torch-off";
                //case redstoneRepeaterOn1North: return "redstone_repeater_1_north-on";
                //case redstoneRepeaterOn1South: return "redstone_repeater_1_south-on";
                //case redstoneRepeaterOn1West: return "redstone_repeater_1_west-on";
                //case redstoneRepeaterOn1East: return "redstone_repeater_1_east-on";
                //case redstoneRepeaterOn2North: return "redstone_repeater_2_north-on";
                //case redstoneRepeaterOn2South: return "redstone_repeater_2_south-on";
                //case redstoneRepeaterOn2West: return "redstone_repeater_2_west-on";
                //case redstoneRepeaterOn2East: return "redstone_repeater_2_east-on";
                //case redstoneRepeaterOn3North: return "redstone_repeater_3_north-on";
                //case redstoneRepeaterOn3South: return "redstone_repeater_3_south-on";
                //case redstoneRepeaterOn3West: return "redstone_repeater_3_west-on";
                //case redstoneRepeaterOn3East: return "redstone_repeater_3_east-on";
                //case redstoneRepeaterOn4North: return "redstone_repeater_4_north-on";
                //case redstoneRepeaterOn4South: return "redstone_repeater_4_south-on";
                //case redstoneRepeaterOn4West: return "redstone_repeater_4_west-on";
                //case redstoneRepeaterOn4East: return "redstone_repeater_4_east-on";
                //case redstoneRepeaterOff1North: return "redstone_repeater_1_north-off";
                //case redstoneRepeaterOff1South: return "redstone_repeater_1_south-off";
                //case redstoneRepeaterOff1West: return "redstone_repeater_1_west-off";
                //case redstoneRepeaterOff1East: return "redstone_repeater_1_east-off";
                //case redstoneRepeaterOff2North: return "redstone_repeater_2_north-off";
                //case redstoneRepeaterOff2South: return "redstone_repeater_2_south-off";
                //case redstoneRepeaterOff2West: return "redstone_repeater_2_west-off";
                //case redstoneRepeaterOff2East: return "redstone_repeater_2_east-off";
                //case redstoneRepeaterOff3North: return "redstone_repeater_3_north-off";
                //case redstoneRepeaterOff3South: return "redstone_repeater_3_south-off";
                //case redstoneRepeaterOff3West: return "redstone_repeater_3_west-off";
                //case redstoneRepeaterOff3East: return "redstone_repeater_3_east-off";
                //case redstoneRepeaterOff4North: return "redstone_repeater_4_north-off";
                //case redstoneRepeaterOff4South: return "redstone_repeater_4_south-off";
                //case redstoneRepeaterOff4West: return "redstone_repeater_4_west-off";
                //case redstoneRepeaterOff4East: return "redstone_repeater_4_east-off";
                //case redstoneButtonOn: return "redstone_button-on";
                //case redstoneButtonOff: return "redstone_button-off";
                //case redstoneLeverOn: return "redstone_lever-on";
                //case redstoneLeverOff: return "redstone_lever-off";
                    

                //Blocks after this are converted before saving
                case air_flood: return "air_flood";
                case air_flood_layer: return "air_flood_layer";
                case air_flood_down: return "air_flood_down";
                case air_flood_up: return "air_flood_up";
                case door_air: return "door_air";
                case door2_air: return "door2_air";
                case door3_air: return "door3_air";
                case door4_air: return "door4_air";
                case door5_air: return "door5_air";
                case door6_air: return "door6_air";
                case door7_air: return "door7_air";
                case door8_air: return "door8_air";
                case door9_air: return "door9_air";
                case door10_air: return "door10_air";
                case door11_air: return "door11_air";
                case door12_air: return "door12_air";
                case door13_air: return "door13_air";
                case door14_air: return "door14_air";
                case air_door: return "air_door";
                case air_switch: return "air_switch";
                case water_door: return "water_door";
                case lava_door: return "lava_door";
                case door_iron_air: return "door_iron_air";
                case door_dirt_air: return "door_dirt_air";
                case door_grass_air: return "door_grass_air";
                case door_blue_air: return "door_blue_air";
                case door_book_air: return "door_book_air";
                
                //"AI" blocks
                case train: return "train";
                case traintail: return "train_tail";

                //case snake: return "snake";
                //case snaketail: return "snake_tail";

                //case birdblue: return "blue_bird";
                //case birdred: return "red_robin";
                case birdwhite: return "dove";
                case birdblack: return "pidgeon";
                case birdwater: return "duck";
                //case birdlava: return "phoenix";
                //case birdkill: return "killer_phoenix";

                //case fishbetta: return "betta_fish";
                //case fishgold: return "goldfish";
                //case fishsalmon: return "salmon";
                //case fishshark: return "shark";
                //case fishsponge: return "sea_sponge";
                //case fishlavashark: return "lava_shark";

                default: return "unknown";
            }
        }
        public static ushort Number(string type)
        {
            switch (type)
            {
                case "air": return air;
                case "stone": return rock;
                case "grass": return grass;
                case "dirt": return dirt;
                case "cobble":
                case "cobblestone": return stone;
                case "plank":
                case "wood": return wood;
                case "sapling":
                case "plant": return shrub;
                case "admincrete":
                case "adminium":
                case "bedrock":
                case "blackrock": return blackrock;
                case "active_water": return water;
                case "water": return waterstill;
                case "active_lava": return lava;
                case "lava": return lavastill;
                case "sand": return sand;
                case "gravel": return gravel;
                case "gold_ore": return goldrock;
                case "iron_ore": return ironrock;
                case "coal": return coal;
                case "tree":
                case "log":
                case "trunk": return trunk;
                case "leaves": return leaf;
                case "sponge": return sponge;
                case "glass": return glass;
                case "red": case "redwool": return red;
                case "orange": case "orangewool": return orange;
                case "yellow": case "yellowwool": return yellow;
                case "greenyellow": return lightgreen;
                case "green": return green;
                case "springgreen": return aquagreen;
                case "cyan": return cyan;
                case "blue": return lightblue;
                case "blueviolet": return blue;
                case "indigo": return purple;
                case "purple": return lightpurple;
                case "magenta": return pink;
                case "pink": return darkpink;
                case "black": return darkgrey;
                case "gray": case "graywool": case "grey": case "greywool": return lightgrey;
                case "white": case "wool": case "whitewool": return white;
                case "yellowflower":
                case "yellow_flower": return yellowflower;
                case "redflower":
                case "red_flower": return redflower;
                case "mushroom": case "brownmushroom": case "brownshroom": case "brown_mushroom":
                case "brown_shroom": return mushroom;
                case "red_shroom": return redmushroom;
                case "gold": return goldsolid;
                case "iron": return iron;
                case "double_stair": return staircasefull;
                case "stair": return staircasestep;
                case "brick": return brick;
                case "tnt": return tnt;
                case "bookcase": return bookcase;
                case "mossy":
                case "mossy_cobble":
                case "mossycobblestone":
                case "mossycobble":
                case "mossy_cobblestone": return stonevine;
                case "obsidian": return obsidian;

                case "opglass":
                case "op_glass": return op_glass;
                case "op_obsidian":
                case "opsidian": return opsidian;
                case "op_brick": return op_brick;
                case "op_stone": return op_stone;
                case "op_cobble":
                case "op_cobblestone": return op_cobblestone;
                case "op_air": return op_air;
                case "op_water": return op_water;
                case "op_lava": return op_lava;
                case "op_tree": return op_tree;
                case "op_wood": return op_wood;

                case "wood_float": return wood_float;
                case "lava_fast": return lava_fast;

                case "door_tree":
                case "door": return door;
                case "door_obsidian":
                case "door2": return door2;
                case "door_glass":
                case "door3": return door3;
                case "door_stone":
                case "door4": return door4;
                case "door_leaves":
                case "door5": return door5;
                case "door_sand":
                case "door6": return door6;
                case "door_wood":
                case "door7": return door7;
                case "door_green":
                case "door8": return door8;
                case "door_tnt":
                case "door9": return door9;
                case "door_stair":
                case "door10": return door10;
                case "door11":
                case "door_iron": return door_iron;
                case "door12":
                case "door_dirt": return door_dirt;
                case "door13":
                case "door_grass": return door_grass;
                case "door14":
                case "door_blue": return door_blue;
                case "door15":
                case "door_book": return door_book;

                case "air_portal": return air_portal;
                case "water_portal": return water_portal;
                case "lava_portal": return lava_portal;
                case "blue_portal": return blue_portal;
                case "orange_portal": return orange_portal;

                case "smalltnt": case "small_tnt": return smalltnt;
                case "bigtnt": case "big_tnt": return bigtnt;
                case "tnt_explosion": return tntexplosion;
                case "fire": return fire;
                case "rocketstart": return rocketstart;
                case "rockethead": return rockethead;
                case "fireworks":
                case "firework": return firework;

                case "hot_lava": return deathlava;
                case "cold_water": return deathwater;
                case "nerve_gas": return deathair;
                case "acw":
                case "active_cold_water": return activedeathwater;
                case "ahl":
                case "active_hot_lava": return activedeathlava;
                case "magma": return magma;

                //case "redstone_wire-on": return redstoneWireOn;
                //case "redstone_wire-off": return redstoneWireOff;
                //case "redstone_torch_north-on": return redstoneTorchOnNorth;
                //case "redstone_torch_south-on": return redstoneTorchOnSouth;
                //case "redstone_torch_west-on": return redstoneTorchOnWest;
                //case "redstone_torch_east-on": return redstoneTorchOnEast;
                //case "redstone_torch-on": return redstoneTorchOnNormal;
                //case "redstone_torch_north-off": return redstoneTorchOffNorth;
                //case "redstone_torch_south-off": return redstoneTorchOffSouth;
                //case "redstone_torch_west-off": return redstoneTorchOffWest;
                //case "redstone_torch_east-off": return redstoneTorchOffEast;
                //case "redstone_torch-off": return redstoneTorchOffNormal;
                //case "redstone_repeater_1_north-on": return redstoneRepeaterOn1North;
                //case "redstone_repeater_1_south-on": return redstoneRepeaterOn1South;
                //case "redstone_repeater_1_west-on": return redstoneRepeaterOn1West;
                //case "redstone_repeater_1_east-on": return redstoneRepeaterOn1East;
                //case "redstone_repeater_2_north-on": return redstoneRepeaterOn2North;
                //case "redstone_repeater_2_south-on": return redstoneRepeaterOn2South;
                //case "redstone_repeater_2_west-on": return redstoneRepeaterOn2West;
                //case "redstone_repeater_2_east-on": return redstoneRepeaterOn2East;
                //case "redstone_repeater_3_north-on": return redstoneRepeaterOn3North;
                //case "redstone_repeater_3_south-on": return redstoneRepeaterOn3South;
                //case "redstone_repeater_3_west-on": return redstoneRepeaterOn3West;
                //case "redstone_repeater_3_east-on": return redstoneRepeaterOn3East;
                //case "redstone_repeater_4_north-on": return redstoneRepeaterOn4North;
                //case "redstone_repeater_4_south-on": return redstoneRepeaterOn4South;
                //case "redstone_repeater_4_west-on": return redstoneRepeaterOn4West;
                //case "redstone_repeater_4_east-on": return redstoneRepeaterOn4East;
                //case "redstone_repeater_1_north-off": return redstoneRepeaterOff1North;
                //case "redstone_repeater_1_south-off": return redstoneRepeaterOff1South;
                //case "redstone_repeater_1_west-off": return redstoneRepeaterOff1West;
                //case "redstone_repeater_1_east-off": return redstoneRepeaterOff1East;
                //case "redstone_repeater_2_north-off": return redstoneRepeaterOff2North;
                //case "redstone_repeater_2_south-off": return redstoneRepeaterOff2South;
                //case "redstone_repeater_2_west-off": return redstoneRepeaterOff2West;
                //case "redstone_repeater_2_east-off": return redstoneRepeaterOff2East;
                //case "redstone_repeater_3_north-off": return redstoneRepeaterOff3North;
                //case "redstone_repeater_3_south-off": return redstoneRepeaterOff3South;
                //case "redstone_repeater_3_west-off": return redstoneRepeaterOff3West;
                //case "redstone_repeater_3_east-off": return redstoneRepeaterOff3East;
                //case "redstone_repeater_4_north-off": return redstoneRepeaterOff4North;
                //case "redstone_repeater_4_south-off": return redstoneRepeaterOff4South;
                //case "redstone_repeater_4_west-off": return redstoneRepeaterOff4West;
                //case "redstone_repeater_4_east-off": return redstoneRepeaterOff4East;
                //case "redstone_button-on": return redstoneButtonOn;
                //case "redstone_button-off": return redstoneButtonOff;
                //case "redstone_lever-on": return redstoneLeverOn;
                //case "redstone_lever-off": return redstoneLeverOff;

                //Blocks after this are converted before saving
                case "air_flood": return air_flood;
                case "air_flood_layer": return air_flood_layer;
                case "air_flood_down": return air_flood_down;
                case "air_flood_up": return air_flood_up;
                case "door_air": return door_air;
                case "door2_air": return door2_air;
                case "door3_air": return door3_air;
                case "door4_air": return door4_air;
                case "door5_air": return door5_air;
                case "door6_air": return door6_air;
                case "door7_air": return door7_air;
                case "door8_air": return door8_air;
                case "door9_air": return door9_air;
                case "door10_air": return door10_air;
                case "door11_air": return door11_air;
                case "door12_air": return door12_air;
                case "door13_air": return door13_air;
                case "door14_air": return door14_air;
                case "door_iron_air": return door_iron_air;
                case "door_dirt_air": return door_dirt_air;
                case "door_grass_air": return door_grass_air;
                case "door_blue_air": return door_blue_air;
                case "door_book_air": return door_book_air;

                case "train": return train;
                case "train_tail": return traintail;

                //case "snake": return snake;
                //case "snake_tail": return snaketail;

                //case "blue_bird": return Block.birdblue;
                //case "red_robin": return Block.birdred;
                case "dove": return Block.birdwhite;
                case "pidgeon": return Block.birdblack;
                case "duck": return Block.birdwater;
                //case "phoenix": return Block.birdlava;
                //case "killer_phoenix": return Block.birdkill;

                //case "betta_fish": return fishbetta;
                //case "goldfish": return fishgold;
                //case "salmon": return fishsalmon;
                //case "shark": return fishshark;
                //case "sea_sponge": return fishsponge;
                //case "lava_shark": return fishlavashark;

                default: return Zero;
            }
        }
        public static Random Rand = new Random();
        public static Color ChooseFrom(int r1, int g1, int b1, int r2, int g2, int b2, int percent1 = 50)
        {
            if (Rand.Next(0, 99) < percent1) return Color.FromArgb(r1, g1, b1);
            else return Color.FromArgb(r2, g2, b2);
        }
        public static Color ChooseFrom(Color[] c)
        {
            return c[Rand.Next(c.Length - 1)];
        }
        public static Color ChooseFrom(int r1, int g1, int b1, Color c, int percent1 = 50)
        {
            if (Rand.Next(0, 99) < percent1) return Color.FromArgb(r1, g1, b1);
            else return c;
        }
        public static Color GetColor(ushort b)
        {
            ushort b2 = (ushort)Convert(b);
            switch (b2)
            {
                case rock: return Color.Gray;
                case grass: return ChooseFrom(0, 150, 0, 80, 150, 80, 10);
                case dirt: return ChooseFrom(80, 40, 0, 100, 40, 0, 10);
                case stone: return ChooseFrom(70, 70, 70, 60, 60, 60, 10);
                case wood: return Color.Brown;
                case shrub: return Color.LightGreen;
                case blackrock: return ChooseFrom(new Color[] { Color.FromArgb(20, 20, 20), Color.FromArgb(50, 50, 50), Color.FromArgb(100, 100, 100), Color.FromArgb(150, 150, 150), Color.Black });
                case waterstill:
                case water: return Color.Blue;
                case lavastill:
                case lava: return ChooseFrom(255, 0, 0, 255, 127, 0, 50);
                case sand: return Color.LightYellow;
                case gravel: return Color.Gray;
                case goldrock: return ChooseFrom(255, 220, 0, Color.Gray, 10);
                case ironrock: return ChooseFrom(255, 220, 220, Color.Gray, 10);
                case coal: return ChooseFrom(0, 0, 0, Color.Gray, 10);
                case trunk: return Color.Brown;
                case leaf: return ChooseFrom(0, 255, 0, Color.Transparent, 40);
                case sponge: return Color.Yellow;
                case glass: return ChooseFrom(255, 255, 255, Color.Transparent, 20);
                case red: return Color.Red;
                case orange: return Color.Orange;
                case yellow: return Color.Yellow;
                case lightgreen: return Color.LightGreen;
                case green: return Color.Green;
                case aquagreen: return Color.Green;
                case cyan: return Color.Cyan;
                case lightblue: return Color.LightBlue;
                case purple: return Color.Purple;
                case lightpurple: return Color.DeepPink;
                case pink: return Color.Pink;
                case darkpink: return Color.DeepPink;
                case darkgrey: return Color.DarkGray;
                case lightgrey: return Color.LightGray;
                case white: return Color.White;
                case yellowflower: return ChooseFrom(255, 255, 0, Color.Transparent, 5);
                case goldsolid: return Color.Gold;
                case iron: return Color.Silver;
                case staircasestep:
                case staircasefull: return Color.LightGray;
                case brick: return Color.OrangeRed;
                case tnt: return Color.Red;
                case bookcase: return Color.Brown;
                case obsidian: return Color.Black;
                default: return Color.Transparent;
            }
        }
        public static byte Convert(ushort b)
        {
            switch (b)
            {
                case op_glass: return (byte)glass;
                case opsidian: return (byte)obsidian;
                case op_brick: return (byte)brick;
                case op_stone: return (byte)rock;
                case op_cobblestone: return (byte)stone;
                case op_air: return (byte)air;
                case op_water: return (byte)waterstill;
                case op_lava: return (byte)lavastill;
                case op_tree: return (byte)trunk;
                case op_wood: return (byte)wood;

                case wood_float: return (byte)wood;
                case lava_fast: return (byte)lavastill;

                case door: return (byte)trunk;
                case door2: return (byte)obsidian;
                case door3: return (byte)glass;
                case door4: return (byte)rock;
                case door5: return (byte)leaf;
                case door6: return (byte)sand;
                case door7: return (byte)wood;
                case door8: return (byte)green;
                case door9: return (byte)tnt;
                case door10: return (byte)staircasestep;
                case door_iron: return (byte)iron;
                case door_dirt: return (byte)dirt;
                case door_grass: return (byte)grass;
                case door_blue: return (byte)blue;
                case door_book: return (byte)bookcase;
                case water_door: return (byte)waterstill;
                case lava_door: return (byte)lavastill;

                case air_portal: return (byte)air;
                case water_portal: return (byte)waterstill;
                case lava_portal: return (byte)lavastill;
                case blue_portal: return (byte)blue;
                case orange_portal: return (byte)orange;

                case air_flood:
                case air_flood_layer:
                case air_flood_down:
                case air_flood_up:
                case door_air:
                case door2_air:
                case door3_air:
                case door4_air:
                case door5_air:
                case door6_air:
                case door7_air:
                case door10_air:
                case door11_air:
                case door12_air:
                case door13_air:
                case door14_air:
                case door_iron_air:
                case door_dirt_air:
                case door_grass_air:
                case door_blue_air:
                case door_book_air:
                case air_door:
                case air_switch:
                    return (byte)air;
                case door8_air: return (byte)red;
                case door9_air: return (byte)lavastill;

                case smalltnt: return (byte)tnt;
                case bigtnt: return (byte)tnt;
                case tntexplosion: return (byte)lavastill;

                case fire: return (byte)lavastill;

                case rocketstart: return (byte)glass;
                case rockethead: return (byte)goldsolid;
                case firework: return (byte)iron;

                case deathwater: return (byte)waterstill;
                case deathlava: return (byte)lavastill;
                case deathair: return (byte)air;
                case activedeathwater: return (byte)water;
                case activedeathlava: return (byte)lava;

                case magma: return (byte)lava;
                    
                case redstoneWireOn: return (byte)green;
                case redstoneWireOff: return (byte)red;
                case redstoneTorchOnNorth:
                case redstoneTorchOnSouth:
                case redstoneTorchOnWest:
                case redstoneTorchOnEast:
                case redstoneTorchOnNormal: return (byte)goldsolid;
                case redstoneTorchOffNorth: 
                case redstoneTorchOffSouth: 
                case redstoneTorchOffWest: 
                case redstoneTorchOffEast:
                case redstoneTorchOffNormal: return (byte)iron;
                case redstoneRepeaterOn1North: 
                case redstoneRepeaterOn1South:
                case redstoneRepeaterOn1West:
                case redstoneRepeaterOn1East:
                case redstoneRepeaterOn2North:
                case redstoneRepeaterOn2South:
                case redstoneRepeaterOn2West:
                case redstoneRepeaterOn2East:
                case redstoneRepeaterOn3North:
                case redstoneRepeaterOn3South:
                case redstoneRepeaterOn3West:
                case redstoneRepeaterOn3East:
                case redstoneRepeaterOn4North:
                case redstoneRepeaterOn4South:
                case redstoneRepeaterOn4West:
                case redstoneRepeaterOn4East: return (byte)staircasefull;
                case redstoneRepeaterOff1North:
                case redstoneRepeaterOff1South:
                case redstoneRepeaterOff1West:
                case redstoneRepeaterOff1East:
                case redstoneRepeaterOff2North:
                case redstoneRepeaterOff2South:
                case redstoneRepeaterOff2West:
                case redstoneRepeaterOff2East:
                case redstoneRepeaterOff3North:
                case redstoneRepeaterOff3South:
                case redstoneRepeaterOff3West:
                case redstoneRepeaterOff3East:
                case redstoneRepeaterOff4North:
                case redstoneRepeaterOff4South:
                case redstoneRepeaterOff4West:
                case redstoneRepeaterOff4East: return (byte)staircasestep;
                case redstoneButtonOn: return (byte)white;
                case redstoneButtonOff: return (byte)darkgrey;
                case redstoneLeverOn: return (byte)white;
                case redstoneLeverOff: return (byte)darkgrey;

                case train: return (byte)cyan;
                case traintail: return (byte)obsidian;

                case snake: return (byte)darkgrey;
                case snaketail: return (byte)coal;

                case birdwhite: return (byte)white;
                case birdblack: return (byte)darkgrey;
                case birdlava: return (byte)lavastill;
                case birdred: return (byte)red;
                case birdwater: return (byte)waterstill;
                case birdblue: return (byte)blue;
                case birdkill: return (byte)lavastill;

                case fishbetta: return (byte)blue;
                case fishgold: return (byte)goldsolid;
                case fishsalmon: return (byte)red;
                case fishshark: return (byte)lightgrey;
                case fishsponge: return (byte)sponge;
                case fishlavashark: return (byte)obsidian;

                default: if (b < 50) return (byte)b; else return (byte)orange;
            }
        }
        public static ushort SaveConvert(ushort b)
        {
            switch (b)
            {
                case air_flood:
                case air_flood_layer:
                case air_flood_down:
                case air_flood_up:
                    return air; //air_flood must be converted to air on save to prevent issues
                case door_air: return door;
                case door2_air: return door2;
                case door3_air: return door3;
                case door4_air: return door4;
                case door5_air: return door5;
                case door6_air: return door6;
                case door7_air: return door7;
                case door8_air: return door8;
                case door9_air: return door9;
                case door10_air: return door10;
                case door11_air: return air_door;
                case door12_air: return air_switch;
                case door13_air: return water_door;
                case door14_air: return lava_door;
                default: return b;
            }
        }
        public static ushort DoorAirs(ushort b)
        {
            switch (b)
            {
                case door: return door_air;
                case door2: return door2_air;
                case door3: return door3_air;
                case door4: return door4_air;
                case door5: return door5_air;
                case door6: return door6_air;
                case door7: return door7_air;
                case door8: return door8_air;
                case door9: return door9_air;
                case door10: return door10_air;
                case air_switch: return door11_air;
                case water_door: return door12_air;
                case lava_door: return door13_air;
                case air_door: return door14_air;
                default: return 0;
            }
        }
    }
}
