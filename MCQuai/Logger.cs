﻿using System;
using System.IO;
using System.Text;

namespace MCRevive
{
    public class Logger
    {
        public static void LogMessage(string text)
        {
            if (Server.reportBack)
            {
                int tries = 0;
            retry:
                try
                {
                    if (text.Length > 0)
                    {
                        File.AppendAllText("logs/" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt", text);
                    }
                }
                catch (DirectoryNotFoundException)
                {
                    if (!Directory.Exists("logs"))
                        Directory.CreateDirectory("logs");
                    if (tries > 2)
                        throw new Exception();
                    tries++;
                    goto retry;
                }
                catch (Exception)
                {
                    Server.Restart();
                }
            }
        }
        public static void LogError(Exception ex)
        {
            int tries = 0;
            retry:
            try
            {
                if (ex != null)
                {
                    string text = "---- " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " ----" + Environment.NewLine + getErrorText(ex) + Environment.NewLine;
                    File.AppendAllText("logs/errors/" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt", text);
                }
            }
            catch (DirectoryNotFoundException)
            {
                if (!Directory.Exists("logs"))
                    Directory.CreateDirectory("logs");
                if (tries > 2)
                    throw new Exception();
                tries++;
                goto retry;
            }
            catch (Exception)
            {
                Server.Restart();
            }
        }
        static string getErrorText(Exception ex)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Type: " + ex.GetType().Name);
            sb.AppendLine("Source: " + ex.Source);
            sb.AppendLine("Message: " + ex.Message);
            sb.AppendLine("Target: " + ex.TargetSite.Name);
            sb.AppendLine("Trace: " + ex.StackTrace);

            return sb.ToString();
        }
    }
}